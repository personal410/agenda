package com.pacifico.agenda.Views.CustomView;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class setFuente {

    public static void setFuenteRg(Context context, TextView rb){

        String font_path = "foco_std_rg_webfont.ttf";

        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);

        rb.setTypeface(TF);


    }

    public static void setFuentebd(Context context, TextView rb){

        String font_path = "foco_std_bd_webfont.ttf";

        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);

        rb.setTypeface(TF);


    }

    public static void setRadioButton(Context context, RadioButton rb){

        String font_path = "foco_std_rg_webfont.ttf";

        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);

        rb.setTypeface(TF);


    }

    public static void setButton(Context context, Button btn){

        String font_path = "foco_std_rg_webfont.ttf";

        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);

        btn.setTypeface(TF);


    }
}
