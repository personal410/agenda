package com.pacifico.agenda.Views.Dialogs;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.pacifico.agenda.R;

public class DialogEntrevista extends DialogFragment {


    IDialogEntrevista mListener;
    String mTitulo;
    String mMensaje;

    private TextView tvTitulo;
    private TextView tvMensaje;

    public DialogEntrevista() {

    }

    public interface IDialogEntrevista {
        void respuesta_cierre_venta();
        void respuesta_agendar();
        void respuesta_descartar();
    }


    public void setListener(IDialogEntrevista mListener) {
        this.mListener = mListener;
    }

    public void setDatos(String titulo, String mensaje) {
        this.mTitulo = titulo;
        this.mMensaje = mensaje;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_post_entrevista, null);

        this.tvTitulo = (TextView) view.findViewById(R.id.tvTitulo);
        this.tvTitulo.setText(mTitulo);

        this.tvMensaje = (TextView) view.findViewById(R.id.tvMensaje);
        this.tvMensaje.setText(mMensaje);

        builder.setView(view)
                .setPositiveButton(getString(R.string.etiqueta_boton_descartar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_descartar();
                    }
                })
                .setNegativeButton(getString(R.string.etiqueta_boton_siguiente_entrevista), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_agendar();
                    }
                })
                .setNegativeButton(getString(R.string.etiqueta_boton_cierre_venta), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_cierre_venta();
                    }
                });
        return builder.create();
    }

}

