package com.pacifico.agenda.Views.CustomView;

import android.graphics.Paint;
import android.util.TypedValue;
import android.widget.TextView;

/**
 * Created by vctrls3477 on 18/08/16.
 */
public class ViewUtil {
    public static void resizeText(TextView textView, int originalTextSize, int minTextSize) {
        final Paint paint = textView.getPaint();
        final int width = textView.getWidth();
        if (width == 0) return;
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, originalTextSize);
        float ratio = width / paint.measureText(textView.getText().toString());
        if (ratio <= 1.0f) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, Math.max(minTextSize, originalTextSize * ratio));
        }
    }
}