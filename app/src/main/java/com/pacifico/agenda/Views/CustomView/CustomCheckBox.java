package com.pacifico.agenda.Views.CustomView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.pacifico.agenda.R;

/**
 * Created by xcode on 8/08/2016.
 */
public class CustomCheckBox extends AppCompatCheckBox {
    public CustomCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomCheckBox, 0, 0);
        try {
            String typeFaceName = typedArray.getString(R.styleable.CustomCheckBox_typeFace5);
            if(typeFaceName!=null && !typeFaceName.isEmpty()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), typeFaceName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            typedArray.recycle();
        }


    }
}
