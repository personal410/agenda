package com.pacifico.agenda.Views.CustomView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.pacifico.agenda.R;

/**
 * Created by victorsalazar on 18/05/16.
 */
public class CustomEditText extends EditText{
    public CustomEditText(Context context){
        super(context);
        this.configure();
    }
    public CustomEditText(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CustomEditText, 0, 0);
        try {
            String typeFaceName = typedArray.getString(R.styleable.CustomEditText_typeFace3);
            if(typeFaceName!=null && !typeFaceName.isEmpty()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), typeFaceName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            typedArray.recycle();
        }
        this.configure();
    }
    public void configure(){
        this.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == 6){
                    CustomEditText.this.clearFocus();
                    InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(CustomEditText.this.getWindowToken(), 0);
                }
                return false;
            }
        });
    }
    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event){
        if(event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
            this.clearFocus();
        }
        return super.onKeyPreIme(keyCode, event);
    }

    public void setError(CharSequence error) {
        if (error == null){
            setCompoundDrawables(null, null, null, null);
        }else{
            Drawable errorIcon = getResources().getDrawable(R.drawable.ic_error);
            errorIcon.setBounds(new Rect(0, 0, errorIcon.getIntrinsicWidth(), errorIcon.getIntrinsicHeight()));
            setCompoundDrawables(null, null, errorIcon, null);
        }
    }

}