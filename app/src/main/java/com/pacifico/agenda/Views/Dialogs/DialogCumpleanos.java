package com.pacifico.agenda.Views.Dialogs;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pacifico.agenda.Model.Bean.GeneralBean;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Views.CustomView.setFuente;

import java.util.List;

public class DialogCumpleanos extends DialogFragment {


    IDialogSiNo mListener;
    String mTitulo;
    List<GeneralBean> mLista;

    TextView tvTitulo;
    ListView lvLista;



    public DialogCumpleanos() {

    }

    public void setListener(IDialogSiNo mListener) {
        this.mListener = mListener;
    }

    public void setDatos(String titulo, List<GeneralBean> lista) {
        this.mTitulo = titulo;
        this.mLista = lista;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_cumpleanos, null);

        this.tvTitulo = (TextView) view.findViewById(R.id.tvTitulo);
        this.tvTitulo.setText(mTitulo);

        this.lvLista = (ListView) view.findViewById(R.id.lvLista);

        DialogCierreAdapter adapter = new DialogCierreAdapter(getActivity(), R.layout.row_dialogo_cumpleanos, mLista);
        lvLista.setAdapter(adapter);



        builder.setView(view)
                .setNegativeButton(R.string.CERRAR, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_no();
                    }
                });

        return builder.create();
    }


    class DialogCierreAdapter extends ArrayAdapter<GeneralBean> {
        private Activity context;
        private int resource;
        private List<GeneralBean> items;

        public DialogCierreAdapter(Activity context, int resource, List<GeneralBean> items) {
            super(context, resource, items);
            this.context = context;
            this.resource = resource;
            this.items = items;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final GeneralBean item = items.get(position);
            final LinearLayout rowView;


            rowView = new LinearLayout(getContext());
            inflater.inflate(resource, rowView, true);


            TextView tvNombre =  (TextView)rowView.findViewById(R.id.tvNombre);
            TextView tvReferencia =  (TextView)rowView.findViewById(R.id.tvReferencia);
            TextView tvTelefono =  (TextView)rowView.findViewById(R.id.tvTelefono);

            tvNombre.setText(item.getCampo1());
            tvReferencia.setText(item.getCampo2());
            tvTelefono.setText(item.getCampo3());

            return rowView;
        }

    }


}

