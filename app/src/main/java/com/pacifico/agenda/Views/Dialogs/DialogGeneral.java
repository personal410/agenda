package com.pacifico.agenda.Views.Dialogs;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pacifico.agenda.R;
import com.pacifico.agenda.Views.CustomView.setFuente;

public class DialogGeneral extends DialogFragment {


    IDialogSiNo mListener;
    String mTitulo;

    private android.widget.TextView tvTitulo;
    private String mEtiquetaSI;
    private String mEtiquetaNO;
    public DialogGeneral() {
    }
    public void setListener(IDialogSiNo mListener) {
        this.mListener = mListener;
    }

    public void setDatos(String titulo, String etiquetaSI, String etiquetaNo) {
        this.mTitulo = titulo;
        this.mEtiquetaSI = etiquetaSI;
        this.mEtiquetaNO = etiquetaNo;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_general, null);

        this.tvTitulo = (TextView) view.findViewById(R.id.tvTitulo);
        this.tvTitulo.setText(mTitulo);

        builder.setView(view)
                .setPositiveButton(mEtiquetaSI, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_si();
                    }
                })
                .setNegativeButton(mEtiquetaNO, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_no();
                    }
                });


       AlertDialog alert = builder.create();
        alert.show();
        Button btnCancelar = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button btnAceptar = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            btnAceptar.setTextColor(getResources().getColor(R.color.colorCeleste));
            btnCancelar.setTextColor(getResources().getColor(R.color.colorMarron));
            setFuente.setButton(getActivity(), btnAceptar);
            setFuente.setButton(getActivity(), btnCancelar);

        return alert;
    }

}

