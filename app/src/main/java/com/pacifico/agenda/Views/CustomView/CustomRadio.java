package com.pacifico.agenda.Views.CustomView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.pacifico.agenda.R;

/**
 * Created by xcode on 8/08/2016.
 */
public class CustomRadio extends RadioButton {
    public CustomRadio(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomRadio, 0, 0);
        try {
            String typeFaceName = typedArray.getString(R.styleable.CustomRadio_typeFace4);
            if(typeFaceName!=null && !typeFaceName.isEmpty()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), typeFaceName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            typedArray.recycle();
        }

    }
}
