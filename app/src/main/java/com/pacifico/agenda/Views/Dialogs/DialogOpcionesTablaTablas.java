package com.pacifico.agenda.Views.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.pacifico.agenda.Model.Bean.TablaTablasBean;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Views.CustomView.setFuente;

import java.util.ArrayList;

/**
 * Created by joel on 7/22/16.
 */
public class DialogOpcionesTablaTablas extends DialogFragment {

    IDialogSiNoValor mListener;
    String mTitulo;

    RadioGroup rdgItemsOpciones;

    private android.widget.TextView tvTitulo;
    private String mEtiquetaSI;
    private String mEtiquetaNO;
    private ArrayList<TablaTablasBean> listaItems;
    private int mItemSeleccionadoInicial;

    public DialogOpcionesTablaTablas() {

    }

    public void setListener(IDialogSiNoValor mListener) {
        this.mListener = mListener;
    }

    public void setDatos(String titulo, String etiquetaSI, String etiquetaNo, ArrayList<TablaTablasBean> opcionesTablaTablas,int itemSeleccionadoInicial) {
        this.mTitulo = titulo;
        this.mEtiquetaSI = etiquetaSI;
        this.mEtiquetaNO = etiquetaNo;
        listaItems =  opcionesTablaTablas;

        mItemSeleccionadoInicial = itemSeleccionadoInicial;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_opciones, null);

        this.tvTitulo = (TextView) view.findViewById(R.id.tvTitulo);
        this.tvTitulo.setText(mTitulo);

        this.rdgItemsOpciones = (RadioGroup) view.findViewById(R.id.rdgItemsOpciones);

        builder.setView(view)
                .setPositiveButton(mEtiquetaSI, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null) {
                            int id_opcion = -1;

                            id_opcion = rdgItemsOpciones.getCheckedRadioButtonId();
                            if (id_opcion == -1) return;

                            View radioButton = rdgItemsOpciones.findViewById(id_opcion);
                            int idx = rdgItemsOpciones.indexOfChild(radioButton);

                            mListener.respuesta_si(listaItems.get(idx));
                        }
                    }
                })
                .setNegativeButton(mEtiquetaNO, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_no();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        Button btnAceptar = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        Button btnCancelar = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        btnAceptar.setTextColor(getResources().getColor(R.color.colorCeleste));
        btnCancelar.setTextColor(getResources().getColor(R.color.colorMarron));
        setFuente.setButton(getActivity(),btnAceptar);
        setFuente.setButton(getActivity(),btnCancelar);

        int rbChecked = -1;
        // Agregamos los rb
        for(int i = 0; i<listaItems.size();i++){
            AppCompatRadioButton rb = new AppCompatRadioButton (this.getActivity());
            rb.setText(listaItems.get(i).getValorCadena());
            setFuente.setRadioButton(getContext(),rb);
            rb.setTextSize(18);

            ColorStateList colorStateList = new ColorStateList(
                    new int[][]{

                            new int[]{-android.R.attr.state_checked}, //disabled
                            new int[]{android.R.attr.state_checked} //enabled
                    },
                    new int[] {

                            getResources().getColor(R.color.colorGris)//disabled
                            ,getResources().getColor(R.color.colorCeleste), //enabled

                    }
            );
            rb.setSupportButtonTintList(colorStateList);
            rdgItemsOpciones.addView(rb);

            // Validar si es el checkeado
            if (listaItems.get(i).getCodigoCampo() == mItemSeleccionadoInicial)
                rbChecked = rb.getId();

        }

        if (rbChecked != -1)
            rdgItemsOpciones.check(rbChecked);
        return alertDialog;
    }

}
