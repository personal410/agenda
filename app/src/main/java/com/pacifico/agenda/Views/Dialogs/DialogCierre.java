package com.pacifico.agenda.Views.Dialogs;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pacifico.agenda.Model.Bean.GeneralBean;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Views.CustomView.setFuente;

import java.util.List;

public class DialogCierre extends DialogFragment {


    IDialogSiNo mListener;
    String mTitulo;
    List<GeneralBean> mLista;

    TextView tvTitulo;
    ListView lvLista;



    public DialogCierre() {

    }

    public void setListener(IDialogSiNo mListener) {
        this.mListener = mListener;
    }

    public void setDatos(String titulo, List<GeneralBean> lista) {
        this.mTitulo = titulo;
        this.mLista = lista;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_cierre, null);

        this.tvTitulo = (TextView) view.findViewById(R.id.tvTitulo);
        this.tvTitulo.setText(mTitulo);

        this.lvLista = (ListView) view.findViewById(R.id.lvLista);

        DialogCierreAdapter adapter = new DialogCierreAdapter(getActivity(), R.layout.row_dialogo_cierre, mLista);
        lvLista.setAdapter(adapter);

        builder.setView(view)
                .setPositiveButton(R.string.CONFIRMAR_CIERRE, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (mListener != null)
                            mListener.respuesta_si();
                    }
                })
                .setNegativeButton(R.string.CANCELAR, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_no();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
        Button btnPositivo = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        Button btnNegativo = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        btnPositivo.setTextColor(getResources().getColor(R.color.colorCeleste));
        setFuente.setButton(getActivity(),btnPositivo);
        setFuente.setButton(getActivity(),btnNegativo);


        return builder.create();
    }


    class DialogCierreAdapter extends ArrayAdapter<GeneralBean> {

        private Activity context;
        private int resource;
        private List<GeneralBean> items;

        public DialogCierreAdapter(Activity context, int resource, List<GeneralBean> items) {
            super(context, resource, items);
            this.context = context;
            this.resource = resource;
            this.items = items;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final GeneralBean item = items.get(position);
            final LinearLayout rowView;


            rowView = new LinearLayout(getContext());
            inflater.inflate(resource, rowView, true);


            AppCompatCheckBox cbItem =  (AppCompatCheckBox)rowView.findViewById(R.id.cbItem);

            cbItem.setChecked(false);
            cbItem.setText(item.getCampo1());

            return rowView;
        }

    }


}

