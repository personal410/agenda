package com.pacifico.agenda.Views.Dialogs;

import com.pacifico.agenda.Model.Bean.TablaTablasBean;

/**
 * Created by joel on 7/24/16.
 */
public interface IDialogSiNoValor {
    void respuesta_si(TablaTablasBean valor);
    void respuesta_no();
}
