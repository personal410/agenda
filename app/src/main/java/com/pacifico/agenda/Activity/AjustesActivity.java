package com.pacifico.agenda.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.github.clans.fab.FloatingActionMenu;
import com.pacifico.agenda.Fragments.Ajustes.AjustesFragment;
import com.pacifico.agenda.R;

public class AjustesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private FloatingActionMenu famAcciones;
    AjustesFragment menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        famAcciones = (FloatingActionMenu) findViewById(R.id.famAcciones);
        famAcciones.setVisibility(View.GONE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout1);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Button button =(Button)findViewById(R.id.btnMostrarCalendario) ;
        button.setText("Ajustes");
        button.setFocusable(false);
        button.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_Ajustes1);
        navigationView.setNavigationItemSelectedListener(this);

        menu = new AjustesFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.FragmentContainers,menu ).commit();

    }
    @Override
    public void onBackPressed(){}
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.nav_agenda){
            Intent intent = new Intent(AjustesActivity.this,CalendarioActivity.class);
            startActivity(intent);
        }else if(id == R.id.nav_salir){
            finishAffinity();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout1);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}