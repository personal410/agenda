package com.pacifico.agenda.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pacifico.agenda.Model.Bean.DispositivoBean;
import com.pacifico.agenda.Model.Bean.IntermediarioBean;
import com.pacifico.agenda.Model.Controller.DispositivoController;
import com.pacifico.agenda.Model.Controller.IntermediarioController;
import com.pacifico.agenda.Network.Request.ValidarToken.ValidarTokenRequest;
import com.pacifico.agenda.Network.Request.ValidarToken.ValidarTokenTokenRequest;
import com.pacifico.agenda.Network.Request.ValidarUsuario.ValidarUsuarioRequest;
import com.pacifico.agenda.Network.Request.ValidarUsuario.ValidarUsuarioTokenRequest;
import com.pacifico.agenda.Network.Response.ValidarToken.ValidarTokenResponse;
import com.pacifico.agenda.Network.Response.ValidarToken.ValidarTokenUsuarioResponse;
import com.pacifico.agenda.Network.Response.ValidarUsuario.ValidarUsuarioResponse;
import com.pacifico.agenda.Network.RestMethods;
import com.pacifico.agenda.Network.SincronizacionController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Views.CustomView.CustomEditText;
import com.squareup.okhttp.OkHttpClient;

import java.lang.reflect.Method;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class InicioSesionActivity extends AppCompatActivity implements TextWatcher{
    final private String MensajeIngresaCorreoCompleto = "Ingresa el token recibido";
    private CustomEditText edtCodigoAsesor;
    private Button btnSiguiente;
    private String numeroSerie = "", login = "", token = "";
    private int etapa = 1;
    private boolean verificarIntermediario = false;
    private ProgressDialog progressDialog;
    private IntermediarioBean intermediarioBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);
        edtCodigoAsesor = (CustomEditText)findViewById(R.id.edtCodigoAsesor);
        intermediarioBean = IntermediarioController.obtenerIntermediario();
        if(intermediarioBean == null){
            verificarIntermediario = true;
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            try{
                Class<?> c = Class.forName("android.os.SystemProperties");
                Method get = c.getMethod("get", String.class, String.class);
                numeroSerie = (String)get.invoke(c, "sys.serialnumber", "Error");
                if(numeroSerie.equals("Error")) {
                    numeroSerie = (String)get.invoke(c, "ril.serialnumber", "Error");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            DispositivoBean dispositivoBean = DispositivoController.obtenerDispositivoPorIdDispositivo(1);
            Bundle extras = getIntent().getExtras();
            numeroSerie = dispositivoBean.getNumeroSerie();
            etapa = extras.getInt("etapa");
            if(etapa == 2){
                edtCodigoAsesor.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
                TextInputLayout textInputLayout = (TextInputLayout)edtCodigoAsesor.getParent();
                textInputLayout.setHint(MensajeIngresaCorreoCompleto);
                btnSiguiente.setText("Ingresar");
                btnSiguiente.setEnabled(false);
                btnSiguiente.setBackgroundColor(getResources().getColor(R.color.colorGris));
            }
            Log.i("TAG", "etapa: " + etapa);
        }
        //edtCodigoAsesor.setText("BTafu");
        edtCodigoAsesor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(etapa == 2){
                    TextInputLayout textInputLayout = (TextInputLayout)edtCodigoAsesor.getParent();
                    if(hasFocus){
                        textInputLayout.setHint("Ingresa el token");
                    }else{
                        if(edtCodigoAsesor.getText().toString().length() == 0){
                            textInputLayout.setHint(MensajeIngresaCorreoCompleto);
                        }
                    }
                }
            }
        });
        btnSiguiente = (Button)findViewById(R.id.btnSiguiente);
        edtCodigoAsesor.addTextChangedListener(this);
        btnSiguiente.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(progressDialog == null){
                    progressDialog = new ProgressDialog(InicioSesionActivity.this);
                    progressDialog.setMessage("Espere, por favor");
                    progressDialog.setCancelable(false);
                }
                progressDialog.setTitle(etapa == 1 ? "Validando usuario" : "Validando token");
                progressDialog.show();
                OkHttpClient okHttpClient = new OkHttpClient();
                try{
                    SincronizacionController sincronizacionController = new SincronizacionController();
                    okHttpClient.setSslSocketFactory(sincronizacionController.getSSLSocketFactory(InicioSesionActivity.this));
                }catch(Exception e){}
                if(etapa == 1){
                    Retrofit retrofit = new Retrofit.Builder().baseUrl(Constantes.URLBase).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
                    RestMethods restMethods = retrofit.create(RestMethods.class);
                    ValidarUsuarioTokenRequest validarUsuarioTokenRequest = new ValidarUsuarioTokenRequest();
                    login = edtCodigoAsesor.getText().toString();
                    validarUsuarioTokenRequest.setLogin(login);
                    validarUsuarioTokenRequest.setIdDispositivo(numeroSerie);
                    ValidarUsuarioRequest validarUsuarioRequest = new ValidarUsuarioRequest();
                    validarUsuarioRequest.setToken(validarUsuarioTokenRequest);
                    Gson gson = new Gson();
                    String string = gson.toJson(validarUsuarioRequest);
                    Log.i("TAG", "json: " + string);
                    Call<ValidarUsuarioResponse> respuesta = restMethods.ValidarUsuario(validarUsuarioRequest);
                    respuesta.enqueue(new Callback<ValidarUsuarioResponse>() {
                        @Override
                        public void onResponse(Response<ValidarUsuarioResponse> response, Retrofit retrofit){
                            progressDialog.dismiss();
                            ValidarUsuarioResponse validarUsuarioResponse = response.body();
                            if(validarUsuarioResponse != null){
                                TextInputLayout textInputLayout = (TextInputLayout)edtCodigoAsesor.getParent();
                                if(validarUsuarioResponse.getIsValid().toLowerCase().equals("true")){
                                    //edtCodigoAsesor.setText("6D7C7");
                                    edtCodigoAsesor.setText("");
                                    edtCodigoAsesor.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
                                    textInputLayout.setHint(MensajeIngresaCorreoCompleto);
                                    btnSiguiente.setText("Ingresar");
                                    btnSiguiente.setEnabled(false);
                                    btnSiguiente.setBackgroundColor(getResources().getColor(R.color.colorGris));
                                    etapa = 2;
                                }else{
                                    textInputLayout.setError(validarUsuarioResponse.getResultMessage());
                                }
                            }else{
                                Toast.makeText(InicioSesionActivity.this, "Hubo un error en la conexión", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Throwable t){
                            progressDialog.dismiss();
                            Toast.makeText(InicioSesionActivity.this, "Hubo un error en la conexión", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Retrofit retrofit = new Retrofit.Builder().baseUrl(Constantes.URLBase).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
                    RestMethods restMethods = retrofit.create(RestMethods.class);
                    ValidarTokenTokenRequest validarTokenTokenRequest = new ValidarTokenTokenRequest();
                    token = edtCodigoAsesor.getText().toString();
                    validarTokenTokenRequest.setLogin(login);
                    validarTokenTokenRequest.setCodigoValidacion(token);
                    validarTokenTokenRequest.setIdDispositivo(numeroSerie);
                    ValidarTokenRequest validarTokenRequest = new ValidarTokenRequest();
                    validarTokenRequest.setToken(validarTokenTokenRequest);
                    Gson gson = new Gson();
                    String string = gson.toJson(validarTokenRequest);
                    Log.i("TAG", "json: " + string);
                    Call<ValidarTokenResponse> respuesta = restMethods.ValidarToken(validarTokenRequest);
                    respuesta.enqueue(new Callback<ValidarTokenResponse>() {
                        @Override
                        public void onResponse(Response<ValidarTokenResponse> response, Retrofit retrofit){
                            progressDialog.dismiss();
                            ValidarTokenResponse validarTokenResponse = response.body();
                            if(validarTokenResponse.getResultCode().equals("1")){
                                if (intermediarioBean == null) {
                                    ValidarTokenUsuarioResponse validarTokenUsuarioResponse = validarTokenResponse.getUsuario();
                                    Intent onboardingIntent = new Intent(InicioSesionActivity.this, OnboardingActivity.class);
                                    onboardingIntent.putExtra("login", login);
                                    onboardingIntent.putExtra("token", token);
                                    onboardingIntent.putExtra("numeroSerie", numeroSerie);
                                    onboardingIntent.putExtra("codigoIntermediario", validarTokenUsuarioResponse.getCodigoIntermediario());
                                    startActivity(onboardingIntent);
                                } else {
                                    Intent resultIntent = new Intent();
                                    setResult(Activity.RESULT_OK, resultIntent);
                                    finish();
                                }
                            }else{
                                TextInputLayout textInputLayout = (TextInputLayout)edtCodigoAsesor.getParent();
                                textInputLayout.setError(validarTokenResponse.getResultMessage());
                            }
                        }
                        @Override
                        public void onFailure(Throwable t){
                            progressDialog.dismiss();
                            Toast.makeText(InicioSesionActivity.this, "Hubo un error en la conexión", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
    @Override
    protected void onResume(){
        super.onResume();
        if(IntermediarioController.obtenerIntermediario() != null && verificarIntermediario){
            Intent buscarProspectoIntent = new Intent(this, CalendarioActivity.class);
            startActivity(buscarProspectoIntent);
        }
    }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after){}
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count){}
    @Override
    public void afterTextChanged(Editable s){
        boolean botonHabilitado = (edtCodigoAsesor.getText().length() > 0);
        btnSiguiente.setEnabled(botonHabilitado);
        btnSiguiente.setBackgroundColor(botonHabilitado ? getResources().getColor(R.color.colorNaranja) : getResources().getColor(R.color.colorGris));
        TextInputLayout textInputLayout = (TextInputLayout)edtCodigoAsesor.getParent();
        textInputLayout.setError(null);
        textInputLayout.setErrorEnabled(false);
    }
    @Override
    public void onBackPressed(){}
}