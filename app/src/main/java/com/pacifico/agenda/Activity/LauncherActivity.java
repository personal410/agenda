package com.pacifico.agenda.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.Model.Controller.IntermediarioController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;

import java.util.Timer;
import java.util.TimerTask;

public class LauncherActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run(){

                int tipo_operacion = -1;
                try{
                    Intent intent = getIntent();
                    tipo_operacion = intent.getExtras().getInt(Constantes.tipo_operacion);
                }catch(Exception e ){}

                if (tipo_operacion == Constantes.operacion_ADNTerminaryAgendar)
                {// SI llegan extras siempre hay data
                    Intent intent = getIntent();
                    int idprospecto = -1;
                    try {
                        idprospecto = intent.getExtras().getInt(Constantes.parametro_idProspecto);
                    }catch(Exception e){}

                    Intent intent_calendar = new Intent(LauncherActivity.this, CalendarioActivity.class);
                    intent_calendar.putExtra(Constantes.tipo_operacion,Constantes.operacion_ADNTerminaryAgendar);
                    intent_calendar.putExtra(Constantes.parametro_idProspecto,idprospecto);
                    startActivity(intent_calendar);
                }
                else {

                    if(IntermediarioController.obtenerIntermediario() == null){
                        startActivity(new Intent(LauncherActivity.this, InicioSesionActivity.class));
                    }else{
                        if(CitaReunionController.hayAlgunaCita()){
                            startActivity(new Intent(LauncherActivity.this, CalendarioActivity.class));
                        }else{
                            startActivity(new Intent(LauncherActivity.this, OnboardingActivity.class));
                        }
                    }
                }
                LauncherActivity.this.finish();
            }
        }, 1000);
    }
}