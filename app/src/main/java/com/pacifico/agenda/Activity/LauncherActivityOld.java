package com.pacifico.agenda.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;

import java.util.Timer;
import java.util.TimerTask;

public class LauncherActivityOld extends AppCompatActivity {
    boolean usuarioInicioSesion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        SharedPreferences sharedPref = this.getSharedPreferences("com.pacifico.agenda", MODE_PRIVATE);
        usuarioInicioSesion = sharedPref.getBoolean("usuarioInicioSesion", false);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run(){

                int tipo_operacion = -1;
                try{
                    Intent intent = getIntent();
                    tipo_operacion = intent.getExtras().getInt(Constantes.tipo_operacion);
                }catch(Exception e ){}

                if (tipo_operacion == Constantes.operacion_ADNTerminaryAgendar)
                {
                    Intent intent = getIntent();
                    int idprospecto = -1;
                    try {
                        idprospecto = intent.getExtras().getInt(Constantes.parametro_idProspecto);
                    }catch(Exception e){}

                    Intent intent_calendar = new Intent(LauncherActivityOld.this, CalendarioActivity.class);
                    intent_calendar.putExtra(Constantes.tipo_operacion,Constantes.operacion_ADNTerminaryAgendar);
                    intent_calendar.putExtra(Constantes.parametro_idProspecto,idprospecto);
                    startActivity(intent_calendar);
                }
                else {
                    if (usuarioInicioSesion) {
                        startActivity(new Intent(LauncherActivityOld.this, CalendarioActivity.class));
                    } else {
                        startActivity(new Intent(LauncherActivityOld.this, InicioSesionActivity.class));
                    }
                }
            }
        }, 1000);
    }
}