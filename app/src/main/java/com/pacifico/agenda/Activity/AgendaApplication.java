package com.pacifico.agenda.Activity;

import android.content.Context;

import com.dsbmobile.dsbframework.DSBApplication;
import com.pacifico.agenda.Model.Controller.AjustesController;
import com.pacifico.agenda.Model.Controller.TablaIdentificadorController;
import com.pacifico.agenda.Util.FontsOverride;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateDatabaseAdapter;

/**
 * Created by DSBMobile-AAE14-001 on 13/05/2016.
 */
public class AgendaApplication extends DSBApplication {
    @Override
    public void onCreate(){
        super.onCreate();
        // Creacion de Base de Datos
        Context context = getApplicationContext();
        String a = "5";
        //DatabaseConstants.DATABASE_PATH = context.getExternalFilesDir(null).toString();
        //DatabaseConstants.DATABASE_DIRECTORY = "PACIFICO_ORGANIZATE_AGENDA";
        DatabaseConstants.DATABASE_DIRECTORY = "PACIFICO_ORGANIZATE";

        OrganizateDatabaseAdapter.open(context,/* DatabaseConstants.DATABASE_PATH
                        + "/" + DatabaseConstants.DATABASE_DIRECTORY
                        + "/" +*/ DatabaseConstants.DATABASE_NAME,
                DatabaseConstants.DATABASE_VERSION);

        // Inicialización de Tabla de Identitys
        TablaIdentificadorController.inicializarTabla();
        AjustesController.inicializarTabla();

        //Creacion Fonts
        FontsOverride.setDefaultFont(this, "DEFAULT", "webfont.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "webfont.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "webfont.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "webfont.ttf");
    }
}