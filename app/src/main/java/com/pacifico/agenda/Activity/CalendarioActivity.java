package com.pacifico.agenda.Activity;

//import android.app.Fragment;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.pacifico.agenda.Fragments.EntrevistaFragment;
import com.pacifico.agenda.Fragments.RecordatorioIndependienteFragment;
import com.pacifico.agenda.Fragments.ReunionFragment;
import com.pacifico.agenda.Model.Bean.AdnBean;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.agenda.Model.Bean.EventoBean;
import com.pacifico.agenda.Model.Bean.FamiliarBean;
import com.pacifico.agenda.Model.Bean.GeneralBean;
import com.pacifico.agenda.Model.Bean.ParametroBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoDatosReferidoBean;
import com.pacifico.agenda.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Model.Bean.ReferidoBean;
import com.pacifico.agenda.Model.Bean.ReunionInternaBean;
import com.pacifico.agenda.Model.Bean.TablaTablasBean;
import com.pacifico.agenda.Model.Controller.ADNController;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.Model.Controller.FamiliarController;
import com.pacifico.agenda.Model.Controller.ParametroController;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.agenda.Model.Controller.ReferidoController;
import com.pacifico.agenda.Model.Controller.TablasGeneralesController;
import com.pacifico.agenda.Network.RespuestaSincronizacionListener;
import com.pacifico.agenda.Network.SincronizacionController;
import com.pacifico.agenda.R;

import com.github.clans.fab.FloatingActionMenu;
import com.pacifico.agenda.Util.CalendarView;
import com.pacifico.agenda.Util.CeldaBirthday;
import com.pacifico.agenda.Util.CeldaDia;
import com.pacifico.agenda.Util.CeldaRecordatorio;
import com.pacifico.agenda.Fragments.BuscarProspectoFragment;
import com.pacifico.agenda.Fragments.ProspectosFragment;
import com.pacifico.agenda.Util.CeldaView;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.LineaHoraCalendario;
import com.pacifico.agenda.Util.Util;
import com.pacifico.agenda.Views.CustomView.setFuente;
import com.pacifico.agenda.Views.Dialogs.DialogCumpleanos;
import com.pacifico.agenda.Views.Dialogs.DialogGeneral;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


public class CalendarioActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //////////////// VARIABLES ACTUALES   /////////////

    // Variables Entrevista
    public ProspectoBean prospectoActual;
    LinearLayout linearLayout;

    public int citaSoloLectura = Constantes.FLAG_FALSO; // TODO: MEJOR QUITAR Y USAR LOGICA CON ESTADO DE REALIZADA
    //public int etapaActual = -1; // 1: primera cita , 2:segunda cita , 3 cita adicional
    public CitaBean citaActual; //Entrevista Actual creada en el proceso
    public CitaBean citaOriginal = null;
    public boolean citaModificoProspecto = false;
    public RecordatorioLlamadaBean recordatorioLLamadaDirectoBean = null;
    public ReunionInternaBean reunionActual;
    public int etapaFab = -1;

    public ProspectoDatosReferidoBean prospectoDatosRefFiltro= null; // Filtro Referidos
    public String textoBuscadoProspecto_temp;
    public ProspectoBean prospectoCreacionTemp;

    //////////////////////////////////////////////////

    CoordinatorLayout  coordinatorLayoutMain;
    LinearLayout linCalendarioSeleccion;
    Button btnMostrarCalendario;
    //LinearLayout linMostrarCalendario;
    FrameLayout fraFormularioFlotante;
    private FloatingActionMenu famAcciones;
    TextView txtHora9am;
    GridLayout gridCabeceraCalendario;

    private com.github.clans.fab.FloatingActionButton fabPrimeraEntrevista;
    private com.github.clans.fab.FloatingActionButton fabSegundaEntrevista;
    private com.github.clans.fab.FloatingActionButton fabEntrevistaAdicional;
    private com.github.clans.fab.FloatingActionButton fabRecordatorioLLamada;
    private com.github.clans.fab.FloatingActionButton fabReunionInterna;

    private MaterialCalendarView materialCalendarView;
    private CalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Probando el LinearLayout
        //GridLayout rlTop = (LinearLayoutTouchListener) findViewById(R.id.rlTop);
        int COLUMN = 8;
        int ROW = 24;
        int ROW_VIEW = 8;
        int INICIO_DIA = 14;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Funcion mostrar Calendar:
        //linMostrarCalendario = (LinearLayout)findViewById(R.id.linMostrarCalendario);
        coordinatorLayoutMain = (CoordinatorLayout) findViewById(R.id.cordinatorLayoutMain);
        btnMostrarCalendario = (Button) findViewById(R.id.btnMostrarCalendario);
        linCalendarioSeleccion = (LinearLayout) findViewById(R.id.linCalendarioSeleccion);
        fraFormularioFlotante = (FrameLayout) findViewById(R.id.fraFormularioFlotante);
        calendarView = new CalendarView(this, COLUMN, ROW, ROW_VIEW);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Se agrego esto para que no se habiliten los click de las vistas debajo del layout
        fraFormularioFlotante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        //linMostrarCalendario.setOnClickListener(new View.OnClickListener() {
        btnMostrarCalendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //linCalendar.setVisibility(View.GONE);
                //linCalendar.animate().alpha(0.0f);

                if (linCalendarioSeleccion.getVisibility() == View.GONE) {
                    linCalendarioSeleccion.setVisibility(View.VISIBLE);
                } else {
                    linCalendarioSeleccion.setVisibility(View.GONE);
                }

                // TODO: AFINAR ANIMACION Y REEMPLAZAR AL SIMPLE
                /*if (linCalendar.getVisibility() == View.GONE) {
                    linCalendar.animate()
                            .translationX(linCalendar.getWidth()).alpha(1.0f)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                    super.onAnimationStart(animation);
                                    linCalendar.setVisibility(View.VISIBLE);
                                    linCalendar.setAlpha(0.0f);
                                }
                            });
                } else {
                    linCalendar.animate()
                            .translationX(0).alpha(0.0f)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    linCalendar.setVisibility(View.GONE);
                                }
                            });
                }*/
            }
        });

        materialCalendarView = (MaterialCalendarView) findViewById(R.id.materialCalendarView);
        materialCalendarView.setOnDateChangedListener(myOndateSelectedListener);
        materialCalendarView.setFirstDayOfWeek(2);

        //Funciones Botones

        famAcciones = (FloatingActionMenu) findViewById(R.id.famAcciones);
        fabPrimeraEntrevista = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fabPrimeraEntrevista);
        fabPrimeraEntrevista.setOnClickListener(clickListener);

        fabSegundaEntrevista = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fabSegundaEntrevista);
        fabSegundaEntrevista.setOnClickListener(clickListener);

        fabEntrevistaAdicional = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fabEntrevistaAdicional);
        fabEntrevistaAdicional.setOnClickListener(clickListener);

        fabRecordatorioLLamada = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_recordatorio_llamada);
        fabRecordatorioLLamada.setOnClickListener(clickListener);

        fabReunionInterna = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_reunion_interna);
        fabReunionInterna.setOnClickListener(clickListener);

        calendarView.addListener(new GridLayoutTouchListener(this, calendarView));

        ////// Carga el calendario con la semana actual
        actualizarCalendario(); // Actualiza calendario con la semana sele
        // Obteniendo mes actual
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH);
        btnMostrarCalendario.setText(Constantes.ObtenerTextoMes(month)+" "+Calendar.getInstance().get(Calendar.YEAR));
        ////////////////////////////////
         linearLayout = ((LinearLayout) findViewById(R.id.ll_calendar));
        calendarView.attach(linearLayout);
        ViewTreeObserver vto = linearLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // Put your code here.
                ScrollView scrollCalendar = ((ScrollView) findViewById(R.id.scrollData));
                scrollCalendar.scrollTo(0,800);

                linearLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });


        /// Se carga por default la tarjeta de Prospectos
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fraFormularioFlotante, ProspectosFragment.newInstance()); // newInstance() is a static factory method.
        //transaction.addToBackStack(null);
        transaction.commit();
        /////////////////////////

        fraFormularioFlotante.setVisibility(View.VISIBLE);
        fraFormularioFlotante.bringToFront();

        validarOperacionesExternas();
    }

    @Override
    protected void onResume() {
        super.onResume();

        verificarTiempoSinSincronizar();

    }

    @Override
    public void onBackPressed() {

        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getFragmentManager().getBackStackEntryCount() == 0 && fraFormularioFlotante.getVisibility() == View.VISIBLE) {
            fraFormularioFlotante.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if (getFragmentManager().getBackStackEntryCount() == 0 && fraFormularioFlotante.getVisibility() == View.VISIBLE) {
            // Hacer Nada
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_prospectos) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fraFormularioFlotante, ProspectosFragment.newInstance()); // newInstance() is a static factory method.
            //transaction.addToBackStack(null);
            transaction.commit();

            fraFormularioFlotante.setVisibility(View.VISIBLE);
            fraFormularioFlotante.bringToFront();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Log.i("TAG","ajustes"+item+id);
        if(id== R.id.nav_agenda){
            //Intent intent = new Intent(this,CalendarioActivity.class);
            //startActivity(intent);

        }


        if (id == R.id.nav_ajuste) {
            Intent intent = new Intent(CalendarioActivity.this,AjustesActivity.class);
            startActivity(intent);
            finish();
        }
        if(id==R.id.nav_salir){


            finishAffinity();
        }


        /*else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_share) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    OnDateSelectedListener myOndateSelectedListener = new OnDateSelectedListener() {
        @Override
        public void onDateSelected(MaterialCalendarView materialCalendarView, CalendarDay calendarDay, boolean b) {
          int day=calendarDay.getDay(),  month = calendarDay.getMonth(), year = calendarDay.getYear();
            if (calendarView!=null)
                calendarView.setWeek(day,month,year);

            btnMostrarCalendario.setText(Constantes.ObtenerTextoMes(month)+" "+Calendar.getInstance().get(Calendar.YEAR));

            actualizarCalendario();

        }
    };

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            switch (v.getId()) {
                case R.id.fabPrimeraEntrevista:
                    etapaFab = Constantes.EtapaProspecto_PrimeraEntrevista;
                    transaction.replace(R.id.fraFormularioFlotante, BuscarProspectoFragment.newInstance());
                    transaction.commit();
                    break;
                case R.id.fabSegundaEntrevista:
                    etapaFab = Constantes.EtapaProspecto_SegundaEntrevista;
                    transaction.replace(R.id.fraFormularioFlotante, BuscarProspectoFragment.newInstance());
                    transaction.commit();
                    break;
                case R.id.fabEntrevistaAdicional:
                    etapaFab = Constantes.EtapaProspecto_EntrevistaAdicional;
                    transaction.replace(R.id.fraFormularioFlotante, BuscarProspectoFragment.newInstance());
                    transaction.commit();
                    break;
                case R.id.fab_recordatorio_llamada:
                    transaction.replace(R.id.fraFormularioFlotante, BuscarProspectoFragment.newInstance());
                    transaction.commit();
                    break;
                case R.id.fab_reunion_interna:
                    reunionActual = new ReunionInternaBean();
                    transaction.replace(R.id.fraFormularioFlotante, ReunionFragment.newInstance());
                    transaction.commit();
                    break;
            }

            fraFormularioFlotante.setVisibility(View.VISIBLE);
            fraFormularioFlotante.bringToFront();

            famAcciones.close(false);

        }
    };

    public void CerrarTarjetas() {

        fraFormularioFlotante.setVisibility(View.GONE);
        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.fraFormularioFlotante)).commit();

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    public void OcultarTeclado() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    // ONLINEAR TRUE
    public class GridLayoutTouchListener implements View.OnTouchListener {

        static final String logTag = "ActivitySwipeDetector";
        private Activity activity;
        static final int MIN_DISTANCE = 100;
        private float downX, downY, upX, upY;
        private CalendarView calendarView;


        //public GridLayoutTouchListener(MainActivity mainActivity,CalendarView calendarView) {
        public GridLayoutTouchListener(CalendarioActivity mainActivity,CalendarView calendarView) {
            activity = mainActivity;
            this.calendarView = calendarView;
        }

        public void onRightToLeftSwipe() {
            calendarView.nextWeek();
            actualizarCalendario();
        }

        public void onLeftToRightSwipe() {
            calendarView.beforeWeek();
            actualizarCalendario();
        }

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    downX = event.getX();
                    downY = event.getY();
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    upX = event.getX();
                    upY = event.getY();

                    float deltaX = downX - upX;
                    float deltaY = downY - upY;

                    // swipe horizontal?
                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        // left or right
                        if (deltaX < 0) {
                            this.onLeftToRightSwipe();
                            //return true;
                            return false;
                        }
                        if (deltaX > 0) {
                            this.onRightToLeftSwipe();
                            //return true;
                            return false;
                        }
                    }
                }
            }
            return false;
        }
    }


    public void actualizarCalendario() {
        /// Listar Cumpleaños
        //calendar.limpiarCumples();


        String Cadena = "" + calendarView.getCalendar();
        Log.i("TAG", "Dias" + Cadena.substring(5, 7));
           String mesSeleccionado = Cadena.substring(5, 7);
           String anioSeleccionado = Cadena.substring(0,4);

        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH);

        switch (mesSeleccionado) {

            case "01": {
                btnMostrarCalendario.setText("Enero " + anioSeleccionado);
                break;
            }
            case "02": {
                btnMostrarCalendario.setText("Febrero " + anioSeleccionado);
                break;
            }
            case "03": {
                btnMostrarCalendario.setText("Marzo " + anioSeleccionado);
                break;
            }
            case "04": {
                btnMostrarCalendario.setText("Abril " + anioSeleccionado);
                break;
            }
            case "05": {
                btnMostrarCalendario.setText("Mayo " + anioSeleccionado);
                break;
            }
            case "06": {
                btnMostrarCalendario.setText("Junio " + anioSeleccionado);
                break;
            }
            case "07": {
                btnMostrarCalendario.setText("Julio " + anioSeleccionado);
                break;
            }
            case "08": {
                btnMostrarCalendario.setText("Agosto " + anioSeleccionado);
                break;
            }
            case "09": {
                btnMostrarCalendario.setText("Setiembre " + anioSeleccionado);
                break;
            }
            case "10": {
                btnMostrarCalendario.setText("Octubre " + anioSeleccionado);
                break;
            }
            case "11": {
                btnMostrarCalendario.setText("Noviembre " + anioSeleccionado);
                break;
            }
            case "12": {
                btnMostrarCalendario.setText("Diciembre " + anioSeleccionado);
                break;
            }
        }

        /////// Listar Calendarios
        DateTimeFormatter fmt = DateTimeFormat.forPattern(Constantes.formato_solofecha_envio);
        String fechaInicio = fmt.print(calendarView.getCalendar());
        String fechaFin = fmt.print(calendarView.getCalendar().plus(Period.days(6)));


        /// Listar Cumpleaños
        calendarView.limpiarCumples();


        ArrayList<ProspectoBean> listaProspectoBean = ProspectoController.getCumpleanosDeLaSemana(fechaInicio, fechaFin);
        if (listaProspectoBean != null) {

            LinkedHashMap<String, ArrayList<ProspectoBean>> linkHashProspectos = convertirALinkedHash(listaProspectoBean);


            for (Map.Entry<String, ArrayList<ProspectoBean>> entry : linkHashProspectos.entrySet()) {
                String fechaNacimiento = "";

                final ArrayList<ProspectoBean> listaAgrupada =  entry.getValue();

                if (listaAgrupada.size() == 0)
                    continue;

                CeldaBirthday celdaBirthday = new CeldaBirthday(this);
                String texto = "";

                if (listaAgrupada.size() == 1) {
                    fechaNacimiento = listaAgrupada.get(0).getFechaNacimiento();
                    ProspectoBean prospectoBean = listaAgrupada.get(0);
                    texto = Util.capitalizedString(prospectoBean.getNombres()) + " " +
                            Util.capitalizedString(prospectoBean.getApellidoPaterno());
                } else {
                    fechaNacimiento = listaAgrupada.get(0).getFechaNacimiento();
                    texto = listaAgrupada.size() + " cumpleaños";
                }

                celdaBirthday.txt.setText(texto);

                Calendar calFecha = Calendar.getInstance();
                SimpleDateFormat sdf_fec = new SimpleDateFormat(Constantes.formato_solofecha_envio);
                try {
                    Date dateFechaNacimiento = sdf_fec.parse(fechaNacimiento);
                    Calendar calFechaNacimiento = Calendar.getInstance();
                    calFechaNacimiento.setTime(dateFechaNacimiento);
                    int mesNacimiento = calFechaNacimiento.get(Calendar.MONTH);
                    int diaNacimiento = calFechaNacimiento.get(Calendar.DAY_OF_MONTH);
                    int anioNacimiento = calFechaNacimiento.get(Calendar.YEAR);


                    int diffYears = Util.getDiffYears(dateFechaNacimiento, calFecha.getTime());

                    int anio  =anioNacimiento + diffYears;


                    calFecha.set(anio, mesNacimiento, diaNacimiento);


                } catch (ParseException e) {
                    e.printStackTrace();
                }

                int diaSemana = calFecha.get(Calendar.DAY_OF_WEEK) - 1;
                if (diaSemana == 0) diaSemana = 7;


                celdaBirthday.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fm = getSupportFragmentManager();
                        DialogCumpleanos dialogCumpleanos = new DialogCumpleanos();

                        ArrayList<GeneralBean> listaGeneral = convertirAListaGeneralCumpleanios(listaAgrupada);

                        dialogCumpleanos.setDatos("Cumpleaños", listaGeneral);
                        dialogCumpleanos.setCancelable(false);
                        dialogCumpleanos.show(fm, "dialogoCumpleanios");
                    }
                });

                calendarView.addBirthday(celdaBirthday, diaSemana);

            }

        }
        //////////////////////////////////
        calendarView.limpiarCitas();

        /// Agregar Linea de Tiempo
        Calendar calhoraactual = Calendar.getInstance();

        calendarView.borrarLineaHora();
        LineaHoraCalendario lineaHoraCalendario = new LineaHoraCalendario(this);
        int horaActual= calhoraactual.get(Calendar.HOUR_OF_DAY);
        int minutoActual= calhoraactual.get(Calendar.MINUTE);
        calendarView.addLineaHora(lineaHoraCalendario,horaActual,minutoActual);
        ////

        ArrayList<EventoBean> listaEventosSemana = CitaReunionController.obtenerEventosSemana(fechaInicio, fechaFin);

        // Cargando las citas
        if (listaEventosSemana != null)
        {
            int menorHoraInicio=-1;
            int menorMinutosInicio=-1;
            int mayorHoraFin=-1;
            int mayorMinutoFin=-1;
            int diaAgrupado = -1;
            int diaSemanaactual = -1;

            double alturaMinuto = calendarView.altoceldacalendario / 60.0;

            LinearLayout agrupadorEventos= null;

            for (int i = 0; i < listaEventosSemana.size(); i++) {
                try {
                    final EventoBean eventoBean = listaEventosSemana.get(i);

                    CeldaView celda = null;

                    if (eventoBean.getTipoEvento() == Constantes.TIPO_EVENTO_CITA) {
                        if (eventoBean.getEtapaCita() == Constantes.EtapaProspecto_PrimeraEntrevista)
                            celda = new CeldaDia(this, Color.parseColor("#F0FAF3"), Color.parseColor("#00AF3F"));
                        else if (eventoBean.getEtapaCita() == Constantes.EtapaProspecto_SegundaEntrevista)
                            celda = new CeldaDia(this, Color.parseColor("#FEF4F7"), Color.parseColor("#E91E63"));
                        else if (eventoBean.getEtapaCita() == Constantes.EtapaProspecto_EntrevistaAdicional)
                            celda = new CeldaDia(this, Color.parseColor("#F7F4FC"), Color.parseColor("#5C2DC5"));
                        else // AGregado solo para pruebas, para ver si alguna cita se gaurdo mal pintarlo de negro
                            celda = new CeldaDia(this, Color.parseColor("#000000"), Color.parseColor("#ffffff"));

                        ((CeldaDia)celda).txt.setText(Util.capitalizedString(eventoBean.getNombres())
                                                        + " " + Util.capitalizedString(eventoBean.getApellidoPaterno()));

                        celda.linHorizontal.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                ProspectoBean prospectoEvento = ProspectoController.getProspectoBeanByIdProspectoDispositivo(eventoBean.getIdProspectoDispositivo());
                                CitaBean ultimaCita = CitaReunionController.obtenerUltimaCitaProspecto(prospectoEvento);

                                // Si la cita es una cita actual no realizada
                                if (ultimaCita.getIdCitaDispositivo() == eventoBean.getIdEventoDispositivo() && ultimaCita.getCodigoEstado() != Constantes.EstadoCita_Realizada) {
                                    GestionFlujoProspecto(prospectoEvento);
                                } else {
                                    // Se carga Cita antigua
                                    reiniciarParametros();
                                    prospectoActual =prospectoEvento;
                                    citaActual = CitaReunionController.obtenerCitaPorIdCitaDispositivo(eventoBean.getIdEventoDispositivo());
                                    citaSoloLectura = Constantes.FLAG_VERDADERO;

                                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                                    transaction.replace(R.id.fraFormularioFlotante, EntrevistaFragment.newInstance()); // newInstance() is a static factory method.
                                    //transaction.addToBackStack(null);
                                    transaction.commit();

                                    fraFormularioFlotante.setVisibility(View.VISIBLE);
                                    fraFormularioFlotante.bringToFront();
                                }

                            }
                        });
                    }else if (eventoBean.getTipoEvento() == Constantes.TIPO_EVENTO_REUNION){
                        celda = new CeldaDia(this, Color.parseColor("#F6F6F6"), Color.parseColor("#4A4A4A"));

                        if (eventoBean.getCodigoTipoReunion() == Constantes.Reunion_Interna)
                            ((CeldaDia)celda).txt.setText("Reunión Interna");
                        else if (eventoBean.getCodigoTipoReunion() == Constantes.Reunion_Externa)
                            ((CeldaDia)celda).txt.setText("Reunión Externa");

                        celda.view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                reunionActual = CitaReunionController.obtenerReunionInterPorIdReunionInternaDispositivo(eventoBean.getIdEventoDispositivo());

                                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                                transaction.replace(R.id.fraFormularioFlotante, ReunionFragment.newInstance());

                                transaction.commit();

                                fraFormularioFlotante.setVisibility(View.VISIBLE);
                                fraFormularioFlotante.bringToFront();
                            }
                        });
                    }else if (eventoBean.getTipoEvento() == Constantes.TIPO_EVENTO_RECORDATORIOLLAMADA)
                    {
                        celda = new CeldaRecordatorio(this);
                        ((CeldaRecordatorio)celda).image.setImageDrawable(getResources().getDrawable(R.drawable.llamada_celeste));
                        ((CeldaRecordatorio)celda).txt.setText(Util.capitalizedString(eventoBean.getNombres())
                                                                + " " + Util.capitalizedString(eventoBean.getApellidoPaterno()));
                        ((CeldaRecordatorio)celda).txt.setTextColor(getResources().getColor(R.color.colorPrimary));

                        celda.view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                FragmentManager fm = getSupportFragmentManager();
                                DialogCumpleanos dialogLLamarContacto = new DialogCumpleanos();

                                ArrayList<GeneralBean> listarec = new ArrayList<GeneralBean>();
                                GeneralBean genRecLlamada = new GeneralBean();

                                String nombreCompleto = (Util.capitalizedString(eventoBean.getNombres())
                                                        + " " + Util.capitalizedString(eventoBean.getApellidoPaterno())
                                                        + " " + Util.capitalizedString(eventoBean.getApellidoMaterno())).trim();

                                genRecLlamada.setCampo1(nombreCompleto);

                                // Se puede optimizar en el mismo query
                                ProspectoBean prospectoEvento = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(eventoBean.getIdProspectoDispositivo());
                                ProspectoBean referente = null;
                                if (prospectoEvento != null && prospectoEvento.getIdReferenciadorDispositivo() != -1)
                                    referente = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(prospectoEvento.getIdReferenciadorDispositivo());
                                if (referente != null)
                                {
                                    String nombreCompletoReferente = (Util.capitalizedString(referente.getNombres())
                                            + " " + Util.capitalizedString(referente.getApellidoPaterno())
                                            + " " + Util.capitalizedString(referente.getApellidoMaterno())).trim();

                                    genRecLlamada.setCampo2("Ref. "+nombreCompletoReferente);
                                }else{
                                    TablaTablasBean fuente = TablasGeneralesController.ObtenerItemTablaTablas(7,prospectoEvento.getCodigoFuente());
                                    if (fuente != null)
                                        genRecLlamada.setCampo2(fuente.getValorCadena());
                                }

                                if  (prospectoEvento.getTelefonoCelular() != null && !"".equals(prospectoEvento.getTelefonoCelular()))
                                    genRecLlamada.setCampo3(prospectoEvento.getTelefonoCelular());
                                else
                                {
                                    if (prospectoEvento.getTelefonoFijo() != null)
                                        genRecLlamada.setCampo3(prospectoEvento.getTelefonoFijo());
                                }

                                listarec.add(genRecLlamada);

                                dialogLLamarContacto.setDatos("Recordatorio de llamada", listarec);
                                dialogLLamarContacto.setCancelable(false);
                                dialogLLamarContacto.show(fm, "dialogo1");
                            }
                        });
                    }

                    if (celda == null)
                        break;

                    // Obtener Fecha
                    Calendar calFecha = Calendar.getInstance();
                    SimpleDateFormat sdf_fec = new SimpleDateFormat(Constantes.formato_solofecha_envio);
                    calFecha.setTime(sdf_fec.parse(eventoBean.getFecha()));
                    diaSemanaactual = calFecha.get(Calendar.DAY_OF_WEEK) - 1;
                    if (diaSemanaactual == 0) diaSemanaactual = 7;

                    // Obtener Hora de Inicio
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    cal.setTime(sdf.parse(eventoBean.getHoraInicio()));

                    int horasInicio = cal.get(Calendar.HOUR_OF_DAY);
                    int minutosInicio = cal.get(Calendar.MINUTE);

                    int horasFin;
                    int minutosFin;

                    if (eventoBean.getTipoEvento() == Constantes.TIPO_EVENTO_RECORDATORIOLLAMADA)
                    {
                        int temp_minutos_fin = (horasInicio*60 + minutosInicio) +30;// Se le da media hora de espacio a la llamada (mas como tema visual)
                        horasFin = temp_minutos_fin/60;
                        minutosFin = temp_minutos_fin - (horasFin*60);
                    }else // Si es cita o recordatorio
                    {
                        // Obtener Hora de Fin
                        cal.setTime(sdf.parse(eventoBean.getHoraFin()));
                        horasFin = cal.get(Calendar.HOUR_OF_DAY);
                        minutosFin = cal.get(Calendar.MINUTE);
                    }

                    //////////////////////////////////////
                    if (diaAgrupado != diaSemanaactual)  // SI EL DIA CAMBIA (O RECIEN SE INICIA)
                    {
                        // Si hay un Linear agregarlo
                        if (agrupadorEventos != null) {
                            int altura = (int)Math.round((mayorHoraFin - menorHoraInicio)*60.0*alturaMinuto + mayorMinutoFin*alturaMinuto);
                            calendarView.addCell(agrupadorEventos, diaAgrupado, menorHoraInicio, altura);
                        }

                        // Se setan los datos del agrupador
                        diaAgrupado = diaSemanaactual;
                        menorHoraInicio = horasInicio;
                        menorMinutosInicio = minutosInicio;
                        mayorHoraFin = horasFin;
                        mayorMinutoFin = minutosFin;

                        //Se crea el nuevo agrupador
                        agrupadorEventos = new LinearLayout(this);

                        agrupadorEventos.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                        agrupadorEventos.setOrientation(LinearLayout.HORIZONTAL);

                        // Se setea el space before de la cita
                        celda.spaceBefore.getLayoutParams().height = (int)Math.round(alturaMinuto * menorMinutosInicio);
                        int alt_evento = (int)Math.round((horasFin - menorHoraInicio)*60.0*alturaMinuto + minutosFin*alturaMinuto);
                        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,alt_evento,1);
                        celda.view.setLayoutParams(param);

                        agrupadorEventos.addView(celda.view);

                    }else{ // SI ES EL MISMO DIA
                        int ini= menorHoraInicio*60 + menorMinutosInicio;
                        int fin = mayorHoraFin*60+ mayorMinutoFin;

                        int minCitaActual = horasInicio*60+minutosInicio;

                        if (minCitaActual >= ini && minCitaActual < fin) // SI LA CITA INICIA ENTRE EL ACTUAL INICIO Y FIN
                        {
                            int minutos_antes_evento = (horasInicio - menorHoraInicio)* 60 +  minutosInicio; // Minutos desde la hora inicio del agrupador hasta el inicio de la cita

                            // Se setea el space before de la cita
                            celda.spaceBefore.getLayoutParams().height = (int)Math.round(alturaMinuto * minutos_antes_evento);
                            int alt_evento = (int)Math.round((horasFin - menorHoraInicio)*60.0*alturaMinuto + minutosFin*alturaMinuto);
                            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,alt_evento,1);
                            celda.view.setLayoutParams(param);
                            agrupadorEventos.addView(celda.view);

                            if (horasFin*60 + minutosFin > mayorHoraFin*60+mayorMinutoFin)
                            {
                                mayorHoraFin = horasFin;
                                mayorMinutoFin = minutosFin;
                            }

                        }else // SI LA CITA ESTA FUERA DEL AGRUPADOR ACTUAL
                        {
                            //SE AGREGA EL AGRUPADOR ACTUAL
                            int altura = (int)Math.round((mayorHoraFin - menorHoraInicio)*60.0*alturaMinuto + mayorMinutoFin*alturaMinuto);
                            calendarView.addCell(agrupadorEventos,diaAgrupado,menorHoraInicio,altura);

                            //crear linear, setear nuevos min max, setear cita y agregar la cita
                            agrupadorEventos = new LinearLayout(this);
                            agrupadorEventos.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                            agrupadorEventos.setOrientation(LinearLayout.HORIZONTAL);

                            menorHoraInicio =  horasInicio;
                            menorMinutosInicio  = minutosInicio;
                            mayorHoraFin =  horasFin;
                            mayorMinutoFin  = minutosFin;

                            int minutos_antes_evento = (horasInicio - menorHoraInicio)* 60 +  minutosInicio; // Minutos desde la hora inicio del agrupador hasta el inicio de la cita

                            celda.spaceBefore.getLayoutParams().height = (int)Math.round(alturaMinuto * minutos_antes_evento);
                            int alt_cita = (int)Math.round((horasFin - menorHoraInicio)*60.0*alturaMinuto + minutosFin*alturaMinuto);
                            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,alt_cita,1);
                            celda.view.setLayoutParams(param);
                            agrupadorEventos.addView(celda.view);

                        }
                    }
                    /////////////////////////////////////

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (agrupadorEventos != null) {
                int altura = (int)Math.round((mayorHoraFin - menorHoraInicio)*60.0*alturaMinuto + mayorMinutoFin*alturaMinuto);
                calendarView.addCell(agrupadorEventos, diaAgrupado, menorHoraInicio, altura);
            }
        }

    }

    private ArrayList<GeneralBean> convertirAListaGeneralCumpleanios(ArrayList<ProspectoBean> listaAgrupada) {

        GeneralBean generalBean;
        ArrayList<GeneralBean> listarec = new ArrayList<>();

        for (ProspectoBean prospectoBean : listaAgrupada) {
            generalBean = new GeneralBean();
            String nombreCompleto = (Util.capitalizedString(prospectoBean.getNombres())
                    + " " + Util.capitalizedString(prospectoBean.getApellidoPaterno())
                    + " " + Util.capitalizedString(prospectoBean.getApellidoMaterno())).trim();

            generalBean.setCampo1(nombreCompleto);

            // Se puede optimizar en el mismo query
            ProspectoBean prospectoEvento = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
            ProspectoBean referente = null;
            if (prospectoEvento != null && prospectoEvento.getIdReferenciadorDispositivo() != -1)
                referente = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(prospectoEvento.getIdReferenciadorDispositivo());
            if (referente != null)
            {
                String nombreCompletoReferente = (Util.capitalizedString(referente.getNombres())
                        + " " + Util.capitalizedString(referente.getApellidoPaterno())
                        + " " +Util.capitalizedString(referente.getApellidoMaterno())).trim();

                generalBean.setCampo2("Ref. "+nombreCompletoReferente);
            }else{
                TablaTablasBean fuente = TablasGeneralesController.ObtenerItemTablaTablas(7,prospectoEvento.getCodigoFuente());
                if (fuente != null) {
                    generalBean.setCampo2(fuente.getValorCadena());
                }
                else
                {
                    generalBean.setCampo2("");
                }
            }

            if  (prospectoEvento.getTelefonoCelular() != null && !"".equals(prospectoEvento.getTelefonoCelular()))
                generalBean.setCampo3(prospectoEvento.getTelefonoCelular());
            else
            {
                if (prospectoEvento.getTelefonoFijo() != null)
                    generalBean.setCampo3(prospectoEvento.getTelefonoFijo());
            }

            listarec.add(generalBean);


        }

        return listarec;


    }

    private LinkedHashMap<String, ArrayList<ProspectoBean>> convertirALinkedHash(ArrayList<ProspectoBean> listaProspectoBean) {

        LinkedHashMap<String, ArrayList<ProspectoBean>> listaAgrupada = new LinkedHashMap<>();
        ArrayList<ProspectoBean> listaInterior;


        String grupo;
        for (ProspectoBean bean : listaProspectoBean) {
            grupo = Util.quitarAnio(bean.getFechaNacimiento());

            listaInterior = listaAgrupada.get(grupo);

            if (listaInterior == null) {
                listaInterior = new ArrayList<>();
                listaInterior.add(bean);
                listaAgrupada.put(grupo, listaInterior);
            }
            else
            {
                listaInterior.add(bean);
            }

        }

        return listaAgrupada;
    }

    //// Gestion de Flujo Prospecto
    public void GestionFlujoProspecto(ProspectoBean pProspecto)
    {
        reiniciarParametros(); // SI se inicia el flujo de un prospecto primero reiniciamos parametros
        // Seteamos el Prospecto Actual
        prospectoActual = pProspecto;

        if (prospectoActual.getCodigoEstado() == Constantes.EstadoProspecto_NoInteresado ||
                prospectoActual.getCodigoEtapa() == Constantes.EtapaProspecto_Nuevo) {

            recordatorioLLamadaDirectoBean = CitaReunionController.obtenerRecordatorioLlamadaDirectoPorIdProspectoDispositivo(pProspecto.getIdProspectoDispositivo());

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Fragment _recordatorioIndFragment = RecordatorioIndependienteFragment.newInstance();

            transaction.replace(R.id.fraFormularioFlotante, _recordatorioIndFragment); // newInstance() is a static factory method.
            transaction.commit();

            fraFormularioFlotante.setVisibility(View.VISIBLE);
            fraFormularioFlotante.bringToFront();
        }else
        {
            // SI esta en proceso o cierre de venta carga la ultima cita
            GestionarCitaProspectoEnProceso();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Fragment _entrevistaFragment = EntrevistaFragment.newInstance();

            transaction.replace(R.id.fraFormularioFlotante, _entrevistaFragment); // newInstance() is a static factory method.
            transaction.commit();

            fraFormularioFlotante.setVisibility(View.VISIBLE);
            fraFormularioFlotante.bringToFront();
        }
    }

    ///// Gestion de Entrevistas de Prospectos en Proceso
    public void GestionarCitaProspectoEnProceso() {

        citaActual = CitaReunionController.obtenerUltimaCitaProspecto(prospectoActual);

        if (citaActual != null)
        {
            if (citaActual.getCodigoResultado() == Constantes.ResultadoCita_SiguienteEtapa || citaActual.getCodigoResultado() == Constantes.ResultadoCita_CierreVenta)
            {
                citaActual = new CitaBean();

                citaActual.setIdProspecto(prospectoActual.getIdProspecto());
                citaActual.setIdProspectoDispositivo(prospectoActual.getIdProspectoDispositivo());

                switch (prospectoActual.getCodigoEtapa())
                {
                    case Constantes.EtapaProspecto_PrimeraEntrevista:
                        citaActual.setCodigoEtapaProspecto(Constantes.EtapaProspecto_SegundaEntrevista);
                        break;
                    case Constantes.EtapaProspecto_SegundaEntrevista:
                        citaActual.setCodigoEtapaProspecto(Constantes.EtapaProspecto_EntrevistaAdicional);
                        break;
                    case Constantes.EtapaProspecto_EntrevistaAdicional:
                        citaActual.setCodigoEtapaProspecto(Constantes.EtapaProspecto_EntrevistaAdicional);
                        break;
                }
            }else
            {
                citaOriginal = CitaReunionController.obtenerUltimaCitaProspecto(prospectoActual);
            }
        }else { // Si no tiene cita quiere decir que le toca primera
            citaActual = new CitaBean();

            citaActual.setIdProspecto(prospectoActual.getIdProspecto());
            citaActual.setIdProspectoDispositivo(prospectoActual.getIdProspectoDispositivo());
            citaActual.setCodigoEtapaProspecto(Constantes.EtapaProspecto_PrimeraEntrevista);
        }
    }

    // Gestionar Flujo Fabs
    // ACA SOLO ENTRAN PROSPECTOS EN ESTADO NUEVO
    public void GestionFlujoEntrevistasDirectas(ProspectoBean pProspecto, int etapaEntrevista)
    {
        reiniciarParametros(); // SI se inicia el flujo de un prospecto primero reiniciamos parametros
        // Seteamos el Prospecto Actual
        prospectoActual = pProspecto;

        citaActual = new CitaBean();

        citaActual.setIdProspecto(prospectoActual.getIdProspecto());
        citaActual.setIdProspectoDispositivo(prospectoActual.getIdProspectoDispositivo());
        citaActual.setCodigoEtapaProspecto(etapaEntrevista);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment _entrevistaFragment = EntrevistaFragment.newInstance();

        transaction.replace(R.id.fraFormularioFlotante, _entrevistaFragment); // newInstance() is a static factory method.
        transaction.commit();
    }

    public void validarOperacionesExternas()
    {
        /// Ver si hay alguna operacion
        int tipo_operacion = -1;
        try {
            Intent intent = getIntent();
            tipo_operacion = intent.getExtras().getInt(Constantes.tipo_operacion);
        } catch (Exception e) {
        }

        if (tipo_operacion == Constantes.operacion_ADNTerminaryAgendar) {
            Intent intent = getIntent();
            int idprospecto_dispositivo = -1;

            try {
                idprospecto_dispositivo = intent.getExtras().getInt(Constantes.parametro_idProspecto);
            } catch (Exception e) {
            }

            if (idprospecto_dispositivo != -1) {

                ProspectoBean prospectoBean = ProspectoController.getProspectoBeanByIdProspectoDispositivo(idprospecto_dispositivo);//prospecto;

                //GestionarCita();
                GestionFlujoProspecto(prospectoBean);

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, EntrevistaFragment.newInstance()); // newInstance() is a static factory method.
                //transaction.addToBackStack(null);
                transaction.commit();

                fraFormularioFlotante.setVisibility(View.VISIBLE);
                fraFormularioFlotante.bringToFront();
            }
        }
        else if(tipo_operacion == Constantes.operacion_AbrirRecordatorioIndependiente)
        {
            Intent intent = getIntent();
            int idrecordatorioind_dispositivo = -1;

            try {
                idrecordatorioind_dispositivo = intent.getExtras().getInt(Constantes.parametro_idRecordatorioIndependiente);
            } catch (Exception e) {
            }

            if (idrecordatorioind_dispositivo != -1) {

                RecordatorioLlamadaBean recLlamadaBean = CitaReunionController.obtenerRecordatorioLlamadaPorIdRecordatorioLlamadaDispositivo(idrecordatorioind_dispositivo);
                ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(recLlamadaBean.getIdProspectoDispositivo());

                //Se abre la tarjeta del flujo
                GestionFlujoProspecto(prospectoBean);

                // Abriendo el dialog
                DialogCumpleanos dialogLLamarContacto = new DialogCumpleanos();

                ArrayList<GeneralBean> listarec = new ArrayList<GeneralBean>();
                GeneralBean genRecLlamada = new GeneralBean();

                String nombreCompleto = (Util.capitalizedString(prospectoBean.getNombres())
                        + " " + Util.capitalizedString(prospectoBean.getApellidoPaterno())
                        + " " +Util.capitalizedString(prospectoBean.getApellidoMaterno())).trim();

                genRecLlamada.setCampo1(nombreCompleto);

                ProspectoBean referente = null;
                if (prospectoBean != null && prospectoBean.getIdReferenciadorDispositivo() != -1)
                    referente = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(prospectoBean.getIdReferenciadorDispositivo());

                if (referente != null)
                {
                    String nombreCompletoReferente = (Util.capitalizedString(referente.getNombres())
                            + " " + Util.capitalizedString(referente.getApellidoPaterno())
                            + " " +Util.capitalizedString(referente.getApellidoMaterno())).trim();

                    genRecLlamada.setCampo2("Ref. "+nombreCompletoReferente);
                }else {
                    TablaTablasBean fuente = TablasGeneralesController.ObtenerItemTablaTablas(7,prospectoBean.getCodigoFuente());
                    genRecLlamada.setCampo2(fuente.getValorCadena());
                }


                if  (prospectoBean.getTelefonoCelular() != null && !"".equals(prospectoBean.getTelefonoCelular()))
                    genRecLlamada.setCampo3(prospectoBean.getTelefonoCelular());
                else
                {
                    if (prospectoBean.getTelefonoFijo() != null)
                        genRecLlamada.setCampo3(prospectoBean.getTelefonoFijo());
                }

                listarec.add(genRecLlamada);

                dialogLLamarContacto.setDatos("Recordatorio de llamada", listarec);
                FragmentManager fm = getSupportFragmentManager();
                dialogLLamarContacto.setCancelable(false);
                dialogLLamarContacto.show(fm, "dialogo1");
            }
        }
        else if(tipo_operacion == Constantes.operacion_AbrirAlertaCita)
        {
            Intent intent = getIntent();
            int idcita = -1;

            try {
                idcita = intent.getExtras().getInt(Constantes.parametro_idAlertaCita);
            } catch (Exception e) {
            }

            if (idcita != -1) {
                CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCitaDispositivo(idcita);
                ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(citaBean.getIdProspectoDispositivo());

                //Se abre la tarjeta del flujo
                GestionFlujoProspecto(prospectoBean);

                String nombreCompleto = (Util.capitalizedString(prospectoBean.getNombres())
                        + " " + Util.capitalizedString(prospectoBean.getApellidoPaterno())
                        + " " +Util.capitalizedString(prospectoBean.getApellidoMaterno())).trim();

                String pattern = "HH:mm";
                String text_horaInicio = "";
                try {
                    Date horaini = new SimpleDateFormat(pattern).parse(citaBean.getHoraInicio());

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");
                    text_horaInicio = simpleDateFormat.format(horaini);
                }catch (Exception e){}

                FragmentManager fm = getSupportFragmentManager();
                DialogGeneral dialogGeneral = new DialogGeneral();
                dialogGeneral.setListener(null);
                dialogGeneral.setDatos("Tienes una cita con: \n"+nombreCompleto +" a las " + text_horaInicio, "", getString(R.string.continuar));
                dialogGeneral.setCancelable(false);
                dialogGeneral.show(fm, "dialogo1");
            }
        }

        else if(tipo_operacion == Constantes.operacion_AbrirAlertaPostCita)
        {
            Intent intent = getIntent();
            int idcita = -1;

            try {
                idcita = intent.getExtras().getInt(Constantes.parametro_idAlertaCita);
            } catch (Exception e) {
            }

            if (idcita != -1) {
                final CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCitaDispositivo(idcita);

                if (citaBean.getCodigoResultado() == Constantes.ResultadoCita_Pendiente) {
                    final ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(citaBean.getIdProspectoDispositivo());

                    String nombreCompleto = (Util.capitalizedString(prospectoBean.getNombres())
                            + " " + Util.capitalizedString(prospectoBean.getApellidoPaterno())
                            + " " + Util.capitalizedString(prospectoBean.getApellidoMaterno())).trim();

                    if (citaBean.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista){
                        completarADNPostCita(prospectoBean);
                    }else {
                        LayoutInflater layoutInflater = LayoutInflater.from(this);
                        final View dialogPostCita = layoutInflater.inflate(R.layout.dialog_post_entrevista, null);
                        final AlertDialog.Builder builderPostCita = new AlertDialog.Builder(this);
                        builderPostCita.setView(dialogPostCita).setCancelable(false).setNegativeButton("", null)
                                .setPositiveButton("", null)
                        ;

                        final android.app.AlertDialog alertPostCita = builderPostCita.create();

                        TextView tvTitulo = (TextView) dialogPostCita.findViewById(R.id.tvTitulo);
                        Button btnCierreVentaPostEntrevista = (Button) dialogPostCita.findViewById(R.id.btnCierreVentaPostEntrevista);
                        Button btnAgendarPostEntrevista = (Button) dialogPostCita.findViewById(R.id.btnAgendarPostEntrevista);
                        Button btnReagendarPostEntrevista = (Button) dialogPostCita.findViewById(R.id.btnReagendarPostEntrevista);
                        Button btnCancelarPostEntrevista = (Button) dialogPostCita.findViewById(R.id.btnCancelarPostEntrevista);
                        Button btnDescartarPostEntrevista = (Button) dialogPostCita.findViewById(R.id.btnDescartarPostEntrevista);

                        if (CitaReunionController.validarCitaVentaRealizada(citaBean.getIdProspectoDispositivo()))
                        {
                            btnDescartarPostEntrevista.setVisibility(View.GONE);
                        }

                        tvTitulo.setText("¿Cómo te fue en la entrevista de " + nombreCompleto + "?");

                        btnCancelarPostEntrevista.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertPostCita.dismiss();
                            }
                        });

                        btnCierreVentaPostEntrevista.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertPostCita.dismiss();
                                cierreVentaPostCita(citaBean, prospectoBean);
                            }
                        });

                        btnAgendarPostEntrevista.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                alertPostCita.dismiss();
                                realizarCitaPostCita(citaBean, prospectoBean);
                            }
                        });

                        btnDescartarPostEntrevista.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertPostCita.dismiss();
                                descartarPostCita(citaBean, prospectoBean);
                            }
                        });

                        btnReagendarPostEntrevista.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertPostCita.dismiss();
                                GestionFlujoProspecto(prospectoBean);
                            }
                        });

                        alertPostCita.setCancelable(false);
                        alertPostCita.show();
                    }
                }else{
                    FragmentManager fm =  getSupportFragmentManager();
                    DialogGeneral dialogGeneral = new DialogGeneral();
                    dialogGeneral.setListener(null);
                    dialogGeneral.setDatos("La cita ya fue realizada", "", getString(R.string.continuar));
                    dialogGeneral.setCancelable(false);
                    dialogGeneral.show(fm, "dialogo1");
                }
            }
        }
    }

    public void descartarPostCita(final CitaBean citaBean, final ProspectoBean prospectoBean)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(CalendarioActivity.this);
        final View dialogConfirmacionDescartado = layoutInflater.inflate(R.layout.dialog_general, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(CalendarioActivity.this);

        TextView tvTitulo = (TextView) dialogConfirmacionDescartado.findViewById(R.id.tvTitulo);
        tvTitulo.setText("¿Seguro que desea descartar el prospecto?");

        builder.setView(dialogConfirmacionDescartado).setCancelable(false).setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String fechaActual = Util.obtenerFechaActual();

                // Se actualiza el prospecto
                prospectoBean.setCodigoEstado(Constantes.EstadoProspecto_NoInteresado);
                prospectoBean.setFechaModificacionDispositivo(fechaActual);
                prospectoBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
                ProspectoController.actualizarProspectoxIDDispositivo(prospectoBean);

                // Se genera el prospecto Movimiento Etapa
                ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean = new ProspectoMovimientoEtapaBean();

                prospectoMovimientoEtapaBean.setIdProspecto(prospectoBean.getIdProspecto());
                prospectoMovimientoEtapaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                prospectoMovimientoEtapaBean.setCodigoEstado(prospectoBean.getCodigoEstado());
                prospectoMovimientoEtapaBean.setCodigoEtapa(prospectoBean.getCodigoEtapa());
                prospectoMovimientoEtapaBean.setFechaMovimientoEtapaDispositivo(fechaActual);
                prospectoMovimientoEtapaBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                ProspectoMovimientoEtapaController.guardarProspectoMovimientoEtapa_GenerarIDDispositivo(prospectoMovimientoEtapaBean);

                // Actualizando cita
                citaBean.setCodigoEstado(Constantes.EstadoCita_Realizada);
                citaBean.setCodigoResultado(Constantes.ResultadoCita_NoInteresado);
                citaBean.setFechaModificacionDispositivo(fechaActual);
                citaBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
                CitaReunionController.modificarCita(citaBean,CalendarioActivity.this);

                CitaMovimientoEstadoBean citaMovimientoEstadoBean = new CitaMovimientoEstadoBean();
                citaMovimientoEstadoBean.setIdCita(citaBean.getIdCita());
                citaMovimientoEstadoBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());

                citaMovimientoEstadoBean.setCodigoEstado(citaBean.getCodigoEstado());
                citaMovimientoEstadoBean.setCodigoResultado(citaBean.getCodigoResultado());
                citaMovimientoEstadoBean.setFechaMovimientoEstadoDispositivo(fechaActual);
                citaMovimientoEstadoBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
                // Se Guarda la cita movimiento estado
                CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(citaMovimientoEstadoBean);
                ///////

                sincronizar();
            }
        }).setNegativeButton("Cancelar", null);

        AlertDialog  alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Button btnCancelar = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button btnAceptar = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        setFuente.setButton(this,btnAceptar);
        setFuente.setButton(this,btnCancelar);
    }

    public void completarADNPostCita(final ProspectoBean prospectoBean)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(CalendarioActivity.this);
        final View dialogCompletarADN = layoutInflater.inflate(R.layout.dialog_general, null);
        TextView tvTitulo = (TextView) dialogCompletarADN.findViewById(R.id.tvTitulo) ;
        tvTitulo.setText("Por favor completa el  ADN del prospecto.");
        final android.app.AlertDialog.Builder builderCompletarADN = new android.app.AlertDialog.Builder(CalendarioActivity.this);
        builderCompletarADN.setView(dialogCompletarADN).setCancelable(false).setNegativeButton("Cancelar", null).setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.pacifico.adn");
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                if(launchIntent != null){
                    launchIntent.putExtra(Constantes.tipo_operacion, Constantes.operacion_ADNdesdeAgenda);
                    launchIntent.putExtra(Constantes.parametro_idProspectoDis_ADNdesdeAgenda, prospectoBean.getIdProspectoDispositivo());
                    startActivity(launchIntent);

                    reiniciarParametros();
                    CerrarTarjetas();
                }
            }
        });

        android.app.AlertDialog alertCompletarADN = builderCompletarADN.create();
        alertCompletarADN.setCancelable(false);
        alertCompletarADN.show();
        Button btnaceptar = alertCompletarADN.getButton(DialogInterface.BUTTON_POSITIVE);
        Button btncancelar = alertCompletarADN.getButton(DialogInterface.BUTTON_NEGATIVE);
        setFuente.setButton(this,btnaceptar);
        setFuente.setButton(this,btncancelar);
        btnaceptar.setTextColor(getResources().getColor(R.color.colorCeleste));
    }

    public void cierreVentaPostCita(final CitaBean citaBean, final ProspectoBean prospectoBean)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(CalendarioActivity.this);
        final View dialogCierreVenta = layoutInflater.inflate(R.layout.dialog_cierre_venta, null);
        final android.app.AlertDialog.Builder builderCierreVenta = new android.app.AlertDialog.Builder(CalendarioActivity.this);
        builderCierreVenta.setView(dialogCierreVenta).setCancelable(false).setNegativeButton("Cancelar", null).setPositiveButton("Confirmar Cierre", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                    String fechaAccionCita = Util.obtenerFechaActual();
                    citaBean.setCodigoEstado(Constantes.EstadoCita_Realizada); // Se marca la cita como realizada
                    citaBean.setCodigoResultado(Constantes.ResultadoCita_CierreVenta); // resultado : Cierre de venta

                    // Actualizamos el prospecto
                    prospectoBean.setCodigoEstado(Constantes.EstadoProspecto_CierreVenta);

                    CheckBox checkSeguroVida = (CheckBox) dialogCierreVenta.findViewById(R.id.checkSeguroVida);
                    CheckBox checkSeguroAP = (CheckBox) dialogCierreVenta.findViewById(R.id.checkSeguroAP);

                    if (checkSeguroVida.isChecked()) {
                        EditText edtCantidadSV = (EditText) dialogCierreVenta.findViewById(R.id.edtCantidadSV);
                        String cantidadSV = edtCantidadSV.getText().toString();

                        if (cantidadSV.length() > 0) {
                            citaBean.setCantidadVI(Integer.parseInt(cantidadSV));
                        } else {
                            citaBean.setCantidadVI(0);
                        }

                        EditText edtPrimaTargetSV = (EditText) dialogCierreVenta.findViewById(R.id.edtPrimaTargetSV);
                        String primaTargetSV = edtPrimaTargetSV.getText().toString();
                        if (primaTargetSV.length() > 0) {
                            citaBean.setPrimaTargetVI(Double.parseDouble(primaTargetSV));
                        } else {
                            citaBean.setPrimaTargetVI(0);
                        }
                    }

                    if (checkSeguroAP.isChecked()) {
                        EditText edtCantidaSAP = (EditText) dialogCierreVenta.findViewById(R.id.edtCantidadSAP);
                        String cantidadSAP = edtCantidaSAP.getText().toString();

                        if (cantidadSAP.length() > 0) {
                            citaBean.setCantidadAP(Integer.parseInt(cantidadSAP));
                        } else {
                            citaBean.setCantidadAP(0);
                        }

                        EditText edtPrimaTargetSAP = (EditText) dialogCierreVenta.findViewById(R.id.edtPrimaTargetSAP);
                        String primaTargetSAP = edtPrimaTargetSAP.getText().toString();

                        if (primaTargetSAP.length() > 0) {
                            citaBean.setPrimaTargetAP(Double.parseDouble(primaTargetSAP));
                        } else {
                            citaBean.setPrimaTargetAP(0);
                        }
                    }

                    citaBean.setFechaModificacionDispositivo(fechaAccionCita);
                    citaBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                    // GENERANDO MOVIMIENTO DE CITA
                    CitaMovimientoEstadoBean citaMovimientoEstadoBean = new CitaMovimientoEstadoBean();
                    citaMovimientoEstadoBean.setIdCita(citaBean.getIdCita());
                    citaMovimientoEstadoBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());

                    citaMovimientoEstadoBean.setCodigoEstado(citaBean.getCodigoEstado());
                    citaMovimientoEstadoBean.setCodigoResultado(citaBean.getCodigoResultado());
                    citaMovimientoEstadoBean.setFechaMovimientoEstadoDispositivo(fechaAccionCita);
                    citaMovimientoEstadoBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                    CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(citaMovimientoEstadoBean);

                    CitaReunionController.modificarCita(citaBean, CalendarioActivity.this);

                    // Guardar Prospectos
                    prospectoBean.setFechaModificacionDispositivo(fechaAccionCita);
                    prospectoBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
                    ProspectoController.actualizarProspectoxIDDispositivo(prospectoBean);

                    tipoSincroCalendarioActivity = Constantes.dialog_sincro_siguienteetapa;
                    idProspectoDispositivoDialog = prospectoBean.getIdProspectoDispositivo();
                    sincronizar();
            }
        });

        android.app.AlertDialog alertCierreVenta = builderCierreVenta.create();
        alertCierreVenta.setCancelable(false);
        alertCierreVenta.show();
    }

    public void realizarCitaPostCita(final CitaBean citaBean, final ProspectoBean prospectoBean)
    {
        // Validamos escenario primera entrevista sin adn
        AdnBean adnProspecto =  ADNController.getAdnPorIdProspecto(prospectoBean.getIdProspecto(),prospectoBean.getIdProspectoDispositivo());
        if ( citaBean.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista &&
                ( adnProspecto == null || (adnProspecto!= null && adnProspecto.getFlagTerminado() != Constantes.FLAG_ACTIVO))
                )
        {
            completarADNPostCita(prospectoBean);
        }
        else {

            if (citaBean != null) {
                String fechaAccionCita = Util.obtenerFechaActual();
                citaBean.setCodigoEstado(Constantes.EstadoCita_Realizada); // Se marca la cita como realizada
                citaBean.setCodigoResultado(Constantes.ResultadoCita_SiguienteEtapa); // Se pasa a la siguiente etapa automaticamente
                citaBean.setFechaModificacionDispositivo(fechaAccionCita);
                citaBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                CitaReunionController.modificarCita(citaBean,CalendarioActivity.this);

                CitaMovimientoEstadoBean citaMovimientoEstadoBean = new CitaMovimientoEstadoBean();
                citaMovimientoEstadoBean.setIdCita(citaBean.getIdCita());
                citaMovimientoEstadoBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());

                citaMovimientoEstadoBean.setCodigoEstado(citaBean.getCodigoEstado());
                citaMovimientoEstadoBean.setCodigoResultado(citaBean.getCodigoResultado());
                citaMovimientoEstadoBean.setFechaMovimientoEstadoDispositivo(fechaAccionCita);
                citaMovimientoEstadoBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(citaMovimientoEstadoBean);

                tipoSincroCalendarioActivity = Constantes.dialog_sincro_siguienteetapa;
                idProspectoDispositivoDialog = prospectoBean.getIdProspectoDispositivo();
                sincronizar();
            }
        }
    }

    public void reiniciarParametros(){

        citaSoloLectura = 0;

        prospectoActual = null;
        citaActual = null; //Entrevista Actual creada en el proceso
        citaOriginal = null;

        citaModificoProspecto = false; // Indica si la Cita o el recordatorio modifico el prospecto

        reunionActual = null;
        recordatorioLLamadaDirectoBean = null;

        etapaFab = -1;
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()){
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                view.clearFocus();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /// SINCRONIZAR
    private ProgressDialog progressDialog;
    private int tipoSincroCalendarioActivity;
    private int idProspectoDispositivoDialog;
    public void sincronizar(){
        // Llamar al envío de datos #ENVIO
        SincronizacionController sincronizacionController = new SincronizacionController();
        if(progressDialog == null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Enviando información");
            progressDialog.setMessage("Espere, por favor");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
            @Override
            public void terminoSincronizacion(int codigo, String mensaje) {
                progressDialog.dismiss();
                if (codigo < 0) {
                    mostrarMensajeParaContinuar(mensaje);
                }else if(codigo == 1){
                    mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
                    continuar();
                }else{
                    if(codigo == 4 || codigo == 6){
                        Intent inicioSesionIntent = new Intent(CalendarioActivity.this, InicioSesionActivity.class);
                        inicioSesionIntent.putExtra("etapa", (codigo - 4)/2 + 1);
                        startActivity(inicioSesionIntent);
                    }else {
                        mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                    }
                }
            }
        });
        int resultado = sincronizacionController.sincronizar(this);
        if(resultado == 0){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
        }else if(resultado == 2){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
        }
    }

    private void mostrarMensajeParaContinuar(String mensaje){ // Funciona para guardar y descartar
        new android.app.AlertDialog.Builder(this)
                .setTitle("Alerta")
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              /*  dialog.cancel();*/
                verificarTiempoSinSincronizar();

            }
        }).setCancelable(false).show();
    }

    private void continuar() {
        reiniciarParametros();
        CerrarTarjetas();
        actualizarCalendario();
        if (tipoSincroCalendarioActivity == Constantes.dialog_sincro_siguienteetapa || tipoSincroCalendarioActivity == Constantes.dialog_sincro_descartar)
        {
            ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(idProspectoDispositivoDialog);
            GestionFlujoProspecto(prospectoBean);
        }
    }
    public void verificarTiempoSinSincronizar(){
        double tiempo = Util.expiroTiempoSinSincronizacion();
        if (tiempo > 0){
            int cantObjectosSinEnviar = 0;
            ArrayList<ReunionInternaBean> arrReunionInternas = CitaReunionController.obtenerReunionInternasSinEnviar();
            cantObjectosSinEnviar += arrReunionInternas.size();
            ArrayList<ProspectoBean> arrProspectos = ProspectoController.obtenerProspectosSinEviar();
            cantObjectosSinEnviar += arrProspectos.size();
            ArrayList<ProspectoMovimientoEtapaBean> arrProspectoMovimientoEtapas = ProspectoMovimientoEtapaController.obtenerProspectoMovimientoEtapaSinEnviar();
            cantObjectosSinEnviar += arrProspectoMovimientoEtapas.size();
            final ArrayList<AdnBean> arrAdns = ADNController.obtenerADNSinEviar();
            cantObjectosSinEnviar += arrAdns.size();
            ArrayList<FamiliarBean> arrFamiliares = FamiliarController.obtenerFamiliaresSinEviar();
            cantObjectosSinEnviar += arrFamiliares.size();
            ArrayList<CitaBean> arrCitas = CitaReunionController.obtenerCitasSinEnviar();
            cantObjectosSinEnviar += arrCitas.size();
            ArrayList<CitaMovimientoEstadoBean> arrCitaMovimientoEstados = CitaReunionController.obtenerCitasMovimientoEstadoSinEnviar();
            cantObjectosSinEnviar += arrCitaMovimientoEstados.size();
            ArrayList<ReferidoBean> arrReferidos = ReferidoController.obtenerReferidosSinEnviar();
            cantObjectosSinEnviar += arrReferidos.size();
            ArrayList<RecordatorioLlamadaBean> arrRecordatorioLlamadas = CitaReunionController.obtenerRecordatorioLlamadasSinEnviar();
            cantObjectosSinEnviar += arrRecordatorioLlamadas.size();
            if (cantObjectosSinEnviar > 0){
                ParametroBean tiempoSinSincronizacionParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(16);
                double tiempoSinSincronizacionExtra = tiempoSinSincronizacionParametroBean.getValorNumerico() * 60 * 60 * 1000;
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MILLISECOND, (int)(tiempoSinSincronizacionExtra - tiempo));
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd 'de' MMMM 'a las' hh:mm a");
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog
                        .setTitle("Alerta")
                        .setMessage("Por favor, realiza la sincronización antes del " + simpleDateFormat.format(calendar.getTime())+". Después de esta hora se bloquearán las aplicaciones.")
                        .setPositiveButton("Sincronizar Ahora", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sincronizar();
                            }
                        }).setCancelable(false);
                if (tiempo < tiempoSinSincronizacionExtra){
                    alertDialog.setNegativeButton("Sincronizar mas tarde", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                alertDialog.show();
            }
        }
    }
    // FIN METODOS SINCRONIZAR

}
