package com.pacifico.agenda.Reminder;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ComponentInfo;
import android.util.Log;

import com.pacifico.agenda.Model.Bean.ReminderBean;

public class OnAlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("REMINDER", "Received wake up from alarm manager.");
		int rowid = intent.getExtras().getInt(ReminderBean.CN_ID);
		Log.d("REMINDER", "OnAlarmReceiver ID=" + rowid);

		WakeReminderIntentService.acquireStaticLock(context);
		Intent i = new Intent(context, ReminderService.class);
		i.putExtra(ReminderBean.CN_ID, rowid);
		context.startService(i);
	}
}
