package com.pacifico.agenda.Reminder;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.pacifico.agenda.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class ReminderTestActivity extends AppCompatActivity {
    Context context;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;

    private android.widget.EditText etFecha;
    private android.widget.EditText etHora;
    private android.widget.Button btnAgregar;
    private android.widget.EditText etId;
    private android.widget.Button btnBorrar;
    private EditText etTitle;
    private EditText etBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_test);
        this.etBody = (EditText) findViewById(R.id.etBody);
        this.etTitle = (EditText) findViewById(R.id.etTitle);
        this.btnBorrar = (Button) findViewById(R.id.btnBorrar);
        this.etId = (EditText) findViewById(R.id.etId);
        this.btnAgregar = (Button) findViewById(R.id.btnAgregar);
        this.etHora = (EditText) findViewById(R.id.etHora);
        this.etFecha = (EditText) findViewById(R.id.etFecha);

        context = this;

        this.btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = etId.getText().toString().trim();
                if (!id.isEmpty()) {
                    new ReminderManager(context).deleteReminder(Integer.parseInt(id));
                } else {

                    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    Date d = new Date();
                    String date = f.format(d);
                    Log.d("REMINDER", "CURRENT DATE=" + date);

                }
            }
        });

        this.btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String title = etTitle.getText().toString().trim();
                String body = etBody.getText().toString().trim();

                String fecha = etFecha.getText().toString().trim();
                String hora = etHora.getText().toString().trim();

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date d = null;
                try {
                    d = f.parse(fecha + " " + hora);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (d != null) {
                    long milliseconds = d.getTime();

                    ReminderManager reminderManager = new ReminderManager(context);

                    // Se comento por cambios
                    //int id = reminderManager.addReminder(title, body, milliseconds);
                    //etId.setText(String.valueOf(id));
                }

            }
        });

        final Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etFecha.setText(formatFecha(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(newCalendar.get(Calendar.YEAR),
                        newCalendar.get(Calendar.MONTH),
                        newCalendar.get(Calendar.DAY_OF_MONTH),
                        i,
                        i1);
                etHora.setText(formatHora(newDate.getTime()));

            }


        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);

        etFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }

        });

        etHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePickerDialog.show();
            }
        });

        etFecha.setInputType(InputType.TYPE_NULL);
        etFecha.setFocusable(false);

        etHora.setInputType(InputType.TYPE_NULL);
        etHora.setFocusable(false);


    }

    public String formatFecha(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    public String formatHora(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        return dateFormat.format(date);
    }
}







