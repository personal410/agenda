package com.pacifico.agenda.Reminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ComponentInfo;
import android.database.Cursor;
import android.util.Log;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Model.Bean.ReminderBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class OnBootReceiver extends BroadcastReceiver {
	
	private static final String TAG = ComponentInfo.class.getCanonicalName();
	
	@Override
	public void onReceive(Context context, Intent intent) {

		ReminderManager reminderMgr = new ReminderManager(context);

		ArrayList<Entity> reminders = ReminderBean.tableHelper.getEntities(null, null);


		ReminderBean bean;
		Date currentDate = new Date();
		long currentMillis = currentDate.getTime();

		for (Entity entity : reminders) {
			bean = (ReminderBean) entity;

			int rowId = Integer.parseInt(bean.getId());
			String dateTime =  bean.getReminderDateTime() ;

			//Calendar cal = Calendar.getInstance();
			//SimpleDateFormat format = new SimpleDateFormat(ReminderBean.DATE_TIME_FORMAT);

			try {

				long dtLong = Long.parseLong(dateTime);

				Log.d("REMINDER", "dtLong =" + dtLong + " currentMillis=" + currentMillis);

				Log.d("REMINDER", "REPROGRAMANDO ID=" + rowId);

				reminderMgr.setReminder(rowId, dtLong);

				/*
				if (dtLong >= currentMillis) {

					Log.d("REMINDER", "REPROGRAMANDO ID=" + rowId);

					reminderMgr.setReminder(rowId, dtLong);
				} else {
					//borrando los antiguos a currentMillis
					Log.d("REMINDER", "BORRANDO ID=" + rowId);
					String whereClause = ReminderBean.CN_ID + " = ?";
					String[] whereArgs = {String.valueOf(rowId)};
					ReminderBean.tableHelper.deleteEntity(whereClause, whereArgs);

				}*/

			} catch (Exception e) {
				Log.e("OnBootReceiver", e.getMessage(), e);
			}
		}

	}
}

