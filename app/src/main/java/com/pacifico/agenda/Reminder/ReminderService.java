package com.pacifico.agenda.Reminder;

import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.ReminderBean;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ReminderService extends WakeReminderIntentService {
	Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	public ReminderService() {
		super("ReminderService");
			}

	@Override
	void doReminderWork( Intent intent) {

		int id = intent.getExtras().getInt(ReminderBean.CN_ID, -1);

		Log.d("REMINDER", "ReminderService ID = " + id);

		NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

		String whereClause = ReminderBean.CN_ID + " = ?";
		String[] whereArgs = {String.valueOf(id)};
		ArrayList<Entity> lista = ReminderBean.tableHelper.getEntities(whereClause, whereArgs);

		// Luego de obtenerlos se eliminan
		ReminderBean.tableHelper.deleteEntity(whereClause, whereArgs);

		if (lista != null && lista.size() > 0) {

			ReminderBean bean = (ReminderBean) lista.get(0);

			int notificationId = Integer.valueOf(bean.getId());

			builder.setSmallIcon(R.drawable.logo_notification)
					.setAutoCancel(true)
					.setSound(alarmSound)
					.setContentTitle(bean.getTitle())
					.setContentText(bean.getBody())
					;

			//Dismiss Intent
			Intent dismissIntent = new Intent(this, ButtonNotificationReceiver.class);
			dismissIntent.putExtra(Constantes.extra_not_id,notificationId);
			dismissIntent.putExtra(Constantes.extra_not_tipoBoton,Constantes.boton_dismiss);
			PendingIntent btPendingIntent = PendingIntent.getBroadcast(this, notificationId, dismissIntent,PendingIntent.FLAG_ONE_SHOT);

			builder.addAction(R.drawable.ic_done, "ENTENDIDO", btPendingIntent).setAutoCancel(true);

			if (bean.getFuenteAlerta() == Constantes.alerta_recordatorio) {
				Intent revisarIntent = new Intent(this, ButtonNotificationReceiver.class);
				revisarIntent.putExtra(Constantes.extra_not_id,notificationId);
				revisarIntent.putExtra(Constantes.extra_not_tipoBoton,Constantes.boton_recordatorio);
				revisarIntent.putExtra(Constantes.extra_not_idFuente,bean.getIdDispositivoFuente());
				PendingIntent pendingIntentRevisar = PendingIntent.getBroadcast(this, notificationId +1, revisarIntent,PendingIntent.FLAG_ONE_SHOT);

				builder.addAction(R.drawable.ic_action_communication_phone, "REVISAR EVENTOS", pendingIntentRevisar).setAutoCancel(true);

				notificationManager.notify(notificationId, builder.build());
			}
			else if (bean.getFuenteAlerta() == Constantes.alerta_cita) {
				Intent revisarIntent = new Intent(this, ButtonNotificationReceiver.class);
				revisarIntent.putExtra(Constantes.extra_not_id,notificationId);
				revisarIntent.putExtra(Constantes.extra_not_tipoBoton,Constantes.boton_alerta_cita);
				revisarIntent.putExtra(Constantes.extra_not_idFuente,bean.getIdDispositivoFuente());
				PendingIntent pendingIntentRevisar = PendingIntent.getBroadcast(this, notificationId +1, revisarIntent,PendingIntent.FLAG_ONE_SHOT);

				builder.addAction(R.drawable.calendario_gris_oscuro, "REVISAR EVENTOS", pendingIntentRevisar).setAutoCancel(true);

				//notificationManager.notify(notificationId, builder.build());
				CitaBean citatemp = CitaReunionController.obtenerCitaPorIdCitaDispositivo(bean.getIdDispositivoFuente());

				Calendar calActual = Calendar.getInstance();
				Date dateCita = null;
				Calendar calCita = Calendar.getInstance();
				try {
					dateCita = new SimpleDateFormat(Constantes.formato_fechafull_envio).parse(citatemp.getFechaCita() + " " +citatemp.getHoraInicio());
					calCita.setTime(dateCita);
				}catch (Exception e){}

				if (citatemp != null && dateCita != null && citatemp.getFechaCita()!= null && citatemp.getHoraInicio()!= null
						&& (calActual.getTimeInMillis() < calCita.getTimeInMillis()))
					notificationManager.notify(notificationId, builder.build());
			}
			else if (bean.getFuenteAlerta() == Constantes.alerta_post_cita) {
				Intent revisarIntent = new Intent(this, ButtonNotificationReceiver.class);
				revisarIntent.putExtra(Constantes.extra_not_id,notificationId);
				revisarIntent.putExtra(Constantes.extra_not_tipoBoton,Constantes.boton_alerta_post_cita);
				revisarIntent.putExtra(Constantes.extra_not_idFuente,bean.getIdDispositivoFuente());
				PendingIntent pendingIntentRevisar = PendingIntent.getBroadcast(this, notificationId +1, revisarIntent,PendingIntent.FLAG_ONE_SHOT);

				builder.addAction(R.drawable.calendario_gris_oscuro, "REVISAR EVENTOS", pendingIntentRevisar).setAutoCancel(true);

				CitaBean citatemp = CitaReunionController.obtenerCitaPorIdCitaDispositivo(bean.getIdDispositivoFuente());

				if (citatemp != null && citatemp.getCodigoResultado() == Constantes.ResultadoCita_Pendiente)
					notificationManager.notify(notificationId, builder.build());

			}


		}

	}
}
