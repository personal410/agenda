package com.pacifico.agenda.Reminder;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Debug;
import android.util.Log;

import com.pacifico.agenda.Util.Constantes;

import java.lang.reflect.Method;

/**
 * Created by joel on 8/30/16.
 */
public class ButtonNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        //Debug.waitForDebugger();
        int notificationId = intent.getIntExtra(Constantes.extra_not_id, 0);
        int tipoBoton = intent.getIntExtra(Constantes.extra_not_tipoBoton, 0);

        Log.d("ButtonNotification", "onReceive: notification id= "+notificationId);

        // Do what you want were.
        if (tipoBoton == Constantes.boton_recordatorio)
        {
            int idFuente = intent.getIntExtra(Constantes.extra_not_idFuente, -1);

            if (idFuente != -1) {
                Intent i = new Intent();
                i.setClassName("com.pacifico.agenda", "com.pacifico.agenda.Activity.CalendarioActivity");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(Constantes.tipo_operacion, Constantes.operacion_AbrirRecordatorioIndependiente);
                i.putExtra(Constantes.parametro_idRecordatorioIndependiente, idFuente);
                context.startActivity(i);
            }
        }else if (tipoBoton == Constantes.boton_alerta_cita)
        {
            int idFuente = intent.getIntExtra(Constantes.extra_not_idFuente, -1);

            if (idFuente != -1) {
                Intent i = new Intent();
                i.setClassName("com.pacifico.agenda", "com.pacifico.agenda.Activity.CalendarioActivity");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(Constantes.tipo_operacion, Constantes.operacion_AbrirAlertaCita);
                i.putExtra(Constantes.parametro_idAlertaCita, idFuente);
                context.startActivity(i);
            }
        }else if (tipoBoton == Constantes.boton_alerta_post_cita)
        {
            int idFuente = intent.getIntExtra(Constantes.extra_not_idFuente, -1);

            if (idFuente != -1) {
                Intent i = new Intent();
                i.setClassName("com.pacifico.agenda", "com.pacifico.agenda.Activity.CalendarioActivity");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(Constantes.tipo_operacion, Constantes.operacion_AbrirAlertaPostCita);
                i.putExtra(Constantes.parametro_idAlertaCita, idFuente);
                context.startActivity(i);
            }
        }

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(notificationId);
    }
}