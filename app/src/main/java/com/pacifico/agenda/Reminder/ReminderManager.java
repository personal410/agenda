package com.pacifico.agenda.Reminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ReminderBean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ReminderManager {

	private Context mContext;
	private AlarmManager mAlarmManager;
	
	public ReminderManager(Context context) {
		mContext = context; 
		mAlarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
	}
	
	public void setReminder(int id, long time) {
		
        Intent intent = new Intent(mContext, OnAlarmReceiver.class);
		intent.putExtra(ReminderBean.CN_ID, id);

        PendingIntent pi = PendingIntent.getBroadcast(mContext, id ,intent, PendingIntent.FLAG_ONE_SHOT);
		//PendingIntent pi = PendingIntent.getBroadcast(mContext, 0, i,
		//		PendingIntent.FLAG_UPDATE_CURRENT);

        mAlarmManager.set(AlarmManager.RTC_WAKEUP, time, pi);
	}

	public void deleteReminder(int id) {

		Intent intent = new Intent(mContext, OnAlarmReceiver.class);
		//i.putExtra(ReminderBean.CN_ID, id);

		//PendingIntent pi = PendingIntent.getBroadcast(mContext, 0, i, PendingIntent.FLAG_ONE_SHOT);

		PendingIntent pi = PendingIntent.getBroadcast(mContext, id, intent,
				PendingIntent.FLAG_ONE_SHOT);

		String whereClause = ReminderBean.CN_ID + " = ?";
		String[] whereArgs = {String.valueOf(id)};
		ReminderBean.tableHelper.deleteEntity(whereClause, whereArgs);
		mAlarmManager.cancel(pi);
	}

	/*public int addReminder(String title, String body, long time) {

		final int id = (int) System.currentTimeMillis();

		ReminderBean bean = new ReminderBean();
		bean.setId(String.valueOf(id));
		bean.setTitle(title);
		bean.setBody(body);
		bean.setReminderDateTime(String.valueOf(time));

		ReminderBean.tableHelper.insertEntity(bean);

		setReminder(id, time);

		return id;
	}*/

	public int addReminder(ReminderBean reminderBean) {

		final int id = (int) System.currentTimeMillis();

		reminderBean.setId(String.valueOf(id));

		ReminderBean.tableHelper.insertEntity(reminderBean);

		setReminder(id, Long.parseLong(reminderBean.getReminderDateTime()));

		return id;
	}

	private String getFechaHoraActual() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	/// Obtener IDS
	public String obtenerIDReminderPorIDFuente(int fuenteAlerta,int idfuente){
		String[] parameters = new String[]{String.valueOf(fuenteAlerta),String.valueOf(idfuente)};
		ArrayList<Entity> arrReminder = ReminderBean.tableHelper.getEntities("FUENTEALERTA= ? AND IDDISPOSITIVOFUENTE = ?", parameters);
		if(arrReminder.size() > 0){
			return ((ReminderBean)arrReminder.get(0)).getId();
		}else{
			return null;
		}
	}

}
