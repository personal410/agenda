package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by joel on 7/4/16.
 */


public class RecordatorioLlamadaBean extends Entity {
    int IdRecordatorioLlamada;
    int IdRecordatorioLlamadaDispositivo;
    int IdProspecto;
    int IdProspectoDispositivo;
    int IdCita;
    int IdCitaDispositivo;
    private String FechaRecordatorio;
    int FlagActivo;
    private int FlagEnviado;
    private String FechaCreacionDispositivo;
    private String FechaModificacionDispositivo;

    public RecordatorioLlamadaBean(){
        IdRecordatorioLlamada = -1;
        IdRecordatorioLlamadaDispositivo = -1;
        IdProspecto = -1;
        IdProspectoDispositivo = -1;
        IdCita = -1;
        IdCitaDispositivo =-1;
        FechaRecordatorio = null;
        FlagActivo = -1;
        FlagEnviado = -1;
        FechaCreacionDispositivo = null;
        FechaModificacionDispositivo = null;
    }

    public int getIdRecordatorioLlamada() {
        return IdRecordatorioLlamada;
    }

    public void setIdRecordatorioLlamada(int idRecordatorioLlamada) {
        IdRecordatorioLlamada = idRecordatorioLlamada;
    }

    public int getIdProspecto() {
        return IdProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        IdProspecto = idProspecto;
    }

    public int getIdCita() {
        return IdCita;
    }

    public void setIdCita(int idCita) {
        IdCita = idCita;
    }

    public int getIdRecordatorioLlamadaDispositivo() {
        return IdRecordatorioLlamadaDispositivo;
    }

    public void setIdRecordatorioLlamadaDispositivo(int idRecordatorioLlamadaDispositivo) {
        IdRecordatorioLlamadaDispositivo = idRecordatorioLlamadaDispositivo;
    }

    public String getFechaRecordatorio() {
        return FechaRecordatorio;
    }

    public void setFechaRecordatorio(String fechaRecordatorio) {
        FechaRecordatorio = fechaRecordatorio;
    }

    public int getFlagActivo() {
        return FlagActivo;
    }

    public void setFlagActivo(int flagActivo) {
        FlagActivo = flagActivo;
    }

    public int getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public int getIdCitaDispositivo() {
        return IdCitaDispositivo;
    }

    public void setIdCitaDispositivo(int idCitaDispositivo) {
        IdCitaDispositivo = idCitaDispositivo;
    }

    public int getFlagEnviado() {
        return FlagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        FlagEnviado = flagEnviado;
    }

    public String getFechaCreacionDispositivo() {
        return FechaCreacionDispositivo;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return FechaModificacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDRECORDATORIOLLAMADA:
                return IdRecordatorioLlamada;
            case CO_IDRECORDATORIOLLAMADADISPOSITIVO:
                return IdRecordatorioLlamadaDispositivo;
            case CO_IDPROSPECTO:
                return IdProspecto;
            case CO_IDPROSPECTODISPOSITIVO:
                return IdProspectoDispositivo;
            case CO_IDCITA:
                return IdCita;
            case CO_IDCITADISPOSITIVO:
                return IdCitaDispositivo;
            case CO_FECHARECORDATORIO:
                return FechaRecordatorio;
            case CO_FECHACREACIONDISPOSITIVO:
                return FechaCreacionDispositivo;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                return FechaModificacionDispositivo;
            case CO_FLAGACTIVO:
                return FlagActivo;
            case CO_FLAGENVIADO:
                return FlagEnviado;
        }
        return null;
    }
    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDRECORDATORIOLLAMADA:
                IdRecordatorioLlamada = (int) object;
                break;
            case CO_IDRECORDATORIOLLAMADADISPOSITIVO:
                IdRecordatorioLlamadaDispositivo = (int) object;
                break;
            case CO_IDPROSPECTO:
                IdProspecto = (int) object;
                break;
            case CO_IDPROSPECTODISPOSITIVO:
                IdProspectoDispositivo = (int) object;
                break;
            case CO_IDCITA:
                IdCita = (int) object;
                break;
            case CO_IDCITADISPOSITIVO:
                IdCitaDispositivo = (int) object;
                break;
            case CO_FECHARECORDATORIO:
                FechaRecordatorio = (String) object;
                break;
            case CO_FECHACREACIONDISPOSITIVO:
                FechaCreacionDispositivo = (String)object;
                break;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                FechaModificacionDispositivo = (String)object;
                break;
            case CO_FLAGACTIVO:
                FlagActivo = (int) object;
                break;
            case CO_FLAGENVIADO:
                FlagEnviado = (int)object;
                break;
        }
    }

    public static final String CN_IDRECORDATORIOLLAMADA = "IDRECORDATORIOLLAMADA";
    public static final String CT_IDRECORDATORIOLLAMADA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDRECORDATORIOLLAMADA = 1;

    public static final String CN_IDRECORDATORIOLLAMADADISPOSITIVO = "IDRECORDATORIOLLAMADADISPOSITIVO";
    public static final String CT_IDRECORDATORIOLLAMADADISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDRECORDATORIOLLAMADADISPOSITIVO = 2;

    public static final String CN_IDPROSPECTO = "IDPROSPECTO";
    public static final String CT_IDPROSPECTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTO = 3;

    public static final String CN_IDPROSPECTODISPOSITIVO = "IDPROSPECTODISPOSITIVO";
    public static final String CT_IDPROSPECTODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTODISPOSITIVO = 4;

    public static final String CN_IDCITA= "IDCITA";
    public static final String CT_IDCITA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDCITA= 5;

    public static final String CN_IDCITADISPOSITIVO= "IDCITADISPOSITIVO";
    public static final String CT_IDCITADISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDCITADISPOSITIVO= 6;

    public static final String CN_FECHARECORDATORIO = "FECHARECORDATORIO";
    public static final String CT_FECHARECORDATORIO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHARECORDATORIO = 7;

    public static final String CN_FECHACREACIONDISPOSITIVO = "FECHACREACIONDISPOSITIVO";
    public static final String CT_FECHACREACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACREACIONDISPOSITIVO = 8;

    public static final String CN_FECHAMODIFICACIONDISPOSITIVO = "FECHAMODIFICACIONDISPOSITIVO";
    public static final String CT_FECHAMODIFICACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAMODIFICACIONDISPOSITIVO = 9;

    public static final String CN_FLAGACTIVO = "FLAGACTIVO";
    public static final String CT_FLAGACTIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGACTIVO = 10;

    public static final String CN_FLAGENVIADO = "FLAGENVIADO";
    public static final String CT_FLAGENVIADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIADO = 11;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_RECORDATORIO_LLAMADA, new OrganizateEntityFactory())
            .addColumn(CN_IDRECORDATORIOLLAMADA, CT_IDRECORDATORIOLLAMADA, CO_IDRECORDATORIOLLAMADA)
            .addColumn(CN_IDRECORDATORIOLLAMADADISPOSITIVO, CT_IDRECORDATORIOLLAMADADISPOSITIVO, CO_IDRECORDATORIOLLAMADADISPOSITIVO)
            .addColumn(CN_IDPROSPECTO, CT_IDPROSPECTO, CO_IDPROSPECTO)
            .addColumn(CN_IDPROSPECTODISPOSITIVO, CT_IDPROSPECTODISPOSITIVO, CO_IDPROSPECTODISPOSITIVO)
            .addColumn(CN_IDCITA, CT_IDCITA, CO_IDCITA)
            .addColumn(CN_IDCITADISPOSITIVO, CT_IDCITADISPOSITIVO, CO_IDCITADISPOSITIVO)
            .addColumn(CN_FECHARECORDATORIO, CT_FECHARECORDATORIO, CO_FECHARECORDATORIO)
            .addColumn(CN_FECHACREACIONDISPOSITIVO, CT_FECHACREACIONDISPOSITIVO, CO_FECHACREACIONDISPOSITIVO)
            .addColumn(CN_FECHAMODIFICACIONDISPOSITIVO, CT_FECHAMODIFICACIONDISPOSITIVO, CO_FECHAMODIFICACIONDISPOSITIVO)
            .addColumn(CN_FLAGACTIVO, CT_FLAGACTIVO, CO_FLAGACTIVO)
            .addColumn(CN_FLAGENVIADO, CT_FLAGENVIADO, CO_FLAGENVIADO);
}