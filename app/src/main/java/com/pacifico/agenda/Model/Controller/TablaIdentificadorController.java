package com.pacifico.agenda.Model.Controller;

import com.dsbmobile.dsbframework.controller.persistence.Entity;

import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.TablaIdentificadorBean;
import com.pacifico.agenda.Persistence.DatabaseConstants;


import java.util.ArrayList;

/**
 * Created by joel on 7/5/16.
 */
public class TablaIdentificadorController{
    public static TablaIdentificadorBean obtenerTablaIdentificadorporTabla(String tabla){
        String[] parameters = new String[]{tabla};
        ArrayList<Entity> arrIdentityTabla = TablaIdentificadorBean.tableHelper.getEntities(TablaIdentificadorBean.CN_NOMBRETABLA + " = ?", parameters);
        if(arrIdentityTabla.size() > 0){
            return (TablaIdentificadorBean)arrIdentityTabla.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarTablaIdentificador(TablaIdentificadorBean tablaIdentificadorBean){
        String[] parameters = new String[]{String.valueOf(tablaIdentificadorBean.getNombreTabla())};
        TablaIdentificadorBean.tableHelper.updateEntity(tablaIdentificadorBean, TablaIdentificadorBean.CN_NOMBRETABLA + " = ?", parameters);
    }
    public static void inicializarTabla(){
        if(!existeIDs()){
            TablaIdentificadorBean tablaIdentificadorBean = new TablaIdentificadorBean();
            tablaIdentificadorBean.setIdentity(0);

            tablaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_CITA);
            TablaIdentificadorBean.tableHelper.insertEntity(tablaIdentificadorBean);

            tablaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO);
            TablaIdentificadorBean.tableHelper.insertEntity(tablaIdentificadorBean);

            tablaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_FAMILIAR);
            TablaIdentificadorBean.tableHelper.insertEntity(tablaIdentificadorBean);

            tablaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_REFERIDO);
            TablaIdentificadorBean.tableHelper.insertEntity(tablaIdentificadorBean);

            tablaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_REUNION_INTERNA);
            TablaIdentificadorBean.tableHelper.insertEntity(tablaIdentificadorBean);

            tablaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_PROSPECTO);
            TablaIdentificadorBean.tableHelper.insertEntity(tablaIdentificadorBean);

            tablaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_PROSPECTO_MOVIMIENTO_ETAPA);
            TablaIdentificadorBean.tableHelper.insertEntity(tablaIdentificadorBean);

            tablaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_RECORDATORIO_LLAMADA);
            TablaIdentificadorBean.tableHelper.insertEntity(tablaIdentificadorBean);
        }
    }
    public static boolean existeIDs(){
        ArrayList<Entity> arrIdentificadores = TablaIdentificadorBean.tableHelper.getEntities("", new String[]{});
        return arrIdentificadores.size() > 0;
    }
}