package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by vctrls3477 on 2/07/16.
 */
public class EntidadBean extends Entity {
    private int IdEntidad;
    private String Descripcion;
    private String Imagen;
    private int CodigoTipoCobranza;
    public int getIdEntidad() {
        return IdEntidad;
    }
    public void setIdEntidad(int idEntidad) {
        IdEntidad = idEntidad;
    }
    public String getDescripcion() {
        return Descripcion;
    }
    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
    public String getImagen() {
        return Imagen;
    }
    public void setImagen(String imagen) {
        Imagen = imagen;
    }
    public int getCodigoTipoCobranza() {
        return CodigoTipoCobranza;
    }
    public void setCodigoTipoCobranza(int codigoTipoCobranza) {
        CodigoTipoCobranza = codigoTipoCobranza;
    }
    @Override
    public Object getColumnValue(int column) {
        switch(column){
            case CO_IDENTIDAD:
                return IdEntidad;
            case CO_DESCRIPCION:
                return Descripcion;
            case CO_IMAGEN:
                return Imagen;
            case CO_CODIGOTIPOCOBRANZA:
                return CodigoTipoCobranza;
        }
        return null;
    }
    @Override
    public void setColumnValue(int column, Object object) {
        switch(column){
            case CO_IDENTIDAD:
                IdEntidad = (int)object;
                break;
            case CO_DESCRIPCION:
                Descripcion = (String)object;
                break;
            case CO_IMAGEN:
                Imagen = (String)object;
                break;
            case CO_CODIGOTIPOCOBRANZA:
                CodigoTipoCobranza = (int)object;
                break;
        }
    }
    public static final String CN_IDENTIDAD = "IDENTIDAD";
    public static final String CT_IDENTIDAD = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDENTIDAD = 1;

    public static final String CN_DESCRIPCION = "DESCRIPCION";
    public static final String CT_DESCRIPCION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_DESCRIPCION = 2;

    public static final String CN_IMAGEN = "IMAGEN";
    public static final String CT_IMAGEN = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_IMAGEN = 3;

    public static final String CN_CODIGOTIPOCOBRANZA = "CODIGOTIPOCOBRANZA";
    public static final String CT_CODIGOTIPOCOBRANZA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOTIPOCOBRANZA = 4;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_ENTIDAD, new OrganizateEntityFactory())
            .addColumn(CN_IDENTIDAD, CT_IDENTIDAD, CO_IDENTIDAD)
            .addColumn(CN_DESCRIPCION, CT_DESCRIPCION, CO_DESCRIPCION)
            .addColumn(CN_IMAGEN, CT_IMAGEN, CO_IMAGEN)
            .addColumn(CN_CODIGOTIPOCOBRANZA, CT_CODIGOTIPOCOBRANZA, CO_CODIGOTIPOCOBRANZA);
}