package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class TablaTablasBean extends Entity{

    //private int IdTablaTablas;
    private int IdTabla;
    private int CodigoCampo;
    private String ValorCadena;
    private double ValorNumerico;
    private int FlagActivo;

    /*public int getIdTablaTablas() {
        return IdTablaTablas;
    }

    public void setIdTablaTablas(int idTablaTablas) {
        IdTablaTablas = idTablaTablas;
    }*/

    public int getIdTabla() {
        return IdTabla;
    }

    public void setIdTabla(int idTabla) {
        IdTabla = idTabla;
    }

    public int getCodigoCampo() {
        return CodigoCampo;
    }

    public void setCodigoCampo(int codigoCampo) {
        CodigoCampo = codigoCampo;
    }

    public String getValorCadena() {
        return ValorCadena;
    }

    public void setValorCadena(String valorCadena) {
        ValorCadena = valorCadena;
    }

    public double getValorNumerico() {
        return ValorNumerico;
    }

    public void setValorNumerico(double valorNumerico) {
        ValorNumerico = valorNumerico;
    }

    public int getFlagActivo() {
        return FlagActivo;
    }

    public void setFlagActivo(int flagActivo) {
        FlagActivo = flagActivo;
    }

    // Persistencia

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            //case CO_IDTABLATABLAS:
              //  return IdTablaTablas;

            case CO_IDTABLA:
                return IdTabla;

            case CO_CODIGOCAMPO:
                return CodigoCampo;

            case CO_VALORCADENA:
                return ValorCadena;

            case CO_VALORNUMERICO:
                return ValorNumerico;

            case CO_FLAGACTIVO:
                return FlagActivo;

        }
        return null;
    }



    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            /*case CO_IDTABLATABLAS:
                IdTablaTablas = (int) object;
                break;*/
            case CO_IDTABLA:
                IdTabla = (int) object;
                break;
            case CO_CODIGOCAMPO:
                CodigoCampo = (int) object;
                break;
            case CO_VALORCADENA:
                ValorCadena = (String) object;
                break;
            case CO_VALORNUMERICO:
                ValorNumerico = (double) object;
                break;
            case CO_FLAGACTIVO:
                FlagActivo = (int) object;
                break;
        }
    }

    /*public static final String CN_IDTABLATABLAS = "IDTABLATABLAS";
    public static final String CT_IDTABLATABLAS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDTABLATABLAS = 1;*/

    public static final String CN_IDTABLA = "IDTABLA";
    public static final String CT_IDTABLA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDTABLA = 1;

    public static final String CN_CODIGOCAMPO = "CODIGOCAMPO";
    public static final String CT_CODIGOCAMPO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOCAMPO = 2;

    public static final String CN_VALORCADENA = "VALORCADENA";
    public static final String CT_VALORCADENA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_VALORCADENA = 3;

    public static final String CN_VALORNUMERICO = "VALORNUMERICO";
    public static final String CT_VALORNUMERICO = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_VALORNUMERICO = 4;

    public static final String CN_FLAGACTIVO = "FLAGACTIVO";
    public static final String CT_FLAGACTIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGACTIVO = 5;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_TABLA_TABLAS, new OrganizateEntityFactory())
            //.addColumn(CN_IDTABLATABLAS, CT_IDTABLATABLAS, CO_IDTABLATABLAS)
            .addColumn(CN_IDTABLA, CT_IDTABLA, CO_IDTABLA)
            .addColumn(CN_CODIGOCAMPO, CT_CODIGOCAMPO, CO_CODIGOCAMPO)
            .addColumn(CN_VALORCADENA, CT_VALORCADENA, CO_VALORCADENA)
            .addColumn(CN_VALORNUMERICO, CT_VALORNUMERICO, CO_VALORNUMERICO)
            .addColumn(CN_FLAGACTIVO, CT_FLAGACTIVO, CO_FLAGACTIVO);
}
