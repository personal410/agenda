package com.pacifico.agenda.Model.Controller;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Model.Bean.IntermediarioBean;

import java.util.ArrayList;

/**
 * Created by joel on 7/11/16.
 */
public class IntermediarioController {

        public static void guardarIntermediario(IntermediarioBean intermediarioBean){
            IntermediarioBean intermediario = obtenerIntermediario();
            if(intermediario == null){
                IntermediarioBean.tableHelper.insertEntity(intermediarioBean);
            }else{
                IntermediarioBean.tableHelper.updateEntity(intermediarioBean, "", null);
            }
        }

        public static IntermediarioBean obtenerIntermediario(){
            ArrayList<Entity> arrIntermediarios = IntermediarioBean.tableHelper.getEntities("", null);
            if(arrIntermediarios.size() > 0){
                return (IntermediarioBean)arrIntermediarios.get(0);
            }else{
                return null;
            }
        }

}
