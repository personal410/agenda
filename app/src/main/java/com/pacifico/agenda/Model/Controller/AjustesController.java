package com.pacifico.agenda.Model.Controller;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Model.Bean.AjustesBean;

import java.util.ArrayList;

/**
 * Created by daniel on 06/09/2016.
 */
public class AjustesController {


    public static void actualizarAjuste(AjustesBean ajustesBean){
        String[] parametros = {Integer.toString(ajustesBean.getIdAjuste())};
        AjustesBean.tableHelper.updateEntity(ajustesBean, "IDAJUSTE = ?", parametros);
    }

    public static void inicializarTabla(){

        AjustesBean ajustes = obtenerAjuste();
        AjustesBean ajustesBean = new AjustesBean();
        if(ajustes == null){
            ajustesBean.setIdAjuste(1);
            ajustesBean.setDescripcion("Ajuste por defecto");
            ajustesBean.setValor1("60");
            ajustesBean.setValor2("0");
            ajustesBean.setValor3("0");
            ajustesBean.setValor4("0");
            ajustesBean.setValor5("0");
            AjustesBean.tableHelper.insertEntity(ajustesBean);

        }else{
            AjustesBean.tableHelper.updateEntity(ajustes, "", null);
        }
    }

    public static AjustesBean obtenerAjuste(){
        ArrayList<Entity> arrAjustes = AjustesBean.tableHelper.getEntities("", null);
        if(arrAjustes.size() > 0){
            return (AjustesBean) arrAjustes.get(0);
        }else{
            return null;
        }
    }

}
