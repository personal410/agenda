package com.pacifico.agenda.Model.Bean;


import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class DispositivoBean  extends Entity {
    private int IdDispositivo;
    private int IdIntermediario;
    private String Marca;
    private String Modelo;
    private String SistemaOperativo;
    private String MAC;
    private String NumeroSerie;
    private String FechaCreacion;
    private String FechaUltimaSincronizacion;
    private int FlagNuevaMigracionCartera;

    public int getIdDispositivo() {
        return IdDispositivo;
    }

    public void setIdDispositivo(int idDispositivo) {
        IdDispositivo = idDispositivo;
    }

    public int getIdIntermediario() {
        return IdIntermediario;
    }

    public void setIdIntermediario(int idIntermediario) {
        IdIntermediario = idIntermediario;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String marca) {
        Marca = marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String modelo) {
        Modelo = modelo;
    }

    public String getSistemaOperativo() {
        return SistemaOperativo;
    }

    public void setSistemaOperativo(String sistemaOperativo) {
        SistemaOperativo = sistemaOperativo;
    }

    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public String getNumeroSerie() {
        return NumeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        NumeroSerie = numeroSerie;
    }

    public String getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        FechaCreacion = fechaCreacion;
    }

    public String getFechaUltimaSincronizacion() {
        return FechaUltimaSincronizacion;
    }

    public void setFechaUltimaSincronizacion(String fechaUltimaSincronizacion) {
        FechaUltimaSincronizacion = fechaUltimaSincronizacion;
    }

    public int getFlagNuevaMigracionCartera() {
        return FlagNuevaMigracionCartera;
    }

    public void setFlagNuevaMigracionCartera(int flagNuevaMigracionCartera) {
        FlagNuevaMigracionCartera = flagNuevaMigracionCartera;
    }

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDDISPOSITIVO:
                return IdDispositivo;
            case CO_IDINTERMEDIARIO:
                return IdIntermediario;
            case CO_MARCA:
                return Marca;
            case CO_MODELO:
                return Modelo;
            case CO_SISTEMAOPERATIVO:
                return SistemaOperativo;
            case CO_MAC:
                return MAC;
            case CO_NUMEROSERIE:
                return NumeroSerie;
            case CO_FECHACREACION:
                return FechaCreacion;
            case CO_FECHAULTIMASINCRONIZACION:
                return FechaUltimaSincronizacion;
            case CO_FLAGNUEVAMIGRACIONCARTERA:
                return FlagNuevaMigracionCartera;
        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDDISPOSITIVO:
                IdDispositivo = (int) object;
                break;
            case CO_IDINTERMEDIARIO:
                IdIntermediario = (int) object;
                break;
            case CO_MARCA:
                Marca = (String) object;
                break;
            case CO_MODELO:
                Modelo = (String) object;
                break;
            case CO_SISTEMAOPERATIVO:
                SistemaOperativo = (String) object;
                break;
            case CO_MAC:
                MAC = (String) object;
                break;
            case CO_NUMEROSERIE:
                NumeroSerie = (String) object;
                break;
            case CO_FECHACREACION:
                FechaCreacion = (String) object;
                break;
            case CO_FECHAULTIMASINCRONIZACION:
                FechaUltimaSincronizacion = (String) object;
                break;
            case CO_FLAGNUEVAMIGRACIONCARTERA:
                FlagNuevaMigracionCartera = (int) object;
                break;
        }
    }

    public static final String CN_IDDISPOSITIVO = "IDDISPOSITIVO";
    public static final String CT_IDDISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDDISPOSITIVO = 1;

    public static final String CN_IDINTERMEDIARIO = "IDINTERMEDIARIO";
    public static final String CT_IDINTERMEDIARIO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDINTERMEDIARIO = 2;

    public static final String CN_MARCA = "MARCA";
    public static final String CT_MARCA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_MARCA = 3;

    public static final String CN_MODELO = "MODELO";
    public static final String CT_MODELO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_MODELO = 4;

    public static final String CN_SISTEMAOPERATIVO = "SISTEMAOPERATIVO";
    public static final String CT_SISTEMAOPERATIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_SISTEMAOPERATIVO = 5;

    public static final String CN_MAC = "MAC";
    public static final String CT_MAC = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_MAC = 6;

    public static final String CN_NUMEROSERIE = "NUMEROSERIE";
    public static final String CT_NUMEROSERIE = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NUMEROSERIE = 7;

    public static final String CN_FECHACREACION = "FECHACREACION";
    public static final String CT_FECHACREACION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACREACION = 8;

    public static final String CN_FECHAULTIMASINCRONIZACION = "FECHAULTIMASINCRONIZACION";
    public static final String CT_FECHAULTIMASINCRONIZACION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAULTIMASINCRONIZACION = 9;

    public static final String CN_FLAGNUEVAMIGRACIONCARTERA = "FLAGNUEVAMIGRACIONCARTERA";
    public static final String CT_FLAGNUEVAMIGRACIONCARTERA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGNUEVAMIGRACIONCARTERA = 10;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_DISPOSITIVO, new OrganizateEntityFactory())
            .addColumn(CN_IDDISPOSITIVO, CT_IDDISPOSITIVO, CO_IDDISPOSITIVO)
            .addColumn(CN_IDINTERMEDIARIO, CT_IDINTERMEDIARIO, CO_IDINTERMEDIARIO)
            .addColumn(CN_MARCA, CT_MARCA, CO_MARCA)
            .addColumn(CN_MODELO, CT_MODELO, CO_MODELO)
            .addColumn(CN_SISTEMAOPERATIVO, CT_SISTEMAOPERATIVO, CO_SISTEMAOPERATIVO)
            .addColumn(CN_MAC, CT_MAC, CO_MAC)
            .addColumn(CN_NUMEROSERIE, CT_NUMEROSERIE, CO_NUMEROSERIE)
            .addColumn(CN_FECHACREACION, CT_FECHACREACION, CO_FECHACREACION)
            .addColumn(CN_FECHAULTIMASINCRONIZACION, CT_FECHAULTIMASINCRONIZACION, CO_FECHAULTIMASINCRONIZACION)
            .addColumn(CN_FLAGNUEVAMIGRACIONCARTERA, CT_FLAGNUEVAMIGRACIONCARTERA, CO_FLAGNUEVAMIGRACIONCARTERA);

}
