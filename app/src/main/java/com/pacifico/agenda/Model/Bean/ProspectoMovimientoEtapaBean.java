package com.pacifico.agenda.Model.Bean;


import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class ProspectoMovimientoEtapaBean extends Entity {

    private int IdMovimiento;
    private int IdMovimientoDispositivo;
    private int IdProspecto;
    private int IdProspectoDispositivo;
    private int CodigoEtapa;
    private int CodigoEstado;
    private String FechaMovimientoEtapaDispositivo;
    private int FlagEnviado;

    public ProspectoMovimientoEtapaBean(){
        IdMovimiento = -1;
        IdMovimientoDispositivo = -1;
        IdProspecto = -1;
        IdProspectoDispositivo = -1;
        CodigoEtapa = -1;
        CodigoEstado = -1;
        FechaMovimientoEtapaDispositivo = null;
        FlagEnviado = -1;
    }

    public int getIdMovimiento() {
        return IdMovimiento;
    }

    public void setIdMovimiento(int idMovimiento) {
        IdMovimiento = idMovimiento;
    }

    public int getIdMovimientoDispositivo() {
        return IdMovimientoDispositivo;
    }

    public void setIdMovimientoDispositivo(int idMovimientoDispositivo) {
        IdMovimientoDispositivo = idMovimientoDispositivo;
    }

    public int getIdProspecto() {
        return IdProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        IdProspecto = idProspecto;
    }

    public int getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public int getCodigoEtapa() {
        return CodigoEtapa;
    }

    public void setCodigoEtapa(int codigoEtapa) {
        CodigoEtapa = codigoEtapa;
    }

    public int getCodigoEstado() {
        return CodigoEstado;
    }

    public void setCodigoEstado(int codigoEstado) {
        CodigoEstado = codigoEstado;
    }

    public String getFechaMovimientoEtapaDispositivo() {
        return FechaMovimientoEtapaDispositivo;
    }

    public void setFechaMovimientoEtapaDispositivo(String fechaMovimientoEtapaDispositivo) {
        FechaMovimientoEtapaDispositivo = fechaMovimientoEtapaDispositivo;
    }

    public int getFlagEnviado() {
        return FlagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        FlagEnviado = flagEnviado;
    }

    /// Persistencia
    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDMOVIMIENTO:
                return IdMovimiento;

            case CO_IDMOVIMIENTODISPOSITIVO:
                return IdMovimientoDispositivo;

            case CO_IDPROSPECTO:
                return IdProspecto;

            case CO_IDPROSPECTODISPOSITIVO:
                return IdProspectoDispositivo;

            case CO_CODIGOETAPA:
                return CodigoEtapa;

            case CO_CODIGOESTADO:
                return CodigoEstado;

            case CO_FECHAMOVIMIENTOETAPADISPOSITIVO:
                return FechaMovimientoEtapaDispositivo;

            case CO_FLAGENVIADO:
                return FlagEnviado;

        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDMOVIMIENTO:
                IdMovimiento = (int) object;
                break;
            case CO_IDMOVIMIENTODISPOSITIVO:
                IdMovimientoDispositivo = (int) object;
                break;
            case CO_IDPROSPECTO:
                IdProspecto= (int) object;
                break;
            case CO_IDPROSPECTODISPOSITIVO:
                IdProspectoDispositivo = (int) object;
                break;
            case CO_CODIGOETAPA:
                CodigoEtapa= (int) object;
                break;
            case CO_CODIGOESTADO:
                CodigoEstado = (int) object;
                break;
            case CO_FECHAMOVIMIENTOETAPADISPOSITIVO:
                FechaMovimientoEtapaDispositivo = (String) object;
                break;
            case CO_FLAGENVIADO:
                FlagEnviado = (int) object;
                break;
        }
    }

    public static final String CN_IDMOVIMIENTO = "IDMOVIMIENTO";
    public static final String CT_IDMOVIMIENTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDMOVIMIENTO = 1;

    public static final String CN_IDMOVIMIENTODISPOSITIVO = "IDMOVIMIENTODISPOSITIVO";
    public static final String CT_IDMOVIMIENTODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDMOVIMIENTODISPOSITIVO = 2;

    public static final String CN_IDPROSPECTO = "IDPROSPECTO";
    public static final String CT_IDPROSPECTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTO = 3;

    public static final String CN_IDPROSPECTODISPOSITIVO = "IDPROSPECTODISPOSITIVO";
    public static final String CT_IDPROSPECTODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTODISPOSITIVO = 4;

    public static final String CN_CODIGOETAPA = "CODIGOETAPA";
    public static final String CT_CODIGOETAPA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOETAPA = 5;

    public static final String CN_CODIGOESTADO = "CODIGOESTADO";
    public static final String CT_CODIGOESTADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOESTADO = 6;

    public static final String CN_FECHAMOVIMIENTOETAPADISPOSITIVO = "FECHAMOVIMIENTOETAPADISPOSITIVO";
    public static final String CT_FECHAMOVIMIENTOETAPADISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAMOVIMIENTOETAPADISPOSITIVO = 7;

    public static final String CN_FLAGENVIADO = "FLAGENVIADO";
    public static final String CT_FLAGENVIADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIADO = 8;


    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_PROSPECTO_MOVIMIENTO_ETAPA, new OrganizateEntityFactory())
            .addColumn(CN_IDMOVIMIENTO, CT_IDMOVIMIENTO, CO_IDMOVIMIENTO)
            .addColumn(CN_IDMOVIMIENTODISPOSITIVO, CT_IDMOVIMIENTODISPOSITIVO, CO_IDMOVIMIENTODISPOSITIVO)
            .addColumn(CN_IDPROSPECTO, CT_IDPROSPECTO, CO_IDPROSPECTO)
            .addColumn(CN_IDPROSPECTODISPOSITIVO, CT_IDPROSPECTODISPOSITIVO, CO_IDPROSPECTODISPOSITIVO)
            .addColumn(CN_CODIGOETAPA, CT_CODIGOETAPA, CO_CODIGOETAPA)
            .addColumn(CN_CODIGOESTADO, CT_CODIGOESTADO, CO_CODIGOESTADO)
            .addColumn(CN_FECHAMOVIMIENTOETAPADISPOSITIVO, CT_FECHAMOVIMIENTOETAPADISPOSITIVO, CO_FECHAMOVIMIENTOETAPADISPOSITIVO)
            .addColumn(CN_FLAGENVIADO, CT_FLAGENVIADO, CO_FLAGENVIADO);

}
