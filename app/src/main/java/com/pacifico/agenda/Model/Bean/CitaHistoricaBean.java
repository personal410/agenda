package com.pacifico.agenda.Model.Bean;


import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

import java.util.ArrayList;

/**
 * Created by Joel on 31/05/2016.
 */
public class CitaHistoricaBean extends Entity {


    private int IdCita;
    private int IdProspecto;
    private int IdProspectoDispositivo;
    private int IdCitaDispositivo;
    private int NumeroEntrevista;
    private int CodigoEstado;
    private int CodigoResultado;
    private String FechaCita;
    private String HoraInicio;
    private String HoraFin;
    private String Ubicacion;
    private String ReferenciaUbicacion;
    private int FlagInvitadoGU;
    private int FlagInvitadoGA;
    //private String RecordatorioLlamada;
    private int AlertaMinutosAntes;
    private int CantidadVI;
    private double PrimaTargetVI;
    private int CantidadAP;
    private double PrimaTargetAP;
    private int CodigoIntermediarioCreacion;
    private int CodigoIntermediarioModificacion;
    private int CodigoEtapaProspecto;
    private String FechaCreacionDispositivo;
    private String FechaModificacionDispositivo;
    private int FlagEnviado;


    //Lista de Recordatorios de llamada (tiene su propia tabla)
    private ArrayList<RecordatorioLlamadaBean> listaRecordatorioLLamada;

    //private String Notas;

    public CitaHistoricaBean(){
        IdCita = -1;
        IdProspecto = -1;
        IdProspectoDispositivo = -1;
        IdCitaDispositivo = -1;
        NumeroEntrevista = -1;
        CodigoEstado = -1;
        CodigoResultado = -1;
        /*FechaCita;
        HoraInicio;
        HoraFin;
        Ubicacion;
        ReferenciaUbicacion;*/
        FlagInvitadoGU = -1;
        FlagInvitadoGA = -1;
        AlertaMinutosAntes = -1;
        CantidadVI = -1;
        PrimaTargetVI = -1;
        CantidadAP = -1;
        PrimaTargetAP = -1;
        CodigoIntermediarioCreacion = -1;
        CodigoIntermediarioModificacion = -1;
        CodigoEtapaProspecto = -1;
        //FechaCreacionDispositivo;
        //FechaModificacionDispositivo;
        FlagEnviado = -1;
        listaRecordatorioLLamada = new ArrayList<RecordatorioLlamadaBean>();
    }


    public int getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public int getIdCita() {
        return IdCita;
    }

    public void setIdCita(int idCita) {
        IdCita = idCita;
    }

    public int getIdProspecto() {
        return IdProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        IdProspecto = idProspecto;
    }



    public int getIdCitaDispositivo() {
        return IdCitaDispositivo;
    }

    public void setIdCitaDispositivo(int idCitaDispositivo) {
        IdCitaDispositivo = idCitaDispositivo;
    }

    public int getNumeroEntrevista() {
        return NumeroEntrevista;
    }

    public void setNumeroEntrevista(int numeroEntrevista) {
        NumeroEntrevista = numeroEntrevista;
    }

    public int getCodigoEstado() {
        return CodigoEstado;
    }

    public void setCodigoEstado(int codigoEstado) {
        CodigoEstado = codigoEstado;
    }

    public int getCodigoResultado() {
        return CodigoResultado;
    }

    public void setCodigoResultado(int codigoResultado) {
        CodigoResultado = codigoResultado;
    }

    public String getFechaCita() {
        return FechaCita;
    }

    public void setFechaCita(String fechaCita) {
        FechaCita = fechaCita;
    }

    public String getHoraInicio() {
        return HoraInicio;
    }

    public void setHoraInicio(String horaInicio) {
        HoraInicio = horaInicio;
    }

    public String getHoraFin() {
        return HoraFin;
    }

    public void setHoraFin(String horaFin) {
        HoraFin = horaFin;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        Ubicacion = ubicacion;
    }

    public String getReferenciaUbicacion() {
        return ReferenciaUbicacion;
    }

    public void setReferenciaUbicacion(String referenciaUbicacion) {
        ReferenciaUbicacion = referenciaUbicacion;
    }

    public int getFlagInvitadoGU() {
        return FlagInvitadoGU;
    }

    public void setFlagInvitadoGU(int flagInvitadoGU) {
        FlagInvitadoGU = flagInvitadoGU;
    }

    public int getFlagInvitadoGA() {
        return FlagInvitadoGA;
    }

    public void setFlagInvitadoGA(int flagInvitadoGA) {
        FlagInvitadoGA = flagInvitadoGA;
    }

    /*public String getRecordatorioLlamada() {
        return RecordatorioLlamada;
    }

    public void setRecordatorioLlamada(String recordatorioLlamada) {
        RecordatorioLlamada = recordatorioLlamada;
    }*/

    public int getAlertaMinutosAntes() {
        return AlertaMinutosAntes;
    }

    public void setAlertaMinutosAntes(int alertaMinutosAntes) {
        AlertaMinutosAntes = alertaMinutosAntes;
    }

    public int getCantidadVI() {
        return CantidadVI;
    }

    public void setCantidadVI(int cantidadVI) {
        CantidadVI = cantidadVI;
    }

    public double getPrimaTargetVI() {
        return PrimaTargetVI;
    }

    public void setPrimaTargetVI(double primaTargetVI) {
        PrimaTargetVI = primaTargetVI;
    }

    public int getCantidadAP() {
        return CantidadAP;
    }

    public void setCantidadAP(int cantidadAP) {
        CantidadAP = cantidadAP;
    }

    public double getPrimaTargetAP() {
        return PrimaTargetAP;
    }

    public void setPrimaTargetAP(double primaTargetAP) {
        PrimaTargetAP = primaTargetAP;
    }

    public int getCodigoIntermediarioCreacion() {
        return CodigoIntermediarioCreacion;
    }

    public void setCodigoIntermediarioCreacion(int codigoIntermediarioCreacion) {
        CodigoIntermediarioCreacion = codigoIntermediarioCreacion;
    }

    public int getCodigoIntermediarioModificacion() {
        return CodigoIntermediarioModificacion;
    }

    public void setCodigoIntermediarioModificacion(int codigoIntermediarioModificacion) {
        CodigoIntermediarioModificacion = codigoIntermediarioModificacion;
    }

    public int getCodigoEtapaProspecto() {
        return CodigoEtapaProspecto;
    }

    public void setCodigoEtapaProspecto(int codigoEtapaProspecto) {
        CodigoEtapaProspecto = codigoEtapaProspecto;
    }

    public String getFechaCreacionDispositivo() {
        return FechaCreacionDispositivo;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return FechaModificacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public int getFlagEnviado() {
        return FlagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        FlagEnviado = flagEnviado;
    }


    /*public String getNotas() {
        return Notas;
    }

    public void setNotas(String notas) {
        Notas = notas;
    }*/

    public ArrayList<RecordatorioLlamadaBean> getListaRecordatorioLLamada() {
        return listaRecordatorioLLamada;
    }

    public void setListaRecordatorioLLamada(ArrayList<RecordatorioLlamadaBean> listaRecordatorioLLamada) {
        this.listaRecordatorioLLamada = listaRecordatorioLLamada;
    }


    ///  PERSISTENCIA

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDCITA:
                return IdCita;

            case CO_IDPROSPECTO:
                return IdProspecto;

            case CO_IDPROSPECTODISPOSITIVO:
                return IdProspectoDispositivo;

            case CO_IDCITADISPOSITIVO:
                return IdCitaDispositivo;

            case CO_NUMEROENTREVISTA:
                return NumeroEntrevista;

            case CO_CODIGOESTADO:
                return CodigoEstado;

            case CO_CODIGORESULTADO:
                return CodigoResultado;

            case CO_FECHACITA:
                return FechaCita;

            case CO_HORAINICIO:
                return HoraInicio;

            case CO_HORAFIN:
                return HoraFin;

            case CO_UBICACION:
                return Ubicacion;

            case CO_REFERENCIA_UBICACION:
                return ReferenciaUbicacion;

            case CO_FLAGINVITADOGU:
                return FlagInvitadoGU;

            case CO_FLAGINVITADOGA:
                return FlagInvitadoGA;

            case CO_ALERTAMINUTOSANTES:
                return AlertaMinutosAntes;

            case CO_CANTIDADVI:
                return CantidadVI;

            case CO_PRIMATARGETVI:
                return PrimaTargetVI;

            case CO_CANTIDADAP:
                return CantidadAP;

            case CO_PRIMATARGETAP:
                return PrimaTargetAP;

            case CO_CODIGOINTERMEDIARIOCREACION:
                return CodigoIntermediarioCreacion;

            case CO_CODIGOINTERMEDIARIOMODIFICACION:
                return CodigoIntermediarioModificacion;

            case CO_CODIGOETAPAPROSPECTO:
                return CodigoEtapaProspecto;

            case CO_FECHACREACIONDISPOSITIVO:
                return FechaCreacionDispositivo;

            case CO_FECHAMODIFICACIONDISPOSITIVO:
                return FechaModificacionDispositivo;

            case CO_FLAGENVIADO:
                return FlagEnviado;

            /*case CO_NOTAS:
                return Notas;*/



        }
        return null;
    }



    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDCITA:
                IdCita = (int) object;
                break;
            case CO_IDPROSPECTO:
                IdProspecto = (int) object;
                break;
            case CO_IDPROSPECTODISPOSITIVO:
                IdProspectoDispositivo = (int) object;
                break;
            case CO_IDCITADISPOSITIVO:
                IdCitaDispositivo = (int) object;
                break;
            case CO_NUMEROENTREVISTA:
                NumeroEntrevista = (int) object;
                break;
            case CO_CODIGOESTADO:
                CodigoEstado = (int) object;
                break;
            case CO_CODIGORESULTADO:
                CodigoResultado = (int) object;
                break;
            case CO_FECHACITA:
                FechaCita = (String) object;
                break;
            case CO_HORAINICIO:
                HoraInicio = (String) object;
                break;
            case CO_HORAFIN:
                HoraFin = (String) object;
                break;
            case CO_UBICACION:
                Ubicacion = (String) object;
                break;
            case CO_FLAGINVITADOGU:
                FlagInvitadoGU = (int) object;
                break;
            case CO_FLAGINVITADOGA:
                FlagInvitadoGA = (int) object;
                break;
            /*case CO_RECORDATORIOLLAMADA:
                RecordatorioLlamada = (String) object;
                break;*/
            case CO_ALERTAMINUTOSANTES:
                AlertaMinutosAntes = (int) object;
                break;
            case CO_CANTIDADVI:
                CantidadVI = (int) object;
                break;
            case CO_PRIMATARGETVI:
                PrimaTargetVI = (double) object;
                break;
            case CO_CANTIDADAP:
                CantidadAP = (int) object;
                break;
            case CO_PRIMATARGETAP:
                PrimaTargetAP = (double) object;
                break;
            case CO_CODIGOINTERMEDIARIOCREACION:
                CodigoIntermediarioCreacion = (int) object;
                break;
            case CO_CODIGOINTERMEDIARIOMODIFICACION:
                CodigoIntermediarioModificacion = (int) object;
                break;
            case CO_CODIGOETAPAPROSPECTO:
                CodigoEtapaProspecto = (int) object;
                break;
            case CO_FECHACREACIONDISPOSITIVO:
                FechaCreacionDispositivo = (String) object;
                break;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                FechaModificacionDispositivo = (String) object;
                break;
            case CO_FLAGENVIADO:
                FlagEnviado = (int) object;
                break;
            /*case CO_NOTAS:
                Notas = (String) object;*/
            case CO_REFERENCIA_UBICACION:
                ReferenciaUbicacion = (String) object;
                break;
        }
    }

    public static final String CN_IDCITA = "IDCITA";
    public static final String CT_IDCITA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDCITA = 1;

    public static final String CN_IDPROSPECTO = "IDPROSPECTO";
    public static final String CT_IDPROSPECTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTO = 2;

    public static final String CN_IDPROSPECTODISPOSITIVO = "IDPROSPECTODISPOSITIVO";
    public static final String CT_IDPROSPECTODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTODISPOSITIVO = 3;

    public static final String CN_IDCITADISPOSITIVO = "IDCITADISPOSITIVO";
    public static final String CT_IDCITADISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDCITADISPOSITIVO = 4;

    public static final String CN_NUMEROENTREVISTA = "NUMEROENTREVISTA";
    public static final String CT_NUMEROENTREVISTA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_NUMEROENTREVISTA = 5;

    public static final String CN_CODIGOESTADO = "CODIGOESTADO";
    public static final String CT_CODIGOESTADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOESTADO = 6;

    public static final String CN_CODIGORESULTADO = "CODIGORESULTADO";
    public static final String CT_CODIGORESULTADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGORESULTADO = 7;

    public static final String CN_FECHACITA = "FECHACITA";
    public static final String CT_FECHACITA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACITA = 8;

    public static final String CN_HORAINICIO = "HORAINICIO";
    public static final String CT_HORAINICIO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_HORAINICIO = 9;

    public static final String CN_HORAFIN = "HORAFIN";
    public static final String CT_HORAFIN = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_HORAFIN = 10;

    public static final String CN_UBICACION = "UBICACION";
    public static final String CT_UBICACION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_UBICACION = 11;

    public static final String CN_REFERENCIA_UBICACION = "REFERENCIA";
    public static final String CT_REFERENCIA_UBICACION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_REFERENCIA_UBICACION = 12;

    public static final String CN_FLAGINVITADOGU = "FLAGINVITADOGU";
    public static final String CT_FLAGINVITADOGU = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGINVITADOGU = 13;

    public static final String CN_FLAGINVITADOGA = "FLAGINVITADOGA";
    public static final String CT_FLAGINVITADOGA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGINVITADOGA = 14;

    /*public static final String CN_RECORDATORIOLLAMADA = "RECORDATORIOLLAMADA";
    public static final String CT_RECORDATORIOLLAMADA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_RECORDATORIOLLAMADA = 12;*/

    public static final String CN_ALERTAMINUTOSANTES = "ALERTAMINUTOSANTES";
    public static final String CT_ALERTAMINUTOSANTES = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_ALERTAMINUTOSANTES = 15;

    public static final String CN_CANTIDADVI = "ALERTAMINUTOSANTES";
    public static final String CT_CANTIDADVI = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CANTIDADVI = 16;

    public static final String CN_PRIMATARGETVI = "ALERTAMINUTOSANTES";
    public static final String CT_PRIMATARGETVI = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_PRIMATARGETVI = 17;

    public static final String CN_CANTIDADAP = "ALERTAMINUTOSANTES";
    public static final String CT_CANTIDADAP = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CANTIDADAP = 18;

    public static final String CN_PRIMATARGETAP = "ALERTAMINUTOSANTES";
    public static final String CT_PRIMATARGETAP = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_PRIMATARGETAP = 19;

    public static final String CN_CODIGOINTERMEDIARIOCREACION = "CODIGOINTERMEDIARIOCREACION";
    public static final String CT_CODIGOINTERMEDIARIOCREACION = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOINTERMEDIARIOCREACION = 20;

    public static final String CN_CODIGOINTERMEDIARIOMODIFICACION = "CODIGOINTERMEDIARIOMODIFICACION";
    public static final String CT_CODIGOINTERMEDIARIOMODIFICACION = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOINTERMEDIARIOMODIFICACION = 21;

    public static final String CN_CODIGOETAPAPROSPECTO = "CODIGOETAPAPROSPECTO";
    public static final String CT_CODIGOETAPAPROSPECTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOETAPAPROSPECTO = 22;

    public static final String CN_FECHACREACIONDISPOSITIVO = "FECHACREACIONDISPOSITIVO";
    public static final String CT_FECHACREACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACREACIONDISPOSITIVO = 23;

    public static final String CN_FECHAMODIFICACIONDISPOSITIVO = "FECHAMODIFICACIONDISPOSITIVO";
    public static final String CT_FECHAMODIFICACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAMODIFICACIONDISPOSITIVO = 24;

    public static final String CN_FLAGENVIADO = "FLAGENVIADO";
    public static final String CT_FLAGENVIADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIADO = 25;

    /*public static final String CN_NOTAS = "NOTAS";
    public static final String CT_NOTAS = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOTAS = 21;*/

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_CITA_HISTORICA, new OrganizateEntityFactory())
            .addColumn(CN_IDCITA, CT_IDCITA, CO_IDCITA)
            .addColumn(CN_IDPROSPECTO, CT_IDPROSPECTO, CO_IDPROSPECTO)
            .addColumn(CN_IDPROSPECTODISPOSITIVO, CT_IDPROSPECTODISPOSITIVO, CO_IDPROSPECTODISPOSITIVO)
            .addColumn(CN_IDCITADISPOSITIVO, CT_IDCITADISPOSITIVO, CO_IDCITADISPOSITIVO)
            .addColumn(CN_NUMEROENTREVISTA, CT_NUMEROENTREVISTA, CO_NUMEROENTREVISTA)
            .addColumn(CN_CODIGOESTADO, CT_CODIGOESTADO, CO_CODIGOESTADO)
            .addColumn(CN_CODIGORESULTADO, CT_CODIGORESULTADO, CO_CODIGORESULTADO)
            .addColumn(CN_FECHACITA, CT_FECHACITA, CO_FECHACITA)
            .addColumn(CN_HORAINICIO, CT_HORAINICIO, CO_HORAINICIO)
            .addColumn(CN_HORAFIN, CT_HORAFIN, CO_HORAFIN)
            .addColumn(CN_UBICACION, CT_UBICACION, CO_UBICACION)
            .addColumn(CN_REFERENCIA_UBICACION, CT_REFERENCIA_UBICACION, CO_REFERENCIA_UBICACION)
            .addColumn(CN_FLAGINVITADOGU, CT_FLAGINVITADOGU, CO_FLAGINVITADOGU)
            .addColumn(CN_FLAGINVITADOGA, CT_FLAGINVITADOGA, CO_FLAGINVITADOGA)
            //.addColumn(CN_RECORDATORIOLLAMADA, CT_RECORDATORIOLLAMADA, CO_RECORDATORIOLLAMADA)
            .addColumn(CN_ALERTAMINUTOSANTES, CT_ALERTAMINUTOSANTES, CO_ALERTAMINUTOSANTES)
            .addColumn(CN_CANTIDADVI, CT_CANTIDADVI, CO_CANTIDADVI)
            .addColumn(CN_PRIMATARGETVI, CT_PRIMATARGETVI, CO_PRIMATARGETVI)
            .addColumn(CN_CANTIDADAP, CT_CANTIDADAP, CO_CANTIDADAP)
            .addColumn(CN_PRIMATARGETAP, CT_PRIMATARGETAP, CO_PRIMATARGETAP)
            .addColumn(CN_CODIGOINTERMEDIARIOCREACION, CT_CODIGOINTERMEDIARIOCREACION, CO_CODIGOINTERMEDIARIOCREACION)
            .addColumn(CN_CODIGOINTERMEDIARIOMODIFICACION, CT_CODIGOINTERMEDIARIOMODIFICACION, CO_CODIGOINTERMEDIARIOMODIFICACION)
            .addColumn(CN_CODIGOETAPAPROSPECTO, CT_CODIGOETAPAPROSPECTO, CO_CODIGOETAPAPROSPECTO)
            .addColumn(CN_FECHACREACIONDISPOSITIVO, CT_FECHACREACIONDISPOSITIVO, CO_FECHACREACIONDISPOSITIVO)
            .addColumn(CN_FECHAMODIFICACIONDISPOSITIVO, CT_FECHAMODIFICACIONDISPOSITIVO, CO_FECHAMODIFICACIONDISPOSITIVO)
            .addColumn(CN_FLAGENVIADO, CT_FLAGENVIADO, CO_FLAGENVIADO);
    //.addColumn(CN_NOTAS, CT_NOTAS, CO_NOTAS)
}
