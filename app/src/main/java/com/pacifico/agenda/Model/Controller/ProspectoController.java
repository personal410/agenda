package com.pacifico.agenda.Model.Controller;

import android.database.Cursor;
import android.util.Log;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Activity.AgendaApplication;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoDatosReferidoBean;
import com.pacifico.agenda.Model.Bean.TablaIdentificadorBean;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;

import java.util.ArrayList;

/**
 * Created by Joel on 03/06/2016.
 */
public class ProspectoController{
    public static void guardarProspecto(ProspectoBean prospectoBean){
        ProspectoBean.tableHelper.insertEntity(prospectoBean);
    }

    public static boolean guardarProspectoValidacion(ProspectoBean prospectoBean) { // validado

        //int lastid = TablaIdentificadorController.ObtenerUtimoIdTabla(DatabaseConstants.TBL_PROSPECTO);
        TablaIdentificadorBean identificadorProspectoBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_PROSPECTO);

        prospectoBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
        prospectoBean.setIdProspectoDispositivo(identificadorProspectoBean.getIdentity()+1);
        long row = ProspectoBean.tableHelper.insertEntity(prospectoBean);

        if (row != -1) // SI ES -1, HUBO UN PROBLEMA
        {
            identificadorProspectoBean.setIdentity(identificadorProspectoBean.getIdentity()+1);
            TablaIdentificadorController.actualizarTablaIdentificador(identificadorProspectoBean);
        }
        /////

        if (row !=-1)
            return true;
        else
            return false;
    }

    public static boolean actualizarProspectoxIDDispositivoValidacion(ProspectoBean prospectoBean) {
        prospectoBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());

        String[] parameters = new String[]{String.valueOf(prospectoBean.getIdProspectoDispositivo())};
        long filas_afectadas = ProspectoBean.tableHelper.updateEntity(prospectoBean,"IDPROSPECTODISPOSITIVO = ?", parameters);
        if (filas_afectadas > 0)
            return true;
        else
            return false;
    }
    public static int obtenerMaximoIdProspectoDispositivo(){
        int idProspectoDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDPROSPECTODISPOSITIVO) FROM " + DatabaseConstants.TBL_PROSPECTO;
        Cursor cursor = AgendaApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idProspectoDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idProspectoDispositivoMax;
    }


    public static ArrayList<ProspectoBean> obtenerProspectoSinProspectoDispositivo(){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTODISPOSITIVO = -1", null);
        ArrayList<ProspectoBean> arrProspectosFinal = new ArrayList<>();
        for(Entity entity : arrProspectos){
            arrProspectosFinal.add((ProspectoBean)entity);
        }
        return arrProspectosFinal;
    }

    public static void guardarListaProspectos(ArrayList<ProspectoBean> listaProspectoBean) {

        ArrayList<ProspectoBean> prospectosConReferente = new ArrayList<ProspectoBean>();

        if(listaProspectoBean != null){
            for(ProspectoBean prospectoBean : listaProspectoBean){
                if (prospectoBean.getIdReferenciador() != -1)
                    prospectosConReferente.add(prospectoBean);

                guardarProspecto(prospectoBean);
            }
        }

        // ACTUALIZAR LOS ID REFERIDOS (se inserta aparte por que es la misma entidad)
        for(ProspectoBean prospectoBean : prospectosConReferente){
            ProspectoBean prospectoReferente = obtenerProspectoPorIdProspecto(prospectoBean.getIdReferenciador());
            prospectoBean.setIdReferenciadorDispositivo(prospectoReferente.getIdProspectoDispositivo());

            actualizarProspecto(prospectoBean);
        }
    }

    public static ProspectoBean getProspectoBeanByIdProspecto(int IdProspecto){
        String[] parameters = new String[]{String.valueOf(IdProspecto)};
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTO = ?", parameters);
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }
    public static ProspectoBean getProspectoBeanByIdProspectoDispositivo(int IdProspectoDispositivo){
        String[] parameters = new String[]{String.valueOf(IdProspectoDispositivo)};
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTODISPOSITIVO = ?", parameters);
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }

    public static void actualizarProspectoSinProspectoDispositivo(ProspectoBean prospectoBean){
        String[] parameters = new String[]{Integer.toString(prospectoBean.getIdProspecto())};
        ProspectoBean.tableHelper.updateEntity(prospectoBean, "IDPROSPECTO = ?", parameters);
    }

    public static void actualizarProspectoxIDDispositivo(ProspectoBean prospectoBean) {
        String[] parameters = new String[]{String.valueOf(prospectoBean.getIdProspectoDispositivo())};
        ProspectoBean.tableHelper.updateEntity(prospectoBean,"IDPROSPECTODISPOSITIVO = ?", parameters);
    }

    public static ArrayList<ProspectoBean> getCumpleanosDeLaSemana(String fechaInicio, String fechaFin)
    {
        // se quita el año porque se comprará solo mes y dia
        fechaInicio = Util.quitarAnio(fechaInicio);
        fechaFin = Util.quitarAnio(fechaFin);

        ArrayList<ProspectoBean> lista =  new ArrayList<>();

        String sql = String.format(
                "select  IDPROSPECTO" +
                        //" ,CODIGOINTERMEDIARIO " +
                        " ,IDPROSPECTODISPOSITIVO " +
                        " ,IDPROSPECTOEXTERNO " +
                        " ,NOMBRES " +
                        " ,APELLIDOPATERNO " +
                        " ,APELLIDOMATERNO " +
                        " ,FECHANACIMIENTO " +
                        " ,CODIGOTIPODOCUMENTO " +
                        " ,NUMERODOCUMENTO "+
                        " ,TELEFONOCELULAR "+
                        " ,TELEFONOFIJO " +
                        " ,TELEFONOADICIONAL " +
                        " ,CORREOELECTRONICO1 " +
                        " ,CORREOELECTRONICO2 "+
                        " ,IDREFERENCIADOR "+
                        " ,IDREFERENCIADORDISPOSITIVO "+
                " from "+ DatabaseConstants.TBL_PROSPECTO +
                        " where " +
                        //" FECHANACIMIENTO BETWEEN '%s' AND '%s' "+
                        " substr(FECHANACIMIENTO, 6, 5) BETWEEN '%s' AND '%s' "+
                        " ORDER BY FECHANACIMIENTO ASC",
                fechaInicio,
                fechaFin
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        ProspectoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new ProspectoBean();
                bean.setIdProspecto(cursor.getInt(0));
                //bean.setCodigoIntermediario(cursor.getInt(1));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setIdProspectoExterno(cursor.getInt(2));
                bean.setNombres(cursor.getString(3));
                bean.setApellidoPaterno(cursor.getString(4));
                bean.setApellidoMaterno(cursor.getString(5));
                bean.setFechaNacimiento(cursor.getString(6));
                bean.setCodigoTipoDocumento(cursor.getInt(7));
                bean.setNumeroDocumento(cursor.getString(8));
                bean.setTelefonoCelular(cursor.getString(9));
                bean.setTelefonoFijo(cursor.getString(10));
                bean.setTelefonoAdicional(cursor.getString(11));
                bean.setCorreoElectronico1(cursor.getString(12));
                bean.setCorreoElectronico2(cursor.getString(13));
                bean.setIdReferenciador(cursor.getInt(14));
                bean.setIdReferenciadorDispositivo(cursor.getInt(15));

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }

    /*public static ArrayList<ProspectoBean> ObtenerProspectosPrimeraEntrevista(String pNombre)
    {
        ArrayList<ProspectoBean> lista = new ArrayList<ProspectoBean>();
        String sql =
                "select "+ ProspectoBean.CN_IDPROSPECTO +", "+
                        ProspectoBean.CN_IDPROSPECTODISPOSITIVO +", "+
                        ProspectoBean.CN_NOMBRES +", "+
                        ProspectoBean.CN_APELLIDOPATERNO +", "+
                        ProspectoBean.CN_APELLIDOMATERNO +", "+
                        ProspectoBean.CN_CODIGOETAPA +" "+
                        " from "+DatabaseConstants.TBL_PROSPECTO+
                        " where" +
                        " ("+ ProspectoBean.CN_CODIGOETAPA +" ="+ Constantes.EtapaProspecto_Nuevo +
                        " or "+ ProspectoBean.CN_CODIGOETAPA +" ="+ Constantes.EtapaProspecto_PrimeraEntrevista +")"+
                        " and (" + ProspectoBean.CN_NOMBRES + " || "+ ProspectoBean.CN_APELLIDOPATERNO + " || "+ ProspectoBean.CN_APELLIDOMATERNO+ ") like '%" + pNombre +"%'" +
                        " and "+ProspectoBean.CN_CODIGOESTADO + " <> "+ Constantes.EstadoProspecto_CierreVenta+
                        " and "+ProspectoBean.CN_CODIGOESTADO + " <> "+ Constantes.EstadoProspecto_NoInteresado;

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                ProspectoBean bean = new ProspectoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoPaterno(cursor.getString(3));
                bean.setApellidoMaterno(cursor.getString(4));

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }*/

    public static ArrayList<ProspectoBean> filtroProspectosEntrevistaxNombre(String pNombre)
    {
        ArrayList<ProspectoBean> lista = new ArrayList<ProspectoBean>();
        String sql =
                "select  "+
                        ProspectoBean.CN_IDPROSPECTO +", "+
                        ProspectoBean.CN_IDPROSPECTODISPOSITIVO +", "+
                        ProspectoBean.CN_IDPROSPECTOEXTERNO +", "+
                        ProspectoBean.CN_NOMBRES +", "+
                        ProspectoBean.CN_APELLIDOPATERNO +", "+
                        ProspectoBean.CN_APELLIDOMATERNO +", "+
                        ProspectoBean.CN_CODIGOSEXO +", "+
                        ProspectoBean.CN_CODIGOESTADOCIVIL +", "+
                        ProspectoBean.CN_FECHANACIMIENTO +", "+
                        ProspectoBean.CN_CODIGORANGOEDAD +", "+
                        ProspectoBean.CN_CODIGORANGOINGRESO +", "+
                        ProspectoBean.CN_FLAGHIJO +", "+
                        ProspectoBean.CN_FLAGCONYUGE +", "+
                        ProspectoBean.CN_CODIGOTIPODOCUMENTO+", "+
                        ProspectoBean.CN_NUMERODOCUMENTO+", "+
                        ProspectoBean.CN_TELEFONOCELULAR+", "+
                        ProspectoBean.CN_TELEFONOFIJO+", "+
                        ProspectoBean.CN_TELEFONOADICIONAL+", "+
                        ProspectoBean.CN_CORREOELECTRONICO1+", "+
                        ProspectoBean.CN_CORREOELECTRONICO2+", "+
                        ProspectoBean.CN_CODIGONACIONALIDAD+", "+
                        ProspectoBean.CN_CONDICIONFUMADOR+", "+
                        ProspectoBean.CN_EMPRESA+", "+
                        ProspectoBean.CN_CARGO+", "+
                        ProspectoBean.CN_NOTA+", "+
                        ProspectoBean.CN_CODIGOETAPA+", "+
                        ProspectoBean.CN_CODIGOESTADO+", "+
                        ProspectoBean.CN_CODIGOFUENTE+", "+
                        ProspectoBean.CN_CODIGOENTORNO+", "+
                        ProspectoBean.CN_IDREFERENCIADOR+", "+
                        ProspectoBean.CN_FECHACREACIONDISPOSITIVO+", "+
                        ProspectoBean.CN_FECHAMODIFICACIONDISPOSITIVO+", "+
                        ProspectoBean.CN_ADICIONALTEXTO1+", "+
                        ProspectoBean.CN_ADICIONALNUMERICO1+", "+
                        ProspectoBean.CN_ADICIONALTEXTO2+", "+
                        ProspectoBean.CN_ADICIONALNUMERICO2+", "+
                        ProspectoBean.CN_IDREFERENCIADORDISPOSITIVO+", "+
                        ProspectoBean.CN_FLAGENVIADO+", "+
                        ProspectoBean.CN_FLAGENVIARADNDIGITAL+
                        " from "+DatabaseConstants.TBL_PROSPECTO+
                        " where" +
                        " (IFNULL(" + ProspectoBean.CN_NOMBRES + ",'') || IFNULL("+ ProspectoBean.CN_APELLIDOPATERNO + ",'') || IFNULL("+ ProspectoBean.CN_APELLIDOMATERNO+ ",'')) like '%" + pNombre +"%'" +
                        " and "+ProspectoBean.CN_CODIGOESTADO + " = "+ Constantes.EstadoProspecto_Nuevo;

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                ProspectoBean bean = new ProspectoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setIdProspectoExterno(cursor.getInt(2));
                bean.setNombres(cursor.getString(3));
                bean.setApellidoPaterno(cursor.getString(4));
                bean.setApellidoMaterno(cursor.getString(5));
                bean.setCodigoSexo(cursor.getInt(6));
                bean.setCodigoEstadoCivil(cursor.getInt(7));
                bean.setFechaNacimiento(cursor.getString(8));
                bean.setCodigoRangoEdad(cursor.getInt(9));
                bean.setCodigoRangoIngreso(cursor.getInt(10));
                bean.setFlagHijo(cursor.getInt(11));
                bean.setFlagConyuge(cursor.getInt(12));
                bean.setCodigoTipoDocumento(cursor.getInt(13));
                bean.setNumeroDocumento(cursor.getString(14));
                bean.setTelefonoCelular(cursor.getString(15));
                bean.setTelefonoFijo(cursor.getString(16));
                bean.setTelefonoAdicional(cursor.getString(17));
                bean.setCorreoElectronico1(cursor.getString(18));
                bean.setCorreoElectronico2(cursor.getString(19));
                bean.setCodigoNacionalidad(cursor.getInt(20));
                bean.setCondicionFumador(cursor.getInt(21));
                bean.setEmpresa(cursor.getString(22));
                bean.setCargo(cursor.getString(23));
                bean.setNota(cursor.getString(24));
                bean.setCodigoEtapa(cursor.getInt(25));
                bean.setCodigoEstado(cursor.getInt(26));
                bean.setCodigoFuente(cursor.getInt(27));
                bean.setCodigoEntorno(cursor.getInt(28));
                bean.setIdReferenciador(cursor.getInt(29));
                bean.setFechaCreacionDispositivo(cursor.getString(30));
                bean.setFechaModificacionDispositivo(cursor.getString(31));
                bean.setAdicionalTexto1(cursor.getString(32));
                bean.setAdicionalNumerico1(cursor.getDouble(33));
                bean.setAdicionalTexto2(cursor.getString(34));
                bean.setAdicionalNumerico2(cursor.getDouble(35));
                bean.setIdReferenciadorDispositivo(cursor.getInt(36));
                bean.setFlagEnviado(cursor.getInt(37));
                bean.setFlagEnviarADNDigital(cursor.getInt(38));
                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }

    public static ArrayList<ProspectoBean> filtroProspectosEntrevistaxNombre(String pNombre,int pEtapa)
    {
        String filtro_etapa = "";
        if (pEtapa == Constantes.EtapaProspecto_PrimeraEntrevista)
            filtro_etapa = " ("+ ProspectoBean.CN_CODIGOETAPA +" ="+ Constantes.EtapaProspecto_Nuevo + " or "+ ProspectoBean.CN_CODIGOETAPA +" ="+ Constantes.EtapaProspecto_PrimeraEntrevista +")";
        else if (pEtapa == Constantes.EtapaProspecto_SegundaEntrevista)
            filtro_etapa = " ("+ ProspectoBean.CN_CODIGOETAPA +" ="+ Constantes.EtapaProspecto_Nuevo + " or "+ ProspectoBean.CN_CODIGOETAPA +" ="+ Constantes.EtapaProspecto_SegundaEntrevista +")";
        else if (pEtapa == Constantes.EtapaProspecto_EntrevistaAdicional)
            filtro_etapa = " ("+ ProspectoBean.CN_CODIGOETAPA +" ="+ Constantes.EtapaProspecto_Nuevo + " or "+ ProspectoBean.CN_CODIGOETAPA +" ="+ Constantes.EtapaProspecto_EntrevistaAdicional +")";

        ArrayList<ProspectoBean> lista = new ArrayList<ProspectoBean>();
        String sql =
                "select "+
                        ProspectoBean.CN_IDPROSPECTO +", "+
                        ProspectoBean.CN_IDPROSPECTODISPOSITIVO +", "+
                        ProspectoBean.CN_IDPROSPECTOEXTERNO +", "+
                        ProspectoBean.CN_NOMBRES +", "+
                        ProspectoBean.CN_APELLIDOPATERNO +", "+
                        ProspectoBean.CN_APELLIDOMATERNO +", "+
                        ProspectoBean.CN_CODIGOSEXO +", "+
                        ProspectoBean.CN_CODIGOESTADOCIVIL +", "+
                        ProspectoBean.CN_FECHANACIMIENTO +", "+
                        ProspectoBean.CN_CODIGORANGOEDAD +", "+
                        ProspectoBean.CN_CODIGORANGOINGRESO +", "+
                        ProspectoBean.CN_FLAGHIJO +", "+
                        ProspectoBean.CN_FLAGCONYUGE +", "+
                        ProspectoBean.CN_CODIGOTIPODOCUMENTO+", "+
                        ProspectoBean.CN_NUMERODOCUMENTO+", "+
                        ProspectoBean.CN_TELEFONOCELULAR+", "+
                        ProspectoBean.CN_TELEFONOFIJO+", "+
                        ProspectoBean.CN_TELEFONOADICIONAL+", "+
                        ProspectoBean.CN_CORREOELECTRONICO1+", "+
                        ProspectoBean.CN_CORREOELECTRONICO2+", "+
                        ProspectoBean.CN_CODIGONACIONALIDAD+", "+
                        ProspectoBean.CN_CONDICIONFUMADOR+", "+
                        ProspectoBean.CN_EMPRESA+", "+
                        ProspectoBean.CN_CARGO+", "+
                        ProspectoBean.CN_NOTA+", "+
                        ProspectoBean.CN_CODIGOETAPA+", "+
                        ProspectoBean.CN_CODIGOESTADO+", "+
                        ProspectoBean.CN_CODIGOFUENTE+", "+
                        ProspectoBean.CN_CODIGOENTORNO+", "+
                        ProspectoBean.CN_IDREFERENCIADOR+", "+
                        ProspectoBean.CN_FECHACREACIONDISPOSITIVO+", "+
                        ProspectoBean.CN_FECHAMODIFICACIONDISPOSITIVO+", "+
                        ProspectoBean.CN_ADICIONALTEXTO1+", "+
                        ProspectoBean.CN_ADICIONALNUMERICO1+", "+
                        ProspectoBean.CN_ADICIONALTEXTO2+", "+
                        ProspectoBean.CN_ADICIONALNUMERICO2+", "+
                        ProspectoBean.CN_IDREFERENCIADORDISPOSITIVO+", "+
                        ProspectoBean.CN_FLAGENVIADO+", "+
                        ProspectoBean.CN_FLAGENVIARADNDIGITAL+
                        " from "+DatabaseConstants.TBL_PROSPECTO+
                        " where" +
                        filtro_etapa +
                        " and (IFNULL(" + ProspectoBean.CN_NOMBRES + ",'') || IFNULL("+ ProspectoBean.CN_APELLIDOPATERNO + ",'') || IFNULL("+ ProspectoBean.CN_APELLIDOMATERNO+ ",'')) like '%" + pNombre +"%'" +
                        " and "+ProspectoBean.CN_CODIGOESTADO + " <> "+ Constantes.EstadoProspecto_CierreVenta+
                        " and "+ProspectoBean.CN_CODIGOESTADO + " <> "+ Constantes.EstadoProspecto_NoInteresado;

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                ProspectoBean bean = new ProspectoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setIdProspectoExterno(cursor.getInt(2));
                bean.setNombres(cursor.getString(3));
                bean.setApellidoPaterno(cursor.getString(4));
                bean.setApellidoMaterno(cursor.getString(5));
                bean.setCodigoSexo(cursor.getInt(6));
                bean.setCodigoEstadoCivil(cursor.getInt(7));
                bean.setFechaNacimiento(cursor.getString(8));
                bean.setCodigoRangoEdad(cursor.getInt(9));
                bean.setCodigoRangoIngreso(cursor.getInt(10));
                bean.setFlagHijo(cursor.getInt(11));
                bean.setFlagConyuge(cursor.getInt(12));
                bean.setCodigoTipoDocumento(cursor.getInt(13));
                bean.setNumeroDocumento(cursor.getString(14));
                bean.setTelefonoCelular(cursor.getString(15));
                bean.setTelefonoFijo(cursor.getString(16));
                bean.setTelefonoAdicional(cursor.getString(17));
                bean.setCorreoElectronico1(cursor.getString(18));
                bean.setCorreoElectronico2(cursor.getString(19));
                bean.setCodigoNacionalidad(cursor.getInt(20));
                bean.setCondicionFumador(cursor.getInt(21));
                bean.setEmpresa(cursor.getString(22));
                bean.setCargo(cursor.getString(23));
                bean.setNota(cursor.getString(24));
                bean.setCodigoEtapa(cursor.getInt(25));
                bean.setCodigoEstado(cursor.getInt(26));
                bean.setCodigoFuente(cursor.getInt(27));
                bean.setCodigoEntorno(cursor.getInt(28));
                bean.setIdReferenciador(cursor.getInt(29));
                bean.setFechaCreacionDispositivo(cursor.getString(30));
                bean.setFechaModificacionDispositivo(cursor.getString(31));
                bean.setAdicionalTexto1(cursor.getString(32));
                bean.setAdicionalNumerico1(cursor.getDouble(33));
                bean.setAdicionalTexto2(cursor.getString(34));
                bean.setAdicionalNumerico2(cursor.getDouble(35));
                bean.setIdReferenciadorDispositivo(cursor.getInt(36));
                bean.setFlagEnviado(cursor.getInt(37));
                bean.setFlagEnviarADNDigital(cursor.getInt(38));

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }

    public static ProspectoBean obtenerProspectoPorIdProspectoDispositivo(int idProspectoDispositivo){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTODISPOSITIVO = ?", new String[]{Integer.toString(idProspectoDispositivo)});
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }

    public static ProspectoBean obtenerProspectoPorIdProspecto(int idProspecto){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTO = ?", new String[]{Integer.toString(idProspecto)});
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }

    // Para envío al server
    public static ArrayList<ProspectoBean> obtenerProspectosSinEviar(){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<ProspectoBean> arrProspectosFinal = new ArrayList<>();
        for(Entity entity : arrProspectos){
            arrProspectosFinal.add((ProspectoBean)entity);
        }
        return arrProspectosFinal;
    }

    public static void actualizarProspecto(ProspectoBean prospectoBean){
        String[] parameters = new String[]{Integer.toString(prospectoBean.getIdProspectoDispositivo())};
        ProspectoBean.tableHelper.updateEntity(prospectoBean, "IDPROSPECTODISPOSITIVO = ?", parameters);
    }


    public static ArrayList<ProspectoDatosReferidoBean> obtenerProspectosReferidosUltCita(){
        ArrayList<ProspectoDatosReferidoBean> lista =  new ArrayList<>();

        String sql = String.format(
                "SELECT \n" +
                        "\t\tP.IDPROSPECTO,\n" +
                        "\t\tP.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tP.NOMBRES,\n" +
                        "\t\tP.APELLIDOPATERNO,\n" +
                        "\t\tP.APELLIDOMATERNO,\n" +
                        "\t\tP.CODIGOETAPA AS PROSPECTOETAPA,\n" +
                        "\t\tP.CODIGOESTADO AS PROSPECTOESTADO,\n" +
                        "\t\tRE.IDPROSPECTO AS IDRREFERENCIADOR,\n" +
                        "\t\tRE.IDPROSPECTODISPOSITIVO AS IDREFERENCIADORDISPOSITIVO,\n" +
                        "\t\tRE.NOMBRES AS NOMBRESREFERENCIADOR,\n" +
                        "\t\tRE.APELLIDOPATERNO AS APELLIDOPATERNOREFERENCIADOR,\n" +
                        "\t\tRE.APELLIDOMATERNO AS APELLIDOMATERNOREFERENCIADOR,\n" +
                        "\t\tULTCITA.CODIGOESTADO AS ULTCITAESTADO,\n" +
                        "\t\tULTCITA.CODIGORESULTADO AS ULTCITARESULTADO,\n" +
                        "\t\t(SELECT  VALORCADENA FROM  TBL_TABLA_TABLAS WHERE IDTABLA = 7 AND CODIGOCAMPO = P.CODIGOFUENTE LIMIT 1) AS NOMBRE_FUENTE\n" +
                        " FROM TBL_PROSPECTO P\n" +
                        " LEFT JOIN TBL_PROSPECTO RE\n" +
                        " ON P.IDREFERENCIADORDISPOSITIVO = RE.IDPROSPECTODISPOSITIVO\n" +
                        " LEFT JOIN \n" +
                        " (\n" +
                        "\tSELECT\n" +
                        "\t\tIDPROSPECTODISPOSITIVO,\n" +
                        "\t\tMAX(NUMEROENTREVISTA) AS NUMEROENTREVISTA,\n" +
                        "\t\tCODIGOESTADO,\n" +
                        "\t\tCODIGORESULTADO\n" +
                        "\tFROM TBL_CITA \n" +
                        "\tGROUP BY \n" +
                        "\t\tIDPROSPECTODISPOSITIVO\n" +
                        ") ULTCITA\n" +
                        "ON ULTCITA.IDPROSPECTODISPOSITIVO = P.IDPROSPECTODISPOSITIVO\n" +
                        "ORDER BY P.NOMBRES, P.APELLIDOPATERNO, P.APELLIDOMATERNO"
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        ProspectoDatosReferidoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new ProspectoDatosReferidoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoPaterno(cursor.getString(3));
                bean.setApellidoMaterno(cursor.getString(4));
                bean.setEtapaProspecto(cursor.getInt(5));
                bean.setEstadoProspecto(cursor.getInt(6));
                bean.setIdReferenciador(cursor.getInt(7));
                bean.setIdReferenciadorDispositivo(cursor.getInt(8));
                bean.setNombresReferenciador(cursor.getString(9));
                bean.setApellidoPaternoReferenciador(cursor.getString(10));
                bean.setApellidoMaternoReferenciador(cursor.getString(11));
                bean.setEstadoUltCita(cursor.getInt(12));
                bean.setResultadoUltCita(cursor.getInt(13));
                bean.setNombreFuente(cursor.getString(14));

                if (bean.getIdReferenciador() == 0)bean.setIdReferenciador(-1);
                if (bean.getIdReferenciadorDispositivo() == 0)bean.setIdReferenciadorDispositivo(-1);
                if (bean.getEstadoUltCita() == 0)bean.setEstadoUltCita(-1);
                if (bean.getResultadoUltCita() == 0)bean.setResultadoUltCita(-1);

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }

    public static ArrayList<ProspectoDatosReferidoBean> obtenerProspectosCantReferidos(){
        ArrayList<ProspectoDatosReferidoBean> lista = new ArrayList<ProspectoDatosReferidoBean>();
        String sql =
                "SELECT \n" +
                        "\tP.IDPROSPECTO,\n" +
                        "\tP.IDPROSPECTODISPOSITIVO,\n" +
                        "\tP.NOMBRES,\n" +
                        "\tP.APELLIDOPATERNO,\n" +
                        "\tP.APELLIDOMATERNO,\n" +
                        "\tCANTREF.CANTIDADREFERIDOS\n" +
                        "FROM\n" +
                        "TBL_PROSPECTO P\n" +
                        "LEFT JOIN\n" +
                        "(\n" +
                        "\tSELECT \n" +
                        "\t\tIDREFERENCIADORDISPOSITIVO,\n" +
                        "\t\tCOUNT(1)AS CANTIDADREFERIDOS\n" +
                        "\tFROM TBL_PROSPECTO\n" +
                        "\tWHERE IDREFERENCIADORDISPOSITIVO <> -1\n" +
                        "\tGROUP BY IDREFERENCIADORDISPOSITIVO\n" +
                        ") CANTREF\n" +
                        "ON P.IDPROSPECTODISPOSITIVO = CANTREF.IDREFERENCIADORDISPOSITIVO\n" +
                        "WHERE \n" +
                        "\tCANTREF.CANTIDADREFERIDOS IS NOT NULL\n" +
                        "ORDER BY\n" +
                        "\tP.NOMBRES,\n" +
                        "\tP.APELLIDOPATERNO,\n" +
                        "\tP.APELLIDOMATERNO";

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                ProspectoDatosReferidoBean bean = new ProspectoDatosReferidoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoPaterno(cursor.getString(3));
                bean.setApellidoMaterno(cursor.getString(4));
                bean.setCantReferidos(cursor.getInt(5));

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }

    public static ArrayList<ProspectoDatosReferidoBean> obtenerReferidosxIDProspectoDispositivo(int idProspectoDispositivo){
        ArrayList<ProspectoDatosReferidoBean> lista = new ArrayList<ProspectoDatosReferidoBean>();
        String sql =
                "SELECT \n" +
                        "\t\tP.IDPROSPECTO,\n" +
                        "\t\tP.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tP.NOMBRES,\n" +
                        "\t\tP.APELLIDOPATERNO,\n" +
                        "\t\tP.APELLIDOMATERNO,\n" +
                        "\t\tP.CODIGOETAPA AS PROSPECTOETAPA,\n" +
                        "\t\tP.CODIGOESTADO AS PROSPECTOESTADO,\n" +
                        "\t\tULTCITA.CODIGOESTADO AS ULTCITAESTADO,\n" +
                        "\t\tULTCITA.CODIGORESULTADO AS ULTCITARESULTADO\n" +
                        " FROM TBL_PROSPECTO P\n" +
                        " LEFT JOIN \n" +
                        " (\n" +
                        "\tSELECT\n" +
                        "\t\tIDPROSPECTODISPOSITIVO,\n" +
                        "\t\tMAX(NUMEROENTREVISTA) AS NUMEROENTREVISTA,\n" +
                        "\t\tCODIGOESTADO,\n" +
                        "\t\tCODIGORESULTADO\n" +
                        "\tFROM TBL_CITA \n" +
                        "\tGROUP BY \n" +
                        "\t\tIDPROSPECTODISPOSITIVO\n" +
                        ") ULTCITA\n" +
                        "ON ULTCITA.IDPROSPECTODISPOSITIVO = P.IDPROSPECTODISPOSITIVO\n" +
                        "WHERE\n" +
                        "\tIDREFERENCIADORDISPOSITIVO = "+ String.valueOf(idProspectoDispositivo) +"\n" +
                        "ORDER BY P.NOMBRES, P.APELLIDOPATERNO, P.APELLIDOMATERNO\n";

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                ProspectoDatosReferidoBean bean = new ProspectoDatosReferidoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoPaterno(cursor.getString(3));
                bean.setApellidoMaterno(cursor.getString(4));
                bean.setEtapaProspecto(cursor.getInt(5));
                bean.setEstadoProspecto(cursor.getInt(6));
                bean.setEstadoUltCita(cursor.getInt(7));
                bean.setResultadoUltCita(cursor.getInt(8));

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }

    public static void limpiarTablaProspecto(){
        ProspectoBean.tableHelper.deleteAllEntities();
    }

    /*public static ArrayList<ProspectoBean> obtenerTodosProspectos(){
        String[] parameters = new String[]{};
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("", parameters);
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }*/

    public static  ArrayList<ProspectoDatosReferidoBean> getProspectosDelDia() {
        ArrayList<ProspectoDatosReferidoBean> lista =  new ArrayList<>();

        //se usa el codigoestado = 1 "por contactar" y  codigoestado = 2 "agendado" de acuerdo a tabla de tablas

        //String fechaCita = "26/05/2016";
        String fechaCita = Util.obtenerFechaActual_SoloFecha();

        String sql = String.format(
                "SELECT \n" +
                        "\t\tP.IDPROSPECTO,\n" +
                        "\t\tP.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tP.NOMBRES,\n" +
                        "\t\tP.APELLIDOPATERNO,\n" +
                        "\t\tP.APELLIDOMATERNO,\n" +
                        "\t\tRE.IDPROSPECTO AS IDRREFERENCIADOR,\n" +
                        "\t\tRE.IDPROSPECTODISPOSITIVO AS IDREFERENCIADORDISPOSITIVO,\n" +
                        "\t\tRE.NOMBRES AS NOMBRESREFERENCIADOR,\n" +
                        "\t\tRE.APELLIDOPATERNO AS APELLIDOPATERNOREFERENCIADOR,\n" +
                        "\t\tRE.APELLIDOMATERNO AS APELLIDOMATERNOREFERENCIADOR,\n" +
                        "\t\t(SELECT  VALORCADENA FROM  TBL_TABLA_TABLAS WHERE IDTABLA = 7 AND CODIGOCAMPO = P.CODIGOFUENTE LIMIT 1) AS NOMBRE_FUENTE\n" +
                        "FROM TBL_PROSPECTO P\n" +
                        "LEFT JOIN\n" +
                        "\t\tTBL_PROSPECTO RE\n" +
                        "ON P.IDREFERENCIADORDISPOSITIVO = RE.IDPROSPECTODISPOSITIVO\n" +
                        "WHERE\n" +
                        "\tP.CODIGOETAPA = %s AND \n" +
                        "\tP.CODIGOESTADO = %s  \n" +
                        "ORDER BY \n" +
                        "\tP.FLAGHIJO DESC, -- 1: HIJO,0: SIN HIJO, -1: SIN DATOS\n" +
                        "\tCASE P.CODIGORANGOEDAD\n" +
                        "\t\t\tWHEN 3 THEN 0\t--31-40\n" +
                        "\t\t\tWHEN 4 THEN 1 --MAYOR 41\n" +
                        "\t\t\tWHEN 2 THEN 2 --25-30\n" +
                        "\t\t\tWHEN 1 THEN 3 -- MENOS 25\n" +
                        "\t\t\tELSE  4 -- SIN DATO\n" +
                        "\tEND ASC,\n" +
                        "\tP.CODIGORANGOINGRESO DESC ",
                Constantes.EtapaProspecto_Nuevo,
                Constantes.EstadoProspecto_Nuevo
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        Log.d("ProspectoController", "getProspectosDelDia: "+sql);

        ProspectoDatosReferidoBean bean;

        if (cursor.moveToFirst()) {
            do {
                bean = new ProspectoDatosReferidoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoPaterno(cursor.getString(3));
                bean.setApellidoMaterno(cursor.getString(4));
                bean.setIdReferenciador(cursor.getInt(5));
                bean.setIdReferenciadorDispositivo(cursor.getInt(6));
                bean.setNombresReferenciador(cursor.getString(7));
                bean.setApellidoPaternoReferenciador(cursor.getString(8));
                bean.setApellidoMaternoReferenciador(cursor.getString(9));
                bean.setNombreFuente(cursor.getString(10));

                if (bean.getIdReferenciador() == 0)
                    bean.setIdReferenciador(-1);
                if (bean.getIdReferenciadorDispositivo() == 0)
                    bean.setIdReferenciadorDispositivo(-1);

                lista.add(bean);

            } while(cursor.moveToNext());

            //lista.add(listaAgendados);
            //lista.add(listaContactados);
        }

        return lista;
    }
}
