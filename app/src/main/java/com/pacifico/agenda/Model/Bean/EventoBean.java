package com.pacifico.agenda.Model.Bean;

/**
 * Created by joel on 8/25/16.
 */
public class EventoBean {
    private int tipoEvento;
    private int idEventoDispositivo;
    private String fecha;
    private String HoraInicio;
    private String HoraFin; // Cita y reunion
    private String Ubicacion; // Cita y Reunion
    private int idProspectoDispositivo; // Reunion y Recordatorio
    private String Nombres; // Cita y Reunion
    private String ApellidoPaterno; // Cita y Reunion
    private String ApellidoMaterno; // Cita y Reunion
    private int codigoTipoReunion; // Exclusivo de Reunion
    private int etapaCita; // Exclusivo CIta
    private String telefonoCelular;

    public EventoBean(){
        tipoEvento = -1;
        idEventoDispositivo = -1;
        idProspectoDispositivo = -1;
        codigoTipoReunion = -1;
        etapaCita = -1;
    }

    public int getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(int tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public int getIdEventoDispositivo() {
        return idEventoDispositivo;
    }

    public void setIdEventoDispositivo(int idEventoDispositivo) {
        this.idEventoDispositivo = idEventoDispositivo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fechaReunion) {
        this.fecha = fechaReunion;
    }

    public String getHoraInicio() {
        return HoraInicio;
    }

    public void setHoraInicio(String horaInicio) {
        HoraInicio = horaInicio;
    }

    public String getHoraFin() {
        return HoraFin;
    }

    public void setHoraFin(String horaFin) {
        HoraFin = horaFin;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        Ubicacion = ubicacion;
    }

    public int getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        this.idProspectoDispositivo = idProspectoDispositivo;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public int getCodigoTipoReunion() {
        return codigoTipoReunion;
    }

    public void setCodigoTipoReunion(int codigoTipoReunion) {
        this.codigoTipoReunion = codigoTipoReunion;
    }

    public int getEtapaCita() {
        return etapaCita;
    }

    public void setEtapaCita(int etapaCita) {
        this.etapaCita = etapaCita;
    }

    public String getTelefonoCelular() {
        return telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }
}
