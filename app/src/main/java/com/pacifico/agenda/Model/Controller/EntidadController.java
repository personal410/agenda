package com.pacifico.agenda.Model.Controller;




import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Model.Bean.EntidadBean;

import java.util.ArrayList;

/**
 * Created by vctrls3477 on 2/07/16.
 */
public class EntidadController{
    public static ArrayList<EntidadBean> obtenerEntidadesPorCodigoTipoCobranza(int codigoTipoCobranza){
        String[] parametros = new String[]{Integer.toString(codigoTipoCobranza)};
        ArrayList<Entity> arrEntidades = EntidadBean.tableHelper.getEntities("CODIGOTIPOCOBRANZA = ?", parametros);
        ArrayList<EntidadBean> arrEntidadesFinal = new ArrayList<>();
        for(Entity entity : arrEntidades){
            arrEntidadesFinal.add((EntidadBean)entity);
        }
        return arrEntidadesFinal;
    }
    public static void guardaListaEntidad(ArrayList<EntidadBean> listaEntidadBean){
        if(listaEntidadBean != null){
            for(EntidadBean entidadBean : listaEntidadBean){
                EntidadBean.tableHelper.insertEntity(entidadBean);
            }
        }
    }
    public static EntidadBean obtenerEntidadPorIdEntidad(int idEntidad){
        String[] parametros = {Integer.toString(idEntidad)};
        ArrayList<Entity> arrEntidades = EntidadBean.tableHelper.getEntities("IDENTIDAD = ?", parametros);
        if(arrEntidades.size() > 0){
            return (EntidadBean)arrEntidades.get(0);
        }else{
            return null;
        }
    }
    public static void guardarEntidad(EntidadBean entidadBean){
        EntidadBean.tableHelper.insertEntity(entidadBean);
    }
    public static void actualizarEntidad(EntidadBean entidadBean){
        String[] parametros = {Integer.toString(entidadBean.getIdEntidad())};
        EntidadBean.tableHelper.updateEntity(entidadBean, "IDENTIDAD = ?", parametros);
    }
    public static void limpiarTablaEntidad(){
        EntidadBean.tableHelper.deleteAllEntities();
    }
}