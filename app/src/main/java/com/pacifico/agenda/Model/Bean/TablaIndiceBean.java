package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 02/06/2016.
 */


public class TablaIndiceBean extends Entity {

    private int idTabla;
    private String nombreTabla;

    public int getIdTabla() {
        return idTabla;
    }

    public void setIdTabla(int idTabla) {
        this.idTabla = idTabla;
    }

    public String getNombreTabla() {
        return nombreTabla;
    }

    public void setNombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    // Persistencia

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDTABLA:
                return idTabla;

            case CO_NOMBRETABLA:
                return nombreTabla;
        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDTABLA:
                idTabla = (int) object;
                break;
            case CO_NOMBRETABLA:
                nombreTabla = (String) object;
                break;
        }
    }

    public static final String CN_IDTABLA = "IDTABLA";
    public static final String CT_IDTABLA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDTABLA = 1;

    public static final String CN_NOMBRETABLA = "NOMBRETABLA";
    public static final String CT_NOMBRETABLA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOMBRETABLA = 2;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_TABLA_INDICE, new OrganizateEntityFactory())
            .addColumn(CN_IDTABLA, CT_IDTABLA, CO_IDTABLA)
            .addColumn(CN_NOMBRETABLA, CT_NOMBRETABLA, CO_NOMBRETABLA);
}
