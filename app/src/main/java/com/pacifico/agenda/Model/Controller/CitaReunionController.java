package com.pacifico.agenda.Model.Controller;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Activity.AgendaApplication;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.agenda.Model.Bean.CitaProspectoBean;
import com.pacifico.agenda.Model.Bean.EventoBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoDatosReferidoBean;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Model.Bean.ReminderBean;
import com.pacifico.agenda.Model.Bean.ReunionInternaBean;
import com.pacifico.agenda.Model.Bean.TablaIdentificadorBean;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Reminder.ReminderManager;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Joel on 13/06/2016.
 */
public class CitaReunionController {

    // Reunion
    /*public static void guardarReunion(ReunionInternaBean reunionInternaBean) {
        ReunionInternaBean.tableHelper.insertEntity(reunionInternaBean);
    }*/

    //////////////////////
    public static int obtenerMaximoIdCitaDispositivo(){
        int idCitaDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDCITADISPOSITIVO) FROM " + DatabaseConstants.TBL_CITA;
        Cursor cursor = AgendaApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idCitaDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idCitaDispositivoMax;
    }
    public static int obtenerMaximoIdCitaMovimientoDispositivo(){
        int idCitaMovimientoDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDMOVIMIENTODISPOSITIVO) FROM " + DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO;
        Cursor cursor = AgendaApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idCitaMovimientoDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idCitaMovimientoDispositivoMax;
    }

    public static int obtenerMaximoIdReunionInternaDispositivo(){
        int idReunionInternaDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDREUNIONINTERNADISPOSITIVO) FROM " + DatabaseConstants.TBL_REUNION_INTERNA;
        Cursor cursor = AgendaApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idReunionInternaDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idReunionInternaDispositivoMax;
    }

    public static int obtenerMaximoIdRecordatorioLlamadaDispositivo(){
        int idRecordatorioLlamadaDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDRECORDATORIOLLAMADADISPOSITIVO) FROM " + DatabaseConstants.TBL_RECORDATORIO_LLAMADA;
        Cursor cursor = AgendaApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idRecordatorioLlamadaDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idRecordatorioLlamadaDispositivoMax;
    }

    /////////////////////

    public static void modificarReunion(ReunionInternaBean reunionInternaBean)
    {
        String[] parameters = new String[]{Integer.toString(reunionInternaBean.getIdReunionInternaDispositivo())};
        ReunionInternaBean.tableHelper.updateEntity(reunionInternaBean, ReunionInternaBean.CN_IDREUNIONINTERNADISPOSITIVO+" = ?", parameters);
    }

    public static void guardarReunion_GenerarIDDispositivo(ReunionInternaBean reunionInternaBean)
    {
        TablaIdentificadorBean identificadorReunionBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_REUNION_INTERNA);
        reunionInternaBean.setIdReunionInternaDispositivo(identificadorReunionBean.getIdentity()+1);

        long row = ReunionInternaBean.tableHelper.insertEntity(reunionInternaBean);

        if (row != -1) // SI ES -1, HUBO UN PROBLEMA
        {
            identificadorReunionBean.setIdentity(identificadorReunionBean.getIdentity()+1);
            TablaIdentificadorController.actualizarTablaIdentificador(identificadorReunionBean);
        }
    }

    public static void guardarListaReuniones(ArrayList<ReunionInternaBean> listaReunionInternaBean)
    {
        if (listaReunionInternaBean != null && listaReunionInternaBean.size()>0)
            for(int i =0; i<listaReunionInternaBean.size(); i++)
                ReunionInternaBean.tableHelper.insertEntity(listaReunionInternaBean.get(i));
    }

    public static void guardarListaRecordatorioLlamadas(ArrayList<RecordatorioLlamadaBean> listaRecordatorioLLamadaBean)
    {
        if (listaRecordatorioLLamadaBean != null && listaRecordatorioLLamadaBean.size()>0)
            for (RecordatorioLlamadaBean recordatorioLlamadaBean : listaRecordatorioLLamadaBean){
                if(recordatorioLlamadaBean.getIdProspectoDispositivo() == -1){
                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(recordatorioLlamadaBean.getIdProspecto());
                    recordatorioLlamadaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                }
                if(recordatorioLlamadaBean.getIdCitaDispositivo() == -1 && recordatorioLlamadaBean.getIdCita() != -1){ // No siempre hay una cita relacionada al recordatorio
                    CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCita(recordatorioLlamadaBean.getIdCita());
                    recordatorioLlamadaBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());
                }

                RecordatorioLlamadaBean.tableHelper.insertEntity(recordatorioLlamadaBean);
            }
    }

    public static ReunionInternaBean obtenerReunionInterPorIdReunionInternaDispositivo(int idReunionInternaDispositivo){
        ArrayList<Entity> arrReunionInternas = ReunionInternaBean.tableHelper.getEntities("IDREUNIONINTERNADISPOSITIVO = ?", new String[]{Integer.toString(idReunionInternaDispositivo)});
        if(arrReunionInternas.size() > 0){
            return (ReunionInternaBean)arrReunionInternas.get(0);
        }else{
            return null;
        }
    }

    public static void actualizarReunionIntera(ReunionInternaBean reunionInternaBean){
        String[] parameters = new String[]{Integer.toString(reunionInternaBean.getIdReunionInternaDispositivo())};
        ReunionInternaBean.tableHelper.updateEntity(reunionInternaBean, "IDREUNIONINTERNADISPOSITIVO = ?", parameters);
    }

    public static void limpiarTablaReunionInterna(){
        ReunionInternaBean.tableHelper.deleteAllEntities();
    }

    /*public static void guardarListaCitas(ArrayList<CitaBean> listaCitaBean)
    {
        if (listaCitaBean != null && listaCitaBean.size()>0)
            for(int i =0; i<listaCitaBean.size(); i++)
                CitaBean.tableHelper.insertEntity(listaCitaBean.get(i));
    }*/

    public static CitaBean obtenerCitaPorIdCitaDispositivo(int idCitaDispositivo){
        String[] parametros = {Integer.toString(idCitaDispositivo)};
        ArrayList<Entity> arrCitas = CitaBean.tableHelper.getEntities("IDCITADISPOSITIVO = ?", parametros);
        if(arrCitas.size() > 0){
            return (CitaBean)arrCitas.get(0);
        }else{
            return null;
        }
    }

    public static void guardarListaCitas(ArrayList<CitaBean> listaCitaBean) {
        if(listaCitaBean != null){
            for (CitaBean citaBean : listaCitaBean){
                if(citaBean.getIdProspectoDispositivo() == -1){
                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(citaBean.getIdProspecto());
                    citaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                }
                CitaBean.tableHelper.insertEntity(citaBean);
            }
        }
    }

    public static void insertarCitaDesdeTarjeta(CitaBean citaBean,Context context)
    {
        // Obteniendo el numero de Entrevista de prospecto
        citaBean.setNumeroEntrevista(ultimoNumeroEntrevistaProspecto(citaBean.getIdProspectoDispositivo()) +1);

        // Seteando el número de entrevista creación
        citaBean.setCodigoIntermediarioCreacion(IntermediarioController.obtenerIntermediario().getCodigoIntermediario());

        // Guardando ID
         TablaIdentificadorBean identificadorCitaBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_CITA);

         citaBean.setIdCitaDispositivo(identificadorCitaBean.getIdentity()+1);
         long row = CitaBean.tableHelper.insertEntity(citaBean);

         if (row != -1) // SI ES -1, HUBO UN PROBLEMA
         {
            identificadorCitaBean.setIdentity(identificadorCitaBean.getIdentity()+1);
            TablaIdentificadorController.actualizarTablaIdentificador(identificadorCitaBean);
         }

        ProspectoBean prospectoCita =  ProspectoController.getProspectoBeanByIdProspectoDispositivo(citaBean.getIdProspectoDispositivo());
        String nombreCompletoProspecto= (Util.capitalizedString(prospectoCita.getNombres()) + " " +
                Util.capitalizedString(prospectoCita.getApellidoPaterno())+ " "+
                Util.capitalizedString(prospectoCita.getApellidoMaterno())).trim();

        //// Seteando Alerta de Cita
        if (citaBean.getCodigoEstado() == Constantes.EstadoCita_Agendada && citaBean.getCodigoResultado() == Constantes.ResultadoCita_Pendiente && citaBean.getAlertaMinutosAntes() != -1)
        {
            guardarAlertaCita(citaBean,context,nombreCompletoProspecto);
        }

        // Guardar la alerta post-cita
        if (citaBean.getCodigoEstado() == Constantes.EstadoCita_Agendada && citaBean.getCodigoResultado() == Constantes.ResultadoCita_Pendiente) {
            guardarAlertaResultadoCita(citaBean, context, nombreCompletoProspecto);
        }

        // Guardando los recordatorios de llamada
        RecordatorioLlamadaBean recordatorio = citaBean.getRecordatorioLlamadaBean();

        if (recordatorio != null) {
            recordatorio.setIdCitaDispositivo(citaBean.getIdCitaDispositivo()); // Se actualiza el idCitaDispositivo

            guardarRecordatorioLLamada_GenerarIDDispositivo(recordatorio);

            guardarAlertaRecordatorioCita(citaBean,context,nombreCompletoProspecto);
        }

    }

    public static int ultimoNumeroEntrevistaProspecto(int idProspectoDispositivo)
    {

        String sql = "select max(NumeroEntrevista) from tbl_cita where  idProspectoDIspositivo = "+ idProspectoDispositivo;

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        int ultimoNumeroEntrevista = 0;
        if (cursor.moveToFirst()) {
            do {
                ultimoNumeroEntrevista = cursor.getInt(0);
            } while(cursor.moveToNext());
        }

        return ultimoNumeroEntrevista;
    }

    public static void modificarCita(CitaBean citaBean, Context context)
    {
        // Seteando el número de entrevista creación
        citaBean.setCodigoIntermediarioModificacion(IntermediarioController.obtenerIntermediario().getCodigoIntermediario());

        ProspectoBean prospectoCita =  ProspectoController.getProspectoBeanByIdProspectoDispositivo(citaBean.getIdProspectoDispositivo());
        String nombreCompletoProspecto= (Util.capitalizedString(prospectoCita.getNombres()) + " " +
                Util.capitalizedString(prospectoCita.getApellidoPaterno())+ " "+
                Util.capitalizedString(prospectoCita.getApellidoMaterno())).trim();
        /////

        String[] parameters = new String[]{Integer.toString(citaBean.getIdCitaDispositivo())};
        CitaBean.tableHelper.updateEntity(citaBean, CitaBean.CN_IDCITADISPOSITIVO+" = ?", parameters);

        ////// NOTIFICACIONES
        ReminderManager rm = new ReminderManager(context);
        // Se elimina alerta cita anterior
        String idReminderalerta = rm.obtenerIDReminderPorIDFuente(Constantes.alerta_cita, citaBean.getIdCitaDispositivo());
        if (idReminderalerta != null)
            rm.deleteReminder(Integer.parseInt(idReminderalerta));

        //// Seteando Alerta de Cita
        if (citaBean.getCodigoEstado() == Constantes.EstadoCita_Agendada && citaBean.getCodigoResultado() == Constantes.ResultadoCita_Pendiente && citaBean.getAlertaMinutosAntes() != -1)
        {
            guardarAlertaCita(citaBean,context,nombreCompletoProspecto);
        }

        // ALERTA POST CITA si es una agendada pendiente
        if (citaBean.getCodigoEstado() == Constantes.EstadoCita_Agendada && citaBean.getCodigoResultado() == Constantes.ResultadoCita_Pendiente) {
            // Se elimina alerta post cita cita anterior
            String idReminderPostCita = rm.obtenerIDReminderPorIDFuente(Constantes.alerta_post_cita, citaBean.getIdCitaDispositivo());
            if (idReminderPostCita != null)
                rm.deleteReminder(Integer.parseInt(idReminderPostCita));

            // Guardar la alerta post-cita
            if (citaBean.getFechaCita() != null && citaBean.getHoraInicio() != null && citaBean.getHoraFin() != null) {
                guardarAlertaResultadoCita(citaBean, context, nombreCompletoProspecto);
            }
        }

        /// Recordatorio de llamada
        if (citaBean.getRecordatorioLlamadaBean() != null && // Si el recordatorio  no es nulo
            citaBean.getRecordatorioLlamadaBean().getIdRecordatorioLlamadaDispositivo() == -1) // Si el id es -1 (es nuevo); si no es -1; no se modifico la hora del recordatorio
        {
            // Obtenemos los recordatorios LLamada de la cita
            ArrayList<RecordatorioLlamadaBean> listaRecordatoriosAEliminar = obtenerRecordatoriosporCitaDispositivo(citaBean.getIdCitaDispositivo());
            for (int i =0; i<listaRecordatoriosAEliminar.size();i++)
            {
                String idReminder = rm.obtenerIDReminderPorIDFuente(Constantes.alerta_recordatorio, listaRecordatoriosAEliminar.get(i).getIdRecordatorioLlamadaDispositivo());
                if(idReminder != null)
                    rm.deleteReminder(Integer.parseInt(idReminder));
            }
            ///////////////

            // SE DESACTIVA EL RECORDATORIO DE LLAMADA YA EXISTENTE (DE HABERLO)
            String sql = "";

            sql = "update " + DatabaseConstants.TBL_RECORDATORIO_LLAMADA +
                    " set " + RecordatorioLlamadaBean.CN_FLAGACTIVO + " = " + Constantes.FLAG_INACTIVO+ ","+
                    RecordatorioLlamadaBean.CN_FECHAMODIFICACIONDISPOSITIVO + " = '" + Util.obtenerFechaActual()+ "',"+
                    RecordatorioLlamadaBean.CN_FLAGENVIADO + " = " + Constantes.ENVIO_PENDIENTE +
                    " where" +
                    " " + RecordatorioLlamadaBean.CN_IDCITADISPOSITIVO + " = " + citaBean.getIdCitaDispositivo() +";";

            AgendaApplication.getDB().beginTransaction();
            AgendaApplication.getDB().execSQL(sql);
            AgendaApplication.getDB().setTransactionSuccessful();
            AgendaApplication.getDB().endTransaction();

            guardarRecordatorioLLamada_GenerarIDDispositivo(citaBean.getRecordatorioLlamadaBean());

            guardarAlertaRecordatorioCita(citaBean,context,nombreCompletoProspecto);
        }
    }


    public static void guardarCitaMovimientoEstado_GenerarIDDispositivo(CitaMovimientoEstadoBean citaMovimientoEstadoBean) {

        //int lastid = TablaIdentificadorController.ObtenerUtimoIdTabla(DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO);
        TablaIdentificadorBean identificadorCitaMovimientoEstadoBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO);

        citaMovimientoEstadoBean.setIdMovimientoDispositivo(identificadorCitaMovimientoEstadoBean.getIdentity()+1);
        long row = CitaMovimientoEstadoBean.tableHelper.insertEntity(citaMovimientoEstadoBean);

        if (row != -1) // SI ES -1, HUBO UN PROBLEMA
        {
            identificadorCitaMovimientoEstadoBean.setIdentity(identificadorCitaMovimientoEstadoBean.getIdentity()+1);
            TablaIdentificadorController.actualizarTablaIdentificador(identificadorCitaMovimientoEstadoBean);
        }
    }

    /*public static void guardarListaCitaMovimientoEstado(ArrayList<CitaMovimientoEstadoBean> listaCitaMovimientoEstadoBean)
    {
        if (listaCitaMovimientoEstadoBean != null && listaCitaMovimientoEstadoBean.size()>0)
            for(int i =0; i<listaCitaMovimientoEstadoBean.size(); i++)
                CitaMovimientoEstadoBean.tableHelper.insertEntity(listaCitaMovimientoEstadoBean.get(i));
    }*/

    public static void guardarListaCitaMovimientoEstado(ArrayList<CitaMovimientoEstadoBean> listaCitaMovimientoEstadoBean) {
        if (listaCitaMovimientoEstadoBean != null){
            for(CitaMovimientoEstadoBean citaMovimientoEstadoBean : listaCitaMovimientoEstadoBean){
                if(citaMovimientoEstadoBean.getIdCitaDispositivo() == -1){
                    CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCita(citaMovimientoEstadoBean.getIdCita());
                    citaMovimientoEstadoBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());
                }
                CitaMovimientoEstadoBean.tableHelper.insertEntity(citaMovimientoEstadoBean);
            }
        }
    }

    public static CitaBean obtenerCitaPorIdCita(int idCita){
        String[] parametros = {Integer.toString(idCita)};
        ArrayList<Entity> arrCitas = CitaBean.tableHelper.getEntities("IDCITA = ?", parametros);
        if(arrCitas.size() > 0){
            return (CitaBean)arrCitas.get(0);
        }else{
            return null;
        }
    }

    public static void actualizarCita(CitaBean citaBean){
        String[] parametros = {Integer.toString(citaBean.getIdCitaDispositivo())};
        CitaBean.tableHelper.updateEntity(citaBean, "IDCITADISPOSITIVO = ?", parametros);
    }

    /*public static  ArrayList<ArrayList<CitaProspectoBean>> getProspectosDelDia() {

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayList<CitaProspectoBean> listaAgendados =  new ArrayList<>();
        ArrayList<CitaProspectoBean> listaContactados =  new ArrayList<>();
        ArrayList<ArrayList<CitaProspectoBean>> lista = new ArrayList<>();

        //se usa el codigoestado = 1 "por contactar" y  codigoestado = 2 "agendado" de acuerdo a tabla de tablas

        //String fechaCita = "26/05/2016";
        String fechaCita = Util.obtenerFechaActual_SoloFecha();

        String sql = String.format(
                "select DISTINCT IDCITADISPOSITIVO" +
                        " ,FECHACITA " +
                        " ,c.IDPROSPECTO " +
                        " ,NOMBRES " +
                        " ,APELLIDOPATERNO " +
                        " ,APELLIDOMATERNO " +
                        " ,CODIGOESTADO " +

                        " from "+DatabaseConstants.TBL_CITA+" c" +
                        " inner join "+ DatabaseConstants.TBL_PROSPECTO + "p" +
                        " on c.IDPROSPECTO = p.IDPROSPECTO" +

                        " where " +
                        " FECHACITA = '%s'" +
                        " and p.codigoestado = %s" +
                        " or p.codigoestado = %s",
                fechaCita,
                Constantes.PROSPECTO_POR_CONTACTAR,
                Constantes.PROSPECTO_AGENDADO
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        CitaProspectoBean bean;
        String codigoEstado = "";
        if (cursor.moveToFirst()) {
            do {
                bean = new CitaProspectoBean();
                bean.setIdCitaDispositivo(cursor.getInt(0));
                bean.setFechaCita(cursor.getString(1));
                bean.setIdProspecto(cursor.getInt(2));
                bean.setNombres(cursor.getString(3));
                bean.setApellidoPaterno(cursor.getString(4));
                bean.setApellidoMaterno(cursor.getString(5));

                codigoEstado = Util.strNULL(cursor.getString(6));

                if (codigoEstado.equalsIgnoreCase(Constantes.PROSPECTO_POR_CONTACTAR)) {
                    listaAgendados.add(bean);
                }
                else if (codigoEstado.equalsIgnoreCase(Constantes.PROSPECTO_AGENDADO)) {
                    listaContactados.add(bean);
                }

            } while(cursor.moveToNext());

            lista.add(listaAgendados);
            lista.add(listaContactados);
        }

        return lista;
    }*/



    /*
    // TODAS LOS PROSPECTOS CON CITAS DE FECHA CREACION EL DIA DE HOY Q NO SEAN REAGENDADA
    public static  ArrayList<ProspectoDatosReferidoBean> getProspectosAgendados() {

        String fecha_actual = Util.obtenerFechaActual_SoloFecha();

        ArrayList<ProspectoDatosReferidoBean> lista =  new ArrayList<>();

        String sql = String.format(
                "SELECT \n" +
                        "            P.IDPROSPECTO,\n" +
                        "            P.IDPROSPECTODISPOSITIVO,\n" +
                        "            P.NOMBRES,\n" +
                        "            P.APELLIDOPATERNO,\n" +
                        "\t\t\tP.APELLIDOMATERNO,\n" +
                        "            RE.IDPROSPECTO AS IDRREFERENCIADOR,\n" +
                        "            RE.IDPROSPECTODISPOSITIVO AS IDREFERENCIADORDISPOSITIVO,\n" +
                        "            RE.NOMBRES AS NOMBRESREFERENCIADOR,\n" +
                        "            RE.APELLIDOPATERNO AS APELLIDOPATERNOREFERENCIADOR,\n" +
                        "            RE.APELLIDOMATERNO AS APELLIDOMATERNOREFERENCIADOR,\n" +
                        "\t\t\tULTCITAS.FECHACITA,\n" +
                        "\t\t\tULTCITAS.HORAINICIO,\n" +
                        "\t\t\tULTCITAS.HORAFIN,\n" +
                        "\t\t\tULTCITAS.CODIGOESTADO,\n" +
                        "\t\t\tULTCITAS.CODIGORESULTADO\n" +
                        "FROM TBL_PROSPECTO P\n" +
                        "LEFT JOIN\n" +
                        "         TBL_PROSPECTO RE\n" +
                        "ON P.IDREFERENCIADORDISPOSITIVO = RE.IDPROSPECTODISPOSITIVO\n" +
                        "LEFT JOIN \n" +
                        " (SELECT \n" +
                        "\t\tMAX(C.NUMEROENTREVISTA),\n" +
                        "\t\tC.FECHACITA,\n" +
                        "\t\tC.HORAINICIO,\n" +
                        "\t\tC.HORAFIN,\n" +
                        "\t\tC.CODIGOESTADO,\n" +
                        "\t\tC.CODIGORESULTADO,\n" +
                        "\t\tC.CODIGOETAPAPROSPECTO,\n" +
                        "\t\tC.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tC.FECHACREACIONDISPOSITIVO\n" +
                        "  FROM \n" +
                        "\t\tTBL_CITA  C\n" +
                        "  GROUP BY C.IDPROSPECTODISPOSITIVO\n" +
                        ") ULTCITAS\n" +
                        "ON P.IDPROSPECTODISPOSITIVO = ULTCITAS.IDPROSPECTODISPOSITIVO\n" +
                        "WHERE \n" +
                        "\tULTCITAS.CODIGOESTADO <> %S AND\n" +
                        "\tSUBSTR(ULTCITAS.FECHACREACIONDISPOSITIVO,0,11) = '%S' \n" +
                        "ORDER BY \n" +
                        "\tULTCITAS.FECHACITA ASC,\n" +
                        "\tULTCITAS.HORAINICIO ASC",
                Constantes.EstadoCita_ReAgendada,
                fecha_actual
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        ProspectoDatosReferidoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new ProspectoDatosReferidoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoPaterno(cursor.getString(3));
                bean.setApellidoMaterno(cursor.getString(4));
                bean.setIdReferenciador(cursor.getInt(5));
                bean.setIdReferenciadorDispositivo(cursor.getInt(6));
                bean.setNombresReferenciador(cursor.getString(7));
                bean.setApellidoPaternoReferenciador(cursor.getString(8));
                bean.setApellidoMaternoReferenciador(cursor.getString(9));
                //
                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }*/

    // SON TODOS LOS PROSPECTOS QUE TIENEN UN MOVIMIENTO DE ESTADO CITA A AGENDADO HOY
    public static ArrayList<ProspectoDatosReferidoBean> obtenerProspectosAgendadosHoy()
    {
        String fecha_actual = Util.obtenerFechaActual_SoloFecha();

        ArrayList<ProspectoDatosReferidoBean> lista =  new ArrayList<>();

        String sql = String.format("SELECT \n" +
                        "\t\tP.IDPROSPECTO,\n" +
                        "\t\tP.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tP.NOMBRES,\n" +
                        "        P.APELLIDOPATERNO,\n" +
                        "\t\tP.APELLIDOMATERNO,\n" +
                        "\t\tRE.IDPROSPECTO AS IDRREFERENCIADOR,\n" +
                        "        RE.IDPROSPECTODISPOSITIVO AS IDREFERENCIADORDISPOSITIVO,\n" +
                        "        RE.NOMBRES AS NOMBRESREFERENCIADOR,\n" +
                        "        RE.APELLIDOPATERNO AS APELLIDOPATERNOREFERENCIADOR,\n" +
                        "        RE.APELLIDOMATERNO AS APELLIDOMATERNOREFERENCIADOR,\n" +
                        "        (SELECT  VALORCADENA FROM  TBL_TABLA_TABLAS WHERE IDTABLA = 7 AND CODIGOCAMPO = P.CODIGOFUENTE LIMIT 1) AS NOMBRE_FUENTE,\n" +
                        "        P.CODIGOETAPA \n" +
                        "FROM\n" +
                        "-- SELECCIONA TODOS LOS PROSPECTOS QUE HAN AGENDADO ALGO ESE DIA\n" +
                        "(\n" +
                        "\tSELECT DISTINCT C.IDPROSPECTODISPOSITIVO FROM\n" +
                        "\t(\n" +
                        "\t\tSELECT IDCITADISPOSITIVO FROM TBL_CITA_MOVIMIENTO_ESTADO \n" +
                        "\t\t\t\tWHERE \n" +
                        "\t\t\t\t\t\tSUBSTR(FECHAMOVIMIENTOESTADODISPOSITIVO,0,11)= '%S'  AND\n" +
                        "\t\t\t\t\t   CODIGOESTADO = 2\n" + // AGENDADO
                        "\t)MOV\n" +
                        "\tINNER JOIN\n" +
                        "\tTBL_CITA  C\n" +
                        "\tON  MOV.IDCITADISPOSITIVO = C.IDCITADISPOSITIVO\n" +
                        ")CMOV\n" +
                        "INNER JOIN \n" +
                        "TBL_PROSPECTO P\n" +
                        "ON P.IDPROSPECTODISPOSITIVO = CMOV.IDPROSPECTODISPOSITIVO\n" +
                        "LEFT JOIN\n" +
                        " TBL_PROSPECTO RE\n" +
                        "ON P.IDREFERENCIADORDISPOSITIVO = RE.IDPROSPECTODISPOSITIVO\n" +
                        "ORDER BY \n" +
                        "P.NOMBRES,\n" +
                        "P.APELLIDOPATERNO,\n" +
                        "P.APELLIDOMATERNO",
                fecha_actual
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        ProspectoDatosReferidoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new ProspectoDatosReferidoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoPaterno(cursor.getString(3));
                bean.setApellidoMaterno(cursor.getString(4));
                bean.setIdReferenciador(cursor.getInt(5));
                bean.setIdReferenciadorDispositivo(cursor.getInt(6));
                bean.setNombresReferenciador(cursor.getString(7));
                bean.setApellidoPaternoReferenciador(cursor.getString(8));
                bean.setApellidoMaternoReferenciador(cursor.getString(9));
                bean.setNombreFuente(cursor.getString(10));
                bean.setEtapaProspecto(cursor.getInt(11));

                if (bean.getIdReferenciador() == 0)
                    bean.setIdReferenciador(-1);
                if (bean.getIdReferenciadorDispositivo() == 0)
                    bean.setIdReferenciadorDispositivo(-1);

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }

    // SON TODOS LOS PROSPECTOS QUE TIENEN UN MOVIMIENTO DE ESTADO CITA A POR CONTACTAR HOY
    public static ArrayList<ProspectoDatosReferidoBean> obtenerProspectosPorContactarHoy()
    {
        String fecha_actual = Util.obtenerFechaActual_SoloFecha();

        ArrayList<ProspectoDatosReferidoBean> lista =  new ArrayList<>();

        String sql = String.format("SELECT \n" +
                        "\t\tP.IDPROSPECTO,\n" +
                        "\t\tP.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tP.NOMBRES,\n" +
                        "        P.APELLIDOPATERNO,\n" +
                        "\t\tP.APELLIDOMATERNO,\n" +
                        "\t\tRE.IDPROSPECTO AS IDRREFERENCIADOR,\n" +
                        "        RE.IDPROSPECTODISPOSITIVO AS IDREFERENCIADORDISPOSITIVO,\n" +
                        "        RE.NOMBRES AS NOMBRESREFERENCIADOR,\n" +
                        "        RE.APELLIDOPATERNO AS APELLIDOPATERNOREFERENCIADOR,\n" +
                        "        RE.APELLIDOMATERNO AS APELLIDOMATERNOREFERENCIADOR,\n" +
                        "        (SELECT  VALORCADENA FROM  TBL_TABLA_TABLAS WHERE IDTABLA = 7 AND CODIGOCAMPO = P.CODIGOFUENTE LIMIT 1) AS NOMBRE_FUENTE\n" +
                        "FROM\n" +
                        "-- SELECCIONA TODOS LOS PROSPECTOS QUE HAN AGENDADO ALGO ESE DIA\n" +
                        "(\n" +
                        "\tSELECT DISTINCT C.IDPROSPECTODISPOSITIVO FROM\n" +
                        "\t(\n" +
                        "\t\tSELECT IDCITADISPOSITIVO FROM TBL_CITA_MOVIMIENTO_ESTADO \n" +
                        "\t\t\t\tWHERE \n" +
                        "\t\t\t\t\t\tSUBSTR(FECHAMOVIMIENTOESTADODISPOSITIVO,0,11)= '%S'  AND\n" +
                        "\t\t\t\t\t   CODIGOESTADO = 1\n" + // POR CONTACTAR
                        "\t)MOV\n" +
                        "\tINNER JOIN\n" +
                        "\tTBL_CITA  C\n" +
                        "\tON  MOV.IDCITADISPOSITIVO = C.IDCITADISPOSITIVO\n" +
                        ")CMOV\n" +
                        "INNER JOIN \n" +
                        "TBL_PROSPECTO P\n" +
                        "ON P.IDPROSPECTODISPOSITIVO = CMOV.IDPROSPECTODISPOSITIVO\n" +
                        "LEFT JOIN\n" +
                        " TBL_PROSPECTO RE\n" +
                        "ON P.IDREFERENCIADORDISPOSITIVO = RE.IDPROSPECTODISPOSITIVO\n" +
                        "ORDER BY \n" +
                        "P.NOMBRES,\n" +
                        "P.APELLIDOPATERNO,\n" +
                        "P.APELLIDOMATERNO",
                fecha_actual
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        ProspectoDatosReferidoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new ProspectoDatosReferidoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoPaterno(cursor.getString(3));
                bean.setApellidoMaterno(cursor.getString(4));
                bean.setIdReferenciador(cursor.getInt(5));
                bean.setIdReferenciadorDispositivo(cursor.getInt(6));
                bean.setNombresReferenciador(cursor.getString(7));
                bean.setApellidoPaternoReferenciador(cursor.getString(8));
                bean.setApellidoMaternoReferenciador(cursor.getString(9));
                bean.setNombreFuente(cursor.getString(10));

                if (bean.getIdReferenciador() == 0)
                    bean.setIdReferenciador(-1);
                if (bean.getIdReferenciadorDispositivo() == 0)
                    bean.setIdReferenciadorDispositivo(-1);

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }


    public static void desactivarRecordatorioLLamada(RecordatorioLlamadaBean recordatorioLlamadaBean){
        String sql = "";

        sql = "update " + DatabaseConstants.TBL_RECORDATORIO_LLAMADA +
                " set " + RecordatorioLlamadaBean.CN_FLAGACTIVO + " = " + Constantes.FLAG_INACTIVO+ ","+
                RecordatorioLlamadaBean.CN_FLAGENVIADO + " = " + Constantes.ENVIO_PENDIENTE +
                " where" +
                " " + RecordatorioLlamadaBean.CN_IDRECORDATORIOLLAMADADISPOSITIVO + " = " + recordatorioLlamadaBean.getIdRecordatorioLlamadaDispositivo() +";";

        AgendaApplication.getDB().beginTransaction();
        AgendaApplication.getDB().execSQL(sql);
        AgendaApplication.getDB().setTransactionSuccessful();
        AgendaApplication.getDB().endTransaction();
    }

    /*
    // TODOS LOS RECORDATORIOS DE LLAMADA ACTIVAS DE FECHAS POSTERIORES AL ACTUAL
    public static  ArrayList<ProspectoDatosReferidoBean> getProspectosRecordatorioLlamada() {

        ArrayList<ProspectoDatosReferidoBean> lista =  new ArrayList<>();

        String fecha_actual = Util.obtenerFechaActual();

        String sql = String.format(
                "SELECT \n" +
                        "            P.IDPROSPECTO,\n" +
                        "            P.IDPROSPECTODISPOSITIVO,\n" +
                        "            P.NOMBRES,\n" +
                        "            P.APELLIDOPATERNO,\n" +
                        "\t\t\tP.APELLIDOMATERNO,\n" +
                        "            RE.IDPROSPECTO AS IDRREFERENCIADOR,\n" +
                        "            RE.IDPROSPECTODISPOSITIVO AS IDREFERENCIADORDISPOSITIVO,\n" +
                        "            RE.NOMBRES AS NOMBRESREFERENCIADOR,\n" +
                        "            RE.APELLIDOPATERNO AS APELLIDOPATERNOREFERENCIADOR,\n" +
                        "            RE.APELLIDOMATERNO AS APELLIDOMATERNOREFERENCIADOR,\n" +
                        "\t\t\tULTREC.FECHARECORDATORIO\n" +
                        "FROM TBL_PROSPECTO P\n" +
                        "LEFT JOIN\n" +
                        "         TBL_PROSPECTO RE\n" +
                        "ON P.IDREFERENCIADORDISPOSITIVO = RE.IDPROSPECTODISPOSITIVO\n" +
                        "INNER JOIN \n" +
                        " (SELECT \n" +
                        "\t\tMAX(R.FECHARECORDATORIO) AS FECHARECORDATORIO,\n" +
                        "\t\tR.IDPROSPECTODISPOSITIVO\n" +
                        "  FROM \n" +
                        "\t\tTBL_RECORDATORIO_LLAMADA  R\n" +
                        "  WHERE FLAGACTIVO = 1\n" +
                        "  GROUP BY R.IDPROSPECTODISPOSITIVO\n" +
                        ") ULTREC\n" +
                        "ON P.IDPROSPECTODISPOSITIVO = ULTREC.IDPROSPECTODISPOSITIVO\n" +
                        "WHERE \n" +
                        "\tULTREC.FECHARECORDATORIO  > '%s'\n" +
                        "ORDER BY \n" +
                        "ULTREC.FECHARECORDATORIO ASC",
                fecha_actual
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        ProspectoDatosReferidoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new ProspectoDatosReferidoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoPaterno(cursor.getString(3));
                bean.setApellidoMaterno(cursor.getString(4));
                bean.setIdReferenciador(cursor.getInt(5));
                bean.setIdReferenciadorDispositivo(cursor.getInt(6));
                bean.setNombresReferenciador(cursor.getString(7));
                bean.setApellidoPaternoReferenciador(cursor.getString(8));
                bean.setApellidoMaternoReferenciador(cursor.getString(9));
                lista.add(bean);
            } while(cursor.moveToNext());
        }


        return lista;
    }*/


    // Obteniendo Citas de las semanas
    public static  ArrayList<CitaProspectoBean> getCitasEntreFechas(String fechaInicio, String fechaFin) {

        /*try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        ArrayList<CitaProspectoBean> lista =  new ArrayList<>();

        //TODO Se debe agregar que las citas sean >= al dia actual
        String sql = String.format(
                "SELECT \n" +
                        "\t\tC.IDCITADISPOSITIVO,\n" +
                        "\t\tC.IDCITA,\n" +
                        "\t\tC.FECHACITA,\n" +
                        "\t\tC.HORAINICIO,\n" +
                        "\t\tC.HORAFIN,\n" +
                        "\t\tC.IDPROSPECTO,\n" +
                        "\t\tC.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tC.CODIGOETAPAPROSPECTO,\n" +
                        "\t\tP.NOMBRES,\n" +
                        "\t\tP.APELLIDOPATERNO,\n" +
                        "\t\tP.APELLIDOMATERNO\n" +
                        " FROM\n" +
                        "TBL_CITA c \n" +
                        "inner join TBL_PROSPECTO p on \n" +
                        "C.IDPROSPECTODISPOSITIVO = P.IDPROSPECTODISPOSITIVO\n" +
                        "WHERE C.FECHACITA BETWEEN '%S' AND '%S'  and (c.codigoestado = %S OR c.codigoestado = %S)\n"+
                        "ORDER BY C.FECHACITA ASC, C.HORAINICIO ASC",
                fechaInicio,
                fechaFin,
                Constantes.EstadoCita_Agendada,
                Constantes.EstadoCita_Realizada
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        CitaProspectoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new CitaProspectoBean();
                bean.setIdCitaDispositivo(cursor.getInt(0));
                bean.setIdCita(cursor.getInt(1));
                bean.setFechaCita(cursor.getString(2));
                bean.setHoraIncioCita(cursor.getString(3));
                bean.setHoraFinCita(cursor.getString(4));
                bean.setIdProspecto(cursor.getInt(5));
                bean.setIdProspectoDispositivo(cursor.getInt(6));
                bean.setCodigoEtapaProspecto(cursor.getInt(7));
                bean.setNombres(cursor.getString(8));
                bean.setApellidoPaterno(cursor.getString(9));
                bean.setApellidoMaterno(cursor.getString(10));
                lista.add(bean);
            } while(cursor.moveToNext());
        }

        Log.i("TAG","lista"+lista);


        return lista;
    }

    public static  ArrayList<ReunionInternaBean> getReunionesEntreFechas(String fechaInicio, String fechaFin) {
        ArrayList<ReunionInternaBean> lista =  new ArrayList<>();

        //TODO Se debe agregar que las citas sean >= al dia actual
        String sql = String.format(
                "\n" +
                        "SELECT \n" +
                        "\tIDREUNIONINTERNA,\n" +
                        "\tCODIGOINTERMEDIARIO,\n" +
                        "\tIDREUNIONINTERNADISPOSITIVO,\t\n" +
                        "\tFECHAREUNION,\n" +
                        "\tHORAINICIO,\n" +
                        "\tHORAFIN,\n" +
                        "\tUBICACION,\n" +
                        "\tFLAGINVITADOGU,\n" +
                        "\tFLAGINVITADOGA,\n" +
                        "\tRECORDATORIO,\n" +
                        "\tCODIGOTIPOREUNION,\n" +
                        "\tFECHACREACIONDISPOSITIVO,\n" +
                        "\tFECHAMODIFICACIONDISPOSITIVO,\n" +
                        "\tFLAGENVIADO\n" +
                        "FROM\n" +
                        "\tTBL_REUNION_INTERNA\n" +
                        "WHERE FECHAREUNION BETWEEN '%S' AND '%S' \n" +
                        "ORDER BY FECHAREUNION ASC,HORAINICIO ASC",
                fechaInicio,
                fechaFin
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        ReunionInternaBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new ReunionInternaBean();
                bean.setIdReunionInterna(cursor.getInt(0));
                bean.setCodigoIntermediario(cursor.getInt(1));
                bean.setIdReunionInternaDispositivo(cursor.getInt(2));
                bean.setFechaReunion(cursor.getString(3));
                bean.setHoraInicio(cursor.getString(4));
                bean.setHoraFin(cursor.getString(5));
                bean.setUbicacion(cursor.getString(6));
                bean.setFlagInvitadoGU(cursor.getInt(7));
                bean.setFlagInvitadoGA(cursor.getInt(8));
                bean.setRecordatorio(cursor.getString(9));
                bean.setCodigoTipoReunion(cursor.getInt(10));
                bean.setFechaCreacionDispositivo(cursor.getString(11));
                bean.setFechaModificacionDispositivo(cursor.getString(12));
                bean.setFlagEnviado(cursor.getInt(13));

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }

    public static ArrayList<EventoBean> obtenerEventosSemana(String fechaInicio, String fechaFin)
    {
        ArrayList<EventoBean> lista =  new ArrayList<>();

        //TODO Se debe agregar que las citas sean >= al dia actual
        String sql = String.format(
                "SELECT \n" +
                        "\t\t1 AS TIPOEVENTO,\n" +
                        "\t\tC.IDCITADISPOSITIVO AS IDEVENTODISPOSITIVO,\n" +
                        "\t\tC.FECHACITA AS FECHA,\n" +
                        "\t\tC.HORAINICIO AS HORAINICIO,\n" +
                        "\t\tC.HORAFIN AS HORAFIN,\n" +
                        "\t\tC.UBICACION,\n" +
                        "\t\tC.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tP.NOMBRES AS PROSPECTONOMBRES,\n" +
                        "\t\tP.APELLIDOPATERNO AS PROSPECTOAPELLIDOPATERNO,\n" +
                        "\t\tP.APELLIDOMATERNO AS PROSPECTOAPELLIDOMATERNO,\n" +
                        "\t\t-1 AS CODIGOTIPOREUNION,\n" +
                        "\t\tC.CODIGOETAPAPROSPECTO AS CODIGOETAPACITA,\n" +
                        "\t\tP.TELEFONOCELULAR AS PROSPECTOTELEFONOCELULAR\n" +
                        "\tFROM\n" +
                        "\t\tTBL_CITA C\n" +
                        "\tINNER JOIN TBL_PROSPECTO P ON \n" +
                        "\t\tC.IDPROSPECTODISPOSITIVO = P.IDPROSPECTODISPOSITIVO\n" +
                        "\tWHERE \n" +
                        "\t\tC.FECHACITA BETWEEN  '%S' AND '%S' AND (C.CODIGOESTADO = 2  OR C.CODIGOESTADO = 3) \n" +
                        "UNION \n" +
                        "\tSELECT \n" +
                        "\t\t2 AS TIPOEVENTO,\n" +
                        "\t\tR.IDREUNIONINTERNADISPOSITIVO AS IDEVENTODISPOSITIVO,\n" +
                        "\t\tR.FECHAREUNION AS FECHA,\n" +
                        "\t\tR.HORAINICIO AS HORAINICIO,\n" +
                        "\t\tR.HORAFIN AS HORAFIN,\n" +
                        "\t\tR.UBICACION,\n" +
                        "\t\t-1 AS IDPROSPECTODISPOSITIVO,\n" +
                        "\t\t'' AS PROSPECTONOMBRES,\n" +
                        "\t\t'' AS PROSPECTOAPELLIDOPATERNO,\n" +
                        "\t\t'' AS PROSPECTOAPELLIDOMATERNO,\n" +
                        "\t\tR.CODIGOTIPOREUNION,  \n" +
                        "\t\t-1 AS CODIGOETAPACITA,\n" +
                        "\t\t'' AS PROSPECTOTELEFONOCELULAR\n" +
                        "\tFROM\n" +
                        "\t\tTBL_REUNION_INTERNA R\n" +
                        "\tWHERE \n" +
                        "\t\tR.FECHAREUNION BETWEEN  '%S' AND '%S' \n" +
                        "UNION\n" +
                        "\tSELECT \n" +
                        "\t\t3 AS TIPOEVENTO,\n" +
                        "\t\tREC.IDRECORDATORIOLLAMADADISPOSITIVO AS IDEVENTODISPOSITIVO,\n" +
                        "\t\tSUBSTR(REC.FECHARECORDATORIO,0,11) AS FECHA,\n" +
                        "\t\tSUBSTR(REC.FECHARECORDATORIO,12,5) AS HORAINICIO,\n" +
                        "\t\t'' AS HORAFIN,\n" +
                        "\t\t'' AS UBICACION,\n" +
                        "\t\tREC.IDPROSPECTODISPOSITIVO AS IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tP2.NOMBRES AS PROSPECTONOMBRES,\n" +
                        "\t\tP2.APELLIDOPATERNO AS PROSPECTOAPELLIDOPATERNO,\n" +
                        "\t\tP2.APELLIDOMATERNO AS PROSPECTOAPELLIDOMATERNO,\n" +
                        "\t\t-1 AS CODIGOTIPOREUNION,\n" +
                        "\t\t-1 AS CODIGOETAPACITA,\n" +
                        "\t\tP2.TELEFONOCELULAR AS PROSPECTOTELEFONOCELULAR\n" +
                        "\tFROM\n" +
                        "\t\tTBL_RECORDATORIO_LLAMADA REC\n" +
                        "\tINNER JOIN TBL_PROSPECTO P2 ON \n" +
                        "\t\tREC.IDPROSPECTODISPOSITIVO = P2.IDPROSPECTODISPOSITIVO\n" +
                        "\tWHERE \n" +
                        "\t\tSUBSTR(REC.FECHARECORDATORIO,0,11)  BETWEEN  '%S' AND '%S' AND FLAGACTIVO = 1\n" +
                        "ORDER BY FECHA ASC, HORAINICIO ASC",
                fechaInicio,
                fechaFin,
                fechaInicio,
                fechaFin,
                fechaInicio,
                fechaFin
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        EventoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new EventoBean();
                bean.setTipoEvento(cursor.getInt(0));
                bean.setIdEventoDispositivo(cursor.getInt(1));
                bean.setFecha(cursor.getString(2));
                bean.setHoraInicio(cursor.getString(3));
                bean.setHoraFin(cursor.getString(4));
                bean.setUbicacion(cursor.getString(5));
                bean.setIdProspectoDispositivo(cursor.getInt(6));
                bean.setNombres(cursor.getString(7));
                bean.setApellidoPaterno(cursor.getString(8));
                bean.setApellidoMaterno(cursor.getString(9));
                bean.setCodigoTipoReunion(cursor.getInt(10));
                bean.setEtapaCita(cursor.getInt(11));
                bean.setTelefonoCelular(cursor.getString(12));

                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }

    /*public static ArrayList<ProspectoBean>CargarCumpleañosEntreFechas(String fechaInicio, String fechaFin){

    }*/

    /*public static  ArrayList<CitaBean> obtenerCitasSinEnviar() {

        ArrayList<Entity> arrBeans = CitaBean.tableHelper.getEntities("FlagEnviado = 0", null);
        ArrayList<CitaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((CitaBean)entity);
        }
        return arrBeanFinal;
    }

    public static  ArrayList<CitaMovimientoEstadoBean> obtenerCitasMovimientoEstadoSinEnviar() {

        ArrayList<Entity> arrBeans = CitaMovimientoEstadoBean.tableHelper.getEntities("FlagEnviado = 0", null);
        ArrayList<CitaMovimientoEstadoBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((CitaMovimientoEstadoBean) entity);
        }
        return arrBeanFinal;
    }


    public static  ArrayList<ProspectoMovimientoEtapaBean> obtenerProspectoMovimientoEtapa() {

        ArrayList<Entity> arrBeans = ProspectoMovimientoEtapaBean.tableHelper.getEntities("FlagEnviado = 0", null);
        ArrayList<ProspectoMovimientoEtapaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((ProspectoMovimientoEtapaBean) entity);
        }
        return arrBeanFinal;
    }

    */


    ///////
    public static void ObtenerIDCita(){
        //select , si no existe inserta sino suma 1
    }

    //public static CitaBean ObteniendoUltimaCitaProspectoxEtapa(ProspectoBean prospectoBean)  // Obtener Cita Vigente


    /*
    // Metodo que escoge id_dispositivo si no tiene id
    public static CitaBean ObteniendoUltimaCitaProspecto(ProspectoBean prospectoBean)  // Obtener Cita Vigente
    {
        String sql = "";

        String idUsado = CitaBean.CN_IDPROSPECTODISPOSITIVO;
        int valorIdUsado = prospectoBean.getIdProspectoDispositivo();

        if (prospectoBean.getIdProspecto() != -1) {
            idUsado = CitaBean.CN_IDPROSPECTO;
            valorIdUsado = prospectoBean.getIdProspecto();
        }

        sql = String.format(
                "select *" +
                        " from " + DatabaseConstants.TBL_CITA +
                        " where" +
                        " " + idUsado + " = %s" +
                        //" AND " + CitaBean.CN_CODIGOETAPAPROSPECTO + " = %s" +
                        " order by " + CitaBean.CN_NUMEROENTREVISTA + " desc" +
                        " LIMIT 1",
                valorIdUsado//,
                //prospectoBean.getCodigoEtapa()
        );
        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        CitaBean bean= null;
        if (cursor.moveToFirst()) {
            do {
                bean = new CitaBean();
                bean.setIdCita(cursor.getInt(0));
                bean.setIdProspecto(cursor.getInt(1));
                bean.setIdProspectoDispositivo(cursor.getInt(2));
                bean.setIdCitaDispositivo(cursor.getInt(3));
                bean.setNumeroEntrevista(cursor.getInt(4));
                bean.setCodigoEstado(cursor.getInt(5));
                bean.setCodigoResultado(cursor.getInt(6));
                bean.setFechaCita(cursor.getString(7));
                bean.setHoraInicio(cursor.getString(8));
                bean.setHoraFin(cursor.getString(9));
                bean.setUbicacion(cursor.getString(10));
                bean.setReferenciaUbicacion(cursor.getString(11));
                bean.setFlagInvitadoGU(cursor.getInt(12));
                bean.setFlagInvitadoGA(cursor.getInt(13));
                bean.setAlertaMinutosAntes(cursor.getInt(14));
                bean.setCodigoIntermediarioCreacion(cursor.getInt(15));
                bean.setCodigoIntermediarioModificacion(cursor.getInt(16));
                bean.setCodigoEtapaProspecto(cursor.getInt(17));
                bean.setFechaCreacionDispositivo(cursor.getString(18));
                bean.setFechaModificacionDispositivo(cursor.getString(19));
                bean.setFlagEnviado(cursor.getInt(20));

            } while(cursor.moveToNext());
        }

        return bean;
    }*/

    // Metodo que valida directamente el idDispositivo
    public static CitaBean obtenerUltimaCitaProspecto(ProspectoBean prospectoBean)  // Obtener Cita Vigente
    {
        String sql = "";

        sql = "select " +
                CitaBean.CN_IDCITA +", "+
                CitaBean.CN_IDPROSPECTO +", "+
                CitaBean.CN_IDPROSPECTODISPOSITIVO +", "+
                CitaBean.CN_IDCITADISPOSITIVO +", "+
                CitaBean.CN_NUMEROENTREVISTA +", "+
                CitaBean.CN_CODIGOESTADO +", "+
                CitaBean.CN_CODIGORESULTADO +", "+
                CitaBean.CN_FECHACITA +", "+
                CitaBean.CN_HORAINICIO +", "+
                CitaBean.CN_HORAFIN +", "+
                CitaBean.CN_UBICACION +", "+
                CitaBean.CN_REFERENCIA_UBICACION +", "+
                CitaBean.CN_FLAGINVITADOGU +", "+
                CitaBean.CN_FLAGINVITADOGA +", "+
                CitaBean.CN_ALERTAMINUTOSANTES +", "+
                CitaBean.CN_CANTIDADVI +", "+
                CitaBean.CN_PRIMATARGETVI +", "+
                CitaBean.CN_CANTIDADAP +", "+
                CitaBean.CN_PRIMATARGETAP +", "+
                CitaBean.CN_CODIGOINTERMEDIARIOCREACION +", "+
                CitaBean.CN_CODIGOINTERMEDIARIOMODIFICACION +", "+
                CitaBean.CN_CODIGOETAPAPROSPECTO +", "+
                CitaBean.CN_FECHACREACIONDISPOSITIVO +", "+
                CitaBean.CN_FECHAMODIFICACIONDISPOSITIVO +", "+
                CitaBean.CN_FLAGENVIADO +
                        " from " + DatabaseConstants.TBL_CITA +
                        " where" +
                        " " + CitaBean.CN_IDPROSPECTODISPOSITIVO + " = " + prospectoBean.getIdProspectoDispositivo() +
                        " order by " + CitaBean.CN_NUMEROENTREVISTA + " desc" +
                        " LIMIT 1";

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        CitaBean bean= null;
        if (cursor.moveToFirst()) {
            do {
                bean = new CitaBean();
                bean.setIdCita(cursor.getInt(0));
                bean.setIdProspecto(cursor.getInt(1));
                bean.setIdProspectoDispositivo(cursor.getInt(2));
                bean.setIdCitaDispositivo(cursor.getInt(3));
                bean.setNumeroEntrevista(cursor.getInt(4));
                bean.setCodigoEstado(cursor.getInt(5));
                bean.setCodigoResultado(cursor.getInt(6));
                bean.setFechaCita(cursor.getString(7));
                bean.setHoraInicio(cursor.getString(8));
                bean.setHoraFin(cursor.getString(9));
                bean.setUbicacion(cursor.getString(10));
                bean.setReferenciaUbicacion(cursor.getString(11));
                bean.setFlagInvitadoGU(cursor.getInt(12));
                bean.setFlagInvitadoGA(cursor.getInt(13));
                bean.setAlertaMinutosAntes(cursor.getInt(14));
                bean.setCantidadVI(cursor.getInt(15));
                bean.setPrimaTargetVI(cursor.getInt(16));
                bean.setCantidadAP(cursor.getInt(17));
                bean.setPrimaTargetAP(cursor.getInt(18));
                bean.setCodigoIntermediarioCreacion(cursor.getInt(19));
                bean.setCodigoIntermediarioModificacion(cursor.getInt(20));
                bean.setCodigoEtapaProspecto(cursor.getInt(21));
                bean.setFechaCreacionDispositivo(cursor.getString(22));
                bean.setFechaModificacionDispositivo(cursor.getString(23));
                bean.setFlagEnviado(cursor.getInt(24));

            } while(cursor.moveToNext());
        }

        if (bean != null) // Si la cita no es nula se carga los recordatorios de llamadas
        {
            bean.setRecordatorioLlamadaBean(CitaReunionController.obtenerRecordatorioLLamadaporCitaDispositivo(bean.getIdCitaDispositivo()));
        }

        return bean;
    }


    // Devuelve el recordatorio de llamada asociado al dispositivo
    public static RecordatorioLlamadaBean obtenerRecordatorioLLamadaporCitaDispositivo(int idCitaDispositivo){

        String sql = "";

        sql = "select " +
                RecordatorioLlamadaBean.CN_IDRECORDATORIOLLAMADA + ", "+
                RecordatorioLlamadaBean.CN_IDRECORDATORIOLLAMADADISPOSITIVO + ", "+
                RecordatorioLlamadaBean.CN_IDPROSPECTO + ", "+
                RecordatorioLlamadaBean.CN_IDPROSPECTODISPOSITIVO + ", "+
                RecordatorioLlamadaBean.CN_IDCITA + ", "+
                RecordatorioLlamadaBean.CN_IDCITADISPOSITIVO + ", "+
                RecordatorioLlamadaBean.CN_FECHARECORDATORIO + ", "+
                RecordatorioLlamadaBean.CN_FLAGACTIVO + ", "+
                RecordatorioLlamadaBean.CN_FLAGENVIADO +
                " from " + DatabaseConstants.TBL_RECORDATORIO_LLAMADA +
                " where" +
                " " + RecordatorioLlamadaBean.CN_IDCITADISPOSITIVO + " = " + idCitaDispositivo + " AND"+
                " " + RecordatorioLlamadaBean.CN_FLAGACTIVO + " = " + Constantes.FLAG_ACTIVO +
                " LIMIT 1";

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        RecordatorioLlamadaBean recordatorio= null;
        if (cursor.moveToFirst()) {
            do {
                recordatorio = new RecordatorioLlamadaBean();
                recordatorio.setIdRecordatorioLlamada(cursor.getInt(0));
                recordatorio.setIdRecordatorioLlamadaDispositivo(cursor.getInt(1));
                recordatorio.setIdProspecto(cursor.getInt(2));
                recordatorio.setIdProspectoDispositivo(cursor.getInt(3));
                recordatorio.setIdCita(cursor.getInt(4));
                recordatorio.setIdCitaDispositivo(cursor.getInt(5));
                recordatorio.setFechaRecordatorio(cursor.getString(6));
                recordatorio.setFlagActivo(cursor.getInt(7));
                recordatorio.setFlagEnviado(cursor.getInt(8));
            } while (cursor.moveToNext());
        }

        return recordatorio;
    }


    /*
    public void ObtenerUltimoMovimientoEstado()
    {
        //ArrayList<CitaProspectoBean> lista =  new ArrayList<>();

        //TODO Se debe agregar que las citas sean >= al dia actual
        String sql = String.format(
                "select TOP 1 IDMOVIMIENTO" +
                        " ,IDCITA " +
                        " ,IDMOVIMIENTODISPOSITIVO " +
                        " ,CODIGOESTADO " +
                        " ,CODIGORESULTADO " +
                        " ,FECHAMOVIMIENTOESTADODISPOSITIVO " +
                        " ,IDCITADISPOSITIVO " +
                        " ,FLAGENVIADO " +
                        " from "+DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO+
                        " where " +
                        " FECHACITA BETWEEN '%s' AND '%s'" +
                        " and c.codigoestado = %s",
                        fechaInicio,
                        fechaFin,
                Constantes.EstadoCita_Agendada  // TODO: VER SI SE MUESTRAN TAMBIEN ALS REALIZADAS
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        CitaProspectoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new CitaProspectoBean();
                bean.setIdCita(cursor.getString(0));
                bean.setFechaCita(cursor.getString(1));
                bean.setHoraIncioCita(cursor.getString(2));
                bean.setHoraFinCita(cursor.getString(3));
                bean.setNombres(cursor.getString(4));
                bean.setApellidoPaterno(cursor.getString(5));
                bean.setApellidoMaterno(cursor.getString(6));
                lista.add(bean);
            } while(cursor.moveToNext());
        }

        return lista;
    }*/

    public static void guardarRecordatorioLLamada_GenerarIDDispositivo(RecordatorioLlamadaBean recordatorioLLamadaBean)
    {

        recordatorioLLamadaBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
        // Guardando ID
        TablaIdentificadorBean identificadorRecordatorioLLamadaBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_RECORDATORIO_LLAMADA);

        recordatorioLLamadaBean.setIdRecordatorioLlamadaDispositivo(identificadorRecordatorioLLamadaBean.getIdentity()+1);
        long row = RecordatorioLlamadaBean.tableHelper.insertEntity(recordatorioLLamadaBean);

        if (row != -1) // SI ES -1, HUBO UN PROBLEMA
        {
            identificadorRecordatorioLLamadaBean.setIdentity(identificadorRecordatorioLLamadaBean.getIdentity()+1);
            TablaIdentificadorController.actualizarTablaIdentificador(identificadorRecordatorioLLamadaBean);
        }
    }

    ////////// Métodos para envíos de Pendientes
    public static ArrayList<CitaBean> obtenerCitasSinEnviar() {
        ArrayList<Entity> arrBeans = CitaBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<CitaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((CitaBean)entity);
        }
        return arrBeanFinal;
    }

    public static  ArrayList<CitaMovimientoEstadoBean> obtenerCitasMovimientoEstadoSinEnviar() {
        ArrayList<Entity> arrBeans = CitaMovimientoEstadoBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<CitaMovimientoEstadoBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((CitaMovimientoEstadoBean) entity);
        }
        return arrBeanFinal;
    }

    public static  ArrayList<RecordatorioLlamadaBean> obtenerRecordatorioLlamadasSinEnviar() {
        ArrayList<Entity> arrBeans = RecordatorioLlamadaBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<RecordatorioLlamadaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((RecordatorioLlamadaBean) entity);
        }
        return arrBeanFinal;
    }

    public static  ArrayList<RecordatorioLlamadaBean> obtenerRecordatoriosporCitaDispositivo(int idCitaDispositivo) {
        String[] parametros = {String.valueOf(idCitaDispositivo),String.valueOf(Constantes.FLAG_ACTIVO)};

        ArrayList<Entity> arrBeans = RecordatorioLlamadaBean.tableHelper.getEntities("IDCITADISPOSITIVO = ?  AND FLAGACTIVO = ?", null);
        ArrayList<RecordatorioLlamadaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((RecordatorioLlamadaBean) entity);
        }
        return arrBeanFinal;
    }

    public static void limpiarTablaRecordatorioLlamada(){
        RecordatorioLlamadaBean.tableHelper.deleteAllEntities();
    }

    public static  ArrayList<ReunionInternaBean> obtenerReunionInternasSinEnviar(){
        ArrayList<Entity> arrBeans = ReunionInternaBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<ReunionInternaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((ReunionInternaBean)entity);
        }
        return arrBeanFinal;
    }

    /*
    // Todas las citas con fecha de creacion al dia de hoy que no sean estado reagendado
    public static int obtenerTotalAgendadosDia()
    {
        int total = 0;

        String fecha_actual = Util.obtenerFechaActual_SoloFecha();

        String sql = String.format(
                "SELECT \n" +
                        "            count (1) as total\n" +
                        "FROM TBL_PROSPECTO P\n" +
                        "LEFT JOIN \n" +
                        " (SELECT \n" +
                        "\t\tMAX(C.NUMEROENTREVISTA),\n" +
                        "\t\tC.FECHACITA,\n" +
                        "\t\tC.CODIGOESTADO,\n" +
                        "\t\tC.CODIGORESULTADO,\n" +
                        "\t\tC.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tC.FECHACREACIONDISPOSITIVO\n" +
                        "  FROM \n" +
                        "\t\tTBL_CITA  C\n" +
                        "  GROUP BY C.IDPROSPECTODISPOSITIVO\n" +
                        ") ULTCITAS\n" +
                        "ON P.IDPROSPECTODISPOSITIVO = ULTCITAS.IDPROSPECTODISPOSITIVO\n" +
                        "WHERE \n" +
                        //"\tULTCITAS.CODIGOESTADO = 2 AND\n" +
                       // "\tULTCITAS.CODIGORESULTADO = 0 AND\n" +
                        "\tULTCITAS.CODIGOESTADO <> 4 AND\n" + // ES DIFERENTE DE REAGENDADA
                        "\tSUBSTR(ULTCITAS.FECHACREACIONDISPOSITIVO,0,11)  = '%s' ",
                fecha_actual
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {
                total = cursor.getInt(0);
                break;
            } while(cursor.moveToNext());
        }

        return total;
    }*/


    // CANTIDAD DE PROSPECTOS CON MOVIMIENTOS DE CITA A AGENDADO EL DIA DE HOY
    public static int obtenerTotalAgendadosDia(){
        int total = 0;

        String fecha_actual = Util.obtenerFechaActual_SoloFecha();

        String sql = String.format(
                "SELECT \n" +
                        "\t\tcount(1)\n" +
                        "FROM\n" +
                        "(\n" +
                        "\tSELECT DISTINCT C.IDPROSPECTODISPOSITIVO FROM\n" +
                        "\t(\n" +
                        "\t\tSELECT IDCITADISPOSITIVO FROM TBL_CITA_MOVIMIENTO_ESTADO \n" +
                        "\t\t\t\tWHERE \n" +
                        "\t\t\t\t\t\tSUBSTR(FECHAMOVIMIENTOESTADODISPOSITIVO,0,11)= '%S'  AND\n" +
                        "\t\t\t\t\t    CODIGOESTADO = 2 \n" + // MOVIMIENTO A AGENDADO
                        "\t)MOV\n" +
                        "\tINNER JOIN\n" +
                        "\tTBL_CITA  C\n" +
                        "\tON  MOV.IDCITADISPOSITIVO = C.IDCITADISPOSITIVO\n" +
                        ")CMOV\n" +
                        "INNER JOIN \n" +
                        "TBL_PROSPECTO P\n" +
                        "ON P.IDPROSPECTODISPOSITIVO = CMOV.IDPROSPECTODISPOSITIVO\n",
                fecha_actual
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                total = cursor.getInt(0);
                break;
            } while(cursor.moveToNext());
        }

        return total;
    }

    // CANTIDAD DE PROSPECTOS CON MOVIMIENTOS DE CITA A AGENDADO O POR CONTACTAR EL DIA DE HOY
    public static int obtenerTotalContactadosDia()
    {
        int total = 0;

        String fecha_actual = Util.obtenerFechaActual_SoloFecha();

        String sql = String.format(
                "SELECT \n" +
                        "\t\tcount(1)\n" +
                        "FROM\n" +
                        "(\n" +
                        "\tSELECT DISTINCT C.IDPROSPECTODISPOSITIVO FROM\n" +
                        "\t(\n" +
                        "\t\tSELECT IDCITADISPOSITIVO FROM TBL_CITA_MOVIMIENTO_ESTADO \n" +
                        "\t\t\t\tWHERE \n" +
                        "\t\t\t\t\t\tSUBSTR(FECHAMOVIMIENTOESTADODISPOSITIVO,0,11)= '%S'  AND\n" +
                        "\t\t\t\t\t    (CODIGOESTADO = 2 OR  CODIGOESTADO = 1)\n" + // MOVIMIENTO A AGENDADO O POR CONTACTAR
                        "\t)MOV\n" +
                        "\tINNER JOIN\n" +
                        "\tTBL_CITA  C\n" +
                        "\tON  MOV.IDCITADISPOSITIVO = C.IDCITADISPOSITIVO\n" +
                        ")CMOV\n" +
                        "INNER JOIN \n" +
                        "TBL_PROSPECTO P\n" +
                        "ON P.IDPROSPECTODISPOSITIVO = CMOV.IDPROSPECTODISPOSITIVO\n",
                fecha_actual
        );

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                total = cursor.getInt(0);
                break;
            } while(cursor.moveToNext());
        }

        return total;
    }

    public static CitaMovimientoEstadoBean obtenerCitaMovimientoEstadoPorIdMovimientoDispositivo(int idMovimientoDispositivo){
        String[] parametros = {Integer.toString(idMovimientoDispositivo)};
        ArrayList<Entity> arrCitaMovimientoEstados = CitaMovimientoEstadoBean.tableHelper.getEntities("IDMOVIMIENTODISPOSITIVO = ?", parametros);
        if(arrCitaMovimientoEstados.size() > 0){
            return (CitaMovimientoEstadoBean)arrCitaMovimientoEstados.get(0);
        }else{
            return null;
        }
    }

    public static void actualizarCitaMovimientoEstado(CitaMovimientoEstadoBean citaMovimientoEstadoBean){
        String[] parametros = {Integer.toString(citaMovimientoEstadoBean.getIdMovimientoDispositivo())};
        CitaMovimientoEstadoBean.tableHelper.updateEntity(citaMovimientoEstadoBean, "IDMOVIMIENTODISPOSITIVO = ?", parametros);
    }

    public static RecordatorioLlamadaBean obtenerRecordatorioLlamadaPorIdRecordatorioLlamadaDispositivo(int idRecordatorioLlamadaDispositivo){
        String[] parametros = {Integer.toString(idRecordatorioLlamadaDispositivo)};
        ArrayList<Entity> arrRecordatorioLlamadas = RecordatorioLlamadaBean.tableHelper.getEntities("IDRECORDATORIOLLAMADADISPOSITIVO = ?", parametros);
        if(arrRecordatorioLlamadas.size() > 0){
            return (RecordatorioLlamadaBean)arrRecordatorioLlamadas.get(0);
        }else{
            return null;
        }
    }

    public static void actualizarRecordatorioLlamada(RecordatorioLlamadaBean recordatorioLlamadaBean){

        String[] parametros = {Integer.toString(recordatorioLlamadaBean.getIdRecordatorioLlamadaDispositivo())};
        RecordatorioLlamadaBean.tableHelper.updateEntity(recordatorioLlamadaBean, "IDRECORDATORIOLLAMADADISPOSITIVO = ?", parametros);
    }

    public static void limpiarTablaCita(){
        CitaBean.tableHelper.deleteAllEntities();
    }

    public static void limpiarTablaCitaMovimiento(){
        CitaMovimientoEstadoBean.tableHelper.deleteAllEntities();
    }

    // Trae el recordatorio de llamada que no tenga cita (idcitaprospectodispositivo -1)
    public static  RecordatorioLlamadaBean obtenerRecordatorioLlamadaDirectoPorIdProspectoDispositivo(int idProspectoDispositivo) {
        String [] params  = new String[]{"1","-1",Integer.toString(idProspectoDispositivo)};
        ArrayList<Entity> arrBeans = RecordatorioLlamadaBean.tableHelper.getEntities("FLAGACTIVO = ? AND IDCITADISPOSITIVO = ? AND IDPROSPECTODISPOSITIVO = ?", params);
        ArrayList<RecordatorioLlamadaBean> arrBeanFinal = new ArrayList<>();

        for(Entity entity : arrBeans){
            return (RecordatorioLlamadaBean) entity;
        }
        return null;
    }

    public static void desactivarRecordatoriosIndependientesProspectoDispositivo(int idProspectoDispositivo)
    {
        // SE DESACTIVA EL RECORDATORIO DE LLAMADA YA EXISTENTE (DE HABERLO)
        String sql = "";

        sql = "update " + DatabaseConstants.TBL_RECORDATORIO_LLAMADA +
                " set " + RecordatorioLlamadaBean.CN_FLAGACTIVO + " = " + Constantes.FLAG_INACTIVO+ ","+
                RecordatorioLlamadaBean.CN_FECHAMODIFICACIONDISPOSITIVO + " = '" + Util.obtenerFechaActual()+ "',"+
                RecordatorioLlamadaBean.CN_FLAGENVIADO + " = " + Constantes.ENVIO_PENDIENTE +
                " where" +
                " " + RecordatorioLlamadaBean.CN_IDCITADISPOSITIVO + " = -1 AND" +
                " " + RecordatorioLlamadaBean.CN_IDPROSPECTODISPOSITIVO + " = "+ idProspectoDispositivo +";";

        AgendaApplication.getDB().beginTransaction();
        AgendaApplication.getDB().execSQL(sql);
        AgendaApplication.getDB().setTransactionSuccessful();
        AgendaApplication.getDB().endTransaction();
    }

    public static void guardarAlertaCita(CitaBean citaBean, Context context, String nombreCompletoProspecto)
    {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date d = null;
        try {
            d = f.parse(citaBean.getFechaCita() + " " + citaBean.getHoraInicio());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (d != null) {
            long milliseconds = d.getTime() - citaBean.getAlertaMinutosAntes()*60*1000;
            ReminderManager reminderManager = new ReminderManager(context);
            ReminderBean reminderBean = new ReminderBean();
            reminderBean.setTitle("Tienes una cita.");

            String texto = "Tu cita con "+ nombreCompletoProspecto +" comienza en " ;

            int horas = citaBean.getAlertaMinutosAntes()/60;  int minutos = citaBean.getAlertaMinutosAntes() - horas*60;

            String textotiempoantes = "";
            if (horas ==1)
                textotiempoantes = textotiempoantes + String.valueOf(horas) +" hora ";
            if (horas > 1)
                textotiempoantes = textotiempoantes + String.valueOf(horas) +" horas ";
            if (minutos == 1)
                textotiempoantes = textotiempoantes + String.valueOf(minutos) +" minuto ";
            if (minutos > 1)
                textotiempoantes = textotiempoantes + String.valueOf(minutos) +" minutos ";

            texto = texto + textotiempoantes.substring(0,textotiempoantes.length()-1) + ".";

            reminderBean.setBody(texto);
            reminderBean.setReminderDateTime(String.valueOf(milliseconds));
            reminderBean.setFuenteAlerta(Constantes.alerta_cita);
            reminderBean.setIdDispositivoFuente(citaBean.getIdCitaDispositivo()); // Se seteo al grabar en BD
            int id = reminderManager.addReminder(reminderBean);
        }
    }

    public static void guardarAlertaResultadoCita(CitaBean citaBean, Context context, String nombreCompletoProspecto)
    {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date d = null;
        try {
            d = f.parse(citaBean.getFechaCita() + " " + citaBean.getHoraFin());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (d != null) {
            long milliseconds = d.getTime();
            ReminderManager reminderManager = new ReminderManager(context);
            ReminderBean reminderBean = new ReminderBean();
            reminderBean.setTitle("Entrevista finalizada.");

            String texto = "¿Cómo te fue en tu entrevista con "+ nombreCompletoProspecto +"?" ;

            reminderBean.setBody(texto);
            reminderBean.setReminderDateTime(String.valueOf(milliseconds));
            reminderBean.setFuenteAlerta(Constantes.alerta_post_cita);
            reminderBean.setIdDispositivoFuente(citaBean.getIdCitaDispositivo()); // Se seteo al grabar en BD
            int id = reminderManager.addReminder(reminderBean);
        }
    }

    public static void guardarAlertaRecordatorioCita(CitaBean citaBean,Context context, String nombreCompletoProspecto)
    {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date d = null;
        try {
            d = f.parse(citaBean.getRecordatorioLlamadaBean().getFechaRecordatorio());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (d != null) {
            long milliseconds = d.getTime();
            ReminderManager reminderManager = new ReminderManager(context);
            ReminderBean reminderBean = new ReminderBean();
            reminderBean.setTitle("Tienes un recordatorio de llamada.");

            String texto = "No olvides llamar a : "+ nombreCompletoProspecto;
            reminderBean.setBody(texto);
            reminderBean.setReminderDateTime(String.valueOf(milliseconds));
            reminderBean.setFuenteAlerta(Constantes.alerta_recordatorio);
            reminderBean.setIdDispositivoFuente(citaBean.getRecordatorioLlamadaBean().getIdRecordatorioLlamadaDispositivo()); // Se seteo al grabar en BD
            int id = reminderManager.addReminder(reminderBean);
        }
    }


    public static boolean validarCitaMismoHorario(CitaBean citaBean){ // Nos e usa between par avalidar extremos
        String [] params  = new String[]{String.valueOf(citaBean.getIdCitaDispositivo()),citaBean.getFechaCita(),
                citaBean.getHoraInicio(),citaBean.getHoraFin(),
                citaBean.getHoraInicio(),citaBean.getHoraFin(),
                citaBean.getHoraInicio(),citaBean.getHoraInicio()};
        ArrayList<Entity> arrBeans = CitaBean.tableHelper.getEntities("CODIGOESTADO=2 AND IDCITADISPOSITIVO <> ? AND FECHACITA = ? AND " +
                "(" +
                    "(HORAINICIO >= ? AND HORAINICIO <?) OR (HORAFIN > ? AND HORAFIN <=?) OR" +
                    "(? >= HORAINICIO AND ? < HORAFIN) "+//"OR (? > HORAINICIO AND ? <= HORAFIN) " + // para validar cuando una cita contiene en su totalidad la otra
                ")", params);

        String [] paramsReuniones  = new String[]{citaBean.getFechaCita(),
                citaBean.getHoraInicio(),citaBean.getHoraFin(),
                citaBean.getHoraInicio(),citaBean.getHoraFin(),
                citaBean.getHoraInicio(),citaBean.getHoraInicio()};
        ArrayList<Entity> arrBeansReuniones = ReunionInternaBean.tableHelper.getEntities("FECHAREUNION = ? AND " +
                "(" +
                    "(HORAINICIO >= ? AND HORAINICIO <?) OR (HORAFIN > ? AND HORAFIN <=?) OR" +
                    "(? >= HORAINICIO AND ? < HORAFIN) "+//"OR (? > HORAINICIO AND ? <= HORAFIN) " + // para validar cuando una cita contiene en su totalidad la otra
                ") ", paramsReuniones);

        if (arrBeans.size() >0 || arrBeansReuniones.size() >0) return true;
        return false;
    }

    public static boolean validarCitaMismoHorarioReunion(ReunionInternaBean reunionBean){
        String [] params  = new String[]{reunionBean.getFechaReunion(),
                reunionBean.getHoraInicio(),reunionBean.getHoraFin(),
                reunionBean.getHoraInicio(),reunionBean.getHoraFin(),
                reunionBean.getHoraInicio(),reunionBean.getHoraInicio()};
        ArrayList<Entity> arrBeans = CitaBean.tableHelper.getEntities("CODIGOESTADO=2 AND FECHACITA = ? AND " +
                "(" +
                    "(HORAINICIO >= ? AND HORAINICIO <?) OR (HORAFIN > ? AND HORAFIN <=?) OR" +
                    "(? >= HORAINICIO AND ? < HORAFIN) "+//"OR (? > HORAINICIO AND ? <= HORAFIN) " + // para validar cuando una cita contiene en su totalidad la otra
                ")", params);

        String [] paramsReuniones  = new String[]{String.valueOf(reunionBean.getIdReunionInternaDispositivo()),reunionBean.getFechaReunion(),
                reunionBean.getHoraInicio(),reunionBean.getHoraFin(),
                reunionBean.getHoraInicio(),reunionBean.getHoraFin(),
                reunionBean.getHoraInicio(),reunionBean.getHoraInicio()};
        ArrayList<Entity> arrBeansReuniones = ReunionInternaBean.tableHelper.getEntities("IDREUNIONINTERNADISPOSITIVO <> ? AND FECHAREUNION = ? AND " +
                "(" +
                    "(HORAINICIO >= ? AND HORAINICIO <?) OR (HORAFIN > ? AND HORAFIN <=?) OR" +
                    "(? >= HORAINICIO AND ? < HORAFIN) "+//"OR (? > HORAINICIO AND ? <= HORAFIN) " + // para validar cuando una cita contiene en su totalidad la otra
                ") ", paramsReuniones);

        if (arrBeans.size() >0 || arrBeansReuniones.size() >0) return true;
        return false;
    }

    public static boolean validarCitaVentaRealizada(int idProspectoDispositivo){
        String[] parametros = {Integer.toString(Constantes.ResultadoCita_CierreVenta),Integer.toString(idProspectoDispositivo)};
        ArrayList<Entity> arrCitas = CitaBean.tableHelper.getEntities("CODIGORESULTADO = ? AND IDPROSPECTODISPOSITIVO = ?", parametros);
        if(arrCitas.size() > 0){
            return true;
        }else{
            return false;
        }
    }

    public static boolean hayAlgunaCita(){
        ArrayList<Entity> arrBeans = CitaBean.tableHelper.getEntities("", null);
        return arrBeans.size() > 0;
    }

    public static CitaBean obtenerUltimaCitaRealizadaProspectoDispositivo(int idProspectoDispositivo)
    {
        String sql = "";

        sql = "select " +
                CitaBean.CN_IDCITA +", "+
                CitaBean.CN_IDPROSPECTO +", "+
                CitaBean.CN_IDPROSPECTODISPOSITIVO +", "+
                CitaBean.CN_IDCITADISPOSITIVO +", "+
                CitaBean.CN_NUMEROENTREVISTA +", "+
                CitaBean.CN_CODIGOESTADO +", "+
                CitaBean.CN_CODIGORESULTADO +", "+
                CitaBean.CN_FECHACITA +", "+
                CitaBean.CN_HORAINICIO +", "+
                CitaBean.CN_HORAFIN +", "+
                CitaBean.CN_UBICACION +", "+
                CitaBean.CN_REFERENCIA_UBICACION +", "+
                CitaBean.CN_FLAGINVITADOGU +", "+
                CitaBean.CN_FLAGINVITADOGA +", "+
                CitaBean.CN_ALERTAMINUTOSANTES +", "+
                CitaBean.CN_CANTIDADVI +", "+
                CitaBean.CN_PRIMATARGETVI +", "+
                CitaBean.CN_CANTIDADAP +", "+
                CitaBean.CN_PRIMATARGETAP +", "+
                CitaBean.CN_CODIGOINTERMEDIARIOCREACION +", "+
                CitaBean.CN_CODIGOINTERMEDIARIOMODIFICACION +", "+
                CitaBean.CN_CODIGOETAPAPROSPECTO +", "+
                CitaBean.CN_FECHACREACIONDISPOSITIVO +", "+
                CitaBean.CN_FECHAMODIFICACIONDISPOSITIVO +", "+
                CitaBean.CN_FLAGENVIADO +
                " from " + DatabaseConstants.TBL_CITA +
                " where" +
                " " + CitaBean.CN_IDPROSPECTODISPOSITIVO + " = " + idProspectoDispositivo + " AND"+
                " " + CitaBean.CN_CODIGOESTADO + " = " + Constantes.EstadoCita_Realizada +
                " order by " + CitaBean.CN_NUMEROENTREVISTA + " desc" +
                " LIMIT 1";

        Cursor cursor = AgendaApplication.getDB().rawQuery(sql, null);

        CitaBean bean= null;
        if (cursor.moveToFirst()) {
            do {
                bean = new CitaBean();
                bean.setIdCita(cursor.getInt(0));
                bean.setIdProspecto(cursor.getInt(1));
                bean.setIdProspectoDispositivo(cursor.getInt(2));
                bean.setIdCitaDispositivo(cursor.getInt(3));
                bean.setNumeroEntrevista(cursor.getInt(4));
                bean.setCodigoEstado(cursor.getInt(5));
                bean.setCodigoResultado(cursor.getInt(6));
                bean.setFechaCita(cursor.getString(7));
                bean.setHoraInicio(cursor.getString(8));
                bean.setHoraFin(cursor.getString(9));
                bean.setUbicacion(cursor.getString(10));
                bean.setReferenciaUbicacion(cursor.getString(11));
                bean.setFlagInvitadoGU(cursor.getInt(12));
                bean.setFlagInvitadoGA(cursor.getInt(13));
                bean.setAlertaMinutosAntes(cursor.getInt(14));
                bean.setCantidadVI(cursor.getInt(15));
                bean.setPrimaTargetVI(cursor.getInt(16));
                bean.setCantidadAP(cursor.getInt(17));
                bean.setPrimaTargetAP(cursor.getInt(18));
                bean.setCodigoIntermediarioCreacion(cursor.getInt(19));
                bean.setCodigoIntermediarioModificacion(cursor.getInt(20));
                bean.setCodigoEtapaProspecto(cursor.getInt(21));
                bean.setFechaCreacionDispositivo(cursor.getString(22));
                bean.setFechaModificacionDispositivo(cursor.getString(23));
                bean.setFlagEnviado(cursor.getInt(24));

            } while(cursor.moveToNext());
        }

        if (bean != null) // Si la cita no es nula se carga los recordatorios de llamadas
        {
            bean.setRecordatorioLlamadaBean(CitaReunionController.obtenerRecordatorioLLamadaporCitaDispositivo(bean.getIdCitaDispositivo()));
        }

        return bean;
    }
}
