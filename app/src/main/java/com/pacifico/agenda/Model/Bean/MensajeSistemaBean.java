package com.pacifico.agenda.Model.Bean;


import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class MensajeSistemaBean extends Entity {

    private int IdMensajeSistema;
    private String DescripcionMensaje;
    private String FechaInicioVigencia;
    private String FechaFinVigencia;

    public int getIdMensajeSistema() {
        return IdMensajeSistema;
    }

    public void setIdMensajeSistema(int idMensajeSistema) {
        IdMensajeSistema = idMensajeSistema;
    }

    public String getDescripcionMensaje() {
        return DescripcionMensaje;
    }

    public void setDescripcionMensaje(String descripcion) {
        DescripcionMensaje = descripcion;
    }

    public String getFechaInicioVigencia() {
        return FechaInicioVigencia;
    }

    public void setFechaInicioVigencia(String fechaInicioVigencia) {
        FechaInicioVigencia = fechaInicioVigencia;
    }

    public String getFechaFinVigencia() {
        return FechaFinVigencia;
    }

    public void setFechaFinVigencia(String fechaFinVigencia) {
        FechaFinVigencia = fechaFinVigencia;
    }

    // Persistir
    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDMENSAJESISTEMA:
                return IdMensajeSistema;

            case CO_DESCRIPCION:
                return DescripcionMensaje;

            case CO_FECHAINICIOVIGENCIA:
                return FechaInicioVigencia;

            case CO_FECHAFINVIGENCIA:
                return FechaFinVigencia;

        }
        return null;
    }



    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDMENSAJESISTEMA:
                IdMensajeSistema = (int) object;
                break;
            case CO_DESCRIPCION:
                DescripcionMensaje = (String) object;
                break;
            case CO_FECHAINICIOVIGENCIA:
                FechaInicioVigencia = (String) object;
                break;
            case CO_FECHAFINVIGENCIA:
                FechaFinVigencia = (String) object;
                break;
        }
    }

    public static final String CN_IDMENSAJESISTEMA = "IDMENSAJESISTEMA";
    public static final String CT_IDMENSAJESISTEMA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDMENSAJESISTEMA = 1;

    public static final String CN_DESCRIPCION = "DESCRIPCION";
    public static final String CT_DESCRIPCION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_DESCRIPCION = 2;

    public static final String CN_FECHAINICIOVIGENCIA = "FECHAINICIOVIGENCIA";
    public static final String CT_FECHAINICIOVIGENCIA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAINICIOVIGENCIA = 3;

    public static final String CN_FECHAFINVIGENCIA = "FECHAFINVIGENCIA";
    public static final String CT_FECHAFINVIGENCIA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAFINVIGENCIA = 4;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_MENSAJE_SISTEMA, new OrganizateEntityFactory())
            .addColumn(CN_IDMENSAJESISTEMA, CT_IDMENSAJESISTEMA, CO_IDMENSAJESISTEMA)
            .addColumn(CN_DESCRIPCION, CT_DESCRIPCION, CO_DESCRIPCION)
            .addColumn(CN_FECHAINICIOVIGENCIA, CT_FECHAINICIOVIGENCIA, CO_FECHAINICIOVIGENCIA)
            .addColumn(CN_FECHAFINVIGENCIA, CT_FECHAFINVIGENCIA, CO_FECHAFINVIGENCIA);

}
