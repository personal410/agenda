package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Model.Controller.ParametroController;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;

/**
 * Created by Joel on 31/05/2016.
 */
public class AdnBean extends Entity {
    private int IdProspecto;
    private int IdProspectoDispositivo;
    private double TipoCambio;
    private int MonedaEfectivoAhorros;
    private int EfectivoAhorros;
    private int MonedaPropiedades;
    private int Propiedades;
    private int MonedaVehiculos;
    private int Vehiculos;
    private int MonedaTotalActivoRealizable;
    private double TotalActivoRealizable;
    private int MonedaSeguroIndividual;
    private int SeguroIndividual;
    private int MonedaIngresoBrutoMensualVidaLey;
    private int IngresoBrutoMensualVidaLey;
    private int FactorVidaLey;
    private int MonedaTopeVidaLey;
    private double TopeVidaLey;
    private int MonedaSeguroVidaLey;
    private double SeguroVidaLey;
    private int FlagCuentaConVidaLey;
    private int MonedaTotalSegurosVida;
    private double TotalSegurosVida;
    private double PorcentajeAFPConyuge;
    private double PorcentajeAFPHijos;
    private int NumeroHijos;
    private int MonedaPensionConyuge;
    private double PensionConyuge;
    private int MonedaPensionHijos;
    private double PensionHijos;
    private int FlagPensionConyuge;
    private int FlagPensionHijos;
    private int FlagPensionNoAFP;
    private int MonedaTotalPensionMensualAFP;
    private double TotalPensionMensualAFP;
    private int MonedaIngresoMensualTitular;
    private int IngresoMensualTitular;
    private int MonedaIngresoMensualConyuge;
    private int IngresoMensualConyuge;
    private int MonedaIngresoFamiliarOtros;
    private int IngresoFamiliarOtros;
    private int MonedaTotalIngresoFamiliarMensual;
    private double TotalIngresoFamiliarMensual;
    private int MonedaGastoVivienda;
    private int GastoVivienda;
    private int MonedaGastoServicios;
    private int GastoServicios;
    private int MonedaGastoHogarAlimentacion;
    private int GastoHogarAlimentacion;
    private int MonedaGastoSalud;
    private int GastoSalud;
    private int MonedaGastoEducacion;
    private int GastoEducacion;
    private int MonedaGastoVehiculoTransporte;
    private int GastoVehiculoTransporte;
    private int MonedaGastoEsparcimiento;
    private int GastoEsparcimiento;
    private int MonedaGastoOtros;
    private int GastoOtros;
    private int MonedaTotalGastoFamiliarMensual;
    private double TotalGastoFamiliarMensual;
    private int MonedaDeficitMensual;
    private double DeficitMensual;
    private int AniosProteger;
    private int MonedaCapitalNecesarioFallecimiento;
    private double CapitalNecesarioFallecimiento;
    private double PorcentajeInversion;
    private int MonedaMontoMensualInvertir;
    private double MontoMensualInvertir;
    private int IdEntidad;
    private int CodigoFrecuenciaPago;
    private int IndicadorVentana;
    private int FlagTerminado;
    private int FlagADNDigital;
    private String FechaCreacionDispositivo;
    private String FechaModificacionDispositivo;
    private String AdicionalTexto1;
    private double AdicionalNumerico1;
    private String AdicionalTexto2;
    private double AdicionalNumerico2;
    private int FlagEnviado;
    private double TotalGastoVivienda;
    private double TotalGastoSaludEducacion;
    private double TotalGastoTransporte;
    private double TotalGastoOtros;
    private int FlagModificado;
    public void inicializarValores(){
        IdProspecto = -1;
        IdProspectoDispositivo = -1;
        ParametroBean tipoCambioParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(2);
        ParametroBean topeVidaLeyParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(3);
        ParametroBean factorVidaLeyParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(4);
        ParametroBean porcentajeConyugeParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(5);
        ParametroBean porcentajeHijosParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(6);
        TipoCambio = tipoCambioParametroBean.getValorNumerico();
        MonedaEfectivoAhorros = Constantes.MonedaSoles;
        EfectivoAhorros = 0;
        MonedaPropiedades = Constantes.MonedaSoles;
        Propiedades = 0;
        MonedaVehiculos = Constantes.MonedaSoles;
        Vehiculos = 0;
        MonedaTotalActivoRealizable = Constantes.MonedaDolares;
        TotalActivoRealizable = 0;
        MonedaSeguroIndividual = Constantes.MonedaSoles;
        SeguroIndividual = 0;
        MonedaIngresoBrutoMensualVidaLey = Constantes.MonedaSoles;
        IngresoBrutoMensualVidaLey = 0;
        FactorVidaLey = (int)factorVidaLeyParametroBean.getValorNumerico();
        MonedaTopeVidaLey = Constantes.MonedaSoles;
        TopeVidaLey = Util.redondearNumero(topeVidaLeyParametroBean.getValorNumerico(), 0);
        MonedaSeguroVidaLey = Constantes.MonedaSoles;
        SeguroVidaLey = 0;
        FlagCuentaConVidaLey = 1;
        MonedaTotalSegurosVida = Constantes.MonedaDolares;
        TotalSegurosVida = 0;
        PorcentajeAFPConyuge = porcentajeConyugeParametroBean.getValorNumerico();
        PorcentajeAFPHijos = porcentajeHijosParametroBean.getValorNumerico();
        MonedaPensionConyuge = Constantes.MonedaDolares;
        PensionConyuge = 0;
        MonedaPensionHijos = Constantes.MonedaDolares;
        PensionHijos = 0;
        FlagPensionConyuge = 0;
        FlagPensionHijos = 0;
        FlagPensionNoAFP = 0;
        MonedaTotalPensionMensualAFP = Constantes.MonedaDolares;
        TotalPensionMensualAFP = 0;
        MonedaIngresoMensualTitular = Constantes.MonedaSoles;
        IngresoMensualTitular = 0;
        MonedaIngresoMensualConyuge = Constantes.MonedaSoles;
        IngresoMensualConyuge = 0;
        MonedaIngresoFamiliarOtros = Constantes.MonedaSoles;
        IngresoFamiliarOtros = 0;
        MonedaTotalIngresoFamiliarMensual = Constantes.MonedaDolares;
        TotalIngresoFamiliarMensual = 0;
        MonedaGastoVivienda = Constantes.MonedaSoles;
        GastoVivienda = 0;
        MonedaGastoServicios = Constantes.MonedaSoles;
        GastoServicios = 0;
        MonedaGastoHogarAlimentacion = Constantes.MonedaSoles;
        GastoHogarAlimentacion = 0;
        MonedaGastoSalud = Constantes.MonedaSoles;
        GastoSalud = 0;
        MonedaGastoEducacion = Constantes.MonedaSoles;
        GastoEducacion = 0;
        MonedaGastoVehiculoTransporte = Constantes.MonedaSoles;
        GastoVehiculoTransporte = 0;
        MonedaGastoEsparcimiento = Constantes.MonedaSoles;
        GastoEsparcimiento = 0;
        MonedaGastoOtros = 0;
        GastoOtros = 0;
        MonedaTotalGastoFamiliarMensual = Constantes.MonedaDolares;
        TotalGastoFamiliarMensual = 0;
        MonedaDeficitMensual = Constantes.MonedaDolares;
        DeficitMensual = 0;
        AniosProteger = 20;
        MonedaCapitalNecesarioFallecimiento = Constantes.MonedaDolares;
        CapitalNecesarioFallecimiento = 0;
        PorcentajeInversion = -1;
        MonedaMontoMensualInvertir = Constantes.MonedaDolares;
        MontoMensualInvertir = 0;
        IdEntidad = -1;
        CodigoFrecuenciaPago = -1;
        IndicadorVentana = 1;
        FlagTerminado = 0;
        FlagADNDigital = 0;
        AdicionalNumerico1 = -1;
        AdicionalNumerico2 = -1;
        FlagEnviado = 0;
        FlagModificado = 0;
    }
    public void calcularTotalGastoVivienda(){
        double factorGastoVivienda = MonedaGastoVivienda == 0 ? 1 : TipoCambio;
        double factorGastoServicios = MonedaGastoServicios == 0 ? 1 : TipoCambio;
        double factorGastoHogarAlimentacion = MonedaGastoHogarAlimentacion == 0 ? 1 : TipoCambio;
        TotalGastoVivienda = GastoVivienda / factorGastoVivienda + GastoServicios / factorGastoServicios + GastoHogarAlimentacion / factorGastoHogarAlimentacion;
    }
    public void calcularTotalGastoSaludEducacion(){
        double factorGastoSalud = MonedaGastoSalud == 0 ? 1 : TipoCambio;
        double factorGastoEducacion = MonedaGastoEducacion == 0 ? 1 : TipoCambio;
        TotalGastoSaludEducacion = GastoSalud / factorGastoSalud + GastoEducacion / factorGastoEducacion;
    }
    public void calcularTotalGastoTransporte(){
        double factorGastoVehiculoTransporte = MonedaGastoVehiculoTransporte == 0 ? 1 : TipoCambio;
        TotalGastoTransporte = GastoVehiculoTransporte / factorGastoVehiculoTransporte;
    }
    public void calcularTotalGastoOtros(){
        double factorGastoEsparcimiento = MonedaGastoEsparcimiento == 0 ? 1 : TipoCambio;
        double factorGastoOtros = MonedaGastoOtros == 0 ? 1 : TipoCambio;
        TotalGastoOtros = GastoEsparcimiento / factorGastoEsparcimiento + GastoOtros / factorGastoOtros;
    }

    public int getIdProspecto() {
        return IdProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        IdProspecto = idProspecto;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public int getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public double getTipoCambio() {
        return TipoCambio;
    }

    public void setTipoCambio(double tipoCambio) {
        TipoCambio = tipoCambio;
    }

    public int getMonedaEfectivoAhorros() {
        return MonedaEfectivoAhorros;
    }

    public void setMonedaEfectivoAhorros(int monedaEfectivoAhorros) {
        MonedaEfectivoAhorros = monedaEfectivoAhorros;
    }

    public int getEfectivoAhorros() {
        return EfectivoAhorros;
    }

    public void setEfectivoAhorros(int efectivoAhorros) {
        EfectivoAhorros = efectivoAhorros;
    }

    public int getMonedaPropiedades() {
        return MonedaPropiedades;
    }

    public void setMonedaPropiedades(int monedaPropiedades) {
        MonedaPropiedades = monedaPropiedades;
    }

    public int getPropiedades() {
        return Propiedades;
    }

    public void setPropiedades(int propiedades) {
        Propiedades = propiedades;
    }

    public int getMonedaVehiculos() {
        return MonedaVehiculos;
    }

    public void setMonedaVehiculos(int monedaVehiculos) {
        MonedaVehiculos = monedaVehiculos;
    }

    public int getVehiculos() {
        return Vehiculos;
    }

    public void setVehiculos(int vehiculos) {
        Vehiculos = vehiculos;
    }

    public int getMonedaTotalActivoRealizable() {
        return MonedaTotalActivoRealizable;
    }

    public void setMonedaTotalActivoRealizable(int monedaTotalActivoRealizable) {
        MonedaTotalActivoRealizable = monedaTotalActivoRealizable;
    }

    public double getTotalActivoRealizable() {
        return TotalActivoRealizable;
    }

    public void setTotalActivoRealizable(double totalActivoRealizable) {
        TotalActivoRealizable = totalActivoRealizable;
    }

    public int getMonedaSeguroIndividual() {
        return MonedaSeguroIndividual;
    }

    public void setMonedaSeguroIndividual(int monedaSeguroIndividual) {
        MonedaSeguroIndividual = monedaSeguroIndividual;
    }

    public int getSeguroIndividual() {
        return SeguroIndividual;
    }

    public void setSeguroIndividual(int seguroIndividual) {
        SeguroIndividual = seguroIndividual;
    }

    public int getMonedaIngresoBrutoMensualVidaLey() {
        return MonedaIngresoBrutoMensualVidaLey;
    }

    public void setMonedaIngresoBrutoMensualVidaLey(int monedaIngresoBrutoMensualVidaLey) {
        MonedaIngresoBrutoMensualVidaLey = monedaIngresoBrutoMensualVidaLey;
    }

    public int getIngresoBrutoMensualVidaLey() {
        return IngresoBrutoMensualVidaLey;
    }

    public void setIngresoBrutoMensualVidaLey(int ingresoBrutoMensualVidaLey) {
        IngresoBrutoMensualVidaLey = ingresoBrutoMensualVidaLey;
    }

    public int getFactorVidaLey() {
        return FactorVidaLey;
    }

    public void setFactorVidaLey(int factorVidaLey) {
        FactorVidaLey = factorVidaLey;
    }

    public int getMonedaTopeVidaLey() {
        return MonedaTopeVidaLey;
    }

    public void setMonedaTopeVidaLey(int monedaTopeVidaLey) {
        MonedaTopeVidaLey = monedaTopeVidaLey;
    }

    public double getTopeVidaLey() {
        return TopeVidaLey;
    }

    public void setTopeVidaLey(double topeVidaLey) {
        TopeVidaLey = topeVidaLey;
    }

    public int getMonedaSeguroVidaLey() {
        return MonedaSeguroVidaLey;
    }

    public void setMonedaSeguroVidaLey(int monedaSeguroVidaLey) {
        MonedaSeguroVidaLey = monedaSeguroVidaLey;
    }

    public double getSeguroVidaLey() {
        return SeguroVidaLey;
    }

    public void setSeguroVidaLey(double seguroVidaLey) {
        SeguroVidaLey = seguroVidaLey;
    }

    public int getFlagCuentaConVidaLey() {
        return FlagCuentaConVidaLey;
    }

    public void setFlagCuentaConVidaLey(int flagCuentaConVidaLey) {
        FlagCuentaConVidaLey = flagCuentaConVidaLey;
    }

    public int getMonedaTotalSegurosVida() {
        return MonedaTotalSegurosVida;
    }

    public void setMonedaTotalSegurosVida(int monedaTotalSegurosVida) {
        MonedaTotalSegurosVida = monedaTotalSegurosVida;
    }

    public double getTotalSegurosVida() {
        return TotalSegurosVida;
    }

    public void setTotalSegurosVida(double totalSegurosVida) {
        TotalSegurosVida = totalSegurosVida;
    }

    public double getPorcentajeAFPConyuge() {
        return PorcentajeAFPConyuge;
    }

    public void setPorcentajeAFPConyuge(double porcentajeAFPConyuge) {
        PorcentajeAFPConyuge = porcentajeAFPConyuge;
    }

    public double getPorcentajeAFPHijos() {
        return PorcentajeAFPHijos;
    }

    public void setPorcentajeAFPHijos(double porcentajeAFPHijos) {
        PorcentajeAFPHijos = porcentajeAFPHijos;
    }

    public int getNumeroHijos() {
        return NumeroHijos;
    }

    public void setNumeroHijos(int numeroHijos) {
        NumeroHijos = numeroHijos;
    }

    public int getMonedaPensionConyuge() {
        return MonedaPensionConyuge;
    }

    public void setMonedaPensionConyuge(int monedaPensionConyuge) {
        MonedaPensionConyuge = monedaPensionConyuge;
    }

    public double getPensionConyuge() {
        return PensionConyuge;
    }

    public void setPensionConyuge(double pensionConyuge) {
        PensionConyuge = pensionConyuge;
    }

    public int getMonedaPensionHijos() {
        return MonedaPensionHijos;
    }

    public void setMonedaPensionHijos(int monedaPensionHijos) {
        MonedaPensionHijos = monedaPensionHijos;
    }

    public double getPensionHijos() {
        return PensionHijos;
    }

    public void setPensionHijos(double pensionHijos) {
        PensionHijos = pensionHijos;
    }

    public int getFlagPensionConyuge() {
        return FlagPensionConyuge;
    }

    public void setFlagPensionConyuge(int flagPensionConyuge) {
        FlagPensionConyuge = flagPensionConyuge;
    }

    public int getFlagPensionHijos() {
        return FlagPensionHijos;
    }

    public void setFlagPensionHijos(int flagPensionHijos) {
        FlagPensionHijos = flagPensionHijos;
    }

    public int getFlagPensionNoAFP() {
        return FlagPensionNoAFP;
    }

    public void setFlagPensionNoAFP(int flagPensionNoAFP) {
        FlagPensionNoAFP = flagPensionNoAFP;
    }

    public int getMonedaTotalPensionMensualAFP() {
        return MonedaTotalPensionMensualAFP;
    }

    public void setMonedaTotalPensionMensualAFP(int monedaTotalPensionMensualAFP) {
        MonedaTotalPensionMensualAFP = monedaTotalPensionMensualAFP;
    }

    public double getTotalPensionMensualAFP() {
        return TotalPensionMensualAFP;
    }

    public void setTotalPensionMensualAFP(double totalPensionMensualAFP) {
        TotalPensionMensualAFP = totalPensionMensualAFP;
    }

    public int getMonedaIngresoMensualTitular() {
        return MonedaIngresoMensualTitular;
    }

    public void setMonedaIngresoMensualTitular(int monedaIngresoMensualTitular) {
        MonedaIngresoMensualTitular = monedaIngresoMensualTitular;
    }

    public int getIngresoMensualTitular() {
        return IngresoMensualTitular;
    }

    public void setIngresoMensualTitular(int ingresoMensualTitular) {
        IngresoMensualTitular = ingresoMensualTitular;
    }

    public int getMonedaIngresoMensualConyuge() {
        return MonedaIngresoMensualConyuge;
    }

    public void setMonedaIngresoMensualConyuge(int monedaIngresoMensualConyuge) {
        MonedaIngresoMensualConyuge = monedaIngresoMensualConyuge;
    }

    public int getIngresoMensualConyuge() {
        return IngresoMensualConyuge;
    }

    public void setIngresoMensualConyuge(int ingresoMensualConyuge) {
        IngresoMensualConyuge = ingresoMensualConyuge;
    }

    public int getMonedaIngresoFamiliarOtros() {
        return MonedaIngresoFamiliarOtros;
    }

    public void setMonedaIngresoFamiliarOtros(int monedaIngresoFamiliarOtros) {
        MonedaIngresoFamiliarOtros = monedaIngresoFamiliarOtros;
    }

    public int getIngresoFamiliarOtros() {
        return IngresoFamiliarOtros;
    }

    public void setIngresoFamiliarOtros(int ingresoFamiliarOtros) {
        IngresoFamiliarOtros = ingresoFamiliarOtros;
    }

    public int getMonedaTotalIngresoFamiliarMensual() {
        return MonedaTotalIngresoFamiliarMensual;
    }

    public void setMonedaTotalIngresoFamiliarMensual(int monedaTotalIngresoFamiliarMensual) {
        MonedaTotalIngresoFamiliarMensual = monedaTotalIngresoFamiliarMensual;
    }

    public double getTotalIngresoFamiliarMensual() {
        return TotalIngresoFamiliarMensual;
    }

    public void setTotalIngresoFamiliarMensual(double totalIngresoFamiliarMensual) {
        TotalIngresoFamiliarMensual = totalIngresoFamiliarMensual;
    }

    public int getMonedaGastoVivienda() {
        return MonedaGastoVivienda;
    }

    public void setMonedaGastoVivienda(int monedaGastoVivienda) {
        MonedaGastoVivienda = monedaGastoVivienda;
    }

    public int getGastoVivienda() {
        return GastoVivienda;
    }

    public void setGastoVivienda(int gastoVivienda) {
        GastoVivienda = gastoVivienda;
    }

    public int getMonedaGastoServicios() {
        return MonedaGastoServicios;
    }

    public void setMonedaGastoServicios(int monedaGastoServicios) {
        MonedaGastoServicios = monedaGastoServicios;
    }

    public int getGastoServicios() {
        return GastoServicios;
    }

    public void setGastoServicios(int gastoServicios) {
        GastoServicios = gastoServicios;
    }

    public int getMonedaGastoHogarAlimentacion() {
        return MonedaGastoHogarAlimentacion;
    }

    public void setMonedaGastoHogarAlimentacion(int monedaGastoHogarAlimentacion) {
        MonedaGastoHogarAlimentacion = monedaGastoHogarAlimentacion;
    }

    public int getGastoHogarAlimentacion() {
        return GastoHogarAlimentacion;
    }

    public void setGastoHogarAlimentacion(int gastoHogarAlimentacion) {
        GastoHogarAlimentacion = gastoHogarAlimentacion;
    }

    public int getMonedaGastoSalud() {
        return MonedaGastoSalud;
    }

    public void setMonedaGastoSalud(int monedaGastoSalud) {
        MonedaGastoSalud = monedaGastoSalud;
    }

    public int getGastoSalud() {
        return GastoSalud;
    }

    public void setGastoSalud(int gastoSalud) {
        GastoSalud = gastoSalud;
    }

    public int getMonedaGastoEducacion() {
        return MonedaGastoEducacion;
    }

    public void setMonedaGastoEducacion(int monedaGastoEducacion) {
        MonedaGastoEducacion = monedaGastoEducacion;
    }

    public int getGastoEducacion() {
        return GastoEducacion;
    }

    public void setGastoEducacion(int gastoEducacion) {
        GastoEducacion = gastoEducacion;
    }

    public int getMonedaGastoVehiculoTransporte() {
        return MonedaGastoVehiculoTransporte;
    }

    public void setMonedaGastoVehiculoTransporte(int monedaGastoVehiculoTransporte) {
        MonedaGastoVehiculoTransporte = monedaGastoVehiculoTransporte;
    }

    public int getGastoVehiculoTransporte() {
        return GastoVehiculoTransporte;
    }

    public void setGastoVehiculoTransporte(int gastoVehiculoTransporte) {
        GastoVehiculoTransporte = gastoVehiculoTransporte;
    }

    public int getMonedaGastoEsparcimiento() {
        return MonedaGastoEsparcimiento;
    }

    public void setMonedaGastoEsparcimiento(int monedaGastoEsparcimiento) {
        MonedaGastoEsparcimiento = monedaGastoEsparcimiento;
    }

    public int getGastoEsparcimiento() {
        return GastoEsparcimiento;
    }

    public void setGastoEsparcimiento(int gastoEsparcimiento) {
        GastoEsparcimiento = gastoEsparcimiento;
    }

    public int getMonedaGastoOtros() {
        return MonedaGastoOtros;
    }

    public void setMonedaGastoOtros(int monedaGastoOtros) {
        MonedaGastoOtros = monedaGastoOtros;
    }

    public int getGastoOtros() {
        return GastoOtros;
    }

    public void setGastoOtros(int gastoOtros) {
        GastoOtros = gastoOtros;
    }

    public int getMonedaTotalGastoFamiliarMensual() {
        return MonedaTotalGastoFamiliarMensual;
    }

    public void setMonedaTotalGastoFamiliarMensual(int monedaTotalGastoFamiliarMensual) {
        MonedaTotalGastoFamiliarMensual = monedaTotalGastoFamiliarMensual;
    }

    public double getTotalGastoFamiliarMensual() {
        return TotalGastoFamiliarMensual;
    }

    public void setTotalGastoFamiliarMensual(double totalGastoFamiliarMensual) {
        TotalGastoFamiliarMensual = totalGastoFamiliarMensual;
    }

    public int getMonedaDeficitMensual() {
        return MonedaDeficitMensual;
    }

    public void setMonedaDeficitMensual(int monedaDeficitMensual) {
        MonedaDeficitMensual = monedaDeficitMensual;
    }

    public double getDeficitMensual() {
        return DeficitMensual;
    }

    public void setDeficitMensual(double deficitMensual) {
        DeficitMensual = deficitMensual;
    }

    public int getAniosProteger() {
        return AniosProteger;
    }

    public void setAniosProteger(int aniosProteger) {
        AniosProteger = aniosProteger;
    }

    public int getMonedaCapitalNecesarioFallecimiento() {
        return MonedaCapitalNecesarioFallecimiento;
    }

    public void setMonedaCapitalNecesarioFallecimiento(int monedaCapitalNecesarioFallecimiento) {
        MonedaCapitalNecesarioFallecimiento = monedaCapitalNecesarioFallecimiento;
    }

    public double getCapitalNecesarioFallecimiento() {
        return CapitalNecesarioFallecimiento;
    }

    public void setCapitalNecesarioFallecimiento(double capitalNecesarioFallecimiento) {
        CapitalNecesarioFallecimiento = capitalNecesarioFallecimiento;
    }

    public double getPorcentajeInversion() {
        return PorcentajeInversion;
    }

    public void setPorcentajeInversion(double porcentajeInversion) {
        PorcentajeInversion = porcentajeInversion;
    }

    public int getMonedaMontoMensualInvertir() {
        return MonedaMontoMensualInvertir;
    }

    public void setMonedaMontoMensualInvertir(int monedaMontoMensualInvertir) {
        MonedaMontoMensualInvertir = monedaMontoMensualInvertir;
    }

    public double getMontoMensualInvertir() {
        return MontoMensualInvertir;
    }

    public void setMontoMensualInvertir(double montoMensualInvertir) {
        MontoMensualInvertir = montoMensualInvertir;
    }

    public int getIdEntidad() {
        return IdEntidad;
    }

    public void setIdEntidad(int idEntidad) {
        IdEntidad = idEntidad;
    }

    public int getCodigoFrecuenciaPago() {
        return CodigoFrecuenciaPago;
    }

    public void setCodigoFrecuenciaPago(int codigoFrecuenciaPago) {
        CodigoFrecuenciaPago = codigoFrecuenciaPago;
    }

    public int getIndicadorVentana() {
        return IndicadorVentana;
    }

    public void setIndicadorVentana(int indicadorVentana) {
        IndicadorVentana = indicadorVentana;
    }

    public int getFlagTerminado() {
        return FlagTerminado;
    }

    public void setFlagTerminado(int flagTerminado) {
        FlagTerminado = flagTerminado;
    }

    public int getFlagADNDigital() {
        return FlagADNDigital;
    }

    public void setFlagADNDigital(int flagADNDigital) {
        FlagADNDigital = flagADNDigital;
    }

    public String getFechaCreacionDispositivo() {
        return FechaCreacionDispositivo;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return FechaModificacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public String getAdicionalTexto1() {
        return AdicionalTexto1;
    }

    public void setAdicionalTexto1(String adicionalTexto1) {
        AdicionalTexto1 = adicionalTexto1;
    }

    public double getAdicionalNumerico1() {
        return AdicionalNumerico1;
    }

    public void setAdicionalNumerico1(double adicionalNumerico1) {
        AdicionalNumerico1 = adicionalNumerico1;
    }

    public String getAdicionalTexto2() {
        return AdicionalTexto2;
    }

    public void setAdicionalTexto2(String adicionalTexto2) {
        AdicionalTexto2 = adicionalTexto2;
    }

    public double getAdicionalNumerico2() {
        return AdicionalNumerico2;
    }

    public void setAdicionalNumerico2(double adicionalNumerico2) {
        AdicionalNumerico2 = adicionalNumerico2;
    }

    public int getFlagEnviado() {
        return FlagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        FlagEnviado = flagEnviado;
    }

    public double getTotalGastoVivienda() {
        return TotalGastoVivienda;
    }

    public void setTotalGastoVivienda(double totalGastoVivienda) {
        TotalGastoVivienda = totalGastoVivienda;
    }

    public double getTotalGastoSaludEducacion() {
        return TotalGastoSaludEducacion;
    }

    public void setTotalGastoSaludEducacion(double totalGastoSaludEducacion) {
        TotalGastoSaludEducacion = totalGastoSaludEducacion;
    }

    public double getTotalGastoTransporte() {
        return TotalGastoTransporte;
    }

    public void setTotalGastoTransporte(double totalGastoTransporte) {
        TotalGastoTransporte = totalGastoTransporte;
    }

    public double getTotalGastoOtros() {
        return TotalGastoOtros;
    }

    public void setTotalGastoOtros(double totalGastoOtros) {
        TotalGastoOtros = totalGastoOtros;
    }

    public int getFlagModificado() {
        return FlagModificado;
    }

    public void setFlagModificado(int flagModificado) {
        FlagModificado = flagModificado;
    }

    @Override
    public Object getColumnValue(int column) {
        switch(column){
            case CO_IDPROSPECTO:
                return IdProspecto;
            case CO_IDPROSPECTODISPOSITIVO:
                return IdProspectoDispositivo;
            case CO_TIPOCAMBIO:
                return TipoCambio;
            case CO_MONEDAEFECTIVOAHORROS:
                return MonedaEfectivoAhorros;
            case CO_EFECTIVOAHORROS:
                return EfectivoAhorros;
            case CO_MONEDAPROPIEDADES:
                return MonedaPropiedades;
            case CO_PROPIEDADES:
                return Propiedades;
            case CO_MONEDAVEHICULOS:
                return MonedaVehiculos;
            case CO_VEHICULOS:
                return Vehiculos;
            case CO_MONEDATOTALACTIVOREALIZABLE:
                return MonedaTotalActivoRealizable;
            case CO_TOTALACTIVOREALIZABLE:
                return TotalActivoRealizable;
            case CO_MONEDASEGUROINDIVIDUAL:
                return MonedaSeguroIndividual;
            case CO_SEGUROINDIVIDUAL:
                return SeguroIndividual;
            case CO_MONEDAINGRESOBRUTOMENSUALVIDALEY:
                return MonedaIngresoBrutoMensualVidaLey;
            case CO_INGRESOBRUTOMENSUALVIDALEY:
                return IngresoBrutoMensualVidaLey;
            case CO_FACTORVIDALEY:
                return FactorVidaLey;
            case CO_MONEDATOPEVIDALEY:
                return MonedaTopeVidaLey;
            case CO_TOPEVIDALEY:
                return TopeVidaLey;
            case CO_MONEDASEGUROVIDALEY:
                return MonedaSeguroVidaLey;
            case CO_SEGUROVIDALEY:
                return SeguroVidaLey;
            case CO_FLAGCUENTACONVIDALEY:
                return FlagCuentaConVidaLey;
            case CO_MONEDATOTALSEGUROSVIDA:
                return MonedaTotalSegurosVida;
            case CO_TOTALSEGUROSVIDA:
                return TotalSegurosVida;
            case CO_PORCENTAJEAFPCONYUGE:
                return PorcentajeAFPConyuge;
            case CO_PORCENTAJEAFPHIJOS:
                return PorcentajeAFPHijos;
            case CO_NUMEROHIJOS:
                return NumeroHijos;
            case CO_MONEDAPENSIONCONYUGE:
                return MonedaPensionConyuge;
            case CO_PENSIONCONYUGE:
                return PensionConyuge;
            case CO_MONEDAPENSIONHIJOS:
                return MonedaPensionHijos;
            case CO_PENSIONHIJOS:
                return PensionHijos;
            case CO_FLAGPENSIONCONYUGE:
                return FlagPensionConyuge;
            case CO_FLAGPENSIONHIJOS:
                return FlagPensionHijos;
            case CO_FLAGPENSIONNOAFP:
                return FlagPensionNoAFP;
            case CO_MONEDATOTALPENSIONMENSUALAFP:
                return MonedaTotalPensionMensualAFP;
            case CO_TOTALPENSIONMENSUALAFP:
                return TotalPensionMensualAFP;
            case CO_MONEDAINGRESOMENSUALTITULAR:
                return MonedaIngresoMensualTitular;
            case CO_INGRESOMENSUALTITULAR:
                return IngresoMensualTitular;
            case CO_MONEDAINGRESOMENSUALCONYUGE:
                return MonedaIngresoMensualConyuge;
            case CO_INGRESOMENSUALCONYUGE:
                return IngresoMensualConyuge;
            case CO_MONEDAINGRESOFAMILIAROTROS:
                return MonedaIngresoFamiliarOtros;
            case CO_INGRESOFAMILIAROTROS:
                return IngresoFamiliarOtros;
            case CO_MONEDATOTALINGRESOFAMILIARMENSUAL:
                return MonedaTotalIngresoFamiliarMensual;
            case CO_TOTALINGRESOFAMILIARMENSUAL:
                return TotalIngresoFamiliarMensual;
            case CO_MONEDAGASTOVIVIENDA:
                return MonedaGastoVivienda;
            case CO_GASTOVIVIENDA:
                return GastoVivienda;
            case CO_MONEDAGASTOSERVICIOS:
                return MonedaGastoServicios;
            case CO_GASTOSERVICIOS:
                return GastoServicios;
            case CO_MONEDAGASTOHOGARALIMENTACION:
                return MonedaGastoHogarAlimentacion;
            case CO_GASTOHOGARALIMENTACION:
                return GastoHogarAlimentacion;
            case CO_MONEDAGASTOSALUD:
                return MonedaGastoSalud;
            case CO_GASTOSALUD:
                return GastoSalud;
            case CO_MONEDAGASTOEDUCACION:
                return MonedaGastoEducacion;
            case CO_GASTOEDUCACION:
                return GastoEducacion;
            case CO_MONEDAGASTOVEHICULOTRANSPORTE:
                return MonedaGastoVehiculoTransporte;
            case CO_GASTOVEHICULOTRANSPORTE:
                return GastoVehiculoTransporte;
            case CO_MONEDAGASTOESPARCIMIENTO:
                return MonedaGastoEsparcimiento;
            case CO_GASTOESPARCIMIENTO:
                return GastoEsparcimiento;
            case CO_MONEDAGASTOOTROS:
                return MonedaGastoOtros;
            case CO_GASTOOTROS:
                return GastoOtros;
            case CO_MONEDATOTALGASTOFAMILIARMENSUAL:
                return MonedaTotalGastoFamiliarMensual;
            case CO_TOTALGASTOFAMILIARMENSUAL:
                return TotalGastoFamiliarMensual;
            case CO_MONEDADEFICITMENSUAL:
                return MonedaDeficitMensual;
            case CO_DEFICITMENSUAL:
                return DeficitMensual;
            case CO_ANIOSPROTEGER:
                return AniosProteger;
            case CO_MONEDACAPITALNECESARIOFALLECIMIENTO:
                return MonedaCapitalNecesarioFallecimiento;
            case CO_CAPITALNECESARIOFALLECIMIENTO:
                return CapitalNecesarioFallecimiento;
            case CO_PORCENTAJEINVERSION:
                return PorcentajeInversion;
            case CO_MONEDAMONTOMENSUALINVERTIR:
                return MonedaMontoMensualInvertir;
            case CO_MONTOMENSUALINVERTIR:
                return MontoMensualInvertir;
            case CO_IDENTIDAD:
                return IdEntidad;
            case CO_CODIGOFRECUENCIAPAGO:
                return CodigoFrecuenciaPago;
            case CO_INDICADORVENTANA:
                return IndicadorVentana;
            case CO_FLAGTERMINADO:
                return FlagTerminado;
            case CO_FLAGADNDIGITAL:
                return FlagADNDigital;
            case CO_FECHACREACIONDISPOSITIVO:
                return FechaCreacionDispositivo;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                return FechaModificacionDispositivo;
            case CO_ADICIONALTEXTO1:
                return AdicionalTexto1;
            case CO_ADICIONALNUMERICO1:
                return AdicionalNumerico1;
            case CO_ADICIONALTEXTO2:
                return AdicionalTexto2;
            case CO_ADICIONALNUMERICO2:
                return AdicionalNumerico2;
            case CO_FLAGENVIADO:
                return FlagEnviado;
        }
        return null;
    }
    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDPROSPECTO:
                IdProspecto = (int) object;
                break;
            case CO_IDPROSPECTODISPOSITIVO:
                IdProspectoDispositivo = (int) object;
                break;
            case CO_TIPOCAMBIO:
                TipoCambio = (double) object;
                break;
            case CO_MONEDAEFECTIVOAHORROS:
                MonedaEfectivoAhorros = (int) object;
                break;
            case CO_EFECTIVOAHORROS:
                EfectivoAhorros = (int) object;
                break;
            case CO_MONEDAPROPIEDADES:
                MonedaPropiedades = (int) object;
                break;
            case CO_PROPIEDADES:
                Propiedades = (int) object;
                break;
            case CO_MONEDAVEHICULOS:
                MonedaVehiculos = (int) object;
                break;
            case CO_VEHICULOS:
                Vehiculos = (int) object;
                break;
            case CO_MONEDATOTALACTIVOREALIZABLE:
                MonedaTotalActivoRealizable = (int) object;
                break;
            case CO_TOTALACTIVOREALIZABLE:
                TotalActivoRealizable = (double) object;
                break;
            case CO_MONEDASEGUROINDIVIDUAL:
                MonedaSeguroIndividual = (int) object;
                break;
            case CO_SEGUROINDIVIDUAL:
                SeguroIndividual = (int) object;
                break;
            case CO_MONEDAINGRESOBRUTOMENSUALVIDALEY:
                MonedaIngresoBrutoMensualVidaLey = (int) object;
                break;
            case CO_INGRESOBRUTOMENSUALVIDALEY:
                IngresoBrutoMensualVidaLey = (int) object;
                break;
            case CO_FACTORVIDALEY:
                FactorVidaLey = (int) object;
                break;
            case CO_MONEDATOPEVIDALEY:
                MonedaTopeVidaLey = (int) object;
                break;
            case CO_TOPEVIDALEY:
                TopeVidaLey = (double) object;
                break;
            case CO_MONEDASEGUROVIDALEY:
                MonedaSeguroVidaLey = (int) object;
                break;
            case CO_SEGUROVIDALEY:
                SeguroVidaLey = (double) object;
                break;
            case CO_FLAGCUENTACONVIDALEY:
                FlagCuentaConVidaLey = (int)object;
                break;
            case CO_MONEDATOTALSEGUROSVIDA:
                MonedaTotalSegurosVida = (int) object;
                break;
            case CO_TOTALSEGUROSVIDA:
                TotalSegurosVida = (double) object;
                break;
            case CO_PORCENTAJEAFPCONYUGE:
                PorcentajeAFPConyuge = (double) object;
                break;
            case CO_PORCENTAJEAFPHIJOS:
                PorcentajeAFPHijos = (double) object;
                break;
            case CO_NUMEROHIJOS:
                NumeroHijos = (int) object;
                break;
            case CO_MONEDAPENSIONCONYUGE:
                MonedaPensionConyuge = (int) object;
                break;
            case CO_PENSIONCONYUGE:
                PensionConyuge = (double) object;
                break;
            case CO_MONEDAPENSIONHIJOS:
                MonedaPensionHijos = (int) object;
                break;
            case CO_PENSIONHIJOS:
                PensionHijos = (double) object;
                break;
            case CO_FLAGPENSIONCONYUGE:
                FlagPensionConyuge = (int)object;
                break;
            case CO_FLAGPENSIONHIJOS:
                FlagPensionHijos = (int)object;
                break;
            case CO_FLAGPENSIONNOAFP:
                FlagPensionNoAFP = (int)object;
                break;
            case CO_MONEDATOTALPENSIONMENSUALAFP:
                MonedaTotalPensionMensualAFP = (int) object;
                break;
            case CO_TOTALPENSIONMENSUALAFP:
                TotalPensionMensualAFP = (double) object;
                break;
            case CO_MONEDAINGRESOMENSUALTITULAR:
                MonedaIngresoMensualTitular = (int) object;
                break;
            case CO_INGRESOMENSUALTITULAR:
                IngresoMensualTitular = (int) object;
                break;
            case CO_MONEDAINGRESOMENSUALCONYUGE:
                MonedaIngresoMensualConyuge = (int) object;
                break;
            case CO_INGRESOMENSUALCONYUGE:
                IngresoMensualConyuge = (int) object;
                break;
            case CO_MONEDAINGRESOFAMILIAROTROS:
                MonedaIngresoFamiliarOtros = (int) object;
                break;
            case CO_INGRESOFAMILIAROTROS:
                IngresoFamiliarOtros = (int) object;
                break;
            case CO_MONEDATOTALINGRESOFAMILIARMENSUAL:
                MonedaTotalIngresoFamiliarMensual = (int) object;
                break;
            case CO_TOTALINGRESOFAMILIARMENSUAL:
                TotalIngresoFamiliarMensual = (double) object;
                break;
            case CO_MONEDAGASTOVIVIENDA:
                MonedaGastoVivienda = (int) object;
                break;
            case CO_GASTOVIVIENDA:
                GastoVivienda = (int) object;
                break;
            case CO_MONEDAGASTOSERVICIOS:
                MonedaGastoServicios = (int) object;
                break;
            case CO_GASTOSERVICIOS:
                GastoServicios = (int) object;
                break;
            case CO_MONEDAGASTOHOGARALIMENTACION:
                MonedaGastoHogarAlimentacion = (int) object;
                break;
            case CO_GASTOHOGARALIMENTACION:
                GastoHogarAlimentacion = (int) object;
                break;
            case CO_MONEDAGASTOSALUD:
                MonedaGastoSalud = (int) object;
                break;
            case CO_GASTOSALUD:
                GastoSalud = (int) object;
                break;
            case CO_MONEDAGASTOEDUCACION:
                MonedaGastoEducacion = (int) object;
                break;
            case CO_GASTOEDUCACION:
                GastoEducacion = (int) object;
                break;
            case CO_MONEDAGASTOVEHICULOTRANSPORTE:
                MonedaGastoVehiculoTransporte = (int) object;
                break;
            case CO_GASTOVEHICULOTRANSPORTE:
                GastoVehiculoTransporte = (int) object;
                break;
            case CO_MONEDAGASTOESPARCIMIENTO:
                MonedaGastoEsparcimiento = (int) object;
                break;
            case CO_GASTOESPARCIMIENTO:
                GastoEsparcimiento = (int) object;
                break;
            case CO_MONEDAGASTOOTROS:
                MonedaGastoOtros = (int) object;
                break;
            case CO_GASTOOTROS:
                GastoOtros = (int) object;
                break;
            case CO_MONEDATOTALGASTOFAMILIARMENSUAL:
                MonedaTotalGastoFamiliarMensual = (int) object;
                break;
            case CO_TOTALGASTOFAMILIARMENSUAL:
                TotalGastoFamiliarMensual = (double) object;
                break;
            case CO_MONEDADEFICITMENSUAL:
                MonedaDeficitMensual = (int) object;
                break;
            case CO_DEFICITMENSUAL:
                DeficitMensual = (double) object;
                break;
            case CO_ANIOSPROTEGER:
                AniosProteger = (int) object;
                break;
            case CO_MONEDACAPITALNECESARIOFALLECIMIENTO:
                MonedaCapitalNecesarioFallecimiento = (int) object;
                break;
            case CO_CAPITALNECESARIOFALLECIMIENTO:
                CapitalNecesarioFallecimiento = (double) object;
                break;
            case CO_PORCENTAJEINVERSION:
                PorcentajeInversion = (double) object;
                break;
            case CO_MONEDAMONTOMENSUALINVERTIR:
                MonedaMontoMensualInvertir = (int) object;
                break;
            case CO_MONTOMENSUALINVERTIR:
                MontoMensualInvertir = (double) object;
                break;
            case CO_IDENTIDAD:
                IdEntidad = (int) object;
                break;
            case CO_CODIGOFRECUENCIAPAGO:
                CodigoFrecuenciaPago = (int) object;
                break;
            case CO_INDICADORVENTANA:
                IndicadorVentana = (int)object;
                break;
            case CO_FLAGTERMINADO:
                FlagTerminado = (int)object;
                break;
            case CO_FLAGADNDIGITAL:
                FlagADNDigital = (int)object;
                break;
            case CO_FECHACREACIONDISPOSITIVO:
                FechaCreacionDispositivo = (String) object;
                break;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                FechaModificacionDispositivo = (String) object;
                break;
            case CO_ADICIONALTEXTO1:
                AdicionalTexto1 = (String)object;
                break;
            case CO_ADICIONALNUMERICO1:
                AdicionalNumerico1 = (double)object;
                break;
            case CO_ADICIONALTEXTO2:
                AdicionalTexto2 = (String)object;
                break;
            case CO_ADICIONALNUMERICO2:
                AdicionalNumerico2 = (double)object;
                break;
            case CO_FLAGENVIADO:
                FlagEnviado = (int) object;
                break;
        }
    }

    public static final String CN_IDPROSPECTO = "IDPROSPECTO";
    public static final String CT_IDPROSPECTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTO = 1;

    public static final String CN_IDPROSPECTODISPOSITIVO = "IDPROSPECTODISPOSITIVO";
    public static final String CT_IDPROSPECTODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTODISPOSITIVO = 2;

    public static final String CN_TIPOCAMBIO = "TIPOCAMBIO";
    public static final String CT_TIPOCAMBIO = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_TIPOCAMBIO = 3;

    public static final String CN_MONEDAEFECTIVOAHORROS = "MONEDAEFECTIVOAHORROS";
    public static final String CT_MONEDAEFECTIVOAHORROS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAEFECTIVOAHORROS = 4;

    public static final String CN_EFECTIVOAHORROS = "EFECTIVOAHORROS";
    public static final String CT_EFECTIVOAHORROS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_EFECTIVOAHORROS = 5;

    public static final String CN_MONEDAPROPIEDADES = "MONEDAPROPIEDADES";
    public static final String CT_MONEDAPROPIEDADES = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAPROPIEDADES = 6;

    public static final String CN_PROPIEDADES = "PROPIEDADES";
    public static final String CT_PROPIEDADES = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_PROPIEDADES = 7;

    public static final String CN_MONEDAVEHICULOS = "MONEDAVEHICULOS";
    public static final String CT_MONEDAVEHICULOS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAVEHICULOS = 8;

    public static final String CN_VEHICULOS = "VEHICULOS";
    public static final String CT_VEHICULOS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_VEHICULOS = 9;

    public static final String CN_MONEDATOTALACTIVOREALIZABLE = "MONEDATOTALACTIVOREALIZABLE";
    public static final String CT_MONEDATOTALACTIVOREALIZABLE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDATOTALACTIVOREALIZABLE = 10;

    public static final String CN_TOTALACTIVOREALIZABLE = "TOTALACTIVOREALIZABLE";
    public static final String CT_TOTALACTIVOREALIZABLE = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_TOTALACTIVOREALIZABLE = 11;

    public static final String CN_MONEDASEGUROINDIVIDUAL = "MONEDASEGUROINDIVIDUAL";
    public static final String CT_MONEDASEGUROINDIVIDUAL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDASEGUROINDIVIDUAL = 12;

    public static final String CN_SEGUROINDIVIDUAL = "SEGUROINDIVIDUAL";
    public static final String CT_SEGUROINDIVIDUAL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_SEGUROINDIVIDUAL = 13;

    public static final String CN_MONEDAINGRESOBRUTOMENSUALVIDALEY = "MONEDAINGRESOBRUTOMENSUALVIDALEY";
    public static final String CT_MONEDAINGRESOBRUTOMENSUALVIDALEY = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAINGRESOBRUTOMENSUALVIDALEY = 14;

    public static final String CN_INGRESOBRUTOMENSUALVIDALEY = "INGRESOBRUTOMENSUALVIDALEY";
    public static final String CT_INGRESOBRUTOMENSUALVIDALEY = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_INGRESOBRUTOMENSUALVIDALEY = 15;

    public static final String CN_FACTORVIDALEY = "FACTORVIDALEY";
    public static final String CT_FACTORVIDALEY = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FACTORVIDALEY = 16;

    public static final String CN_MONEDATOPEVIDALEY = "MONEDATOPEVIDALEY";
    public static final String CT_MONEDATOPEVIDALEY = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDATOPEVIDALEY = 17;

    public static final String CN_TOPEVIDALEY = "TOPEVIDALEY";
    public static final String CT_TOPEVIDALEY = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_TOPEVIDALEY = 18;

    public static final String CN_MONEDASEGUROVIDALEY = "MONEDASEGUROVIDALEY";
    public static final String CT_MONEDASEGUROVIDALEY = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDASEGUROVIDALEY = 19;

    public static final String CN_SEGUROVIDALEY = "SEGUROVIDALEY";
    public static final String CT_SEGUROVIDALEY = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_SEGUROVIDALEY = 20;

    public static final String CN_FLAGCUENTACONVIDALEY = "FLAGCUENTACONVIDALEY";
    public static final String CT_FLAGCUENTACONVIDALEY = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGCUENTACONVIDALEY = 21;

    public static final String CN_MONEDATOTALSEGUROSVIDA = "MONEDATOTALSEGUROSVIDA";
    public static final String CT_MONEDATOTALSEGUROSVIDA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDATOTALSEGUROSVIDA = 22;

    public static final String CN_TOTALSEGUROSVIDA = "TOTALSEGUROSVIDA";
    public static final String CT_TOTALSEGUROSVIDA = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_TOTALSEGUROSVIDA = 23;

    public static final String CN_PORCENTAJEAFPCONYUGE = "PORCENTAJEAFPCONYUGE";
    public static final String CT_PORCENTAJEAFPCONYUGE = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_PORCENTAJEAFPCONYUGE = 24;

    public static final String CN_PORCENTAJEAFPHIJOS = "PORCENTAJEAFPHIJOS";
    public static final String CT_PORCENTAJEAFPHIJOS = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_PORCENTAJEAFPHIJOS = 25;

    public static final String CN_NUMEROHIJOS = "NUMEROHIJOS";
    public static final String CT_NUMEROHIJOS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_NUMEROHIJOS = 26;

    public static final String CN_MONEDAPENSIONCONYUGE = "MONEDAPENSIONCONYUGE";
    public static final String CT_MONEDAPENSIONCONYUGE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAPENSIONCONYUGE = 27;

    public static final String CN_PENSIONCONYUGE = "PENSIONCONYUGE";
    public static final String CT_PENSIONCONYUGE = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_PENSIONCONYUGE = 28;

    public static final String CN_MONEDAPENSIONHIJOS = "MONEDAPENSIONHIJOS";
    public static final String CT_MONEDAPENSIONHIJOS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAPENSIONHIJOS = 29;

    public static final String CN_PENSIONHIJOS = "PENSIONHIJOS";
    public static final String CT_PENSIONHIJOS = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_PENSIONHIJOS = 30;

    public static final String CN_FLAGPENSIONCONYUGE = "FLAGPENSIONCONYUGE";
    public static final String CT_FLAGPENSIONCONYUGE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGPENSIONCONYUGE = 31;

    public static final String CN_FLAGPENSIONHIJOS = "FLAGPENSIONHIJOS";
    public static final String CT_FLAGPENSIONHIJOS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGPENSIONHIJOS = 32;

    public static final String CN_FLAGPENSIONNOAFP = "FLAGPENSIONNOAFP";
    public static final String CT_FLAGPENSIONNOAFP = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGPENSIONNOAFP = 33;

    public static final String CN_MONEDATOTALPENSIONMENSUALAFP = "MONEDATOTALPENSIONMENSUALAFP";
    public static final String CT_MONEDATOTALPENSIONMENSUALAFP = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDATOTALPENSIONMENSUALAFP = 34;

    public static final String CN_TOTALPENSIONMENSUALAFP = "TOTALPENSIONMENSUALAFP";
    public static final String CT_TOTALPENSIONMENSUALAFP = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_TOTALPENSIONMENSUALAFP = 35;

    public static final String CN_MONEDAINGRESOMENSUALTITULAR = "MONEDAINGRESOMENSUALTITULAR";
    public static final String CT_MONEDAINGRESOMENSUALTITULAR = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAINGRESOMENSUALTITULAR = 36;

    public static final String CN_INGRESOMENSUALTITULAR = "INGRESOMENSUALTITULAR";
    public static final String CT_INGRESOMENSUALTITULAR = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_INGRESOMENSUALTITULAR = 37;

    public static final String CN_MONEDAINGRESOMENSUALCONYUGE = "CN_MONEDAINGRESOMENSUALCONYUGE";
    public static final String CT_MONEDAINGRESOMENSUALCONYUGE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAINGRESOMENSUALCONYUGE = 38;

    public static final String CN_INGRESOMENSUALCONYUGE = "INGRESOMENSUALCONYUGE";
    public static final String CT_INGRESOMENSUALCONYUGE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_INGRESOMENSUALCONYUGE = 39;

    public static final String CN_MONEDAINGRESOFAMILIAROTROS = "MONEDAINGRESOFAMILIAROTROS";
    public static final String CT_MONEDAINGRESOFAMILIAROTROS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAINGRESOFAMILIAROTROS = 40;

    public static final String CN_INGRESOFAMILIAROTROS = "INGRESOFAMILIAROTROS";
    public static final String CT_INGRESOFAMILIAROTROS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_INGRESOFAMILIAROTROS = 41;

    public static final String CN_MONEDATOTALINGRESOFAMILIARMENSUAL = "MONEDATOTALINGRESOFAMILIARMENSUAL";
    public static final String CT_MONEDATOTALINGRESOFAMILIARMENSUAL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDATOTALINGRESOFAMILIARMENSUAL = 42;

    public static final String CN_TOTALINGRESOFAMILIARMENSUAL = "TOTALINGRESOFAMILIARMENSUAL";
    public static final String CT_TOTALINGRESOFAMILIARMENSUAL = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_TOTALINGRESOFAMILIARMENSUAL = 43;

    public static final String CN_MONEDAGASTOVIVIENDA = "MONEDAGASTOVIVIENDA";
    public static final String CT_MONEDAGASTOVIVIENDA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAGASTOVIVIENDA = 44;

    public static final String CN_GASTOVIVIENDA = "GASTOVIVIENDA";
    public static final String CT_GASTOVIVIENDA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_GASTOVIVIENDA = 45;

    public static final String CN_MONEDAGASTOSERVICIOS = "MONEDAGASTOSERVICIOS";
    public static final String CT_MONEDAGASTOSERVICIOS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAGASTOSERVICIOS = 46;

    public static final String CN_GASTOSERVICIOS = "GASTOSERVICIOS";
    public static final String CT_GASTOSERVICIOS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_GASTOSERVICIOS = 47;

    public static final String CN_MONEDAGASTOHOGARALIMENTACION = "MONEDAGASTOHOGARALIMENTACION";
    public static final String CT_MONEDAGASTOHOGARALIMENTACION = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAGASTOHOGARALIMENTACION = 48;

    public static final String CN_GASTOHOGARALIMENTACION = "GASTOHOGARALIMENTACION";
    public static final String CT_GASTOHOGARALIMENTACION = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_GASTOHOGARALIMENTACION = 49;

    public static final String CN_MONEDAGASTOSALUD = "MONEDAGASTOSALUD";
    public static final String CT_MONEDAGASTOSALUD = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAGASTOSALUD = 50;

    public static final String CN_GASTOSALUD = "GASTOSALUD";
    public static final String CT_GASTOSALUD = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_GASTOSALUD = 51;

    public static final String CN_MONEDAGASTOEDUCACION = "MONEDAGASTOEDUCACION";
    public static final String CT_MONEDAGASTOEDUCACION = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAGASTOEDUCACION = 52;

    public static final String CN_GASTOEDUCACION = "GASTOEDUCACION";
    public static final String CT_GASTOEDUCACION = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_GASTOEDUCACION = 53;

    public static final String CN_MONEDAGASTOVEHICULOTRANSPORTE = "MONEDAGASTOVEHICULOTRANSPORTE";
    public static final String CT_MONEDAGASTOVEHICULOTRANSPORTE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAGASTOVEHICULOTRANSPORTE = 54;

    public static final String CN_GASTOVEHICULOTRANSPORTE = "GASTOVEHICULOTRANSPORTE";
    public static final String CT_GASTOVEHICULOTRANSPORTE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_GASTOVEHICULOTRANSPORTE = 55;

    public static final String CN_MONEDAGASTOESPARCIMIENTO = "MONEDAGASTOESPARCIMIENTO";
    public static final String CT_MONEDAGASTOESPARCIMIENTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAGASTOESPARCIMIENTO = 56;

    public static final String CN_GASTOESPARCIMIENTO = "GASTOESPARCIMIENTO";
    public static final String CT_GASTOESPARCIMIENTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_GASTOESPARCIMIENTO = 57;

    public static final String CN_MONEDAGASTOOTROS = "MONEDAGASTOOTROS";
    public static final String CT_MONEDAGASTOOTROS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAGASTOOTROS = 58;

    public static final String CN_GASTOOTROS = "GASTOOTROS";
    public static final String CT_GASTOOTROS = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_GASTOOTROS = 59;

    public static final String CN_MONEDATOTALGASTOFAMILIARMENSUAL = "MONEDATOTALGASTOFAMILIARMENSUAL";
    public static final String CT_MONEDATOTALGASTOFAMILIARMENSUAL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDATOTALGASTOFAMILIARMENSUAL = 60;

    public static final String CN_TOTALGASTOFAMILIARMENSUAL = "TOTALGASTOFAMILIARMENSUAL";
    public static final String CT_TOTALGASTOFAMILIARMENSUAL = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_TOTALGASTOFAMILIARMENSUAL = 61;

    public static final String CN_MONEDADEFICITMENSUAL = "MONEDADEFICITMENSUAL";
    public static final String CT_MONEDADEFICITMENSUAL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDADEFICITMENSUAL = 62;

    public static final String CN_DEFICITMENSUAL = "DEFICITMENSUAL";
    public static final String CT_DEFICITMENSUAL = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_DEFICITMENSUAL = 63;

    public static final String CN_ANIOSPROTEGER = "ANIOSPROTEGER";
    public static final String CT_ANIOSPROTEGER = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_ANIOSPROTEGER = 64;

    public static final String CN_MONEDACAPITALNECESARIOFALLECIMIENTO = "MONEDACAPITALNECESARIOFALLECIMIENTO";
    public static final String CT_MONEDACAPITALNECESARIOFALLECIMIENTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDACAPITALNECESARIOFALLECIMIENTO = 65;

    public static final String CN_CAPITALNECESARIOFALLECIMIENTO = "CAPITALNECESARIOFALLECIMIENTO";
    public static final String CT_CAPITALNECESARIOFALLECIMIENTO = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_CAPITALNECESARIOFALLECIMIENTO = 66;

    public static final String CN_PORCENTAJEINVERSION = "PORCENTAJEINVERSION";
    public static final String CT_PORCENTAJEINVERSION = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_PORCENTAJEINVERSION = 67;

    public static final String CN_MONEDAMONTOMENSUALINVERTIR = "MONEDAMONTOMENSUALINVERTIR";
    public static final String CT_MONEDAMONTOMENSUALINVERTIR = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MONEDAMONTOMENSUALINVERTIR = 68;

    public static final String CN_MONTOMENSUALINVERTIR = "MONTOMENSUALINVERTIR";
    public static final String CT_MONTOMENSUALINVERTIR = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_MONTOMENSUALINVERTIR = 69;

    public static final String CN_IDENTIDAD = "IDENTIDAD";
    public static final String CT_IDENTIDAD = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDENTIDAD = 70;

    public static final String CN_CODIGOFRECUENCIAPAGO = "CODIGOFRECUENCIAPAGO";
    public static final String CT_CODIGOFRECUENCIAPAGO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOFRECUENCIAPAGO = 71;

    public static final String CN_INDICADORVENTANA = "INDICADORVENTANA";
    public static final String CT_INDICADORVENTANA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_INDICADORVENTANA = 72;

    public static final String CN_FLAGADNDIGITAL = "FLAGADNDIGITAL";
    public static final String CT_FLAGADNDIGITAL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGADNDIGITAL = 73;

    public static final String CN_FLAGTERMINADO = "FLAGTERMINADO";
    public static final String CT_FLAGTERMINADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGTERMINADO = 74;

    public static final String CN_FECHACREACIONDISPOSITIVO = "FECHACREACIONDISPOSITIVO";
    public static final String CT_FECHACREACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACREACIONDISPOSITIVO = 75;

    public static final String CN_FECHAMODIFICACIONDISPOSITIVO = "FECHAMODIFICACIONDISPOSITIVO";
    public static final String CT_FECHAMODIFICACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAMODIFICACIONDISPOSITIVO = 76;

    public static final String CN_ADICIONALTEXTO1 = "ADICIONALTEXTO1";
    public static final String CT_ADICIONALTEXTO1 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_ADICIONALTEXTO1 = 77;

    public static final String CN_ADICIONALNUMERICO1 = "ADICIONALNUMERICO1";
    public static final String CT_ADICIONALNUMERICO1 = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_ADICIONALNUMERICO1 = 78;

    public static final String CN_ADICIONALTEXTO2 = "ADICIONALTEXTO2";
    public static final String CT_ADICIONALTEXTO2 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_ADICIONALTEXTO2 = 79;

    public static final String CN_ADICIONALNUMERICO2 = "ADICIONALNUMERICO2";
    public static final String CT_ADICIONALNUMERICO2 = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_ADICIONALNUMERICO2 = 80;

    public static final String CN_FLAGENVIADO = "FLAGENVIADO";
    public static final String CT_FLAGENVIADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIADO = 81;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_ADN, new OrganizateEntityFactory())
            .addColumn(CN_IDPROSPECTO, CT_IDPROSPECTO, CO_IDPROSPECTO)
            .addColumn(CN_IDPROSPECTODISPOSITIVO, CT_IDPROSPECTODISPOSITIVO, CO_IDPROSPECTODISPOSITIVO)
            .addColumn(CN_TIPOCAMBIO, CT_TIPOCAMBIO, CO_TIPOCAMBIO)
            .addColumn(CN_MONEDAEFECTIVOAHORROS, CT_MONEDAEFECTIVOAHORROS, CO_MONEDAEFECTIVOAHORROS)
            .addColumn(CN_EFECTIVOAHORROS, CT_EFECTIVOAHORROS, CO_EFECTIVOAHORROS)
            .addColumn(CN_MONEDAPROPIEDADES, CT_MONEDAPROPIEDADES, CO_MONEDAPROPIEDADES)
            .addColumn(CN_PROPIEDADES, CT_PROPIEDADES, CO_PROPIEDADES)
            .addColumn(CN_MONEDAVEHICULOS, CT_MONEDAVEHICULOS, CO_MONEDAVEHICULOS)
            .addColumn(CN_VEHICULOS, CT_VEHICULOS, CO_VEHICULOS)
            .addColumn(CN_MONEDATOTALACTIVOREALIZABLE, CT_MONEDATOTALACTIVOREALIZABLE, CO_MONEDATOTALACTIVOREALIZABLE)
            .addColumn(CN_TOTALACTIVOREALIZABLE, CT_TOTALACTIVOREALIZABLE, CO_TOTALACTIVOREALIZABLE)
            .addColumn(CN_MONEDASEGUROINDIVIDUAL, CT_MONEDASEGUROINDIVIDUAL, CO_MONEDASEGUROINDIVIDUAL)
            .addColumn(CN_SEGUROINDIVIDUAL, CT_SEGUROINDIVIDUAL, CO_SEGUROINDIVIDUAL)
            .addColumn(CN_MONEDAINGRESOBRUTOMENSUALVIDALEY, CT_MONEDAINGRESOBRUTOMENSUALVIDALEY, CO_MONEDAINGRESOBRUTOMENSUALVIDALEY)
            .addColumn(CN_INGRESOBRUTOMENSUALVIDALEY, CT_INGRESOBRUTOMENSUALVIDALEY, CO_INGRESOBRUTOMENSUALVIDALEY)
            .addColumn(CN_FACTORVIDALEY, CT_FACTORVIDALEY, CO_FACTORVIDALEY)
            .addColumn(CN_MONEDATOPEVIDALEY, CT_MONEDATOPEVIDALEY, CO_MONEDATOPEVIDALEY)
            .addColumn(CN_TOPEVIDALEY, CT_TOPEVIDALEY, CO_TOPEVIDALEY)
            .addColumn(CN_MONEDASEGUROVIDALEY, CT_MONEDASEGUROVIDALEY, CO_MONEDASEGUROVIDALEY)
            .addColumn(CN_SEGUROVIDALEY, CT_SEGUROVIDALEY, CO_SEGUROVIDALEY)
            .addColumn(CN_FLAGCUENTACONVIDALEY, CT_FLAGCUENTACONVIDALEY, CO_FLAGCUENTACONVIDALEY)
            .addColumn(CN_MONEDATOTALSEGUROSVIDA, CT_MONEDATOTALSEGUROSVIDA, CO_MONEDATOTALSEGUROSVIDA)
            .addColumn(CN_TOTALSEGUROSVIDA, CT_TOTALSEGUROSVIDA, CO_TOTALSEGUROSVIDA)
            .addColumn(CN_PORCENTAJEAFPCONYUGE, CT_PORCENTAJEAFPCONYUGE, CO_PORCENTAJEAFPCONYUGE)
            .addColumn(CN_PORCENTAJEAFPHIJOS, CT_PORCENTAJEAFPHIJOS, CO_PORCENTAJEAFPHIJOS)
            .addColumn(CN_NUMEROHIJOS, CT_NUMEROHIJOS, CO_NUMEROHIJOS)
            .addColumn(CN_MONEDAPENSIONCONYUGE, CT_MONEDAPENSIONCONYUGE, CO_MONEDAPENSIONCONYUGE)
            .addColumn(CN_PENSIONCONYUGE, CT_PENSIONCONYUGE, CO_PENSIONCONYUGE)
            .addColumn(CN_MONEDAPENSIONHIJOS, CT_MONEDAPENSIONHIJOS, CO_MONEDAPENSIONHIJOS)
            .addColumn(CN_PENSIONHIJOS, CT_PENSIONHIJOS, CO_PENSIONHIJOS)
            .addColumn(CN_FLAGPENSIONCONYUGE, CT_FLAGPENSIONCONYUGE, CO_FLAGPENSIONCONYUGE)
            .addColumn(CN_FLAGPENSIONHIJOS, CT_FLAGPENSIONHIJOS, CO_FLAGPENSIONHIJOS)
            .addColumn(CN_FLAGPENSIONNOAFP, CT_FLAGPENSIONNOAFP, CO_FLAGPENSIONNOAFP)
            .addColumn(CN_MONEDATOTALPENSIONMENSUALAFP, CT_MONEDATOTALPENSIONMENSUALAFP, CO_MONEDATOTALPENSIONMENSUALAFP)
            .addColumn(CN_TOTALPENSIONMENSUALAFP, CT_TOTALPENSIONMENSUALAFP, CO_TOTALPENSIONMENSUALAFP)
            .addColumn(CN_MONEDAINGRESOMENSUALTITULAR, CT_MONEDAINGRESOMENSUALTITULAR, CO_MONEDAINGRESOMENSUALTITULAR)
            .addColumn(CN_INGRESOMENSUALTITULAR, CT_INGRESOMENSUALTITULAR, CO_INGRESOMENSUALTITULAR)
            .addColumn(CN_MONEDAINGRESOMENSUALCONYUGE, CT_MONEDAINGRESOMENSUALCONYUGE, CO_MONEDAINGRESOMENSUALCONYUGE)
            .addColumn(CN_INGRESOMENSUALCONYUGE, CT_INGRESOMENSUALCONYUGE, CO_INGRESOMENSUALCONYUGE)
            .addColumn(CN_MONEDAINGRESOFAMILIAROTROS, CT_MONEDAINGRESOFAMILIAROTROS, CO_MONEDAINGRESOFAMILIAROTROS)
            .addColumn(CN_INGRESOFAMILIAROTROS, CT_INGRESOFAMILIAROTROS, CO_INGRESOFAMILIAROTROS)
            .addColumn(CN_MONEDATOTALINGRESOFAMILIARMENSUAL, CT_MONEDATOTALINGRESOFAMILIARMENSUAL, CO_MONEDATOTALINGRESOFAMILIARMENSUAL)
            .addColumn(CN_TOTALINGRESOFAMILIARMENSUAL, CT_TOTALINGRESOFAMILIARMENSUAL, CO_TOTALINGRESOFAMILIARMENSUAL)
            .addColumn(CN_MONEDAGASTOVIVIENDA, CT_MONEDAGASTOVIVIENDA, CO_MONEDAGASTOVIVIENDA)
            .addColumn(CN_GASTOVIVIENDA, CT_GASTOVIVIENDA, CO_GASTOVIVIENDA)
            .addColumn(CN_MONEDAGASTOSERVICIOS, CT_MONEDAGASTOSERVICIOS, CO_MONEDAGASTOSERVICIOS)
            .addColumn(CN_GASTOSERVICIOS, CT_GASTOSERVICIOS, CO_GASTOSERVICIOS)
            .addColumn(CN_MONEDAGASTOHOGARALIMENTACION, CT_MONEDAGASTOHOGARALIMENTACION, CO_MONEDAGASTOHOGARALIMENTACION)
            .addColumn(CN_GASTOHOGARALIMENTACION, CT_GASTOHOGARALIMENTACION, CO_GASTOHOGARALIMENTACION)
            .addColumn(CN_MONEDAGASTOSALUD, CT_MONEDAGASTOSALUD, CO_MONEDAGASTOSALUD)
            .addColumn(CN_GASTOSALUD, CT_GASTOSALUD, CO_GASTOSALUD)
            .addColumn(CN_MONEDAGASTOEDUCACION, CT_MONEDAGASTOEDUCACION, CO_MONEDAGASTOEDUCACION)
            .addColumn(CN_GASTOEDUCACION, CT_GASTOEDUCACION, CO_GASTOEDUCACION)
            .addColumn(CN_MONEDAGASTOVEHICULOTRANSPORTE, CT_MONEDAGASTOVEHICULOTRANSPORTE, CO_MONEDAGASTOVEHICULOTRANSPORTE)
            .addColumn(CN_GASTOVEHICULOTRANSPORTE, CT_GASTOVEHICULOTRANSPORTE, CO_GASTOVEHICULOTRANSPORTE)
            .addColumn(CN_MONEDAGASTOESPARCIMIENTO, CT_MONEDAGASTOESPARCIMIENTO, CO_MONEDAGASTOESPARCIMIENTO)
            .addColumn(CN_GASTOESPARCIMIENTO, CT_GASTOESPARCIMIENTO, CO_GASTOESPARCIMIENTO)
            .addColumn(CN_MONEDAGASTOOTROS, CT_MONEDAGASTOOTROS, CO_MONEDAGASTOOTROS)
            .addColumn(CN_GASTOOTROS, CT_GASTOOTROS, CO_GASTOOTROS)
            .addColumn(CN_MONEDATOTALGASTOFAMILIARMENSUAL, CT_MONEDATOTALGASTOFAMILIARMENSUAL, CO_MONEDATOTALGASTOFAMILIARMENSUAL)
            .addColumn(CN_TOTALGASTOFAMILIARMENSUAL, CT_TOTALGASTOFAMILIARMENSUAL, CO_TOTALGASTOFAMILIARMENSUAL)
            .addColumn(CN_MONEDADEFICITMENSUAL, CT_MONEDADEFICITMENSUAL, CO_MONEDADEFICITMENSUAL)
            .addColumn(CN_DEFICITMENSUAL, CT_DEFICITMENSUAL, CO_DEFICITMENSUAL)
            .addColumn(CN_ANIOSPROTEGER, CT_ANIOSPROTEGER, CO_ANIOSPROTEGER)
            .addColumn(CN_MONEDACAPITALNECESARIOFALLECIMIENTO, CT_MONEDACAPITALNECESARIOFALLECIMIENTO, CO_MONEDACAPITALNECESARIOFALLECIMIENTO)
            .addColumn(CN_CAPITALNECESARIOFALLECIMIENTO, CT_CAPITALNECESARIOFALLECIMIENTO, CO_CAPITALNECESARIOFALLECIMIENTO)
            .addColumn(CN_PORCENTAJEINVERSION, CT_PORCENTAJEINVERSION, CO_PORCENTAJEINVERSION)
            .addColumn(CN_MONEDAMONTOMENSUALINVERTIR, CT_MONEDAMONTOMENSUALINVERTIR, CO_MONEDAMONTOMENSUALINVERTIR)
            .addColumn(CN_MONTOMENSUALINVERTIR, CT_MONTOMENSUALINVERTIR, CO_MONTOMENSUALINVERTIR)
            .addColumn(CN_IDENTIDAD, CT_IDENTIDAD, CO_IDENTIDAD)
            .addColumn(CN_CODIGOFRECUENCIAPAGO, CT_CODIGOFRECUENCIAPAGO, CO_CODIGOFRECUENCIAPAGO)
            .addColumn(CN_INDICADORVENTANA, CT_INDICADORVENTANA, CO_INDICADORVENTANA)
            .addColumn(CN_FLAGTERMINADO, CT_FLAGTERMINADO, CO_FLAGTERMINADO)
            .addColumn(CN_FLAGADNDIGITAL, CT_FLAGADNDIGITAL, CO_FLAGADNDIGITAL)
            .addColumn(CN_FECHACREACIONDISPOSITIVO, CT_FECHACREACIONDISPOSITIVO, CO_FECHACREACIONDISPOSITIVO)
            .addColumn(CN_FECHAMODIFICACIONDISPOSITIVO, CT_FECHAMODIFICACIONDISPOSITIVO, CO_FECHAMODIFICACIONDISPOSITIVO)
            .addColumn(CN_ADICIONALTEXTO1, CT_ADICIONALTEXTO1, CO_ADICIONALTEXTO1)
            .addColumn(CN_ADICIONALNUMERICO1, CT_ADICIONALNUMERICO1, CO_ADICIONALNUMERICO1)
            .addColumn(CN_ADICIONALTEXTO2, CT_ADICIONALTEXTO2, CO_ADICIONALTEXTO2)
            .addColumn(CN_ADICIONALNUMERICO2, CT_ADICIONALNUMERICO2, CO_ADICIONALNUMERICO2)
            .addColumn(CN_FLAGENVIADO, CT_FLAGENVIADO, CO_FLAGENVIADO);
}