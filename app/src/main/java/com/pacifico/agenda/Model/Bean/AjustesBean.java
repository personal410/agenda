package com.pacifico.agenda.Model.Bean;


import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by daniel on 06/09/2016.
 */
public class AjustesBean extends Entity {

    private int IdAjuste;
    private String Descripcion;
    private String valor1;
    private String valor2;
    private String valor3;
    private String valor4;
    private String valor5;

    private int FlActivo;

    public void AjustesBean() {
        IdAjuste = 1;
        Descripcion = "Ajuste por defecto";
        valor1 = "10";
        valor2 = "0";
        valor3 = "0";
        valor4 = "0";
        valor5 = "0";
    }

    public int getIdAjuste() {
        return IdAjuste;
    }

    public void setIdAjuste(int idAjuste) {
        IdAjuste = idAjuste;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getValor1() {
        return valor1;
    }

    public void setValor1(String valor1) {
        this.valor1 = valor1;
    }

    public String getValor2() {
        return valor2;
    }

    public void setValor2(String valor2) {
        this.valor2 = valor2;
    }

    public String getValor3() {
        return valor3;
    }

    public void setValor3(String valor3) {
        this.valor3 = valor3;
    }

    public String getValor4() {
        return valor4;
    }

    public void setValor4(String valor4) {
        this.valor4 = valor4;
    }

    public String getValor5() {
        return valor5;
    }

    public void setValor5(String valor5) {
        this.valor5 = valor5;
    }

    public int getFlActivo() {
        return FlActivo;
    }

    public void setFlActivo(int flActivo) {
        FlActivo = flActivo;
    }

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDAJUSTE:
                return IdAjuste;
            case CO_DESCRIPCION:
                return Descripcion;
            case CO_VALOR1:
                return valor1;
            case CO_VALOR2:
                return valor2;
            case CO_VALOR3:
                return valor3;
            case CO_VALOR4:
                return valor4;
            case CO_VALOR5:
                return valor5;

        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDAJUSTE:
                IdAjuste = (int) object;
                break;
            case CO_DESCRIPCION:
                Descripcion = (String) object;
                break;
            case CO_VALOR1:
                valor1 = (String) object;
                break;
            case CO_VALOR2:
                valor2 = (String) object;
                break;
            case CO_VALOR3:
                valor3 = (String) object;
                break;
            case CO_VALOR4:
                valor4 = (String) object;
                break;
            case CO_VALOR5:
                valor5 = (String) object;
                break;

    }}

    public static final String CN_IDAJUSTE= "IDAJUSTE";
    public static final String CT_IDAJUSTE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDAJUSTE = 1;

    public static final String CN_DESCRIPCION= "DESCRIPCION";
    public static final String CT_DESCRIPCION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_DESCRIPCION = 2;

    public static final String CN_VALOR1 = "VALOR1";
    public static final String CT_VALOR1 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_VALOR1 = 3;

    public static final String CN_VALOR2 = "VALOR2";
    public static final String CT_VALOR2 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_VALOR2 = 4;

    public static final String CN_VALOR3 = "VALOR3";
    public static final String CT_VALOR3 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_VALOR3 = 5;

    public static final String CN_VALOR4 = "VALOR4";
    public static final String CT_VALOR4 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_VALOR4 = 6;

    public static final String CN_VALOR5 = "VALOR5";
    public static final String CT_VALOR5 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_VALOR5 = 7;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_AJUSTES, new OrganizateEntityFactory())
            .addColumn(CN_IDAJUSTE, CT_IDAJUSTE, CO_IDAJUSTE)
            .addColumn(CN_DESCRIPCION, CT_DESCRIPCION, CO_DESCRIPCION)
            .addColumn(CN_VALOR1, CT_VALOR1, CO_VALOR1)
            .addColumn(CN_VALOR2, CT_VALOR2, CO_VALOR2)
            .addColumn(CN_VALOR3, CT_VALOR3, CO_VALOR3)
            .addColumn(CN_VALOR4, CT_VALOR4, CO_VALOR4)
            .addColumn(CN_VALOR5, CT_VALOR5, CO_VALOR5)
            ;
}
