package com.pacifico.agenda.Model.Bean;

import android.util.Log;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class FamiliarBean extends Entity{
    private int IdFamiliar;
    private int IdFamiliarDispositivo;
    private int IdProspecto;
    private int IdProspectoDispositivo;
    private String Nombres;
    private String ApellidoPaterno;
    private String ApellidoMaterno;
    private String FechaNacimiento;
    private int Edad;
    private String Ocupacion;
    private int CodigoTipoFamiliar;
    private String FechaCreacionDispositivo;
    private String FechaModificacionDispositivo;
    private int FlagEnviado;
    private int FlagActivo;
    private int FlagModificado = 0;

    public void inicializarValores(){
        IdFamiliar = -1;
        IdFamiliarDispositivo = -1;
        Nombres = "";
        ApellidoPaterno = "";
        ApellidoMaterno = "";
        FechaNacimiento = "";
        Ocupacion = "";
        Edad = -1;
        FlagActivo = 1;
        FlagEnviado = 0;
    }

    public int getIdFamiliar() {
        return IdFamiliar;
    }

    public void setIdFamiliar(int idFamiliar) {
        IdFamiliar = idFamiliar;
    }

    public int getIdFamiliarDispositivo() {
        return IdFamiliarDispositivo;
    }

    public void setIdFamiliarDispositivo(int idFamiliarDispositivo) {
        IdFamiliarDispositivo = idFamiliarDispositivo;
    }

    public int getIdProspecto() {
        return IdProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        IdProspecto = idProspecto;
    }

    public int getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        FechaNacimiento = fechaNacimiento;
    }

    public Integer getEdad() {
        return Edad;
    }

    public void setEdad(Integer edad) {
        Edad = edad;
    }

    public String getOcupacion() {
        return Ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        Ocupacion = ocupacion;
    }

    public int getCodigoTipoFamiliar() {
        return CodigoTipoFamiliar;
    }

    public void setCodigoTipoFamiliar(int codigoTipoFamiliar) {
        CodigoTipoFamiliar = codigoTipoFamiliar;
    }

    public String getFechaCreacionDispositivo() {
        return FechaCreacionDispositivo;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return FechaModificacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public int getFlagEnviado() {
        return FlagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        FlagEnviado = flagEnviado;
    }

    public int getFlagActivo() {
        return FlagActivo;
    }

    public void setFlagActivo(int flagActivo) {
        FlagActivo = flagActivo;
    }

    public int getFlagModificado() {
        return FlagModificado;
    }

    public void setFlagModificado(int flagModificado) {
        FlagModificado = flagModificado;
    }
    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDFAMILIAR:
                return IdFamiliar;
            case CO_IDFAMILIARDISPOSITIVO:
                return IdFamiliarDispositivo;
            case CO_IDPROSPECTO:
                return IdProspecto;
            case CO_IDPROSPECTODISPOSITIVO:
                return IdProspectoDispositivo;
            case CO_NOMBRES:
                return Nombres;
            case CO_APELLIDOPATERNO:
                return ApellidoPaterno;
            case CO_APELLIDOMATERNO:
                return ApellidoMaterno;
            case CO_FECHANACIMIENTO:
                return FechaNacimiento;
            case CO_EDAD:
                return Edad;
            case CO_OCUPACION:
                return Ocupacion;
            case CO_CODIGOTIPOFAMILIAR:
                return CodigoTipoFamiliar;
            case CO_FECHACREACIONDISPOSITIVO:
                return FechaCreacionDispositivo;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                return FechaModificacionDispositivo;
            case CO_FLAGENVIADO:
                return FlagEnviado;
            case CO_FLAGACTIVO:
                return FlagActivo;
        }
        return null;
    }
    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDFAMILIAR:
                IdFamiliar = (int) object;
                break;
            case CO_IDFAMILIARDISPOSITIVO:
                IdFamiliarDispositivo = (int) object;
                break;
            case CO_IDPROSPECTO:
                IdProspecto = (int) object;
                break;
            case CO_IDPROSPECTODISPOSITIVO:
                IdProspectoDispositivo = (int) object;
                break;
            case CO_NOMBRES:
                Nombres = (String) object;
                break;
            case CO_APELLIDOPATERNO:
                ApellidoPaterno = (String) object;
                break;
            case CO_APELLIDOMATERNO:
                ApellidoMaterno = (String) object;
                break;
            case CO_FECHANACIMIENTO:
                FechaNacimiento = (String) object;
                break;
            case CO_EDAD:
                Edad = (int) object;
                break;
            case CO_OCUPACION:
                Ocupacion = (String) object;
                break;
            case CO_CODIGOTIPOFAMILIAR:
                CodigoTipoFamiliar = (int) object;
                break;
            case CO_FECHACREACIONDISPOSITIVO:
                FechaCreacionDispositivo = (String) object;
                break;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                FechaModificacionDispositivo = (String) object;
                break;
            case CO_FLAGENVIADO:
                FlagEnviado = (int) object;
                break;
            case CO_FLAGACTIVO:
                FlagActivo = (int) object;
                break;
        }
    }
    public static final String CN_IDFAMILIAR = "IDFAMILIAR";
    public static final String CT_IDFAMILIAR = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDFAMILIAR = 1;

    public static final String CN_IDFAMILIARDISPOSITIVO = "IDFAMILIARDISPOSITIVO";
    public static final String CT_IDFAMILIARDISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDFAMILIARDISPOSITIVO = 2;

    public static final String CN_IDPROSPECTO = "IDPROSPECTO";
    public static final String CT_IDPROSPECTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTO = 3;

    public static final String CN_IDPROSPECTODISPOSITIVO = "IDPROSPECTODISPOSITIVO";
    public static final String CT_IDPROSPECTODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTODISPOSITIVO = 4;

    public static final String CN_NOMBRES = "NOMBRES";
    public static final String CT_NOMBRES = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOMBRES = 5;

    public static final String CN_APELLIDOPATERNO = "APELLIDOPATERNO";
    public static final String CT_APELLIDOPATERNO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_APELLIDOPATERNO = 6;

    public static final String CN_APELLIDOMATERNO = "APELLIDOMATERNO";
    public static final String CT_APELLIDOMATERNO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_APELLIDOMATERNO = 7;

    public static final String CN_FECHANACIMIENTO = "FECHANACIMIENTO";
    public static final String CT_FECHANACIMIENTO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHANACIMIENTO = 8;

    public static final String CN_EDAD = "EDAD";
    public static final String CT_EDAD = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_EDAD = 9;

    public static final String CN_OCUPACION = "OCUPACION";
    public static final String CT_OCUPACION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_OCUPACION = 10;

    public static final String CN_CODIGOTIPOFAMILIAR = "CODIGOTIPOFAMILIAR";
    public static final String CT_CODIGOTIPOFAMILIAR = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOTIPOFAMILIAR = 11;

    public static final String CN_FECHACREACIONDISPOSITIVO = "FECHACREACIONDISPOSITIVO";
    public static final String CT_FECHACREACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACREACIONDISPOSITIVO = 12;

    public static final String CN_FECHAMODIFICACIONDISPOSITIVO = "FECHAMODIFICACIONDISPOSITIVO";
    public static final String CT_FECHAMODIFICACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAMODIFICACIONDISPOSITIVO = 13;

    public static final String CN_FLAGENVIADO = "FLAGENVIADO";
    public static final String CT_FLAGENVIADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIADO = 14;

    public static final String CN_FLAGACTIVO = "FLAGACTIVO";
    public static final String CT_FLAGACTIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGACTIVO = 15;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_FAMILIAR, new OrganizateEntityFactory())
            .addColumn(CN_IDFAMILIAR, CT_IDFAMILIAR, CO_IDFAMILIAR)
            .addColumn(CN_IDFAMILIARDISPOSITIVO, CT_IDFAMILIARDISPOSITIVO, CO_IDFAMILIARDISPOSITIVO)
            .addColumn(CN_IDPROSPECTO, CT_IDPROSPECTO, CO_IDPROSPECTO)
            .addColumn(CN_IDPROSPECTODISPOSITIVO, CT_IDPROSPECTODISPOSITIVO, CO_IDPROSPECTODISPOSITIVO)
            .addColumn(CN_NOMBRES, CT_NOMBRES, CO_NOMBRES)
            .addColumn(CN_APELLIDOPATERNO, CT_APELLIDOPATERNO, CO_APELLIDOPATERNO)
            .addColumn(CN_APELLIDOMATERNO, CT_APELLIDOMATERNO, CO_APELLIDOMATERNO)
            .addColumn(CN_FECHANACIMIENTO, CT_FECHANACIMIENTO, CO_FECHANACIMIENTO)
            .addColumn(CN_EDAD, CT_EDAD, CO_EDAD)
            .addColumn(CN_OCUPACION, CT_OCUPACION, CO_OCUPACION)
            .addColumn(CN_CODIGOTIPOFAMILIAR, CT_CODIGOTIPOFAMILIAR, CO_CODIGOTIPOFAMILIAR)
            .addColumn(CN_FECHACREACIONDISPOSITIVO, CT_FECHACREACIONDISPOSITIVO, CO_FECHACREACIONDISPOSITIVO)
            .addColumn(CN_FECHAMODIFICACIONDISPOSITIVO, CT_FECHAMODIFICACIONDISPOSITIVO, CO_FECHAMODIFICACIONDISPOSITIVO)
            .addColumn(CN_FLAGENVIADO, CT_FLAGENVIADO, CO_FLAGENVIADO)
            .addColumn(CN_FLAGACTIVO, CT_FLAGACTIVO, CO_FLAGACTIVO);
}