package com.pacifico.agenda.Model.Bean;


import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class ParametroBean extends Entity {

    private int IdParametro;
    private String Descripcion;
    private String ValorCadena;
    private double ValorNumerico;

    public int getIdParametro() {
        return IdParametro;
    }

    public void setIdParametro(int idParametro) {
        IdParametro = idParametro;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getValorCadena() {
        return ValorCadena;
    }

    public void setValorCadena(String valorCadena) {
        ValorCadena = valorCadena;
    }

    public double getValorNumerico() {
        return ValorNumerico;
    }

    public void setValorNumerico(double valorNumerico) {
        ValorNumerico = valorNumerico;
    }

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDPARAMETRO:
                return IdParametro;

            case CO_DESCRIPCION:
                return Descripcion;

            case CO_VALORCADENA:
                return ValorCadena;

            case CO_VALORNUMERICO:
                return ValorNumerico;
        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDPARAMETRO:
                IdParametro = (int) object;
                break;
            case CO_DESCRIPCION:
                Descripcion = (String) object;
                break;
            case CO_VALORCADENA:
                ValorCadena = (String) object;
                break;
            case CO_VALORNUMERICO:
                ValorNumerico = (double) object;
                break;
        }
    }

    public static final String CN_IDPARAMETRO = "IDPARAMETRO";
    public static final String CT_IDPARAMETRO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPARAMETRO = 1;

    public static final String CN_DESCRIPCION = "DESCRIPCION";
    public static final String CT_DESCRIPCION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_DESCRIPCION = 2;

    public static final String CN_VALORCADENA = "VALORCADENA";
    public static final String CT_VALORCADENA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_VALORCADENA = 3;

    public static final String CN_VALORNUMERICO = "VALORNUMERICO";
    public static final String CT_VALORNUMERICO = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_VALORNUMERICO = 4;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_PARAMETRO, new OrganizateEntityFactory())
            .addColumn(CN_IDPARAMETRO, CT_IDPARAMETRO, CO_IDPARAMETRO)
            .addColumn(CN_DESCRIPCION, CT_DESCRIPCION, CO_DESCRIPCION)
            .addColumn(CN_VALORCADENA, CT_VALORCADENA, CO_VALORCADENA)
            .addColumn(CN_VALORNUMERICO, CT_VALORNUMERICO, CO_VALORNUMERICO);
}
