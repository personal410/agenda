package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

public class ReminderBean extends Entity {
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd kk:mm:ss";

    private String id;

    private String title;
    private String body;
    private String reminderDateTime;

    // TIPO ID ORIGINAL
    private int idDispositivoFuente;
    private int fuenteAlerta;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getReminderDateTime() {
        return reminderDateTime;
    }

    public void setReminderDateTime(String reminderDateTime) {
        this.reminderDateTime = reminderDateTime;
    }

    public int getIdDispositivoFuente() {
        return idDispositivoFuente;
    }

    public void setIdDispositivoFuente(int idDispositivoFuente) {
        this.idDispositivoFuente = idDispositivoFuente;
    }

    public int getFuenteAlerta() {
        return fuenteAlerta;
    }

    public void setFuenteAlerta(int fuenteAlerta) {
        this.fuenteAlerta = fuenteAlerta;
    }

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_ID:
                return id;
            case CO_TITLE:
                return title;
            case CO_BODY:
                return body;
            case CO_REMINDERDATETIME:
                return reminderDateTime;

            case CO_IDDISPOSITIVOFUENTE:
                return idDispositivoFuente;
            case CO_FUENTEALERTA:
                return fuenteAlerta;

        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_ID:
                id = (String) object;
                break;
            case CO_TITLE:
                title = (String) object;
                break;
            case CO_BODY:
                body = (String) object;
                break;
            case CO_REMINDERDATETIME:
                reminderDateTime = (String) object;
                break;

            case CO_IDDISPOSITIVOFUENTE:
                idDispositivoFuente = (int) object;
            case CO_FUENTEALERTA:
                fuenteAlerta = (int) object;
        }
    }

    public static final String CN_ID = "ID";
    public static final String CT_ID = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_ID = 1;

    public static final String CN_TITLE = "TITLE";
    public static final String CT_TITLE = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_TITLE = 2;

    public static final String CN_BODY = "BODY";
    public static final String CT_BODY = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_BODY = 3;

    public static final String CN_REMINDERDATETIME = "REMINDERDATETIME";
    public static final String CT_REMINDERDATETIME = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_REMINDERDATETIME = 4;

    public static final String CN_IDDISPOSITIVOFUENTE = "IDDISPOSITIVOFUENTE";
    public static final String CT_IDDISPOSITIVOFUENTE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDDISPOSITIVOFUENTE = 5;

    public static final String CN_FUENTEALERTA = "FUENTEALERTA";
    public static final String CT_FUENTEALERTA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FUENTEALERTA = 6;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_RECORDATORIO_GENERAL, new OrganizateEntityFactory())
            .addColumn(CN_ID, CT_ID, CO_ID)
            .addColumn(CN_TITLE, CT_TITLE, CO_TITLE)
            .addColumn(CN_BODY, CT_BODY, CO_BODY)
            .addColumn(CN_REMINDERDATETIME, CT_REMINDERDATETIME, CO_REMINDERDATETIME)
            .addColumn(CN_IDDISPOSITIVOFUENTE, CT_IDDISPOSITIVOFUENTE, CO_IDDISPOSITIVOFUENTE)
            .addColumn(CN_FUENTEALERTA, CT_FUENTEALERTA, CO_FUENTEALERTA);
}
