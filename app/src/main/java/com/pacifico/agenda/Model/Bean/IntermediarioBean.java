package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class IntermediarioBean extends Entity {
    private int CodigoIntermediario;
    private String NombreRazonSocial;
    private String TipoDocumento;
    private String DocumentoIdentidad;
    private String TelefonoCelular;
    private String TelefonoFijo;
    private String CorreoElectronico;
    private String FechaPromocion;
    private String NombreGU;
    private String CorreoElectronicoGU;
    private String NombreGA;
    private String CorreoElectronicoGA;
    private String DescripcionAgencia;
    private String DescripcionOficina;
    private String DescripcionCategoria;
    private int CodigoDepartamento;
    private String Login;
    private String Token;

    public int getCodigoIntermediario() {
        return CodigoIntermediario;
    }

    public void setCodigoIntermediario(int codigoIntermediario) {
        CodigoIntermediario = codigoIntermediario;
    }

    public String getNombreRazonSocial() {
        return NombreRazonSocial;
    }

    public void setNombreRazonSocial(String nombreRazonSocial) {
        NombreRazonSocial = nombreRazonSocial;
    }

    public String getTipoDocumento() {
        return TipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        TipoDocumento = tipoDocumento;
    }

    public String getDocumentoIdentidad() {
        return DocumentoIdentidad;
    }

    public void setDocumentoIdentidad(String documentoIdentidad) {
        DocumentoIdentidad = documentoIdentidad;
    }

    public String getTelefonoCelular() {
        return TelefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        TelefonoCelular = telefonoCelular;
    }

    public String getTelefonoFijo() {
        return TelefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        TelefonoFijo = telefonoFijo;
    }

    public String getCorreoElectronico() {
        return CorreoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        CorreoElectronico = correoElectronico;
    }

    public String getFechaPromocion() {
        return FechaPromocion;
    }

    public void setFechaPromocion(String fechaPromocion) {
        FechaPromocion = fechaPromocion;
    }

    public String getNombreGU() {
        return NombreGU;
    }

    public void setNombreGU(String nombreGU) {
        NombreGU = nombreGU;
    }

    public String getCorreoElectronicoGU() {
        return CorreoElectronicoGU;
    }

    public void setCorreoElectronicoGU(String correoElectronicoGU) {
        CorreoElectronicoGU = correoElectronicoGU;
    }

    public String getNombreGA() {
        return NombreGA;
    }

    public void setNombreGA(String nombreGA) {
        NombreGA = nombreGA;
    }

    public String getCorreoElectronicoGA() {
        return CorreoElectronicoGA;
    }

    public void setCorreoElectronicoGA(String correoElectronicoGA) {
        CorreoElectronicoGA = correoElectronicoGA;
    }

    public String getDescripcionAgencia() {
        return DescripcionAgencia;
    }

    public void setDescripcionAgencia(String descripcionAgencia) {
        DescripcionAgencia = descripcionAgencia;
    }

    public String getDescripcionOficina() {
        return DescripcionOficina;
    }

    public void setDescripcionOficina(String descripcionOficina) {
        DescripcionOficina = descripcionOficina;
    }

    public String getDescripcionCategoria() {
        return DescripcionCategoria;
    }

    public void setDescripcionCategoria(String descripcionCategoria) {
        DescripcionCategoria = descripcionCategoria;
    }

    public int getCodigoDepartamento() {
        return CodigoDepartamento;
    }

    public void setCodigoDepartamento(int codigoDepartamento) {
        CodigoDepartamento = codigoDepartamento;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_CODIGOINTERMEDIARIO:
                return CodigoIntermediario;
            case CO_NOMBRERAZONSOCIAL:
                return NombreRazonSocial;
            case CO_TIPODOCUMENTO:
                return TipoDocumento;
            case CO_DOCUMENTOIDENTIDAD:
                return DocumentoIdentidad;
            case CO_TELEFONOCELULAR:
                return TelefonoCelular;
            case CO_TELEFONOFIJO:
                return TelefonoFijo;
            case CO_CORREOELECTRONICO:
                return CorreoElectronico;
            case CO_FECHAPROMOCION:
                return FechaPromocion;
            case CO_NOMBREGU:
                return NombreGU;
            case CO_CORREOELECTRONICOGU:
                return CorreoElectronicoGU;
            case CO_NOMBREGA:
                return NombreGA;
            case CO_CORREOELECTRONICOGA:
                return CorreoElectronicoGA;
            case CO_DESCRIPCIONAGENCIA:
                return DescripcionAgencia;
            case CO_DESCRIPCIONOFICINA:
                return DescripcionOficina;
            case CO_DESCRIPCIONCATEGORIA:
                return DescripcionCategoria;
            case CO_CODIGODEPARTAMENTO:
                return CodigoDepartamento;
            case CO_LOGIN:
                return Login;
            case CO_TOKEN:
                return Token;
        }
        return null;
    }
    @Override
    public void setColumnValue(int column, Object object) {
        switch(column){
            case CO_CODIGOINTERMEDIARIO:
                CodigoIntermediario = (int) object;
                break;
            case CO_NOMBRERAZONSOCIAL:
                NombreRazonSocial = (String) object;
                break;
            case CO_TIPODOCUMENTO:
                TipoDocumento = (String) object;
                break;
            case CO_DOCUMENTOIDENTIDAD:
                DocumentoIdentidad = (String) object;
                break;
            case CO_TELEFONOCELULAR:
                TelefonoCelular = (String) object;
                break;
            case CO_TELEFONOFIJO:
                TelefonoFijo = (String) object;
                break;
            case CO_CORREOELECTRONICO:
                CorreoElectronico = (String) object;
                break;
            case CO_FECHAPROMOCION:
                FechaPromocion = (String) object;
                break;
            case CO_NOMBREGU:
                NombreGU = (String) object;
                break;
            case CO_CORREOELECTRONICOGU:
                CorreoElectronicoGU = (String) object;
                break;
            case CO_NOMBREGA:
                NombreGA = (String) object;
                break;
            case CO_CORREOELECTRONICOGA:
                CorreoElectronicoGA = (String) object;
                break;
            case CO_DESCRIPCIONAGENCIA:
                DescripcionAgencia = (String) object;
                break;
            case CO_DESCRIPCIONOFICINA:
                DescripcionOficina = (String) object;
                break;
            case CO_DESCRIPCIONCATEGORIA:
                DescripcionCategoria = (String) object;
                break;
            case CO_CODIGODEPARTAMENTO:
                CodigoDepartamento = (int) object;
                break;
            case CO_LOGIN:
                Login = (String)object;
                break;
            case CO_TOKEN:
                Token = (String)object;
                break;
        }
    }
    public static final String CN_CODIGOINTERMEDIARIO = "CODIGOINTERMEDIARIO";
    public static final String CT_CODIGOINTERMEDIARIO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOINTERMEDIARIO = 1;

    public static final String CN_NOMBRERAZONSOCIAL = "NOMBRERAZONSOCIAL";
    public static final String CT_NOMBRERAZONSOCIAL = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOMBRERAZONSOCIAL = 2;

    public static final String CN_TIPODOCUMENTO = "TIPODOCUMENTO";
    public static final String CT_TIPODOCUMENTO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_TIPODOCUMENTO = 3;

    public static final String CN_DOCUMENTOIDENTIDAD = "DOCUMENTOIDENTIDAD";
    public static final String CT_DOCUMENTOIDENTIDAD = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_DOCUMENTOIDENTIDAD = 4;

    public static final String CN_TELEFONOCELULAR = "TELEFONOCELULAR";
    public static final String CT_TELEFONOCELULAR = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_TELEFONOCELULAR = 5;

    public static final String CN_TELEFONOFIJO = "TELEFONOFIJO";
    public static final String CT_TELEFONOFIJO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_TELEFONOFIJO = 6;

    public static final String CN_CORREOELECTRONICO = "CORREOELECTRONICO";
    public static final String CT_CORREOELECTRONICO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_CORREOELECTRONICO = 7;

    public static final String CN_FECHAPROMOCION = "FECHAPROMOCION";
    public static final String CT_FECHAPROMOCION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAPROMOCION = 8;

    public static final String CN_NOMBREGU = "NOMBREGU";
    public static final String CT_NOMBREGU = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOMBREGU = 9;

    public static final String CN_CORREOELECTRONICOGU = "CORREOELECTRONICOGU";
    public static final String CT_CORREOELECTRONICOGU = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_CORREOELECTRONICOGU = 10;

    public static final String CN_NOMBREGA = "NOMBREGA";
    public static final String CT_NOMBREGA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOMBREGA = 11;

    public static final String CN_CORREOELECTRONICOGA = "CORREOELECTRONICOGA";
    public static final String CT_CORREOELECTRONICOGA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_CORREOELECTRONICOGA = 12;

    public static final String CN_DESCRIPCIONAGENCIA = "DESCRIPCIONAGENCIA";
    public static final String CT_DESCRIPCIONAGENCIA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_DESCRIPCIONAGENCIA = 13;

    public static final String CN_DESCRIPCIONOFICINA = "DESCRIPCIONOFICINA";
    public static final String CT_DESCRIPCIONOFICINA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_DESCRIPCIONOFICINA = 14;

    public static final String CN_DESCRIPCIONCATEGORIA = "DESCRIPCIONCATEGORIA";
    public static final String CT_DESCRIPCIONCATEGORIA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_DESCRIPCIONCATEGORIA = 15;

    public static final String CN_CODIGODEPARTAMENTO = "CODIGODEPARTAMENTO";
    public static final String CT_CODIGODEPARTAMENTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGODEPARTAMENTO = 16;

    public static final String CN_LOGIN = "LOGIN";
    public static final String CT_LOGIN = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_LOGIN = 17;

    public static final String CN_TOKEN = "TOKEN";
    public static final String CT_TOKEN = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_TOKEN = 18;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_INTERMEDIARIO, new OrganizateEntityFactory())
            .addColumn(CN_CODIGOINTERMEDIARIO, CT_CODIGOINTERMEDIARIO, CO_CODIGOINTERMEDIARIO)
            .addColumn(CN_NOMBRERAZONSOCIAL, CT_NOMBRERAZONSOCIAL, CO_NOMBRERAZONSOCIAL)
            .addColumn(CN_TIPODOCUMENTO, CT_TIPODOCUMENTO, CO_TIPODOCUMENTO)
            .addColumn(CN_DOCUMENTOIDENTIDAD, CT_DOCUMENTOIDENTIDAD, CO_DOCUMENTOIDENTIDAD)
            .addColumn(CN_TELEFONOCELULAR, CT_TELEFONOCELULAR, CO_TELEFONOCELULAR)
            .addColumn(CN_TELEFONOFIJO, CT_TELEFONOFIJO, CO_TELEFONOFIJO)
            .addColumn(CN_CORREOELECTRONICO, CT_CORREOELECTRONICO, CO_CORREOELECTRONICO)
            .addColumn(CN_FECHAPROMOCION, CT_FECHAPROMOCION, CO_FECHAPROMOCION)
            .addColumn(CN_NOMBREGU, CT_NOMBREGU, CO_NOMBREGU)
            .addColumn(CN_CORREOELECTRONICOGU, CT_CORREOELECTRONICOGU, CO_CORREOELECTRONICOGU)
            .addColumn(CN_NOMBREGA, CT_NOMBREGA, CO_NOMBREGA)
            .addColumn(CN_CORREOELECTRONICOGA, CT_CORREOELECTRONICOGA, CO_CORREOELECTRONICOGA)
            .addColumn(CN_DESCRIPCIONAGENCIA, CT_DESCRIPCIONAGENCIA, CO_DESCRIPCIONAGENCIA)
            .addColumn(CN_DESCRIPCIONOFICINA, CT_DESCRIPCIONOFICINA, CO_DESCRIPCIONOFICINA)
            .addColumn(CN_DESCRIPCIONCATEGORIA, CT_DESCRIPCIONCATEGORIA, CO_DESCRIPCIONCATEGORIA)
            .addColumn(CN_CODIGODEPARTAMENTO, CT_CODIGODEPARTAMENTO, CO_CODIGODEPARTAMENTO)
            .addColumn(CN_LOGIN, CT_LOGIN, CO_LOGIN)
            .addColumn(CN_TOKEN, CT_TOKEN, CO_TOKEN);

}