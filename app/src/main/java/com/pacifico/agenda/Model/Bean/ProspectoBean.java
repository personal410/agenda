package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

import java.io.Serializable;

/**
 * Created by Joel on 31/05/2016.
 */
public class ProspectoBean extends Entity  implements Serializable{
    private int IdProspecto;
    private int IdProspectoDispositivo;
    private int IdProspectoExterno;
    private String Nombres;
    private String ApellidoPaterno;
    private String ApellidoMaterno;
    private int CodigoSexo;
    private int CodigoEstadoCivil;
    private String FechaNacimiento;
    private int CodigoRangoEdad;
    private int CodigoRangoIngreso;
    private int FlagHijo;
    private int FlagConyuge;
    private int CodigoTipoDocumento;
    private String NumeroDocumento;
    private String TelefonoCelular;
    private String TelefonoFijo;
    private String TelefonoAdicional;
    private String CorreoElectronico1;
    private String CorreoElectronico2;
    private int CodigoNacionalidad;
    private int CondicionFumador;
    private String Empresa;
    private String Cargo;
    private String Nota;
    private int FlagEnviarADNDigital;
    private int CodigoEtapa;
    private int CodigoEstado;
    private int CodigoFuente;
    private int CodigoEntorno;
    private int IdReferenciador;
    private String FechaCreacionDispositivo;
    private String FechaModificacionDispositivo;
    private String AdicionalTexto1;
    private double AdicionalNumerico1;
    private String AdicionalTexto2;
    private double AdicionalNumerico2;
    private int IdReferenciadorDispositivo;
    private int FlagEnviado;

    // TODO: Inicializacion de Prospecto

    public ProspectoBean(){
        IdProspecto = -1;
        IdProspectoDispositivo = -1;
        IdProspectoExterno = -1;
        Nombres = null;
        ApellidoPaterno = null;
        ApellidoMaterno = null;
        CodigoSexo = -1;
        CodigoEstadoCivil = -1;
        FechaNacimiento = null;
        CodigoRangoEdad = -1;
        CodigoRangoIngreso = -1;
        FlagHijo = -1;
        FlagConyuge = -1;
        CodigoTipoDocumento = -1;
        NumeroDocumento = null;
        TelefonoCelular = null;
        TelefonoFijo = null;
        TelefonoAdicional = null;
        CorreoElectronico1 = null;
        CorreoElectronico2 = null;
        CodigoNacionalidad = -1;
        CondicionFumador = -1;
        Empresa = null;
        Cargo = null;
        Nota = null;
        CodigoEtapa = -1;
        CodigoEstado = -1;
        CodigoFuente = -1;
        CodigoEntorno = -1;
        IdReferenciador = -1;
        FechaCreacionDispositivo = null;
        FechaModificacionDispositivo = null;
        AdicionalTexto1 = null;
        AdicionalNumerico1 = -1;
        AdicionalTexto2 = null;
        AdicionalNumerico2 = -1;
        IdReferenciadorDispositivo = -1;
        FlagEnviado = 0;
    }

    public int getIdProspecto() {
        return IdProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        IdProspecto = idProspecto;
    }

    public int getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public int getIdProspectoExterno() {
        return IdProspectoExterno;
    }

    public void setIdProspectoExterno(int idProspectoExterno) {
        IdProspectoExterno = idProspectoExterno;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public int getCodigoSexo() {
        return CodigoSexo;
    }

    public void setCodigoSexo(int codigoSexo) {
        CodigoSexo = codigoSexo;
    }

    public int getCodigoEstadoCivil() {
        return CodigoEstadoCivil;
    }

    public void setCodigoEstadoCivil(int codigoEstadoCivil) {
        CodigoEstadoCivil = codigoEstadoCivil;
    }

    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        FechaNacimiento = fechaNacimiento;
    }

    public int getCodigoRangoEdad() {
        return CodigoRangoEdad;
    }

    public void setCodigoRangoEdad(int codigoRangoEdad) {
        CodigoRangoEdad = codigoRangoEdad;
    }

    public int getCodigoRangoIngreso() {
        return CodigoRangoIngreso;
    }

    public void setCodigoRangoIngreso(int codigoRangoIngreso) {
        CodigoRangoIngreso = codigoRangoIngreso;
    }

    public int getFlagHijo() {
        return FlagHijo;
    }

    public void setFlagHijo(int flagHijo) {
        FlagHijo = flagHijo;
    }

    public int getFlagConyuge() {
        return FlagConyuge;
    }

    public void setFlagConyuge(int flagConyuge) {
        FlagConyuge = flagConyuge;
    }

    public int getCodigoTipoDocumento() {
        return CodigoTipoDocumento;
    }

    public void setCodigoTipoDocumento(int codigoTipoDocumento) {
        CodigoTipoDocumento = codigoTipoDocumento;
    }

    public String getNumeroDocumento() {
        return NumeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        NumeroDocumento = numeroDocumento;
    }

    public String getTelefonoCelular() {
        return TelefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        TelefonoCelular = telefonoCelular;
    }

    public String getTelefonoFijo() {
        return TelefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        TelefonoFijo = telefonoFijo;
    }

    public String getTelefonoAdicional() {
        return TelefonoAdicional;
    }

    public void setTelefonoAdicional(String telefonoAdicional) {
        TelefonoAdicional = telefonoAdicional;
    }

    public String getCorreoElectronico1() {
        return CorreoElectronico1;
    }

    public void setCorreoElectronico1(String correoElectronico1) {
        CorreoElectronico1 = correoElectronico1;
    }

    public String getCorreoElectronico2() {
        return CorreoElectronico2;
    }

    public void setCorreoElectronico2(String correoElectronico2) {
        CorreoElectronico2 = correoElectronico2;
    }

    public int getCodigoNacionalidad() {
        return CodigoNacionalidad;
    }

    public void setCodigoNacionalidad(int codigoNacionalidad) {
        CodigoNacionalidad = codigoNacionalidad;
    }

    public int getCondicionFumador() {
        return CondicionFumador;
    }

    public void setCondicionFumador(int condicionFumador) {
        CondicionFumador = condicionFumador;
    }

    public String getEmpresa() {
        return Empresa;
    }

    public void setEmpresa(String empresa) {
        Empresa = empresa;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String cargo) {
        Cargo = cargo;
    }

    public String getNota() {
        return Nota;
    }

    public void setNota(String nota) {
        Nota = nota;
    }

    public int getFlagEnviarADNDigital() {
        return FlagEnviarADNDigital;
    }

    public void setFlagEnviarADNDigital(int flagEnviarADNDigital) {
        FlagEnviarADNDigital = flagEnviarADNDigital;
    }

    public int getCodigoEtapa() {
        return CodigoEtapa;
    }

    public void setCodigoEtapa(int codigoEtapa) {
        CodigoEtapa = codigoEtapa;
    }

    public int getCodigoEstado() {
        return CodigoEstado;
    }

    public void setCodigoEstado(int codigoEstado) {
        CodigoEstado = codigoEstado;
    }

    public int getCodigoFuente() {
        return CodigoFuente;
    }

    public void setCodigoFuente(int codigoFuente) {
        CodigoFuente = codigoFuente;
    }

    public int getCodigoEntorno() {
        return CodigoEntorno;
    }

    public void setCodigoEntorno(int codigoEntorno) {
        CodigoEntorno = codigoEntorno;
    }

    public int getIdReferenciador() {
        return IdReferenciador;
    }

    public void setIdReferenciador(int idReferenciador) {
        IdReferenciador = idReferenciador;
    }

    public String getFechaCreacionDispositivo() {
        return FechaCreacionDispositivo;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return FechaModificacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public int getIdReferenciadorDispositivo() {
        return IdReferenciadorDispositivo;
    }

    public void setIdReferenciadorDispositivo(int idReferenciadorDispositivo) {
        IdReferenciadorDispositivo = idReferenciadorDispositivo;
    }

    public String getAdicionalTexto1() {
        return AdicionalTexto1;
    }

    public void setAdicionalTexto1(String adicionalTexto1) {
        AdicionalTexto1 = adicionalTexto1;
    }

    public double getAdicionalNumerico1() {
        return AdicionalNumerico1;
    }

    public void setAdicionalNumerico1(double adicionalNumerico1) {
        AdicionalNumerico1 = adicionalNumerico1;
    }

    public String getAdicionalTexto2() {
        return AdicionalTexto2;
    }

    public void setAdicionalTexto2(String adicionalTexto2) {
        AdicionalTexto2 = adicionalTexto2;
    }

    public double getAdicionalNumerico2() {
        return AdicionalNumerico2;
    }

    public void setAdicionalNumerico2(double adicionalNumerico2) {
        AdicionalNumerico2 = adicionalNumerico2;
    }

    public int getFlagEnviado() {
        return FlagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        FlagEnviado = flagEnviado;
    }
    /*public String getNombreCompleto(){
        return String.format("%s %s %s", ApellidoPaterno, ApellidoMaterno, Nombres);
    }*/
    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDPROSPECTO:
                return IdProspecto;
            case CO_IDPROSPECTODISPOSITIVO:
                return IdProspectoDispositivo;
            case CO_IDPROSPECTOEXTERNO:
                return IdProspectoExterno;
            case CO_NOMBRES:
                return Nombres;
            case CO_APELLIDOPATERNO:
                return ApellidoPaterno;
            case CO_APELLIDOMATERNO:
                return ApellidoMaterno;
            case CO_CODIGOSEXO:
                return CodigoSexo;
            case CO_CODIGOESTADOCIVIL:
                return CodigoEstadoCivil;
            case CO_FECHANACIMIENTO:
                return FechaNacimiento;
            case CO_CODIGORANGOEDAD:
                return CodigoRangoEdad;
            case CO_CODIGORANGOINGRESO:
                return CodigoRangoIngreso;
            case CO_FLAGHIJO:
                return FlagHijo;
            case CO_FLAGCONYUGE:
                return FlagConyuge;
            case CO_CODIGOTIPODOCUMENTO:
                return CodigoTipoDocumento;
            case CO_NUMERODOCUMENTO:
                return NumeroDocumento;
            case CO_TELEFONOCELULAR:
                return TelefonoCelular;
            case CO_TELEFONOFIJO:
                return TelefonoFijo;
            case CO_TELEFONOADICIONAL:
                return TelefonoAdicional;
            case CO_CORREOELECTRONICO1:
                return CorreoElectronico1;
            case CO_CORREOELECTRONICO2:
                return CorreoElectronico2;
            case CO_CODIGONACIONALIDAD:
                return CodigoNacionalidad;
            case CO_CONDICIONFUMADOR:
                return CondicionFumador;
            case CO_EMPRESA:
                return Empresa;
            case CO_CARGO:
                return Cargo;
            case CO_NOTA:
                return Nota;
            case CO_FLAGENVIARADNDIGITAL:
                return FlagEnviarADNDigital;
            case CO_CODIGOETAPA:
                return CodigoEtapa;
            case CO_CODIGOESTADO:
                return CodigoEstado;
            case CO_CODIGOFUENTE:
                return CodigoFuente;
            case CO_CODIGOENTORNO:
                return CodigoEntorno;
            case CO_IDREFERENCIADOR:
                return IdReferenciador;
            case CO_FECHACREACIONDISPOSITIVO:
                return FechaCreacionDispositivo;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                return FechaModificacionDispositivo;
            case CO_ADICIONALTEXTO1:
                return AdicionalTexto1;
            case CO_ADICIONALNUMERICO1:
                return AdicionalNumerico1;
            case CO_ADICIONALTEXTO2:
                return AdicionalTexto2;
            case CO_ADICIONALNUMERICO2:
                return AdicionalNumerico2;
            case CO_IDREFERENCIADORDISPOSITIVO:
                return IdReferenciadorDispositivo;
            case CO_FLAGENVIADO:
                return FlagEnviado;
        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDPROSPECTO:
                IdProspecto = (int) object;
                break;
            case CO_IDPROSPECTODISPOSITIVO:
                IdProspectoDispositivo = (int) object;
                break;
            case CO_IDPROSPECTOEXTERNO:
                IdProspectoExterno = (int) object;
                break;
            case CO_NOMBRES:
                Nombres = (String) object;
                break;
            case CO_APELLIDOPATERNO:
                ApellidoPaterno = (String) object;
                break;
            case CO_APELLIDOMATERNO:
                ApellidoMaterno = (String) object;
                break;
            case CO_CODIGOSEXO:
                CodigoSexo = (int) object;
                break;
            case CO_CODIGOESTADOCIVIL:
                CodigoEstadoCivil = (int) object;
                break;
            case CO_FECHANACIMIENTO:
                FechaNacimiento = (String) object;
                break;
            case CO_CODIGORANGOEDAD:
                CodigoRangoEdad = (int) object;
                break;
            case CO_CODIGORANGOINGRESO:
                CodigoRangoIngreso = (int) object;
                break;
            case CO_FLAGHIJO:
                FlagHijo = (int) object;
                break;
            case CO_FLAGCONYUGE:
                FlagConyuge = (int) object;
                break;
            case CO_CODIGOTIPODOCUMENTO:
                CodigoTipoDocumento = (int) object;
                break;
            case CO_NUMERODOCUMENTO:
                NumeroDocumento = (String) object;
                break;
            case CO_TELEFONOCELULAR:
                TelefonoCelular = (String) object;
                break;
            case CO_TELEFONOFIJO:
                TelefonoFijo = (String) object;
                break;
            case CO_TELEFONOADICIONAL:
                TelefonoAdicional = (String) object;
                break;
            case CO_CORREOELECTRONICO1:
                CorreoElectronico1 = (String) object;
                break;
            case CO_CORREOELECTRONICO2:
                CorreoElectronico2 = (String) object;
                break;
            case CO_CODIGONACIONALIDAD:
                CodigoNacionalidad = (int) object;
                break;
            case CO_CONDICIONFUMADOR:
                CondicionFumador = (int) object;
                break;
            case CO_EMPRESA:
                Empresa = (String) object;
                break;
            case CO_CARGO:
                Cargo = (String) object;
                break;
            case CO_NOTA:
                Nota = (String) object;
                break;
            case CO_FLAGENVIARADNDIGITAL:
                FlagEnviarADNDigital = (int) object;
                break;
            case CO_CODIGOETAPA:
                CodigoEtapa = (int) object;
                break;
            case CO_CODIGOESTADO:
                CodigoEstado = (int) object;
                break;
            case CO_CODIGOFUENTE:
                CodigoFuente = (int) object;
                break;
            case CO_CODIGOENTORNO:
                CodigoEntorno = (int) object;
                break;
            case CO_IDREFERENCIADOR:
                IdReferenciador = (int) object;
                break;
            case CO_FECHACREACIONDISPOSITIVO:
                FechaCreacionDispositivo = (String) object;
                break;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                FechaModificacionDispositivo = (String) object;
                break;
            case CO_ADICIONALTEXTO1:
                AdicionalTexto1 = (String) object;
                break;
            case CO_ADICIONALNUMERICO1:
                AdicionalNumerico1 = (double) object;
                break;
            case CO_ADICIONALTEXTO2:
                AdicionalTexto2 = (String) object;
                break;
            case CO_ADICIONALNUMERICO2:
                AdicionalNumerico2 = (double) object;
                break;
            case CO_IDREFERENCIADORDISPOSITIVO:
                IdReferenciadorDispositivo = (int) object;
                break;
            case CO_FLAGENVIADO:
                FlagEnviado = (int) object;
                break;
        }
    }

    public static final String CN_IDPROSPECTO = "IDPROSPECTO";
    public static final String CT_IDPROSPECTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTO = 1;

    public static final String CN_IDPROSPECTODISPOSITIVO = "IDPROSPECTODISPOSITIVO";
    public static final String CT_IDPROSPECTODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTODISPOSITIVO = 2;

    public static final String CN_IDPROSPECTOEXTERNO = "IDPROSPECTOEXTERNO";
    public static final String CT_IDPROSPECTOEXTERNO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTOEXTERNO = 3;

    public static final String CN_NOMBRES = "NOMBRES";
    public static final String CT_NOMBRES = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOMBRES = 4;

    public static final String CN_APELLIDOPATERNO = "APELLIDOPATERNO";
    public static final String CT_APELLIDOPATERNO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_APELLIDOPATERNO = 5;

    public static final String CN_APELLIDOMATERNO = "APELLIDOMATERNO";
    public static final String CT_APELLIDOMATERNO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_APELLIDOMATERNO = 6;

    public static final String CN_CODIGOSEXO = "CODIGOSEXO";
    public static final String CT_CODIGOSEXO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOSEXO = 7;

    public static final String CN_CODIGOESTADOCIVIL = "CODIGOESTADOCIVIL";
    public static final String CT_CODIGOESTADOCIVIL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOESTADOCIVIL = 8;

    public static final String CN_FECHANACIMIENTO = "FECHANACIMIENTO";
    public static final String CT_FECHANACIMIENTO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHANACIMIENTO = 9;

    public static final String CN_CODIGORANGOEDAD = "CODIGORANGOEDAD";
    public static final String CT_CODIGORANGOEDAD = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGORANGOEDAD = 10;

    public static final String CN_CODIGORANGOINGRESO = "CODIGORANGOINGRESO";
    public static final String CT_CODIGORANGOINGRESO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGORANGOINGRESO = 11;

    public static final String CN_FLAGHIJO = "FLAGHIJO";
    public static final String CT_FLAGHIJO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGHIJO = 12;

    public static final String CN_FLAGCONYUGE = "FLAGCONYUGE";
    public static final String CT_FLAGCONYUGE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGCONYUGE = 13;

    public static final String CN_CODIGOTIPODOCUMENTO = "CODIGOTIPODOCUMENTO";
    public static final String CT_CODIGOTIPODOCUMENTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOTIPODOCUMENTO = 14;

    public static final String CN_NUMERODOCUMENTO = "NUMERODOCUMENTO";
    public static final String CT_NUMERODOCUMENTO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NUMERODOCUMENTO = 15;

    public static final String CN_TELEFONOCELULAR = "TELEFONOCELULAR";
    public static final String CT_TELEFONOCELULAR = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_TELEFONOCELULAR = 16;

    public static final String CN_TELEFONOFIJO = "TELEFONOFIJO";
    public static final String CT_TELEFONOFIJO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_TELEFONOFIJO = 17;

    public static final String CN_TELEFONOADICIONAL = "TELEFONOADICIONAL";
    public static final String CT_TELEFONOADICIONAL = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_TELEFONOADICIONAL = 18;

    public static final String CN_CORREOELECTRONICO1 = "CORREOELECTRONICO1";
    public static final String CT_CORREOELECTRONICO1 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_CORREOELECTRONICO1 = 19;

    public static final String CN_CORREOELECTRONICO2 = "CORREOELECTRONICO2";
    public static final String CT_CORREOELECTRONICO2 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_CORREOELECTRONICO2 = 20;

    public static final String CN_CODIGONACIONALIDAD = "CODIGONACIONALIDAD";
    public static final String CT_CODIGONACIONALIDAD = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGONACIONALIDAD = 21;

    public static final String CN_CONDICIONFUMADOR = "CONDICIONFUMADOR";
    public static final String CT_CONDICIONFUMADOR = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CONDICIONFUMADOR = 22;

    public static final String CN_EMPRESA = "EMPRESA";
    public static final String CT_EMPRESA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_EMPRESA = 23;

    public static final String CN_CARGO = "CARGO";
    public static final String CT_CARGO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_CARGO = 24;

    public static final String CN_NOTA = "NOTA";
    public static final String CT_NOTA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOTA = 25;

    public static final String CN_FLAGENVIARADNDIGITAL = "FLAGENVIARADNDIGITAL";
    public static final String CT_FLAGENVIARADNDIGITAL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIARADNDIGITAL = 26;

    public static final String CN_CODIGOETAPA = "CODIGOETAPA";
    public static final String CT_CODIGOETAPA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOETAPA = 27;

    public static final String CN_CODIGOESTADO = "CODIGOESTADO";
    public static final String CT_CODIGOESTADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOESTADO = 28;

    public static final String CN_CODIGOFUENTE = "CODIGOFUENTE";
    public static final String CT_CODIGOFUENTE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOFUENTE = 29;

    public static final String CN_CODIGOENTORNO = "CODIGOENTORNO";
    public static final String CT_CODIGOENTORNO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOENTORNO = 30;

    public static final String CN_IDREFERENCIADOR = "IDREFERENCIADOR";
    public static final String CT_IDREFERENCIADOR = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDREFERENCIADOR = 31;

    public static final String CN_FECHACREACIONDISPOSITIVO = "FECHACREACIONDISPOSITIVO";
    public static final String CT_FECHACREACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACREACIONDISPOSITIVO = 32;

    public static final String CN_FECHAMODIFICACIONDISPOSITIVO = "FECHAMODIFICACIONDISPOSITIVO";
    public static final String CT_FECHAMODIFICACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAMODIFICACIONDISPOSITIVO = 33;

    public static final String CN_ADICIONALTEXTO1 = "ADICIONALTEXTO1";
    public static final String CT_ADICIONALTEXTO1 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_ADICIONALTEXTO1 = 34;

    public static final String CN_ADICIONALNUMERICO1 = "ADICIONALNUMERICO1";
    public static final String CT_ADICIONALNUMERICO1 = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_ADICIONALNUMERICO1 = 35;

    public static final String CN_ADICIONALTEXTO2 = "ADICIONALTEXTO2";
    public static final String CT_ADICIONALTEXTO2 = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_ADICIONALTEXTO2 = 36;

    public static final String CN_ADICIONALNUMERICO2 = "ADICIONALNUMERICO2";
    public static final String CT_ADICIONALNUMERICO2 = TableHelper.DECIMAL_DATATYPE_NAME;
    public static final int CO_ADICIONALNUMERICO2 = 37;

    public static final String CN_IDREFERENCIADORDISPOSITIVO = "IDREFERENCIADORDISPOSITIVO";
    public static final String CT_IDREFERENCIADORDISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDREFERENCIADORDISPOSITIVO = 38;

    public static final String CN_FLAGENVIADO = "FLAGENVIADO";
    public static final String CT_FLAGENVIADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIADO = 39;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_PROSPECTO, new OrganizateEntityFactory())
            .addColumn(CN_IDPROSPECTO, CT_IDPROSPECTO, CO_IDPROSPECTO)
            .addColumn(CN_IDPROSPECTODISPOSITIVO, CT_IDPROSPECTODISPOSITIVO, CO_IDPROSPECTODISPOSITIVO)
            .addColumn(CN_IDPROSPECTOEXTERNO, CT_IDPROSPECTOEXTERNO, CO_IDPROSPECTOEXTERNO)
            .addColumn(CN_NOMBRES, CT_NOMBRES, CO_NOMBRES)
            .addColumn(CN_APELLIDOPATERNO, CT_APELLIDOPATERNO, CO_APELLIDOPATERNO)
            .addColumn(CN_APELLIDOMATERNO, CT_APELLIDOMATERNO, CO_APELLIDOMATERNO)
            .addColumn(CN_CODIGOSEXO, CT_CODIGOSEXO, CO_CODIGOSEXO)
            .addColumn(CN_CODIGOESTADOCIVIL, CT_CODIGOESTADOCIVIL, CO_CODIGOESTADOCIVIL)
            .addColumn(CN_FECHANACIMIENTO, CT_FECHANACIMIENTO, CO_FECHANACIMIENTO)
            .addColumn(CN_CODIGORANGOEDAD, CT_CODIGORANGOEDAD, CO_CODIGORANGOEDAD)
            .addColumn(CN_CODIGORANGOINGRESO, CT_CODIGORANGOINGRESO, CO_CODIGORANGOINGRESO)
            .addColumn(CN_FLAGHIJO, CT_FLAGHIJO, CO_FLAGHIJO)
            .addColumn(CN_FLAGCONYUGE, CT_FLAGCONYUGE, CO_FLAGCONYUGE)
            .addColumn(CN_CODIGOTIPODOCUMENTO, CT_CODIGOTIPODOCUMENTO, CO_CODIGOTIPODOCUMENTO)
            .addColumn(CN_NUMERODOCUMENTO, CT_NUMERODOCUMENTO, CO_NUMERODOCUMENTO)
            .addColumn(CN_TELEFONOCELULAR, CT_TELEFONOCELULAR, CO_TELEFONOCELULAR)
            .addColumn(CN_TELEFONOFIJO, CT_TELEFONOFIJO, CO_TELEFONOFIJO)
            .addColumn(CN_TELEFONOADICIONAL, CT_TELEFONOADICIONAL, CO_TELEFONOADICIONAL)
            .addColumn(CN_CORREOELECTRONICO1, CT_CORREOELECTRONICO1, CO_CORREOELECTRONICO1)
            .addColumn(CN_CORREOELECTRONICO2, CT_CORREOELECTRONICO2, CO_CORREOELECTRONICO2)
            .addColumn(CN_CODIGONACIONALIDAD, CT_CODIGONACIONALIDAD, CO_CODIGONACIONALIDAD)
            .addColumn(CN_CONDICIONFUMADOR, CT_CONDICIONFUMADOR, CO_CONDICIONFUMADOR)
            .addColumn(CN_EMPRESA, CT_EMPRESA, CO_EMPRESA)
            .addColumn(CN_CARGO, CT_CARGO, CO_CARGO)
            .addColumn(CN_NOTA, CT_NOTA, CO_NOTA)
            .addColumn(CN_FLAGENVIARADNDIGITAL, CT_FLAGENVIARADNDIGITAL, CO_FLAGENVIARADNDIGITAL)
            .addColumn(CN_CODIGOETAPA, CT_CODIGOETAPA, CO_CODIGOETAPA)
            .addColumn(CN_CODIGOESTADO, CT_CODIGOESTADO, CO_CODIGOESTADO)
            .addColumn(CN_CODIGOFUENTE, CT_CODIGOFUENTE, CO_CODIGOFUENTE)
            .addColumn(CN_CODIGOENTORNO, CT_CODIGOENTORNO, CO_CODIGOENTORNO)
            .addColumn(CN_IDREFERENCIADOR, CT_IDREFERENCIADOR, CO_IDREFERENCIADOR)
            .addColumn(CN_FECHACREACIONDISPOSITIVO, CT_FECHACREACIONDISPOSITIVO, CO_FECHACREACIONDISPOSITIVO)
            .addColumn(CN_FECHAMODIFICACIONDISPOSITIVO, CT_FECHAMODIFICACIONDISPOSITIVO, CO_FECHAMODIFICACIONDISPOSITIVO)
            .addColumn(CN_ADICIONALTEXTO1, CT_ADICIONALTEXTO1, CO_ADICIONALTEXTO1)
            .addColumn(CN_ADICIONALNUMERICO1, CT_ADICIONALNUMERICO1, CO_ADICIONALNUMERICO1)
            .addColumn(CN_ADICIONALTEXTO2, CT_ADICIONALTEXTO2, CO_ADICIONALTEXTO2)
            .addColumn(CN_ADICIONALNUMERICO2, CT_ADICIONALNUMERICO2, CO_ADICIONALNUMERICO2)
            .addColumn(CN_IDREFERENCIADORDISPOSITIVO, CT_IDREFERENCIADORDISPOSITIVO, CO_IDREFERENCIADORDISPOSITIVO)
            .addColumn(CN_FLAGENVIADO, CT_FLAGENVIADO, CO_FLAGENVIADO);
}