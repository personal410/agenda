package com.pacifico.agenda.Model.Bean;

public class CitaProspectoBean {

    private int idCitaDispositivo = -1;
    private int idCita = -1;
    private String fechaCita = "";
    private String horaIncioCita = "";
    private String horaFinCita = "";
    private int idProspecto = -1;
    private int idProspectoDispositivo = -1;
    private int codigoEtapaProspecto = -1;
    private String nombres = "";
    private String apellidoPaterno = "";
    private String apellidoMaterno = "";
    private int idReferenciador = -1;
    //private Strin


    public CitaProspectoBean()
    {

    }

    public int getIdCita() {
        return idCita;
    }

    public void setIdCita(int idCita) {
        this.idCita = idCita;
    }

    public int getIProspecto() {
        return idProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        this.idProspecto = idProspecto;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public int getIdReferenciador() {
        return idReferenciador;
    }

    public void setIdReferenciador(int idReferenciador) {
        this.idReferenciador = idReferenciador;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getHoraIncioCita() {
        return horaIncioCita;
    }

    public void setHoraIncioCita(String horaIncioCita) {
        this.horaIncioCita = horaIncioCita;
    }

    public String getHoraFinCita() {
        return horaFinCita;
    }

    public void setHoraFinCita(String horaFinCita) {
        this.horaFinCita = horaFinCita;
    }

    public int getIdCitaDispositivo() {
        return idCitaDispositivo;
    }

    public void setIdCitaDispositivo(int idCitaDispositivo) {
        this.idCitaDispositivo = idCitaDispositivo;
    }

    public int getIdProspecto() {
        return idProspecto;
    }

    public int getCodigoEtapaProspecto() {
        return codigoEtapaProspecto;
    }

    public void setCodigoEtapaProspecto(int codigoEtapaProspecto) {
        this.codigoEtapaProspecto = codigoEtapaProspecto;
    }

    public int getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        this.idProspectoDispositivo = idProspectoDispositivo;
    }
}
