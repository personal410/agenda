package com.pacifico.agenda.Model.Controller;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Model.Bean.AdnBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;

import java.util.ArrayList;

/**
 * Created by vctrls3477 on 11/06/16.
 */
public class ADNController {
    public static void guardarAdn(AdnBean adnBean){
        AdnBean.tableHelper.insertEntity(adnBean);
    }
    public static void actualizarADN(AdnBean adnBean){
        String[] parametros = new String[1];
        String sentencia = "IDPROSPECTODISPOSITIVO = ?";
        parametros[0] = Integer.toString(adnBean.getIdProspectoDispositivo());
        AdnBean.tableHelper.updateEntity(adnBean, sentencia, parametros);
    }
    public static AdnBean getAdnPorIdProspecto(int idProspecto, int idProspectoDispositivo){
        String[] parametros = new String[1];
        String sentencia;
        if(idProspecto == -1){
            parametros[0] = Integer.toString(idProspectoDispositivo);
            sentencia = "IDPROSPECTODISPOSITIVO = ?";
        }else{
            parametros[0] = Integer.toString(idProspecto);
            sentencia = "IDPROSPECTO = ?";
        }
        ArrayList<Entity> arrBeans = AdnBean.tableHelper.getEntities(sentencia, parametros);
        if(arrBeans.size() > 0){
            return (AdnBean)arrBeans.get(0);
        }else{
            return null;
        }
    }
    public static void guardarListaADNs(ArrayList<AdnBean> listaADNBean) {
        if(listaADNBean != null){
            for(AdnBean adnBean : listaADNBean){
                if(adnBean.getIdProspectoDispositivo() == -1){
                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(adnBean.getIdProspecto());
                    adnBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                }
                guardarAdn(adnBean);
            }
        }
    }
    public static ArrayList<AdnBean> obtenerADNSinEviar(){
        ArrayList<Entity> arrBeans = AdnBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<AdnBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((AdnBean)entity);
        }
        return arrBeanFinal;
    }
    public static boolean hayAlgunADN(){
        ArrayList<Entity> arrBeans = AdnBean.tableHelper.getEntities("", null);
        return arrBeans.size() > 0;
    }
    public static void limpiarTablaADN(){
        AdnBean.tableHelper.deleteAllEntities();
    }
}