package com.pacifico.agenda.Model.Bean;

/**
 * Created by joel on 8/11/16.
 */
public class ProspectoDatosReferidoBean {

    private int IdProspecto;
    private int IdProspectoDispositivo;
    private String Nombres;
    private String ApellidoPaterno;
    private String ApellidoMaterno;
    private int IdReferenciador;
    private int IdReferenciadorDispositivo;
    private String NombresReferenciador;
    private String ApellidoPaternoReferenciador;
    private String ApellidoMaternoReferenciador;

    // Agregado para prospecto lista de prospectos
    public int EtapaProspecto;
    public int EstadoProspecto;
    public int EstadoUltCita;
    public int ResultadoUltCita;

    // Agregado para filtro referente
    public int cantReferidos;

    private String nombreFuente;

    public ProspectoDatosReferidoBean()
    {
        IdProspecto = -1;
        IdProspectoDispositivo = -1;
        IdReferenciador = -1;
        IdReferenciadorDispositivo = -1;
        EtapaProspecto = -1;
        EstadoProspecto = -1;
        EstadoUltCita = -1;
        ResultadoUltCita = -1;
        cantReferidos = -1;
    }

    public int getIdProspecto() {
        return IdProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        IdProspecto = idProspecto;
    }

    public int getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public int getIdReferenciador() {
        return IdReferenciador;
    }

    public void setIdReferenciador(int idReferenciador) {
        IdReferenciador = idReferenciador;
    }

    public int getIdReferenciadorDispositivo() {
        return IdReferenciadorDispositivo;
    }

    public void setIdReferenciadorDispositivo(int idReferenciadorDispositivo) {
        IdReferenciadorDispositivo = idReferenciadorDispositivo;
    }

    public String getNombresReferenciador() {
        return NombresReferenciador;
    }

    public void setNombresReferenciador(String nombresReferenciador) {
        NombresReferenciador = nombresReferenciador;
    }

    public String getApellidoPaternoReferenciador() {
        return ApellidoPaternoReferenciador;
    }

    public void setApellidoPaternoReferenciador(String apellidoPaternoReferenciador) {
        ApellidoPaternoReferenciador = apellidoPaternoReferenciador;
    }

    public String getApellidoMaternoReferenciador() {
        return ApellidoMaternoReferenciador;
    }

    public void setApellidoMaternoReferenciador(String apellidoMaternoReferenciador) {
        ApellidoMaternoReferenciador = apellidoMaternoReferenciador;
    }

    public int getEtapaProspecto() {
        return EtapaProspecto;
    }

    public void setEtapaProspecto(int etapaProspecto) {
        EtapaProspecto = etapaProspecto;
    }

    public int getEstadoProspecto() {
        return EstadoProspecto;
    }

    public void setEstadoProspecto(int estadoProspecto) {
        EstadoProspecto = estadoProspecto;
    }

    public int getEstadoUltCita() {
        return EstadoUltCita;
    }

    public void setEstadoUltCita(int estadoUltCita) {
        EstadoUltCita = estadoUltCita;
    }

    public int getResultadoUltCita() {
        return ResultadoUltCita;
    }

    public void setResultadoUltCita(int resultadoUltCita) {
        ResultadoUltCita = resultadoUltCita;
    }

    public int getCantReferidos() {
        return cantReferidos;
    }

    public void setCantReferidos(int cantReferidos) {
        this.cantReferidos = cantReferidos;
    }

    public String getNombreFuente() {
        return nombreFuente;
    }

    public void setNombreFuente(String nombreFuente) {
        this.nombreFuente = nombreFuente;
    }
}
