package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class ReferidoBean extends Entity {
    private int IdReferido;
    private int IdProspecto;
    private int IdReferidoDispositivo;
    private int IdProspectoDispositivo;
    private int CodigoTipoReferido;
    private String Nombres;
    private String ApellidoPaterno;
    private String ApellidoMaterno;
    private int CodigoRangoEdad;
    private int FlagHijo;
    private int CodigoRangoIngreso;
    private String Telefono;
    private String FechaCreaContacto;
    private int CodigoTipoEmpleo;
    private int FlagActivo;
    private String FechaCreacionDispositivo;
    private String FechaModificacionDispositivo;
    private int FlagEnviado;
    private int FlagModificado = 0;
    public void inicializarValores(){
        Nombres = "";
        ApellidoPaterno = "";
        CodigoRangoEdad = -1;
        FlagHijo = -1;
        CodigoRangoIngreso = -1;
        CodigoTipoEmpleo = -1;
        FlagActivo = 1;
        FlagEnviado = 0;
    }

    public int getIdReferido() {
        return IdReferido;
    }

    public void setIdReferido(int idReferido) {
        IdReferido = idReferido;
    }

    public int getIdProspecto() {
        return IdProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        IdProspecto = idProspecto;
    }

    public int getIdReferidoDispositivo() {
        return IdReferidoDispositivo;
    }

    public void setIdReferidoDispositivo(int idReferidoDispositivo) {
        IdReferidoDispositivo = idReferidoDispositivo;
    }

    public int getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public int getCodigoTipoReferido() {
        return CodigoTipoReferido;
    }

    public void setCodigoTipoReferido(int codigoTipoReferido) {
        CodigoTipoReferido = codigoTipoReferido;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public int getCodigoRangoEdad() {
        return CodigoRangoEdad;
    }

    public void setCodigoRangoEdad(int codigoRangoEdad) {
        CodigoRangoEdad = codigoRangoEdad;
    }

    public int getFlagHijo() {
        return FlagHijo;
    }

    public void setFlagHijo(int flagHijo) {
        FlagHijo = flagHijo;
    }

    public int getCodigoRangoIngreso() {
        return CodigoRangoIngreso;
    }

    public void setCodigoRangoIngreso(int codigoRangoIngresos) {
        CodigoRangoIngreso = codigoRangoIngresos;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getFechaCreaContacto() {
        return FechaCreaContacto;
    }

    public void setFechaCreaContacto(String fechaCreaContacto) {
        FechaCreaContacto = fechaCreaContacto;
    }

    public int getCodigoTipoEmpleo() {
        return CodigoTipoEmpleo;
    }

    public void setCodigoTipoEmpleo(int codigoTipoEmpleo) {
        CodigoTipoEmpleo = codigoTipoEmpleo;
    }

    public int getFlagActivo() {
        return FlagActivo;
    }

    public void setFlagActivo(int flagActivo) {
        FlagActivo = flagActivo;
    }

    public String getFechaCreacionDispositivo() {
        return FechaCreacionDispositivo;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return FechaModificacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public int getFlagEnviado() {
        return FlagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        FlagEnviado = flagEnviado;
    }

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDREFERIDO:
                return IdReferido;
            case CO_IDPROSPECTO:
                return IdProspecto;
            case CO_IDREFERIDODISPOSITIVO:
                return IdReferidoDispositivo;
            case CO_IDPROSPECTODISPOSITIVO:
                return IdProspectoDispositivo;
            case CO_CODIGOTIPOREFERIDO:
                return CodigoTipoReferido;
            case CO_NOMBRES:
                return Nombres;
            case CO_APELLIDOPATERNO:
                return ApellidoPaterno;
            case CO_APELLIDOMATERNO:
                return ApellidoMaterno;
            case CO_CODIGORANGOEDAD:
                return CodigoRangoEdad;
            case CO_FLAGHIJO:
                return FlagHijo;
            case CO_CODIGORANGOINGRESO:
                return CodigoRangoIngreso;
            case CO_TELEFONO:
                return Telefono;
            case CO_FECHACREACONTACTO:
                return FechaCreaContacto;
            case CO_CODIGOTIPOEMPLEO:
                return CodigoTipoEmpleo;
            case CO_FLAGACTIVO:
                return FlagActivo;
            case CO_FECHACREACIONDISPOSITIVO:
                return FechaCreacionDispositivo;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                return FechaModificacionDispositivo;
            case CO_FLAGENVIADO:
                return FlagEnviado;
        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDREFERIDO:
                IdReferido = (int) object;
                break;
            case CO_IDPROSPECTO:
                IdProspecto = (int) object;
                break;
            case CO_IDREFERIDODISPOSITIVO:
                IdReferidoDispositivo = (int) object;
                break;
            case CO_IDPROSPECTODISPOSITIVO:
                IdProspectoDispositivo = (int)object;
            case CO_CODIGOTIPOREFERIDO:
                CodigoTipoReferido = (int) object;
                break;
            case CO_NOMBRES:
                Nombres = (String) object;
                break;
            case CO_APELLIDOPATERNO:
                ApellidoPaterno = (String) object;
                break;
            case CO_APELLIDOMATERNO:
                ApellidoMaterno = (String) object;
                break;
            case CO_CODIGORANGOEDAD:
                CodigoRangoEdad = (int) object;
                break;
            case CO_FLAGHIJO:
                FlagHijo = (int) object;
                break;
            case CO_CODIGORANGOINGRESO:
                CodigoRangoIngreso = (int) object;
                break;
            case CO_TELEFONO:
                Telefono = (String) object;
                break;
            case CO_FECHACREACONTACTO:
                FechaCreaContacto = (String) object;
                break;
            case CO_CODIGOTIPOEMPLEO:
                CodigoTipoEmpleo = (int) object;
                break;
            case CO_FLAGACTIVO:
                FlagActivo = (int) object;
                break;
            case CO_FECHACREACIONDISPOSITIVO:
                FechaCreacionDispositivo = (String) object;
                break;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                FechaModificacionDispositivo = (String) object;
                break;
            case CO_FLAGENVIADO:
                FlagEnviado = (int) object;
                break;
        }
    }

    public static final String CN_IDREFERIDO = "IDREFERIDO";
    public static final String CT_IDREFERIDO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDREFERIDO = 1;

    public static final String CN_IDPROSPECTO = "IDPROSPECTO";
    public static final String CT_IDPROSPECTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTO = 2;

    public static final String CN_IDREFERIDODISPOSITIVO = "IDREFERIDODISPOSITIVO";
    public static final String CT_IDREFERIDODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDREFERIDODISPOSITIVO = 3;

    public static final String CN_IDPROSPECTODISPOSITIVO = "IDPROSPECTODISPOSITIVO";
    public static final String CT_IDPROSPECTODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDPROSPECTODISPOSITIVO = 4;

    public static final String CN_CODIGOTIPOREFERIDO = "CODIGOTIPOREFERIDO";
    public static final String CT_CODIGOTIPOREFERIDO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOTIPOREFERIDO = 5;

    public static final String CN_NOMBRES = "NOMBRES";
    public static final String CT_NOMBRES = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOMBRES = 6;

    public static final String CN_APELLIDOPATERNO = "APELLIDOPATERNO";
    public static final String CT_APELLIDOPATERNO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_APELLIDOPATERNO = 7;

    public static final String CN_APELLIDOMATERNO = "APELLIDOMATERNO";
    public static final String CT_APELLIDOMATERNO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_APELLIDOMATERNO = 8;

    public static final String CN_CODIGORANGOEDAD = "CODIGORANGOEDAD";
    public static final String CT_CODIGORANGOEDAD = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGORANGOEDAD = 9;

    public static final String CN_FLAGHIJO = "FLAGHIJO";
    public static final String CT_FLAGHIJO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGHIJO = 10;

    public static final String CN_CODIGORANGOINGRESO = "CODIGORANGOINGRESO";
    public static final String CT_CODIGORANGOINGRESO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGORANGOINGRESO = 11;

    public static final String CN_TELEFONO = "TELEFONO";
    public static final String CT_TELEFONO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_TELEFONO = 12;

    public static final String CN_FECHACREACONTACTO = "FECHACREACONTACTO";
    public static final String CT_FECHACREACONTACTO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACREACONTACTO = 13;

    public static final String CN_CODIGOTIPOEMPLEO = "CODIGOTIPOEMPLEO";
    public static final String CT_CODIGOTIPOEMPLEO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOTIPOEMPLEO = 14;

    public static final String CN_FLAGACTIVO = "FLAGACTIVO";
    public static final String CT_FLAGACTIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGACTIVO = 15;

    public static final String CN_FECHACREACIONDISPOSITIVO = "FECHACREACIONDISPOSITIVO";
    public static final String CT_FECHACREACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACREACIONDISPOSITIVO = 16;

    public static final String CN_FECHAMODIFICACIONDISPOSITIVO = "FECHAMODIFICACIONDISPOSITIVO";
    public static final String CT_FECHAMODIFICACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAMODIFICACIONDISPOSITIVO = 17;

    public static final String CN_FLAGENVIADO = "FLAGENVIADO";
    public static final String CT_FLAGENVIADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIADO = 18;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_REFERIDO, new OrganizateEntityFactory())
            .addColumn(CN_IDREFERIDO, CT_IDREFERIDO, CO_IDREFERIDO)
            .addColumn(CN_IDPROSPECTO, CT_IDPROSPECTO, CO_IDPROSPECTO)
            .addColumn(CN_IDREFERIDODISPOSITIVO, CT_IDREFERIDODISPOSITIVO, CO_IDREFERIDODISPOSITIVO)
            .addColumn(CN_IDPROSPECTODISPOSITIVO, CT_IDPROSPECTODISPOSITIVO, CO_IDPROSPECTODISPOSITIVO)
            .addColumn(CN_CODIGOTIPOREFERIDO, CT_CODIGOTIPOREFERIDO, CO_CODIGOTIPOREFERIDO)
            .addColumn(CN_NOMBRES, CT_NOMBRES, CO_NOMBRES)
            .addColumn(CN_APELLIDOPATERNO, CT_APELLIDOPATERNO, CO_APELLIDOPATERNO)
            .addColumn(CN_APELLIDOMATERNO, CT_APELLIDOMATERNO, CO_APELLIDOMATERNO)
            .addColumn(CN_CODIGORANGOEDAD, CT_CODIGORANGOEDAD, CO_CODIGORANGOEDAD)
            .addColumn(CN_FLAGHIJO, CT_FLAGHIJO, CO_FLAGHIJO)
            .addColumn(CN_CODIGORANGOINGRESO, CT_CODIGORANGOINGRESO, CO_CODIGORANGOINGRESO)
            .addColumn(CN_TELEFONO, CT_TELEFONO, CO_TELEFONO)
            .addColumn(CN_FECHACREACONTACTO, CT_FECHACREACONTACTO, CO_FECHACREACONTACTO)
            .addColumn(CN_CODIGOTIPOEMPLEO, CT_CODIGOTIPOEMPLEO, CO_CODIGOTIPOEMPLEO)
            .addColumn(CN_FLAGACTIVO, CT_FLAGACTIVO, CO_FLAGACTIVO)
            .addColumn(CN_FECHACREACIONDISPOSITIVO, CT_FECHACREACIONDISPOSITIVO, CO_FECHACREACIONDISPOSITIVO)
            .addColumn(CN_FECHAMODIFICACIONDISPOSITIVO, CT_FECHAMODIFICACIONDISPOSITIVO, CO_FECHAMODIFICACIONDISPOSITIVO)
            .addColumn(CN_FLAGENVIADO, CT_FLAGENVIADO, CO_FLAGENVIADO);

    public int getFlagModificado() {
        return FlagModificado;
    }

    public void setFlagModificado(int flagModificado) {
        FlagModificado = flagModificado;
    }
}