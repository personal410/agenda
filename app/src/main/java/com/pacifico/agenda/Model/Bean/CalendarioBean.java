package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class CalendarioBean extends Entity {

    private int IdCalendario;
    private int Anio;
    private int SemanaAno;
    private int MesAno;
    private int SemanaMes;
    private String FechaInicioSemana;
    private String FechaFinSemana;
    private int CodigoSucursal;
    private int CodigoCanal;

    ////////// GETTER AND SETTER /////////////////////////////////////////////////////////////////////

    public int getIdCalendario() {
        return IdCalendario;
    }

    public void setIdCalendario(int idCalendario) {
        IdCalendario = idCalendario;
    }

    public int getAnio() {
        return Anio;
    }

    public void setAnio(int anio) {
        Anio = anio;
    }

    public int getSemanaAno() {
        return SemanaAno;
    }

    public void setSemanaAno(int semanaAno) {
        SemanaAno = semanaAno;
    }

    public int getMesAno() {
        return MesAno;
    }

    public void setMesAno(int mesAno) {
        MesAno = mesAno;
    }

    public int getSemanaMes() {
        return SemanaMes;
    }

    public void setSemanaMes(int semanaMes) {
        SemanaMes = semanaMes;
    }

    public String getFechaInicioSemana() {
        return FechaInicioSemana;
    }

    public void setFechaInicioSemana(String fechaInicioSemana) {
        FechaInicioSemana = fechaInicioSemana;
    }

    public String getFechaFinSemana() {
        return FechaFinSemana;
    }

    public void setFechaFinSemana(String fechaFinSemana) {
        FechaFinSemana = fechaFinSemana;
    }

    public int getCodigoSucursal() {
        return CodigoSucursal;
    }

    public void setCodigoCanal(int codigoCanal) {
        CodigoCanal = codigoCanal;
    }

    public int getCodigoCanal() {
        return CodigoCanal;
    }

    public void setCodigoSucursal(int codigoSucursal) {
        CodigoSucursal = codigoSucursal;
    }

    /////////////////////////////////////////////////////////////////

    ///////////////// PERSISTENCIA//////////////////////////////////////

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDCALENDARIO:
                return IdCalendario;

            case CO_ANIO:
                return Anio;

            case CO_SEMANAANO:
                return SemanaAno;

            case CO_MESANO:
                return MesAno;

            case CO_SEMANAMES:
                return SemanaMes;

            case CO_FECHAINICIOSEMANA:
                return FechaInicioSemana;

            case CO_FECHAFINSEMANA:
                return FechaFinSemana;

            case CO_CODIGOSUCURSAL:
                return CodigoSucursal;
            case CO_CODIGOCANAL:
                return CodigoCanal;

        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDCALENDARIO:
                IdCalendario = (int) object;
                break;
            case CO_ANIO:
                Anio = (int) object;
                break;
            case CO_SEMANAANO:
                SemanaAno = (int) object;
                break;
            case CO_MESANO:
                MesAno = (int) object;
                break;
            case CO_SEMANAMES:
                SemanaMes = (int) object;
                break;
            case CO_FECHAINICIOSEMANA:
                FechaInicioSemana = (String) object;
                break;
            case CO_FECHAFINSEMANA:
                FechaFinSemana = (String) object;
                break;
            case CO_CODIGOSUCURSAL:
                CodigoSucursal = (int) object;
                break;
            case CO_CODIGOCANAL:
                CodigoCanal = (int) object;
                break;
        }
    }




    public static final String CN_IDCALENDARIO = "IDCALENDARIO";
    public static final String CT_IDCALENDARIO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDCALENDARIO = 1;

    public static final String CN_ANIO = "ANIO";
    public static final String CT_ANIO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_ANIO = 2;

    public static final String CN_SEMANAANO = "SEMANAANO";
    public static final String CT_SEMANAANO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_SEMANAANO = 3;

    public static final String CN_MESANO = "MESANO";
    public static final String CT_MESANO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_MESANO = 4;

    public static final String CN_SEMANAMES = "SEMANAMES";
    public static final String CT_SEMANAMES = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_SEMANAMES = 5;

    public static final String CN_FECHAINICIOSEMANA = "FECHAINICIOSEMANA";
    public static final String CT_FECHAINICIOSEMANA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAINICIOSEMANA = 6;

    public static final String CN_FECHAFINSEMANA = "FECHAFINSEMANA";
    public static final String CT_FECHAFINSEMANA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAFINSEMANA = 7;

    public static final String CN_CODIGOSUCURSAL = "CODIGOSUCURSAL";
    public static final String CT_CODIGOSUCURSAL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOSUCURSAL = 8;

    public static final String CN_CODIGOCANAL = "CODIGOCANAL";
    public static final String CT_CODIGOCANAL = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOCANAL = 9;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_CALENDARIO, new OrganizateEntityFactory())
            .addColumn(CN_IDCALENDARIO, CT_IDCALENDARIO, CO_IDCALENDARIO)
            .addColumn(CN_ANIO, CT_ANIO, CO_ANIO)
            .addColumn(CN_SEMANAANO, CT_SEMANAANO, CO_SEMANAANO)
            .addColumn(CN_MESANO, CT_MESANO, CO_MESANO)
            .addColumn(CN_SEMANAMES, CT_SEMANAMES, CO_SEMANAMES)
            .addColumn(CN_FECHAINICIOSEMANA, CT_FECHAINICIOSEMANA, CO_FECHAINICIOSEMANA)
            .addColumn(CN_FECHAFINSEMANA, CT_FECHAFINSEMANA, CO_FECHAFINSEMANA)
            .addColumn(CN_CODIGOSUCURSAL, CT_CODIGOSUCURSAL, CO_CODIGOSUCURSAL)
            .addColumn(CN_CODIGOCANAL, CT_CODIGOCANAL, CO_CODIGOCANAL);
}
