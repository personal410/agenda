package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class ReunionInternaBean extends Entity {

    private int IdReunionInterna;
    private int CodigoIntermediario;
    private int IdReunionInternaDispositivo;
    private String FechaReunion;
    private String HoraInicio;
    private String HoraFin;
    private String Ubicacion;
    private int FlagInvitadoGU;
    private int FlagInvitadoGA;
    private String Recordatorio;
    private int CodigoTipoReunion;
    private String FechaCreacionDispositivo;
    private String FechaModificacionDispositivo;
    private int FlagEnviado;
    //private int IdContactoInterno;

    public ReunionInternaBean(){
        IdReunionInterna = -1;
        CodigoIntermediario = -1;
        IdReunionInternaDispositivo = -1;
        FlagInvitadoGU = -1;
        FlagInvitadoGA = -1;
        CodigoTipoReunion = -1;
        FlagEnviado = -1;
    }

    public int getIdReunionInterna() {
        return IdReunionInterna;
    }

    public void setIdReunionInterna(int idReunionInterna) {
        IdReunionInterna = idReunionInterna;
    }

    public int getCodigoIntermediario() {
        return CodigoIntermediario;
    }

    public void setCodigoIntermediario(int codigoIntermediario) {
        CodigoIntermediario = codigoIntermediario;
    }

    public int getIdReunionInternaDispositivo() {
        return IdReunionInternaDispositivo;
    }

    public void setIdReunionInternaDispositivo(int idReunionInternaDispositivo) {
        IdReunionInternaDispositivo = idReunionInternaDispositivo;
    }

    public String getFechaReunion() {
        return FechaReunion;
    }

    public void setFechaReunion(String fechaReunion) {
        FechaReunion = fechaReunion;
    }

    public String getHoraInicio() {
        return HoraInicio;
    }

    public void setHoraInicio(String horaInicio) {
        HoraInicio = horaInicio;
    }

    public String getHoraFin() {
        return HoraFin;
    }

    public void setHoraFin(String horaFin) {
        HoraFin = horaFin;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        Ubicacion = ubicacion;
    }

    public int getFlagInvitadoGU() {
        return FlagInvitadoGU;
    }

    public void setFlagInvitadoGU(int flagInvitadoGU) {
        FlagInvitadoGU = flagInvitadoGU;
    }

    public int getFlagInvitadoGA() {
        return FlagInvitadoGA;
    }

    public void setFlagInvitadoGA(int flagInvitadoGA) {
        FlagInvitadoGA = flagInvitadoGA;
    }

    public String getRecordatorio() {
        return Recordatorio;
    }

    public void setRecordatorio(String recordatorio) {
        Recordatorio = recordatorio;
    }

    public String getFechaCreacionDispositivo() {
        return FechaCreacionDispositivo;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return FechaModificacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public int getFlagEnviado() {
        return FlagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        FlagEnviado = flagEnviado;
    }

    public int getCodigoTipoReunion() {
        return CodigoTipoReunion;
    }

    public void setCodigoTipoReunion(int codigoTipoReunion) {
        CodigoTipoReunion = codigoTipoReunion;
    }

    /*public int getIdContactoInterno() {
        return IdContactoInterno;
    }

    public void setIdContactoInterno(int idContactoInterno) {
        IdContactoInterno = idContactoInterno;
    }*/

    // Persistencia

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDREUNIONINTERNA:
                return IdReunionInterna;

            case CO_CODIGOINTERMEDIARIO:
                return CodigoIntermediario;

            case CO_IDREUNIONINTERNADISPOSITIVO:
                return IdReunionInternaDispositivo;

            case CO_FECHAREUNION:
                return FechaReunion;

            case CO_HORAINICIO:
                return HoraInicio;

            case CO_HORAFIN:
                return HoraFin;

            case CO_UBICACION:
                return Ubicacion;

            case CO_FLAGINVITADOGU:
                return FlagInvitadoGU;

            case CO_FLAGINVITADOGA:
                return FlagInvitadoGA;

            case CO_CODIGOTIPOREUNION:
                return CodigoTipoReunion;

            case CO_RECORDATORIO:
                return Recordatorio;

            case CO_FECHACREACIONDISPOSITIVO:
                return FechaCreacionDispositivo;

            case CO_FECHAMODIFICACIONDISPOSITIVO:
                return FechaModificacionDispositivo;

            case CO_FLAGENVIADO:
                return FlagEnviado;

            //case CO_IDCONTACTOINTERNO:
                //return IdContactoInterno;

        }
        return null;
    }



    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDREUNIONINTERNA:
                IdReunionInterna = (int) object;
                break;
            case CO_CODIGOINTERMEDIARIO:
                CodigoIntermediario = (int) object;
                break;
            case CO_IDREUNIONINTERNADISPOSITIVO:
                IdReunionInternaDispositivo = (int) object;
                break;
            case CO_FECHAREUNION:
                FechaReunion = (String) object;
                break;
            case CO_HORAINICIO:
                HoraInicio = (String) object;
                break;
            case CO_HORAFIN:
                HoraFin = (String) object;
                break;
            case CO_UBICACION:
                Ubicacion = (String) object;
                break;
            case CO_FLAGINVITADOGU:
                FlagInvitadoGU = (int) object;
                break;
            case CO_FLAGINVITADOGA:
                FlagInvitadoGA = (int) object;
                break;
            case CO_CODIGOTIPOREUNION:
                CodigoTipoReunion = (int) object;
                break;
            case CO_RECORDATORIO:
                Recordatorio = (String) object;
                break;
            case CO_FECHACREACIONDISPOSITIVO:
                FechaCreacionDispositivo = (String) object;
                break;
            case CO_FECHAMODIFICACIONDISPOSITIVO:
                FechaModificacionDispositivo = (String) object;
                break;
            case CO_FLAGENVIADO:
                FlagEnviado = (int) object;
                break;
            /*case CO_IDCONTACTOINTERNO:
                IdContactoInterno = (String) object;
                break;*/
        }
    }

    public static final String CN_IDREUNIONINTERNA = "IDREUNIONINTERNA";
    public static final String CT_IDREUNIONINTERNA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDREUNIONINTERNA = 1;

    public static final String CN_CODIGOINTERMEDIARIO = "CODIGOINTERMEDIARIO";
    public static final String CT_CODIGOINTERMEDIARIO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOINTERMEDIARIO = 2;

    public static final String CN_IDREUNIONINTERNADISPOSITIVO = "IDREUNIONINTERNADISPOSITIVO";
    public static final String CT_IDREUNIONINTERNADISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDREUNIONINTERNADISPOSITIVO = 3;

    public static final String CN_FECHAREUNION = "FECHAREUNION";
    public static final String CT_FECHAREUNION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAREUNION = 4;

    public static final String CN_HORAINICIO = "HORAINICIO";
    public static final String CT_HORAINICIO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_HORAINICIO = 5;

    public static final String CN_HORAFIN = "HORAFIN";
    public static final String CT_HORAFIN = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_HORAFIN = 6;

    public static final String CN_UBICACION = "UBICACION";
    public static final String CT_UBICACION = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_UBICACION = 7;

    public static final String CN_FLAGINVITADOGU = "FLAGINVITADOGU";
    public static final String CT_FLAGINVITADOGU = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGINVITADOGU = 8;

    public static final String CN_FLAGINVITADOGA = "FLAGINVITADOGA";
    public static final String CT_FLAGINVITADOGA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGINVITADOGA = 9;

    public static final String CN_CODIGOTIPOREUNION = "CODIGOTIPOREUNION";
    public static final String CT_CODIGOTIPOREUNION = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOTIPOREUNION = 10;

    public static final String CN_RECORDATORIO = "RECORDATORIO";
    public static final String CT_RECORDATORIO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_RECORDATORIO = 11;

    public static final String CN_FECHACREACIONDISPOSITIVO = "FECHACREACIONDISPOSITIVO";
    public static final String CT_FECHACREACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHACREACIONDISPOSITIVO = 12;

    public static final String CN_FECHAMODIFICACIONDISPOSITIVO = "FECHAMODIFICACIONDISPOSITIVO";
    public static final String CT_FECHAMODIFICACIONDISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAMODIFICACIONDISPOSITIVO = 13;

    public static final String CN_FLAGENVIADO = "FLAGENVIADO";
    public static final String CT_FLAGENVIADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIADO = 14;

    /*public static final String CN_IDCONTACTOINTERNO = "IDCONTACTOINTERNO";
    public static final String CT_IDCONTACTOINTERNO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_IDCONTACTOINTERNO = 14;*/

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_REUNION_INTERNA, new OrganizateEntityFactory())
            .addColumn(CN_IDREUNIONINTERNA, CT_IDREUNIONINTERNA, CO_IDREUNIONINTERNA)
            .addColumn(CN_CODIGOINTERMEDIARIO, CT_CODIGOINTERMEDIARIO, CO_CODIGOINTERMEDIARIO)
            .addColumn(CN_IDREUNIONINTERNADISPOSITIVO, CT_IDREUNIONINTERNADISPOSITIVO, CO_IDREUNIONINTERNADISPOSITIVO)
            .addColumn(CN_FECHAREUNION, CT_FECHAREUNION, CO_FECHAREUNION)
            .addColumn(CN_HORAINICIO, CT_HORAINICIO, CO_HORAINICIO)
            .addColumn(CN_HORAFIN, CT_HORAFIN, CO_HORAFIN)
            .addColumn(CN_UBICACION, CT_UBICACION, CO_UBICACION)
            .addColumn(CN_FLAGINVITADOGU, CT_FLAGINVITADOGU, CO_FLAGINVITADOGU)
            .addColumn(CN_FLAGINVITADOGA, CT_FLAGINVITADOGA, CO_FLAGINVITADOGA)
            .addColumn(CN_RECORDATORIO, CT_RECORDATORIO, CO_RECORDATORIO)
            .addColumn(CN_CODIGOTIPOREUNION, CT_CODIGOTIPOREUNION, CO_CODIGOTIPOREUNION)
            .addColumn(CN_FECHACREACIONDISPOSITIVO, CT_FECHACREACIONDISPOSITIVO, CO_FECHACREACIONDISPOSITIVO)
            .addColumn(CN_FECHAMODIFICACIONDISPOSITIVO, CT_FECHAMODIFICACIONDISPOSITIVO, CO_FECHAMODIFICACIONDISPOSITIVO)
            .addColumn(CN_FLAGENVIADO, CT_FLAGENVIADO, CO_FLAGENVIADO);
            //.addColumn(CN_IDCONTACTOINTERNO, CT_IDCONTACTOINTERNO, CO_IDCONTACTOINTERNO);
}
