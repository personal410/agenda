package com.pacifico.agenda.Model.Controller;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Model.Bean.CalendarioBean;
import com.pacifico.agenda.Model.Bean.IntermediarioBean;
import com.pacifico.agenda.Model.Bean.MensajeSistemaBean;
import com.pacifico.agenda.Model.Bean.ParametroBean;
import com.pacifico.agenda.Model.Bean.TablaIndiceBean;
import com.pacifico.agenda.Model.Bean.TablaTablasBean;
import com.pacifico.agenda.Util.Constantes;

import java.util.ArrayList;

/**
 * Created by Joel on 02/06/2016.
 */


public class TablasGeneralesController {

    // TABLAS DE SOLO LECTURA

    // TablaIndice
    public static void guardarTablaIndice(TablaIndiceBean tablaIndiceBean){
        TablaIndiceBean.tableHelper.insertEntity(tablaIndiceBean);
    }
    public static void guardarListaTablaIndice(ArrayList<TablaIndiceBean> listaTablaIndiceBean){
        if(listaTablaIndiceBean != null){
            for(int i = 0; i < listaTablaIndiceBean.size(); i++){
                TablaIndiceBean.tableHelper.insertEntity(listaTablaIndiceBean.get(i));
            }
        }
    }

    // TablaTablas
    public static void guardarTablaTablas(TablaTablasBean tablaTablas) {
        TablaTablasBean.tableHelper.insertEntity(tablaTablas);
    }

    public static void actualizarTablaIndice(TablaIndiceBean tablaIndiceBean){
        String[] parametros = {Integer.toString(tablaIndiceBean.getIdTabla())};
        TablaIndiceBean.tableHelper.updateEntity(tablaIndiceBean, "IDTABLA = ?", parametros);
    }

    public static TablaIndiceBean obtenerTablaIndicePorIdTabla(int idTabla){
        String[] parametros = new String[]{Integer.toString(idTabla)};
        ArrayList<Entity> arrTablaIndices = TablaIndiceBean.tableHelper.getEntities("IDTABLA = ?", parametros);
        if (arrTablaIndices.size() > 0){
            return  (TablaIndiceBean)arrTablaIndices.get(0);
        }else{
            return null;
        }
    }

    public static void limpiarTablaTablaIndice(){
        TablaIndiceBean.tableHelper.deleteAllEntities();
    }

    public static void guardarListaTablaTablas(ArrayList<TablaTablasBean> listaTablaTablasBean)
    {
        if (listaTablaTablasBean != null && listaTablaTablasBean.size()>0)
            for(int i =0; i<listaTablaTablasBean.size(); i++)
                TablaTablasBean.tableHelper.insertEntity(listaTablaTablasBean.get(i));
    }

    public static TablaTablasBean ObtenerItemTablaTablas(int idTabla, int idItem){
        String[] parameters = new String[]{String.valueOf(idTabla),String.valueOf(idItem)};
        ArrayList<Entity> arrTablaTablas = TablaTablasBean.tableHelper.getEntities("IDTABLA = ? AND CODIGOCAMPO = ?", parameters);

            if(arrTablaTablas.size() > 0){
                return (TablaTablasBean)arrTablaTablas.get(0);
            }else{
                return null;
            }
    }

    public static ArrayList<TablaTablasBean> obtenerTablaTablasPorIdTabla(int IdTabla){
        String[] parameters = new String[]{Integer.toString(IdTabla)};
        ArrayList<Entity> arrTablaTablas = TablaTablasBean.tableHelper.getEntities("IDTABLA = ? AND FLAGACTIVO = 1", parameters);
        ArrayList<TablaTablasBean> arrTablaTablasFinal = new ArrayList<>();
        for (Entity entity : arrTablaTablas) {
            arrTablaTablasFinal.add((TablaTablasBean)entity);
        }
        return arrTablaTablasFinal;
    }

    public static ArrayList<TablaTablasBean> obtenerTablaTablasPorIdTablaIncluidos(int IdTabla, String ids){
        String[] parameters = new String[]{Integer.toString(IdTabla),ids};
        ArrayList<Entity> arrTablaTablas = TablaTablasBean.tableHelper.getEntities("IDTABLA = ? AND CODIGOCAMPO IN (?)", parameters);
        ArrayList<TablaTablasBean> arrTablaTablasFinal = new ArrayList<>();
        for (Entity entity : arrTablaTablas) {
            arrTablaTablasFinal.add((TablaTablasBean)entity);
        }
        return arrTablaTablasFinal;
    }

    public static TablaTablasBean obtenerTablaTablasPorIdTablaCodigoCampo(int idTabla, int codigoCampo){
        String[] parametros = {Integer.toString(idTabla), Integer.toString(codigoCampo)};
        ArrayList<Entity> arrTablaTablas = TablaTablasBean.tableHelper.getEntities("IDTABLA = ? AND CODIGOCAMPO = ? AND FLAGACTIVO = 1", parametros);
        if(arrTablaTablas.size() > 0){
            return (TablaTablasBean)arrTablaTablas.get(0);
        }else{
            return null;
        }
    }

    /*public static ArrayList<TablaTablasBean> obtenerFuentesAlternativas(){
        String[] parameters = new String[]{String.valueOf(Constantes.Fuente),String.valueOf(Constantes.Fuente_ADN)};
        ArrayList<Entity> arrTablaTablas = TablaTablasBean.tableHelper.getEntities("IDTABLA = ? AND CODIGOCAMPO != ?", parameters);
        ArrayList<TablaTablasBean> arrTablaTablasFinal = new ArrayList<>();
        for (Entity entity : arrTablaTablas) {
            arrTablaTablasFinal.add((TablaTablasBean)entity);
        }
        return arrTablaTablasFinal;
    }*/

    public static void actualizarTablaTablas(TablaTablasBean tablaTablasBean){
        String[] parametros = {Integer.toString(tablaTablasBean.getIdTabla()), Integer.toString(tablaTablasBean.getCodigoCampo())};
        TablaTablasBean.tableHelper.updateEntity(tablaTablasBean, "IDTABLA = ? AND CODIGOCAMPO = ?", parametros);
    }
    public static void limpiarTablaTablaTablas(){
        TablaTablasBean.tableHelper.deleteAllEntities();
    }
    //////////////////////////////////////////////

    // Calendario
    public static void guardarCalendario(CalendarioBean calendarioBean) {
        CalendarioBean.tableHelper.insertEntity(calendarioBean);
    }

    public static void guardarListaCalendario(ArrayList<CalendarioBean> listaCalendarioBean)
    {
        if (listaCalendarioBean != null && listaCalendarioBean.size()>0)
            for(int i =0; i<listaCalendarioBean.size(); i++)
                CalendarioBean.tableHelper.insertEntity(listaCalendarioBean.get(i));
    }

    public static CalendarioBean obtenerCalendarioPorIdCalendario(int idCalendario){
        String[] parametros = {Integer.toString(idCalendario)};
        ArrayList<Entity> arrTablaTablas = CalendarioBean.tableHelper.getEntities("IDCALENDARIO = ?", parametros);
        if(arrTablaTablas.size() > 0){
            return (CalendarioBean)arrTablaTablas.get(0);
        }else{
            return null;
        }
    }

    public static void actualizarCalendario(CalendarioBean calendarioBean){
        String[] parametros = {Integer.toString(calendarioBean.getIdCalendario())};
        CalendarioBean.tableHelper.updateEntity(calendarioBean, "IDCALENDARIO = ?", parametros);
    }

    public static void limpiarTablaCalendario(){
        CalendarioBean.tableHelper.deleteAllEntities();
    }

    // MensajeSistema
    public static void guardarMensajeSistema(MensajeSistemaBean mensajeSistemaBean) {
        MensajeSistemaBean.tableHelper.insertEntity(mensajeSistemaBean);
    }

    public static void guardarListaMensajesSistemas(ArrayList<MensajeSistemaBean> listaMensajeSistemaBean)
    {
        if (listaMensajeSistemaBean != null && listaMensajeSistemaBean.size()>0)
            for(int i =0; i<listaMensajeSistemaBean.size(); i++)
                MensajeSistemaBean.tableHelper.insertEntity(listaMensajeSistemaBean.get(i));
    }

    public static void limpiarTablaMensajeSistema(){
        MensajeSistemaBean.tableHelper.deleteAllEntities();
    }

    public static MensajeSistemaBean obtenerMensajeSistemaPorIdMensajeSistema(int idMensajeSistema){
        String[] parametros = {Integer.toString(idMensajeSistema)};
        ArrayList<Entity> arrMensajeSistemas = MensajeSistemaBean.tableHelper.getEntities("IDMENSAJESISTEMA = ?", parametros);
        if(arrMensajeSistemas.size() > 0){
            return (MensajeSistemaBean)arrMensajeSistemas.get(0);
        }else{
            return null;
        }
    }

    public static void actualizarMensajeSistema(MensajeSistemaBean mensajeSistemaBean){
        String[] parametros = {Integer.toString(mensajeSistemaBean.getIdMensajeSistema())};
        MensajeSistemaBean.tableHelper.updateEntity(mensajeSistemaBean, "IDMENSAJESISTEMA = ?", parametros);
    }

}