package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by Joel on 31/05/2016.
 */
public class CitaMovimientoEstadoBean extends Entity {


    private int IdMovimiento;
    private int IdMovimientoDispositivo;
    private int IdCita;
    private int IdCitaDispositivo;
    private int CodigoEstado;
    private int CodigoResultado;
    private String FechaMovimientoEstadoDispositivo;
    private int FlagEnviado;

    public CitaMovimientoEstadoBean(){
        IdMovimiento = -1;
        IdMovimientoDispositivo = -1;
        IdCita = -1;
        IdCitaDispositivo = -1;
        IdMovimientoDispositivo = -1;
        CodigoEstado = -1;
        CodigoResultado = -1;
        FechaMovimientoEstadoDispositivo = null;
        FlagEnviado = -1;
    }

    public int getIdMovimiento() {
        return IdMovimiento;
    }

    public void setIdMovimiento(int idMovimiento) {
        IdMovimiento = idMovimiento;
    }

    public int getIdCita() {
        return IdCita;
    }

    public void setIdCita(int idCita) {
        IdCita = idCita;
    }

    public int getIdMovimientoDispositivo() {
        return IdMovimientoDispositivo;
    }

    public void setIdMovimientoDispositivo(int idMovimientoDispositivo) {
        IdMovimientoDispositivo = idMovimientoDispositivo;
    }

    public int getCodigoEstado() {
        return CodigoEstado;
    }

    public void setCodigoEstado(int codigoEstado) {
        CodigoEstado = codigoEstado;
    }

    public int getCodigoResultado() {
        return CodigoResultado;
    }

    public void setCodigoResultado(int codigoResultado) {
        CodigoResultado = codigoResultado;
    }

    public String getFechaMovimientoEstadoDispositivo() {
        return FechaMovimientoEstadoDispositivo;
    }

    public void setFechaMovimientoEstadoDispositivo(String fechaMovimientoEstadoDispositivo) {
        FechaMovimientoEstadoDispositivo = fechaMovimientoEstadoDispositivo;
    }

    public int getIdCitaDispositivo() {
        return IdCitaDispositivo;
    }

    public void setIdCitaDispositivo(int idCitaDispositivo) {
        IdCitaDispositivo = idCitaDispositivo;
    }

    public int getFlagEnviado() {
        return FlagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        FlagEnviado = flagEnviado;
    }

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_IDMOVIMIENTO:
                return IdMovimiento;
            case CO_IDCITA:
                return IdCita;
            case CO_IDMOVIMIENTODISPOSITIVO:
                return IdMovimientoDispositivo;
            case CO_CODIGOESTADO:
                return CodigoEstado;
            case CO_CODIGORESULTADO:
                return CodigoResultado;
            case CO_FECHAMOVIMIENTOESTADODISPOSITIVO:
                return FechaMovimientoEstadoDispositivo;
            case CO_IDCITADISPOSITIVO:
                return IdCitaDispositivo;
            case CO_FLAGENVIADO:
                return FlagEnviado;
        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_IDMOVIMIENTO:
                IdMovimiento = (int) object;
                break;
            case CO_IDCITA:
                IdCita = (int) object;
                break;
            case CO_IDMOVIMIENTODISPOSITIVO:
                IdMovimientoDispositivo = (int) object;
                break;
            case CO_CODIGOESTADO:
                CodigoEstado = (int) object;
                break;
            case CO_CODIGORESULTADO:
                CodigoResultado = (int) object;
                break;
            case CO_FECHAMOVIMIENTOESTADODISPOSITIVO:
                FechaMovimientoEstadoDispositivo = (String) object;
                break;
            case CO_IDCITADISPOSITIVO:
                IdCitaDispositivo = (int) object;
                break;
            case CO_FLAGENVIADO:
                FlagEnviado = (int) object;
                break;
        }
    }

    public static final String CN_IDMOVIMIENTO = "IDMOVIMIENTO";
    public static final String CT_IDMOVIMIENTO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDMOVIMIENTO = 1;

    public static final String CN_IDMOVIMIENTODISPOSITIVO = "IDMOVIMIENTODISPOSITIVO";
    public static final String CT_IDMOVIMIENTODISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDMOVIMIENTODISPOSITIVO = 2;

    public static final String CN_IDCITA = "IDCITA";
    public static final String CT_IDCITA = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDCITA = 3;

    public static final String CN_IDCITADISPOSITIVO = "IDCITADISPOSITIVO";
    public static final String CT_IDCITADISPOSITIVO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDCITADISPOSITIVO = 4;

    public static final String CN_CODIGOESTADO = "CODIGOESTADO";
    public static final String CT_CODIGOESTADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGOESTADO = 5;

    public static final String CN_CODIGORESULTADO = "CODIGORESULTADO";
    public static final String CT_CODIGORESULTADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_CODIGORESULTADO = 6;

    public static final String CN_FECHAMOVIMIENTOESTADODISPOSITIVO = "FECHAMOVIMIENTOESTADODISPOSITIVO";
    public static final String CT_FECHAMOVIMIENTOESTADODISPOSITIVO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_FECHAMOVIMIENTOESTADODISPOSITIVO = 7;

    public static final String CN_FLAGENVIADO = "FLAGENVIADO";
    public static final String CT_FLAGENVIADO = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_FLAGENVIADO = 8;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO, new OrganizateEntityFactory())
            .addColumn(CN_IDMOVIMIENTO, CT_IDMOVIMIENTO, CO_IDMOVIMIENTO)
            .addColumn(CN_IDMOVIMIENTODISPOSITIVO, CT_IDMOVIMIENTODISPOSITIVO, CO_IDMOVIMIENTODISPOSITIVO)
            .addColumn(CN_IDCITA, CT_IDCITA, CO_IDCITA)
            .addColumn(CN_IDCITADISPOSITIVO, CT_IDCITADISPOSITIVO, CO_IDCITADISPOSITIVO)
            .addColumn(CN_CODIGOESTADO, CT_CODIGOESTADO, CO_CODIGOESTADO)
            .addColumn(CN_CODIGORESULTADO, CT_CODIGORESULTADO, CO_CODIGORESULTADO)
            .addColumn(CN_FECHAMOVIMIENTOESTADODISPOSITIVO, CT_FECHAMOVIMIENTOESTADODISPOSITIVO, CO_FECHAMOVIMIENTOESTADODISPOSITIVO)
            .addColumn(CN_FLAGENVIADO, CT_FLAGENVIADO, CO_FLAGENVIADO);
}
