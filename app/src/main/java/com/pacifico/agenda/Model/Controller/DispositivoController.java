package com.pacifico.agenda.Model.Controller;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.agenda.Model.Bean.DispositivoBean;

import java.util.ArrayList;

/**
 * Created by joel on 7/18/16.
 */
public class DispositivoController{
    public static void guardarDispositivo(DispositivoBean dispositivoBean){
        int idDispositivo = dispositivoBean.getIdDispositivo();
        if(obtenerDispositivoPorIdDispositivo(idDispositivo) == null){
            DispositivoBean.tableHelper.insertEntity(dispositivoBean);
        }else{
            String[] parametros = {Integer.toString(idDispositivo)};
            DispositivoBean.tableHelper.updateEntity(dispositivoBean, "IDDISPOSITIVO = ?", parametros);
        }
    }
    public static DispositivoBean obtenerDispositivoPorIdDispositivo(int idDispositivo){
        String[] parametros = {Integer.toString(idDispositivo)};
        ArrayList<Entity> arrDispositivos = DispositivoBean.tableHelper.getEntities("IDDISPOSITIVO = ?", parametros);
        if(arrDispositivos.size() > 0){
            return (DispositivoBean)arrDispositivos.get(0);
        }else{
            return null;
        }
    }
    public static void limpiarTablaDispositivo(){
        DispositivoBean.tableHelper.deleteAllEntities();
    }
}