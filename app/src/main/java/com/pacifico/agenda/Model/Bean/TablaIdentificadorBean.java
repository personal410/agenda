package com.pacifico.agenda.Model.Bean;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.Persistence.OrganizateEntityFactory;

/**
 * Created by joel on 7/4/16.
 */
public class TablaIdentificadorBean extends Entity {

    private String nombreTabla;
    private int identity;

    public String getNombreTabla() {
        return nombreTabla;
    }

    public void setnombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    public int getIdentity() {
        return identity;
    }

    public void setIdentity(int identity) {
        this.identity = identity;
    }

    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_NOMBRETABLA:
                return nombreTabla;

            case CO_IDENTITY:
                return identity;

        }
        return null;
    }

    @Override
    public void setColumnValue(int column, Object object) {
        switch (column) {
            case CO_NOMBRETABLA:
                nombreTabla = (String) object;
                break;
            case CO_IDENTITY:
                identity = (int) object;
                break;
        }
    }

    public static final String CN_NOMBRETABLA = "NOMBRETABLA";
    public static final String CT_NOMBRETABLA = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_NOMBRETABLA = 1;

    public static final String CN_IDENTITY = "IDENTIFICADOR";
    public static final String CT_IDENTITY = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_IDENTITY = 2;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_IDENTIFICADOR, new OrganizateEntityFactory())
            .addColumn(CN_NOMBRETABLA, CT_NOMBRETABLA, CO_NOMBRETABLA)
            .addColumn(CN_IDENTITY, CT_IDENTITY, CO_IDENTITY);
}
