package com.pacifico.agenda.Util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.pacifico.agenda.Model.Bean.DispositivoBean;
import com.pacifico.agenda.Model.Bean.ParametroBean;
import com.pacifico.agenda.Model.Controller.DispositivoController;
import com.pacifico.agenda.Model.Controller.ParametroController;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by joel on 6/27/16.
 */
public class Util {

    public static double expiroTiempoSinSincronizacion(){
        DispositivoBean dispositivoBean = DispositivoController.obtenerDispositivoPorIdDispositivo(1);
        String fechaUltimaSincronizacion = dispositivoBean.getFechaUltimaSincronizacion();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSS");
        try{
            Date dateUltimaSincronizacion = simpleDateFormat.parse(fechaUltimaSincronizacion);
            Date dateAhora = new Date();
            double diferencia = dateAhora.getTime() - dateUltimaSincronizacion.getTime();
            ParametroBean tiempoSinSincronizacionParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(9);
            double tiempoSinSincronizacion = tiempoSinSincronizacionParametroBean.getValorNumerico() * 60 * 60 * 1000;
            diferencia = diferencia - tiempoSinSincronizacion;
            return diferencia;
        } catch (ParseException e) {
            e.printStackTrace();
            Log.i("TAG", "error");
        }
        return 0;
    }


    public static String obtenerFechaActual(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar ahora = Calendar.getInstance();
        return simpleDateFormat.format(ahora.getTime());
    }

    public static String obtenerFechaActual_SoloFecha(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar ahora = Calendar.getInstance();
        return simpleDateFormat.format(ahora.getTime());
    }

    public static String strNULL(Object object) {
        return object == null ? "" : object.toString();
    }


    public static void showToast(Context context, String mensaje) {

        Toast.makeText(context, mensaje,
                Toast.LENGTH_SHORT).show();

    }

    public static boolean esVacioONull(Object o)
    {
        if (o == null)
            return true;

        if (o.toString().trim().isEmpty())
            return true;

        return false;
    }
    public static double redondearNumero(double numero, int decimales){
        int factor = (int)Math.pow(10, decimales);
        numero = numero * factor;
        double numeroRedondeado = Math.round(numero);
        return numeroRedondeado / factor;
    }

    public static String capitalizedString(String string){

        if(string != null && string.length() > 0){
            String[] arrString = string.split(" ");
            String strFinal = "";
            for(int i = 0; i < arrString.length; i++){
                String tempString = arrString[i].trim();

                if (tempString.length() > 1)
                    strFinal = strFinal + String.format("%s%s ", tempString.substring(0, 1), tempString.substring(1).toLowerCase());
                else if (tempString.length() == 1)
                    strFinal = strFinal + tempString;
            }
            return strFinal.trim();
        }else{
            return "";
        }
    }

    public static void mostrarAlertaConTitulo(String titulo, String mensaje, Context context){
        new AlertDialog.Builder(context)
                .setTitle(titulo)
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).setCancelable(false).show();
    }

    public static String obtenerNumeroConFormato(double numero){
        return obtenerNumeroConFormatoDefinido(numero, "##,###,###");
    }
    public static String obtenerNumeroConDecimales(double numero){
        return obtenerNumeroConFormatoDefinido(numero, "###########.##");
    }
    public static String obtenerNumeroConFormatoDefinido(double numero, String formato){
        return obtenerDecimalFormat(formato).format(Math.round(numero));
    }

    public static DecimalFormat obtenerDecimalFormat(String formato){
        DecimalFormat formatter = new DecimalFormat(formato);
        DecimalFormatSymbols custom = new DecimalFormatSymbols(new Locale("es", "US"));
        formatter.setDecimalFormatSymbols(custom);
        return formatter;
    }

    public static String getFechaInicioCumpleanos(DateTime calendar) {

        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM-dd");
        String fecha = fmt.print(calendar);

        return "1900-" + fecha;
    }

    public static String quitarAnio(String fecha) {

        return fecha.substring(5, 10);

    }

    public static int getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        /*if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }*/
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
}
