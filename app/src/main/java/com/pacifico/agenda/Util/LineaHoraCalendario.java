package com.pacifico.agenda.Util;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import com.pacifico.agenda.R;

/**
 * Created by joel on 8/25/16.
 */
public class LineaHoraCalendario extends CeldaView{

    public LineaHoraCalendario(Context context){
        //linea_horacalendar_view
        // celda_cita
        view = LayoutInflater.from(context).inflate(R.layout.linea_horacalendar_view, null);
        linHorizontal = (LinearLayout) view.findViewById(R.id.linHorizontal);
        spaceBefore = (Space)view.findViewById(R.id.spaceBefore);
    }

}
