package com.pacifico.agenda.Util;

import java.util.Calendar;

/**
 * Created by joel on 6/20/16.
 */
public class Constantes {

    // TablaTablas

    public static final String SemillaEncriptacion = "pacifico2016";
    public static final String URLBase = "https://desvconecta.pacificovida.net";
    public static final String MensajeErrorCampoVacio = "Por favor registra los datos para este campo";

    /// TODO: ACTUALIZAR POR LOS VALORES DE MOTIVO DE REAGENDADO
    public static final int Reagendado_ProspectoCambioFecha = 1; // Prospecto cambio la fecha
    public static final int Reagendado_NoPudeHoraPactada = 3; // Intermediario no pudo en la hora pactada

    // TABLA id 9 - Etapa Prospecto
    public static final int EtapaProspecto_Nuevo = 1;
    public static final int EtapaProspecto_PrimeraEntrevista = 2;
    public static final int EtapaProspecto_SegundaEntrevista = 3;
    public static final int EtapaProspecto_EntrevistaAdicional = 4;

    // Tabla id 10 - Estado del Prospecto
    public static final int EstadoProspecto_Nuevo = 1;
    public static final int EstadoProspecto_EnProceso = 2;
    public static final int EstadoProspecto_CierreVenta = 3;
    public static final int EstadoProspecto_NoInteresado = 4;

    // Tabla id 11 - EstadoCita
    public static final int EstadoCita_PorContactar = 1;
    public static final int EstadoCita_Agendada = 2;
    public static final int EstadoCita_Realizada = 3;
    public static final int EstadoCita_ReAgendada = 4;

    // Tabla id 12 - ResultadoCita
    public static final int ResultadoCita_Pendiente = 0;
    public static final int ResultadoCita_CierreVenta = 1;
    public static final int ResultadoCita_SiguienteEtapa = 2;
    public static final int ResultadoCita_NoInteresado = 3;
    public static final int ResultadoCita_PorContactar = 4;
    public static final int ResultadoCita_ReAgendado = 5;
    public static final int ResultadoCita_VolveraLlamar = 6;

    // Tabla id 3
    public static final int RangoEdad_menor25= 1;
    public static final int RangoEdad_entre25_30= 2;
    public static final int RangoEdad_entre31_40= 3;
    public static final int RangoEdad_mayor41= 4;

    // Tabla id 7 - Fuentes
    public static final int Fuente_ADN = 2;

    // Tabla id 14 - Tipos Reunion
    public static final int Reunion_Interna = 1;
    public static final int Reunion_Externa = 2;

    // TablaIndice
    public static final int RangoEdad = 3;
    public static final int RangoIngresos = 4;
    public static final int Fuente = 7;

    // Formatos Fechas
    public static final String formato_fechafull_envio = "yyyy-MM-dd HH:mm";
    public static final String formato_solofecha_envio = "yyyy-MM-dd";
    public static final String formato_solohora_envio = "HH:mm";
    public static final String formato_fecha_mostrar = "dd-MM-yyyy";

    // Flag Activo
    public static final int FLAG_VERDADERO = 1;
    public static final int FLAG_FALSO = 0;

    // Flag Activo
    public static final int FLAG_ACTIVO = 1;
    public static final int FLAG_INACTIVO = 0;

    // Estados Flags de Envio
    public static final int ENVIO_PENDIENTE = 0;
    public static final int ENVIO_ENVIANDO = 1;
    public static final int ENVIO_RECIBIDO = 2; // validado por el servidor

    /*public static final String PROSPECTO_POR_CONTACTAR = "1";
    public static final String PROSPECTO_AGENDADO = "1";
    public static final String PROSPECTO_REALIZADO = "1"; */

    public static final int parametro_fuentesAgenda = 12;


    public static final int MonedaSoles = 0;
    public static final int MonedaDolares = 1;

    public static final int proceso_reagendada_fecha = 1;
    public static final int proceso_reagendada_volverLLamar = 2;

    /////// TIPOS EVENTO ( PARA PINTAR EN EL CALENDAR)
    public static final int TIPO_EVENTO_CITA = 1;
    public static final int TIPO_EVENTO_REUNION = 2;
    public static final int TIPO_EVENTO_RECORDATORIOLLAMADA = 3;

    ////// Operaciones Externas
    public static final String tipo_operacion ="tipo_operacion";
    public static final int operacion_ADNTerminaryAgendar = 1;
    public static final int operacion_ADNdesdeAgenda = 2;
    public static final int operacion_AbrirRecordatorioIndependiente = 3;
    public static final int operacion_AbrirAlertaCita = 4;
    public static final int operacion_AbrirAlertaPostCita = 5;
    public static final String parametro_idProspecto ="parametro_idProspecto";
    public static final String parametro_idRecordatorioIndependiente ="parametro_idRecordatorioIndependiente";
    public static final String parametro_idAlertaCita ="parametro_idAlertaCita";
    public static final String parametro_idProspectoDis_ADNdesdeAgenda = "id_prospecto_dispositivo";

    // TIPOS DE ALERTAS
    public static final int alerta_recordatorio = 1;
    public static final int alerta_cita = 2;
    public static final int alerta_post_cita = 3;

    // TIPOS REUNION
    public static final int TIPO_REUNION_INTERNA = 1;
    public static final int TIPO_REUNION_EXTERNA = 2;


    // Obtener Textos de Fechas
    public static String ObtenerTextoAbreviadoDiaSemana(int diaSemana)
    {
        if (Calendar.MONDAY == diaSemana) {
            return "Lun";
        } else if (Calendar.TUESDAY == diaSemana) {
            return "Mar";
        } else if (Calendar.WEDNESDAY == diaSemana) {
            return "Mie";
        } else if (Calendar.THURSDAY == diaSemana) {
            return "Jue";
        } else if (Calendar.FRIDAY == diaSemana) {
            return "Vie";
        } else if (Calendar.SATURDAY == diaSemana) {
            return "Sab";
        } else if (Calendar.SUNDAY == diaSemana) {
            return "Dom";
        }

        return "";
    }

    public static String ObtenerTextoAbreviadoMes(int mesCita){
        if (Calendar.JANUARY == mesCita) {
            return "Ene";
        } else if (Calendar.FEBRUARY == mesCita) {
            return "Feb";
        } else if (Calendar.MARCH == mesCita) {
            return "Mar";
        } else if (Calendar.APRIL == mesCita) {
            return "Abr";
        } else if (Calendar.MAY == mesCita) {
            return "May";
        } else if (Calendar.JUNE == mesCita) {
            return "Jun";
        } else if (Calendar.JULY == mesCita) {
            return "Jul";
        }else if (Calendar.AUGUST == mesCita) {
            return "Ago";
        } else if (Calendar.SEPTEMBER == mesCita) {
            return "Set";
        } else if (Calendar.OCTOBER == mesCita) {
            return "Oct";
        } else if (Calendar.NOVEMBER == mesCita) {
            return "Nov";
        } else if (Calendar.DECEMBER == mesCita) {
            return "Dic";
        }
        return "";
    }

    public static String ObtenerTextoMes(int mesCita){
        if (Calendar.JANUARY == mesCita) {
            return "Enero";
        } else if (Calendar.FEBRUARY == mesCita) {
            return "Febrero";
        } else if (Calendar.MARCH == mesCita) {
            return "Marzo";
        } else if (Calendar.APRIL == mesCita) {
            return "Abril";
        } else if (Calendar.MAY == mesCita) {
            return "Mayo";
        } else if (Calendar.JUNE == mesCita) {
            return "Junio";
        } else if (Calendar.JULY == mesCita) {
            return "Julio";
        }else if (Calendar.AUGUST == mesCita) {
            return "Agosto";
        } else if (Calendar.SEPTEMBER == mesCita) {
            return "Setiembre";
        } else if (Calendar.OCTOBER == mesCita) {
            return "Octubre";
        } else if (Calendar.NOVEMBER == mesCita) {
            return "Noviembre";
        } else if (Calendar.DECEMBER == mesCita) {
            return "Dicciembre";
        }
        return "";
    }

    // TIPOS BOTONES NOTIFICACION
    public static final int boton_dismiss = 1;
    public static final int boton_recordatorio = 2;
    public static final int boton_alerta_cita = 3;
    public static final int boton_alerta_post_cita = 4;

    // EXTRAS NOTIFICACION
    public static final String extra_not_tipoBoton = "tipoBoton";
    public static final String extra_not_id = "notificationId";
    public static final String extra_not_idFuente = "notificationIdFuente";
    public static final String extra_not_tipoFuente = "notificationTipoFuente";


    // OPERACIONES SINCRO ENTREVISTAFRAGMENT
    public static final int entrevista_sincro_siguienteetapa = 1;


    // OPERACIONES SINCRO CALENDARIOACTIVITY
    public static final int dialog_sincro_siguienteetapa = 1;
    public static final int dialog_sincro_descartar = 2;

}
