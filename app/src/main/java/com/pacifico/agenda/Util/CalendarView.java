package com.pacifico.agenda.Util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.R;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by DSB on 14/06/2016.
 */
public class CalendarView {

    private static final String NEW = "NEW";
    private static int ROW_DIM;
    private static int ROW_BIRTHDAY_DIM;
    private static int COL_HOUR_DIM = 65;
    private static int COL_DATE_DIM = 135;

    ProspectoBean cumple = new ProspectoBean();
    public View view;
    public GridLayout myGridLayout, gridWeekly;
    public GridLayout gridBirthday;
    private int columnCount;
    private int rowCount;
    public LinearLayout.LayoutParams paramsDate, paramsHour;
    String[] displayDays = new String[7];
    String[] monthNames = new String[12];
    //private ArrayList<View> citas = new ArrayList<View>();
    private ArrayList<View> citasAgrupadas = new ArrayList<View>();
    private ArrayList<View> cumples = new ArrayList<View>();
    int rowSize;
    public final int altoceldacalendario = 100;
    int columDateSize;
    int columHourSize;
    private DateTime calendar;
    Context context;
    Caretaker caretaker;
    Originator originator;
    ScrollView scrollCalendario;

    private View lineaHoraView;

    public CalendarView(Context context, int columnCount, int rowCount, int rowCountView) {
        this.context = context;
        view = (View) LayoutInflater.from(context).inflate(R.layout.calendar_view, null);
        gridWeekly = (GridLayout) view.findViewById(R.id.gridWeekly);
        gridBirthday = (GridLayout) view.findViewById(R.id.gridBirthday);
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int barNotificationHight = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0)
            barNotificationHight = context.getResources().getDimensionPixelSize(resourceId);
        DateFormatSymbols dfs = DateFormatSymbols.getInstance(new Locale("es"));
        monthNames = dfs.getShortMonths();
        for (int i = 1; i <= 7; i++) displayDays[i - 1] = dfs.getWeekdays()[i % 7 + 1];
        columHourSize = dpToPx(COL_HOUR_DIM);
        columDateSize = dpToPx(COL_DATE_DIM);
        rowSize = Math.round((size.y - barNotificationHight) / rowCountView);
        ROW_BIRTHDAY_DIM = (int) Math.floor(rowSize * 0.75);
        calendar = DateTime.now().withDayOfWeek(1);
        paramsDate = new LinearLayout.LayoutParams(columDateSize, altoceldacalendario);
        paramsHour = new LinearLayout.LayoutParams(columHourSize, altoceldacalendario);
        this.columnCount = columnCount;
        this.rowCount = rowCount;
        createGridLayout();
        createWeek();
        createBirthdays();
//        createHours(0, 23);
        originator = new Originator();
        caretaker = new Caretaker();
        save(NEW);
    }


    private void createGridLayout() {
        myGridLayout = new GridLayout(context);
        myGridLayout.removeAllViews();
        myGridLayout.setColumnCount(columnCount);
        myGridLayout.setRowCount(rowCount);
        myGridLayout.setBackgroundColor(context.getResources().getColor(R.color.blanco));


        for (int i = 0; i < rowCount; i++)
            for (int j = 0; j < columnCount; j++) {
                LinearLayout blank_linear = (LinearLayout) LayoutInflater.from(context)
                        .inflate((j == 0) ? R.layout.celda_horas : R.layout.celda_dias, null);
                blank_linear.setGravity(Gravity.NO_GRAVITY);
                if (j != 0) {
                    blank_linear.setLayoutParams(paramsDate);
                    myGridLayout.addView(blank_linear);
                } else {
                    ((TextView) blank_linear.findViewById(R.id.txtHoras)).setText((i < 12) ? String.valueOf(i) + ":00 am" :
                            String.valueOf((i) % 12) + ":00 pm");

                    if (i == 12)
                    ((TextView) blank_linear.findViewById(R.id.txtHoras)).setText("12:00 pm");

                    blank_linear.findViewById(R.id.txtHoras).getLayoutParams().height = altoceldacalendario;
                    blank_linear.findViewById(R.id.txtHoras).getLayoutParams().width = columHourSize;
                    blank_linear.setLayoutParams(paramsHour);
                    myGridLayout.addView(blank_linear, new GridLayout.LayoutParams(
                            GridLayout.spec(i), GridLayout.spec(0)));
                }
            }

        scrollCalendario = ((ScrollView) view.findViewById(R.id.scrollData));
        scrollCalendario.removeAllViews();
        scrollCalendario.addView(myGridLayout);

    }

    private void createBirthdays() {
        gridBirthday.removeAllViews();
        gridBirthday.setRowCount(1);
        gridBirthday.setColumnCount(columnCount);
        View v = LayoutInflater.from(context).inflate(R.layout.celda_horas, null);
        v.setLayoutParams(paramsHour);
        v.getLayoutParams().height = ROW_BIRTHDAY_DIM;
        gridBirthday.addView(v);
        for (int i = 0; i < columnCount - 1; i++) {
            LinearLayout ll = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.celda_dias, null);
            ll.setLayoutParams(paramsDate);
            ll.getLayoutParams().height = ROW_BIRTHDAY_DIM;
            gridBirthday.addView(ll);
            gridBirthday.setVisibility(View.VISIBLE);
        }

    }

//    private void createHours(int min, int max) {
//        for (int i = min; i <= max; i++) {
//            View v = LayoutInflater.from(context).inflate(R.layout.celda_horas, null);
//            ((TextView) v.findViewById(R.id.txtHoras)).setText((i <= 12) ? String.valueOf(i) + ":00 am" :
//                    String.valueOf((i) % 12) + ":00 pm");
//            v.findViewById(R.id.txtHoras).getLayoutParams().height = rowSize;
//            v.findViewById(R.id.txtHoras).getLayoutParams().width = columHourSize;
//            v.setLayoutParams(paramsHour);
//            myGridLayout.addView(v, new GridLayout.LayoutParams(
//                    GridLayout.spec(i), GridLayout.spec(0)));
//        }
//    }

    /*public void addCell(CeldaView celdaDia, int x, int y, int beginQuarter, int countQuarter) {
        citas.add(celdaDia.view);
        //int quater = Math.round((altoceldacalendario / 60));
        double quater = altoceldacalendario / 60.0;
        celdaDia.spaceBefore.getLayoutParams().height = (int)Math.round(quater * beginQuarter);
        GridLayout.LayoutParams param = new GridLayout.LayoutParams();
        param.height = (int)Math.round(quater * (countQuarter + beginQuarter));
        param.width = columDateSize;
        param.setGravity(Gravity.NO_GRAVITY);
        param.columnSpec = GridLayout.spec((x), 0);
        param.rowSpec = GridLayout.spec((y), 0);
        celdaDia.view.setLayoutParams(param);
        myGridLayout.addView(celdaDia.view);
    }*/

    public void addLineaHora(LineaHoraCalendario linea, int y, int minutos)
    {
        lineaHoraView = linea.view;

        double altminuto = altoceldacalendario / 60.0;
        linea.spaceBefore.getLayoutParams().height = (int)Math.round(altminuto * minutos);;

        GridLayout.LayoutParams param = new GridLayout.LayoutParams();
        param.height = (int)Math.round(altminuto * (2 + minutos));;
        param.width = columDateSize*7;
        param.setGravity(Gravity.NO_GRAVITY);
        param.columnSpec = GridLayout.spec((1), 0);
        param.rowSpec = GridLayout.spec((y), 0);
        linea.view.setLayoutParams(param);
        myGridLayout.addView(linea.view);
    }

    public void addCell(View p_view, int x, int y,int altura) {
        citasAgrupadas.add(p_view);
        GridLayout.LayoutParams param = new GridLayout.LayoutParams();
        param.height = altura;
        param.width = columDateSize;
        param.setGravity(Gravity.NO_GRAVITY);
        param.columnSpec = GridLayout.spec((x), 0);
        param.rowSpec = GridLayout.spec((y), 0);
        p_view.setLayoutParams(param);
        myGridLayout.addView(p_view);
    }

    public void addBirthday(CeldaBirthday celdaCumplenos, int y) {
        cumples.add(celdaCumplenos.view);
        GridLayout.LayoutParams param = new GridLayout.LayoutParams();
        param.height = ROW_BIRTHDAY_DIM;
        param.width = columDateSize;
        param.columnSpec = GridLayout.spec((y), 0);
        param.rowSpec = GridLayout.spec((0), 0);
        celdaCumplenos.view.setLayoutParams(param);
        gridBirthday.setVisibility(View.VISIBLE);
        gridBirthday.addView(celdaCumplenos.view);
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    private void createWeek() {

        gridWeekly.removeAllViews();
        gridWeekly.setRowCount(1);
        gridWeekly.setColumnCount(columnCount);
        View v = LayoutInflater.from(context).inflate(R.layout.celda_horas, null);
        v.setLayoutParams(paramsHour);
        v.getLayoutParams().height = ROW_BIRTHDAY_DIM;
        gridWeekly.addView(v);

        for (int i = 1; i <= columnCount - 1; i++) {
            v = LayoutInflater.from(context).inflate(R.layout.celda_dias, null);
            String txtoDia = displayDays[calendar.getDayOfWeek() - 1];
            ((TextView) v.findViewById(R.id.txtTxtoDia)).setText(Util.capitalizedString(txtoDia));
            ((TextView) v.findViewById(R.id.txtNumDia)).setText(String.valueOf(calendar.getDayOfMonth()));
            ((TextView) v.findViewById(R.id.txtMes)).setText(monthNames[calendar.getMonthOfYear() - 1]);
            calendar = calendar.plusDays(1);
            v.setLayoutParams(paramsDate);
            v.getLayoutParams().height = ROW_BIRTHDAY_DIM;
            gridWeekly.addView(v);
        }
        calendar = calendar.minusWeeks(1);
    }

    public void nextWeek() {
        String hashSave = "" + calendar.getYear() + calendar.getMonthOfYear() + calendar.getDayOfMonth();
        calendar = calendar.plus(Period.weeks(1)).withDayOfWeek(1);
        createWeek();
        Log.i("TAG","Dias"+calendar);
//        String hash = NEW;
//        if (caretaker.exist(hash)) {
//            originator.restoreFromMementoGrid(caretaker.getMemento(hash));
//            myGridLayout = (GridLayout) originator.getMainGrid();
//            gridBirthday = (GridLayout) originator.getStateMainBirth();
        gridBirthday.setVisibility(View.VISIBLE);
        for (View view : cumples)
            gridBirthday.removeView(view);
        for (View view : citasAgrupadas)
            myGridLayout.removeView(view);

        myGridLayout.removeView(lineaHoraView);
//      }

        //CargarCitas
    }

    public void beforeWeek() {
        String hashSave = "" + calendar.getYear() + calendar.getMonthOfYear() + calendar.getDayOfMonth();
        calendar = calendar.minus(Period.weeks(1)).withDayOfWeek(1);
        createWeek();
        Log.i("TAG","Dias"+calendar);
//        String hash = NEW;
//        if (caretaker.exist(hash)) {
//            originator.restoreFromMementoGrid(caretaker.getMemento(hash));
//            myGridLayout = (GridLayout) originator.getMainGrid();
//            gridBirthday = (GridLayout) originator.getStateMainBirth();
        gridBirthday.setVisibility(View.VISIBLE);
        for (View view : cumples)
            gridBirthday.removeView(view);
        for (View view : citasAgrupadas)
            myGridLayout.removeView(view);

        myGridLayout.removeView(lineaHoraView);
//      }

    }

    public void save() {
        String hashSave = "" + calendar.getYear() + calendar.getMonthOfYear() + calendar.getDayOfMonth();
        if (!caretaker.exist(hashSave)) {
            originator.setMainGrid(myGridLayout);
            originator.setStateMainBirth(gridBirthday);
            originator.setStateMainAll(view);
            caretaker.addMemento(originator.saveToMementoGrid(), hashSave);
        }
    }

    public void save(String hashSave) {
        if (!caretaker.exist(hashSave)) {
            originator.setMainGrid(myGridLayout);
            originator.setStateMainBirth(gridBirthday);
            originator.setStateMainAll(view);
            caretaker.addMemento(originator.saveToMementoGrid(), hashSave);
        }
    }

    public void setWeek(int day, int month, int year) {
        String hashBeafore = "" + calendar.getYear() + calendar.getMonthOfYear() + calendar.getDayOfMonth();
        calendar = calendar.withYear(year).withMonthOfYear(month + 1).withDayOfMonth(day).withDayOfWeek(1);
        String hashNow = "" + calendar.getYear() + calendar.getMonthOfYear() + calendar.getDayOfMonth();
        if (hashBeafore.equals(hashNow)) return;
        createWeek();
//        String hash = NEW;
//        if (caretaker.exist(hash)) {
//            originator.restoreFromMementoGrid(caretaker.getMemento(hash));
//            myGridLayout = (GridLayout) originator.getMainGrid();
//            gridBirthday = (GridLayout) originator.getStateMainBirth();
        gridBirthday.setVisibility(View.VISIBLE);
        for (View view : cumples)
            gridBirthday.removeView(view);
        for (View view : citasAgrupadas)
            myGridLayout.removeView(view);

        myGridLayout.removeView(lineaHoraView);
//      }
    }

    LinearLayout linearLayout;

    public void attach(LinearLayout linearLayout) {
        this.linearLayout = linearLayout;
        this.linearLayout.addView(view);
    }

    View.OnTouchListener gridLayoutTouchListener;

    public void addListener(View.OnTouchListener gridLayoutTouchListener) {
        this.gridLayoutTouchListener = gridLayoutTouchListener;
        this.gridWeekly.setOnTouchListener(gridLayoutTouchListener);
        this.myGridLayout.setOnTouchListener(gridLayoutTouchListener);
        this.gridBirthday.setOnTouchListener(gridLayoutTouchListener);
        //this.scrollCalendario.getParent().requestDisallowInterceptTouchEvent(true);
        //this.scrollCalendario.setOnTouchListener(gridLayoutTouchListener);
        //this.view.setOnTouchListener(gridLayoutTouchListener);
    }

    /*public void addListeners() {
        gridWeekly.setOnTouchListener(gridLayoutTouchListener);
        myGridLayout.setOnTouchListener(gridLayoutTouchListener);
        gridBirthday.setOnTouchListener(gridLayoutTouchListener);
    }*/

    private class Memento {
        private View stateGrid;
        private View stateBirth;
        private View stateAll;

        public Memento(View stateToSave, View stateToSaveBirth, View stateToSaveAll) {
            stateGrid = stateToSave;
            stateBirth = stateToSaveBirth;
            stateAll = stateToSaveAll;
        }

        public View getSavedStateGrid() {
            return stateGrid;
        }

        public View getSavedStateBirth() {
            return stateBirth;
        }

        public View getSavedStateAll() {
            return stateAll;
        }

    }

    class Originator {
        private View stateMainGrid;
        private View stateMainBirth;
        private View stateMainAll;

        public void setMainGrid(View state) {
            this.stateMainGrid = state;
        }

        public View getMainGrid() {
            return this.stateMainGrid;
        }

        public View getStateMainBirth() {
            return stateMainBirth;
        }

        public void setStateMainBirth(View stateMainBirth) {
            this.stateMainBirth = stateMainBirth;
        }

        public View getStateMainAll() {
            return stateMainAll;
        }

        public void setStateMainAll(View stateMainAll) {
            this.stateMainAll = stateMainAll;
        }

        public Memento saveToMementoGrid() {
            return new Memento(stateMainGrid, stateMainBirth, stateMainAll);
        }

        public void restoreFromMementoGrid(Memento m) {
            stateMainGrid = m.getSavedStateGrid();
            stateMainBirth = m.getSavedStateBirth();
            stateMainAll = m.getSavedStateAll();
        }
    }

    class Caretaker {
        private HashMap<String, Memento> savedStates = new HashMap<String, Memento>();

        public boolean exist(String hash) {
            return savedStates.containsKey(hash) ? true : false;
        }

        public void addMemento(Memento m, String hash) {
            savedStates.put(hash, m);
        }

        public Memento getMemento(String hash) {
            return savedStates.get(hash);
        }
    }

    public ArrayList<View> getCumples() {
        return cumples;
    }

    public void setCumples(ArrayList<View> cumples) {
        this.cumples = cumples;
    }

    public GridLayout getGridWeekly() {
        return gridWeekly;
    }

    public void setGridWeekly(GridLayout gridWeekly) {
        this.gridWeekly = gridWeekly;
    }

    public GridLayout getGridBirthday() {
        return gridBirthday;
    }

    public void setGridBirthday(GridLayout gridBirthday) {
        this.gridBirthday = gridBirthday;
    }

    public GridLayout getMyGridLayout() {
        return myGridLayout;
    }

    public void setMyGridLayout(GridLayout myGridLayout) {
        this.myGridLayout = myGridLayout;
    }

    public void limpiarCitas()
    {
        for (View view : citasAgrupadas)
            myGridLayout.removeView(view);
    }

    public void limpiarCumples()
    {
        for (View view : cumples)
            gridBirthday.removeView(view);
        //gridBirthday.removeAllViews();
    }

    public void borrarLineaHora(){
        if (lineaHoraView != null)
            myGridLayout.removeView(lineaHoraView);
    }

    public DateTime getCalendar() {
        return calendar;
    }

    public void setCalendar(DateTime calendar) {
        this.calendar = calendar;
    }

}
