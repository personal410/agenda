package com.pacifico.agenda.Util;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import com.pacifico.agenda.R;
import com.pacifico.agenda.Views.CustomView.setFuente;

/**
 * Created by DSB on 14/06/2016.
 */
public class CeldaBirthday extends CeldaView {

    public ImageView image;
    public TextView txt;

    public CeldaBirthday(Context context){
        view = LayoutInflater.from(context).inflate(R.layout.celda_cumple, null);
        linHorizontal = (LinearLayout) view.findViewById(R.id.linHorizontal);
        spaceBefore = (Space)view.findViewById(R.id.spaceBefore);
        txt = (TextView)view.findViewById(R.id.txt);
        txt.setPadding(5,0,0,0);
        setFuente.setFuenteRg(context,txt);
        txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        txt.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        image= (ImageView)view.findViewById(R.id.image);
        image.setImageDrawable(context.getResources().getDrawable(R.drawable.regalo));
    }


}
