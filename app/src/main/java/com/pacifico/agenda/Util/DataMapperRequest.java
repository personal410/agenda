package com.pacifico.agenda.Util;

import com.pacifico.agenda.Model.Bean.AdnBean;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.agenda.Model.Bean.FamiliarBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Model.Bean.ReferidoBean;
import com.pacifico.agenda.Model.Bean.ReunionInternaBean;
import com.pacifico.agenda.Model.Controller.ADNController;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.Model.Controller.FamiliarController;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.Model.Controller.ReferidoController;
import com.pacifico.agenda.Network.Request.SetData.ADNRequest;
import com.pacifico.agenda.Network.Request.SetData.CitaMovimientoEstadoRequest;
import com.pacifico.agenda.Network.Request.SetData.CitaRequest;
import com.pacifico.agenda.Network.Request.SetData.FamiliarRequest;
import com.pacifico.agenda.Network.Request.SetData.ProspectoMovimientoEtapaRequest;
import com.pacifico.agenda.Network.Request.SetData.ProspectoRequest;
import com.pacifico.agenda.Network.Request.SetData.RecordatorioLLamadaRequest;
import com.pacifico.agenda.Network.Request.SetData.ReferidoRequest;
import com.pacifico.agenda.Network.Request.SetData.ReunionInternaRequest;

import java.util.ArrayList;

/**
 * Created by vctrls3477 on 8/06/16.
 */
public class DataMapperRequest{
    public static ReunionInternaRequest transform(ReunionInternaBean reunionInternaBean) {
        ReunionInternaRequest reunionRequest = new ReunionInternaRequest();
        int idReunion = reunionInternaBean.getIdReunionInterna();
        if(idReunion == -1){
            reunionRequest.setIdReunion(null);
        }else {
            reunionRequest.setIdReunion(String.valueOf(idReunion));
        }
        int idReunionDispositivo = reunionInternaBean.getIdReunionInternaDispositivo();
        if(idReunionDispositivo == -1){
            reunionRequest.setIdReunionDispositivo(null);
        }else {
            reunionRequest.setIdReunionDispositivo(String.valueOf(reunionInternaBean.getIdReunionInternaDispositivo()));
        }
        reunionRequest.setFechaReunion(reunionInternaBean.getFechaReunion());
        reunionRequest.setHoraInicio(reunionInternaBean.getHoraInicio());
        reunionRequest.setHoraFin(reunionInternaBean.getHoraFin());
        reunionRequest.setUbicacion(reunionInternaBean.getUbicacion());
        int flagInvitadoGU = reunionInternaBean.getFlagInvitadoGU();
        if(flagInvitadoGU == -1){
            reunionRequest.setFlagInvitadoGU(null);
        }else {
            reunionRequest.setFlagInvitadoGU(String.valueOf(reunionInternaBean.getFlagInvitadoGU()));
        }
        int flagInvitadoGA = reunionInternaBean.getFlagInvitadoGA();
        if(flagInvitadoGA == -1){
            reunionRequest.setFlagInvitadoGA(null);
        }else {
            reunionRequest.setFlagInvitadoGA(String.valueOf(reunionInternaBean.getFlagInvitadoGA()));
        }
        reunionRequest.setRecordatorio(reunionInternaBean.getRecordatorio());
        int codigoTipoReunion = reunionInternaBean.getCodigoTipoReunion();
        if(codigoTipoReunion == -1){
            reunionRequest.setCodigoTipoReunion(null);
        }else {
            reunionRequest.setCodigoTipoReunion(String.valueOf(reunionInternaBean.getCodigoTipoReunion()));
        }
        reunionRequest.setRecordatorio(reunionInternaBean.getRecordatorio());
        reunionRequest.setFechaCreacionDispositivo(reunionInternaBean.getFechaCreacionDispositivo());
        reunionRequest.setFechaModificacionDispositivo(reunionInternaBean.getFechaModificacionDispositivo());
        return reunionRequest;
    }

    public static ProspectoRequest transform(ProspectoBean prospectoBean){
        ProspectoRequest prospectoRequest = new ProspectoRequest();
        int idProspecto = prospectoBean.getIdProspecto();
        if (idProspecto == -1){
            prospectoRequest.setIdProspecto(null);
        }else {
            prospectoRequest.setIdProspecto(Integer.toString(idProspecto));
        }
        prospectoRequest.setIdProspectoDispositivo(Integer.toString(prospectoBean.getIdProspectoDispositivo()));
        prospectoRequest.setNombres(prospectoBean.getNombres());
        prospectoRequest.setApellidoPaterno(prospectoBean.getApellidoPaterno());
        prospectoRequest.setApellidoMaterno(prospectoBean.getApellidoMaterno());
        int codigoSexo = prospectoBean.getCodigoSexo();
        if(codigoSexo == -1){
            prospectoRequest.setCodigoSexo(null);
        }else{
            prospectoRequest.setCodigoSexo(Integer.toString(codigoSexo));
        }
        int codigoEstadoCivil = prospectoBean.getCodigoEstadoCivil();
        if(codigoEstadoCivil == -1){
            prospectoRequest.setCodigoEstadoCivil(null);
        }else{
            prospectoRequest.setCodigoEstadoCivil(Integer.toString(codigoEstadoCivil));
        }
        prospectoRequest.setFechaNacimiento(prospectoBean.getFechaNacimiento());
        int codigoRangoEdad = prospectoBean.getCodigoRangoEdad();
        if(codigoRangoEdad == -1){
            prospectoRequest.setCodigoRangoEdad(null);
        }else{
            prospectoRequest.setCodigoRangoEdad(Integer.toString(codigoRangoEdad));
        }
        int codigoRangoIngreso = prospectoBean.getCodigoRangoIngreso();
        if(codigoRangoIngreso == -1){
            prospectoRequest.setCodigoRangoIngreso(null);
        }else{
            prospectoRequest.setCodigoRangoIngreso(Integer.toString(codigoRangoIngreso));
        }
        int flagHijo = prospectoBean.getFlagHijo();
        if(flagHijo == -1){
            prospectoRequest.setFlagHijo(null);
        }else{
            prospectoRequest.setFlagHijo(Integer.toString(flagHijo));
        }
        int flagConyuge = prospectoBean.getFlagConyuge();
        if (flagConyuge == -1){
            prospectoRequest.setFlagConyuge(null);
        }else{
            prospectoRequest.setFlagConyuge(Integer.toString(flagConyuge));
        }
        int codigoTipoDocumento = prospectoBean.getCodigoTipoDocumento();
        if(codigoTipoDocumento == -1){
            prospectoRequest.setCodigoTipoDocumento(null);
        }else{
            prospectoRequest.setCodigoTipoDocumento(Integer.toString(codigoTipoDocumento));
        }
        prospectoRequest.setNumeroDocumento(prospectoBean.getNumeroDocumento());
        prospectoRequest.setTelefonoCelular(prospectoBean.getTelefonoCelular());
        prospectoRequest.setTelefonoFijo(prospectoBean.getTelefonoFijo());
        prospectoRequest.setTelefonoAdicional(prospectoBean.getTelefonoAdicional());
        prospectoRequest.setCorreoElectronico1(prospectoBean.getCorreoElectronico1());
        prospectoRequest.setCorreoElectronico2(prospectoBean.getCorreoElectronico2());
        int codigoNacionalidad = prospectoBean.getCodigoNacionalidad();
        if(codigoNacionalidad == -1){
            prospectoRequest.setCodigoNacionalidad(null);
        }else{
            prospectoRequest.setCodigoNacionalidad(Integer.toString(codigoNacionalidad));
        }
        int condicionFumador = prospectoBean.getCondicionFumador();
        if (condicionFumador == -1){
            prospectoRequest.setCondicionFumador(null);
        }else {
            prospectoRequest.setCondicionFumador(Integer.toString(condicionFumador));
        }
        prospectoRequest.setEmpresa(prospectoBean.getEmpresa());
        prospectoRequest.setCargo(prospectoBean.getCargo());
        prospectoRequest.setNota(prospectoBean.getNota());
        prospectoRequest.setFlagEnviarADNDigital(Integer.toString(prospectoBean.getFlagEnviarADNDigital()));
        int codigoEtapa = prospectoBean.getCodigoEtapa();
        if(codigoEtapa == -1){
            prospectoRequest.setCodigoEtapa(null);
        }else{
            prospectoRequest.setCodigoEtapa(Integer.toString(codigoEtapa));
        }
        int codigoEstado = prospectoBean.getCodigoEstado();
        if (codigoEstado == -1){
            prospectoRequest.setCodigoEstado(null);
        }else{
            prospectoRequest.setCodigoEstado(Integer.toString(codigoEstado));
        }
        int codigoFuente = prospectoBean.getCodigoFuente();
        if (codigoFuente == -1){
            prospectoRequest.setCodigoFuente(null);
        }else {
            prospectoRequest.setCodigoFuente(Integer.toString(codigoFuente));
        }
        int codigoEntorno = prospectoBean.getCodigoEntorno();
        if (codigoEntorno == -1){
            prospectoRequest.setCodigoEntorno(null);
        }else{
            prospectoRequest.setCodigoEntorno(Integer.toString(codigoEntorno));
        }
        int idReferenciador = prospectoBean.getIdReferenciador();
        if(idReferenciador == -1){
            prospectoRequest.setIdReferenciador(null);
        }else{
            prospectoRequest.setIdReferenciador(Integer.toString(idReferenciador));
        }
        prospectoRequest.setFechaCreacionDispositivo(prospectoBean.getFechaCreacionDispositivo());
        prospectoRequest.setFechaModificacionDispositivo(prospectoBean.getFechaModificacionDispositivo());
        prospectoRequest.setAdicionalTexto1(prospectoBean.getAdicionalTexto1());
        double adicionalNumerico1 = prospectoBean.getAdicionalNumerico1();
        if (adicionalNumerico1 == -1){
            prospectoRequest.setAdicionalNumerico1(null);
        }else{
            prospectoRequest.setAdicionalNumerico1(Util.obtenerNumeroConDecimales(adicionalNumerico1));
        }
        prospectoRequest.setAdicionalTexto2(prospectoBean.getAdicionalTexto2());
        double adicionalNumerico2 = prospectoBean.getAdicionalNumerico2();
        if (adicionalNumerico2 == -1){
            prospectoRequest.setAdicionalNumerico2(null);
        }else{
            prospectoRequest.setAdicionalNumerico2(Util.obtenerNumeroConDecimales(adicionalNumerico2));
        }
        return prospectoRequest;
    }
    public static ProspectoMovimientoEtapaRequest transform(ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean) {
        ProspectoMovimientoEtapaRequest prospectoMovimientoEtapaRequest = new ProspectoMovimientoEtapaRequest();

        int idMovimiento = prospectoMovimientoEtapaBean.getIdMovimiento();
        if(idMovimiento == -1){
            prospectoMovimientoEtapaRequest.setIdMovimiento(null);
        }else {
            prospectoMovimientoEtapaRequest.setIdMovimiento(String.valueOf(prospectoMovimientoEtapaBean.getIdMovimiento()));
        }

        int idProspecto = prospectoMovimientoEtapaBean.getIdProspecto();
        if(idProspecto == -1){
            prospectoMovimientoEtapaRequest.setIdProspecto(null);
        }else {
            prospectoMovimientoEtapaRequest.setIdProspecto(String.valueOf(prospectoMovimientoEtapaBean.getIdProspecto()));
        }

        int idProspectoDispositivo = prospectoMovimientoEtapaBean.getIdProspectoDispositivo();
        if(idProspectoDispositivo == -1){
            prospectoMovimientoEtapaRequest.setIdProspectoDispositivo(null);
        }else {
            prospectoMovimientoEtapaRequest.setIdProspectoDispositivo(String.valueOf(prospectoMovimientoEtapaBean.getIdProspectoDispositivo()));
        }

        int idMovimientoDispositivo = prospectoMovimientoEtapaBean.getIdMovimientoDispositivo();
        if(idMovimientoDispositivo == -1){
            prospectoMovimientoEtapaRequest.setIdMovimientoDispositivo(null);
        }else {
            prospectoMovimientoEtapaRequest.setIdMovimientoDispositivo(String.valueOf(prospectoMovimientoEtapaBean.getIdMovimientoDispositivo()));
        }

        int codigoEtapa = prospectoMovimientoEtapaBean.getCodigoEtapa();
        if(codigoEtapa == -1){
            prospectoMovimientoEtapaRequest.setCodigoEtapa(null);
        }else {
            prospectoMovimientoEtapaRequest.setCodigoEtapa(String.valueOf(prospectoMovimientoEtapaBean.getCodigoEtapa()));
        }

        int codigoEstado = prospectoMovimientoEtapaBean.getCodigoEstado();
        if(codigoEstado == -1){
            prospectoMovimientoEtapaRequest.setCodigoEstado(null);
        }else {
            prospectoMovimientoEtapaRequest.setCodigoEstado(String.valueOf(prospectoMovimientoEtapaBean.getCodigoEstado()));
        }

        prospectoMovimientoEtapaRequest.setFechaMovimientoEtapaDispositivo(prospectoMovimientoEtapaBean.getFechaMovimientoEtapaDispositivo());

        return prospectoMovimientoEtapaRequest;
    }
    public static ADNRequest transform(AdnBean adnBean){
        ADNRequest adnRequest = new ADNRequest();
        int idProspecto = adnBean.getIdProspecto();
        if(idProspecto == -1){
            adnRequest.setIdProspecto(null);
        }else{
            adnRequest.setIdProspecto(Integer.toString(idProspecto));
        }
        adnRequest.setTipoCambio(Util.obtenerNumeroConDecimales(adnBean.getTipoCambio()));
        adnRequest.setMonedaEfectivoAhorros(Integer.toString(adnBean.getMonedaEfectivoAhorros()));
        adnRequest.setEfectivoAhorros(Integer.toString(adnBean.getEfectivoAhorros()));
        adnRequest.setMonedaPropiedades(Integer.toString(adnBean.getMonedaPropiedades()));
        adnRequest.setPropiedades(Integer.toString(adnBean.getPropiedades()));
        adnRequest.setMonedaVehiculos(Integer.toString(adnBean.getMonedaVehiculos()));
        adnRequest.setVehiculos(Integer.toString(adnBean.getVehiculos()));
        adnRequest.setMonedaTotalActivoRealizable(Integer.toString(adnBean.getMonedaTotalActivoRealizable()));
        adnRequest.setTotalActivoRealizable(Util.obtenerNumeroConDecimales(adnBean.getTotalActivoRealizable()));
        adnRequest.setMonedaSeguroIndividual(Integer.toString(adnBean.getMonedaSeguroIndividual()));
        adnRequest.setSeguroIndividual(Integer.toString(adnBean.getSeguroIndividual()));
        adnRequest.setMonedaIngresoBrutoMensualVidaLey(Integer.toString(adnBean.getMonedaIngresoBrutoMensualVidaLey()));
        adnRequest.setIngresoBrutoMensualVidaLey(Integer.toString(adnBean.getIngresoBrutoMensualVidaLey()));
        adnRequest.setFactorVidaLey(Integer.toString(adnBean.getFactorVidaLey()));
        adnRequest.setMonedaTopeVidaLey(Integer.toString(adnBean.getMonedaTopeVidaLey()));
        adnRequest.setTopeVidaLey(Util.obtenerNumeroConDecimales(adnBean.getTopeVidaLey()));
        adnRequest.setMonedaSeguroVidaLey(Integer.toString(adnBean.getMonedaSeguroVidaLey()));
        adnRequest.setSeguroVidaLey(Util.obtenerNumeroConDecimales(adnBean.getSeguroVidaLey()));
        adnRequest.setFlagCuentaConVidaLey(Integer.toString(adnBean.getFlagCuentaConVidaLey()));
        adnRequest.setMonedaTotalSegurosVida(Integer.toString(adnBean.getMonedaTotalSegurosVida()));
        adnRequest.setTotalSegurosVida(Util.obtenerNumeroConDecimales(adnBean.getTotalSegurosVida()));
        adnRequest.setPorcentajeAFPConyuge(Util.obtenerNumeroConDecimales(adnBean.getPorcentajeAFPConyuge()));
        adnRequest.setPorcentajeAFPHijos(Util.obtenerNumeroConDecimales(adnBean.getPorcentajeAFPHijos()));
        adnRequest.setNumeroHijos(Integer.toString(adnBean.getNumeroHijos()));
        adnRequest.setMonedaPensionConyuge(Integer.toString(adnBean.getMonedaPensionConyuge()));
        adnRequest.setPensionConyuge(Util.obtenerNumeroConDecimales(adnBean.getPensionConyuge()));
        adnRequest.setMonedaPensionHijos(Integer.toString(adnBean.getMonedaPensionHijos()));
        adnRequest.setPensionHijos(Util.obtenerNumeroConDecimales(adnBean.getPensionHijos()));
        adnRequest.setFlagPensionConyuge(Integer.toString(adnBean.getFlagPensionConyuge()));
        adnRequest.setFlagPensionHijos(Integer.toString(adnBean.getFlagPensionHijos()));
        adnRequest.setFlagPensionNoAFP(Integer.toString(adnBean.getFlagPensionNoAFP()));
        adnRequest.setMonedaTotalPensionMensualAFP(Integer.toString(adnBean.getMonedaTotalPensionMensualAFP()));
        adnRequest.setTotalPensionMensualAFP(Util.obtenerNumeroConDecimales(adnBean.getTotalPensionMensualAFP()));
        adnRequest.setMonedaIngresoMensualTitular(Integer.toString(adnBean.getMonedaIngresoMensualTitular()));
        adnRequest.setIngresoMensualTitular(Integer.toString(adnBean.getIngresoMensualTitular()));
        adnRequest.setMonedaIngresoMensualConyuge(Integer.toString(adnBean.getMonedaIngresoMensualConyuge()));
        adnRequest.setIngresoMensualConyuge(Integer.toString(adnBean.getIngresoMensualConyuge()));
        adnRequest.setMonedaIngresoFamiliarOtros(Integer.toString(adnBean.getMonedaIngresoFamiliarOtros()));
        adnRequest.setIngresoFamiliarOtros(Integer.toString(adnBean.getIngresoFamiliarOtros()));
        adnRequest.setMonedaTotalIngresoFamiliarMensual(Integer.toString(adnBean.getMonedaTotalIngresoFamiliarMensual()));
        adnRequest.setTotalIngresoFamiliarMensual(Util.obtenerNumeroConDecimales(adnBean.getTotalIngresoFamiliarMensual()));
        adnRequest.setMonedaGastoVivienda(Integer.toString(adnBean.getMonedaGastoVivienda()));
        adnRequest.setGastoVivienda(Integer.toString(adnBean.getGastoVivienda()));
        adnRequest.setMonedaGastoServicios(Integer.toString(adnBean.getMonedaGastoServicios()));
        adnRequest.setGastoServicios(Integer.toString(adnBean.getGastoServicios()));
        adnRequest.setMonedaGastoHogarAlimentacion(Integer.toString(adnBean.getMonedaGastoHogarAlimentacion()));
        adnRequest.setGastoHogarAlimentacion(Integer.toString(adnBean.getGastoHogarAlimentacion()));
        adnRequest.setMonedaGastoSalud(Integer.toString(adnBean.getMonedaGastoSalud()));
        adnRequest.setGastoSalud(Integer.toString(adnBean.getGastoSalud()));
        adnRequest.setMonedaGastoEducacion(Integer.toString(adnBean.getMonedaGastoEducacion()));
        adnRequest.setGastoEducacion(Integer.toString(adnBean.getGastoEducacion()));
        adnRequest.setMonedaGastoVehiculoTransporte(Integer.toString(adnBean.getMonedaGastoVehiculoTransporte()));
        adnRequest.setGastoVehiculoTransporte(Integer.toString(adnBean.getGastoVehiculoTransporte()));
        adnRequest.setMonedaGastoEsparcimiento(Integer.toString(adnBean.getMonedaGastoEsparcimiento()));
        adnRequest.setGastoEsparcimiento(Integer.toString(adnBean.getGastoEsparcimiento()));
        adnRequest.setMonedaGastoOtros(Integer.toString(adnBean.getMonedaGastoOtros()));
        adnRequest.setGastoOtros(Integer.toString(adnBean.getGastoOtros()));
        adnRequest.setMonedaTotalGastoFamiliarMensual(Integer.toString(adnBean.getMonedaTotalGastoFamiliarMensual()));
        adnRequest.setTotalGastoFamiliarMensual(Util.obtenerNumeroConDecimales(adnBean.getTotalGastoFamiliarMensual()));
        adnRequest.setMonedaDeficitMensual(Integer.toString(adnBean.getMonedaDeficitMensual()));
        adnRequest.setDeficitMensual(Util.obtenerNumeroConDecimales(adnBean.getDeficitMensual()));
        adnRequest.setAniosProteger(Integer.toString(adnBean.getAniosProteger()));
        adnRequest.setMonedaCapitalNecesarioFallecimiento(Integer.toString(adnBean.getMonedaCapitalNecesarioFallecimiento()));
        adnRequest.setCapitalNecesarioFallecimiento(Util.obtenerNumeroConDecimales(adnBean.getCapitalNecesarioFallecimiento()));
        adnRequest.setPorcentajeInversion(Util.obtenerNumeroConDecimales(adnBean.getPorcentajeInversion()));
        adnRequest.setMonedaMontoMensualInvertir(Integer.toString(adnBean.getMonedaMontoMensualInvertir()));
        adnRequest.setMontoMensualInvertir(Util.obtenerNumeroConDecimales(adnBean.getMontoMensualInvertir()));
        int idEntidad = adnBean.getIdEntidad();
        if(idEntidad == -1){
            adnRequest.setIdEntidad(null);
        }else{
            adnRequest.setIdEntidad(Integer.toString(idEntidad));
        }
        int codigoFrecuenciaPago = adnBean.getCodigoFrecuenciaPago();
        if(codigoFrecuenciaPago == -1){
            adnRequest.setCodigoFrecuenciaPago(null);
        }else{
            adnRequest.setCodigoFrecuenciaPago(Integer.toString(codigoFrecuenciaPago));
        }
        adnRequest.setIndicadorVentana(Integer.toString(adnBean.getIndicadorVentana()));
        adnRequest.setFlagTerminado(Integer.toString(adnBean.getFlagTerminado()));
        adnRequest.setFechaCreacionDispositivo(adnBean.getFechaCreacionDispositivo());
        adnRequest.setFechaModificacionDispositivo(adnBean.getFechaModificacionDispositivo());
        int idProspectoDispositivo = adnBean.getIdProspectoDispositivo();
        if(idProspectoDispositivo == -1){
            ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(adnBean.getIdProspecto());
            adnBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
            ADNController.actualizarADN(adnBean);
        }
        adnRequest.setIdProspectoDispositivo(Integer.toString(adnBean.getIdProspectoDispositivo()));
        adnRequest.setAdicionalTexto1(adnBean.getAdicionalTexto1());
        double adicionalNumerico1 = adnBean.getAdicionalNumerico1();
        if(adicionalNumerico1 == -1){
            adnRequest.setAdicionalNumerico1(null);
        }else{
            adnRequest.setAdicionalNumerico1(Util.obtenerNumeroConDecimales(adicionalNumerico1));
        }
        adnRequest.setAdicionalTexto2(adnBean.getAdicionalTexto2());
        double adicionalNumerico2 = adnBean.getAdicionalNumerico2();
        if(adicionalNumerico2 == -1){
            adnRequest.setAdicionalNumerico2(null);
        }else{
            adnRequest.setAdicionalNumerico2(Util.obtenerNumeroConDecimales(adicionalNumerico2));
        }
        adnRequest.setFlagADNDigital(Integer.toString(adnBean.getFlagADNDigital()));
        return adnRequest;
    }
    public static FamiliarRequest transform(FamiliarBean familiarBean){
        FamiliarRequest familiarRequest = new FamiliarRequest();
        int idFamiliar = familiarBean.getIdFamiliar();
        if (idFamiliar == -1){
            familiarRequest.setIdFamiliar(null);
        }else{
            familiarRequest.setIdFamiliar(Integer.toString(idFamiliar));
        }
        familiarRequest.setIdFamiliarDispositivo(Integer.toString(familiarBean.getIdFamiliarDispositivo()));
        int idProspecto = familiarBean.getIdProspecto();
        if(idProspecto == -1){
            familiarRequest.setIdProspecto(null);
        }else{
            familiarRequest.setIdProspecto(Integer.toString(idProspecto));
        }
        int idProspectoDispositivo = familiarBean.getIdProspectoDispositivo();
        if (idProspectoDispositivo == -1){
            ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(idProspecto);
            idProspectoDispositivo = prospectoBean.getIdProspectoDispositivo();
            familiarBean.setIdProspectoDispositivo(idProspectoDispositivo);
            FamiliarController.actualizarFamiliar(familiarBean);
        }
        familiarRequest.setIdProspectoDispositivo(Integer.toString(idProspectoDispositivo));
        familiarRequest.setNombres(familiarBean.getNombres());
        familiarRequest.setApellidoPaterno(familiarBean.getApellidoPaterno());
        familiarRequest.setApellidoMaterno(familiarBean.getApellidoMaterno());
        familiarRequest.setFechaNacimiento(familiarBean.getFechaNacimiento());
        int edad = familiarBean.getEdad();
        if (edad == -1){
            familiarRequest.setEdad(null);
        }else{
            familiarRequest.setEdad(Integer.toString(edad));
        }
        familiarRequest.setOcupacion(familiarBean.getOcupacion());
        familiarRequest.setCodigoTipoFamiliar(Integer.toString(familiarBean.getCodigoTipoFamiliar()));
        familiarRequest.setFechaCreacionDispositivo(familiarBean.getFechaCreacionDispositivo());
        familiarRequest.setFechaModificacionDispositivo(familiarBean.getFechaModificacionDispositivo());
        familiarRequest.setFlagActivo(Integer.toString(familiarBean.getFlagActivo()));
        return familiarRequest;
    }
    public static CitaRequest transform(CitaBean citaBean) {
        CitaRequest citaRequest = new CitaRequest();
        if(citaBean.getIdCita() == -1){
            citaRequest.setIdCita(null);
        }else{
            citaRequest.setIdCita(Integer.toString(citaBean.getIdCita()));
        }
        if(citaBean.getIdProspecto() == -1){
            citaRequest.setIdProspecto(null);
        }else{
            citaRequest.setIdProspecto(Integer.toString(citaBean.getIdProspecto()));
        }
        if(citaBean.getIdCitaDispositivo() == -1){
            citaRequest.setIdCitaDispositivo(null);
        }else {
            citaRequest.setIdCitaDispositivo(Integer.toString(citaBean.getIdCitaDispositivo()));
        }
        if(citaBean.getNumeroEntrevista() == -1){
            citaRequest.setNumeroEntrevista(null);
        }else{
            citaRequest.setNumeroEntrevista(Integer.toString(citaBean.getNumeroEntrevista()));
        }
        if(citaBean.getCodigoEstado() == -1){
            citaRequest.setCodigoEstado(null);
        }else{
            citaRequest.setCodigoEstado(Integer.toString(citaBean.getCodigoEstado()));
        }
        if(citaBean.getCodigoResultado() == -1){
            citaRequest.setCodigoResultado(null);
        }else{
            citaRequest.setCodigoResultado(String.valueOf(citaBean.getCodigoResultado()));
        }
        citaRequest.setFechaCita(citaBean.getFechaCita());
        citaRequest.setHoraInicio(citaBean.getHoraInicio());
        citaRequest.setHoraFin(citaBean.getHoraFin());
        citaRequest.setUbicacion(citaBean.getUbicacion());
        int flagInvitadoGU = citaBean.getFlagInvitadoGU();
        if(flagInvitadoGU == -1){
            citaRequest.setFlagInvitadoGU(null);
        }else{
            citaRequest.setFlagInvitadoGU(Integer.toString(flagInvitadoGU));
        }
        int flagInvitadoGA = citaBean.getFlagInvitadoGA();
        if(flagInvitadoGA == -1){
            citaRequest.setFlagInvitadoGA(null);
        }else{
            citaRequest.setFlagInvitadoGA(Integer.toString(flagInvitadoGA));
        }
        int alertaMinutoAntes = citaBean.getAlertaMinutosAntes();
        if(alertaMinutoAntes == -1){
            citaRequest.setAlertaMinutosAntes(null);
        }else {
            citaRequest.setAlertaMinutosAntes(String.valueOf(citaBean.getAlertaMinutosAntes()));
        }
        int codigoIntermediarioCreacion = citaBean.getCodigoIntermediarioCreacion();
        if(codigoIntermediarioCreacion == -1){
            citaRequest.setCodigoIntermediarioCreacion(null);
        }else {
            citaRequest.setCodigoIntermediarioCreacion(String.valueOf(citaBean.getCodigoIntermediarioCreacion()));
        }
        int codigoIntermediarioModificacion = citaBean.getCodigoIntermediarioModificacion();
        if(codigoIntermediarioModificacion == -1){
            citaRequest.setCodigoIntermediarioModificacion(null);
        }else {
            citaRequest.setCodigoIntermediarioModificacion(String.valueOf(citaBean.getCodigoIntermediarioModificacion()));
        }
        int codigoEtapaProspecto = citaBean.getCodigoEtapaProspecto();
        if(codigoEtapaProspecto == -1){
            citaRequest.setCodigoEtapaProspecto(null);
        }else{
            citaRequest.setCodigoEtapaProspecto(Integer.toString(codigoEtapaProspecto));
        }

        if(citaBean.getCodigoMotivoReagendado() == -1){
            citaRequest.setCodigoMotivoReagendado(null);
        }else{
            citaRequest.setCodigoMotivoReagendado(String.valueOf(citaBean.getCodigoMotivoReagendado()));
        }

        citaRequest.setFechaCreacionDispositivo(citaBean.getFechaCreacionDispositivo());
        citaRequest.setFechaModificacionDispositivo(citaBean.getFechaModificacionDispositivo());
        int idProspectoDispositivo = citaBean.getIdProspectoDispositivo();
        if(idProspectoDispositivo == -1){
            citaRequest.setIdProspectoDispositivo(null);
        }else {
            citaRequest.setIdProspectoDispositivo(String.valueOf(citaBean.getIdProspectoDispositivo()));
        }
        return citaRequest;
    }
    public static CitaMovimientoEstadoRequest transform(CitaMovimientoEstadoBean citaMovimientoEstadoBean) {
        CitaMovimientoEstadoRequest citaMovimientoEstadoRequest = new CitaMovimientoEstadoRequest();
        int idMovimiento = citaMovimientoEstadoBean.getIdMovimiento();
        if(idMovimiento == -1){
            citaMovimientoEstadoRequest.setIdMovimiento(null);
        }else {
            citaMovimientoEstadoRequest.setIdMovimiento(String.valueOf(citaMovimientoEstadoBean.getIdMovimiento()));
        }
        int idCita = citaMovimientoEstadoBean.getIdCita();
        if(idCita == -1){
            citaMovimientoEstadoRequest.setIdCita(null);
        }else {
            citaMovimientoEstadoRequest.setIdCita(String.valueOf(citaMovimientoEstadoBean.getIdCita()));
        }
        // Teamsoft indica que necesita el idProspectoDispositivo
        CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCitaDispositivo(citaMovimientoEstadoBean.getIdCitaDispositivo());
        if (citaBean != null && citaBean.getIdProspectoDispositivo() != -1){
            citaMovimientoEstadoRequest.setIdProspectoDispositivo(String.valueOf(citaBean.getIdProspectoDispositivo()));
        }else{
            citaMovimientoEstadoRequest.setIdProspectoDispositivo(null);
        }
        /////////

        int idCitaDispositivo = citaMovimientoEstadoBean.getIdCitaDispositivo();
        if(idCitaDispositivo == -1){
            citaMovimientoEstadoRequest.setIdCitaDispositivo(null);
        }else {
            citaMovimientoEstadoRequest.setIdCitaDispositivo(String.valueOf(citaMovimientoEstadoBean.getIdCitaDispositivo()));
        }

        int idMovimientoDispositivo = citaMovimientoEstadoBean.getIdMovimientoDispositivo();
        if(idMovimientoDispositivo == -1){
            citaMovimientoEstadoRequest.setIdMovimientoDispositivo(null);
        }else {
            citaMovimientoEstadoRequest.setIdMovimientoDispositivo(String.valueOf(citaMovimientoEstadoBean.getIdMovimientoDispositivo()));
        }

        int codigoEstado = citaMovimientoEstadoBean.getCodigoEstado();
        if(codigoEstado == -1){
            citaMovimientoEstadoRequest.setCodigoEstado(null);
        }else {
            citaMovimientoEstadoRequest.setCodigoEstado(String.valueOf(citaMovimientoEstadoBean.getCodigoEstado()));
        }

        int codigoResultado = citaMovimientoEstadoBean.getCodigoResultado();
        if(codigoResultado == -1){
            citaMovimientoEstadoRequest.setCodigoResultado(null);
        }else {
            citaMovimientoEstadoRequest.setCodigoResultado(String.valueOf(citaMovimientoEstadoBean.getCodigoResultado()));
        }

        citaMovimientoEstadoRequest.setFechaMovimientoEstadoDispositivo(citaMovimientoEstadoBean.getFechaMovimientoEstadoDispositivo());

        return citaMovimientoEstadoRequest;
    }

    public static ReferidoRequest transform(ReferidoBean referidoBean){
        ReferidoRequest referidoRequest = new ReferidoRequest();
        int idReferido = referidoBean.getIdReferido();
        if(idReferido == -1){
            referidoRequest.setIdReferido(null);
        }else{
            referidoRequest.setIdReferido(Integer.toString(idReferido));
        }
        referidoRequest.setIdReferidoDispositivo(Integer.toString(referidoBean.getIdReferidoDispositivo()));
        int idProspecto = referidoBean.getIdProspecto();
        if(idProspecto == -1){
            referidoRequest.setIdProspecto(null);
        }else{
            referidoRequest.setIdProspecto(Integer.toString(idProspecto));
        }
        int idProspectoDispositvo = referidoBean.getIdProspectoDispositivo();
        if(idProspectoDispositvo == -1){
            ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(referidoBean.getIdProspecto());
            idProspectoDispositvo = prospectoBean.getIdProspectoDispositivo();
            referidoBean.setIdProspectoDispositivo(idProspectoDispositvo);
            ReferidoController.actualizarReferido(referidoBean);
        }
        referidoRequest.setIdProspectoDispositivo(Integer.toString(idProspectoDispositvo));
        referidoRequest.setCodigoTipoReferido(Integer.toString(referidoBean.getCodigoTipoReferido()));
        referidoRequest.setNombres(referidoBean.getNombres());
        referidoRequest.setApellidoPaterno(referidoBean.getApellidoPaterno());
        int codigoRangoEdad = referidoBean.getCodigoRangoEdad();
        if(codigoRangoEdad == -1){
            referidoRequest.setCodigoRangoEdad(null);
        }else{
            referidoRequest.setCodigoRangoEdad(Integer.toString(codigoRangoEdad));
        }
        int flagHijo = referidoBean.getFlagHijo();
        if(flagHijo == -1){
            referidoRequest.setFlagHijo(null);
        }else{
            referidoRequest.setFlagHijo(Integer.toString(flagHijo));
        }
        int codigoRangoIngreso = referidoBean.getCodigoRangoIngreso();
        if(codigoRangoIngreso == -1){
            referidoRequest.setCodigoRangoIngreso(null);
        }else{
            referidoRequest.setCodigoRangoIngreso(Integer.toString(codigoRangoIngreso));
        }
        referidoRequest.setTelefono(referidoBean.getTelefono());
        referidoRequest.setFechaCreaContacto(referidoBean.getFechaCreaContacto());
        int codigoTipoEmpleo = referidoBean.getCodigoTipoEmpleo();
        if(codigoTipoEmpleo == -1){
            referidoRequest.setCodigoTipoEmpleo(null);
        }else{
            referidoRequest.setCodigoTipoEmpleo(Integer.toString(codigoTipoEmpleo));
        }
        referidoRequest.setFlagActivo(Integer.toString(referidoBean.getFlagActivo()));
        referidoRequest.setFechaCreacionDispositivo(referidoBean.getFechaCreacionDispositivo());
        referidoRequest.setFechaModificacionDispositivo(referidoBean.getFechaModificacionDispositivo());
        return referidoRequest;
    }

    public static RecordatorioLLamadaRequest transform(RecordatorioLlamadaBean recordatorioLlamadaBean) {
        RecordatorioLLamadaRequest recordatorioLLamadaRequest = new RecordatorioLLamadaRequest();
        int idRecordatorioLlamada = recordatorioLlamadaBean.getIdRecordatorioLlamada();

        if(idRecordatorioLlamada == -1){
            recordatorioLLamadaRequest.setIdRecordatorioLlamada(null);
        }else {
            recordatorioLLamadaRequest.setIdRecordatorioLlamada(String.valueOf(recordatorioLlamadaBean.getIdRecordatorioLlamada()));
        }

        int idRecordatorioLLamadaDispositivo = recordatorioLlamadaBean.getIdRecordatorioLlamadaDispositivo();
        if(idRecordatorioLLamadaDispositivo == -1){
            recordatorioLLamadaRequest.setIdRecordatorioLlamadaDispositivo(null);
        }else {
            recordatorioLLamadaRequest.setIdRecordatorioLlamadaDispositivo(String.valueOf(recordatorioLlamadaBean.getIdRecordatorioLlamadaDispositivo()));
        }

        int idProspecto = recordatorioLlamadaBean.getIdProspecto();
        if(idProspecto == -1){
            recordatorioLLamadaRequest.setIdProspecto(null);
        }else {
            recordatorioLLamadaRequest.setIdProspecto(String.valueOf(recordatorioLlamadaBean.getIdProspecto()));
        }

        int idProspectoDispositivo = recordatorioLlamadaBean.getIdProspectoDispositivo();
        if(idProspectoDispositivo == -1){
            recordatorioLLamadaRequest.setIdProspectoDispositivo(null);
        }else {
            recordatorioLLamadaRequest.setIdProspectoDispositivo(String.valueOf(recordatorioLlamadaBean.getIdProspectoDispositivo()));
        }

        int idCita = recordatorioLlamadaBean.getIdCita();
        if(idCita == -1){
            recordatorioLLamadaRequest.setIdCita(null);
        }else {
            recordatorioLLamadaRequest.setIdCita(String.valueOf(recordatorioLlamadaBean.getIdCita()));
        }

        int idCitaDispositivo = recordatorioLlamadaBean.getIdCitaDispositivo();
        if(idCitaDispositivo == -1){
            recordatorioLLamadaRequest.setIdCitaDispositivo(null);
        }else {
            recordatorioLLamadaRequest.setIdCitaDispositivo(String.valueOf(recordatorioLlamadaBean.getIdCitaDispositivo()));
        }

        recordatorioLLamadaRequest.setFechaRecordatorio(recordatorioLlamadaBean.getFechaRecordatorio());

        int flagActivo = recordatorioLlamadaBean.getFlagActivo();
        if(flagActivo == -1){
            recordatorioLLamadaRequest.setFlagActivo(null);
        }else {
            recordatorioLLamadaRequest.setFlagActivo(String.valueOf(recordatorioLlamadaBean.getFlagActivo()));
        }

        recordatorioLLamadaRequest.setFechaCreacionDispositivo(recordatorioLlamadaBean.getFechaCreacionDispositivo());
        recordatorioLLamadaRequest.setFechaModificacionDispositivo(recordatorioLlamadaBean.getFechaModificacionDispositivo());

        return recordatorioLLamadaRequest;
    }

    public static ArrayList<ReunionInternaRequest> transformarListaReunion(ArrayList<ReunionInternaBean> arrReunionInternaBean){
        ArrayList<ReunionInternaRequest> arrReunionRequest = new ArrayList<>();
        for(ReunionInternaBean reunionInternaBean : arrReunionInternaBean){
            arrReunionRequest.add(transform(reunionInternaBean));
        }
        return arrReunionRequest;
    }
    public static ArrayList<ProspectoRequest> transformarListaProspecto(ArrayList<ProspectoBean> arrProspectoBean){
        ArrayList<ProspectoRequest> arrProspectoRequest = new ArrayList<>();
        for(ProspectoBean prospectoBean : arrProspectoBean){
            arrProspectoRequest.add(transform(prospectoBean));
        }
        return arrProspectoRequest;
    }
    public static ArrayList<ProspectoMovimientoEtapaRequest> transformarListaProspectoMovimientoEtapa(ArrayList<ProspectoMovimientoEtapaBean> arrProspectoMovimientoEtapaBean){
        ArrayList<ProspectoMovimientoEtapaRequest> arrProspectoMovimientoEtapaRequest = new ArrayList<>();
        for(ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean : arrProspectoMovimientoEtapaBean){
            arrProspectoMovimientoEtapaRequest.add(transform(prospectoMovimientoEtapaBean));
        }
        return arrProspectoMovimientoEtapaRequest;
    }
    public static ArrayList<ADNRequest> transformarListaAdn(ArrayList<AdnBean> arrAdnBean){
        ArrayList<ADNRequest> arrAdnRequest = new ArrayList<>();
        for(AdnBean adnBean : arrAdnBean){
            arrAdnRequest.add(transform(adnBean));
        }
        return arrAdnRequest;
    }
    public static ArrayList<FamiliarRequest> transformarListaFamiliar(ArrayList<FamiliarBean> arrFamiliarBean){
        ArrayList<FamiliarRequest> arrFamiliaresRequest = new ArrayList<>();
        for(FamiliarBean familiarBean : arrFamiliarBean){
            arrFamiliaresRequest.add(transform(familiarBean));
        }
        return arrFamiliaresRequest;
    }
    public static ArrayList<CitaRequest> transformarListaCita(ArrayList<CitaBean> arrCitaBean){
        ArrayList<CitaRequest> arrCitaRequest = new ArrayList<>();
        for(CitaBean citaBean : arrCitaBean){
            arrCitaRequest.add(transform(citaBean));
        }
        return arrCitaRequest;
    }
    public static ArrayList<CitaMovimientoEstadoRequest> transformarListaCitaMovimientoEstado(ArrayList<CitaMovimientoEstadoBean> arrCitaMovimientoEstadoBean){
        ArrayList<CitaMovimientoEstadoRequest> arrCitaMovimientoEstadoRequest = new ArrayList<>();
        for(CitaMovimientoEstadoBean citaMovimientoEstadoBean : arrCitaMovimientoEstadoBean){
            arrCitaMovimientoEstadoRequest.add(transform(citaMovimientoEstadoBean));
        }
        return arrCitaMovimientoEstadoRequest;
    }
    public static ArrayList<ReferidoRequest> transformListaReferidos(ArrayList<ReferidoBean> arrReferidoBean){
        ArrayList<ReferidoRequest> arrReferidoRequest = new ArrayList<>();
        for(ReferidoBean referidoBean : arrReferidoBean){
            arrReferidoRequest.add(transform(referidoBean));
        }
        return arrReferidoRequest;
    }
    public static ArrayList<RecordatorioLLamadaRequest> transformarListaRecordatorioLlamada(ArrayList<RecordatorioLlamadaBean> arrRecordatorioLLamadaBean){
        ArrayList<RecordatorioLLamadaRequest> arrRecordatorioLlamadaRequest = new ArrayList<>();
        for(RecordatorioLlamadaBean recordatorioLlamadaBean : arrRecordatorioLLamadaBean){
            arrRecordatorioLlamadaRequest.add(transform(recordatorioLlamadaBean));
        }
        return arrRecordatorioLlamadaRequest;
    }
}