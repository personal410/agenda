package com.pacifico.agenda.Util;

import android.util.Log;


import com.pacifico.agenda.Model.Bean.AdnBean;
import com.pacifico.agenda.Model.Bean.CalendarioBean;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.agenda.Model.Bean.EntidadBean;
import com.pacifico.agenda.Model.Bean.FamiliarBean;
import com.pacifico.agenda.Model.Bean.IntermediarioBean;
import com.pacifico.agenda.Model.Bean.MensajeSistemaBean;
import com.pacifico.agenda.Model.Bean.ParametroBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Model.Bean.ReferidoBean;
import com.pacifico.agenda.Model.Bean.ReunionInternaBean;
import com.pacifico.agenda.Model.Bean.TablaIndiceBean;
import com.pacifico.agenda.Model.Bean.TablaTablasBean;
import com.pacifico.agenda.Network.Response.Comun.CalendarioResponse;
import com.pacifico.agenda.Network.Response.Comun.IntermediarioResponse;
import com.pacifico.agenda.Network.Response.Comun.MensajeSistemaResponse;
import com.pacifico.agenda.Network.Response.Comun.ParametroResponse;
import com.pacifico.agenda.Network.Response.Comun.TablaIndiceResponse;
import com.pacifico.agenda.Network.Response.Comun.TablaTablasResponse;
import com.pacifico.agenda.Network.Response.GetData.ADNResponse;
import com.pacifico.agenda.Network.Response.GetData.CitaMovimientoEstadoResponse;
import com.pacifico.agenda.Network.Response.GetData.CitaResponse;
import com.pacifico.agenda.Network.Response.GetData.EntidadResponse;
import com.pacifico.agenda.Network.Response.GetData.FamiliarResponse;
import com.pacifico.agenda.Network.Response.GetData.ProspectoMovimientoEtapaResponse;
import com.pacifico.agenda.Network.Response.GetData.ProspectoResponse;
import com.pacifico.agenda.Network.Response.GetData.RecordatorioLlamadaResponse;
import com.pacifico.agenda.Network.Response.GetData.ReferidoResponse;
import com.pacifico.agenda.Network.Response.GetData.ReunionResponse;

import java.util.ArrayList;
import java.util.List;

public class DataMapperResponse{
    public static AdnBean transform(ADNResponse adnResponse){
        try{
            AdnBean adnBean = new AdnBean();
            adnBean.setIdProspecto(Integer.parseInt(adnResponse.getIdProspecto()));
            adnBean.setIdProspectoDispositivo(-1);
            adnBean.setTipoCambio(Double.parseDouble(adnResponse.getTipoCambio()));
            adnBean.setMonedaEfectivoAhorros(Integer.parseInt(adnResponse.getMonedaEfectivoAhorros()));
            adnBean.setEfectivoAhorros((int) Double.parseDouble(adnResponse.getEfectivoAhorros()));
            adnBean.setMonedaPropiedades(Integer.parseInt(adnResponse.getMonedaPropiedades()));
            adnBean.setPropiedades((int) Double.parseDouble(adnResponse.getPropiedades()));
            adnBean.setMonedaVehiculos(Integer.parseInt(adnResponse.getMonedaVehiculos()));
            adnBean.setVehiculos((int) Double.parseDouble(adnResponse.getVehiculos()));
            adnBean.setMonedaTotalActivoRealizable(Integer.parseInt(adnResponse.getMonedaTotalActivoRealizable()));
            adnBean.setTotalActivoRealizable(Double.parseDouble(adnResponse.getTotalActivoRealizable()));
            adnBean.setMonedaSeguroIndividual(Integer.parseInt(adnResponse.getMonedaSeguroIndividual()));
            adnBean.setSeguroIndividual(((int) Double.parseDouble(adnResponse.getSeguroIndividual())));
            adnBean.setMonedaIngresoBrutoMensualVidaLey(Integer.parseInt(adnResponse.getMonedaIngresoBrutoMensualVidaLey()));
            adnBean.setIngresoBrutoMensualVidaLey((int) Double.parseDouble(adnResponse.getIngresoBrutoMensualVidaLey()));
            adnBean.setFactorVidaLey(Integer.parseInt(adnResponse.getFactorVidaLey()));
            adnBean.setMonedaTopeVidaLey(Integer.parseInt(adnResponse.getMonedaTopeVidaLey()));
            adnBean.setTopeVidaLey(Double.parseDouble(adnResponse.getTopeVidaLey()));
            adnBean.setMonedaSeguroVidaLey(Integer.parseInt(adnResponse.getMonedaSeguroVidaLey()));
            adnBean.setSeguroVidaLey((int) Double.parseDouble(adnResponse.getSeguroVidaLey()));
            String flagCuentaConVidaLey = adnResponse.getFlagCuentaConVidaLey();
            if(flagCuentaConVidaLey == null){
                adnBean.setFlagCuentaConVidaLey(1);
            }else{
                adnBean.setFlagCuentaConVidaLey(flagCuentaConVidaLey.toLowerCase().equals("true") ? 1 : 0);
            }
            adnBean.setMonedaTotalSegurosVida(Integer.parseInt(adnResponse.getMonedaTotalSegurosVida()));
            adnBean.setTotalSegurosVida(Double.parseDouble(adnResponse.getTotalSegurosVida()));
            adnBean.setPorcentajeAFPConyuge(Double.parseDouble(adnResponse.getPorcentajeAFPConyuge()));
            adnBean.setPorcentajeAFPHijos(Double.parseDouble(adnResponse.getPorcentajeAFPHijos()));
            adnBean.setNumeroHijos(Integer.parseInt(adnResponse.getNumeroHijos()));
            adnBean.setMonedaPensionConyuge(Integer.parseInt(adnResponse.getMonedaPensionConyuge()));
            adnBean.setPensionConyuge(Double.parseDouble(adnResponse.getPensionConyuge()));
            adnBean.setMonedaPensionHijos(Integer.parseInt(adnResponse.getMonedaPensionHijos()));
            adnBean.setPensionHijos(Double.parseDouble(adnResponse.getPensionHijos()));
            adnBean.setFlagPensionConyuge(adnResponse.getFlagPensionConyuge().toLowerCase().equals("true") ? 1 : 0);
            String flagPensioHijos = adnResponse.getFlagPensionHijos();
            if(flagPensioHijos == null){
                adnBean.setFlagPensionHijos(0);
            }else{
                adnBean.setFlagPensionHijos(flagPensioHijos.toLowerCase().equals("true") ? 1 : 0);
            }
            adnBean.setFlagPensionNoAFP(adnResponse.getFlagPensionNoAFP().toLowerCase().equals("true") ? 1 : 0);
            adnBean.setMonedaTotalPensionMensualAFP(Integer.parseInt(adnResponse.getMonedaTotalPensionMensualAFP()));
            adnBean.setTotalPensionMensualAFP(Double.parseDouble(adnResponse.getTotalPensionMensualAFP()));
            adnBean.setMonedaIngresoMensualTitular(Integer.parseInt(adnResponse.getMonedaIngresoMensualTitular()));
            adnBean.setIngresoMensualTitular((int) Double.parseDouble(adnResponse.getIngresoMensualTitular()));
            adnBean.setMonedaIngresoMensualConyuge(Integer.parseInt(adnResponse.getMonedaIngresoMensualConyuge()));
            adnBean.setIngresoMensualConyuge((int) Double.parseDouble(adnResponse.getIngresoMensualConyuge()));
            adnBean.setMonedaIngresoFamiliarOtros(Integer.parseInt(adnResponse.getMonedaIngresoFamiliarOtros()));
            adnBean.setIngresoFamiliarOtros((int) Double.parseDouble(adnResponse.getIngresoFamiliarOtros()));
            adnBean.setMonedaTotalIngresoFamiliarMensual(Integer.parseInt(adnResponse.getMonedaTotalIngresoFamiliarMensual()));
            adnBean.setTotalIngresoFamiliarMensual(Double.parseDouble(adnResponse.getTotalIngresoFamiliarMensual()));
            adnBean.setMonedaGastoVivienda(Integer.parseInt(adnResponse.getMonedaGastoVivienda()));
            adnBean.setGastoVivienda((int) Double.parseDouble(adnResponse.getGastoVivienda()));
            adnBean.setMonedaGastoServicios(Integer.parseInt(adnResponse.getMonedaGastoServicios()));
            adnBean.setGastoServicios((int) Double.parseDouble(adnResponse.getGastoServicios()));
            adnBean.setMonedaGastoHogarAlimentacion(Integer.parseInt(adnResponse.getMonedaGastoHogarAlimentacion()));
            adnBean.setGastoHogarAlimentacion((int) Double.parseDouble(adnResponse.getGastoHogarAlimentacion()));
            adnBean.setMonedaGastoSalud(Integer.parseInt(adnResponse.getMonedaGastoSalud()));
            adnBean.setGastoSalud((int) Double.parseDouble(adnResponse.getGastoSalud()));
            adnBean.setMonedaGastoEducacion(Integer.parseInt(adnResponse.getMonedaGastoEducacion()));
            adnBean.setGastoEducacion((int) Double.parseDouble(adnResponse.getGastoEducacion()));
            adnBean.setMonedaGastoVehiculoTransporte(Integer.parseInt(adnResponse.getMonedaGastoVehiculoTransporte()));
            adnBean.setGastoVehiculoTransporte((int) Double.parseDouble(adnResponse.getGastoVehiculoTransporte()));
            adnBean.setMonedaGastoEsparcimiento(Integer.parseInt(adnResponse.getMonedaGastoEsparcimiento()));
            adnBean.setGastoEsparcimiento((int) Double.parseDouble(adnResponse.getGastoEsparcimiento()));
            adnBean.setMonedaGastoOtros(Integer.parseInt(adnResponse.getMonedaGastoOtros()));
            adnBean.setGastoOtros((int) Double.parseDouble(adnResponse.getGastoOtros()));
            adnBean.setMonedaTotalGastoFamiliarMensual(Integer.parseInt(adnResponse.getMonedaTotalGastoFamiliarMensual()));
            adnBean.setTotalGastoFamiliarMensual(Double.parseDouble(adnResponse.getTotalGastoFamiliarMensual()));
            adnBean.setMonedaDeficitMensual(Integer.parseInt(adnResponse.getMonedaDeficitMensual()));
            adnBean.setDeficitMensual(Double.parseDouble(adnResponse.getDeficitMensual()));
            adnBean.setAniosProteger(Integer.parseInt(adnResponse.getAniosProteger()));
            adnBean.setMonedaCapitalNecesarioFallecimiento(Integer.parseInt(adnResponse.getMonedaCapitalNecesarioFallecimiento()));
            adnBean.setCapitalNecesarioFallecimiento(Double.parseDouble(adnResponse.getCapitalNecesarioFallecimiento()));
            adnBean.setPorcentajeInversion(Double.parseDouble(adnResponse.getPorcentajeInversion()));
            adnBean.setMonedaMontoMensualInvertir(Integer.parseInt(adnResponse.getMonedaMontoMensualInvertir()));
            adnBean.setMontoMensualInvertir(Double.parseDouble(adnResponse.getMontoMensualInvertir()));
            String idEntidad = adnResponse.getIdEntidad();
            if(idEntidad == null){
                adnBean.setIdEntidad(-1);
            }else{
                adnBean.setIdEntidad(Integer.parseInt(idEntidad));
            }
            String codigoFrecuenciaPago = adnResponse.getCodigoFrecuenciaPago();
            if(codigoFrecuenciaPago == null){
                adnBean.setCodigoFrecuenciaPago(-1);
            }else{
                adnBean.setCodigoFrecuenciaPago(Integer.parseInt(codigoFrecuenciaPago));
            }
            adnBean.setIndicadorVentana(Integer.parseInt(adnResponse.getIndicadorVentana()));
            adnBean.setFlagTerminado(adnResponse.getFlagTerminado().toLowerCase().equals("true") ? 1 : 0);
            String flagADNDigital = adnResponse.getFlagADNDigital();
            if(flagADNDigital == null){
                adnBean.setFlagADNDigital(adnBean.getIngresoMensualTitular() > 0 ? 1 : 0);
            }else{
                adnBean.setFlagADNDigital(flagADNDigital.toLowerCase().equals("true") ? 1 : 0);
            }
            adnBean.setFechaCreacionDispositivo(adnResponse.getFechaCreacionDispositivo());
            adnBean.setFechaModificacionDispositivo(adnResponse.getFechaModificacionDispositivo());
            adnBean.setAdicionalTexto1(adnResponse.getAdicionalTexto1());
            String adicionalNumerico1 = adnResponse.getAdicionalNumerico1();
            if(adicionalNumerico1 == null){
                adnBean.setAdicionalNumerico1(-1);
            }else{
                adnBean.setAdicionalNumerico1(Double.parseDouble(adicionalNumerico1));
            }
            adnBean.setAdicionalTexto2(adnResponse.getAdicionalTexto2());
            String adicionalNumerico2 = adnResponse.getAdicionalNumerico1();
            if(adicionalNumerico2 == null){
                adnBean.setAdicionalNumerico2(-1);
            }else{
                adnBean.setAdicionalNumerico2(Double.parseDouble(adicionalNumerico2));
            }
            adnBean.setFlagEnviado(2);
            return adnBean;
        }catch(Exception e){
            return null;
        }
    }
    public static CalendarioBean transform(CalendarioResponse calendarioResponse){
        CalendarioBean calendarioBean = new CalendarioBean();
        try {
            calendarioBean.setIdCalendario(Integer.parseInt(calendarioResponse.getIdCalendario()));
        }catch (Exception e){}
        try {
            calendarioBean.setAnio(Integer.parseInt(calendarioResponse.getAnio()));
        }catch (Exception e){}
        try {
            calendarioBean.setSemanaAno(Integer.parseInt(calendarioResponse.getSemanaAno()));
        }catch (Exception e){}
        try {
            calendarioBean.setMesAno(Integer.parseInt(calendarioResponse.getMesAnio()));
        }catch (Exception e){}
        try {
            calendarioBean.setSemanaMes(Integer.parseInt(calendarioResponse.getSemanaMes()));
        }catch (Exception e){}
        calendarioBean.setFechaInicioSemana(calendarioResponse.getFechaInicioSemana());
        calendarioBean.setFechaFinSemana(calendarioResponse.getFechaFinSemana());
        return calendarioBean;
    }
    public static CitaBean transform(CitaResponse citaResponse){
        CitaBean citaBean = new CitaBean();
        try{
            citaBean.setIdCita(Integer.parseInt(citaResponse.getIdCita()));
        }catch(Exception e){}
        try {
            citaBean.setIdProspecto(Integer.parseInt(citaResponse.getIdProspecto()));
        }catch(Exception e){}
        try{
            citaBean.setIdProspectoDispositivo(Integer.parseInt(citaResponse.getIdProspectoDispositivo()));}
        catch(Exception e){};
        try{
            citaBean.setIdCitaDispositivo(Integer.parseInt(citaResponse.getIdCitaDispositivo()));
        }catch(Exception e){}
        try {
            citaBean.setNumeroEntrevista(Integer.parseInt(citaResponse.getNumeroEntrevista()));
        }catch(Exception e){}
        try {
            citaBean.setCodigoEstado(Integer.parseInt(citaResponse.getCodigoEstado()));
        }catch(Exception e){}
        try {
            citaBean.setCodigoResultado(Integer.parseInt(citaResponse.getCodigoResultado()));
        }catch(Exception e){}
        citaBean.setFechaCita(citaResponse.getFechaCita());
        citaBean.setHoraInicio(citaResponse.getHoraInicio());
        citaBean.setHoraFin(citaResponse.getHoraFin());
        citaBean.setUbicacion(citaResponse.getUbicacion());
        citaBean.setReferenciaUbicacion(citaResponse.getReferenciaUbicacion());
        if ("True".equals(citaResponse.getFlagInvitadoGU()))
            citaBean.setFlagInvitadoGU(1);
        else if ("False".equals(citaResponse.getFlagInvitadoGU()))
            citaBean.setFlagInvitadoGU(0);
        else
            citaBean.setFlagInvitadoGU(-1);
        if ("True".equals(citaResponse.getFlagInvitadoGA()))
            citaBean.setFlagInvitadoGA(1);
        else if ("False".equals(citaResponse.getFlagInvitadoGA()))
            citaBean.setFlagInvitadoGA(0);
        else
            citaBean.setFlagInvitadoGA(-1);
        try {
            citaBean.setAlertaMinutosAntes(Integer.parseInt(citaResponse.getAlertaMinutosAntes()));
        }catch(Exception e){}

        try {
            citaBean.setCantidadVI(Integer.parseInt(citaResponse.getCantidadVI()));
        }catch(Exception e){}

        try {
            citaBean.setPrimaTargetVI(Double.parseDouble(citaResponse.getPrimaTargetVI()));
        }catch(Exception e){}

        try {
            citaBean.setCantidadAP(Integer.parseInt(citaResponse.getCantidadAP()));
        }catch(Exception e){}

        try {
            citaBean.setPrimaTargetAP(Double.parseDouble(citaResponse.getPrimaTargetAP()));
        }catch(Exception e){}

        try {
            citaBean.setCodigoIntermediarioCreacion(Integer.parseInt(citaResponse.getCodigoIntermediarioCreacion()));
        }catch(Exception e){}
        try {
            citaBean.setCodigoIntermediarioModificacion(Integer.parseInt(citaResponse.getCodigoIntermediarioModificacion()));
        }catch(Exception e){}

        try {
            citaBean.setCodigoEtapaProspecto(Integer.parseInt(citaResponse.getCodigoEtapaProspecto()));
        }catch(Exception e){}

        try {
            if (citaResponse.getCodigoMotivoReagendado() == null)
                citaBean.setCodigoMotivoReagendado(-1);
            else
                citaBean.setCodigoMotivoReagendado(Integer.parseInt(citaResponse.getCodigoMotivoReagendado()));
        }catch(Exception e){}

        citaBean.setFechaCreacionDispositivo(citaResponse.getFechaCreacionDispositivo());
        citaBean.setFechaModificacionDispositivo(citaResponse.getFechaModificacionDispositivo());
        citaBean.setFlagEnviado(2);
        return citaBean;
    }
    public static CitaMovimientoEstadoBean transform(CitaMovimientoEstadoResponse citaMovimientoEstadoResponse){
        CitaMovimientoEstadoBean citaMovimientoEstadoBean = new CitaMovimientoEstadoBean();
        try {
            citaMovimientoEstadoBean.setIdMovimiento(Integer.parseInt(citaMovimientoEstadoResponse.getIdMovimiento()));
        }catch(Exception e){}

        try{
            citaMovimientoEstadoBean.setIdMovimientoDispositivo(Integer.parseInt(citaMovimientoEstadoResponse.getIdMovimientoDispositivo()));}
        catch(Exception e){};

        try {
            citaMovimientoEstadoBean.setIdCita(Integer.parseInt(citaMovimientoEstadoResponse.getIdCita()));
        }catch(Exception e){}
        try {
            citaMovimientoEstadoBean.setCodigoEstado(Integer.parseInt(citaMovimientoEstadoResponse.getCodigoEstado()));
        }catch(Exception e){}
        try {
            citaMovimientoEstadoBean.setCodigoResultado(Integer.parseInt(citaMovimientoEstadoResponse.getCodigoResultado()));
        }catch(Exception e){}

        citaMovimientoEstadoBean.setFechaMovimientoEstadoDispositivo(citaMovimientoEstadoResponse.getFechaMovimientoEstadoDispositivo());
        try {
            citaMovimientoEstadoBean.setIdCitaDispositivo(Integer.parseInt(citaMovimientoEstadoResponse.getIdCitaDispositivo()));
        }catch(Exception e){}
        citaMovimientoEstadoBean.setFlagEnviado(2);
        return citaMovimientoEstadoBean;
    }

    public static EntidadBean transform(EntidadResponse entidadResponse){
        EntidadBean entidadBean = new EntidadBean();
        entidadBean.setIdEntidad(Integer.parseInt(entidadResponse.getIdEntidad()));
        entidadBean.setDescripcion(entidadResponse.getDescripcion());
        entidadBean.setImagen(entidadResponse.getImagen());
        entidadBean.setCodigoTipoCobranza(Integer.parseInt(entidadResponse.getCodigoTipoCobranza()));
        return entidadBean;
    }
    public static FamiliarBean transform(FamiliarResponse familiarResponse){
        FamiliarBean familiarBean = new FamiliarBean();
        familiarBean.setIdFamiliar(Integer.parseInt(familiarResponse.getIdFamiliar()));
        String idFamiliarDispositivo = familiarResponse.getIdFamiliarDispositivo();
        if(idFamiliarDispositivo != null){
            familiarBean.setIdFamiliarDispositivo(Integer.parseInt(idFamiliarDispositivo));
        }
        familiarBean.setIdProspecto(Integer.parseInt(familiarResponse.getIdProspecto()));
        familiarBean.setIdProspectoDispositivo(-1);
        familiarBean.setNombres(familiarResponse.getNombres());
        familiarBean.setApellidoPaterno(familiarResponse.getApellidoPaterno());
        if(familiarBean.getApellidoPaterno() == null){
            familiarBean.setApellidoPaterno("");
        }
        familiarBean.setApellidoMaterno(familiarResponse.getApellidoMaterno());
        if(familiarBean.getApellidoMaterno() == null) {
            familiarBean.setApellidoMaterno("");
        }
        familiarBean.setFechaNacimiento(familiarResponse.getFechaNacimiento());
        String edad = familiarResponse.getEdad();
        if(edad == null){
            familiarBean.setEdad(-1);
        }else{
            familiarBean.setEdad(Integer.parseInt(edad));
        }
        familiarBean.setOcupacion(familiarResponse.getOcupacion());
        familiarBean.setCodigoTipoFamiliar(Integer.parseInt(familiarResponse.getCodigoTipoFamiliar()));
        familiarBean.setFechaCreacionDispositivo(familiarResponse.getFechaCreacionDispositivo());
        familiarBean.setFechaModificacionDispositivo(familiarResponse.getFechaModificacionDispositivo());
        String flagActivo = familiarResponse.getFlagActivo();
        Log.i("TAG", "flagActivo: " + flagActivo);
        if(flagActivo == null){
            familiarBean.setFlagActivo(1);
        }else{
            familiarBean.setFlagActivo(flagActivo.toLowerCase().equals("true") ? 1 : 0);
        }
        familiarBean.setFlagEnviado(2);
        return familiarBean;
    }
    public static IntermediarioBean transform(IntermediarioResponse intermediarioResponse){
        IntermediarioBean intermediarioBean = new IntermediarioBean();
        intermediarioBean.setCodigoIntermediario(Integer.parseInt(intermediarioResponse.getCodigoIntermediario()));
        intermediarioBean.setNombreRazonSocial(intermediarioResponse.getNombreRazonSocial());
        intermediarioBean.setTipoDocumento(intermediarioResponse.getTipoDocumento());
        intermediarioBean.setDocumentoIdentidad(intermediarioResponse.getDocumentoIdentidad());
        intermediarioBean.setTelefonoCelular(intermediarioResponse.getTelefonoCelular());
        intermediarioBean.setTelefonoFijo(intermediarioResponse.getTelefonoFijo());
        intermediarioBean.setCorreoElectronico(intermediarioResponse.getCorreoElectronico());
        intermediarioBean.setFechaPromocion(intermediarioResponse.getFechaPromocion());
        intermediarioBean.setNombreGU(intermediarioResponse.getNombreGU());
        intermediarioBean.setCorreoElectronicoGU(intermediarioResponse.getCorreoElectronioGU());
        intermediarioBean.setNombreGA(intermediarioResponse.getNombreGA());
        intermediarioBean.setCorreoElectronicoGA(intermediarioResponse.getCorreoElectronioGA());
        intermediarioBean.setDescripcionAgencia(intermediarioResponse.getDescripcionAgencia());
        intermediarioBean.setDescripcionOficina(intermediarioResponse.getDescripcionOficina());
        intermediarioBean.setDescripcionCategoria(intermediarioResponse.getDescripcionCategoria());
        intermediarioBean.setCodigoDepartamento(Integer.parseInt(intermediarioResponse.getCodigoDepartamento()));
        return intermediarioBean;
    }
    public static MensajeSistemaBean transform(MensajeSistemaResponse mensajeSistemaResponse){
        MensajeSistemaBean mensajeSistemaBean = new MensajeSistemaBean();
        try{
            mensajeSistemaBean.setIdMensajeSistema(Integer.parseInt(mensajeSistemaResponse.getIdMensajeSistema()));}
        catch(Exception e){}
        mensajeSistemaBean.setDescripcionMensaje(mensajeSistemaResponse.getDescripcionMensaje());
        mensajeSistemaBean.setFechaInicioVigencia(mensajeSistemaResponse.getFechaInicioVigencia());
        mensajeSistemaBean.setFechaFinVigencia(mensajeSistemaResponse.getFechaFinVigencia());
        return mensajeSistemaBean;
    }
    public static ParametroBean transform(ParametroResponse parametroResponse){
        ParametroBean parametroBean = new ParametroBean();
        parametroBean.setIdParametro(Integer.parseInt(parametroResponse.getIdParametro()));
        parametroBean.setDescripcion(parametroResponse.getDescripcion());
        parametroBean.setValorCadena(parametroResponse.getValorCadena());
        parametroBean.setValorNumerico(Double.parseDouble(parametroResponse.getValorNumerico()));
        return parametroBean;
    }
    public static ProspectoBean transform(ProspectoResponse prospectoResponse){
        ProspectoBean prospectoBean = new ProspectoBean();
        prospectoBean.setIdProspecto(Integer.parseInt(prospectoResponse.getIdProspecto()));
        try{
            prospectoBean.setIdProspectoDispositivo(Integer.parseInt(prospectoResponse.getIdProspectoDispositivo()));
        }catch(Exception e){
            prospectoBean.setIdProspectoDispositivo(-1);
        }
        try{
            prospectoBean.setIdProspectoExterno(Integer.parseInt(prospectoResponse.getIdProspectoExterno()));
        }catch(Exception e){
            prospectoBean.setIdProspectoExterno(-1);
        }
        prospectoBean.setNombres(prospectoResponse.getNombres());
        prospectoBean.setApellidoPaterno(prospectoResponse.getApellidoPaterno());
        prospectoBean.setApellidoMaterno(prospectoResponse.getApellidoMaterno());
        try{
            prospectoBean.setCodigoSexo(Integer.parseInt(prospectoResponse.getCodigoSexo()));
        }catch(Exception e){
            prospectoBean.setCodigoSexo(-1);
        }
        try{
            prospectoBean.setCodigoEstadoCivil(Integer.parseInt(prospectoResponse.getCodigoEstadoCivil()));
        }catch(Exception e){
            prospectoBean.setCodigoEstadoCivil(-1);
        }
        prospectoBean.setFechaNacimiento(prospectoResponse.getFechaNacimiento());
        try{
            prospectoBean.setCodigoRangoEdad(Integer.parseInt(prospectoResponse.getCodigoRangoEdad()));
        }catch(Exception e){
            prospectoBean.setCodigoRangoEdad(-1);
        }
        try{
            prospectoBean.setCodigoRangoIngreso(Integer.parseInt(prospectoResponse.getCodigoRangoIngreso()));
        }catch(Exception e){
            prospectoBean.setCodigoRangoIngreso(-1);
        }
        try{
            prospectoBean.setCodigoRangoIngreso(Integer.parseInt(prospectoResponse.getCodigoRangoIngreso()));
        }catch(Exception e){
            prospectoBean.setCodigoRangoIngreso(-1);
        }
        try{
            prospectoBean.setFlagHijo(prospectoResponse.getFlagHijo().toLowerCase().equals("true") ? 1 : 0);
        }catch(Exception e){
            prospectoBean.setFlagHijo(-1);
        }
        try{
            prospectoBean.setFlagConyuge(prospectoResponse.getFlagConyuge().toLowerCase().equals("true") ? 1 : 0);
        }catch(Exception e){
            prospectoBean.setFlagConyuge(-1);
        }
        try{
            prospectoBean.setCodigoTipoDocumento(Integer.parseInt(prospectoResponse.getCodigoTipoDocumento()));
        }catch(Exception e){
            prospectoBean.setCodigoTipoDocumento(-1);
        }
        prospectoBean.setNumeroDocumento(prospectoResponse.getNumeroDocumento());
        prospectoBean.setTelefonoCelular(prospectoResponse.getTelefonoCelular());
        prospectoBean.setTelefonoFijo(prospectoResponse.getTelefonoFijo());
        prospectoBean.setTelefonoAdicional(prospectoResponse.getTelefonoAdicional());
        prospectoBean.setCorreoElectronico1(prospectoResponse.getCorreoElectronico1());
        prospectoBean.setCorreoElectronico2(prospectoResponse.getCorreoElectronico2());
        try{
            prospectoBean.setCodigoNacionalidad(Integer.parseInt(prospectoResponse.getCodigoNacionalidad()));
        }catch(Exception e){
            prospectoBean.setCodigoNacionalidad(-1);
        }
        try{
            prospectoBean.setCondicionFumador(prospectoResponse.getCondicionFumador().toLowerCase().equals("true") ? 1 : 0);
        }catch(Exception e){
            prospectoBean.setCondicionFumador(-1);
        }
        prospectoBean.setEmpresa(prospectoResponse.getEmpresa());
        prospectoBean.setCargo(prospectoResponse.getCargo());
        prospectoBean.setNota(prospectoResponse.getNota());
        try{
            prospectoBean.setFlagEnviarADNDigital(Integer.parseInt(prospectoResponse.getFlagEnviarADNDigital()));
        }catch(Exception e){
            prospectoBean.setFlagEnviarADNDigital(0);
        }
        try {
            prospectoBean.setCodigoEtapa(Integer.parseInt(prospectoResponse.getCodigoEtapa()));
        }catch(Exception e){
            prospectoBean.setCodigoEtapa(-1);
        }
        try {
            prospectoBean.setCodigoEstado(Integer.parseInt(prospectoResponse.getCodigoEstado()));
        }catch(Exception e){
            prospectoBean.setCodigoEstado(-1);
        }

        try{
            prospectoBean.setCodigoFuente(Integer.parseInt(prospectoResponse.getCodigoFuente()));
        }catch(Exception e){
            prospectoBean.setCodigoFuente(-1);
        }
        try{
            prospectoBean.setCodigoEntorno(Integer.parseInt(prospectoResponse.getCodigoEntorno()));
        }catch(Exception e){
            prospectoBean.setCodigoEntorno(-1);
        }
        try{
            prospectoBean.setIdReferenciador(Integer.parseInt(prospectoResponse.getIdReferenciador()));
        }catch(Exception e){
            prospectoBean.setIdReferenciador(-1);

        }
        prospectoBean.setFechaCreacionDispositivo(prospectoResponse.getFechaCreacionDispositivo());
        prospectoBean.setFechaModificacionDispositivo(prospectoResponse.getFechaModificacionDispositivo());
        prospectoBean.setAdicionalTexto1(prospectoResponse.getAdicionalTexto1());
        try{
            prospectoBean.setAdicionalNumerico1(Double.parseDouble(prospectoResponse.getAdicionalNumerico1()));
        }catch(Exception e){
            prospectoBean.setAdicionalNumerico1(-1);
        }
        prospectoBean.setAdicionalTexto2(prospectoResponse.getAdicionalTexto2());
        try{
            prospectoBean.setAdicionalNumerico2(Double.parseDouble(prospectoResponse.getAdicionalNumerico2()));
        }catch(Exception e){
            prospectoBean.setAdicionalNumerico2(-1);
        }
        prospectoBean.setFlagEnviado(2);
        return prospectoBean;
    }

    public static ProspectoMovimientoEtapaBean transform(ProspectoMovimientoEtapaResponse prospectoMovimientoEtapaResponse){
        ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean = new ProspectoMovimientoEtapaBean();
        try {
            prospectoMovimientoEtapaBean.setIdMovimiento(Integer.parseInt(prospectoMovimientoEtapaResponse.getIdMovimiento()));
        }catch(Exception e){}

        try {
            prospectoMovimientoEtapaBean.setIdMovimientoDispositivo(Integer.parseInt(prospectoMovimientoEtapaResponse.getIdMovimientoDispositivo()));
        }catch(Exception e){}

        try {
            prospectoMovimientoEtapaBean.setIdProspecto(Integer.parseInt(prospectoMovimientoEtapaResponse.getIdProspecto()));
        }catch(Exception e){}

        try {
            prospectoMovimientoEtapaBean.setCodigoEtapa(Integer.parseInt(prospectoMovimientoEtapaResponse.getCodigoEtapa()));
        }catch(Exception e){}

        try {
            prospectoMovimientoEtapaBean.setCodigoEstado(Integer.parseInt(prospectoMovimientoEtapaResponse.getCodigoEstado()));
        }catch(Exception e){}

        prospectoMovimientoEtapaBean.setFechaMovimientoEtapaDispositivo(prospectoMovimientoEtapaResponse.getFechaMovimientoEtapaDispositivo());
        prospectoMovimientoEtapaBean.setFlagEnviado(2);
        return prospectoMovimientoEtapaBean;
    }

    public static ReferidoBean transform(ReferidoResponse referidoResponse){
        ReferidoBean referidoBean = new ReferidoBean();
        referidoBean.setIdReferido(Integer.parseInt(referidoResponse.getIdReferido()));
        referidoBean.setIdProspecto(Integer.parseInt(referidoResponse.getIdProspecto()));
        referidoBean.setIdReferidoDispositivo(Integer.parseInt(referidoResponse.getIdReferidoDispositivo()));
        referidoBean.setIdProspectoDispositivo(-1);
        referidoBean.setCodigoTipoReferido(Integer.parseInt(referidoResponse.getCodigoTipoReferido()));
        referidoBean.setNombres(referidoResponse.getNombres());
        referidoBean.setApellidoPaterno(referidoResponse.getApellidoPaterno());
        referidoBean.setApellidoMaterno(referidoResponse.getApellidoMaterno());
        String codigoRangoEdad = referidoResponse.getCodigoRangoEdad();
        if(codigoRangoEdad == null){
            referidoBean.setCodigoRangoEdad(-1);
        }else{
            referidoBean.setCodigoRangoEdad(Integer.parseInt(codigoRangoEdad));
        }
        String flagHijo = referidoResponse.getFlagHijo();
        if(flagHijo == null){
            referidoBean.setFlagHijo(-1);
        }else{
            if(flagHijo.length() == 0){
                referidoBean.setFlagHijo(-1);
            }else{
                referidoBean.setFlagHijo(flagHijo.toLowerCase().equals("true") ? 1 : 0);
            }
        }
        String codigoRangoIngreso = referidoResponse.getCodigoRangoIngreso();
        if(codigoRangoIngreso == null){
            referidoBean.setCodigoRangoIngreso(-1);
        }else{
            if (codigoRangoIngreso.length() == 0){
                referidoBean.setCodigoRangoIngreso(-1);
            }else{
                referidoBean.setCodigoRangoIngreso(Integer.parseInt(codigoRangoIngreso));
            }
        }
        referidoBean.setTelefono(referidoResponse.getTelefono());
        referidoBean.setFechaCreaContacto(referidoResponse.getFechaCreaContacto());
        String codigoTipoEmpleo = referidoResponse.getCodigoTipoEmpleo();
        if (codigoTipoEmpleo == null){
            referidoBean.setCodigoTipoEmpleo(-1);
        }else{
            referidoBean.setCodigoTipoEmpleo(Integer.parseInt(codigoTipoEmpleo));
        }
        String flagActivo = referidoResponse.getFlagActivo();
        if(flagActivo == null){
            referidoBean.setFlagActivo(1);
        }else{
            referidoBean.setFlagActivo(flagActivo.toLowerCase().equals("true") ? 1 : 0);
        }
        referidoBean.setFechaCreacionDispositivo(referidoResponse.getFechaCreacionDispositivo());
        referidoBean.setFechaModificacionDispositivo(referidoResponse.getFechaModificacionDispositivo());
        referidoBean.setFlagEnviado(2);
        return referidoBean;
    }

    public static RecordatorioLlamadaBean transform(RecordatorioLlamadaResponse recordatorioLlamadaResponse){
        RecordatorioLlamadaBean recordatorioLlamadaBean = new RecordatorioLlamadaBean();

        try {
            recordatorioLlamadaBean.setIdRecordatorioLlamada(Integer.parseInt(recordatorioLlamadaResponse.getIdRecordatorioLlamada()));
        }catch(Exception e){}
        try {
            recordatorioLlamadaBean.setIdRecordatorioLlamadaDispositivo(Integer.parseInt(recordatorioLlamadaResponse.getIdRecordatorioLlamadaDispositivo()));
        }catch(Exception e){}
        try {
            recordatorioLlamadaBean.setIdProspecto(Integer.parseInt(recordatorioLlamadaResponse.getIdProspecto()));
        }catch(Exception e){}
        try {
            recordatorioLlamadaBean.setIdCita(Integer.parseInt(recordatorioLlamadaResponse.getIdCita()));
        }catch(Exception e){}
        try {
            recordatorioLlamadaBean.setIdProspectoDispositivo(Integer.parseInt(recordatorioLlamadaResponse.getIdProspectoDispositivo()));
        }catch(Exception e){}
        try {
            recordatorioLlamadaBean.setIdCitaDispositivo(Integer.parseInt(recordatorioLlamadaResponse.getIdCitaDispositivo()));
        }catch(Exception e){}

        recordatorioLlamadaBean.setFechaRecordatorio(recordatorioLlamadaResponse.getFechaRecordatorio());
        recordatorioLlamadaBean.setFlagActivo(recordatorioLlamadaResponse.getFlagActivo().toLowerCase().equals("true") ? 1 : 0);
        recordatorioLlamadaBean.setFechaCreacionDispositivo(recordatorioLlamadaResponse.getFechaCreacionDispositivo());
        recordatorioLlamadaBean.setFechaModificacionDispositivo(recordatorioLlamadaResponse.getFechaModificacionDispositivo());
        recordatorioLlamadaBean.setFlagEnviado(2);
        return recordatorioLlamadaBean;
    }

    public static ReunionInternaBean transform(ReunionResponse reunionResponse){
        ReunionInternaBean reunionBean = new ReunionInternaBean();
        
        try {
            reunionBean.setIdReunionInterna(Integer.parseInt(reunionResponse.getIdReunion()));
        }catch(Exception e){}
        try {
            reunionBean.setIdReunionInternaDispositivo(Integer.parseInt(reunionResponse.getIdReunionDispositivo()));
        }catch(Exception e){}
        reunionBean.setFechaReunion(reunionResponse.getFechaReunion());
        reunionBean.setHoraInicio(reunionResponse.getHoraInicio());
        reunionBean.setHoraFin(reunionResponse.getHoraFin());
        reunionBean.setUbicacion(reunionResponse.getUbicacion());
        try {
            reunionBean.setFlagInvitadoGU(Integer.parseInt(reunionResponse.getFlagInvitadoGU()));
        }catch(Exception e){}
        try {
            reunionBean.setFlagInvitadoGA(Integer.parseInt(reunionResponse.getFlagInvitadoGA()));
        }catch(Exception e){}
        reunionBean.setRecordatorio(reunionResponse.getRecordatorio());
        reunionBean.setCodigoTipoReunion(reunionResponse.getCodigoTipoReunion());
        reunionBean.setFechaCreacionDispositivo(reunionResponse.getFechaCreacionDispositivo());
        reunionBean.setFechaModificacionDispositivo(reunionResponse.getFechaModificacionDispositivo());
        reunionBean.setFlagEnviado(2);
        return reunionBean;
    }
    public static TablaIndiceBean transform(TablaIndiceResponse tablaIndiceResponse){
        TablaIndiceBean tablaIndiceBean = new TablaIndiceBean();
        tablaIndiceBean.setIdTabla(Integer.parseInt(tablaIndiceResponse.getIdTabla()));
        tablaIndiceBean.setNombreTabla(tablaIndiceResponse.getNombreTabla());
        return tablaIndiceBean;
    }
    public static TablaTablasBean transform(TablaTablasResponse tablaTablasResponse){
        TablaTablasBean tablaTablasBean = new TablaTablasBean();
        tablaTablasBean.setIdTabla(Integer.parseInt(tablaTablasResponse.getIdTabla()));
        tablaTablasBean.setCodigoCampo(Integer.parseInt(tablaTablasResponse.getCodigoCampo()));
        tablaTablasBean.setValorCadena(tablaTablasResponse.getValorCadena());
        tablaTablasBean.setValorNumerico(Double.parseDouble(tablaTablasResponse.getValorNumerico()));
        try{
            tablaTablasBean.setFlagActivo(tablaTablasResponse.getFlagActivo().toLowerCase().equals("true") ? 1 : 0);
        }catch(Exception e){
            tablaTablasBean.setFlagActivo(1);
        }
        return tablaTablasBean;
    }

    public static ArrayList<AdnBean> transformListAdn(List<ADNResponse> listaAdnResponse){
        ArrayList<AdnBean> listaAdnBean = new ArrayList<>();
        if(listaAdnResponse != null && !listaAdnResponse.isEmpty()){
            for(ADNResponse adnResponse : listaAdnResponse) {
                AdnBean adnBean = transform(adnResponse);
                if (adnBean != null){
                    listaAdnBean.add(adnBean);
                }
            }
        }
        return listaAdnBean;
    }
    public static ArrayList<CalendarioBean> transformListCalendario(List<CalendarioResponse> listaCalendarioResponse) {
        ArrayList<CalendarioBean> listaCalendarioBean = new ArrayList<>();
        if (listaCalendarioResponse != null && !listaCalendarioResponse.isEmpty()) {
            for (CalendarioResponse calendarioResponse : listaCalendarioResponse) {
                listaCalendarioBean.add(transform(calendarioResponse));
            }
        }
        return listaCalendarioBean;
    }
    public static ArrayList<CitaBean> transformListCita(List<CitaResponse> listaCitaResponse){
        ArrayList<CitaBean> listaCitaBean = new ArrayList<>();
        if (listaCitaResponse != null && !listaCitaResponse.isEmpty()) {
            for (CitaResponse citaResponse : listaCitaResponse) {
                listaCitaBean.add(transform(citaResponse));
            }
        }
        return listaCitaBean;
    }
    public static ArrayList<CitaMovimientoEstadoBean> transformListCitaMovimientoEstado(List<CitaMovimientoEstadoResponse> listaCitaMovimientoEstadoResponse) {
        ArrayList<CitaMovimientoEstadoBean> listaCitaMovimientoEstadoBean = new ArrayList<>();
        if (listaCitaMovimientoEstadoResponse != null && !listaCitaMovimientoEstadoResponse.isEmpty()) {
            for (CitaMovimientoEstadoResponse citaMovimientoEstadoResponse : listaCitaMovimientoEstadoResponse) {
                listaCitaMovimientoEstadoBean.add(transform(citaMovimientoEstadoResponse));
            }
        }
        return listaCitaMovimientoEstadoBean;
    }
    public static ArrayList<EntidadBean> transformListEntidad(List<EntidadResponse> listaEntidadResponse){
        ArrayList<EntidadBean> listaEntidadBean = new ArrayList<>();
        if(listaEntidadResponse != null) {
            for(EntidadResponse entidadResponse : listaEntidadResponse){
                listaEntidadBean.add(transform(entidadResponse));
            }
        }
        return listaEntidadBean;
    }
    public static ArrayList<FamiliarBean> transformListFamiliar(List<FamiliarResponse> listaFamiliarResponse){
        ArrayList<FamiliarBean> listaFamiliarBean = new ArrayList<>();
        if(listaFamiliarResponse != null) {
            for(FamiliarResponse familiarResponse : listaFamiliarResponse){
                listaFamiliarBean.add(transform(familiarResponse));
            }
        }
        return listaFamiliarBean;
    }
    public static ArrayList<MensajeSistemaBean> transformListMensajeSistema(List<MensajeSistemaResponse> listaMensajeSistemaResponse) {
        ArrayList<MensajeSistemaBean> listaMensajeSistemaBean = new ArrayList<>();
        if (listaMensajeSistemaResponse != null && !listaMensajeSistemaResponse.isEmpty()) {
            for (MensajeSistemaResponse mensajeSistemaResponse : listaMensajeSistemaResponse) {
                listaMensajeSistemaBean.add(transform(mensajeSistemaResponse));
            }
        }
        return listaMensajeSistemaBean;
    }
    public static ArrayList<ParametroBean> transformListParametro(List<ParametroResponse> listaParametroResponse){
        ArrayList<ParametroBean> listaParametroBean = new ArrayList<>();
        if(listaParametroResponse != null && !listaParametroResponse.isEmpty()){
            for (ParametroResponse parametroResponse : listaParametroResponse) {
                listaParametroBean.add(transform(parametroResponse));
            }
        }
        return listaParametroBean;
    }
    public static ArrayList<ProspectoBean> transformListProspecto(List<ProspectoResponse> listaProspectoResponse){
        ArrayList<ProspectoBean> listaProspectoBean = new ArrayList<>();
        if(listaProspectoResponse != null){
            for (ProspectoResponse prospectoResponse : listaProspectoResponse) {
                listaProspectoBean.add(transform(prospectoResponse));
            }
        }
        return listaProspectoBean;
    }
    public static ArrayList<ProspectoMovimientoEtapaBean> transformListProspectoMovimientoEtapa(List<ProspectoMovimientoEtapaResponse> listaProspectoMovimientoEtapaResponse) {
        ArrayList<ProspectoMovimientoEtapaBean> listaProspectoMovimientoEtapaBean = new ArrayList<>();
        if (listaProspectoMovimientoEtapaResponse != null && !listaProspectoMovimientoEtapaResponse.isEmpty()) {
            for (ProspectoMovimientoEtapaResponse prospectoMovimientoEtapaResponse : listaProspectoMovimientoEtapaResponse) {
                listaProspectoMovimientoEtapaBean.add(transform(prospectoMovimientoEtapaResponse));
            }
        }
        return listaProspectoMovimientoEtapaBean;
    }
    public static ArrayList<ReferidoBean> transformListReferido(List<ReferidoResponse> listaReferidoResponse){
        ArrayList<ReferidoBean> listaReferidoBean = new ArrayList<>();
        if (listaReferidoResponse != null){
            for (ReferidoResponse referidoResponse : listaReferidoResponse){
                listaReferidoBean.add(transform(referidoResponse));
            }
        }
        return listaReferidoBean;
    }
    public static ArrayList<RecordatorioLlamadaBean> transformListRecordatorioLlamadaBean(List<RecordatorioLlamadaResponse> listaRecordatorioLlamadaResponse){
        ArrayList<RecordatorioLlamadaBean> listaRecordatorioLlamadaBean = new ArrayList<>();
        if (listaRecordatorioLlamadaResponse != null){
            for (RecordatorioLlamadaResponse recordatorioLLamadaResponse : listaRecordatorioLlamadaResponse){
                listaRecordatorioLlamadaBean.add(transform(recordatorioLLamadaResponse));
            }
        }
        return listaRecordatorioLlamadaBean;
    }
    public static ArrayList<ReunionInternaBean> transformListReunion(List<ReunionResponse> listaReunionResponse){
        ArrayList<ReunionInternaBean> listaReunionBean = new ArrayList<>();
        if (listaReunionResponse != null && !listaReunionResponse.isEmpty()) {
            for (ReunionResponse reunionResponse : listaReunionResponse) {
                listaReunionBean.add(transform(reunionResponse));
            }
        }
        return listaReunionBean;
    }
    public static ArrayList<TablaIndiceBean> transformListTablaIndice(List<TablaIndiceResponse> listaTablaIndiceResponse) {
        ArrayList<TablaIndiceBean> listaTablaIndiceBean = new ArrayList<>();
        if (listaTablaIndiceResponse != null && !listaTablaIndiceResponse.isEmpty()) {
            for (TablaIndiceResponse tablaIndiceResponse : listaTablaIndiceResponse) {
                listaTablaIndiceBean.add(transform(tablaIndiceResponse));
            }
        }
        return listaTablaIndiceBean;
    }
    public static ArrayList<TablaTablasBean> transformListTablaTablas(List<TablaTablasResponse> listaTablaTablasResponse) {
        ArrayList<TablaTablasBean> listaTablaTablasBean = new ArrayList<>();
        if (listaTablaTablasResponse != null && !listaTablaTablasResponse.isEmpty()) {
            for (TablaTablasResponse tablaTablasResponse : listaTablaTablasResponse) {
                listaTablaTablasBean.add(transform(tablaTablasResponse));
            }
        }
        return listaTablaTablasBean;
    }
}