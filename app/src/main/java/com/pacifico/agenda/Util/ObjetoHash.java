package com.pacifico.agenda.Util;

import com.pacifico.agenda.Network.Request.SetData.ADNRequest;
import com.pacifico.agenda.Network.Request.SetData.CitaMovimientoEstadoRequest;
import com.pacifico.agenda.Network.Request.SetData.CitaRequest;
import com.pacifico.agenda.Network.Request.SetData.FamiliarRequest;
import com.pacifico.agenda.Network.Request.SetData.ProspectoMovimientoEtapaRequest;
import com.pacifico.agenda.Network.Request.SetData.ProspectoRequest;
import com.pacifico.agenda.Network.Request.SetData.RecordatorioLLamadaRequest;
import com.pacifico.agenda.Network.Request.SetData.ReferidoRequest;
import com.pacifico.agenda.Network.Request.SetData.ReunionInternaRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DSB on 01/07/2016.
 */
public class ObjetoHash {
    private List<ReunionInternaRequest> Reunion;
    private List<ProspectoRequest> Prospecto;
    private List<ProspectoMovimientoEtapaRequest> ProspectoMovimientoEtapa;
    private List<ADNRequest> ADN;
    private List<FamiliarRequest> Familiar;
    private List<CitaRequest> Cita;
    private List<CitaMovimientoEstadoRequest> CitaMovimientoEstado;
    private List<ReferidoRequest> Referido;
    private List<RecordatorioLLamadaRequest> RecordatorioLlamada;
    public ObjetoHash(){
        Reunion = new ArrayList<>();
        Prospecto = new ArrayList<>();
        ProspectoMovimientoEtapa = new ArrayList<>();
        ADN = new ArrayList<>();
        Familiar = new ArrayList<>();
        Cita = new ArrayList<>();
        CitaMovimientoEstado = new ArrayList<>();
        Referido = new ArrayList<>();
        RecordatorioLlamada = new ArrayList<>();
    }
    public void setReunion(List<ReunionInternaRequest> reunion) {
        Reunion = reunion;
    }
    public void setProspecto(List<ProspectoRequest> prospecto) {
        Prospecto = prospecto;
    }
    public void setProspectoMovimientoEtapa(List<ProspectoMovimientoEtapaRequest> prospectoMovimientoEtapa) {
        ProspectoMovimientoEtapa = prospectoMovimientoEtapa;
    }
    public void setADN(List<ADNRequest> ADN) {
        this.ADN = ADN;
    }
    public void setFamiliar(List<FamiliarRequest> familiar) {
        this.Familiar = familiar;
    }
    public void setCita(List<CitaRequest> cita) {
        this.Cita = cita;
    }
    public void setCitaMovimientoEstado(List<CitaMovimientoEstadoRequest> citaMovimientoEstado) {
        this.CitaMovimientoEstado = citaMovimientoEstado;
    }
    public void setReferido(List<ReferidoRequest> referido) {
        this.Referido = referido;
    }
    public void setRecordatorioLlamada(List<RecordatorioLLamadaRequest> recordatorioLlamada) {
        this.RecordatorioLlamada = recordatorioLlamada;
    }
}