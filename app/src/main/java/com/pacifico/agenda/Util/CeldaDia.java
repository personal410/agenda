package com.pacifico.agenda.Util;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import com.pacifico.agenda.R;

/**
 * Created by DSB on 14/06/2016.
 */
public class CeldaDia extends CeldaView{

    public LinearLayout  lineVertical;
    public TextView txt, seg;

    public CeldaDia(Context context, int colorBackground, int colorText){
        view = LayoutInflater.from(context).inflate(R.layout.celda_cita, null);
        linHorizontal = (LinearLayout) view.findViewById(R.id.linHorizontal);
        linHorizontal.setBackgroundColor(colorBackground);
        lineVertical = (LinearLayout) view.findViewById(R.id.lineVertical);
        lineVertical.setBackgroundColor(colorText);
        spaceBefore = (Space)view.findViewById(R.id.spaceBefore);
        txt = (TextView)view.findViewById(R.id.pri);
        txt.setTextSize(14);
        txt.setTextColor(colorText);
        seg = (TextView)view.findViewById(R.id.seg);
        seg.setTextColor(colorText);
        seg.setTextSize(14);

    }


}
