package com.pacifico.agenda.Util;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import com.pacifico.agenda.R;

/**
 * Created by DSB on 14/06/2016.
 */
public class CeldaRecordatorio extends CeldaView {


    public ImageView image;
    public TextView txt;

    public CeldaRecordatorio(Context context){
        view = LayoutInflater.from(context).inflate(R.layout.celda_recordatorio, null);
        linHorizontal = (LinearLayout) view.findViewById(R.id.linHorizontal);
        spaceBefore = (Space)view.findViewById(R.id.spaceBefore);
        txt = (TextView)view.findViewById(R.id.txt);
        txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        image= (ImageView)view.findViewById(R.id.image);

    }


}
