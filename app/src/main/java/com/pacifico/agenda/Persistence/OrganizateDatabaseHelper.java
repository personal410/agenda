package com.pacifico.agenda.Persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.dsbmobile.dsbframework.controller.persistence.DatabaseHelper;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.agenda.Model.Bean.AdnBean;
import com.pacifico.agenda.Model.Bean.AjustesBean;
import com.pacifico.agenda.Model.Bean.CalendarioBean;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.CitaHistoricaBean;
import com.pacifico.agenda.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.agenda.Model.Bean.EntidadBean;
import com.pacifico.agenda.Model.Bean.ReminderBean;
import com.pacifico.agenda.Model.Bean.TablaIdentificadorBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.agenda.Model.Bean.DispositivoBean;
import com.pacifico.agenda.Model.Bean.FamiliarBean;
import com.pacifico.agenda.Model.Bean.IntermediarioBean;
import com.pacifico.agenda.Model.Bean.MensajeSistemaBean;
import com.pacifico.agenda.Model.Bean.ParametroBean;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Model.Bean.ReferidoBean;
import com.pacifico.agenda.Model.Bean.ReunionInternaBean;
import com.pacifico.agenda.Model.Bean.TablaIndiceBean;
import com.pacifico.agenda.Model.Bean.TablaTablasBean;

import java.util.ArrayList;

public class OrganizateDatabaseHelper extends DatabaseHelper {

	private ArrayList<TableHelper> tableHelpers;

	public OrganizateDatabaseHelper(Context context, String databaseName,
									int databaseVersion) {

		super(context, databaseName, databaseVersion);
		tableHelpers = new ArrayList<TableHelper>();
		tableHelpers.add(AdnBean.tableHelper);
		tableHelpers.add(CalendarioBean.tableHelper);
		tableHelpers.add(CitaBean.tableHelper);
		tableHelpers.add(CitaHistoricaBean.tableHelper);
		tableHelpers.add(CitaMovimientoEstadoBean.tableHelper);
		tableHelpers.add(ProspectoBean.tableHelper);
		tableHelpers.add(ProspectoMovimientoEtapaBean.tableHelper);
		tableHelpers.add(DispositivoBean.tableHelper);
		tableHelpers.add(FamiliarBean.tableHelper);
		tableHelpers.add(IntermediarioBean.tableHelper);
		tableHelpers.add(MensajeSistemaBean.tableHelper);
		tableHelpers.add(ParametroBean.tableHelper);
		tableHelpers.add(ReferidoBean.tableHelper);
		tableHelpers.add(ReunionInternaBean.tableHelper);
		tableHelpers.add(TablaIndiceBean.tableHelper);
		tableHelpers.add(TablaTablasBean.tableHelper);
		tableHelpers.add(TablaIdentificadorBean.tableHelper);
		tableHelpers.add(RecordatorioLlamadaBean.tableHelper);
		tableHelpers.add(ReminderBean.tableHelper);
		tableHelpers.add(EntidadBean.tableHelper);
		tableHelpers.add(AjustesBean.tableHelper);
	}

	@Override
	public void executeCreates(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.beginTransaction();

		for (TableHelper tableHelper : tableHelpers) {
			db.execSQL(tableHelper.getCreateSentence());
		}
		db.setTransactionSuccessful();
		db.endTransaction();

	}

	@Override
	public void executeDrops(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.beginTransaction();
		for (TableHelper tableHelper : tableHelpers) {
			db.execSQL(tableHelper.getDropSentence());
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}

}
