package com.pacifico.agenda.Persistence;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.EntityFactory;
import com.pacifico.agenda.Model.Bean.AdnBean;
import com.pacifico.agenda.Model.Bean.AjustesBean;
import com.pacifico.agenda.Model.Bean.CalendarioBean;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.CitaHistoricaBean;
import com.pacifico.agenda.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.agenda.Model.Bean.DispositivoBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.agenda.Model.Bean.FamiliarBean;
import com.pacifico.agenda.Model.Bean.IntermediarioBean;
import com.pacifico.agenda.Model.Bean.MensajeSistemaBean;
import com.pacifico.agenda.Model.Bean.ParametroBean;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Model.Bean.ReferidoBean;
import com.pacifico.agenda.Model.Bean.ReminderBean;
import com.pacifico.agenda.Model.Bean.ReunionInternaBean;
import com.pacifico.agenda.Model.Bean.TablaIdentificadorBean;
import com.pacifico.agenda.Model.Bean.TablaIndiceBean;
import com.pacifico.agenda.Model.Bean.TablaTablasBean;


public class OrganizateEntityFactory extends EntityFactory {

	@Override
	public Entity newEntity(String tableName) {
		// TODO Auto-generated method stub

		if (tableName.equals(DatabaseConstants.TBL_ADN)) {
			return new AdnBean();
		}
		
		if (tableName.equals(DatabaseConstants.TBL_CALENDARIO)) {
			return new CalendarioBean();
		}
		if (tableName.equals(DatabaseConstants.TBL_CITA)) {
			return new CitaBean();
		}
		
		if (tableName.equals(DatabaseConstants.TBL_CITA_HISTORICA)) {
			return new CitaHistoricaBean();
		}
		
		if (tableName.equals(DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO)) {
			return new CitaMovimientoEstadoBean();
		}
		
		if (tableName.equals(DatabaseConstants.TBL_PROSPECTO)) {
			return new ProspectoBean();
		}
		
		if (tableName.equals(DatabaseConstants.TBL_PROSPECTO_MOVIMIENTO_ETAPA)) {
			return new ProspectoMovimientoEtapaBean();
		}
		
		if (tableName.equals(DatabaseConstants.TBL_DISPOSITIVO)) {
			return new DispositivoBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_FAMILIAR)) {
			return new FamiliarBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_INTERMEDIARIO)) {
			return new IntermediarioBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_MENSAJE_SISTEMA)) {
			return new MensajeSistemaBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_PARAMETRO)) {
			return new ParametroBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_REFERIDO)) {
			return new ReferidoBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_REUNION_INTERNA)){
			return new ReunionInternaBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_TABLA_TABLAS)){
			return new TablaTablasBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_TABLA_INDICE)){
			return new TablaIndiceBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_RECORDATORIO_LLAMADA)){
			return new RecordatorioLlamadaBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_IDENTIFICADOR)){
			return new TablaIdentificadorBean();
		}

		if (tableName.equals(DatabaseConstants.TBL_RECORDATORIO_GENERAL)){
			return new ReminderBean();
		}
		if (tableName.equals(DatabaseConstants.TBL_AJUSTES)){
			return new AjustesBean();
		}

		return null;
	}

}
