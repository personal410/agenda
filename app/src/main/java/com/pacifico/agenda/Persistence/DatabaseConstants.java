package com.pacifico.agenda.Persistence;

import android.os.Environment;

public class DatabaseConstants {

	public static String DATABASE_NAME = "DB_ORGANIZATE_PACIFICO";
	public static String DATABASE_PATH = Environment
			.getExternalStorageDirectory().toString();
	public static String DATABASE_DIRECTORY = "PACIFICO_ORGANIZATE";
	public static int DATABASE_VERSION = 1;

	//public static final String USUARIO_TABLE_NAME = "USUARIO";
	public static final String TBL_ADN= "TBL_ADN";
	public static final String TBL_AJUSTES = "TBL_AJUSTES";
	public static final String TBL_CITA_HISTORICA= "TBL_CITA_HISTORICA";
	public static final String TBL_CITA_MOVIMIENTO_ESTADO = "TBL_CITA_MOVIMIENTO_ESTADO";
	public static final String TBL_PROSPECTO = "TBL_PROSPECTO";
	public static final String TBL_PROSPECTO_MOVIMIENTO_ETAPA = "TBL_PROSPECTO_MOVIMIENTO_ETAPA";
	public static final String TBL_CALENDARIO = "TBL_CALENDARIO";
	public static final String TBL_CITA = "TBL_CITA";
	public static final String TBL_DISPOSITIVO = "TBL_DISPOSITIVO";
	public static final String TBL_FAMILIAR = "TBL_FAMILIAR";
	public static final String TBL_INTERMEDIARIO = "TBL_INTERMEDIARIO";
	public static final String TBL_MENSAJE_SISTEMA = "TBL_MENSAJE_SISTEMA";
	public static final String TBL_PARAMETRO = "TBL_PARAMETRO";
	public static final String TBL_REFERIDO = "TBL_REFERIDO";
	public static final String TBL_REUNION_INTERNA = "TBL_REUNION_INTERNA";
	public static final String TBL_TABLA_TABLAS = "TBL_TABLA_TABLAS";
	public static final String TBL_TABLA_INDICE = "TBL_TABLA_INDICE";
	//public static final String TBL_IDENTITY_TABLA = "TBL_IDENTITY_TABLA";
	public static final String TBL_IDENTIFICADOR = "TBL_IDENTIFICADOR";
	public static final String TBL_ENTIDAD = "TBL_ENTIDAD";
	public static final String TBL_RECORDATORIO_LLAMADA = "TBL_RECORDATORIO_LLAMADA";
	public static final String TBL_RECORDATORIO_GENERAL = "TBL_RECORDATORIO_GENERAL";
}