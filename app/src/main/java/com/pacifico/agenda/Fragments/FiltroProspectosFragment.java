package com.pacifico.agenda.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoDatosReferidoBean;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joel on 8/21/16.
 */
public class FiltroProspectosFragment extends Fragment {

    private android.widget.ImageButton btnRetrocederFiltroProspecto;
    private android.widget.EditText edtFiltroProspecto;
    private ListView lvLista;
    private FiltroProspectoAdapter adapter;

    public static FiltroProspectosFragment newInstance() {
        FiltroProspectosFragment fragment = new FiltroProspectosFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View mView = inflater.inflate(R.layout.fragment_filtro_prospectos, container, false);

        this.lvLista = (ListView) mView.findViewById(R.id.lvLista);
        this.edtFiltroProspecto = (EditText) mView.findViewById(R.id.edtFiltroProspecto);
        this.btnRetrocederFiltroProspecto = (ImageButton) mView.findViewById(R.id.btnRetrocederFiltroProspecto);

        btnRetrocederFiltroProspecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        this.edtFiltroProspecto.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                      int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                if (adapter != null)
                    adapter.getFilter().filter(
                            edtFiltroProspecto.getText().toString());
            }
        });

        obtenerProspectos();
        return mView;
    }

    private void obtenerProspectos(){

        ArrayList<ProspectoDatosReferidoBean> listaProspectos = new ArrayList<>();
        listaProspectos = ProspectoController.obtenerProspectosReferidosUltCita();

        adapter = new FiltroProspectoAdapter(getActivity(), R.layout.row_prospectos_dia, listaProspectos);
        lvLista.setAdapter(adapter);
    }
        /// EL ADAPTER
        class FiltroProspectoAdapter extends ArrayAdapter<ProspectoDatosReferidoBean> {

            private Activity context;
            private int resource;
            private List<ProspectoDatosReferidoBean> items;
            ArrayList<ProspectoDatosReferidoBean> originalList;
            ArrayList<ProspectoDatosReferidoBean> filtroList;
            ProspectoFilter filter;

            public FiltroProspectoAdapter(Activity context, int resource, List<ProspectoDatosReferidoBean> items) {
                super(context, resource, items);
                this.context = context;
                this.resource = resource;
                this.items = items;

                filtroList = new ArrayList<ProspectoDatosReferidoBean>();
                filtroList.addAll(items);

                originalList = new ArrayList<ProspectoDatosReferidoBean>();
                originalList.addAll(items);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final ProspectoDatosReferidoBean item = items.get(position);
                final LinearLayout rowView;

                rowView = new LinearLayout(getContext());
                inflater.inflate(resource, rowView, true);

                ImageView ivProspecto = (ImageView) rowView.findViewById(R.id.ivProspecto);
                TextView tvProspecto = (TextView) rowView.findViewById(R.id.tvProspecto);
                TextView tvReferencia = (TextView) rowView.findViewById(R.id.tvReferencia);

                String nombreCompleto = String.format("%s %s %s",
                        Util.capitalizedString(item.getNombres()),
                        Util.capitalizedString(item.getApellidoPaterno()),
                        Util.capitalizedString(item.getApellidoMaterno())
                ).trim();

                tvProspecto.setText(nombreCompleto);

                if (item.getIdReferenciadorDispositivo() != -1)
                {
                    String nombreCompletoReferenciador = String.format("%s %s %s",
                            Util.capitalizedString(item.getNombresReferenciador()),
                            Util.capitalizedString(item.getApellidoPaternoReferenciador()),
                            Util.capitalizedString(item.getApellidoMaternoReferenciador())
                    ).trim();

                    tvReferencia.setText("Ref. " + nombreCompletoReferenciador);
                }else{
                    if (item.getNombreFuente() == null) item.setNombreFuente("");
                    tvReferencia.setText(item.getNombreFuente());
                }

                // SI es un prospecto en primera etapa al que aun no se le crea cita no entra a ninguno y va a pintar nuevo

                if (item.EstadoProspecto == Constantes.EstadoProspecto_Nuevo) {
                    ivProspecto.setImageResource(R.drawable.ic_action_action_info_outline);

                }
                else if (item.EstadoProspecto == Constantes.EstadoProspecto_NoInteresado)
                {
                    ivProspecto.setImageResource(R.drawable.cancel);
                }
                else if(item.EstadoProspecto == Constantes.EstadoProspecto_CierreVenta)
                {
                    ivProspecto.setImageResource(R.drawable.ic_action_ic_done_all_blue);
                }
                else // SI es en proceso se considera la ultima cita
                {

                    if (item.EstadoUltCita == Constantes.EstadoCita_PorContactar)
                        ivProspecto.setImageResource(R.drawable.ic_telefono_morado);
                    else if (item.EstadoUltCita == Constantes.EstadoCita_Agendada) {
                        if (item.getEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista)
                            ivProspecto.setImageResource(R.drawable.primera_entrevista);
                        else if (item.getEtapaProspecto() == Constantes.EtapaProspecto_SegundaEntrevista)
                            ivProspecto.setImageResource(R.drawable.segunda_entrevista);
                        else if (item.getEtapaProspecto() == Constantes.EtapaProspecto_EntrevistaAdicional)
                            ivProspecto.setImageResource(R.drawable.entrevista_adicional);
                        else
                            ivProspecto.setImageResource(R.drawable.calendario_gris_oscuro);
                    }
                    else if (item.EstadoUltCita == Constantes.EstadoCita_Realizada)
                        ivProspecto.setImageResource(R.drawable.ic_action_ic_done_all_black_24dp);
                }

                rowView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(item.getIdProspectoDispositivo());
                        ((CalendarioActivity)context).GestionFlujoProspecto(prospectoBean);
                    }
                });

                // El Click del textview bloquea el linea

                return rowView;
            }

            public Filter getFilter() {

                if (filter == null)
                    filter = new ProspectoFilter();

                return filter;
            }

            class ProspectoFilter extends Filter {

                public ProspectoFilter() {

                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    constraint = constraint.toString().toUpperCase();

                    FilterResults result = new FilterResults();
                    if (constraint != null && constraint.toString().length() > 0) {
                        ArrayList<ProspectoDatosReferidoBean> filteredItems = new ArrayList<ProspectoDatosReferidoBean>();

                        for (int i = 0, l = originalList.size(); i < l; i++) {
                            String texto = "";

                            texto = Util.strNULL(originalList.get(i).getNombres())+" " + Util.strNULL(originalList.get(i).getApellidoPaterno())+ " "+ Util.strNULL(originalList.get(i).getApellidoMaterno());

                            if (texto.indexOf(constraint.toString().toUpperCase()) != -1)
                                filteredItems.add(originalList.get(i));
                        }
                        result.count = filteredItems.size();
                        result.values = filteredItems;
                    } else {
                        synchronized (this) {
                            result.values = originalList;
                            result.count = originalList.size();
                        }
                    }
                    return result;
                }

                @Override
                protected void publishResults(CharSequence constraint,
                                              FilterResults results) {
                    // TODO Auto-generated method stub
                    filtroList = (ArrayList<ProspectoDatosReferidoBean>) results.values;
                    notifyDataSetChanged();
                    clear();
                    for (int i = 0, l = filtroList.size(); i < l; i++) {
                        add(filtroList.get(i));
                        notifyDataSetInvalidated();
                    }
                }
            }
        }
}
