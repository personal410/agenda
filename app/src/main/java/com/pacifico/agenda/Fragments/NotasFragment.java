package com.pacifico.agenda.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;

/**
 * Created by Joel on 08/05/2016.
 */
public class NotasFragment extends Fragment {

    ImageButton btnRetrocederNotas;
    EditText edtNotas;
    String valorOriginal;
    LinearLayout linCabecera;

    private CalendarioActivity calendarioActivity;

    public static NotasFragment newInstance() {
        NotasFragment fragment = new NotasFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_notas, container, false);
        linCabecera = (LinearLayout) linfragment.findViewById(R.id.linCabeceraEntrevistaNotas);
        btnRetrocederNotas = (ImageButton) linfragment.findViewById(R.id.btnRetrocederNotas);

        FormatearTarjeta();

        btnRetrocederNotas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //calendarioActivity.citaActual.setNotas(edtNotas.getText().toString());

                getFragmentManager().popBackStack();
                ((CalendarioActivity)getActivity()).OcultarTeclado();

                String notasNueva = edtNotas.getText().toString().trim();

                if (!notasNueva.equals(valorOriginal)) {
                    if (notasNueva == "") notasNueva = null;
                    calendarioActivity.prospectoActual.setNota(notasNueva);
                    calendarioActivity.citaModificoProspecto = true;
                }
            }
        });

        edtNotas = (EditText) linfragment.findViewById(R.id.edtNotas);
        edtNotas.requestFocus();

        CargarDatosInicial();

        // Inflate the layout for this fragment
        return linfragment;
    }

    public void CargarDatosInicial() {

        valorOriginal = calendarioActivity.prospectoActual.getNota();

        if (calendarioActivity.prospectoActual.getNota() != null)
            edtNotas.setText(calendarioActivity.prospectoActual.getNota());
    }

    public void FormatearTarjeta()
    {
        if (calendarioActivity.citaActual == null)
        {
            if (calendarioActivity.prospectoActual.getCodigoEstado() == Constantes.EstadoProspecto_NoInteresado)
                linCabecera.setBackgroundColor(getResources().getColor(R.color.colorGris));
            else if(calendarioActivity.prospectoActual.getCodigoEstado() == Constantes.EstadoProspecto_Nuevo)
                linCabecera.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }else {

            if (calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista) {
                linCabecera.setBackgroundColor(getResources().getColor(R.color.color_primera_entrevista));
            } else if (calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_SegundaEntrevista) {
                linCabecera.setBackgroundColor(getResources().getColor(R.color.color_segunda_entrevista));
            } else if (calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_EntrevistaAdicional) {
                linCabecera.setBackgroundColor(getResources().getColor(R.color.color_entrevista_adicional));
            }
        }
    }

}
