package com.pacifico.agenda.Fragments;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Activity.InicioSesionActivity;
import com.pacifico.agenda.Model.Bean.ParametroBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.TablaTablasBean;
import com.pacifico.agenda.Model.Controller.ParametroController;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.Model.Controller.TablasGeneralesController;
import com.pacifico.agenda.Network.RespuestaSincronizacionListener;
import com.pacifico.agenda.Network.SincronizacionController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Views.Dialogs.DialogOpcionesTablaTablas;
import com.pacifico.agenda.Views.Dialogs.IDialogSiNo;
import com.pacifico.agenda.Views.Dialogs.IDialogSiNoValor;

import org.joda.time.DateTime;
import org.joda.time.Years;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * Created by Joel on 09/05/2016.
 */
public class NuevoProspectoFragment extends Fragment  implements IDialogSiNoValor {

    private CalendarioActivity calendarioActivity;

    boolean flag_click = false;
    ImageButton btnCerrarNuevosProspectos;
    EditText edtNombreProspecto;
    EditText edtApellidoPaternoProspecto;
    EditText edtApellidoMaternoProspecto;
    EditText edtTelefonoProspecto;
    EditText edtCorreoElectronicoProspecto;
    Button btnGuardarProspecto;
    Button btnRangoIngresosMensualesProspecto;
    Button  btnFuenteProspecto;
    Button btnFechaNacimientoProspecto;
    RadioButton rdbHijoProspectoNo;
    RadioButton rdbHijoProspectoSi;
    RadioGroup rdgHijosProspecto;
    TextView tvNotaNuevo;
    private ProgressDialog progressDialog;

    private Calendar calFechaNacimientoProspecto;
    private Calendar calMaxFecNac = Calendar.getInstance();
    private Calendar calMinFecNac = Calendar.getInstance();
    //private int rangoIngresos= -1;
    //private int fuente= -1;

    public static NuevoProspectoFragment newInstance() {
        NuevoProspectoFragment fragment = new NuevoProspectoFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();
        calMaxFecNac.add(Calendar.YEAR, -18);
        calMinFecNac.add(Calendar.YEAR, -100);

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_nuevo_prospecto, container, false);

        btnCerrarNuevosProspectos = (ImageButton) linfragment.findViewById(R.id.btnCerrarNuevosProspectos);
        edtNombreProspecto = (EditText) linfragment.findViewById(R.id.edtNombreProspecto);
        edtApellidoPaternoProspecto = (EditText) linfragment.findViewById(R.id.edtApellidoPaternoProspecto);
        edtApellidoMaternoProspecto = (EditText) linfragment.findViewById(R.id.edtApellidoMaternoProspecto);
        edtTelefonoProspecto = (EditText)  linfragment.findViewById(R.id.edtTelefonoProspecto);
        edtCorreoElectronicoProspecto = (EditText)  linfragment.findViewById(R.id.edtCorreoElectronicoProspecto);
        btnRangoIngresosMensualesProspecto = (Button) linfragment.findViewById(R.id.btnRangoIngresosMensualesProspecto);
        btnFuenteProspecto = (Button) linfragment.findViewById(R.id.btnFuenteProspecto);
        btnGuardarProspecto  = (Button) linfragment.findViewById(R.id.btnGuardarProspecto);
        btnFechaNacimientoProspecto = (Button) linfragment.findViewById(R.id.btnFechaNacimientoProspecto);
        rdbHijoProspectoNo = (RadioButton) linfragment.findViewById(R.id.rdbHijoProspectoNo);
        rdbHijoProspectoSi = (RadioButton) linfragment.findViewById(R.id.rdbHijoProspectoSi);
        rdgHijosProspecto = (RadioGroup) linfragment.findViewById(R.id.rdgHijosProspecto);
        tvNotaNuevo = (TextView) linfragment.findViewById(R.id.tvNotaNuevo);

        tvNotaNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Se esta llamando Notas
                transaction.replace(R.id.fraFormularioFlotante, NotasNuevoFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        edtNombreProspecto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validarANombre();
                }
            }

        });
        edtApellidoPaternoProspecto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validarApellido();
                }
            }
        });

        edtTelefonoProspecto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validarTelefono();
                }
            }
        });

        edtCorreoElectronicoProspecto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validarCorreo();
                }
            }
        });

        btnRangoIngresosMensualesProspecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getChildFragmentManager();
                DialogOpcionesTablaTablas dialogIngresos = new DialogOpcionesTablaTablas();
                //dialogIngresos.AgregarOpciones(getActivity());
                dialogIngresos.setListener(NuevoProspectoFragment.this);

                // Lista de Ingresos
                ArrayList<TablaTablasBean> listaItems =  TablasGeneralesController.obtenerTablaTablasPorIdTabla(Constantes.RangoIngresos);

                //dialogIngresos.setDatos("Ingresos","Seleccionar","Cancelar",listaItems,rangoIngresos);
                dialogIngresos.setDatos("Ingresos","Seleccionar","Cancelar",listaItems,calendarioActivity.prospectoCreacionTemp.getCodigoRangoIngreso());
                dialogIngresos.setCancelable(false);
                dialogIngresos.show(fm, "dialogo1");
            }
        });

        btnFuenteProspecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getChildFragmentManager();
                DialogOpcionesTablaTablas dialogFuente = new DialogOpcionesTablaTablas();
                //dialogIngresos.AgregarOpciones(getActivity());
                dialogFuente.setListener(NuevoProspectoFragment.this);

                // Lista de Ingresos
                ParametroBean parametroBean= ParametroController.obtenerParametroBeanPorIdParametro(Constantes.parametro_fuentesAgenda);
                String[] listaids = null;

                ArrayList<TablaTablasBean> arrayOpciones = new ArrayList<TablaTablasBean>(); // Se tiene que agregar en el orden que aparecen en la cadena

                if (parametroBean != null) {
                    listaids = parametroBean.getValorCadena().split(",");

                    if (listaids != null && listaids.length >0)
                    {
                        for (int i = 0; i<listaids.length ; i++)
                        {
                            try {
                                int idTablaTabla = Integer.parseInt(listaids[i]);
                                TablaTablasBean tablatablabean = TablasGeneralesController.obtenerTablaTablasPorIdTablaCodigoCampo(Constantes.Fuente,idTablaTabla);
                                if (tablatablabean != null) arrayOpciones.add(tablatablabean);
                            }catch (Exception e){}
                        }
                    }
                }

                //dialogFuente.setDatos("Ingresos","Seleccionar","Cancelar",listaItems,fuente);
                dialogFuente.setDatos("Fuente","Seleccionar","Cancelar",arrayOpciones,calendarioActivity.prospectoCreacionTemp.getCodigoFuente());
                dialogFuente.setCancelable(false);
                dialogFuente.show(fm, "dialogo1");
            }
        });

        calFechaNacimientoProspecto = null;

        btnFechaNacimientoProspecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int anio,mes,dia;
                if (calFechaNacimientoProspecto != null)
                {
                    anio = calFechaNacimientoProspecto.get(Calendar.YEAR);
                    mes = calFechaNacimientoProspecto.get(Calendar.MONTH);
                    dia = calFechaNacimientoProspecto.get(Calendar.DAY_OF_MONTH);
                }else
                {
                    Calendar calini = Calendar.getInstance();
                    anio = calini.get(Calendar.YEAR) - 30;
                    mes = calini.get(Calendar.MONTH);
                    dia = calini.get(Calendar.DAY_OF_MONTH);
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), DatePickerDialog.THEME_HOLO_LIGHT,new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calFechaNacimientoProspecto = Calendar.getInstance();

                        calFechaNacimientoProspecto.set(Calendar.YEAR, year);
                        calFechaNacimientoProspecto.set(Calendar.MONTH, monthOfYear);
                        calFechaNacimientoProspecto.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateEdtFechaNacimiento();
                    }
                }, anio, mes, dia);

                datePickerDialog.getDatePicker().setCalendarViewShown(false);
                datePickerDialog.getDatePicker().setMinDate(calMinFecNac.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(calMaxFecNac.getTimeInMillis());
                datePickerDialog.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
                datePickerDialog.getDatePicker().setCalendarViewShown(false);
                datePickerDialog.setTitle("Seleccione una fecha");
                datePickerDialog.setCancelable(false);
                datePickerDialog.show();
            }
        });

        btnCerrarNuevosProspectos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        btnGuardarProspecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag_click) return;
                flag_click = true;

                calendarioActivity.prospectoCreacionTemp.setCodigoEtapa(Constantes.EtapaProspecto_Nuevo);
                calendarioActivity.prospectoCreacionTemp.setCodigoEstado(Constantes.EstadoProspecto_Nuevo);
                calendarioActivity.prospectoCreacionTemp.setNombres(edtNombreProspecto.getText().toString());
                calendarioActivity.prospectoCreacionTemp.setApellidoPaterno(edtApellidoPaternoProspecto.getText().toString().toUpperCase().trim());
                calendarioActivity.prospectoCreacionTemp.setApellidoMaterno(edtApellidoMaternoProspecto.getText().toString().toUpperCase().trim());
                calendarioActivity.prospectoCreacionTemp.setTelefonoCelular(edtTelefonoProspecto.getText().toString().trim());
                calendarioActivity.prospectoCreacionTemp.setCorreoElectronico1(edtCorreoElectronicoProspecto.getText().toString().trim());

                if (calFechaNacimientoProspecto != null) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.formato_solofecha_envio);
                    calendarioActivity.prospectoCreacionTemp.setFechaNacimiento(simpleDateFormat.format(calFechaNacimientoProspecto.getTime()));

                    Calendar calFechaActual = Calendar.getInstance();
                    int edad = calFechaActual.get(Calendar.YEAR) - calFechaNacimientoProspecto.get(Calendar.YEAR);

                    if (calFechaNacimientoProspecto.get(Calendar.DAY_OF_YEAR) < calFechaActual.get(Calendar.DAY_OF_YEAR))
                        edad = edad -1;

                    if (edad < 25)
                        calendarioActivity.prospectoCreacionTemp.setCodigoRangoEdad(Constantes.RangoEdad_menor25);
                    else if (edad >= 25 && edad <= 30)
                        calendarioActivity.prospectoCreacionTemp.setCodigoRangoEdad(Constantes.RangoEdad_entre25_30);
                    else if (edad >= 31 && edad <= 40)
                        calendarioActivity.prospectoCreacionTemp.setCodigoRangoEdad(Constantes.RangoEdad_entre31_40);
                    else if (edad > 40)
                        calendarioActivity.prospectoCreacionTemp.setCodigoRangoEdad(Constantes.RangoEdad_mayor41);
                }

                if (rdbHijoProspectoSi.isChecked())
                    calendarioActivity.prospectoCreacionTemp.setFlagHijo(1);
                else if (rdbHijoProspectoNo.isChecked())
                    calendarioActivity.prospectoCreacionTemp.setFlagHijo(0);

                calendarioActivity.prospectoCreacionTemp.setFlagEnviado(0);

                if (validarDatos() ) {
                    // Si el id dispositivo es -1 se inserta
                    if (calendarioActivity.prospectoCreacionTemp.getIdProspectoDispositivo() == -1) {
                        if (ProspectoController.guardarProspectoValidacion(calendarioActivity.prospectoCreacionTemp)) { // Si guarda

                            // Llamar al envío de datos #ENVIO
                            SincronizacionController sincronizacionController = new SincronizacionController();
                            if(progressDialog == null){
                                progressDialog = new ProgressDialog(getContext());
                                progressDialog.setTitle("Enviando información");
                                progressDialog.setMessage("Espere, por favor");
                            }
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
                                @Override
                                public void terminoSincronizacion(int codigo, String mensaje) {
                                    progressDialog.dismiss();
                                    if (codigo < 0) {
                                        mostrarMensajeParaContinuar(mensaje);
                                    }else if(codigo == 1){
                                          mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
                                        //continuar();
                                    }else{
                                        if(codigo == 4 || codigo == 6){
                                            Intent inicioSesionIntent = new Intent(getActivity(), InicioSesionActivity.class);
                                            inicioSesionIntent.putExtra("etapa", (codigo - 4)/2 + 1);
                                            startActivity(inicioSesionIntent);
                                        }else {
                                            mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                                        }
                                    }
                                }
                            });
                            int resultado = sincronizacionController.sincronizar(getContext());
                            if(resultado == 0){
                                progressDialog.dismiss();
                                mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
                                //continuar();
                            }else if(resultado == 2){
                                progressDialog.dismiss();
                                mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
                            }
                        } else {
                            // TODO: Error al grabar
                            Log.d("Agenda", "Error al grabar prospecto en la BD sqlitejjj");
                            flag_click = false;
                        }
                    }
                }else{
                    flag_click = false;
                }
            }
        });

        // Carga Inicial
        CargarDatosInicial();

        return linfragment;
    }

    private void mostrarMensajeParaContinuar(String mensaje){
        new AlertDialog.Builder(getContext())
                .setTitle("Alerta")
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                continuar();
            }
        }).setCancelable(false).show();
    }

    // continua Luego de la sincronización
    private void continuar() {
        if (calendarioActivity.etapaFab == -1)
            calendarioActivity.GestionFlujoProspecto(calendarioActivity.prospectoCreacionTemp);
        else
            calendarioActivity.GestionFlujoEntrevistasDirectas(calendarioActivity.prospectoCreacionTemp,calendarioActivity.etapaFab);

        calendarioActivity.prospectoCreacionTemp = null;

        flag_click = false;
    }

    //// Carga Inicial /////////
    public void CargarDatosInicial()
    {
        if (calendarioActivity.prospectoCreacionTemp.getNombres() == null && calendarioActivity.textoBuscadoProspecto_temp != null)
        {
            calendarioActivity.prospectoCreacionTemp.setNombres(calendarioActivity.textoBuscadoProspecto_temp);
            calendarioActivity.textoBuscadoProspecto_temp = null;

            edtNombreProspecto.setText(calendarioActivity.prospectoCreacionTemp.getNombres());

            edtNombreProspecto.requestFocus();
            InputMethodManager imm = (InputMethodManager) calendarioActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }else {
            edtNombreProspecto.setText(calendarioActivity.prospectoCreacionTemp.getNombres());
        }

        edtApellidoPaternoProspecto.setText(calendarioActivity.prospectoCreacionTemp.getApellidoPaterno());
        edtApellidoMaternoProspecto.setText(calendarioActivity.prospectoCreacionTemp.getApellidoMaterno());
        edtTelefonoProspecto.setText(calendarioActivity.prospectoCreacionTemp.getTelefonoCelular());
        edtCorreoElectronicoProspecto.setText(calendarioActivity.prospectoCreacionTemp.getCorreoElectronico1());

        // Rango de Sueldo
        int codRangoIngreso = calendarioActivity.prospectoCreacionTemp.getCodigoRangoIngreso();
        TablaTablasBean rangoIngresosBean = TablasGeneralesController.ObtenerItemTablaTablas(Constantes.RangoIngresos,codRangoIngreso);
        if (rangoIngresosBean != null)
            btnRangoIngresosMensualesProspecto.setText(rangoIngresosBean.getValorCadena());
        else
            btnRangoIngresosMensualesProspecto.setText("");

        // Fuente
        int codFuente = calendarioActivity.prospectoCreacionTemp.getCodigoFuente();
        TablaTablasBean fuenteBean = TablasGeneralesController.ObtenerItemTablaTablas(Constantes.Fuente,codFuente);
        if (fuenteBean != null)
            btnFuenteProspecto.setText("Fuente : "+fuenteBean.getValorCadena());
        else
            btnFuenteProspecto.setText("");

        // Hijos
        if ("True".equals(calendarioActivity.prospectoCreacionTemp.getFlagHijo()))
            rdgHijosProspecto.check(rdbHijoProspectoSi.getId());
        else if ("False".equals(calendarioActivity.prospectoCreacionTemp.getFlagHijo()))
            rdgHijosProspecto.check(rdbHijoProspectoNo.getId());

        // Fecha de Nacimiento
        if (calendarioActivity.prospectoCreacionTemp.getFechaNacimiento() != null && !"".equals(calendarioActivity.prospectoCreacionTemp.getFechaNacimiento()))
        {
            Date dateNacimiento = null;
            try
            {
                dateNacimiento = new SimpleDateFormat(Constantes.formato_solofecha_envio).parse(calendarioActivity.prospectoCreacionTemp.getFechaNacimiento());
                Calendar cal = Calendar.getInstance();
                cal.setTime(dateNacimiento);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd 'de' MMMM, yyyy");//SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.formato_fecha_mostrar);
                String fechaNacimientoMostrar = simpleDateFormat.format(dateNacimiento);

                btnFechaNacimientoProspecto.setText(fechaNacimientoMostrar);
                calFechaNacimientoProspecto = cal;
            }
            catch(Exception e){}
        }

        tvNotaNuevo.setText(calendarioActivity.prospectoCreacionTemp.getNota());

    }

    private void updateEdtFechaNacimiento(){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.formato_solofecha_envio);
        String fechaNacimiento = simpleDateFormat.format(calFechaNacimientoProspecto.getTime());
        calendarioActivity.prospectoCreacionTemp.setFechaNacimiento(fechaNacimiento);

        SimpleDateFormat simpleDateFormatMostrar = new SimpleDateFormat("dd 'de' MMMM, yyyy");
        String fechaNacimientoMostrar = simpleDateFormatMostrar.format(calFechaNacimientoProspecto.getTime());
        btnFechaNacimientoProspecto.setText(fechaNacimientoMostrar);
    }

    @Override
    public void respuesta_si(TablaTablasBean valor) {
        if (valor.getIdTabla() == Constantes.RangoIngresos) {
            calendarioActivity.prospectoCreacionTemp.setCodigoRangoIngreso(valor.getCodigoCampo());
            btnRangoIngresosMensualesProspecto.setText(valor.getValorCadena());
        }else if (valor.getIdTabla() == Constantes.Fuente)
        {
            calendarioActivity.prospectoCreacionTemp.setCodigoFuente(valor.getCodigoCampo());
            btnFuenteProspecto.setText("Fuente : "+valor.getValorCadena());
        }
    }

    @Override
    public void respuesta_no() {

    }

    public boolean validarDatos(){

        String errorN = "";
        String errorA = "";
        String errorT = "";
        String errorC = "";
        String errorF = "";

        boolean es_valido = true;

        if(edtNombreProspecto.length()==0) {
            edtNombreProspecto.setError(Constantes.MensajeErrorCampoVacio);
            es_valido = false;
            errorN = "Nombre";
        }

        if (edtApellidoPaternoProspecto.length()==0) {
            edtApellidoPaternoProspecto.setError(Constantes.MensajeErrorCampoVacio);
            es_valido = false;
            errorA = "Apellido";
        }

        if(edtTelefonoProspecto.length()==0) {
            edtTelefonoProspecto.setError(Constantes.MensajeErrorCampoVacio);
            es_valido = false;
            errorT = "Teléfono";
        } else if  (edtTelefonoProspecto.getText().toString().length() < 6 )
        {
            edtTelefonoProspecto.setError("Ingrese un número de teléfono válido");
            es_valido = false;
            errorT = "Teléfono";
        }

        if(edtCorreoElectronicoProspecto.length()!=0){
            final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(edtCorreoElectronicoProspecto.getText().toString());
            if(!matcher.matches()) {
                edtCorreoElectronicoProspecto.setError(Constantes.MensajeErrorCampoVacio);
                es_valido = false;
                errorC = "Correo Incorrecto";
            }
        }

        if (calendarioActivity.prospectoCreacionTemp.getCodigoFuente() == -1)
        {
            es_valido = false;
            errorF = "Fuente";
        }

        if (es_valido)
            return true;
        else
        {
            String errorTotal = "";

            if (!"".equals(errorN)) errorTotal = errorTotal.concat(errorN);
            if (!"".equals(errorA)) {
                if ("".equals(errorTotal)) errorTotal =  errorTotal.concat(errorA);
                else errorTotal = errorTotal.concat(", ").concat(errorA);
            }
            if (!"".equals(errorT)) {
                if ("".equals(errorTotal)) errorTotal = errorTotal.concat(errorT);
                else errorTotal= errorTotal.concat(", ").concat(errorT);
            }
            if (!"".equals(errorC)) {
                if ("".equals(errorTotal))  errorTotal= errorTotal.concat(errorC);
                else errorTotal = errorTotal.concat(", ").concat(errorC);
            }
            if (!"".equals(errorF)) {
                if ("".equals(errorTotal)) errorTotal = errorTotal.concat(errorF);
                else errorTotal= errorTotal.concat(", ").concat(errorF);
            }

            Toast msj = Toast.makeText(getActivity(), "Ingresa los datos faltantes : " + errorTotal, Toast.LENGTH_SHORT);
            msj.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            msj.show();

            return false;
        }
    }

    public boolean validarANombre(){

        if(edtNombreProspecto.length()==0) {
            edtNombreProspecto.setError(Constantes.MensajeErrorCampoVacio);
            return false;
        }

        edtNombreProspecto.setError(null);
        return true;
    }

    public boolean validarApellido(){

        if (edtApellidoPaternoProspecto.length()==0){
            edtApellidoPaternoProspecto.setError(Constantes.MensajeErrorCampoVacio);
            return false;
        }

        edtApellidoPaternoProspecto.setError(null);
        return true;
    }

    public boolean validarTelefono() {

        if (edtTelefonoProspecto.length() == 0) {
            edtTelefonoProspecto.setError(Constantes.MensajeErrorCampoVacio);
            return false;
        } else if (edtTelefonoProspecto.getText().toString().length() < 6) {
            edtTelefonoProspecto.setError("Ingrese un número de telefono válido");
            return false;
        }


        edtTelefonoProspecto.setError(null);
        return true;
    }

    public boolean validarCorreo(){

        if(edtCorreoElectronicoProspecto.length()!=0){
            final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(edtCorreoElectronicoProspecto.getText().toString());
            if(!matcher.matches()) {
                edtCorreoElectronicoProspecto.setError(Constantes.MensajeErrorCampoVacio);
                return false;
            }
        }

        edtCorreoElectronicoProspecto.setError(null);
        return true;
    }

}
