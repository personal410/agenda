package com.pacifico.agenda.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.IntermediarioBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.Model.Controller.IntermediarioController;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Adapters.ListaProspectosAutocompleteAdapter;
import com.pacifico.agenda.Adapters.ProspectosAutocompleteLayoutManager;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;

import java.util.ArrayList;

/**
 * Created by Joel on 08/05/2016.
 */
public class BuscarProspectoFragment extends Fragment {

    private CalendarioActivity calendarioActivity;

    RecyclerView rv_listaProspectosAutocomplete;
    EditText edtProspectosBuscar;
    LinearLayout linProspectoAutocomplete;
    ImageButton btnCerrarBuscarProspecto;
    Button btnNuevoProspecto;

    private ListaProspectosAutocompleteAdapter listaProspectosAutocompleteAdapter;

    private ProspectosAutocompleteLayoutManager prospectosAutocompleteLayoutManager;

    public static BuscarProspectoFragment newInstance() {
        BuscarProspectoFragment fragment = new BuscarProspectoFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_buscar_prospecto, container, false);

        prospectosAutocompleteLayoutManager = new ProspectosAutocompleteLayoutManager(this.getContext());

        rv_listaProspectosAutocomplete = (RecyclerView) linfragment.findViewById(R.id.rv_listaProspectosAutocomplete);
        this.rv_listaProspectosAutocomplete.setLayoutManager(prospectosAutocompleteLayoutManager);

        linProspectoAutocomplete = (LinearLayout) linfragment.findViewById(R.id.linProspectoAutocomplete);

        edtProspectosBuscar = (EditText) linfragment.findViewById(R.id.edtProspectosBuscar);

        edtProspectosBuscar.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {

                SetBusquedaKeyPress();
            }
        });

        btnCerrarBuscarProspecto = (ImageButton) linfragment.findViewById(R.id.btnCerrarBuscarProspecto);

        btnCerrarBuscarProspecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CalendarioActivity)getActivity()).CerrarTarjetas();
            }
        });

        btnNuevoProspecto = (Button) linfragment.findViewById(R.id.btnNuevoProspecto);
        btnNuevoProspecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarioActivity.textoBuscadoProspecto_temp = edtProspectosBuscar.getText().toString().trim();
                calendarioActivity.prospectoCreacionTemp = new ProspectoBean();

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, NuevoProspectoFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
                edtProspectosBuscar.setText("");
            }
        });

        // Inflate the layout for this fragment
        return linfragment;
    }

    protected void SetBusquedaKeyPress() {
        //contactosChatPresenter.initialize(tipoBusqueda,textBusqueda.getText().toString());
        String filtro = edtProspectosBuscar.getText().toString().trim();
        if (!"".equals(filtro)) {

            cargarProspectosFiltrados(filtro);
            //TODO: Boton de limpiar lo escrito
            //imgLimpiarContactoAutocomplete.setVisibility(View.VISIBLE);
            //txtNombreContactoBuscar.setCompoundDrawables(null, null, null, null);
        }
        else
        {
            //TODO: Boton de limpiar lo escrito
            //renderContactoAutocompleteList(null);
            //imgLimpiarContactoAutocomplete.setVisibility(View.GONE);
            this.linProspectoAutocomplete.setVisibility(View.GONE);
        }
    }

    private ListaProspectosAutocompleteAdapter.OnItemClickListener onItemClickListener =
             new ListaProspectosAutocompleteAdapter.OnItemClickListener() {
                @Override public void onProspectoAutocompleteClicked(ProspectoBean prospecto) {
                    //Toast.makeText(BuscarProspectoFragment.this.getContext(),prospecto, Toast.LENGTH_SHORT).show();
                    calendarioActivity.OcultarTeclado();

                    /*if (calendarioActivity.etapaFab == Constantes.EtapaProspecto_PrimeraEntrevista ||
                        calendarioActivity.etapaFab == Constantes.EtapaProspecto_SegundaEntrevista ||
                        calendarioActivity.etapaFab == Constantes.EtapaProspecto_EntrevistaAdicional)*/
                    if (calendarioActivity.etapaFab != -1) // SI es una etapa forzada
                    {
                        calendarioActivity.GestionFlujoEntrevistasDirectas(prospecto,calendarioActivity.etapaFab);

                    }else {
                        calendarioActivity.GestionFlujoProspecto(prospecto);
                    }
                }
            };

    //// METODOS DE CONEXION A DATA
    public void cargarProspectosFiltrados(String filtro)
    {
        ArrayList<ProspectoBean> prospectosFiltradosCollection = null;
        /*
        if (calendarioActivity.etapaActual == Constantes.EtapaProspecto_PrimeraEntrevista)
            prospectosFiltradosCollection = ProspectoController.filtroProspectosEntrevistaxNombre(filtro,Constantes.EtapaProspecto_PrimeraEntrevista);
        else if (calendarioActivity.etapaActual == Constantes.EtapaProspecto_SegundaEntrevista)
            prospectosFiltradosCollection = ProspectoController.filtroProspectosEntrevistaxNombre(filtro,Constantes.EtapaProspecto_SegundaEntrevista);
        else if (calendarioActivity.etapaActual == Constantes.EtapaProspecto_EntrevistaAdicional)
            prospectosFiltradosCollection = ProspectoController.filtroProspectosEntrevistaxNombre(filtro,Constantes.EtapaProspecto_EntrevistaAdicional);
        */
        prospectosFiltradosCollection = ProspectoController.filtroProspectosEntrevistaxNombre(filtro);

        if (prospectosFiltradosCollection != null) {
            if (this.listaProspectosAutocompleteAdapter == null) {
                this.listaProspectosAutocompleteAdapter = new ListaProspectosAutocompleteAdapter(this.getContext(), prospectosFiltradosCollection);
            } else {
                this.listaProspectosAutocompleteAdapter.setProspectosCollection(prospectosFiltradosCollection);
            }
            this.listaProspectosAutocompleteAdapter.setOnItemClickListener(onItemClickListener);

            this.rv_listaProspectosAutocomplete.setAdapter(listaProspectosAutocompleteAdapter);
            this.linProspectoAutocomplete.setVisibility(View.VISIBLE);
        }else
        {this.linProspectoAutocomplete.setVisibility(View.GONE);}

    }

    /*public ProspectoMovimientoEtapaBean generarProspectoMovimientoEtapa(ProspectoBean prospectoBean,String fechaMovimiento)
    {
        ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean = new ProspectoMovimientoEtapaBean();

        prospectoMovimientoEtapaBean.setIdProspecto(prospectoBean.getIdProspecto());
        prospectoMovimientoEtapaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
        prospectoMovimientoEtapaBean.setCodigoEstado(prospectoBean.getCodigoEstado());
        prospectoMovimientoEtapaBean.setCodigoEtapa(prospectoBean.getCodigoEtapa());
        prospectoMovimientoEtapaBean.setFechaMovimientoEtapaDispositivo(fechaMovimiento);
        prospectoMovimientoEtapaBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

        return prospectoMovimientoEtapaBean;
    }*/

    /////


}
