package com.pacifico.agenda.Fragments;

//import android.app.Fragment;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Activity.InicioSesionActivity;
import com.pacifico.agenda.Model.Bean.AdnBean;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Model.Bean.TablaTablasBean;
import com.pacifico.agenda.Model.Controller.ADNController;
import com.pacifico.agenda.Model.Controller.AjustesController;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.agenda.Model.Controller.TablasGeneralesController;
import com.pacifico.agenda.Network.RespuestaSincronizacionListener;
import com.pacifico.agenda.Network.SincronizacionController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;
import com.pacifico.agenda.Views.CustomView.setFuente;
import com.pacifico.agenda.Views.Dialogs.DialogGeneral;
import com.pacifico.agenda.Views.Dialogs.DialogOpcionesTablaTablas;
import com.pacifico.agenda.Views.Dialogs.IDialogSiNo;
import com.pacifico.agenda.Views.Dialogs.IDialogSiNoValor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.pacifico.agenda.Util.Util.capitalizedString;

/**
 * Created by Joel on 20/04/2016.
 */
public class EntrevistaFragment  extends Fragment implements IDialogSiNoValor,IDialogSiNo { // extends ProgressFragment {

    public static String TAG = EntrevistaFragment.class.getSimpleName();

    //public static final String input_id_prospecto = "ID_PROSPECTO" ;
    // TODO: probablemente necesita tb un ID de cita

    private CalendarioActivity calendarioActivity;
    private ProgressDialog progressDialog;

    boolean flagClick = false; // Para validar que se haga una cosa a la vez (Evitar doble clicks por performance)

    ImageButton btnCerrarEntrevista;
    TextView txtFechaEntrevista;
    TextView txtHora;
    //TextView txtHoraIncio;
    //TextView txtHoraFin;
    TextView txtUbicacion;
    TextView txtAlertas;
    TextView txtInvitados;
    TextView txtNotas;
    TextView txtRecordatorioLLamadas;
    TextView txtReferenteTexto;
    TextView txtReferenteContenido;
    TextView txtEdadTexto;
    TextView txtHijosTexto;
    TextView txtIngresosTexto;

    TextView txtNombreProspecto;
    TextView txtTelefono;
    TextView txtFlagHijos;

    LinearLayout linCabeceraEntrevista;
    LinearLayout linSubCabeceraEntrevista;
    TextView txtTipoEntrevista;
    TextView txtRangoEdad;
    TextView txtRangoIngresos;
    TextView txtEntrevistaRealizada;
    TextView txtCierreVenta;

    LinearLayout linFechaEntrevista;
    LinearLayout linHoraEntrevista;

    AppCompatCheckBox chkEntrevistaRealizada;
    AppCompatCheckBox chkCierreVenta;

    Button btnGuardarCita;

    private Calendar calFechaCita;
    private Calendar calHoraInicioCita;
    private Calendar calHoraFinCita;

    private Calendar calMaxFechaEntrevista;
    private Calendar calMinFechaEntrevista;
    private CitaBean ultimaCitaRealizada;


    TextView btnDescartar;
    TextView btnSiguienteEntrevista;

    int operacionsincro = 0; // 0: cierra tarjetas

    public static EntrevistaFragment newInstance() {
        EntrevistaFragment fragment = new EntrevistaFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout)inflater.inflate(R.layout.fragment_entrevista, container, false);

        txtNombreProspecto = (TextView) linfragment.findViewById(R.id.txtNombreProspecto);
        txtTelefono = (TextView) linfragment.findViewById(R.id.txtTelefono);
        txtRangoEdad = (TextView) linfragment.findViewById(R.id.txtRangoEdad);
        txtReferenteContenido = (TextView) linfragment.findViewById(R.id.txtReferenteContenido);
        linCabeceraEntrevista = (LinearLayout) linfragment.findViewById(R.id.linCabeceraEntrevista);
        linSubCabeceraEntrevista = (LinearLayout) linfragment.findViewById(R.id.linSubCabeceraEntrevista);
        txtTipoEntrevista = (TextView) linfragment.findViewById(R.id.txtTipoEntrevista);
        txtReferenteTexto = (TextView) linfragment.findViewById(R.id.txtReferenteTexto);
        txtEdadTexto = (TextView) linfragment.findViewById(R.id.txtEdadTexto);
        txtHijosTexto = (TextView) linfragment.findViewById(R.id.txtHijosTexto);
        txtIngresosTexto = (TextView) linfragment.findViewById(R.id.txtIngresosTexto);
        txtFlagHijos = (TextView) linfragment.findViewById(R.id.txtFlagHijos);
        txtRangoIngresos = (TextView) linfragment.findViewById(R.id.txtRangoIngresos);
        btnCerrarEntrevista = (ImageButton) linfragment.findViewById(R.id.btnCerrarEntrevista);
        txtFechaEntrevista = (TextView) linfragment.findViewById(R.id.txtFechaEntrevista);
        btnGuardarCita = (Button) linfragment.findViewById(R.id.btnGuardarCita);
        chkCierreVenta = (AppCompatCheckBox) linfragment.findViewById(R.id.chkCierreVenta);
        chkEntrevistaRealizada = (AppCompatCheckBox) linfragment.findViewById(R.id.chkEntrevistaRealizada);
        btnDescartar = (TextView) linfragment.findViewById(R.id.btnDescartar);
        btnSiguienteEntrevista = (TextView) linfragment.findViewById(R.id.btnSiguienteEntrevista);
        txtEntrevistaRealizada = (TextView) linfragment.findViewById(R.id.txtEntrevistaRealizada);
        txtCierreVenta = (TextView) linfragment.findViewById(R.id.txtCierreVenta);

        linFechaEntrevista = (LinearLayout) linfragment.findViewById(R.id.linFechaEntrevista);
        linHoraEntrevista = (LinearLayout) linfragment.findViewById(R.id.linHoraEntrevista);

        btnCerrarEntrevista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!flagClick) {
                    flagClick = true;

                    reiniciarValoresyCerrarTarjeta();

                    flagClick = false;
                }
            }
        });

        btnGuardarCita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!flagClick) {
                    flagClick = true;

                    //NO CIERRA VENTA NI CAMBIA ETAPA
                    if (calendarioActivity.citaSoloLectura == Constantes.FLAG_FALSO) {

                        // Validando fecha u hora
                        boolean fechaHoraNueva_valido = false;

                        if(calendarioActivity.citaActual.getFechaCita() != null &&
                                !"".equals(calendarioActivity.citaActual.getFechaCita()) &&
                                calendarioActivity.citaActual.getHoraInicio() != null &&
                                !"".equals(calendarioActivity.citaActual.getHoraInicio()) &&
                                calendarioActivity.citaActual.getHoraFin() != null &&
                                !"".equals(calendarioActivity.citaActual.getHoraFin()))
                            fechaHoraNueva_valido = true;

                        boolean recordatorio_nuevo_valido = false;
                        if (calendarioActivity.citaActual.getRecordatorioLlamadaBean() != null)
                            recordatorio_nuevo_valido = true;

                        // validando si tiene fecha incompleta
                        if (!"".equals(
                                (Util.strNULL(calendarioActivity.citaActual.getFechaCita())+
                                 Util.strNULL(calendarioActivity.citaActual.getHoraInicio())+
                                 Util.strNULL(calendarioActivity.citaActual.getHoraFin()))
                            ) && !fechaHoraNueva_valido
                                )
                        {
                            Snackbar.make(getView(), getString(R.string.ValidarFechaIncomplete), Snackbar.LENGTH_LONG).show();
                            flagClick = false;
                            return;
                        }

                        // Si no tiene Fecha ni recordatorio, se muestra un mensaje de no es posible guardar
                        if (!fechaHoraNueva_valido && !recordatorio_nuevo_valido)
                        {
                            Snackbar.make(getView(), getString(R.string.SeleccionarFechaRecordatorio), Snackbar.LENGTH_LONG).show();
                            flagClick = false;
                            return;
                        }

                        // validando evento a la misma hora
                        boolean tieneCitaenHorario = false;

                        if (fechaHoraNueva_valido)
                            tieneCitaenHorario = CitaReunionController.validarCitaMismoHorario(calendarioActivity.citaActual);

                        if (tieneCitaenHorario){
                            dialogReunionMismaHora(fechaHoraNueva_valido,recordatorio_nuevo_valido);
                        } else{
                            generarguardarCita(fechaHoraNueva_valido,recordatorio_nuevo_valido);
                            flagClick = false;
                        }
                    }
                }
            }
        });

        chkEntrevistaRealizada.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!flagClick) {
                    flagClick = true;
                    if (chkEntrevistaRealizada.isChecked()) {
                        chkCierreVenta.setEnabled(true);
                        txtCierreVenta.setTextColor(getResources().getColor(R.color.colorMarron));
                        btnSiguienteEntrevista.setEnabled(true);
                        btnSiguienteEntrevista.setTextColor(getResources().getColor(R.color.colorPrimary));
                    } else {
                        chkCierreVenta.setEnabled(false);
                        txtCierreVenta.setTextColor(getResources().getColor(R.color.colorMarron30));
                        btnSiguienteEntrevista.setEnabled(false);
                        btnSiguienteEntrevista.setTextColor(getResources().getColor(R.color.colorMarron30));
                    }
                    flagClick = false;
                }
            }
        });

        chkCierreVenta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!flagClick) {
                    flagClick = true;
                    if (chkCierreVenta.isChecked()) {
                        btnSiguienteEntrevista.setText(getString(R.string.finalizar_proceso));
                    } else {
                        btnSiguienteEntrevista.setText(getString(R.string.siguiente_entrevista));
                    }
                    flagClick = false;
                }
            }
        });

        btnDescartar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(flagClick) return;

                flagClick = true;

                dialogConfirmacionDescartado();

            }
        });

        btnSiguienteEntrevista.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!flagClick) {
                    flagClick = true;

                    // Este boton solo se habilita cuando el check de entrevista realizada esta activo (chkEntrevistaRealizada.isChecked())
                    // SIempre citaoriginal va a ser diferente de nulo, por que al crear no son checkeables los items,
                    // ACA SE USA EL CITAORIGINAL Y SE IGNORA EL ACTUAL PQ SE IGNORAN LOS CAMBIOS SOLO SE HACE EL CMABIO DE ESTADO O ETAPA

                    String fechaAccionCita = Util.obtenerFechaActual();
                    if (calendarioActivity.citaSoloLectura == Constantes.FLAG_FALSO) {
                        if (chkCierreVenta.isChecked())
                        {
                            dialogCierreVenta();
                        }else{

                            // Validamos escenario primera entrevista sin adn
                            AdnBean adnProspecto =  ADNController.getAdnPorIdProspecto(calendarioActivity.prospectoActual.getIdProspecto(),calendarioActivity.prospectoActual.getIdProspectoDispositivo());
                            if ( calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista &&
                                 ( adnProspecto == null || (adnProspecto!= null && adnProspecto.getFlagTerminado() != Constantes.FLAG_ACTIVO))
                               )
                            {
                                FragmentManager fm = getChildFragmentManager();
                                DialogGeneral dialogGeneral = new DialogGeneral();
                                dialogGeneral.setListener(EntrevistaFragment.this);
                                dialogGeneral.setDatos("Por favor completa el ADN del prospecto.", getString(R.string.continuar),getString(R.string.cancelar_minuscula));
                                dialogGeneral.setCancelable(false);
                                dialogGeneral.show(fm, "dialogo1");

                            }
                            else {
                                operacionsincro = Constantes.entrevista_sincro_siguienteetapa;
                                CitaRealizadaSiguienteEtapa(fechaAccionCita);
                            }
                        }

                    }else{
                        //mensajeCitaSoloLectura();
                        ProspectoBean prospectoBean = ProspectoController.getProspectoBeanByIdProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());
                        calendarioActivity.GestionFlujoProspecto(prospectoBean);
                    }
                    flagClick = false;
                }
            }
        }) ;

        calFechaCita = null;
        calHoraInicioCita = null;
        calHoraFinCita = null;

        //txtFechaEntrevista.setOnClickListener(new View.OnClickListener() {
        linFechaEntrevista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarDialogFecha();
            }
        });

        txtHora = (TextView) linfragment.findViewById(R.id.txtHora);
        linHoraEntrevista.setOnClickListener(new View.OnClickListener() {
        //txtHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (calFechaCita != null)
                    cargarDialogHoraInicio();
                else
                    cargarDialogFecha();
            }
        });

        txtUbicacion = (TextView) linfragment.findViewById(R.id.txtUbicacion);
        txtUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, UbicacionFragment.newInstance()); // newInstance() is a static factory method.

                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        txtAlertas = (TextView) linfragment.findViewById(R.id.txtAlertas);
        txtAlertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, AlertaFragment.newInstance()); // newInstance() is a static factory method.
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        txtInvitados = (TextView) linfragment.findViewById(R.id.txtInvitados);
        txtInvitados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, InvitadosFragment.newInstance()); // newInstance() is a static factory method.
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        txtNotas = (TextView) linfragment.findViewById(R.id.txtNotas);
        txtNotas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, NotasFragment.newInstance()); // newInstance() is a static factory method.
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        txtRecordatorioLLamadas = (TextView) linfragment.findViewById(R.id.txtRecordatorioLLamadas);
        txtRecordatorioLLamadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, RecordatorioLlamadaFragment.newInstance()); // newInstance() is a static factory method.
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        //Seteando ingresos del prospecto
        txtRangoIngresos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getChildFragmentManager();
                DialogOpcionesTablaTablas dialogIngresos = new DialogOpcionesTablaTablas();
                //dialogIngresos.AgregarOpciones(getActivity());
                dialogIngresos.setListener(EntrevistaFragment.this);

                // Lista de Ingresos
                ArrayList<TablaTablasBean> listaItems =  TablasGeneralesController.obtenerTablaTablasPorIdTabla(Constantes.RangoIngresos);

                //dialogIngresos.setDatos("Ingresos","Seleccionar","Cancelar",listaItems,rangoIngresos);
                dialogIngresos.setDatos("Ingresos","Seleccionar","Cancelar",listaItems,calendarioActivity.prospectoActual.getCodigoRangoIngreso());
                dialogIngresos.setCancelable(false);
                dialogIngresos.show(fm, "dialogo1");
            }
        });


        if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista) {
            FormatoTarjetaPrimeraEntrevista();
        }

        if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_SegundaEntrevista) {
            FormatoTarjetaSegundaEntrevista();
        }

        if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_EntrevistaAdicional){
            FormatoTarjetaEntrevistaAdicional();
        }

        CargarDatosInicial();

        // Inflate the layout for this fragment
        return linfragment;
    }

    public void asignarLimiteFechaEntrevista()
    {
        calMaxFechaEntrevista = Calendar.getInstance();
        calMaxFechaEntrevista.add(Calendar.YEAR, 1);

        calMinFechaEntrevista = Calendar.getInstance();
        calMinFechaEntrevista.add(Calendar.MONTH, -1);

        ultimaCitaRealizada = CitaReunionController.obtenerUltimaCitaRealizadaProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());

        if(ultimaCitaRealizada != null) {
            Date fechaCita = null;
            try {
                fechaCita = new SimpleDateFormat(Constantes.formato_solofecha_envio).parse(ultimaCitaRealizada.getFechaCita());
            }catch (Exception e){}

            // Si la fecha es mas reciente al mes se pone como fecha minima
            if(fechaCita != null && (fechaCita.getTime() > calMinFechaEntrevista.getTime().getTime()) ) {
                calMinFechaEntrevista.setTime(fechaCita);
            }
        }
    }


    public void cargarDialogFecha(){
        int anio,mes,dia;
        if (calFechaCita != null)
        {
            anio = calFechaCita.get(Calendar.YEAR);
            mes = calFechaCita.get(Calendar.MONTH);
            dia = calFechaCita.get(Calendar.DAY_OF_MONTH);
        }else
        {
            Calendar calini = Calendar.getInstance();
            anio = calini.get(Calendar.YEAR);
            mes = calini.get(Calendar.MONTH);
            dia = calini.get(Calendar.DAY_OF_MONTH);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), DatePickerDialog.THEME_HOLO_LIGHT,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calFechaCita = Calendar.getInstance();
                calFechaCita.set(Calendar.YEAR, year);
                calFechaCita.set(Calendar.MONTH, monthOfYear);
                calFechaCita.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateEdtFechaCita();

                // Abrir la Hora Inicio
                cargarDialogHoraInicio();
            }
        }, anio, mes, dia);

        datePickerDialog.getDatePicker().setMinDate(calMinFechaEntrevista.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(calMaxFechaEntrevista.getTimeInMillis());
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Siguiente", datePickerDialog);
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", datePickerDialog);
        datePickerDialog.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        datePickerDialog.getDatePicker().setCalendarViewShown(false);
        datePickerDialog.setTitle("Seleccione una fecha");
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();
    }

    public void cargarDialogHoraInicio(){
        final int horaInicio,minutoInicio;
        if (calHoraInicioCita != null)
        {
            horaInicio = calHoraInicioCita.get(Calendar.HOUR_OF_DAY);
            minutoInicio = calHoraInicioCita.get(Calendar.MINUTE);
        }else
        {
            horaInicio = 12;
            minutoInicio = 0;
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar calHoraIniTemp = calHoraInicioCita;

                calHoraInicioCita = Calendar.getInstance();
                calHoraInicioCita.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calHoraInicioCita.set(Calendar.MINUTE, minute);

                updateEdtHora();
                if(calHoraIniTemp == null ||(calHoraInicioCita.get(Calendar.HOUR_OF_DAY) == calHoraIniTemp.get(Calendar.HOUR_OF_DAY) && calHoraInicioCita.get(Calendar.MINUTE) == calHoraIniTemp.get(Calendar.MINUTE))) {
                    cargarDialogHoraFin(false); // No reagenda
                }else
                {
                    cargarDialogHoraFin(true); //reagenda
                }
            }
        }, horaInicio, minutoInicio, false);

        timePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Siguiente", timePickerDialog);
        timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", timePickerDialog);
        timePickerDialog.setCancelable(false);

        timePickerDialog.setTitle("Seleccione una hora de inicio");
        timePickerDialog.setCancelable(false);
        timePickerDialog.show();
    }

    public void cargarDialogHoraFin(boolean reagenda){
        int horaFin,minutoFin;
        if(calHoraFinCita != null){
            //horaFin = calHoraFinCita.get(Calendar.HOUR_OF_DAY);
            //minutoFin = calHoraFinCita.get(Calendar.MINUTE);
            horaFin = calHoraInicioCita.get(Calendar.HOUR_OF_DAY) + 1;
            minutoFin = calHoraInicioCita.get(Calendar.MINUTE);
            if(reagenda) {
                if (calHoraInicioCita != null &&
                    (calHoraInicioCita.get(Calendar.HOUR_OF_DAY)*60 +calHoraInicioCita.get(Calendar.MINUTE)) >= (horaFin * 60 + minutoFin)) {
                        horaFin = calHoraInicioCita.get(Calendar.HOUR_OF_DAY) + 1;
                        minutoFin = calHoraInicioCita.get(Calendar.MINUTE);
                }
            }
        }else {
            if(calHoraInicioCita == null){
                horaFin = 12;
                minutoFin = 0;
            }else{ // Por defecto una hora mas tarde que el inicio
                horaFin = calHoraInicioCita.get(Calendar.HOUR_OF_DAY)+1;
                minutoFin = calHoraInicioCita.get(Calendar.MINUTE);
            }
        }
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                Calendar calHoraFinCitaTemp = Calendar.getInstance();
                calHoraFinCitaTemp.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calHoraFinCitaTemp.set(Calendar.MINUTE, minute);

                if ((calHoraInicioCita.get(Calendar.HOUR_OF_DAY)*60 + calHoraInicioCita.get(Calendar.MINUTE)) >= (calHoraFinCitaTemp.get(Calendar.HOUR_OF_DAY)*60 + calHoraFinCitaTemp.get(Calendar.MINUTE)))
                {
                    dialogHoraFinMenorHoraInicio();
                }else {
                    calHoraFinCita = calHoraFinCitaTemp;
                    updateEdtHora();
                }

            }
        }, horaFin, minutoFin, false);
        timePickerDialog.setTitle("Seleccione una hora de fin:");
        timePickerDialog.setCancelable(false);
        timePickerDialog.show();
    }

    Calendar c = Calendar.getInstance();
    int startYear = c.get(Calendar.YEAR);
    int startMonth = c.get(Calendar.MONTH);
    int startDay = c.get(Calendar.DAY_OF_MONTH);

    @Override
    public void onResume() {
        super.onResume();

        if (calendarioActivity.citaActual != null && calendarioActivity.citaActual.getIdCitaDispositivo() != -1) {
            CitaBean citaValidacion = CitaReunionController.obtenerCitaPorIdCitaDispositivo(calendarioActivity.citaActual.getIdCitaDispositivo());
            // validar si la cita se ha modificado en otra aplicación
            if (citaValidacion.getCodigoEstado() != calendarioActivity.citaActual.getCodigoEstado()) {
                ProspectoBean prospectoSigEtapa = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());
                calendarioActivity.GestionFlujoProspecto(prospectoSigEtapa);
            }
        }
    }

    //// Metodos de Carga de Info:
    public void CargarDatosInicial() // SIempre hay una cita actual asi sea nueva
    {
        //////////////////////// DATOS DEL PROSPECTO ////////////////////////////

        //ProspectoController prospectoController = new ProspectoController();
        //IntermediarioBean intermediarioBean = TablasGeneralesController.obtenerIntermediario();
        //IntermediarioBean intermediarioBean = IntermediarioController.obtenerIntermediario();

        // Se habilita el check de entrevista realizada si la cita fue agendada
        if(calendarioActivity.citaOriginal != null && calendarioActivity.citaOriginal.getCodigoEstado() == Constantes.EstadoCita_Agendada) {
            chkEntrevistaRealizada.setEnabled(true);
            txtEntrevistaRealizada.setTextColor(getResources().getColor(R.color.colorMarron));
        }

        // Cargando Nombres
        String nombreCompleto = "";
        if (calendarioActivity.prospectoActual.getNombres() != null)
            nombreCompleto = nombreCompleto + capitalizedString(calendarioActivity.prospectoActual.getNombres()) ;
        if (calendarioActivity.prospectoActual.getApellidoPaterno() != null)
            nombreCompleto = nombreCompleto  + " " +  capitalizedString(calendarioActivity.prospectoActual.getApellidoPaterno());
        if (calendarioActivity.prospectoActual.getApellidoMaterno() != null)
            nombreCompleto = nombreCompleto  + " " +  capitalizedString(calendarioActivity.prospectoActual.getApellidoMaterno());

        txtNombreProspecto.setText(nombreCompleto);
        txtTelefono.setText(calendarioActivity.prospectoActual.getTelefonoCelular());

        // Obtener Referenciador:
        int idReferenteDispositivo = calendarioActivity.prospectoActual.getIdReferenciadorDispositivo();
        ProspectoBean referente = null;

        if (idReferenteDispositivo != -1)
            referente =  ProspectoController.getProspectoBeanByIdProspectoDispositivo(idReferenteDispositivo); //getProspectoBeanByIdProspecto(idReferente);

        if (referente != null) {
            String nombreReferenteCompleto = "";
            if (referente.getNombres() != null)
                nombreReferenteCompleto = nombreReferenteCompleto + capitalizedString(referente.getNombres());
            if (referente.getApellidoPaterno() != null)
                nombreReferenteCompleto = nombreReferenteCompleto + " " +capitalizedString(referente.getApellidoPaterno());
            if (referente.getApellidoMaterno() != null)
                nombreReferenteCompleto = nombreReferenteCompleto + " " +capitalizedString(referente.getApellidoMaterno());

            txtReferenteContenido.setText(nombreReferenteCompleto);
        }
        else {
            TablaTablasBean fuente = TablasGeneralesController.ObtenerItemTablaTablas(7,calendarioActivity.prospectoActual.getCodigoFuente());
            if (fuente != null)
                txtReferenteContenido.setText(fuente.getValorCadena());
            txtReferenteTexto.setText("");
        }

        // Cargando Rango de Edad
        int codRangoEdad = calendarioActivity.prospectoActual.getCodigoRangoEdad();
        TablaTablasBean rangoEdadBean = TablasGeneralesController.ObtenerItemTablaTablas(Constantes.RangoEdad,codRangoEdad);
        if (rangoEdadBean != null)
            txtRangoEdad.setText(rangoEdadBean.getValorCadena());
        else
            txtRangoEdad.setText("");

        // Cargando Flag de Hijos
        if (calendarioActivity.prospectoActual.getFlagHijo()== Constantes.FLAG_VERDADERO)
            txtFlagHijos.setText("Sí");
        else if (calendarioActivity.prospectoActual.getFlagHijo()== Constantes.FLAG_FALSO)
            txtFlagHijos.setText("No");
        else
            txtFlagHijos.setText("");

        // Ingresos
        int codRangoIngresos = calendarioActivity.prospectoActual.getCodigoRangoIngreso();
        TablaTablasBean rangoIngresosBean = TablasGeneralesController.ObtenerItemTablaTablas(Constantes.RangoIngresos,codRangoIngresos);
        if (rangoIngresosBean != null)
            txtRangoIngresos.setText(rangoIngresosBean.getValorCadena());
        else
            txtRangoIngresos.setText("");

        //////////////////////// DATOS DE LA REUNION ////////////////////////////
        // Fecha de la reunion
        if (calendarioActivity.citaActual.getFechaCita() != null && !"".equals(calendarioActivity.citaActual.getFechaCita()))
        {
            //String pattern = "MM/dd/yyyy";
            Date dateCita = null;
            try
            {
              dateCita = new SimpleDateFormat(Constantes.formato_solofecha_envio).parse(calendarioActivity.citaActual.getFechaCita());
                Calendar cal = Calendar.getInstance();
                cal.setTime(dateCita);

                String diaSemanaTexto= "";
                String mesTexto = "";

                int mesCita = cal.get(Calendar.MONTH);
                int diaCita = cal.get(Calendar.DAY_OF_MONTH);
                int diaSemana = cal.get(Calendar.DAY_OF_WEEK);

                // TODO: Pasar a DateFormat
                // Seteando el Texto Dia
                diaSemanaTexto = Constantes.ObtenerTextoAbreviadoDiaSemana(diaSemana);
                // Seteando el Texto Mes
                mesTexto = Constantes.ObtenerTextoAbreviadoMes(mesCita);

                txtFechaEntrevista.setText(diaSemanaTexto +", "+ diaCita + " " + mesTexto);
                calFechaCita = cal;
            }
            catch(Exception e){}
        }

        String horaCita ="";
        // Hora Inicio de la reunion
        if (calendarioActivity.citaActual.getHoraInicio() != null && !"".equals(calendarioActivity.citaActual.getHoraInicio()))
        {
            String pattern = "HH:mm";
            Date horaInicioCita = null;
            try
            {
                horaInicioCita = new SimpleDateFormat(pattern).parse(calendarioActivity.citaActual.getHoraInicio());

                Calendar cal = Calendar.getInstance();
                cal.setTime(horaInicioCita);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm");
                String text_horaInicio = simpleDateFormat.format(horaInicioCita);
                horaCita = text_horaInicio;

                calHoraInicioCita = cal;
            }
            catch(Exception e){}
        }

        // Hora Fin de la reunion
        if (calendarioActivity.citaActual.getHoraFin() != null && !"".equals(calendarioActivity.citaActual.getHoraFin()))
        {
            String pattern = "HH:mm";
            Date horaFinCita = null;
            try
            {
                horaFinCita = new SimpleDateFormat(pattern).parse(calendarioActivity.citaActual.getHoraFin());

                Calendar cal = Calendar.getInstance();
                cal.setTime(horaFinCita);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");
                String text_horaFin = simpleDateFormat.format(horaFinCita);
                horaCita = horaCita + " - " + text_horaFin;

                calHoraFinCita = cal;
            }
            catch(Exception e){}
        }

        txtHora.setText(horaCita);

        if (calendarioActivity.citaActual.getUbicacion() != null)
            txtUbicacion.setText(calendarioActivity.citaActual.getUbicacion());


        // Invitados
        txtInvitados.setText(calendarioActivity.prospectoActual.getCorreoElectronico1());

        // Recordatorio de llamada
        if (calendarioActivity.citaActual.getRecordatorioLlamadaBean() != null &&
            calendarioActivity.citaActual.getRecordatorioLlamadaBean().getFechaRecordatorio() != null &&
            !"".equals(calendarioActivity.citaActual.getRecordatorioLlamadaBean().getFechaRecordatorio()))
        {
            Date dateRecordatorio = null;
            try
            {
                dateRecordatorio = new SimpleDateFormat(Constantes.formato_fechafull_envio).parse(calendarioActivity.citaActual.getRecordatorioLlamadaBean().getFechaRecordatorio());
                Calendar cal = Calendar.getInstance();
                cal.setTime(dateRecordatorio);

                String diaSemanaTexto= "";
                String mesTexto = "";

                int mesCita = cal.get(Calendar.MONTH);
                int diaCita = cal.get(Calendar.DAY_OF_MONTH);
                int diaSemana = cal.get(Calendar.DAY_OF_WEEK);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");
                String text_horaRecordatorio = simpleDateFormat.format(dateRecordatorio);

                // Seteando el Texto Dia
                diaSemanaTexto = Constantes.ObtenerTextoAbreviadoDiaSemana(diaSemana);
                // Seteando el Texto Mes
                mesTexto = Constantes.ObtenerTextoAbreviadoMes(mesCita);

                txtRecordatorioLLamadas.setText(diaSemanaTexto +", "+ diaCita + " " + mesTexto +", "+text_horaRecordatorio);
            }
            catch(Exception e){}
        }

        // Creacion de Alerta
        if (calendarioActivity.citaActual.getAlertaMinutosAntes() == -1 ) {
            calendarioActivity.citaActual.setAlertaMinutosAntes(Integer.parseInt(AjustesController.obtenerAjuste().getValor1()));
        }

        // Creacion de Alerta
        if (calendarioActivity.citaActual.getAlertaMinutosAntes() != -1) {
            int minutosTotales = calendarioActivity.citaActual.getAlertaMinutosAntes();

            int horas = minutosTotales / 60;
            int minutos = minutosTotales - horas * 60;

            String texto = "";
            if (horas == 1)
                texto = texto + String.valueOf(horas) + " hora ";
            if (horas > 1)
                texto = texto + String.valueOf(horas) + " horas ";
            if (minutos == 1)
                texto = texto + String.valueOf(minutos) + " minuto ";
            if (minutos > 1)
                texto = texto + String.valueOf(minutos) + " minutos ";

            if (!"".equals(texto))
                texto = texto + "antes del evento";

            txtAlertas.setText(texto);
        }
        ///// Fin alertas


        if (calendarioActivity.prospectoActual.getNota() != null)
           txtNotas.setText(calendarioActivity.prospectoActual.getNota());

        if (calendarioActivity.citaActual.getCodigoEstado() == Constantes.EstadoCita_Realizada) {
            chkEntrevistaRealizada.setChecked(true);
            chkEntrevistaRealizada.setEnabled(true);
            btnSiguienteEntrevista.setEnabled(true);
            btnSiguienteEntrevista.setTextColor(getResources().getColor(R.color.colorPrimary));
            txtEntrevistaRealizada.setTextColor(getResources().getColor(R.color.colorMarron));
        }

        if (calendarioActivity.citaActual.getCodigoResultado() == Constantes.ResultadoCita_CierreVenta) {
            chkCierreVenta.setChecked(true);
            chkCierreVenta.setEnabled(true);
            txtCierreVenta.setTextColor(getResources().getColor(R.color.colorMarron));
        }

        // SI ya se le cerro una venta no se muestra el descartado
        if (CitaReunionController.validarCitaVentaRealizada(calendarioActivity.prospectoActual.getIdProspectoDispositivo()))
        {
            btnDescartar.setVisibility(View.GONE);
        }

        // Validar si aun no pasa la hora no se puede realizar
        if (calHoraInicioCita != null && calFechaCita != null) {

            Calendar calFechaHoraActual = Calendar.getInstance();
            Calendar calFechaHoraInicioCita = Calendar.getInstance();

            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date d = null;
            try {
                d = f.parse(calendarioActivity.citaActual.getFechaCita() + " " + calendarioActivity.citaActual.getHoraInicio());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(d != null) {
                calFechaHoraInicioCita.setTime(d);

                if (calFechaHoraInicioCita.getTimeInMillis()>calFechaHoraActual.getTimeInMillis()) {
                    chkEntrevistaRealizada.setEnabled(false);
                    txtEntrevistaRealizada.setTextColor(getResources().getColor(R.color.colorMarron30));
                }
            }
        }
        ////////////////////

        asignarLimiteFechaEntrevista();

        /////// VALIDACIONES SI ES CITA SOLO LECTURA
        if (calendarioActivity.citaSoloLectura == Constantes.FLAG_VERDADERO)
        {
            linHoraEntrevista.setEnabled(false);
            linFechaEntrevista.setEnabled(false);
            txtFechaEntrevista.setEnabled(false);
            txtHora.setEnabled(false);
            txtUbicacion.setEnabled(false);
            txtInvitados.setEnabled(false);
            txtRecordatorioLLamadas.setEnabled(false);
            txtAlertas.setEnabled(false);
            txtNotas.setEnabled(false);
            chkEntrevistaRealizada.setEnabled(false);
            txtEntrevistaRealizada.setTextColor(getResources().getColor(R.color.colorMarron30));
            chkCierreVenta.setEnabled(false);
            txtCierreVenta.setTextColor(getResources().getColor(R.color.colorMarron30));

            btnGuardarCita.setVisibility(View.GONE);
            btnDescartar.setVisibility(View.GONE);

            txtRangoIngresos.setEnabled(false);

            btnSiguienteEntrevista.setEnabled(true);
            btnSiguienteEntrevista.setTextColor(getResources().getColor(R.color.colorPrimary));
            btnSiguienteEntrevista.setText("CONTINUAR");
            btnSiguienteEntrevista.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
        }


    }

    private void updateEdtFechaCita(){
        String diaSemanaTexto= "";
        String mesTexto = "";

        int diaCita = calFechaCita.get(Calendar.DAY_OF_MONTH);
        int mesCita = calFechaCita.get(Calendar.MONTH);
        int diaSemana = calFechaCita.get(Calendar.DAY_OF_WEEK);

        // Seteando el Texto Dia
        diaSemanaTexto = Constantes.ObtenerTextoAbreviadoDiaSemana(diaSemana);
        // Seteando el Texto Mes
        mesTexto = Constantes.ObtenerTextoAbreviadoMes(mesCita);

        txtFechaEntrevista.setText(diaSemanaTexto +", "+ diaCita + " " + mesTexto);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.formato_solofecha_envio);
        calendarioActivity.citaActual.setFechaCita(simpleDateFormat.format(calFechaCita.getTime()));
    }

    private void updateEdtHora(){
        SimpleDateFormat simpleDateFormatIni = new SimpleDateFormat("h:mm");
        SimpleDateFormat simpleDateFormatFin = new SimpleDateFormat("h:mm a");

        String text_horaInicio = "";
        String text_horaFin = "";
        if (calHoraInicioCita != null)
        {
            text_horaInicio = simpleDateFormatIni.format(calHoraInicioCita.getTime());
            SimpleDateFormat simpleDateFormatGuardar = new SimpleDateFormat("HH:mm");
            calendarioActivity.citaActual.setHoraInicio(simpleDateFormatGuardar.format(calHoraInicioCita.getTime()));
        }
        if (calHoraFinCita != null) {
            text_horaFin = simpleDateFormatFin.format(calHoraFinCita.getTime());
            SimpleDateFormat simpleDateFormatGuardar = new SimpleDateFormat("HH:mm");
            calendarioActivity.citaActual.setHoraFin(simpleDateFormatGuardar.format(calHoraFinCita.getTime()));
        }

        txtHora.setText(text_horaInicio + " - " + text_horaFin);
    }

    private boolean generarguardarCita(boolean fechaHoraNueva_valido, boolean recordatorio_nuevo_valido)
    {
        boolean exito = true;
        ///////////////////////////////////////////////

        // Seteando Cita
        String fechaAccionCita = Util.obtenerFechaActual();
        calendarioActivity.citaActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

        boolean seGuardaenDialog = false;

        if (calendarioActivity.citaOriginal != null) // Si se esta editando
        {
            // ACA SOLO SE ACTUALIZA SE IGNORA LOS CIERRE DE VENTA O PASAR A SIGUIENTE ETAPA

            calendarioActivity.citaActual.setFechaModificacionDispositivo(fechaAccionCita);

            // Revisando la fecha Original /////////////////////////
            boolean fechaHora_original_valido = false;

            if (calendarioActivity.citaOriginal.getFechaCita() != null &&
                    !"".equals(calendarioActivity.citaOriginal.getFechaCita()) &&
                    calendarioActivity.citaOriginal.getHoraInicio() != null &&
                    !"".equals(calendarioActivity.citaOriginal.getHoraInicio()) &&
                    calendarioActivity.citaOriginal.getHoraFin() != null &&
                    !"".equals(calendarioActivity.citaOriginal.getHoraFin()))
                    fechaHora_original_valido = true;

            if (fechaHora_original_valido) // Si la cita contaba con una fecha valida
            {
                // Ver si es reagendada
                if (!fechaHoraNueva_valido) { // SI LA FEHA NUEVA NO ES VALIDA (TIENE VOLVER A LLAMAR)
                    // SETEAR LA CITA ORIGINAL COMO REAGENDADO

                    dialogReagendada(Constantes.proceso_reagendada_volverLLamar);
                    seGuardaenDialog = true;

                } else { // SI LA FECHA NUEVA ES VALIDA
                    String reunionCitaOriginal = calendarioActivity.citaOriginal.getFechaCita() + calendarioActivity.citaOriginal.getHoraInicio() + calendarioActivity.citaOriginal.getHoraFin();
                    String reunionCitaActual = calendarioActivity.citaActual.getFechaCita() + calendarioActivity.citaActual.getHoraInicio() + calendarioActivity.citaActual.getHoraFin();

                    if (!reunionCitaActual.equals(reunionCitaOriginal)) // Si se cambio la fecha
                    {
                        dialogReagendada(Constantes.proceso_reagendada_fecha);
                        seGuardaenDialog = true;

                    } else
                    { // SI LA FECHA NO CAMBIO SOLO ES ESTA MODIFICANDO LA CITA
                        calendarioActivity.citaActual.setCodigoEstado(Constantes.EstadoCita_Agendada);
                        calendarioActivity.citaActual.setCodigoResultado(Constantes.ResultadoCita_Pendiente);
                        calendarioActivity.citaActual.setFechaModificacionDispositivo(fechaAccionCita);
                        calendarioActivity.citaActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
                        // ACTUALIZAR CITA
                        CitaReunionController.modificarCita(calendarioActivity.citaActual,getActivity());
                    }
                }
            } else { // SI la cita no contaba con una fecha, entonces contaba con recordatorio de llamada
                if (fechaHoraNueva_valido) {
                    calendarioActivity.citaActual.setCodigoEstado(Constantes.EstadoCita_Agendada);
                    calendarioActivity.citaActual.setCodigoResultado(Constantes.ResultadoCita_Pendiente);
                    calendarioActivity.citaActual.setFechaModificacionDispositivo(fechaAccionCita);
                    calendarioActivity.citaActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
                    // ACTUALIZAR CITA
                    CitaReunionController.modificarCita(calendarioActivity.citaActual,getActivity());

                    CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(GenerarCitaMovimientoEstadoDesdeCita(calendarioActivity.citaActual, fechaAccionCita));

                } else {
                    // En este escenario estosvalores deberian ser los mismos que el anterior
                    //calendarioActivity.citaActual.setCodigoEstado(Constantes.EstadoCita_PorContactar);
                    //calendarioActivity.citaActual.setCodigoResultado(Constantes.ResultadoCita_Pendiente);
                    calendarioActivity.citaActual.setFechaModificacionDispositivo(fechaAccionCita);
                    calendarioActivity.citaActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
                    // ACTUALIZAR CITA
                    CitaReunionController.modificarCita(calendarioActivity.citaActual,getActivity());
                    // No se necesita modificar estado
                }
            }
        }else  // Inserción de una nueva Cita
        {
            guardarCitaNueva(recordatorio_nuevo_valido, fechaHoraNueva_valido, fechaAccionCita);
        }

        if (!seGuardaenDialog) // Se hace esta validacion para que no actualice en vano cuando aun no es definitivo el guardado de cita
        {
            actualizarProspectosiCitaModifico(fechaAccionCita);
            //mensajeCitaGenerico();

            sincronizar();
        }

        return exito;
    }

    public void actualizarProspectosiCitaModifico(String fechaAccionCita){
        if (calendarioActivity.citaModificoProspecto)
        {
            calendarioActivity.prospectoActual.setFechaModificacionDispositivo(fechaAccionCita);
            calendarioActivity.prospectoActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
            ProspectoController.actualizarProspectoxIDDispositivo(calendarioActivity.prospectoActual);
        }
    }

    public void guardarCitaNueva(boolean recordatorio_valido, boolean fechaHora_valido, String fechaAccionCita)
    {
        if (recordatorio_valido) // SI tiene recordatorio valido es por contactar
            calendarioActivity.citaActual.setCodigoEstado(Constantes.EstadoCita_PorContactar);

        if (fechaHora_valido) // SI tiene fecha valida es pedniente
            calendarioActivity.citaActual.setCodigoEstado(Constantes.EstadoCita_Agendada);

        calendarioActivity.citaActual.setCodigoResultado(Constantes.ResultadoCita_Pendiente);
        calendarioActivity.citaActual.setFechaCreacionDispositivo(fechaAccionCita);
        calendarioActivity.citaActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

        CitaReunionController.insertarCitaDesdeTarjeta(calendarioActivity.citaActual, getActivity());
        CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(GenerarCitaMovimientoEstadoDesdeCita(calendarioActivity.citaActual, fechaAccionCita));

        // SE REALIZA EL CAMBIO DE ETAPA DEL PROSPECTO
        switch (calendarioActivity.citaActual.getCodigoEtapaProspecto())
        {
            case Constantes.EtapaProspecto_PrimeraEntrevista:
                calendarioActivity.prospectoActual.setCodigoEtapa(Constantes.EtapaProspecto_PrimeraEntrevista);
                calendarioActivity.prospectoActual.setCodigoEstado(Constantes.EstadoProspecto_EnProceso);
                calendarioActivity.citaModificoProspecto = true; // Al final se va a guardar prospecto

                ProspectoMovimientoEtapaController.guardarProspectoMovimientoEtapa_GenerarIDDispositivo(GenerarProspectoMovimientoEtapa(calendarioActivity.prospectoActual,fechaAccionCita));
                break;
            case Constantes.EtapaProspecto_SegundaEntrevista:
                calendarioActivity.prospectoActual.setCodigoEtapa(Constantes.EtapaProspecto_SegundaEntrevista);
                calendarioActivity.prospectoActual.setCodigoEstado(Constantes.EstadoProspecto_EnProceso);
                calendarioActivity.citaModificoProspecto = true; // Al final se va a guardar prospecto

                ProspectoMovimientoEtapaController.guardarProspectoMovimientoEtapa_GenerarIDDispositivo(GenerarProspectoMovimientoEtapa(calendarioActivity.prospectoActual,fechaAccionCita));
                break;
            case Constantes.EtapaProspecto_EntrevistaAdicional:
                if(calendarioActivity.prospectoActual.getCodigoEtapa() != Constantes.EtapaProspecto_EntrevistaAdicional)
                {
                    calendarioActivity.prospectoActual.setCodigoEtapa(Constantes.EtapaProspecto_EntrevistaAdicional);
                    calendarioActivity.prospectoActual.setCodigoEstado(Constantes.EstadoProspecto_EnProceso);
                    calendarioActivity.citaModificoProspecto = true;// Al final se va a guardar prospecto

                    ProspectoMovimientoEtapaController.guardarProspectoMovimientoEtapa_GenerarIDDispositivo(GenerarProspectoMovimientoEtapa(calendarioActivity.prospectoActual,fechaAccionCita));
                }else
                {
                    if (calendarioActivity.prospectoActual.getCodigoEstado() != Constantes.EstadoProspecto_EnProceso) // si no esta en en proceso y se crea una nueva cita x ejm: SI ya cerro venta en una cita anterior pero se le agenda otra vuelve a estar en proceso
                    {
                        calendarioActivity.prospectoActual.setCodigoEstado(Constantes.EstadoProspecto_EnProceso);
                        calendarioActivity.citaModificoProspecto = true;// Al final se va a guardar prospecto
                    }
                }
                break;
        }
    }

    public ProspectoMovimientoEtapaBean GenerarProspectoMovimientoEtapa(ProspectoBean prospectoBean,String fechaMovimiento)
    {
        ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean = new ProspectoMovimientoEtapaBean();

        prospectoMovimientoEtapaBean.setIdProspecto(prospectoBean.getIdProspecto());
        prospectoMovimientoEtapaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
        prospectoMovimientoEtapaBean.setCodigoEstado(prospectoBean.getCodigoEstado());
        prospectoMovimientoEtapaBean.setCodigoEtapa(prospectoBean.getCodigoEtapa());
        prospectoMovimientoEtapaBean.setFechaMovimientoEtapaDispositivo(fechaMovimiento);
        prospectoMovimientoEtapaBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

        return prospectoMovimientoEtapaBean;
    }

    public CitaMovimientoEstadoBean GenerarCitaMovimientoEstadoDesdeCita(CitaBean citaBean, String fechaMovimiento)
    {
        CitaMovimientoEstadoBean citaMovimientoEstadoBean = new CitaMovimientoEstadoBean();
        citaMovimientoEstadoBean.setIdCita(citaBean.getIdCita());
        citaMovimientoEstadoBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());

        citaMovimientoEstadoBean.setCodigoEstado(citaBean.getCodigoEstado());
        citaMovimientoEstadoBean.setCodigoResultado(citaBean.getCodigoResultado());
        citaMovimientoEstadoBean.setFechaMovimientoEstadoDispositivo(fechaMovimiento);
        citaMovimientoEstadoBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

        return citaMovimientoEstadoBean;
    }

    public void CitaRealizadaSiguienteEtapa(String fechaAccionCita)
    {
        if (calendarioActivity.citaOriginal != null) {
            calendarioActivity.citaOriginal.setCodigoEstado(Constantes.EstadoCita_Realizada); // Se marca la cita como realizada
            //TODO: Validar en el caso de Entrevista Adicional, No Hay siguiente Etapa
            calendarioActivity.citaOriginal.setCodigoResultado(Constantes.ResultadoCita_SiguienteEtapa); // Se pasa a la siguiente etapa automaticamente
            calendarioActivity.citaOriginal.setFechaModificacionDispositivo(fechaAccionCita);
            calendarioActivity.citaOriginal.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

            CitaReunionController.modificarCita(calendarioActivity.citaOriginal,getActivity());
            CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(GenerarCitaMovimientoEstadoDesdeCita(calendarioActivity.citaOriginal, fechaAccionCita));
        } else
        {
            Log.d(TAG, "CambiarEtapaporCitaRealizada:  Entro a cambiar etapa con citaOriginal nula");
        }

        actualizarProspectosiCitaModifico(fechaAccionCita);

        /*
        // Reiniciamos los citas actuales //////////////////////////
        calendarioActivity.citaOriginal = null; // Por que se acaba de cambiar de etapa

        // Cita Actual
        calendarioActivity.citaActual = new CitaBean();
        calendarioActivity.citaActual.setIdProspecto(calendarioActivity.prospectoActual.getIdProspecto());
        calendarioActivity.citaActual.setIdProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());

        // La cita es de la etapa siguiente del prospecto (se esta cambiando de etapa)
        switch (calendarioActivity.prospectoActual.getCodigoEtapa())
        {
            case Constantes.EtapaProspecto_PrimeraEntrevista:
                calendarioActivity.citaActual.setCodigoEtapaProspecto(Constantes.EtapaProspecto_SegundaEntrevista);
                break;
            case Constantes.EtapaProspecto_SegundaEntrevista:
                calendarioActivity.citaActual.setCodigoEtapaProspecto(Constantes.EtapaProspecto_EntrevistaAdicional);
                break;
            case Constantes.EtapaProspecto_EntrevistaAdicional: // sigue en adicional
                calendarioActivity.citaActual.setCodigoEtapaProspecto(Constantes.EtapaProspecto_EntrevistaAdicional);
                break;
        }

        if (calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_SegundaEntrevista)
            FormatoTarjetaSegundaEntrevista();
        else if (calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_EntrevistaAdicional)
            FormatoTarjetaEntrevistaAdicional();*/
        /////////////////////////////////////////////////

        sincronizar();
    }

    public void FormatoTarjetaPrimeraEntrevista(){
        linCabeceraEntrevista.setBackgroundColor(getResources().getColor(R.color.color_primera_entrevista));
        linSubCabeceraEntrevista.setBackgroundColor(getResources().getColor(R.color.subcabecera_primera_entrevista));
        txtTipoEntrevista.setText("1RA ENTREVISTA");
        txtTipoEntrevista.setTextColor(getResources().getColor(R.color.cabecera_texto_primera_entrevista));
        txtReferenteTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_primera_entrevista));
        txtReferenteContenido.setTextColor(getResources().getColor(R.color.cabecera_texto_primera_entrevista));
        txtEdadTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_primera_entrevista));
        txtHijosTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_primera_entrevista));
        txtIngresosTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_primera_entrevista));

        txtFechaEntrevista.setText("");
        txtHora.setText("");
        //txtHoraIncio.setText("");
        //txtHoraFin.setText("");
        txtUbicacion.setText("");
        txtInvitados.setText("");
        txtRecordatorioLLamadas.setText("");
        txtAlertas.setText("");
        // Notas no es necesario limpiar por que son notas del prospecto
        chkEntrevistaRealizada.setChecked(false);
        chkCierreVenta.setChecked(false);

        calFechaCita = null;
        calHoraInicioCita = null;
        calHoraFinCita = null;

        btnSiguienteEntrevista.setEnabled(false);
        btnSiguienteEntrevista.setTextColor(getResources().getColor(R.color.colorMarron30));
    }

    ///
    public void FormatoTarjetaSegundaEntrevista(){
        linCabeceraEntrevista.setBackgroundColor(getResources().getColor(R.color.color_segunda_entrevista));
        linSubCabeceraEntrevista.setBackgroundColor(getResources().getColor(R.color.subcabecera_segunda_entrevista));
        txtTipoEntrevista.setTextColor(getResources().getColor(R.color.cabecera_texto_segunda_entrevista));
        txtTipoEntrevista.setText("2DA ENTREVISTA");
        txtReferenteTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_segunda_entrevista));
        txtReferenteContenido.setTextColor(getResources().getColor(R.color.cabecera_texto_segunda_entrevista));
        txtEdadTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_segunda_entrevista));
        txtHijosTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_segunda_entrevista));
        txtIngresosTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_segunda_entrevista));

        // Inicializando campos
        txtFechaEntrevista.setText("");
        txtHora.setText("");
        //txtHoraIncio.setText("");
        //txtHoraFin.setText("");
        txtUbicacion.setText("");
        txtInvitados.setText("");
        txtRecordatorioLLamadas.setText("");
        txtAlertas.setText("");
        // Notas no es necesario limpiar por que son notas del prospecto
        chkEntrevistaRealizada.setChecked(false);
        chkCierreVenta.setChecked(false);

        calFechaCita = null;
        calHoraInicioCita = null;
        calHoraFinCita = null;

        btnSiguienteEntrevista.setEnabled(false);
        btnSiguienteEntrevista.setTextColor(getResources().getColor(R.color.colorMarron30));

    }

    public void FormatoTarjetaEntrevistaAdicional(){
        linCabeceraEntrevista.setBackgroundColor(getResources().getColor(R.color.color_entrevista_adicional));
        linSubCabeceraEntrevista.setBackgroundColor(getResources().getColor(R.color.subcabecera_entrevista_adicional));
        txtTipoEntrevista.setTextColor(getResources().getColor(R.color.cabecera_texto_entrevista_adicional));
        txtTipoEntrevista.setText("ENTREVISTA ADICIONAL");
        txtReferenteTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_entrevista_adicional));
        txtReferenteContenido.setTextColor(getResources().getColor(R.color.cabecera_texto_entrevista_adicional));
        txtEdadTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_entrevista_adicional));
        txtHijosTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_entrevista_adicional));
        txtIngresosTexto.setTextColor(getResources().getColor(R.color.cabecera_texto_entrevista_adicional));

        // Inicializando campos
        txtFechaEntrevista.setText("");
        txtHora.setText("");
        //txtHoraIncio.setText("");
        //txtHoraFin.setText("");
        txtUbicacion.setText("");
        txtInvitados.setText("");
        txtRecordatorioLLamadas.setText("");
        txtAlertas.setText("");
        // Notas no es necesario limpiar por que son notas del prospecto
        chkEntrevistaRealizada.setChecked(false);
        chkCierreVenta.setChecked(false);

        calFechaCita = null;
        calHoraInicioCita = null;
        calHoraFinCita = null;

        btnSiguienteEntrevista.setEnabled(false);
        btnSiguienteEntrevista.setTextColor(getResources().getColor(R.color.colorMarron30));
    }

    public void dialogCierreVenta(){
        LayoutInflater layoutInflater = LayoutInflater.from(this.getActivity());
        final View dialogCierreVenta = layoutInflater.inflate(R.layout.dialog_cierre_venta, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setView(dialogCierreVenta).setCancelable(false).setNegativeButton("Cancelar", null).setPositiveButton("Confirmar Cierre", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                String fechaAccionCita = Util.obtenerFechaActual();
                calendarioActivity.citaOriginal.setCodigoEstado(Constantes.EstadoCita_Realizada); // Se marca la cita como realizada
                calendarioActivity.citaOriginal.setCodigoResultado(Constantes.ResultadoCita_CierreVenta); // resultado : Cierre de venta

                // Actualizamos el prospecto
                calendarioActivity.citaModificoProspecto = true;
                calendarioActivity.prospectoActual.setCodigoEstado(Constantes.EstadoProspecto_CierreVenta);

                CheckBox checkSeguroVida = (CheckBox) dialogCierreVenta.findViewById(R.id.checkSeguroVida);
                CheckBox checkSeguroAP = (CheckBox) dialogCierreVenta.findViewById(R.id.checkSeguroAP);

                if (checkSeguroVida.isChecked()) {
                    EditText edtCantidadSV = (EditText) dialogCierreVenta.findViewById(R.id.edtCantidadSV);
                    String cantidadSV = edtCantidadSV.getText().toString();

                    if (cantidadSV.length() > 0) {
                        calendarioActivity.citaOriginal.setCantidadVI(Integer.parseInt(cantidadSV));
                    } else {
                        calendarioActivity.citaOriginal.setCantidadVI(0);
                    }

                    EditText edtPrimaTargetSV = (EditText) dialogCierreVenta.findViewById(R.id.edtPrimaTargetSV);
                    String primaTargetSV = edtPrimaTargetSV.getText().toString();
                    if (primaTargetSV.length() > 0) {
                        calendarioActivity.citaOriginal.setPrimaTargetVI(Double.parseDouble(primaTargetSV));
                    } else {
                        calendarioActivity.citaOriginal.setPrimaTargetVI(0);
                    }
                }

                if (checkSeguroAP.isChecked()) {
                    EditText edtCantidaSAP = (EditText) dialogCierreVenta.findViewById(R.id.edtCantidadSAP);
                    String cantidadSAP = edtCantidaSAP.getText().toString();

                    if (cantidadSAP.length() > 0) {
                        calendarioActivity.citaOriginal.setCantidadAP(Integer.parseInt(cantidadSAP));
                    } else {
                        calendarioActivity.citaOriginal.setCantidadAP(0);
                    }

                    EditText edtPrimaTargetSAP = (EditText) dialogCierreVenta.findViewById(R.id.edtPrimaTargetSAP);
                    String primaTargetSAP = edtPrimaTargetSAP.getText().toString();

                    if (primaTargetSAP.length() > 0) {
                        calendarioActivity.citaOriginal.setPrimaTargetAP(Double.parseDouble(primaTargetSAP));
                    } else {
                        calendarioActivity.citaOriginal.setPrimaTargetAP(0);
                    }
                }

                calendarioActivity.citaOriginal.setFechaModificacionDispositivo(fechaAccionCita);
                calendarioActivity.citaOriginal.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(GenerarCitaMovimientoEstadoDesdeCita(calendarioActivity.citaOriginal, fechaAccionCita));
                CitaReunionController.modificarCita(calendarioActivity.citaOriginal, getActivity());

                actualizarProspectosiCitaModifico(fechaAccionCita); // Actualizara si es necesario

                // Actualizando
                sincronizar();

            }
        });
        AlertDialog  alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Button btnCancelar = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button btnAceptar = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        btnAceptar.setTextColor(getResources().getColor(R.color.colorCeleste));
        btnCancelar.setTextColor(getResources().getColor(R.color.colorMarron));
        setFuente.setButton(getActivity(),btnAceptar);
        setFuente.setButton(getActivity(),btnCancelar);
    }

    public void dialogReunionMismaHora(final boolean fechaHoraNueva_valido, final boolean recordatorio_nuevo_valido){
        LayoutInflater layoutInflater = LayoutInflater.from(this.getActivity());
        final View dialogReunionMismaHora = layoutInflater.inflate(R.layout.dialog_general, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        TextView tvTitulo = (TextView) dialogReunionMismaHora.findViewById(R.id.tvTitulo);
        tvTitulo.setText("Tiene una reunión a la misma hora.\n ¿Desea proceder?");

        builder.setView(dialogReunionMismaHora).setCancelable(false).setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                generarguardarCita(fechaHoraNueva_valido, recordatorio_nuevo_valido);
                flagClick = false;
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                flagClick = false;
            }
        });

        AlertDialog  alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Button btnCancelar = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button btnAceptar = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        btnAceptar.setTextColor(getResources().getColor(R.color.colorCeleste));
        btnCancelar.setTextColor(getResources().getColor(R.color.colorMarron));
        setFuente.setButton(getActivity(),btnAceptar);
        setFuente.setButton(getActivity(),btnCancelar);
    }
    public void dialogConfirmacionDescartado()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this.getActivity());
        final View dialogConfirmacionDescartado = layoutInflater.inflate(R.layout.dialog_general, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        TextView tvTitulo = (TextView) dialogConfirmacionDescartado.findViewById(R.id.tvTitulo);
        tvTitulo.setText("¿Seguro que desea descartar el prospecto?");

        builder.setView(dialogConfirmacionDescartado).setCancelable(false).setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                descartarProspecto();
                flagClick = false;
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                flagClick = false;
            }
        });

        AlertDialog  alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Button btnPositivo = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        Button btnNegativo = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        btnPositivo.setTextColor(getResources().getColor(R.color.colorCeleste));
        setFuente.setButton(getActivity(),btnPositivo);
        setFuente.setButton(getActivity(),btnNegativo);
    }

    public void dialogReagendada(final int tipo){ // Tipo reagendada, o reagdendada volver a llamar
        LayoutInflater layoutInflater = LayoutInflater.from(this.getActivity());
        final View dialogReagendada = layoutInflater.inflate(R.layout.dialog_reagendado, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setView(dialogReagendada).setCancelable(false).setNegativeButton("Cancelar", null).setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                String fechaAccionCita = Util.obtenerFechaActual();
                RadioGroup rdgReagendar = (RadioGroup) dialogReagendada.findViewById(R.id.rdgReagendar);
                RadioButton rdbProspectoCambioFecha = (RadioButton) dialogReagendada.findViewById(R.id.rdbProspectoCambioFecha);
                RadioButton rdbErrorAgendarCita = (RadioButton) dialogReagendada.findViewById(R.id.rdbErrorAgendarCita);
                RadioButton rdbNoHoraPactada = (RadioButton) dialogReagendada.findViewById(R.id.rdbNoHoraPactada);
                int selectedId = rdgReagendar.getCheckedRadioButtonId();

                if (selectedId != rdbProspectoCambioFecha.getId() && selectedId != rdbNoHoraPactada.getId() && selectedId != rdbErrorAgendarCita.getId())
                {
                    flagClick = false;
                    return;
                }

                // find which radioButton is checked by id
                if(selectedId == rdbProspectoCambioFecha.getId() || selectedId == rdbNoHoraPactada.getId()) {

                    if (selectedId == rdbProspectoCambioFecha.getId()) // Cambio Fecha
                    {
                        calendarioActivity.citaOriginal.setCodigoMotivoReagendado(Constantes.Reagendado_ProspectoCambioFecha);
                    }else // Cambio Hora Pactada -- rdbNoHoraPactada
                    {
                        calendarioActivity.citaOriginal.setCodigoMotivoReagendado(Constantes.Reagendado_NoPudeHoraPactada);
                    }


                    if (tipo == Constantes.proceso_reagendada_volverLLamar)
                    {
                        calendarioActivity.citaOriginal.setCodigoEstado(Constantes.EstadoCita_ReAgendada);
                        calendarioActivity.citaOriginal.setCodigoResultado(Constantes.ResultadoCita_VolveraLlamar);

                        // ACTUALIZANDO LA CITA ORIGINAL COMO RE-AGENDADA/////////////////////
                        calendarioActivity.citaOriginal.setFechaModificacionDispositivo(fechaAccionCita);
                        calendarioActivity.citaOriginal.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                        RecordatorioLlamadaBean recordatorioLlamadaBean = calendarioActivity.citaOriginal.getRecordatorioLlamadaBean();
                        if(recordatorioLlamadaBean != null){
                            recordatorioLlamadaBean.setFlagActivo(0);
                            CitaReunionController.actualizarRecordatorioLlamada(recordatorioLlamadaBean);
                        }

                        CitaReunionController.modificarCita(calendarioActivity.citaOriginal,getActivity());

                        // Seteando Cita Movimiento Estado de la cita Reagendada
                        CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(GenerarCitaMovimientoEstadoDesdeCita(calendarioActivity.citaOriginal, fechaAccionCita));

                        ////////////////////////////////////////////////////////////////////////

                        // INSERTAR LA NUEVA CITA - SE REINICIAN LOS ID
                        calendarioActivity.citaActual.setIdCita(-1);
                        calendarioActivity.citaActual.setIdCitaDispositivo(-1);
                        calendarioActivity.citaActual.setCodigoEstado(Constantes.EstadoCita_PorContactar);
                        calendarioActivity.citaActual.setCodigoResultado(Constantes.ResultadoCita_Pendiente);

                        calendarioActivity.citaActual.setFechaCreacionDispositivo(fechaAccionCita);
                        calendarioActivity.citaActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
                        CitaReunionController.insertarCitaDesdeTarjeta(calendarioActivity.citaActual,getActivity());

                        // Seteando Cita Movimiento Estado de la nueva Cita Creada
                        CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(GenerarCitaMovimientoEstadoDesdeCita(calendarioActivity.citaActual, fechaAccionCita));

                    }else // if Constantes.proceso_reagendada_fecha
                    {
                        // SETEAR LA CITA ORIGINAL COMO REAGENDADO
                        calendarioActivity.citaOriginal.setCodigoEstado(Constantes.EstadoCita_ReAgendada);
                        calendarioActivity.citaOriginal.setCodigoResultado(Constantes.ResultadoCita_ReAgendado);

                        calendarioActivity.citaOriginal.setFechaModificacionDispositivo(fechaAccionCita);
                        calendarioActivity.citaOriginal.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                        // ACTUALIZANDO LA CITA ORIGINAL COMO REAGENDADA
                        CitaReunionController.modificarCita(calendarioActivity.citaOriginal, getActivity());

                        RecordatorioLlamadaBean recordatorioLlamadaBean = calendarioActivity.citaOriginal.getRecordatorioLlamadaBean();
                        if(recordatorioLlamadaBean != null){
                            recordatorioLlamadaBean.setFlagActivo(0);
                            CitaReunionController.actualizarRecordatorioLlamada(recordatorioLlamadaBean);
                        }

                        CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(GenerarCitaMovimientoEstadoDesdeCita(calendarioActivity.citaOriginal, fechaAccionCita));
                        ///////////////////////////////////////////////////////////////////////

                        // INSERTAR LA NUEVA CITA  - SE REINICIAN LOS ID
                        calendarioActivity.citaActual.setIdCita(-1);
                        calendarioActivity.citaActual.setIdCitaDispositivo(-1);
                        calendarioActivity.citaActual.setCodigoEstado(Constantes.EstadoCita_Agendada);
                        calendarioActivity.citaActual.setCodigoResultado(Constantes.ResultadoCita_Pendiente);

                        calendarioActivity.citaActual.setFechaCreacionDispositivo(fechaAccionCita);
                        calendarioActivity.citaActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                        CitaReunionController.insertarCitaDesdeTarjeta(calendarioActivity.citaActual,getActivity());

                        // Seteando Cita Movimiento Estado de la nueva Cita Creada
                        CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(GenerarCitaMovimientoEstadoDesdeCita(calendarioActivity.citaActual, fechaAccionCita));
                    }
                } else if(selectedId == rdbErrorAgendarCita.getId()) { // Si es un error al agendar cita solo se necesita reagendar
                    calendarioActivity.citaActual.setCodigoEstado(Constantes.EstadoCita_Agendada);
                    calendarioActivity.citaActual.setCodigoResultado(Constantes.ResultadoCita_Pendiente);
                    calendarioActivity.citaActual.setFechaModificacionDispositivo(fechaAccionCita);
                    calendarioActivity.citaActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
                    // ACTUALIZAR CITA
                    CitaReunionController.modificarCita(calendarioActivity.citaActual,getActivity());
                }

                actualizarProspectosiCitaModifico(fechaAccionCita); // Actualizara si es necesario
                //mensajeCitaGenerico();
                sincronizar();
            }
        });
        AlertDialog  alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Button btnPositivo = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        Button btnNegativo = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        btnPositivo.setTextColor(getResources().getColor(R.color.colorCeleste));
        setFuente.setButton(getActivity(),btnPositivo);
        setFuente.setButton(getActivity(),btnNegativo);
    }

    public void mensajeInstaleADN (){
        // Mensaje Instale ADN
        FragmentManager fm = getChildFragmentManager();
        DialogGeneral dialogGeneral = new DialogGeneral();
        dialogGeneral.setListener(null);
        dialogGeneral.setDatos(getString(R.string.mensaje_instalar_adn), "", getString(R.string.continuar));
        dialogGeneral.setCancelable(false);
        dialogGeneral.show(fm, "dialogo1");
    }

    public void reiniciarValoresyCerrarTarjeta()
    {
        ////////// Reseteo de valores
        calendarioActivity.reiniciarParametros();
        calendarioActivity.CerrarTarjetas();
        calendarioActivity.actualizarCalendario();
        ///////
    }
    public void sincronizar(){
        // Llamar al envío de datos #ENVIO
        SincronizacionController sincronizacionController = new SincronizacionController();
        if(progressDialog == null){
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Enviando información");
            progressDialog.setMessage("Espere, por favor");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
            @Override
            public void terminoSincronizacion(int codigo, String mensaje) {
                progressDialog.dismiss();
                if (codigo < 0) {
                    mostrarMensajeParaContinuar(mensaje);
                }else if(codigo == 1){
                      mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
                    //continuar();
                }else{
                    if(codigo == 4 || codigo == 6){
                        Intent inicioSesionIntent = new Intent(getActivity(), InicioSesionActivity.class);
                        inicioSesionIntent.putExtra("etapa", (codigo - 4)/2 + 1);
                        startActivity(inicioSesionIntent);
                    }else {
                        mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                    }
                }
            }
        });
        int resultado = sincronizacionController.sincronizar(getContext());
        if(resultado == 0){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
        }else if(resultado == 2){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
        }
    }

    private void mostrarMensajeParaContinuar(String mensaje){ // Funciona para guardar y descartar
        new AlertDialog.Builder(getContext())
                .setTitle("Alerta")
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                continuar();
            }
        }).setCancelable(false).show();
    }

    private void continuar() {

        if (operacionsincro == Constantes.entrevista_sincro_siguienteetapa) { // Siguiente etapa no cierra la tarjeta
            // Se esta llamando al gestion flujo prospecto en lugar de reiniciar los valores como antes para centralizar la logica
            ProspectoBean prospectoSigEtapa = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());
            calendarioActivity.GestionFlujoProspecto(prospectoSigEtapa);
        }
        else {
            reiniciarValoresyCerrarTarjeta();
        }

        operacionsincro = 0;
        flagClick = false;
    }

    @Override
    public void respuesta_si(TablaTablasBean valor) {
        if (valor.getIdTabla() == Constantes.RangoIngresos) {
            //rangoIngresos = valor.getCodigoCampo();
            if (calendarioActivity.prospectoActual.getCodigoRangoIngreso() != valor.getCodigoCampo())
            {
                calendarioActivity.prospectoActual.setCodigoRangoIngreso(valor.getCodigoCampo());
                txtRangoIngresos.setText(valor.getValorCadena());

                calendarioActivity.citaModificoProspecto = true;
            }
        }
    }

    @Override
    public void respuesta_si() {
        // En el dialog de mandar a ADN
        Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage("com.pacifico.adn");

        if(launchIntent != null){
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            launchIntent.putExtra(Constantes.tipo_operacion, Constantes.operacion_ADNdesdeAgenda);
            launchIntent.putExtra(Constantes.parametro_idProspectoDis_ADNdesdeAgenda, calendarioActivity.prospectoActual.getIdProspectoDispositivo());
            startActivity(launchIntent);

            calendarioActivity.reiniciarParametros();
            calendarioActivity.CerrarTarjetas();
        }else{
            mensajeInstaleADN();
        }

    }

    @Override
    public void respuesta_no() {

    }

    public void descartarProspecto(){

        String fechaActual = Util.obtenerFechaActual();

        // Se actualiza el prospecto
        calendarioActivity.prospectoActual.setCodigoEstado(Constantes.EstadoProspecto_NoInteresado);
        calendarioActivity.prospectoActual.setFechaModificacionDispositivo(fechaActual);
        calendarioActivity.prospectoActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
        ProspectoController.actualizarProspectoxIDDispositivo(calendarioActivity.prospectoActual);
        // Se genera el prospecto Movimiento Etapa
        ProspectoMovimientoEtapaController.guardarProspectoMovimientoEtapa_GenerarIDDispositivo(GenerarProspectoMovimientoEtapa(calendarioActivity.prospectoActual,fechaActual));

        // Actualizando cita de haberla
        calendarioActivity.citaActual.setCodigoEstado(Constantes.EstadoCita_Realizada);
        calendarioActivity.citaActual.setCodigoResultado(Constantes.ResultadoCita_NoInteresado);
        calendarioActivity.citaActual.setFechaModificacionDispositivo(fechaActual);
        calendarioActivity.citaActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
        CitaReunionController.modificarCita(calendarioActivity.citaActual,getActivity());
        // Se Guarda la cita movimiento estado
        CitaReunionController.guardarCitaMovimientoEstado_GenerarIDDispositivo(GenerarCitaMovimientoEstadoDesdeCita(calendarioActivity.citaActual, fechaActual));
        ///////
        sincronizar();
    }

    public void dialogHoraFinMenorHoraInicio()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this.getActivity());
        final View dialogHoraFinMenorHoraInicio = layoutInflater.inflate(R.layout.dialog_general, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        TextView tvTitulo = (TextView) dialogHoraFinMenorHoraInicio.findViewById(R.id.tvTitulo);
        tvTitulo.setText("La hora fin debe ser mayor a la hora inicio.");

        builder.setView(dialogHoraFinMenorHoraInicio).setCancelable(false).setPositiveButton("Continuar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                cargarDialogHoraFin(false);
            }
        }).setNegativeButton("", null);


        AlertDialog  alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Button btnPositivo = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        btnPositivo.setTextColor(getResources().getColor(R.color.colorCeleste));
        setFuente.setButton(getActivity(),btnPositivo);

    }



}
