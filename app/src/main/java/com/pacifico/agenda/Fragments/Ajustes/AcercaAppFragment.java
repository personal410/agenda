package com.pacifico.agenda.Fragments.Ajustes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pacifico.agenda.R;
import com.pacifico.agenda.Views.Dialogs.DialogGeneral;


public class AcercaAppFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_acerca_app, container, false);

        TextView terminos = (TextView)view.findViewById(R.id.TerminosUso);
        TextView Privacidad = (TextView)view.findViewById(R.id.privacidad) ;
        ImageView img1 = (ImageView)view.findViewById(R.id.imgPrivacidad) ;
        ImageView img2 = (ImageView)view.findViewById(R.id.imgTerminos) ;

        Privacidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Privacidad();
            }
        });


        terminos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListarMensajes();
            }
        });
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Privacidad();
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListarMensajes();
            }
        });

        return view;
    }
        public void ListarMensajes(){
String texto1 ="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum" +
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
            FragmentManager fm = getChildFragmentManager();
            DialogGeneral dialogGeneral = new DialogGeneral();
            dialogGeneral.setListener(null);
            dialogGeneral.setDatos("Condiciones", texto1, "Aceptar");
            dialogGeneral.show(fm, "dialogo1");
    }


    public void Privacidad(){

        String Texto ="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        FragmentManager fm = getChildFragmentManager();
        DialogGeneral dialogGeneral = new DialogGeneral();
        dialogGeneral.setListener(null);
        dialogGeneral.setDatos("Privacidad",Texto, "Aceptar");
        dialogGeneral.show(fm, "dialogo1");
    }
}