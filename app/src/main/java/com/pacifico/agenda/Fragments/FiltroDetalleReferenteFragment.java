package com.pacifico.agenda.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoDatosReferidoBean;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joel on 25/04/2016.
 */
public class FiltroDetalleReferenteFragment extends Fragment {

    private CalendarioActivity calendarioActivity;
    private ListView lvLista;
    private FiltroDetalleReferenteAdapter adapter;
    private TextView tvNomReferente;
    private ImageView ivCerrarReferidos;
    String nombreCompletoReferente;

    public static FiltroDetalleReferenteFragment newInstance() {
        FiltroDetalleReferenteFragment fragment = new FiltroDetalleReferenteFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        View mView = inflater.inflate(R.layout.fragment_detalle_filtro_referente, container, false);
        this.lvLista = (ListView) mView.findViewById(R.id.lvLista);
        this.tvNomReferente = (TextView) mView.findViewById(R.id.tvNomReferente);
        this.ivCerrarReferidos = (ImageView) mView.findViewById(R.id.ivCerrarReferidos);

        // Generando Nombre de Referente
        nombreCompletoReferente = String.format("%s %s %s",
                Util.capitalizedString(calendarioActivity.prospectoDatosRefFiltro.getNombres()),
                Util.capitalizedString(calendarioActivity.prospectoDatosRefFiltro.getApellidoPaterno()),
                Util.capitalizedString(calendarioActivity.prospectoDatosRefFiltro.getApellidoMaterno())
        ).trim();

        tvNomReferente.setText(nombreCompletoReferente);
        tvNomReferente.setTextSize(18);

        ivCerrarReferidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        obtenerReferidos();

        return mView;
    }

    private void obtenerReferidos() {
        ArrayList<ProspectoDatosReferidoBean> listaReferidos = new ArrayList<>();
        listaReferidos = ProspectoController.obtenerReferidosxIDProspectoDispositivo(calendarioActivity.prospectoDatosRefFiltro.getIdProspectoDispositivo());
        adapter = new FiltroDetalleReferenteAdapter(getActivity(), R.layout.row_filtro_detalle_referente, listaReferidos);
        lvLista.setAdapter(adapter);
    }

    class FiltroDetalleReferenteAdapter extends ArrayAdapter<ProspectoDatosReferidoBean> {

        private Activity context;
        private int resource;
        private List<ProspectoDatosReferidoBean> items;

        public FiltroDetalleReferenteAdapter(Activity context, int resource, List<ProspectoDatosReferidoBean> items) {
            super(context, resource, items);
            this.context = context;
            this.resource = resource;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final ProspectoDatosReferidoBean item = items.get(position);
            final LinearLayout rowView;

            rowView = new LinearLayout(getContext());
            inflater.inflate(resource, rowView, true);

            ImageView ivProspecto =  (ImageView)rowView.findViewById(R.id.ivProspecto);
            TextView tvProspecto =  (TextView)rowView.findViewById(R.id.tvProspecto);
            TextView tvReferencia =  (TextView)rowView.findViewById(R.id.tvReferencia);

            String nombreCompleto = String.format("%s %s %s",
                    Util.capitalizedString(item.getNombres()),
                    Util.capitalizedString(item.getApellidoPaterno()),
                    Util.capitalizedString(item.getApellidoMaterno())
            ).trim();

            tvProspecto.setText(nombreCompleto);

            //  REFERENTE
            tvReferencia.setText("Ref. " + nombreCompletoReferente);

            //////////////// Cargando Imagenes

            if (item.EstadoProspecto == Constantes.EstadoProspecto_Nuevo)
                ivProspecto.setImageResource(R.drawable.ic_action_action_info_outline);
            else if (item.EstadoProspecto == Constantes.EstadoProspecto_NoInteresado)
            {
                ivProspecto.setImageResource(R.drawable.cancel);
            }
            else if(item.EstadoProspecto == Constantes.EstadoProspecto_CierreVenta)
            {
                ivProspecto.setImageResource(R.drawable.ic_action_ic_done_all_blue);
            }
            else // SI es en proceso se considera la ultima cita
            {
                if (item.EstadoUltCita == Constantes.EstadoCita_PorContactar)
                    ivProspecto.setImageResource(R.drawable.ic_telefono_morado);
                else if (item.EstadoUltCita == Constantes.EstadoCita_Agendada)
                    ivProspecto.setImageResource(R.drawable.calendario_gris_oscuro);
                else if (item.EstadoUltCita == Constantes.EstadoCita_Realizada)
                    ivProspecto.setImageResource(R.drawable.ic_action_ic_done_all_black_24dp);
            }

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(item.getIdProspectoDispositivo());
                    ((CalendarioActivity)context).GestionFlujoProspecto(prospectoBean);
                }
            });

            return rowView;
        }
    }

}
