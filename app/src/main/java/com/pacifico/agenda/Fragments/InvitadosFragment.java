package com.pacifico.agenda.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;

/**
 * Created by Joel on 08/05/2016.
 */
public class InvitadosFragment extends Fragment {

    private CalendarioActivity calendarioActivity;

    LinearLayout linCabecera;
    ImageButton btnRetrocederInvitados;
    EditText edtCorreo;
    AppCompatCheckBox chkGerenteUnidad;
    AppCompatCheckBox chkGerenteAgencia;

    String valorOriginal;

    public static InvitadosFragment newInstance() {
        InvitadosFragment fragment = new InvitadosFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_invitados, container, false);

        linCabecera = (LinearLayout) linfragment.findViewById(R.id.linCabeceraEntrevistaInvitados);

        btnRetrocederInvitados = (ImageButton) linfragment.findViewById(R.id.btnRetrocederInvitados);

        chkGerenteUnidad = (AppCompatCheckBox) linfragment.findViewById(R.id.chkGerenteUnidad);
        chkGerenteAgencia = (AppCompatCheckBox) linfragment.findViewById(R.id.chkGerenteAgencia);

        edtCorreo = (EditText) linfragment.findViewById(R.id.edtCorreo);

        FormatearTarjeta();

        btnRetrocederInvitados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chkGerenteUnidad.isChecked())
                    calendarioActivity.citaActual.setFlagInvitadoGU(1);
                else
                    calendarioActivity.citaActual.setFlagInvitadoGU(0);

                if (chkGerenteAgencia.isChecked())
                    calendarioActivity.citaActual.setFlagInvitadoGA(1);
                else
                    calendarioActivity.citaActual.setFlagInvitadoGA(0);

                // TODO: Se debe permitir vacio aca??

                String emailNuevo = edtCorreo.getText().toString().trim();
                if (emailNuevo == "") emailNuevo = null;

                if (valorOriginal != emailNuevo) {
                    calendarioActivity.prospectoActual.setCorreoElectronico1(emailNuevo);
                    calendarioActivity.citaModificoProspecto = true;
                }

                getFragmentManager().popBackStack();
                ((CalendarioActivity)getActivity()).OcultarTeclado();
            }
        });

        //edtInvitado = (EditText) linfragment.findViewById(R.id.edtInvitado);
        edtCorreo.requestFocus();

        CargarDatosInicial();
        // Inflate the layout for this fragment
        return linfragment;
    }

    public void CargarDatosInicial() {
        if (calendarioActivity.citaActual.getFlagInvitadoGA() == 1)
            chkGerenteAgencia.setChecked(true);
        else
            chkGerenteAgencia.setChecked(false);

        if (calendarioActivity.citaActual.getFlagInvitadoGU() == 1)
            chkGerenteUnidad.setChecked(true);
        else
            chkGerenteUnidad.setChecked(false);

        valorOriginal = calendarioActivity.prospectoActual.getCorreoElectronico1();

        if (calendarioActivity.prospectoActual.getCorreoElectronico1() != null)
            edtCorreo.setText(calendarioActivity.prospectoActual.getCorreoElectronico1());
    }

    public void FormatearTarjeta()
    {
        if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista) {
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_primera_entrevista));
        }else if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_SegundaEntrevista) {
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_segunda_entrevista));
        }else if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_EntrevistaAdicional){
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_entrevista_adicional));
        }
    }



}
