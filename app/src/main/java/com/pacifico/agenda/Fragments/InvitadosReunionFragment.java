package com.pacifico.agenda.Fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.R;

/**
 * Created by joel on 8/15/16.
 */
public class InvitadosReunionFragment extends Fragment{

    private CalendarioActivity calendarioActivity;

    LinearLayout linCabecera;
    ImageButton btnRetrocederInvitadosReunion;
    AppCompatCheckBox chkGerenteUnidadReunion;
    AppCompatCheckBox chkGerenteAgenciaReunion;

    public static InvitadosReunionFragment newInstance() {
        InvitadosReunionFragment fragment = new InvitadosReunionFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_invitados_reunion, container, false);

        linCabecera = (LinearLayout) linfragment.findViewById(R.id.linCabeceraEntrevistaInvitadosReunion);

        btnRetrocederInvitadosReunion = (ImageButton) linfragment.findViewById(R.id.btnRetrocederInvitadosReunion);

        chkGerenteUnidadReunion = (AppCompatCheckBox) linfragment.findViewById(R.id.chkGerenteUnidadReunion);
        chkGerenteAgenciaReunion = (AppCompatCheckBox) linfragment.findViewById(R.id.chkGerenteAgenciaReunion);

        btnRetrocederInvitadosReunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chkGerenteUnidadReunion.isChecked())
                    calendarioActivity.reunionActual.setFlagInvitadoGU(1);
                else
                    calendarioActivity.reunionActual.setFlagInvitadoGU(0);

                if (chkGerenteAgenciaReunion.isChecked())
                    calendarioActivity.reunionActual.setFlagInvitadoGA(1);
                else
                    calendarioActivity.reunionActual.setFlagInvitadoGA(0);

                getFragmentManager().popBackStack();
                ((CalendarioActivity)getActivity()).OcultarTeclado();
            }
        });

        CargarDatosInicial();
        // Inflate the layout for this fragment
        return linfragment;
    }

    public void CargarDatosInicial() {

        if (calendarioActivity.reunionActual.getFlagInvitadoGA() == 1)
            chkGerenteAgenciaReunion.setChecked(true);
        else
            chkGerenteAgenciaReunion.setChecked(false);

        if (calendarioActivity.reunionActual.getFlagInvitadoGU() == 1)
            chkGerenteUnidadReunion.setChecked(true);
        else
            chkGerenteUnidadReunion.setChecked(false);
    }
}
