package com.pacifico.agenda.Fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Activity.InicioSesionActivity;
import com.pacifico.agenda.Model.Bean.ReunionInternaBean;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.Model.Controller.IntermediarioController;
import com.pacifico.agenda.Network.RespuestaSincronizacionListener;
import com.pacifico.agenda.Network.SincronizacionController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;
import com.pacifico.agenda.Views.CustomView.setFuente;
import com.pacifico.agenda.Views.Dialogs.DialogGeneral;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by joel on 8/3/16.
 */
public class ReunionFragment extends Fragment {

    boolean flag_click = false;
    private CalendarioActivity calendarioActivity;

    ImageButton btnCerrarReunionInterna;
    TextView txtFechaReunion;
    TextView txtHoraReunion;
    TextView txtUbicacionReunion;
    TextView txtInvitadosReunion;
    RadioGroup rdgTipoReunion;
    RadioButton rdbReunionInterna;
    RadioButton rdbReunionExterna;

    Button btnGuardarReunion;

    private Calendar calFechaReunion;
    private Calendar calHoraInicioReunion;
    private Calendar calHoraFinReunion;

    private Calendar calMaxFechaEntrevista;
    private Calendar calMinFechaEntrevista;

    private ProgressDialog progressDialog;

    private LinearLayout linFechaReunion;
    private LinearLayout linHoraReunion;

    public static ReunionFragment newInstance() {
        ReunionFragment fragment = new ReunionFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calMaxFechaEntrevista = Calendar.getInstance();
        calMaxFechaEntrevista.add(Calendar.YEAR, 1);

        calMinFechaEntrevista = Calendar.getInstance();
        calMinFechaEntrevista.add(Calendar.MONTH, -1);

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_reunion, container, false);

        btnCerrarReunionInterna = (ImageButton) linfragment.findViewById(R.id.btnCerrarReunionInterna);
        txtFechaReunion = (TextView) linfragment.findViewById(R.id.txtFechaReunion);
        txtHoraReunion = (TextView) linfragment.findViewById(R.id.txtHoraReunion);
        txtUbicacionReunion = (TextView) linfragment.findViewById(R.id.txtUbicacionReunion);
        txtInvitadosReunion = (TextView) linfragment.findViewById(R.id.txtInvitadosReunion);
        btnGuardarReunion = (Button) linfragment.findViewById(R.id.btnGuardarReunion);
        rdgTipoReunion = (RadioGroup) linfragment.findViewById(R.id.rdgTipoReunion);
        rdbReunionInterna = (RadioButton)linfragment.findViewById(R.id.rdbReunionInterna);
        rdbReunionExterna = (RadioButton)linfragment.findViewById(R.id.rdbReunionExterna);
        linFechaReunion = (LinearLayout) linfragment.findViewById(R.id.linFechaReunion);
        linHoraReunion = (LinearLayout) linfragment.findViewById(R.id.linHoraReunion);

        calFechaReunion = null;
        calHoraInicioReunion = null;
        calHoraFinReunion = null;

        linFechaReunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarDialogFecha();
            }
        });

        linHoraReunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (calFechaReunion != null)
                    cargarDialogHoraInicio();
                else
                    cargarDialogFecha();
            }
        });

        btnCerrarReunionInterna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CalendarioActivity)getActivity()).CerrarTarjetas();
            }
        });

        txtUbicacionReunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, UbicacionReunionFragment.newInstance()); // newInstance() is a static factory method.

                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        txtInvitadosReunion = (TextView) linfragment.findViewById(R.id.txtInvitadosReunion);
        txtInvitadosReunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, InvitadosReunionFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        btnCerrarReunionInterna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if (!flagClick) {
                //    flagClick = true;
                    reiniciarValoresyCerrarTarjeta();
                //    flagClick = false;
                //}
            }
        });

        btnGuardarReunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag_click) return;

                flag_click = true;

                if(!rdbReunionInterna.isChecked() && !rdbReunionExterna.isChecked())
                {
                    mensajeGenerico("Ingrese tipo de Reunión");
                    flag_click = false;
                    return;
                }

                /////
                // Validando  Fecha/Hora repetida
                boolean fechaHoraNueva_valido = false;

                if(calendarioActivity.reunionActual.getFechaReunion() != null &&
                        !"".equals(calendarioActivity.reunionActual.getFechaReunion()) &&
                        calendarioActivity.reunionActual.getHoraInicio() != null &&
                        !"".equals(calendarioActivity.reunionActual.getHoraInicio()) &&
                        calendarioActivity.reunionActual.getHoraFin() != null &&
                        !"".equals(calendarioActivity.reunionActual.getHoraFin()))
                            fechaHoraNueva_valido = true;

                boolean tieneReunionenHorario = false;

                if (fechaHoraNueva_valido)
                    tieneReunionenHorario = CitaReunionController.validarCitaMismoHorarioReunion(calendarioActivity.reunionActual);
                ///////

                if (tieneReunionenHorario)
                {
                    dialogReunionMismaHora();
                }else{
                    guardarReunion();
                }
            }
        });

        CargarDatosInicial();

        // Inflate the layout for this fragment
        return linfragment;
    }

    public void cargarDialogFecha(){
        int anio,mes,dia;
        if (calFechaReunion != null)
        {
            anio = calFechaReunion.get(Calendar.YEAR);
            mes = calFechaReunion.get(Calendar.MONTH);
            dia = calFechaReunion.get(Calendar.DAY_OF_MONTH);
        }else
        {
            Calendar calini = Calendar.getInstance();
            anio = calini.get(Calendar.YEAR);
            mes = calini.get(Calendar.MONTH);
            dia = calini.get(Calendar.DAY_OF_MONTH);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), DatePickerDialog.THEME_HOLO_LIGHT,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calFechaReunion = Calendar.getInstance();
                calFechaReunion.set(Calendar.YEAR, year);
                calFechaReunion.set(Calendar.MONTH, monthOfYear);
                calFechaReunion.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateEdtFechaReunion();

                // Abrir la Hora Inicio
                cargarDialogHoraInicio();
            }
        }, anio, mes, dia);

        datePickerDialog.getDatePicker().setMinDate(calMinFechaEntrevista.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(calMaxFechaEntrevista.getTimeInMillis());
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Siguiente", datePickerDialog);
        datePickerDialog.getDatePicker().setCalendarViewShown(false);
        datePickerDialog.setTitle("Seleccione una fecha");
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();
    }

    public void cargarDialogHoraInicio(){
        int horaInicio,minutoInicio;
        if (calHoraInicioReunion != null)
        {
            horaInicio = calHoraInicioReunion.get(Calendar.HOUR_OF_DAY);
            minutoInicio = calHoraInicioReunion.get(Calendar.MINUTE);
        }else
        {
            horaInicio = 12;
            minutoInicio = 0;
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar calHoraIniTemp = calHoraInicioReunion;

                calHoraInicioReunion = Calendar.getInstance();
                calHoraInicioReunion.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calHoraInicioReunion.set(Calendar.MINUTE, minute);

                updateEdtHora();

                if(calHoraIniTemp == null ||(calHoraInicioReunion.get(Calendar.HOUR_OF_DAY) == calHoraIniTemp.get(Calendar.HOUR_OF_DAY) && calHoraInicioReunion.get(Calendar.MINUTE) == calHoraIniTemp.get(Calendar.MINUTE))) {
                    cargarDialogHoraFin(false); // No reagenda
                }else
                {
                    cargarDialogHoraFin(true); //reagenda
                }
            }
        }, horaInicio, minutoInicio, false);

        timePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Siguiente", timePickerDialog);
        timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", timePickerDialog);
        timePickerDialog.setCancelable(false);

        timePickerDialog.setTitle("Seleccione una hora de inicio");
        timePickerDialog.setCancelable(false);
        timePickerDialog.show();
    }

    private void guardarReunion(){
        // Seteando el número de entrevista creación
        calendarioActivity.reunionActual.setCodigoIntermediario(IntermediarioController.obtenerIntermediario().getCodigoIntermediario());
        calendarioActivity.reunionActual.setFlagEnviado(0);

        if (rdgTipoReunion.getCheckedRadioButtonId() == rdbReunionInterna.getId())
            calendarioActivity.reunionActual.setCodigoTipoReunion(Constantes.Reunion_Interna);
        else if (rdgTipoReunion.getCheckedRadioButtonId() == rdbReunionExterna.getId())
            calendarioActivity.reunionActual.setCodigoTipoReunion(Constantes.Reunion_Externa);

        if (calendarioActivity.reunionActual.getIdReunionInternaDispositivo() == -1) {
            calendarioActivity.reunionActual.setFechaCreacionDispositivo(Util.obtenerFechaActual());
            CitaReunionController.guardarReunion_GenerarIDDispositivo(calendarioActivity.reunionActual);
        }
        else {
            calendarioActivity.reunionActual.setFechaModificacionDispositivo(Util.obtenerFechaActual());
            CitaReunionController.modificarReunion(calendarioActivity.reunionActual);
        }

        // Llamar al envío de datos #ENVIO
        sincronizarReiniciarValoresyCerrarTarjeta();
    }

    private void mostrarMensajeParaContinuar(String mensaje){
        new AlertDialog.Builder(getContext())
                .setTitle("Alerta")
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                continuar();
            }
        }).setCancelable(false).show();
    }

    // continua Luego de la sincronización
    private void continuar() {
        reiniciarValoresyCerrarTarjeta();
        flag_click = false;
    }

    public void cargarDialogHoraFin(boolean reagenda){
        int hora,minuto;
        if (calHoraFinReunion != null)
        {
            hora = calHoraFinReunion.get(Calendar.HOUR_OF_DAY);
            minuto = calHoraFinReunion.get(Calendar.MINUTE);

            if(reagenda) {
                if (calHoraInicioReunion != null &&
                        (calHoraInicioReunion.get(Calendar.HOUR_OF_DAY)*60 +calHoraInicioReunion.get(Calendar.MINUTE)) >= (hora*60+minuto)) {
                    hora = calHoraInicioReunion.get(Calendar.HOUR_OF_DAY) + 1;
                    minuto = calHoraInicioReunion.get(Calendar.MINUTE);
                }
            }

        }else
        {
            if (calHoraInicioReunion == null) {
                hora = 12;
                minuto = 0;
            }else{ // Por defecto una hora mas tarde que el inicio
                hora = calHoraInicioReunion.get(Calendar.HOUR_OF_DAY)+1;
                minuto = calHoraInicioReunion.get(Calendar.MINUTE);
            }
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar calHoraFinReunionTemp = Calendar.getInstance();
                calHoraFinReunionTemp.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calHoraFinReunionTemp.set(Calendar.MINUTE, minute);

                if ((calHoraInicioReunion.get(Calendar.HOUR_OF_DAY)*60 + calHoraInicioReunion.get(Calendar.MINUTE)) >= (calHoraFinReunionTemp.get(Calendar.HOUR_OF_DAY)*60 + calHoraFinReunionTemp.get(Calendar.MINUTE)))
                {
                    dialogHoraFinMenorHoraInicio();
                }else {
                    calHoraFinReunion = calHoraFinReunionTemp;
                    updateEdtHora();
                }
            }
        }, hora, minuto, false);
        timePickerDialog.setTitle("Seleccione una hora de fin:");
        timePickerDialog.setCancelable(false);
        timePickerDialog.show();
    }

    private void updateEdtFechaReunion(){

        String diaSemanaTexto= "";
        String mesTexto = "";

        int diaReunion = calFechaReunion.get(Calendar.DAY_OF_MONTH);
        int mesReunion = calFechaReunion.get(Calendar.MONTH);
        int diaSemana = calFechaReunion.get(Calendar.DAY_OF_WEEK);

        // Seteando el Texto Dia
        diaSemanaTexto = Constantes.ObtenerTextoAbreviadoDiaSemana(diaSemana);
        // Seteando el Texto Mes
        mesTexto = Constantes.ObtenerTextoAbreviadoMes(mesReunion);

        txtFechaReunion.setText(diaSemanaTexto +", "+ diaReunion + " " + mesTexto);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.formato_solofecha_envio);
        calendarioActivity.reunionActual.setFechaReunion(simpleDateFormat.format(calFechaReunion.getTime()));
    }

    private void updateEdtHora(){
        SimpleDateFormat simpleDateFormatIni = new SimpleDateFormat("h:mm");
        SimpleDateFormat simpleDateFormatFin = new SimpleDateFormat("h:mm a");

        String text_horaInicio = "";
        String text_horaFin = "";
        if (calHoraInicioReunion != null)
        {
            text_horaInicio = simpleDateFormatIni.format(calHoraInicioReunion.getTime());
            SimpleDateFormat simpleDateFormatGuardar = new SimpleDateFormat("HH:mm");
            calendarioActivity.reunionActual.setHoraInicio(simpleDateFormatGuardar.format(calHoraInicioReunion.getTime()));
        }
        if (calHoraFinReunion != null) {
            text_horaFin = simpleDateFormatFin.format(calHoraFinReunion.getTime());
            SimpleDateFormat simpleDateFormatGuardar = new SimpleDateFormat("HH:mm");
            calendarioActivity.reunionActual.setHoraFin(simpleDateFormatGuardar.format(calHoraFinReunion.getTime()));
        }

        txtHoraReunion.setText(text_horaInicio + " - "+text_horaFin);
    }

    public void CargarDatosInicial() {
        if (calendarioActivity.reunionActual != null) {

            if (calendarioActivity.reunionActual.getCodigoTipoReunion() == Constantes.Reunion_Interna)
                rdgTipoReunion.check(rdbReunionInterna.getId());
            else if (calendarioActivity.reunionActual.getCodigoTipoReunion() == Constantes.Reunion_Externa)
                rdgTipoReunion.check(rdbReunionExterna.getId());

            if (calendarioActivity.reunionActual.getUbicacion() != null)
                txtUbicacionReunion.setText(calendarioActivity.reunionActual.getUbicacion());

            // Seteando Invitados
            String invitados = "";
            if (calendarioActivity.reunionActual.getFlagInvitadoGU() == Constantes.FLAG_VERDADERO)
            {
                invitados = invitados + " Gerente de Unidad";
            }
            if (calendarioActivity.reunionActual.getFlagInvitadoGA() == Constantes.FLAG_VERDADERO)
            {
                if (!"".equals(invitados))
                    invitados = invitados + " \n";
                invitados = invitados + " Gerente de Agencia";
            }

            txtInvitadosReunion.setText(invitados);

            // Fecha de la reunion
            if (calendarioActivity.reunionActual.getFechaReunion() != null && !"".equals(calendarioActivity.reunionActual.getFechaReunion()))
            {
                Date dateReunion = null;
                try
                {
                    dateReunion = new SimpleDateFormat(Constantes.formato_solofecha_envio).parse(calendarioActivity.reunionActual.getFechaReunion());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(dateReunion);

                    String diaSemanaTexto= "";
                    String mesTexto = "";

                    int mesReunion = cal.get(Calendar.MONTH);
                    int diaReunion = cal.get(Calendar.DAY_OF_MONTH);
                    int diaSemana = cal.get(Calendar.DAY_OF_WEEK);

                    // TODO: Pasar a DateFormat
                    // Seteando el Texto Dia
                    diaSemanaTexto = Constantes.ObtenerTextoAbreviadoDiaSemana(diaSemana);
                    // Seteando el Texto Mes
                    mesTexto = Constantes.ObtenerTextoAbreviadoMes(mesReunion);

                    txtFechaReunion.setText(diaSemanaTexto +", "+ diaReunion + " " + mesTexto);
                    calFechaReunion = cal;
                }
                catch(Exception e){}
            }

            String horaReunion ="";
            // Hora Inicio de la reunion
            if (calendarioActivity.reunionActual.getHoraInicio() != null && !"".equals(calendarioActivity.reunionActual.getHoraInicio()))
            {
                String pattern = "HH:mm";
                Date horaInicioReunion = null;
                try
                {
                    horaInicioReunion = new SimpleDateFormat(pattern).parse(calendarioActivity.reunionActual.getHoraInicio());

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(horaInicioReunion);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm");
                    String text_horaInicio = simpleDateFormat.format(horaInicioReunion);
                    horaReunion = text_horaInicio;

                    calHoraInicioReunion = cal;
                }
                catch(Exception e){}
            }

            // Hora Fin de la reunion
            if (calendarioActivity.reunionActual.getHoraFin() != null && !"".equals(calendarioActivity.reunionActual.getHoraFin()))
            {
                String pattern = "HH:mm";
                Date horaFinReunion = null;
                try
                {
                    horaFinReunion = new SimpleDateFormat(pattern).parse(calendarioActivity.reunionActual.getHoraFin());

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(horaFinReunion);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");
                    String text_horaFin = simpleDateFormat.format(horaFinReunion);
                    horaReunion = horaReunion + " - " + text_horaFin;

                    calHoraFinReunion = cal;
                }
                catch(Exception e){}
            }

            txtHoraReunion.setText(horaReunion);
        }
    }

    public void reiniciarValoresyCerrarTarjeta()
    {
        ////////// Reseteo de valores
        calendarioActivity.reiniciarParametros();
        calendarioActivity.CerrarTarjetas();

        // TODO: Hay que revisar si la cita esta en la semana actual del calendario, de no ser así no es necesario recargar
        calendarioActivity.actualizarCalendario();
        ///////
    }

///
    public void sincronizarReiniciarValoresyCerrarTarjeta() {
        SincronizacionController sincronizacionController = new SincronizacionController();
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Enviando información");
            progressDialog.setMessage("Espere, por favor");
        }
        progressDialog.setCancelable(false);
        progressDialog.show();
        sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
            @Override
            public void terminoSincronizacion(int codigo, String mensaje) {
                progressDialog.dismiss();
                if (codigo < 0) {
                    mostrarMensajeParaContinuar(mensaje);
                } else if (codigo == 1) {
                      mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
                    //continuar();
                } else {
                    if (codigo == 4 || codigo == 6) {
                        Intent inicioSesionIntent = new Intent(getActivity(), InicioSesionActivity.class);
                        inicioSesionIntent.putExtra("etapa", (codigo - 4) / 2 + 1);
                        startActivity(inicioSesionIntent);
                    } else {
                        mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                    }
                }
            }
        });
        int resultado = sincronizacionController.sincronizar(getContext());
        if (resultado == 0) {
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
            //continuar();
        } else if (resultado == 2) {
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
        }
    }

    public void mensajeGenerico (String valor){
        // Mensaje Generico de Cita Guardada
        FragmentManager fm = getChildFragmentManager();
        DialogGeneral dialogGeneral = new DialogGeneral();
        dialogGeneral.setListener(null);
        dialogGeneral.setDatos(valor, "", getString(R.string.continuar));
        dialogGeneral.setCancelable(false);
        dialogGeneral.show(fm, "dialogo1");
    }

    public void dialogReunionMismaHora()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this.getActivity());
        final View dialogReunionMismaHora = layoutInflater.inflate(R.layout.dialog_general, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        TextView tvTitulo = (TextView) dialogReunionMismaHora.findViewById(R.id.tvTitulo);
        tvTitulo.setText("Tiene una reunión a la misma hora.¿Desea proceder?");

        builder.setView(dialogReunionMismaHora).setCancelable(false).setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                guardarReunion();
                flag_click = false;
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                flag_click = false;
            }
        });

        AlertDialog  alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Button btnCancelar = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button btnAceptar = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        btnAceptar.setTextColor(getResources().getColor(R.color.colorCeleste));
        btnCancelar.setTextColor(getResources().getColor(R.color.colorMarron));
        setFuente.setButton(getActivity(),btnAceptar);
        setFuente.setButton(getActivity(),btnCancelar);

    }

    public void dialogHoraFinMenorHoraInicio()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this.getActivity());
        final View dialogHoraFinMenorHoraInicio = layoutInflater.inflate(R.layout.dialog_general, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        TextView tvTitulo = (TextView) dialogHoraFinMenorHoraInicio.findViewById(R.id.tvTitulo);
        tvTitulo.setText("La hora fin debe ser mayor a la hora inicio.");

        builder.setView(dialogHoraFinMenorHoraInicio).setCancelable(false).setPositiveButton("Continuar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                cargarDialogHoraFin(false);
            }
        }).setNegativeButton("", null);

        AlertDialog  alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Button btnCancelar = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button btnAceptar = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        btnAceptar.setTextColor(getResources().getColor(R.color.colorCeleste));
        btnCancelar.setTextColor(getResources().getColor(R.color.colorMarron));
        setFuente.setButton(getActivity(),btnAceptar);
        setFuente.setButton(getActivity(),btnCancelar);
    }

}
