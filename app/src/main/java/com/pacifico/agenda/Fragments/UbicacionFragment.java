package com.pacifico.agenda.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;

/**
 * Created by Joel on 08/05/2016.
 */
public class UbicacionFragment extends Fragment {

    private CalendarioActivity calendarioActivity;
    ImageButton btnRetrocederUbicacion;
    EditText edtUbicacion;
    EditText edtReferencia;

    LinearLayout linCabecera;

    public static UbicacionFragment newInstance() {
        UbicacionFragment fragment = new UbicacionFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_ubicacion, container, false);

        linCabecera = (LinearLayout) linfragment.findViewById(R.id.linCabeceraEntrevistaUbicacion);

        btnRetrocederUbicacion = (ImageButton) linfragment.findViewById(R.id.btnRetrocederUbicacion);

        btnRetrocederUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarioActivity.citaActual.setUbicacion(edtUbicacion.getText().toString());
                calendarioActivity.citaActual.setReferenciaUbicacion(edtReferencia.getText().toString());

                getFragmentManager().popBackStack();

                ((CalendarioActivity)getActivity()).OcultarTeclado();
            }
        });

        FormatearTarjeta();

        edtUbicacion = (EditText) linfragment.findViewById(R.id.edtUbicacion);
        edtUbicacion.requestFocus();

        edtReferencia = (EditText) linfragment.findViewById(R.id.edtReferencia);

        CargarDatosInicial();

        // Inflate the layout for this fragment
        return linfragment;
    }

    public void CargarDatosInicial() {
        if (calendarioActivity.citaActual != null && calendarioActivity.citaActual.getUbicacion() != null)
            edtUbicacion.setText(calendarioActivity.citaActual.getUbicacion());

        if (calendarioActivity.citaActual != null && calendarioActivity.citaActual.getReferenciaUbicacion() != null)
            edtReferencia.setText(calendarioActivity.citaActual.getReferenciaUbicacion());
    }

    public void FormatearTarjeta()
    {
        if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista) {
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_primera_entrevista));
        }else if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_SegundaEntrevista) {
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_segunda_entrevista));
        }else if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_EntrevistaAdicional){
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_entrevista_adicional));
        }
    }
}
