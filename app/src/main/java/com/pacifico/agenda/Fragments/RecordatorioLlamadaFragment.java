package com.pacifico.agenda.Fragments;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Joel on 09/05/2016.
 */
public class RecordatorioLlamadaFragment extends Fragment {

    private CalendarioActivity calendarioActivity;
    ImageButton btnRetrocederRecordatorio;
    TextView txtFechaRecordatorio;
    TextView txtHoraRecordatorio;
    LinearLayout linCabecera;

    String fechaOriginalRecordatorio;

    private Calendar calFechaRecordatorio;
    private Calendar calHoraRecordatorio;

    private Calendar calMaxFechaRecordatorio;
    private Calendar calMinFechaRecordatorio;

    public static RecordatorioLlamadaFragment newInstance() {
        RecordatorioLlamadaFragment fragment = new RecordatorioLlamadaFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calMaxFechaRecordatorio = Calendar.getInstance();
        calMaxFechaRecordatorio.add(Calendar.YEAR, 1);

        calMinFechaRecordatorio = Calendar.getInstance();
        calMinFechaRecordatorio.add(Calendar.MONTH, -1);

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_recordatorio_llamada, container, false);

        linCabecera = (LinearLayout) linfragment.findViewById(R.id.linEntrevistaRecordatorioLLamada);
        txtFechaRecordatorio = (TextView) linfragment.findViewById(R.id.txtFechaRecordatorio);
        txtHoraRecordatorio = (TextView) linfragment.findViewById(R.id.txtHoraRecordatorio);

        FormatearTarjeta();

        btnRetrocederRecordatorio = (ImageButton) linfragment.findViewById(R.id.btnRetrocederRecordatorio);

        // Seteandor Calendarios
        calFechaRecordatorio = null;
        calHoraRecordatorio = null;

        txtFechaRecordatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarDialogFechaRecordatorio();
            }
        });

        txtHoraRecordatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calFechaRecordatorio != null)
                    cargarDialogHoraRecordatorio();
                else
                    cargarDialogFechaRecordatorio();
            }
        });

        btnRetrocederRecordatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                //((CalendarioActivity)getActivity()).OcultarTeclado();
                // todo: Mostrar Mensaje si no se guarda??
                if (calFechaRecordatorio != null && calHoraRecordatorio != null)
                {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.formato_solofecha_envio);
                    SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
                    String nuevafechaRecordatorio = dateFormat.format(calFechaRecordatorio.getTime()) + " "
                            + hourFormat.format(calHoraRecordatorio.getTime());

                    // Si cambia la fecha se actualiza el recordatorio
                    if (!nuevafechaRecordatorio.equals(fechaOriginalRecordatorio)) {
                        // Siempre se genera uno nuevo - a la hora de guardar el anterior se desactiva
                        RecordatorioLlamadaBean recordatorioLlamadaBean = new RecordatorioLlamadaBean();

                        recordatorioLlamadaBean.setIdProspecto(calendarioActivity.prospectoActual.getIdProspecto());
                        recordatorioLlamadaBean.setIdProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());

                        // No necesariamente se tiene los ids de citas en este punto
                        recordatorioLlamadaBean.setIdCita(calendarioActivity.citaActual.getIdCita());
                        recordatorioLlamadaBean.setIdCitaDispositivo(calendarioActivity.citaActual.getIdCitaDispositivo());

                        recordatorioLlamadaBean.setFlagActivo(Constantes.FLAG_ACTIVO);
                        recordatorioLlamadaBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

                        recordatorioLlamadaBean.setFechaRecordatorio(nuevafechaRecordatorio);

                        calendarioActivity.citaActual.setRecordatorioLlamadaBean(recordatorioLlamadaBean);
                    }
                }
            }
        });

        CargarDatosInicial();

        // Inflate the layout for this fragment
        return linfragment;
    }

    public void cargarDialogFechaRecordatorio(){
        int anio,mes,dia;
        if (calFechaRecordatorio != null)
        {
            anio = calFechaRecordatorio.get(Calendar.YEAR);
            mes = calFechaRecordatorio.get(Calendar.MONTH);
            dia = calFechaRecordatorio.get(Calendar.DAY_OF_MONTH);
        }else
        {
            Calendar calini = Calendar.getInstance();
            anio = calini.get(Calendar.YEAR);
            mes = calini.get(Calendar.MONTH);
            dia = calini.get(Calendar.DAY_OF_MONTH);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), DatePickerDialog.THEME_HOLO_LIGHT,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calFechaRecordatorio = Calendar.getInstance();

                calFechaRecordatorio.set(Calendar.YEAR, year);
                calFechaRecordatorio.set(Calendar.MONTH, monthOfYear);
                calFechaRecordatorio.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateEdtFechaRecordatorio();

                // Abrir la Hora Inicio
                cargarDialogHoraRecordatorio();
            }
        }, anio, mes, dia);
        datePickerDialog.getDatePicker().setMinDate(calMinFechaRecordatorio.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(calMaxFechaRecordatorio.getTimeInMillis());
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Siguiente", datePickerDialog);
        datePickerDialog.getDatePicker().setCalendarViewShown(false);
        datePickerDialog.setTitle("Seleccione la fecha");
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();
    }

    public void cargarDialogHoraRecordatorio(){
        int hora,minuto;
        if (calHoraRecordatorio != null)
        {
            hora = calHoraRecordatorio.get(Calendar.HOUR_OF_DAY);
            minuto = calHoraRecordatorio.get(Calendar.MINUTE);
        }else
        {
            hora = 12;
            minuto = 0;
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), DatePickerDialog.THEME_HOLO_LIGHT,new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calHoraRecordatorio = Calendar.getInstance();
                calHoraRecordatorio.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calHoraRecordatorio.set(Calendar.MINUTE, minute);
                updateEdtHoraRecordatorio();
            }
        }, hora, minuto, false);
        timePickerDialog.setTitle("Seleccione la hora");
        timePickerDialog.setCancelable(false);
        timePickerDialog.show();
    }

    public void CargaInicial(){
        //Cargar los recordatorios de llamadas de la Cita Inicial

        //if (calendarioActivity.citaActual != null && calendarioActivity.citaActual.getListaRecordatorioLLamada() != null && calendarioActivity.citaActual.getListaRecordatorioLLamada() != null)
        //  edtNotas.setText(calendarioActivity.citaActual.getNotas());
    }


    private void updateEdtFechaRecordatorio(){

        String diaSemanaTexto= "";
        String mesTexto = "";

        int diaCita = calFechaRecordatorio.get(Calendar.DAY_OF_MONTH);
        int mesCita = calFechaRecordatorio.get(Calendar.MONTH);
        int diaSemana = calFechaRecordatorio.get(Calendar.DAY_OF_WEEK);

        // Seteando el Texto Dia
        diaSemanaTexto = Constantes.ObtenerTextoAbreviadoDiaSemana(diaSemana);
        // Seteando el Texto Mes
        mesTexto = Constantes.ObtenerTextoAbreviadoMes(mesCita);

        txtFechaRecordatorio.setText(diaSemanaTexto +", "+ diaCita + " " + mesTexto);

    }

    private void updateEdtHoraRecordatorio(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");

        //int hora = calFechaCita.get(Calendar.HOUR);
        //int minuto = calFechaCita.get(Calendar.MINUTE);

        String text_horaInicio = simpleDateFormat.format(calHoraRecordatorio.getTime());
        txtHoraRecordatorio.setText(text_horaInicio);

        //SimpleDateFormat simpleDateFormatGuardar = SimpleDateFormat simpleDateFormatGuardar = new SimpleDateFormat("HH:mm");
        //calendarioActivity.citaActual.setRecordatorioLlamada(simpleDateFormatGuardar.format(calHoraRecordatorio.getTime()));
    }

    public void CargarDatosInicial() {

        if (calendarioActivity.citaActual.getRecordatorioLlamadaBean() != null &&
            calendarioActivity.citaActual.getRecordatorioLlamadaBean().getFechaRecordatorio() != null &&
            !"".equals(calendarioActivity.citaActual.getRecordatorioLlamadaBean().getFechaRecordatorio()))
        {
            String fecha_full_recordatorio = calendarioActivity.citaActual.getRecordatorioLlamadaBean().getFechaRecordatorio();
            fechaOriginalRecordatorio = fecha_full_recordatorio;

            Date datefullRecordatorio = null;
            try
            {
                datefullRecordatorio = new SimpleDateFormat(Constantes.formato_fechafull_envio).parse(fecha_full_recordatorio);

                //// Obteniendo la Fecha /////
                Calendar calFecha = Calendar.getInstance();
                calFecha.setTime(datefullRecordatorio);

                String diaSemanaTexto= "";
                String mesTexto = "";

                int mesCita = calFecha.get(Calendar.MONTH);
                int diaCita = calFecha.get(Calendar.DAY_OF_MONTH);
                int diaSemana = calFecha.get(Calendar.DAY_OF_WEEK);

                // Seteando el Texto Dia
                diaSemanaTexto = Constantes.ObtenerTextoAbreviadoDiaSemana(diaSemana);
                // Seteando el Texto Mes
                mesTexto = Constantes.ObtenerTextoAbreviadoMes(mesCita);

                txtFechaRecordatorio.setText(diaSemanaTexto +", "+ diaCita + " " + mesTexto);
                calFechaRecordatorio = calFecha;

                //// Obteniendo la Hora ///////////////////////////
                Calendar calHora = Calendar.getInstance();
                calHora.setTime(datefullRecordatorio);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");
                String text_horaRecordatorio = simpleDateFormat.format(datefullRecordatorio);
                txtHoraRecordatorio.setText(text_horaRecordatorio);

                calHoraRecordatorio = calHora;

            }
            catch(Exception e){}
        }
    }

    public void FormatearTarjeta()
    {
        if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista) {
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_primera_entrevista));
        }else if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_SegundaEntrevista) {
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_segunda_entrevista));
        }else if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_EntrevistaAdicional){
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_entrevista_adicional));
        }
    }
}
