package com.pacifico.agenda.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;

/**
 * Created by joel on 8/15/16.
 */
public class UbicacionReunionFragment extends Fragment {
    private CalendarioActivity calendarioActivity;
    ImageButton btnRetrocederUbicacion;
    EditText edtUbicacion;
    EditText edtReferencia;

    LinearLayout linCabecera;

    public static UbicacionReunionFragment newInstance() {
        UbicacionReunionFragment fragment = new UbicacionReunionFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_ubicacion, container, false);

        linCabecera = (LinearLayout) linfragment.findViewById(R.id.linCabeceraEntrevistaUbicacion);

        btnRetrocederUbicacion = (ImageButton) linfragment.findViewById(R.id.btnRetrocederUbicacion);

        btnRetrocederUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarioActivity.reunionActual.setUbicacion(edtUbicacion.getText().toString());

                getFragmentManager().popBackStack();

                ((CalendarioActivity)getActivity()).OcultarTeclado();
            }
        });

        edtUbicacion = (EditText) linfragment.findViewById(R.id.edtUbicacion);
        edtUbicacion.requestFocus();

        edtReferencia = (EditText) linfragment.findViewById(R.id.edtReferencia);

        FormatearTarjeta();

        //CargarDatosInicial();

        // Inflate the layout for this fragment
        return linfragment;
    }
    public void CargarDatosInicial() {
        if (calendarioActivity.reunionActual != null && calendarioActivity.reunionActual.getUbicacion() != null)
            edtUbicacion.setText(calendarioActivity.reunionActual.getUbicacion());
    }
    public void FormatearTarjeta() {
        linCabecera.setBackgroundColor(getResources().getColor(R.color.color_reunion_interna));
        edtReferencia.setVisibility(View.GONE);
    }
}