package com.pacifico.agenda.Fragments;

//import android.app.Fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.agenda.Adapters.ProspectosPagerAdapter;
import com.pacifico.agenda.R;

/**
 * Created by Joel on 25/04/2016.
 */
public class ProspectosFragment_bk extends Fragment {
    ViewPager pagProspecto;

    public static ProspectosFragment_bk newInstance() {
        ProspectosFragment_bk fragment = new ProspectosFragment_bk();

        return fragment;
    }
    ProspectosPagerAdapter prospectosPagerAdapter;
    ViewPager mViewPager;

/*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_prospectos, container, false);
        TabLayout tblTabs = (TabLayout)view.findViewById(R.id.tblTabs);

        pagProspecto = (ViewPager)view.findViewById(R.id.pagProspectos);
        ProspectosPagerAdapter adapter = new ProspectosPagerAdapter(getActivity().getSupportFragmentManager());//this.getChildFragmentManager()
        adapter.addFrag(new ProspectosdelDiaFragment());
        adapter.addFrag(new ProspectosAgendadosFragment());
        adapter.addFrag(new ProspectosRecordatorioLlamadaFragment());

        pagProspecto.setAdapter(adapter);
        pagProspecto.setOffscreenPageLimit(3);
        tblTabs.setupWithViewPager(pagProspecto);

        // Creando el tab Prospectos del día
        RelativeLayout rel1 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt1 = (TextView)rel1.findViewById(R.id.txt1);
        txt1.setText("DEL DÍA");
        txt1.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));

        // Creando el tab Prospectos AGENDADOS
        RelativeLayout rel2 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt2 = (TextView)rel2.findViewById(R.id.txt1);
        txt2.setText("AGENDADOS");
        txt2.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));

        // Creando el tab Prospectos Recordatorio de llamadas
        RelativeLayout rel3 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        //TextView txt3 = (TextView)rel3.findViewById(R.id.txt1);
        //txt3.setText("AGENDADOS");
        //txt3.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));

        ImageView imgView3 = (ImageView)rel3.findViewById(R.id.img1);
        imgView3.setImageResource(R.drawable.ic_action_notification_phone_in_talk);
        //imgView3.setColorFilter(Color.rgb(170, 170, 170));

        // Se agregan los tabs customizados
        tblTabs.getTabAt(0).setCustomView(rel1);
        tblTabs.getTabAt(1).setCustomView(rel2);
        tblTabs.getTabAt(2).setCustomView(rel3);

        pagProspecto.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tblTabs));
        tblTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab){
                RelativeLayout relSelected = (RelativeLayout)tab.getCustomView();
                TextView txt1 = (TextView)relSelected.findViewById(R.id.txt1);
                txt1.setTextColor(getResources().getColor(R.color.blanco));
                txt1.setTypeface(null, Typeface.BOLD);
                pagProspecto.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab){
                RelativeLayout relUnselected = (RelativeLayout)tab.getCustomView();
                TextView txt1 = (TextView)relUnselected.findViewById(R.id.txt1);
                txt1.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));
                txt1.setTypeface(null, Typeface.NORMAL);
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab){}
        });

        return view;
    }
*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_prospectos, container, false);
        TabLayout tblTabs = (TabLayout)view.findViewById(R.id.tblTabs);


        // Creando el tab Prospectos del día
        RelativeLayout rel1 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt1 = (TextView)rel1.findViewById(R.id.txt1);
        txt1.setText("DEL DÍA");
        txt1.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));

        // Creando el tab Prospectos AGENDADOS
        RelativeLayout rel2 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt2 = (TextView)rel2.findViewById(R.id.txt1);
        txt2.setText("AGENDADOS");
        txt2.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));

        // Creando el tab Prospectos Recordatorio de llamadas
        RelativeLayout rel3 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        //TextView txt3 = (TextView)rel3.findViewById(R.id.txt1);
        //txt3.setText("AGENDADOS");
        //txt3.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));

        ImageView imgView3 = (ImageView)rel3.findViewById(R.id.img1);
        imgView3.setImageResource(R.drawable.ic_action_notification_phone_in_talk);
        tblTabs.addTab(tblTabs.newTab().setCustomView(rel1));
        tblTabs.addTab(tblTabs.newTab().setCustomView(rel2));
        tblTabs.addTab(tblTabs.newTab().setCustomView(rel3));

        pagProspecto = (ViewPager)view.findViewById(R.id.pagProspectos);
        final PagerAdapterProspectos adapter = new PagerAdapterProspectos(getChildFragmentManager());
        pagProspecto.setAdapter(adapter);
        pagProspecto.setOffscreenPageLimit(3);
        pagProspecto.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tblTabs));


        tblTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab){
                RelativeLayout relSelected = (RelativeLayout)tab.getCustomView();
                TextView txt1 = (TextView)relSelected.findViewById(R.id.txt1);
                txt1.setTextColor(getResources().getColor(R.color.blanco));
                txt1.setTypeface(null, Typeface.BOLD);
                pagProspecto.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab){
                RelativeLayout relUnselected = (RelativeLayout)tab.getCustomView();
                TextView txt1 = (TextView)relUnselected.findViewById(R.id.txt1);
                txt1.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));
                txt1.setTypeface(null, Typeface.NORMAL);
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab){}
        });

        return view;
    }


    @Override
    public void onResume(){
        Log.i("TAG", "onResume");
        super.onResume();
    }



    private class PagerAdapterProspectos extends FragmentPagerAdapter {

        public PagerAdapterProspectos(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0: return new ProspectosdelDiaFragment();
                case 1: return new ProspectosAgendadosFragment();
                case 2: return new ProspectosRecordatorioLlamadaFragment();
                default: return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
