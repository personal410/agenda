package com.pacifico.agenda.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Activity.InicioSesionActivity;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Model.Bean.ReminderBean;
import com.pacifico.agenda.Model.Bean.TablaTablasBean;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.agenda.Model.Controller.TablasGeneralesController;
import com.pacifico.agenda.Network.RespuestaSincronizacionListener;
import com.pacifico.agenda.Network.SincronizacionController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Reminder.ReminderManager;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;
import com.pacifico.agenda.Views.CustomView.setFuente;
import com.pacifico.agenda.Views.Dialogs.DialogGeneral;
import com.pacifico.agenda.Views.Dialogs.DialogOpcionesTablaTablas;
import com.pacifico.agenda.Views.Dialogs.IDialogSiNoValor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.pacifico.agenda.Util.Util.capitalizedString;

/**
 * Created by joel on 8/3/16.
 */
public class RecordatorioIndependienteFragment extends Fragment implements IDialogSiNoValor {

    boolean flagClick = false;
    private CalendarioActivity calendarioActivity;

    private ProgressDialog progressDialog;

    LinearLayout linCabeceraRecInd;
    LinearLayout linSubCabeceraRecInd;
    TextView txtEtapaRecInd;
    TextView txtRecordatorioLLamadaRecInd;
    TextView txtNombreProspectoRecInd;
    TextView txtTelefonoRecInd;
    TextView txtHijosRecInd;
    TextView txtHijosTextoRecInd;
    TextView txtRangoIngresosRecInd;
    TextView txtReferenteTextoRecInd;
    TextView txtReferenteContenidoRecInd;
    TextView txtNotasRecInd;
    TextView txtRangoEdadRecInd;
    TextView txtEdadTextoRecInd;
    TextView txtIngresosTextoRecInd;
    Button btnGuardarRecInd;
    TextView btnNuevaEntrevistaRecInd;
    TextView btnDescartarRecInd;
    ImageButton btnCerrarRecInd;

    int tipoContinuarSincro = -1;

    public static RecordatorioIndependienteFragment newInstance() {
        RecordatorioIndependienteFragment fragment = new RecordatorioIndependienteFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_recordatorio_independiente, container, false);

        linCabeceraRecInd =(LinearLayout) linfragment.findViewById(R.id.linCabeceraRecInd);
        linSubCabeceraRecInd =(LinearLayout) linfragment.findViewById(R.id.linSubCabeceraRecInd);
        txtEtapaRecInd = (TextView) linfragment.findViewById(R.id.txtEtapaRecInd);
        txtRangoIngresosRecInd = (TextView) linfragment.findViewById(R.id.txtRangoIngresosRecInd);
        txtRecordatorioLLamadaRecInd = (TextView) linfragment.findViewById(R.id.txtRecordatorioLLamadaRecInd);
        txtNotasRecInd = (TextView) linfragment.findViewById(R.id.txtNotasRecInd);
        btnGuardarRecInd = (Button) linfragment.findViewById(R.id.btnGuardarRecInd);
        txtNombreProspectoRecInd = (TextView) linfragment.findViewById(R.id.txtNombreProspectoRecInd);
        txtTelefonoRecInd = (TextView) linfragment.findViewById(R.id.txtTelefonoRecInd);
        txtHijosRecInd = (TextView) linfragment.findViewById(R.id.txtHijosRecInd);
        txtHijosTextoRecInd = (TextView) linfragment.findViewById(R.id.txtHijosTextoRecInd);
        txtReferenteTextoRecInd = (TextView) linfragment.findViewById(R.id.txtReferenteTextoRecInd);
        txtReferenteContenidoRecInd = (TextView) linfragment.findViewById(R.id.txtReferenteContenidoRecInd);
        txtRangoEdadRecInd = (TextView) linfragment.findViewById(R.id.txtRangoEdadRecInd);
        txtEdadTextoRecInd = (TextView) linfragment.findViewById(R.id.txtEdadTextoRecInd);
        txtIngresosTextoRecInd = (TextView) linfragment.findViewById(R.id.txtIngresosTextoRecInd);
        btnNuevaEntrevistaRecInd = (TextView) linfragment.findViewById(R.id.btnNuevaEntrevistaRecInd);
        btnDescartarRecInd = (TextView) linfragment.findViewById(R.id.btnDescartarRecInd);
        btnCerrarRecInd = (ImageButton) linfragment.findViewById(R.id.btnCerrarRecInd);

        btnCerrarRecInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flagClick) return;

                flagClick = true;
                reiniciarValoresyCerrarTarjeta();
                flagClick = false;
            }
        });

        txtRecordatorioLLamadaRecInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fraFormularioFlotante, RecordatorioLLamadaRecIndFragment.newInstance()); // newInstance() is a static factory method.
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        txtNotasRecInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Se esta llamando Notas
                transaction.replace(R.id.fraFormularioFlotante, NotasFragment.newInstance()); // newInstance() is a static factory method.
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        btnGuardarRecInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flagClick) return;
                flagClick = true;

                tipoContinuarSincro = R.id.btnGuardarRecInd;
                if(guardarRecordatorioyProspecto()){
                    sincronizarReiniciarValores();
                }else{
                    flagClick = false;
                }
            }
        });

        btnNuevaEntrevistaRecInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flagClick) return;
                flagClick = true;

                tipoContinuarSincro = R.id.btnNuevaEntrevistaRecInd;
                // Se guarda el recordatorio
                if(guardarRecordatorioyProspecto()){
                    sincronizarReiniciarValores();
                }else
                {
                    cargarTarjetaEntrevista();
                    flagClick = false;
                }
                //cargarTarjetaEntrevista();

            }
        });

        btnDescartarRecInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flagClick) return;
                flagClick = true;

                if (calendarioActivity.prospectoActual.getCodigoEstado() != Constantes.EstadoProspecto_NoInteresado) {
                    dialogConfirmacionDescartado();
                }
            }
        });

        //Seteando ingresos del prospecto

        txtRangoIngresosRecInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getChildFragmentManager();
                DialogOpcionesTablaTablas dialogIngresos = new DialogOpcionesTablaTablas();
                //dialogIngresos.AgregarOpciones(getActivity());
                dialogIngresos.setListener(RecordatorioIndependienteFragment.this);

                // Lista de Ingresos
                ArrayList<TablaTablasBean> listaItems =  TablasGeneralesController.obtenerTablaTablasPorIdTabla(Constantes.RangoIngresos);

                //dialogIngresos.setDatos("Ingresos","Seleccionar","Cancelar",listaItems,rangoIngresos);
                dialogIngresos.setDatos("Ingresos","Seleccionar","Cancelar",listaItems,calendarioActivity.prospectoActual.getCodigoRangoIngreso());
                dialogIngresos.setCancelable(false);
                dialogIngresos.show(fm, "dialogo1");
            }
        });

        if(calendarioActivity.prospectoActual.getCodigoEstado() == Constantes.EstadoProspecto_NoInteresado) {
            FormatoTarjetaDescartado();
        }else
        {
            FormatoTarjetaNuevo();
        }

        CargarDatosInicial();

        // Inflate the layout for this fragment
        return linfragment;
    }

    private void continuar() {

        if (tipoContinuarSincro == R.id.btnNuevaEntrevistaRecInd){
            cargarTarjetaEntrevista();
        } else{
            reiniciarValoresyCerrarTarjeta();
        }

        tipoContinuarSincro = -1;
        flagClick = false;

    }

    private void mostrarMensajeParaContinuar(String mensaje){ // Funciona para guardar y descartar
        new AlertDialog.Builder(getContext())
                .setTitle("Alerta")
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                continuar();
            }
        }).setCancelable(false).show();
    }

    public void CargarDatosInicial() {

        if (calendarioActivity.prospectoActual.getCodigoEstado() == Constantes.EstadoProspecto_NoInteresado) {
            btnDescartarRecInd.setVisibility(View.INVISIBLE);
            btnDescartarRecInd.setClickable(false);
        }

        // Cargando Nombres
        String nombreCompleto = "";
        if (calendarioActivity.prospectoActual.getNombres() != null)
            nombreCompleto = nombreCompleto + capitalizedString(calendarioActivity.prospectoActual.getNombres()) ;
        if (calendarioActivity.prospectoActual.getApellidoPaterno() != null)
            nombreCompleto = nombreCompleto  + " " +  capitalizedString(calendarioActivity.prospectoActual.getApellidoPaterno());
        if (calendarioActivity.prospectoActual.getApellidoMaterno() != null)
            nombreCompleto = nombreCompleto  + " " +  capitalizedString(calendarioActivity.prospectoActual.getApellidoMaterno());

        txtNombreProspectoRecInd.setText(nombreCompleto);
        txtTelefonoRecInd.setText(calendarioActivity.prospectoActual.getTelefonoCelular());

        // Obtener Referenciador:
        int idReferenteDispositivo = calendarioActivity.prospectoActual.getIdReferenciadorDispositivo();
        ProspectoBean referente = null;

        if (idReferenteDispositivo != -1)
            referente =  ProspectoController.getProspectoBeanByIdProspectoDispositivo(idReferenteDispositivo);

        if (referente != null) {
            String nombreReferenteCompleto = "";
            if (referente.getNombres() != null)
                nombreReferenteCompleto = nombreReferenteCompleto + capitalizedString(referente.getNombres());
            if (referente.getApellidoPaterno() != null)
                nombreReferenteCompleto = nombreReferenteCompleto +" " + capitalizedString(referente.getApellidoPaterno());
            if (referente.getApellidoMaterno() != null)
                nombreReferenteCompleto = nombreReferenteCompleto + " " + capitalizedString(referente.getApellidoMaterno());

            txtReferenteContenidoRecInd.setText(nombreReferenteCompleto);
        }
        else {
            TablaTablasBean fuente = TablasGeneralesController.ObtenerItemTablaTablas(7,calendarioActivity.prospectoActual.getCodigoFuente());
            if (fuente != null)
                txtReferenteContenidoRecInd.setText(fuente.getValorCadena());
            txtReferenteTextoRecInd.setText("");
        }

        // Cargando Rango de Edad
        int codRangoEdad = calendarioActivity.prospectoActual.getCodigoRangoEdad();
        TablaTablasBean rangoEdadBean = TablasGeneralesController.ObtenerItemTablaTablas(Constantes.RangoEdad,codRangoEdad);
        if (rangoEdadBean != null)
            txtRangoEdadRecInd.setText(rangoEdadBean.getValorCadena());
        else
            txtRangoEdadRecInd.setText("");

        // Cargando Flag de Hijos
        if (calendarioActivity.prospectoActual.getFlagHijo()== Constantes.FLAG_VERDADERO)
            txtHijosRecInd.setText("Sí");
        else if (calendarioActivity.prospectoActual.getFlagHijo()== Constantes.FLAG_FALSO)
            txtHijosRecInd.setText("No");
        else
            txtHijosRecInd.setText("");

        // Ingresos
        int codRangoIngresos = calendarioActivity.prospectoActual.getCodigoRangoIngreso();
        TablaTablasBean rangoIngresosBean = TablasGeneralesController.ObtenerItemTablaTablas(Constantes.RangoIngresos,codRangoIngresos);
        if (rangoIngresosBean != null)
            txtRangoIngresosRecInd.setText(rangoIngresosBean.getValorCadena());
        else
            txtRangoIngresosRecInd.setText("");

        // Recordatorio de llamada
        if (calendarioActivity.recordatorioLLamadaDirectoBean != null &&
                calendarioActivity.recordatorioLLamadaDirectoBean.getFechaRecordatorio() != null &&
                !"".equals(calendarioActivity.recordatorioLLamadaDirectoBean.getFechaRecordatorio()))
        {
            Date dateRecordatorio = null;
            try
            {
                dateRecordatorio = new SimpleDateFormat(Constantes.formato_fechafull_envio).parse(calendarioActivity.recordatorioLLamadaDirectoBean.getFechaRecordatorio());
                Calendar cal = Calendar.getInstance();
                cal.setTime(dateRecordatorio);

                String diaSemanaTexto= "";
                String mesTexto = "";

                int mesCita = cal.get(Calendar.MONTH);
                int diaCita = cal.get(Calendar.DAY_OF_MONTH);
                int diaSemana = cal.get(Calendar.DAY_OF_WEEK);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");
                String text_horaRecordatorio = simpleDateFormat.format(dateRecordatorio);

                // Seteando el Texto Dia
                diaSemanaTexto = Constantes.ObtenerTextoAbreviadoDiaSemana(diaSemana);
                // Seteando el Texto Mes
                mesTexto = Constantes.ObtenerTextoAbreviadoMes(mesCita);

                txtRecordatorioLLamadaRecInd.setText(diaSemanaTexto +", "+ diaCita + " " + mesTexto +", "+text_horaRecordatorio);
            }
            catch(Exception e){}
        }

        if (calendarioActivity.prospectoActual.getNota() != null)
            txtNotasRecInd.setText(calendarioActivity.prospectoActual.getNota());

    }

    public void actualizarProspecto(){
            calendarioActivity.prospectoActual.setFechaModificacionDispositivo(Util.obtenerFechaActual());
            calendarioActivity.prospectoActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
            ProspectoController.actualizarProspectoxIDDispositivo(calendarioActivity.prospectoActual);
    }

    @Override
    public void respuesta_si(TablaTablasBean valor) {
        if (valor.getIdTabla() == Constantes.RangoIngresos) {
            //rangoIngresos = valor.getCodigoCampo();
            if (calendarioActivity.prospectoActual.getCodigoRangoIngreso() != valor.getCodigoCampo())
            {
                calendarioActivity.prospectoActual.setCodigoRangoIngreso(valor.getCodigoCampo());
                txtRangoIngresosRecInd.setText(valor.getValorCadena());

                calendarioActivity.citaModificoProspecto = true;
            }
        }
    }

    @Override
    public void respuesta_no() {

    }

    public boolean guardarRecordatorioyProspecto() {
        boolean actualizo = false;
        if (calendarioActivity.recordatorioLLamadaDirectoBean != null  && calendarioActivity.recordatorioLLamadaDirectoBean.getIdRecordatorioLlamadaDispositivo() == -1) {
            actualizo = true;
            calendarioActivity.recordatorioLLamadaDirectoBean.setIdProspecto(calendarioActivity.prospectoActual.getIdProspecto());
            calendarioActivity.recordatorioLLamadaDirectoBean.setIdProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());
            calendarioActivity.recordatorioLLamadaDirectoBean.setFlagEnviado(0);
            calendarioActivity.recordatorioLLamadaDirectoBean.setFlagActivo(1);

            RecordatorioLlamadaBean rectemp = CitaReunionController.obtenerRecordatorioLlamadaDirectoPorIdProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());
            if (rectemp != null) { // SI HAY RECORDATORIO
                // Borrar el recordatorio de la programacion
                ReminderManager rm = new ReminderManager(getActivity());
                String idReminder = rm.obtenerIDReminderPorIDFuente(Constantes.alerta_recordatorio, rectemp.getIdRecordatorioLlamadaDispositivo());
                if (idReminder != null)
                    rm.deleteReminder(Integer.parseInt(idReminder));

                // Desactivar el recordatorio de llamada
                CitaReunionController.desactivarRecordatoriosIndependientesProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());
            }

            CitaReunionController.guardarRecordatorioLLamada_GenerarIDDispositivo(calendarioActivity.recordatorioLLamadaDirectoBean);

            // Agregar el recordatorio ////////
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date d = null;
            try {
                d = f.parse(calendarioActivity.recordatorioLLamadaDirectoBean.getFechaRecordatorio());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (d != null) {
                long milliseconds = d.getTime();
                ReminderManager reminderManager = new ReminderManager(getActivity());
                ReminderBean reminderBean = new ReminderBean();
                reminderBean.setTitle("Tienes un recordatorio de llamada.");
                String nombreCompletoProspecto= Util.capitalizedString(calendarioActivity.prospectoActual.getNombres()) + " " +
                                                Util.capitalizedString(calendarioActivity.prospectoActual.getApellidoPaterno()) + " "+
                                                Util.capitalizedString(calendarioActivity.prospectoActual.getApellidoMaterno()).trim();

                String texto = "No olvides llamar a : "+ nombreCompletoProspecto;
                reminderBean.setBody(nombreCompletoProspecto);
                reminderBean.setReminderDateTime(String.valueOf(milliseconds));
                reminderBean.setFuenteAlerta(Constantes.alerta_recordatorio);
                reminderBean.setIdDispositivoFuente(calendarioActivity.recordatorioLLamadaDirectoBean.getIdRecordatorioLlamadaDispositivo()); // Se seteo al grabar en BD
                int id = reminderManager.addReminder(reminderBean);
            }
            ////// Fin de agregar Recordatorio
        }

        if (calendarioActivity.citaModificoProspecto){
            actualizarProspecto();
            actualizo = true;
        }
        return actualizo;
    }

    public void FormatoTarjetaNuevo(){
        linCabeceraRecInd.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        linSubCabeceraRecInd.setBackgroundColor(getResources().getColor(R.color.subcabecera_nuevo));
        txtEtapaRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_nuevo));
        txtReferenteTextoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_nuevo));
        txtReferenteContenidoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_nuevo));
        txtEdadTextoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_nuevo));
        txtHijosTextoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_nuevo));
        txtIngresosTextoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_nuevo));

        txtRecordatorioLLamadaRecInd.setText("");
    }

    public void FormatoTarjetaDescartado(){
        linCabeceraRecInd.setBackgroundColor(getResources().getColor(R.color.color_descartado));
        linSubCabeceraRecInd.setBackgroundColor(getResources().getColor(R.color.subcabecera_descartado));
        txtEtapaRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_descartado));
        txtReferenteTextoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_descartado));
        txtReferenteContenidoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_descartado));
        txtEdadTextoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_descartado));
        txtHijosTextoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_descartado));
        txtIngresosTextoRecInd.setTextColor(getResources().getColor(R.color.cabecera_texto_descartado));

        txtRecordatorioLLamadaRecInd.setText("");
    }

    /*public void CambiarEtapaporNuevaEntrevista()
    {
        String fechaActual = Util.obtenerFechaActual();
        // Actualizamos el prospecto
        calendarioActivity.prospectoActual.setCodigoEtapa(Constantes.EtapaProspecto_PrimeraEntrevista);
        calendarioActivity.prospectoActual.setCodigoEstado(Constantes.EstadoProspecto_EnProceso);
        calendarioActivity.prospectoActual.setFechaModificacionDispositivo(fechaActual);
        calendarioActivity.prospectoActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

        // Se modifica el prospecto y el movimiento de etapa
        ProspectoMovimientoEtapaController.guardarProspectoMovimientoEtapa_GenerarIDDispositivo(GenerarProspectoMovimientoEtapa(calendarioActivity.prospectoActual,fechaActual));
        ProspectoController.actualizarProspectoxIDDispositivo(calendarioActivity.prospectoActual);

        // Reiniciamos los citas actuales //////////////////////////
        calendarioActivity.citaOriginal = null; // Por que se acaba de cambiar de etapa

        // Cita Actual
        calendarioActivity.citaActual = new CitaBean();
        calendarioActivity.citaActual.setIdProspecto(calendarioActivity.prospectoActual.getIdProspecto());
        calendarioActivity.citaActual.setIdProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());
        calendarioActivity.citaActual.setCodigoEtapaProspecto(calendarioActivity.prospectoActual.getCodigoEtapa());

        // Se llama al fragment de entrevista
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment _entrevistaFragment = EntrevistaFragment.newInstance();

        transaction.replace(R.id.fraFormularioFlotante, _entrevistaFragment); // newInstance() is a static factory method.
        transaction.commit();
    }*/

    public void cargarTarjetaEntrevista()
    {
        // Reiniciamos los citas actuales //////////////////////////
        calendarioActivity.citaOriginal = null; // Por que se acaba de cambiar de etapa

        // Cita Actual
        calendarioActivity.citaActual = new CitaBean();
        calendarioActivity.citaActual.setIdProspecto(calendarioActivity.prospectoActual.getIdProspecto());
        calendarioActivity.citaActual.setIdProspectoDispositivo(calendarioActivity.prospectoActual.getIdProspectoDispositivo());
        calendarioActivity.citaActual.setCodigoEtapaProspecto(Constantes.EtapaProspecto_PrimeraEntrevista);

        // Se llama al fragment de entrevista
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment _entrevistaFragment = EntrevistaFragment.newInstance();

        transaction.replace(R.id.fraFormularioFlotante, _entrevistaFragment); // newInstance() is a static factory method.
        transaction.commit();
    }

    public ProspectoMovimientoEtapaBean GenerarProspectoMovimientoEtapa(ProspectoBean prospectoBean, String fechaMovimiento)
    {
        ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean = new ProspectoMovimientoEtapaBean();

        prospectoMovimientoEtapaBean.setIdProspecto(prospectoBean.getIdProspecto());
        prospectoMovimientoEtapaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
        prospectoMovimientoEtapaBean.setCodigoEstado(prospectoBean.getCodigoEstado());
        prospectoMovimientoEtapaBean.setCodigoEtapa(prospectoBean.getCodigoEtapa());
        prospectoMovimientoEtapaBean.setFechaMovimientoEtapaDispositivo(fechaMovimiento);
        prospectoMovimientoEtapaBean.setFlagEnviado(Constantes.ENVIO_PENDIENTE);

        return prospectoMovimientoEtapaBean;
    }

    public void reiniciarValoresyCerrarTarjeta()
    {
        ////////// Reseteo de valores
        calendarioActivity.reiniciarParametros();
        calendarioActivity.CerrarTarjetas();

        // TODO: Hay que revisar si la cita esta en la semana actual del calendario, de no ser así no es necesario recargar
        calendarioActivity.actualizarCalendario();

        ///////
    }

    public void mensajeGuardadoGenerico (){
        // Mensaje Generico de Cita Guardada
        FragmentManager fm = getChildFragmentManager();
        DialogGeneral dialogGeneral = new DialogGeneral();
        dialogGeneral.setListener(null);
        dialogGeneral.setDatos(getString(R.string.recordatorio_agendado_exito), "", getString(R.string.continuar));
        dialogGeneral.setCancelable(false);
        dialogGeneral.show(fm, "dialogo1");
    }

    public void mensajeDescartadoExito (){
        // Mensaje Generico de Cita Guardada
        FragmentManager fm = getChildFragmentManager();
        DialogGeneral dialogGeneral = new DialogGeneral();
        dialogGeneral.setListener(null);
        dialogGeneral.setDatos(getString(R.string.prospecto_descartado_exito), "", getString(R.string.continuar));
        dialogGeneral.setCancelable(false);
        dialogGeneral.show(fm, "dialogo1");
    }


    ////
    // Llamar al envío de datos #ENVIO
    public void sincronizarReiniciarValores() {
        SincronizacionController sincronizacionController = new SincronizacionController();
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Enviando información");
            progressDialog.setMessage("Espere, por favor");
        }
        progressDialog.setCancelable(false);
        progressDialog.show();
        sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
            @Override
            public void terminoSincronizacion(int codigo, String mensaje) {
                progressDialog.dismiss();
                if (codigo < 0) {
                    mostrarMensajeParaContinuar(mensaje);
                } else if (codigo == 1) {
                      mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
                    //continuar();
                } else {
                    if (codigo == 4 || codigo == 6) {
                        Intent inicioSesionIntent = new Intent(getActivity(), InicioSesionActivity.class);
                        inicioSesionIntent.putExtra("etapa", (codigo - 4) / 2 + 1);
                        startActivity(inicioSesionIntent);
                    } else {
                        mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                    }
                }
            }
        });
        int resultado = sincronizacionController.sincronizar(getContext());
        if (resultado == 0) {
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
            //continuar();
        } else if (resultado == 2) {
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
        }
    }
    /////

    public void dialogConfirmacionDescartado()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this.getActivity());
        final View dialogConfirmacionDescartado = layoutInflater.inflate(R.layout.dialog_general, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        TextView tvTitulo = (TextView) dialogConfirmacionDescartado.findViewById(R.id.tvTitulo);
        tvTitulo.setText("Seguro que desea descartar el prospecto?");

        builder.setView(dialogConfirmacionDescartado).setCancelable(false).setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                descartarProspecto();
                flagClick = false;
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                flagClick = false;
            }
        });

        AlertDialog  alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Button btnCancelar = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button btnAceptar = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        btnAceptar.setTextColor(getResources().getColor(R.color.colorCeleste));
        btnCancelar.setTextColor(getResources().getColor(R.color.colorMarron));
        setFuente.setButton(getActivity(),btnAceptar);
        setFuente.setButton(getActivity(),btnCancelar);
    }

    public void descartarProspecto(){
        tipoContinuarSincro = R.id.btnDescartarRecInd;

        String fechaActual = Util.obtenerFechaActual();

        // Se actualiza el prospecto
        calendarioActivity.prospectoActual.setCodigoEstado(Constantes.EstadoProspecto_NoInteresado);
        calendarioActivity.prospectoActual.setFechaModificacionDispositivo(fechaActual);
        calendarioActivity.prospectoActual.setFlagEnviado(Constantes.ENVIO_PENDIENTE);
        ProspectoController.actualizarProspectoxIDDispositivo(calendarioActivity.prospectoActual);
        // Se genera el prospecto Movimiento Etapa
        ProspectoMovimientoEtapaController.guardarProspectoMovimientoEtapa_GenerarIDDispositivo(GenerarProspectoMovimientoEtapa(calendarioActivity.prospectoActual, fechaActual));

        // Llamar al envío de datos #ENVIO
        sincronizarReiniciarValores();
    }


}
