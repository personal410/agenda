package com.pacifico.agenda.Fragments.Ajustes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pacifico.agenda.Model.Bean.IntermediarioBean;
import com.pacifico.agenda.Model.Controller.IntermediarioController;
import com.pacifico.agenda.R;

import static com.pacifico.agenda.Util.Util.capitalizedString;
import static com.pacifico.agenda.Util.Util.strNULL;


public class IntermediarioDatosPersonalesFragment extends Fragment {
    TextView Nombres,codigoAsesor,DocIdentidad,Gu,Ga,Agencia,correoElectronico,telfCelu,telfFijo,FechaPromo,catAsesor;
    String Fecha;
    IntermediarioBean intermediarioBean = IntermediarioController.obtenerIntermediario();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

View view = inflater.inflate(R.layout.fragment_intermediario_datos_personales, container, false);


                Nombres = (TextView)view.findViewById(R.id.Nombres);
                codigoAsesor = (TextView)view.findViewById(R.id.CodAsesor);
                DocIdentidad = (TextView)view.findViewById(R.id.DocIden);
                Gu = (TextView)view.findViewById(R.id.Gu);
                Ga = (TextView)view.findViewById(R.id.Ga);
                Agencia = (TextView)view.findViewById(R.id.Agencia);
                correoElectronico = (TextView)view.findViewById(R.id.Email);
                telfCelu = (TextView)view.findViewById(R.id.tefCel);
                telfFijo = (TextView)view.findViewById(R.id.tefFijo);
                FechaPromo = (TextView)view.findViewById(R.id.fechProm);
                catAsesor = (TextView)view.findViewById(R.id.CatAsesor);

        Log.i("Tag","nuevos"+intermediarioBean.getLogin());
        Fecha();

        Nombres.setText(capitalizedString(intermediarioBean.getNombreRazonSocial()));
        codigoAsesor.setText(strNULL(intermediarioBean.getCodigoIntermediario()));
        DocIdentidad.setText(strNULL(intermediarioBean.getDocumentoIdentidad()));
        Gu.setText(capitalizedString(strNULL(intermediarioBean.getNombreGU())));
        Ga.setText(capitalizedString(strNULL(intermediarioBean.getNombreGA())));
        Agencia.setText(strNULL(intermediarioBean.getDescripcionAgencia()));
        correoElectronico.setText(strNULL(intermediarioBean.getCorreoElectronico()));
        telfCelu.setText(strNULL(intermediarioBean.getTelefonoCelular()));
        telfFijo.setText(strNULL(intermediarioBean.getTelefonoFijo()));
        FechaPromo.setText(Fecha);
        catAsesor.setText(strNULL(intermediarioBean.getDescripcionCategoria()));
        return view;
    }

    public void Fecha(){
        if(intermediarioBean.getFechaPromocion()!=null) {
            String hora = intermediarioBean.getFechaPromocion();
            String Hora[] = hora.split("-");
            Fecha = Hora[2]+"/"+Hora[1]+"/"+Hora[0];
        }
    }

}