package com.pacifico.agenda.Fragments.Ajustes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.pacifico.agenda.Model.Bean.AjustesBean;
import com.pacifico.agenda.Model.Controller.AjustesController;
import com.pacifico.agenda.R;

public class AlertasNotificacionesFragment extends Fragment implements CompoundButton.OnCheckedChangeListener{
    private int intervalorTiempoSeleccionado;
    private AjustesBean ajustesBean;
    private RadioButton rb10min, rb30min, rb60min, rb90min, rb120min, rbNmin, rbActual;
    private EditText edtTiempoPersonalizado;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        ajustesBean = AjustesController.obtenerAjuste();
        View view = inflater.inflate(R.layout.fragment_alertas_notificaciones, container, false);
        rb10min = (RadioButton)view.findViewById(R.id.rb10min);
        rb30min = (RadioButton)view.findViewById(R.id.rb30min);
        rb60min = (RadioButton)view.findViewById(R.id.rb60min);
        rb90min = (RadioButton)view.findViewById(R.id.rb90min);
        rb120min = (RadioButton)view.findViewById(R.id.rb120min);
        rbNmin = (RadioButton)view.findViewById(R.id.rbNmin);
        edtTiempoPersonalizado = (EditText)view.findViewById(R.id.edtTiempoPersonalizado);
        Spinner spiIntervaloTiempo = (Spinner)view.findViewById(R.id.spiIntervaloTiempo);
        rb10min.setOnCheckedChangeListener(this);
        rb30min.setOnCheckedChangeListener(this);
        rb60min.setOnCheckedChangeListener(this);
        rb90min.setOnCheckedChangeListener(this);
        rb120min.setOnCheckedChangeListener(this);
        rbNmin.setOnCheckedChangeListener(this);
        edtTiempoPersonalizado.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rbNmin.setChecked(true);
                } else {
                    int tiempoPersonalizado = Integer.parseInt(edtTiempoPersonalizado.getText().toString());
                    if (tiempoPersonalizado == 0) {
                        //edtTiempoPersonalizado.setText(tiempoPersonalizado);
                        tiempoPersonalizado = 1;
                        edtTiempoPersonalizado.setText(Integer.toString(tiempoPersonalizado));
                    } else {
                        if (intervalorTiempoSeleccionado == 1) {
                            if (tiempoPersonalizado > 24) {
                                tiempoPersonalizado = 24;
                                edtTiempoPersonalizado.setText(Integer.toString(tiempoPersonalizado));
                            }
                            tiempoPersonalizado = tiempoPersonalizado * 60;
                        }
                    }
                    ajustesBean.setValor1(Integer.toString(tiempoPersonalizado));
                    AjustesController.actualizarAjuste(ajustesBean);
                }
            }
        });
        ArrayAdapter<String> arrAdapCodTelFijo = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, new String[]{"minutos antes", "horas antes"});
        arrAdapCodTelFijo.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spiIntervaloTiempo.setAdapter(arrAdapCodTelFijo);
        spiIntervaloTiempo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id){
                intervalorTiempoSeleccionado = position;
                if(rbNmin.isChecked()){
                    int tiempoPersonalizado = Integer.parseInt(edtTiempoPersonalizado.getText().toString());
                    if(intervalorTiempoSeleccionado == 1){
                        if(tiempoPersonalizado > 24){
                            tiempoPersonalizado = 24;
                            edtTiempoPersonalizado.setText(Integer.toString(tiempoPersonalizado));
                        }
                        ajustesBean.setValor1(Integer.toString(tiempoPersonalizado * 60));
                    }else{
                        ajustesBean.setValor1(Integer.toString(tiempoPersonalizado));
                    }
                    AjustesController.actualizarAjuste(ajustesBean);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent){}
        });
        if(ajustesBean.getValor1().equals("10")){
            rb10min.setChecked(true);
            rbActual = rb10min;
        }else if(ajustesBean.getValor1().equals("30")){
            rb30min.setChecked(true);
            rbActual = rb30min;
        }else if(ajustesBean.getValor1().equals("60")){
            rb60min.setChecked(true);
            rbActual = rb60min;
        }else if(ajustesBean.getValor1().equals("90")){
            rb90min.setChecked(true);
            rbActual = rb90min;
        }else if(ajustesBean.getValor1().equals("120")){
            rb120min.setChecked(true);
        }else{
            int tiempo = Integer.parseInt(ajustesBean.getValor1());
            if(tiempo < 100){
                edtTiempoPersonalizado.setText(ajustesBean.getValor1());
            }else{
                edtTiempoPersonalizado.setText(Integer.toString(tiempo / 60));
                spiIntervaloTiempo.setSelection(1);
            }
            rbNmin.setChecked(true);
        }
        return view;
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
        if(isChecked){
            if(rbActual != null){
                rbActual.setChecked(false);
            }
            rbActual = (RadioButton)buttonView;
            String nuevoValor;
            if(rbActual.equals(rb10min)){
                nuevoValor = "10";
            }else if(rbActual.equals(rb30min)){
                nuevoValor = "30";
            }else if(rbActual.equals(rb60min)){
                nuevoValor = "60";
            }else if(rbActual.equals(rb90min)){
                nuevoValor = "90";
            }else if(rbActual.equals(rb120min)){
                nuevoValor = "120";
            }else{
                nuevoValor = edtTiempoPersonalizado.getText().toString();
            }
            ajustesBean.setValor1(nuevoValor);
            AjustesController.actualizarAjuste(ajustesBean);
        }
    }
}