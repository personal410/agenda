package com.pacifico.agenda.Fragments;

//import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Adapters.ProspectosPagerAdapter;
import com.pacifico.agenda.Views.CustomView.setFuente;

/**
 * Created by Joel on 25/04/2016.
 */
public class ProspectosFragment extends Fragment {

    private CalendarioActivity calendarioActivity;
    public static ProspectosFragment newInstance() {
        ProspectosFragment fragment = new ProspectosFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        final View view = inflater.inflate(R.layout.fragment_prospectos, container, false);
        TabLayout tblTabs = (TabLayout)view.findViewById(R.id.tblTabs);
        ImageView imgCerrar =(ImageView)view.findViewById(R.id.Retroceder);
        ImageButton btnFiltroReferente = (ImageButton) view.findViewById(R.id.btnFiltroReferente);
        ImageButton btnBuscarProspectos = (ImageButton) view.findViewById(R.id.btnBuscarProspectos);

        btnBuscarProspectos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fraFormularioFlotante,new FiltroProspectosFragment()).addToBackStack(null).commit();
            }
        });

        btnFiltroReferente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fraFormularioFlotante,new FiltroReferenteFragment()).addToBackStack(null).commit();
            }
        });

        imgCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendarioActivity.CerrarTarjetas();
            }
        });


        // Creando el tab Prospectos del día
        RelativeLayout rel1 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt1 = (TextView)rel1.findViewById(R.id.txt1);
        txt1.setText("DEL DÍA");
        txt1.setTextSize(15);
        txt1.setTextColor(getResources().getColor(R.color.blanco));
        setFuente.setFuenteRg(getContext(),txt1);

        // Creando el tab Prospectos AGENDADOS
        RelativeLayout rel2 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt2 = (TextView)rel2.findViewById(R.id.txt1);
        txt2.setText("AGENDADOS");
        txt2.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));
        txt2.setTextSize(15);
        setFuente.setFuenteRg(getContext(),txt2);


        // Creando el tab Prospectos Recordatorio de llamadas
        RelativeLayout rel3 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        ImageView imgView3 = (ImageView)rel3.findViewById(R.id.img1);
        imgView3.setImageResource(R.drawable.ic_action_notification_phone_in_talk);

        tblTabs.addTab(tblTabs.newTab().setCustomView(rel1));
        tblTabs.addTab(tblTabs.newTab().setCustomView(rel2));
        tblTabs.addTab(tblTabs.newTab().setCustomView(rel3));

        final ViewPager pagProspecto = (ViewPager)view.findViewById(R.id.pagProspectos);
        final PagerAdapterProspectos adapter = new PagerAdapterProspectos(getChildFragmentManager());
        pagProspecto.setAdapter(adapter);
        pagProspecto.setOffscreenPageLimit(3);
        pagProspecto.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tblTabs));

        tblTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab){
                RelativeLayout relSelected = (RelativeLayout)tab.getCustomView();
                TextView txt1 = (TextView)relSelected.findViewById(R.id.txt1);
                ImageView img = (ImageView)relSelected.findViewById(R.id.img1) ;
                img.setColorFilter(Color.rgb(255, 255, 255));
                txt1.setTextSize(15);
                setFuente.setFuentebd(getContext(),txt1);
                txt1.setTextColor(getResources().getColor(R.color.blanco));
                pagProspecto.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab){
                RelativeLayout relUnselected = (RelativeLayout)tab.getCustomView();
                TextView txt1 = (TextView)relUnselected.findViewById(R.id.txt1);
                txt1.setTextColor(getResources().getColor(R.color.color_texto_celeste_hint));
                ImageView img1 = (ImageView)relUnselected.findViewById(R.id.img1) ;
                img1.setColorFilter(Color.rgb(255, 255, 255));
                txt1.setTypeface(null, Typeface.NORMAL);
                setFuente.setFuenteRg(getContext(),txt1);
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab){}
        });

        return view;
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    private class PagerAdapterProspectos extends FragmentPagerAdapter {

        public PagerAdapterProspectos(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0: return new ProspectosdelDiaFragment();
                case 1: return new ProspectosAgendadosFragment();
                //case 2: return new ProspectosRecordatorioLlamadaFragment();
                case 2: return new ProspectosRecordatorioLlamadaFragment();
                default: return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }


}
