package com.pacifico.agenda.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.devspark.progressfragment.ProgressFragment;
import com.pacifico.agenda.Model.Bean.CitaProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoDatosReferidoBean;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Adapters.ProspectosAdapter;
import com.pacifico.agenda.Util.Util;

import java.util.ArrayList;

/**
 * Created by Joel on 25/04/2016.
 */
public class ProspectosAgendadosFragment extends ProgressFragment {

    public static String TAG = ProspectosRecordatorioLlamadaFragment.class.getSimpleName();
    private View mView;
    private TaskDialog mTaskDialog;


    private android.widget.TextView tvNumAgendados;
    private android.widget.ListView lvLista;


    public static ProspectosAgendadosFragment newInstance() {
        ProspectosAgendadosFragment fragment = new ProspectosAgendadosFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         mView = inflater.inflate(R.layout.fragment_prospectos_agendados, container, false);

        this.lvLista = (ListView) mView.findViewById(R.id.lvLista);
        this.tvNumAgendados = (TextView) mView.findViewById(R.id.tvNumAgendados);

        return super.onCreateView(inflater, container, savedInstanceState);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        setContentView(mView);
        consultarDatos();
    }

    @Override
    public void onPause() {
        if (mTaskDialog != null && !mTaskDialog.getStatus().equals(AsyncTask.Status.FINISHED)) {
            try {
                mTaskDialog.cancel(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    private void consultarDatos() {

        if (mTaskDialog != null && mTaskDialog.getStatus() != AsyncTask.Status.FINISHED)
            return;

        mTaskDialog = new TaskDialog(getActivity());
        mTaskDialog.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void populate(ArrayList<ProspectoDatosReferidoBean> lista) {

        try {
            ProspectosAdapter adapter = new ProspectosAdapter(getActivity(), R.layout.row_prospectos_agendados, lista);
            lvLista.setAdapter(adapter);

            String mensaje = lista.size() + " " + getActivity().getString(R.string.agendados);
            tvNumAgendados.setText(mensaje);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    class TaskDialog extends AsyncTask<Void, Integer, Void> {

        private Context context;
        ArrayList<ProspectoDatosReferidoBean> lista;
        String error = "";

        public TaskDialog(Context context) {

            this.context = context;
        }

        protected void onPreExecute() {
            try {
                setContentShown(false);
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }

        }

        protected Void doInBackground(Void... params) {


            synchronized(this) {
                try {
                    lista = CitaReunionController.obtenerProspectosAgendadosHoy();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    error = ex.getMessage();
                }
                return null;
            }

        }

        protected void onPostExecute(Void result) {

            try {
                setContentShown(true);
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }

            if (Util.esVacioONull(error)) {
                try {
                    populate(lista);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    error = ex.getMessage();
                    Util.showToast(getActivity(), error);
                }
            }
            else
            {
                Util.showToast(getActivity(), error);
            }
        }
    }
}
