package com.pacifico.agenda.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;

/**
 * Created by joel on 8/26/16.
 */
public class NotasNuevoFragment extends Fragment {

    ImageButton btnRetrocederNotasNuevo;
    EditText edtNotasNuevo;
    LinearLayout linCabeceraEntrevistaNotasNuevo;

    private CalendarioActivity calendarioActivity;

    public static NotasNuevoFragment newInstance() {
        NotasNuevoFragment fragment = new NotasNuevoFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_notas_nuevo, container, false);
        linCabeceraEntrevistaNotasNuevo = (LinearLayout) linfragment.findViewById(R.id.linCabeceraEntrevistaNotasNuevo);
        btnRetrocederNotasNuevo = (ImageButton) linfragment.findViewById(R.id.btnRetrocederNotasNuevo);

        linCabeceraEntrevistaNotasNuevo.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        btnRetrocederNotasNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFragmentManager().popBackStack();
                ((CalendarioActivity)getActivity()).OcultarTeclado();

                String notasNueva = edtNotasNuevo.getText().toString().trim();
                if (notasNueva == "") notasNueva = null;

                calendarioActivity.prospectoCreacionTemp.setNota(notasNueva);
            }
        });

        edtNotasNuevo = (EditText) linfragment.findViewById(R.id.edtNotasNuevo);
        edtNotasNuevo.requestFocus();

        CargarDatosInicial();

        // Inflate the layout for this fragment
        return linfragment;
    }

    public void CargarDatosInicial() {

        if (calendarioActivity.prospectoCreacionTemp.getNota() != null)
            edtNotasNuevo.setText(calendarioActivity.prospectoCreacionTemp.getNota());
    }

}
