package com.pacifico.agenda.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.pacifico.agenda.Activity.OnboardingActivity;
import com.pacifico.agenda.R;

public class IndicadorPaginaFragment extends Fragment {
    private ImageView imgIndicadorPagina;
    private Button btnEntendido;
    private ImageButton imgBtnAtras;
    private OnboardingActivity onboardingActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view  = inflater.inflate(R.layout.fragment_indicador_pagina, container, false);
        imgIndicadorPagina = (ImageView)view.findViewById(R.id.imgIndicadorPagina);
        onboardingActivity = (OnboardingActivity) getActivity();
        btnEntendido = (Button) view.findViewById(R.id.btnEntendido);
        imgBtnAtras = (ImageButton)view.findViewById(R.id.imgBtnAtras);
        ImageButton imgBtnSiguiente = (ImageButton)view.findViewById(R.id.imgBtnSiguiente);
        imgBtnAtras.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onboardingActivity.actualizarPaginaConDelta(-1);
            }
        });
        imgBtnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                onboardingActivity.actualizarPaginaConDelta(1);
            }
        });
        return view;
    }
    public void actualizarIndicadorPagina(int pagina){
        int[] arrIds = new int[]{R.drawable.posicion1, R.drawable.posicion2, R.drawable.posicion3, R.drawable.posicion4, R.drawable.posicion5, R.drawable.posicion6, R.drawable.posicion7};
        imgIndicadorPagina.setImageDrawable(getResources().getDrawable(arrIds[pagina]));
        if(pagina == 0){
            imgBtnAtras.setVisibility(View.INVISIBLE);
        }else{
            imgBtnAtras.setVisibility(View.VISIBLE);
        }
        if(pagina == 6){
            btnEntendido.setVisibility(View.VISIBLE);
        }else{
            btnEntendido.setVisibility(View.INVISIBLE);
        }
    }
}