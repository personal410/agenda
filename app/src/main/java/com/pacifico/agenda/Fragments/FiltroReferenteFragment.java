package com.pacifico.agenda.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Model.Bean.ProspectoDatosReferidoBean;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joel on 25/04/2016.
 */
public class FiltroReferenteFragment extends Fragment {

    private CalendarioActivity calendarioActivity;
    private android.widget.ImageButton btnRetrocederFiltroReferente;
    private android.widget.EditText edtFiltrarReferente;
    private ListView lvLista;
    private FiltroReferenteAdapter adapter;

    public static FiltroReferenteFragment newInstance() {
        FiltroReferenteFragment fragment = new FiltroReferenteFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        final View mView = inflater.inflate(R.layout.fragment_filtro_referente, container, false);

        this.lvLista = (ListView) mView.findViewById(R.id.lvLista);
        this.edtFiltrarReferente = (EditText) mView.findViewById(R.id.edtFiltrarReferente);
        this.btnRetrocederFiltroReferente = (ImageButton) mView.findViewById(R.id.btnRetrocederFiltroReferente);

        btnRetrocederFiltroReferente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        this.edtFiltrarReferente.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                      int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                if (adapter != null)
                    adapter.getFilter().filter(
                            edtFiltrarReferente.getText().toString());
            }
        });

        obtenerProspectos();
        return mView;
    }

    private void obtenerProspectos() {
        ArrayList<ProspectoDatosReferidoBean> listaProspectos = new ArrayList<>();
        listaProspectos = ProspectoController.obtenerProspectosCantReferidos();
        adapter = new FiltroReferenteAdapter(getActivity(), R.layout.row_filtrar_referente, listaProspectos);
        lvLista.setAdapter(adapter);
    }

    class FiltroReferenteAdapter extends ArrayAdapter<ProspectoDatosReferidoBean> {

        private Activity context;
        private int resource;
        private List<ProspectoDatosReferidoBean> items;
        ArrayList<ProspectoDatosReferidoBean> originalList;
        ArrayList<ProspectoDatosReferidoBean> filtroList;
        ReferenteFilter filter;

        public FiltroReferenteAdapter(Activity context, int resource, List<ProspectoDatosReferidoBean> items) {
            super(context, resource, items);
            this.context = context;
            this.resource = resource;
            this.items = items;

            filtroList = new ArrayList<ProspectoDatosReferidoBean>();
            filtroList.addAll(items);

            originalList = new ArrayList<ProspectoDatosReferidoBean>();
            originalList.addAll(items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final ProspectoDatosReferidoBean item = items.get(position);
            final LinearLayout rowView;

            rowView = new LinearLayout(getContext());
            inflater.inflate(resource, rowView, true);

            TextView tvReferente =  (TextView)rowView.findViewById(R.id.tvReferente);
            TextView tvReferidos =  (TextView)rowView.findViewById(R.id.tvReferidos);

            String nombreCompleto = String.format("%s %s %s",
                    Util.capitalizedString(item.getNombres()),
                    Util.capitalizedString(item.getApellidoPaterno()),
                    Util.capitalizedString(item.getApellidoMaterno())
            ).trim();

            tvReferente.setText(nombreCompleto);

            String cantReferidos = "";
            if (item.getCantReferidos() != 1)
                cantReferidos = String.valueOf(item.getCantReferidos()) + " referidos";
            else
                cantReferidos = String.valueOf(item.getCantReferidos()) + " referido";

            tvReferidos.setText(cantReferidos);

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    calendarioActivity.prospectoDatosRefFiltro = item;
                    getFragmentManager().beginTransaction().replace(R.id.fraFormularioFlotante,new FiltroDetalleReferenteFragment()).addToBackStack(null).commit();
                }
            });

            return rowView;
        }

        public Filter getFilter() {

            if (filter == null)
                filter = new ReferenteFilter();

            return filter;
        }

        class ReferenteFilter extends Filter {

            public ReferenteFilter( ) {
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                constraint = constraint.toString().toUpperCase();

                FilterResults result = new FilterResults();
                if (constraint != null && constraint.toString().length() > 0) {
                    ArrayList<ProspectoDatosReferidoBean> filteredItems = new ArrayList<ProspectoDatosReferidoBean>();

                    for (int i = 0, l = originalList.size(); i < l; i++) {
                        String texto = "";

                        texto = Util.strNULL(originalList.get(i).getNombres())+" " + Util.strNULL(originalList.get(i).getApellidoPaterno())+ " "+ Util.strNULL(originalList.get(i).getApellidoMaterno());

                        if (texto.indexOf(constraint.toString().toUpperCase()) != -1)
                            filteredItems.add(originalList.get(i));
                    }
                    result.count = filteredItems.size();
                    result.values = filteredItems;
                } else {
                    synchronized (this) {
                        result.values = originalList;
                        result.count = originalList.size();
                    }
                }
                return result;
            }

            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                // TODO Auto-generated method stub
                filtroList = (ArrayList<ProspectoDatosReferidoBean>) results.values;
                notifyDataSetChanged();
                clear();
                for (int i = 0, l = filtroList.size(); i < l; i++) {
                    add(filtroList.get(i));
                    notifyDataSetInvalidated();
                }
            }
        }

    }

}
