package com.pacifico.agenda.Fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.devspark.progressfragment.ProgressFragment;
import com.pacifico.agenda.Model.Bean.CitaProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoDatosReferidoBean;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Adapters.ProspectosAdapter;
import com.pacifico.agenda.Util.Util;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Joel on 25/04/2016.
 */
public class ProspectosdelDiaFragment extends ProgressFragment {
    public static String TAG = ProspectosdelDiaFragment.class.getSimpleName();
    private TaskDialog mTaskDialog;
    private View mView;

    private ListView lvLista;
    private TextView tvNumAgendados;
    private TextView tvNumContactados;
    private String mensajeAgendados = "";
    private String mensajeContactados = "";

    public static ProspectosdelDiaFragment newInstance() {
        ProspectosdelDiaFragment fragment = new ProspectosdelDiaFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_prospectos_dia, container, false);
        this.lvLista = (ListView) mView.findViewById(R.id.lvLista);
        this.tvNumAgendados = (TextView) mView.findViewById(R.id.tvNumAgendados);
        this.tvNumContactados = (TextView) mView.findViewById(R.id.tvNumContactados);
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        setContentView(mView);
        consultarDatos();
    }

    @Override
    public void onPause() {
        if (mTaskDialog != null && !mTaskDialog.getStatus().equals(AsyncTask.Status.FINISHED)) {
            try {
                mTaskDialog.cancel(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    private void consultarDatos() {

        if (mTaskDialog != null && mTaskDialog.getStatus() != AsyncTask.Status.FINISHED)
            return;

        mTaskDialog = new TaskDialog(getActivity());
        mTaskDialog.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void populate(ArrayList<ProspectoDatosReferidoBean> lista, int totalAgendadoDia, int totalContactadosDia) {
        try {
            if (totalAgendadoDia >1 )
                mensajeAgendados = totalAgendadoDia + " " + getActivity().getString(R.string.agendados);
            else
                mensajeAgendados = totalAgendadoDia + " " + getActivity().getString(R.string.agendado);

            tvNumAgendados.setText(mensajeAgendados);

            if (totalContactadosDia >1 )
                mensajeContactados = totalContactadosDia + " " + getActivity().getString(R.string.contactados);
            else
                mensajeContactados = totalContactadosDia + " " + getActivity().getString(R.string.contactado);

            tvNumContactados.setText(mensajeContactados);

            if (lista.size() > 0 ) {
                ProspectosAdapter adapter = new ProspectosAdapter(getActivity(), R.layout.row_prospectos_dia, lista);
                lvLista.setAdapter(adapter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class TaskDialog extends AsyncTask<Void, Integer, Void> {

        private Context context;
        //ArrayList<ArrayList<CitaProspectoBean>> lista;
        ArrayList<ProspectoDatosReferidoBean> lista;
        int totalAgendadosDia = 0;
        int totalContactadosDia = 0;
        String error = "";

        public TaskDialog(Context context) {

            this.context = context;
        }

        protected void onPreExecute() {
            try {
                setContentShown(false);
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }

        }

        protected Void doInBackground(Void... params) {


            synchronized(this) {
                try {
                    lista = ProspectoController.getProspectosDelDia();
                    totalAgendadosDia = CitaReunionController.obtenerTotalAgendadosDia();
                    totalContactadosDia = CitaReunionController.obtenerTotalContactadosDia();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    error = ex.getMessage();
                }
                return null;
            }

        }

        protected void onPostExecute(Void result) {

            try {
                setContentShown(true);
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }

            if (Util.esVacioONull(error)) {
                try {
                    populate(lista,totalAgendadosDia,totalContactadosDia);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    error = ex.getMessage();
                    Util.showToast(getActivity(), error);
                }
            }
            else
            {
                Util.showToast(getActivity(), error);
            }
        }
    }

}
