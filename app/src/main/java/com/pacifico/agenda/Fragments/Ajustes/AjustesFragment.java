package com.pacifico.agenda.Fragments.Ajustes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pacifico.agenda.Activity.OnboardingActivity;
import com.pacifico.agenda.R;

public class AjustesFragment extends Fragment implements View.OnClickListener{
    private LinearLayout linLaySeleccionado;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_ajustes, container, false);
        LinearLayout linLayDatosPersonales = (LinearLayout)view.findViewById(R.id.linLayDatosPersonales);
        linLayDatosPersonales.setOnClickListener(this);
        LinearLayout linLayAlertaNotificaciones = (LinearLayout)view.findViewById(R.id.linLayAlertaNotificaciones);
        linLayAlertaNotificaciones.setOnClickListener(this);
        LinearLayout linLaySincronizacionManual = (LinearLayout)view.findViewById(R.id.linLaySincronizacionManual);
        linLaySincronizacionManual.setOnClickListener(this);
        LinearLayout linLayAcercaApp = (LinearLayout)view.findViewById(R.id.linLayAcercaApp);
        linLayAcercaApp.setOnClickListener(this);
        LinearLayout linLayGuiaBienvenida = (LinearLayout)view.findViewById(R.id.linLayGuiaBienvenida);
        linLayGuiaBienvenida.setOnClickListener(this);
        return view;
    }
    private void desactivarLinLaySeleccionado(){
        if(linLaySeleccionado != null){
            TextView txtTitulo = (TextView)linLaySeleccionado.findViewById(R.id.txtTitulo);
            txtTitulo.setTextColor(getResources().getColor(R.color.colorMarron));
        }
    }
    @Override
    public void onClick(View v){
        desactivarLinLaySeleccionado();
        linLaySeleccionado = (LinearLayout)v;
        TextView txtTitulo = (TextView)linLaySeleccionado.findViewById(R.id.txtTitulo);
        txtTitulo.setTextColor(getResources().getColor(R.color.colorCeleste));
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment nuevoFragment;
        int tag = Integer.parseInt((String)linLaySeleccionado.getTag());
        switch (tag){
            case 1:
                nuevoFragment = new IntermediarioDatosPersonalesFragment();
                transaction.replace(R.id.FragmentContainer, nuevoFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case 2:
                nuevoFragment = new AlertasNotificacionesFragment();
                transaction.replace(R.id.FragmentContainer, nuevoFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case 3:
                nuevoFragment = new SincronizacionManualFragment();
                transaction.replace(R.id.FragmentContainer, nuevoFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case 4:
                nuevoFragment = new AcercaAppFragment();
                transaction.replace(R.id.FragmentContainer, nuevoFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case 5:
                Intent intent = new Intent(getActivity(),OnboardingActivity.class);
                intent.putExtra("irCalendario", false);
                startActivity(intent);
                break;
        }
    }
}