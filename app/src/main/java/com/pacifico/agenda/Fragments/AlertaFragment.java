package com.pacifico.agenda.Fragments;

import android.os.Bundle;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Model.Bean.AjustesBean;
import com.pacifico.agenda.Model.Controller.AjustesController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;

import java.util.ArrayList;

/**
 * Created by Joel on 25/04/2016.
 */
public class AlertaFragment extends Fragment{
    ImageButton btnRetrocederRecordatorio;
    LinearLayout linCabecera;
    Spinner spiAlertaCustomizada;
    private RadioGroup rdgAlertasPredefinidas;
    private RadioButton rdb10minutos, rdb30minutos, rdb1hora,rdb1horamedia;
    private EditText edtValorCustomizado;
    AppCompatCheckBox chkCustomizada;

    private CalendarioActivity calendarioActivity;

    public static AlertaFragment newInstance() {
        AlertaFragment fragment = new AlertaFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        calendarioActivity = (CalendarioActivity)getActivity();

        LinearLayout linfragment = (LinearLayout )inflater.inflate(R.layout.fragment_alerta, container, false);

        btnRetrocederRecordatorio = (ImageButton) linfragment.findViewById(R.id.btnRetrocederRecordatorio);

        linCabecera = (LinearLayout) linfragment.findViewById(R.id.linEntrevistaCabeceraAlerta);

        FormatearTarjeta();

        spiAlertaCustomizada = (Spinner) linfragment.findViewById(R.id.spiAlertaCustomizada);

        String[] arrAlertasCustomizadas = {"minutos antes", "horas antes"};
        ArrayAdapter<String> arrayAlerta = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item);
        arrayAlerta.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spiAlertaCustomizada.setAdapter(arrayAlerta);

        setAdapterToSpinnerWithArray(spiAlertaCustomizada, arrAlertasCustomizadas);

        rdgAlertasPredefinidas = (RadioGroup) linfragment.findViewById(R.id.rdgAlertasPredefinidas);
        rdb10minutos = (RadioButton) linfragment.findViewById(R.id.rdb10minutos);
        rdb30minutos = (RadioButton) linfragment.findViewById(R.id.rdb30minutos);
        rdb1hora = (RadioButton) linfragment.findViewById(R.id.rdb1hora);
        rdb1horamedia = (RadioButton) linfragment.findViewById(R.id.rdb1horamedia);
        chkCustomizada = (AppCompatCheckBox) linfragment.findViewById(R.id.chkCustomizada);
        edtValorCustomizado = (EditText) linfragment.findViewById(R.id.edtValorCustomizado);

        rdgAlertasPredefinidas.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                chkCustomizada.setChecked(false);
                edtValorCustomizado.setEnabled(false);
            }
        });


        chkCustomizada.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                rdb10minutos.setChecked(false);
                rdb30minutos.setChecked(false);
                rdb1hora.setChecked(false);
                rdb1horamedia.setChecked(false);
                rdgAlertasPredefinidas.clearCheck();
                chkCustomizada.setChecked(true);
                edtValorCustomizado.setEnabled(true);
            }
        });

        /*edtValorCustomizado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                rdb10minutos.setChecked(false);
                rdb30minutos.setChecked(false);
                rdb1hora.setChecked(false);
                rdb1horamedia.setChecked(false);
                chkCustomizada.setChecked(true);
            }
        });*/


        btnRetrocederRecordatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();

                ((CalendarioActivity)getActivity()).OcultarTeclado();

                SetearAlerta();
            }
        });

        // TODO: validar que solo pueda ingresar numeros en el check personalizado
        // Inflate the layout for this fragment

        CargarDatosInicial();

        return linfragment;
    }

    private void setAdapterToSpinnerWithArray(Spinner spin, String[] arrPosiblesValores){
        ArrayAdapter<String> arrAdap = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, arrPosiblesValores);
        arrAdap.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spin.setAdapter(arrAdap);
    }

    public void SetearAlerta()
    {
        int minutos_antes_alerta= 0; //= "";
        if (chkCustomizada.isChecked()){
            String valor = edtValorCustomizado.getText().toString();

            if (!"".equals(valor))
            if (spiAlertaCustomizada.getSelectedItemPosition() ==0) // minutosAntes

                try{
                    int minutos = Integer.parseInt(valor);
                    minutos_antes_alerta = minutos;
                }
                catch(Exception e){}

            else
            {
                try{
                    int horasaminutos = Integer.parseInt(valor)*60;
                    minutos_antes_alerta = horasaminutos;
                }catch(Exception e){valor = "";}
            }

        }else{

            int selectedId = rdgAlertasPredefinidas.getCheckedRadioButtonId();

            // find which radioButton is checked by id
            if(selectedId == rdb10minutos.getId()) {
                minutos_antes_alerta= 10;
            } else if(selectedId == rdb30minutos.getId()) {
                minutos_antes_alerta = 30;
            } else if(selectedId == rdb1hora.getId()) {
                minutos_antes_alerta = 60;
            } else if(selectedId == rdb1horamedia.getId()) {
                minutos_antes_alerta = 90;
            }
        }

        calendarioActivity.citaActual.setAlertaMinutosAntes(minutos_antes_alerta);
        // TODO: SETEAR ALERTA Y SI ES UNA ACTUALIZACION DESACTIVAR LA ANTERIOR

    }

    public void CargarDatosInicial() {
        if (calendarioActivity.citaActual != null &&
            calendarioActivity.citaActual.getAlertaMinutosAntes() > 0)
        {
            if (calendarioActivity.citaActual.getAlertaMinutosAntes() == 10)
                rdb10minutos.setChecked(true);
            else if (calendarioActivity.citaActual.getAlertaMinutosAntes() == 30)
                rdb30minutos.setChecked(true);
            else if (calendarioActivity.citaActual.getAlertaMinutosAntes() == 60)
                rdb1hora.setChecked(true);
            else if (calendarioActivity.citaActual.getAlertaMinutosAntes() == 90)
                rdb1horamedia.setChecked(true);
            else
            {
                chkCustomizada.setChecked(true);
                if ((calendarioActivity.citaActual.getAlertaMinutosAntes() % 60) == 0 )
                {
                    int horas = calendarioActivity.citaActual.getAlertaMinutosAntes() / 60;
                    edtValorCustomizado.setText(String.valueOf(horas));
                    spiAlertaCustomizada.setSelection(1);
                }else
                {
                    edtValorCustomizado.setText(String.valueOf(calendarioActivity.citaActual.getAlertaMinutosAntes()));
                    spiAlertaCustomizada.setSelection(0);
                }
            }
        }
    }
    public void FormatearTarjeta(){
        if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista) {
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_primera_entrevista));
        }else if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_SegundaEntrevista) {
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_segunda_entrevista));
        }else if(calendarioActivity.citaActual.getCodigoEtapaProspecto() == Constantes.EtapaProspecto_EntrevistaAdicional){
            linCabecera.setBackgroundColor(getResources().getColor(R.color.color_entrevista_adicional));
        }
    }
}