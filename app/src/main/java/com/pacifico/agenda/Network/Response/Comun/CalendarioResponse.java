package com.pacifico.agenda.Network.Response.Comun;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joel on 08/06/2016.
 */
public class CalendarioResponse{
    @SerializedName("IdCalendario")
    @Expose
    private String idCalendario;
    @SerializedName("Anio")
    @Expose
    private String anio;
    @SerializedName("SemanaAno")
    @Expose
    private String semanaAno;
    @SerializedName("MesAnio")
    @Expose
    private String mesAnio;
    @SerializedName("SemanaMes")
    @Expose
    private String semanaMes;
    @SerializedName("FechaInicioSemana")
    @Expose
    private String fechaInicioSemana;
    @SerializedName("FechaFinSemana")
    @Expose
    private String fechaFinSemana;

    public String getIdCalendario() {
        return idCalendario;
    }

    public String getAnio() {
        return anio;
    }

    public String getSemanaAno() {
        return semanaAno;
    }

    public String getMesAnio() {
        return mesAnio;
    }

    public String getSemanaMes() {
        return semanaMes;
    }

    public String getFechaInicioSemana() {
        return fechaInicioSemana;
    }

    public String getFechaFinSemana() {
        return fechaFinSemana;
    }
}