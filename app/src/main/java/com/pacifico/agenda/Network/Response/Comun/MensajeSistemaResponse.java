package com.pacifico.agenda.Network.Response.Comun;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 6/06/16.
 */
public class MensajeSistemaResponse{
    @SerializedName("IdMensajeSistema")
    @Expose
    private String idMensajeSistema;
    @SerializedName("DescripcionMensaje")
    @Expose
    private String descripcionMensaje;
    @SerializedName("FechaInicioVigencia")
    @Expose
    private String fechaInicioVigencia;
    @SerializedName("FechaFinVigencia")
    @Expose
    private String fechaFinVigencia;
    public String getIdMensajeSistema() {
        return idMensajeSistema;
    }

    public String getDescripcionMensaje(){
        return descripcionMensaje;
    }

    public String getFechaInicioVigencia(){
        return fechaInicioVigencia;
    }

    public String getFechaFinVigencia() {
        return fechaFinVigencia;
    }
}