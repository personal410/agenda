package com.pacifico.agenda.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 30/07/16.
 */
public class SetReunionInternaResponse {
    @SerializedName("IdReunion")
    @Expose
    private String IdReunion;
    @SerializedName("IdReunionDispositivo")
    @Expose
    private String IdReunionDispositivo;

    public String getIdReunion() {
        return IdReunion;
    }

    public String getIdReunionDispositivo() {
        return IdReunionDispositivo;
    }
}