package com.pacifico.agenda.Network.Request.ValidarToken;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 27/07/16.
 */
public class ValidarTokenRequest{
    @SerializedName("token")
    @Expose
    private ValidarTokenTokenRequest token;

    public void setToken(ValidarTokenTokenRequest token){
        this.token = token;
    }
}