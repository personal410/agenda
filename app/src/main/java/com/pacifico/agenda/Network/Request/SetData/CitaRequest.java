package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by joel on 7/4/16.
 */
public class CitaRequest{
    @SerializedName("IdCita")
    @Expose
    private String IdCita;
    @SerializedName("IdProspecto")
    @Expose
    private String IdProspecto;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;
    @SerializedName("IdCitaDispositivo")
    @Expose
    private String IdCitaDispositivo;
    @SerializedName("NumeroEntrevista")
    @Expose
    private String NumeroEntrevista;
    @SerializedName("CodigoEstado")
    @Expose
    private String CodigoEstado;
    @SerializedName("CodigoResultado")
    @Expose
    private String CodigoResultado;
    @SerializedName("FechaCita")
    @Expose
    private String FechaCita;
    @SerializedName("HoraInicio")
    @Expose
    private String HoraInicio;
    @SerializedName("HoraFin")
    @Expose
    private String HoraFin;
    @SerializedName("Ubicacion")
    @Expose
    private String Ubicacion;
    @SerializedName("ReferenciaUbicacion")
    @Expose
    private String ReferenciaUbicacion;
    @SerializedName("FlagInvitadoGU")
    @Expose
    private String FlagInvitadoGU;
    @SerializedName("FlagInvitadoGA")
    @Expose
    private String FlagInvitadoGA;
    @SerializedName("AlertaMinutosAntes")
    @Expose
    private String AlertaMinutosAntes;
    @SerializedName("CantidadVI")
    @Expose
    private String CantidadVI;
    @SerializedName("PrimaTargetVI")
    @Expose
    private String PrimaTargetVI;
    @SerializedName("CantidadAP")
    @Expose
    private String CantidadAP;
    @SerializedName("PrimaTargetAP")
    @Expose
    private String PrimaTargetAP;
    @SerializedName("CodigoIntermediarioCreacion")
    @Expose
    private String CodigoIntermediarioCreacion;
    @SerializedName("CodigoIntermediarioModificacion")
    @Expose
    private String CodigoIntermediarioModificacion;
    @SerializedName("CodigoEtapaProspecto")
    @Expose
    private String CodigoEtapaProspecto;

    @SerializedName("CodigoMotivoReagendado")
    @Expose
    private String CodigoMotivoReagendado;

    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String FechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String FechaModificacionDispositivo;



    public void setIdCita(String idCita) {
        IdCita = idCita;
    }

    public void setIdProspecto(String idProspecto) {
        IdProspecto = idProspecto;
    }

    public void setIdProspectoDispositivo(String idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public void setIdCitaDispositivo(String idCitaDispositivo) {
        IdCitaDispositivo = idCitaDispositivo;
    }

    public void setNumeroEntrevista(String numeroEntrevista) {
        NumeroEntrevista = numeroEntrevista;
    }

    public void setCodigoEstado(String codigoEstado) {
        CodigoEstado = codigoEstado;
    }

    public void setCodigoResultado(String codigoResultado) {
        CodigoResultado = codigoResultado;
    }

    public void setFechaCita(String fechaCita) {
        FechaCita = fechaCita;
    }

    public void setHoraInicio(String horaInicio) {
        HoraInicio = horaInicio;
    }

    public void setHoraFin(String horaFin) {
        HoraFin = horaFin;
    }

    public void setUbicacion(String ubicacion) {
        Ubicacion = ubicacion;
    }

    public void setReferenciaUbicacion(String referenciaUbicacion) {
        ReferenciaUbicacion = referenciaUbicacion;
    }

    public void setFlagInvitadoGU(String flagInvitadoGU) {
        FlagInvitadoGU = flagInvitadoGU;
    }

    public void setFlagInvitadoGA(String flagInvitadoGA) {
        FlagInvitadoGA = flagInvitadoGA;
    }

    public void setAlertaMinutosAntes(String alertaMinutosAntes) {
        AlertaMinutosAntes = alertaMinutosAntes;
    }

    public void setCantidadVI(String cantidadVI) {
        CantidadVI = cantidadVI;
    }

    public void setPrimaTargetVI(String primaTargetVI) {
        PrimaTargetVI = primaTargetVI;
    }

    public void setCantidadAP(String cantidadAP) {
        CantidadAP = cantidadAP;
    }

    public void setPrimaTargetAP(String primaTargetAP) {
        PrimaTargetAP = primaTargetAP;
    }

    public void setCodigoIntermediarioCreacion(String codigoIntermediarioCreacion) {
        CodigoIntermediarioCreacion = codigoIntermediarioCreacion;
    }

    public void setCodigoIntermediarioModificacion(String codigoIntermediarioModificacion) {
        CodigoIntermediarioModificacion = codigoIntermediarioModificacion;
    }

    public void setCodigoEtapaProspecto(String codigoEtapaProspecto) {
        CodigoEtapaProspecto = codigoEtapaProspecto;
    }

    public void setCodigoMotivoReagendado(String codigoMotivoReagendado) {
        CodigoMotivoReagendado = codigoMotivoReagendado;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }
}