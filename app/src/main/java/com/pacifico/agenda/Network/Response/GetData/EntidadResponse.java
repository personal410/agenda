package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 5/07/16.
 */
public class EntidadResponse{
    @SerializedName("IdEntidad")
    @Expose
    private String idEntidad;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    @SerializedName("Imagen")
    @Expose
    private String imagen;
    @SerializedName("CodigoTipoCobranza")
    @Expose
    private String codigoTipoCobranza;
    public String getIdEntidad() {
        return idEntidad;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public String getImagen() {
        return imagen;
    }
    public String getCodigoTipoCobranza() {
        return codigoTipoCobranza;
    }
}