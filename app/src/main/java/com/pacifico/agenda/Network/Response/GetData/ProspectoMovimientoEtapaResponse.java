package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joel on 08/06/2016.
 */
public class ProspectoMovimientoEtapaResponse {
    @SerializedName("IdMovimiento")
    @Expose
    private String idMovimiento;
    @SerializedName("IdProspecto")
    @Expose
    private String idProspecto;
    @SerializedName("IdMovimientoDispositivo")
    @Expose
    private String idMovimientoDispositivo;
    @SerializedName("CodigoEtapa")
    @Expose
    private String codigoEtapa;
    @SerializedName("CodigoEstado")
    @Expose
    private String codigoEstado;
    @SerializedName("FechaMovimientoEtapaDispositivo")
    @Expose
    private String fechaMovimientoEtapaDispositivo;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private Object idProspectoDispositivo;

    public String getIdMovimiento() {
        return idMovimiento;
    }

    public String getIdProspecto() {
        return idProspecto;
    }

    public String getIdMovimientoDispositivo() {
        return idMovimientoDispositivo;
    }

    public String getCodigoEtapa() {
        return codigoEtapa;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public String getFechaMovimientoEtapaDispositivo() {
        return fechaMovimientoEtapaDispositivo;
    }

    public Object getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }
}