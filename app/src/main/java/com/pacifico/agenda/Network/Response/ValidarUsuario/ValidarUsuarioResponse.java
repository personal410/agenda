package com.pacifico.agenda.Network.Response.ValidarUsuario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 27/07/16.
 */
public class ValidarUsuarioResponse{
    @SerializedName("IsValid")
    @Expose
    private String isValid;
    @SerializedName("ResultCode")
    @Expose
    private String resultCode;
    @SerializedName("ResultMessage")
    @Expose
    private String resultMessage;

    public String getIsValid(){
        return isValid;
    }

    public String getResultCode(){
        return resultCode;
    }

    public String getResultMessage(){
        return resultMessage;
    }
}