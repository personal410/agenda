package com.pacifico.agenda.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 30/07/16.
 */
public class SetProspectoMovimientoEtapaResponse{
    @SerializedName("IdMovimiento")
    @Expose
    private String IdMovimiento;
    @SerializedName("IdMovimientoDispositivo")
    @Expose
    private String IdMovimientoDispositivo;

    public String getIdMovimiento() {
        return IdMovimiento;
    }

    public String getIdMovimientoDispositivo() {
        return IdMovimientoDispositivo;
    }
}