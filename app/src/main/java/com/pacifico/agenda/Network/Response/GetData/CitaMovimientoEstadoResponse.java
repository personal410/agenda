package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joel on 08/06/2016.
 */
public class CitaMovimientoEstadoResponse {
    @SerializedName("IdMovimiento")
    @Expose
    private String idMovimiento;
    @SerializedName("IdCita")
    @Expose
    private String idCita;
    @SerializedName("IdMovimientoDispositivo")
    @Expose
    private String idMovimientoDispositivo;
    @SerializedName("CodigoEstado")
    @Expose
    private String codigoEstado;
    @SerializedName("CodigoResultado")
    @Expose
    private String codigoResultado;
    @SerializedName("FechaMovimientoEstadoDispositivo")
    @Expose
    private String fechaMovimientoEstadoDispositivo;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String idProspectoDispositivo;
    @SerializedName("IdCitaDispositivo")
    @Expose
    private String idCitaDispositivo;

    public String getIdMovimiento() {
        return idMovimiento;
    }

    public String getIdCita() {
        return idCita;
    }

    public String getIdMovimientoDispositivo() {
        return idMovimientoDispositivo;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public String getCodigoResultado() {
        return codigoResultado;
    }

    public String getFechaMovimientoEstadoDispositivo() {
        return fechaMovimientoEstadoDispositivo;
    }

    public String getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public String getIdCitaDispositivo() {
        return idCitaDispositivo;
    }
}