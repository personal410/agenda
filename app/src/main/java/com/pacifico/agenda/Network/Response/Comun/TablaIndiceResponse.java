package com.pacifico.agenda.Network.Response.Comun;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 6/06/16.
 */
public class TablaIndiceResponse {
    @SerializedName("IdTabla")
    @Expose
    private String idTabla;
    @SerializedName("NombreTabla")
    @Expose
    private String nombreTabla;

    public String getIdTabla() {
        return idTabla;
    }

    public String getNombreTabla() {
        return nombreTabla;
    }
}