package com.pacifico.agenda.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 4/07/16.
 */
public class SetProspectoResponse{
    @SerializedName("IdProspecto")
    @Expose
    private String idProspecto;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String idProspectoDispositivo;
    public String getIdProspecto() {
        return idProspecto;
    }
    public String getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }
}