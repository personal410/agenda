package com.pacifico.agenda.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pacifico.agenda.Network.Response.Comun.CalendarioResponse;
import com.pacifico.agenda.Network.Response.Comun.IntermediarioResponse;
import com.pacifico.agenda.Network.Response.Comun.MensajeSistemaResponse;
import com.pacifico.agenda.Network.Response.Comun.ParametroResponse;
import com.pacifico.agenda.Network.Response.Comun.ProcesoSincronizacionResponse;
import com.pacifico.agenda.Network.Response.Comun.TablaIndiceResponse;
import com.pacifico.agenda.Network.Response.Comun.TablaTablasResponse;
import com.pacifico.agenda.Network.Response.GetData.EntidadResponse;
import com.pacifico.agenda.Network.Response.GetData.ProspectoResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victorsalazar on 9/06/16.
 */
public class SetDataResponse{
    @SerializedName("ExisteNuevaMigracionCartera")
    @Expose
    private String existeNuevaMigracionCartera;
    @SerializedName("ProcesoSincronizacion")
    @Expose
    private ProcesoSincronizacionResponse procesoSincronizacion;
    @SerializedName("Reunion")
    @Expose
    private List<SetReunionInternaResponse> arrReunionInterna;
    @SerializedName("Prospecto")
    @Expose
    private List<SetProspectoResponse> arrProspecto;
    @SerializedName("ProspectoMovimientoEtapa")
    @Expose
    private List<SetProspectoMovimientoEtapaResponse> arrProspectoMovimientoEtapa;
    @SerializedName("Familiar")
    @Expose
    private List<SetFamiliarResponse> arrFamiliar;
    @SerializedName("Cita")
    @Expose
    private List<SetCitaResponse> arrCita;
    @SerializedName("CitaMovimientoEstado")
    @Expose
    private List<SetCitaMovimientoEstadoResponse> arrCitaMovimientoEstado;
    @SerializedName("Referido")
    @Expose
    private List<SetReferidoResponse> arrReferido;
    @SerializedName("RecordatorioLlamada")
    @Expose
    private List<SetRecordatorioLlamadaResponse> arrRecordatorioLlamada;
    @SerializedName("Parametro")
    @Expose
    private List<ParametroResponse> arrParametro = new ArrayList<>();
    @SerializedName("TablaIndice")
    @Expose
    private List<TablaIndiceResponse> arrTablaIndice;
    @SerializedName("TablaTablas")
    @Expose
    private List<TablaTablasResponse> arrTablaTablas;
    @SerializedName("Calendario")
    @Expose
    private List<CalendarioResponse> arrCalendario;
    @SerializedName("MensajeSistema")
    @Expose
    private List<MensajeSistemaResponse> arrMensajeSistema;
    @SerializedName("Entidad")
    @Expose
    private List<EntidadResponse> arrEntidad;
    @SerializedName("ConsolidadoIntermediario")
    @Expose
    private List<IntermediarioResponse> arrIntermediario;
    @SerializedName("ProspectoExterno")
    @Expose
    private List<ProspectoResponse> arrProspectoExterno;
    @SerializedName("ResultMessage")
    @Expose
    private String resultMessage;
    @SerializedName("ResultCode")
    @Expose
    private String resultCode;

    public ProcesoSincronizacionResponse getProcesoSincronizacion() {
        return procesoSincronizacion;
    }
    public List<SetFamiliarResponse> getArrFamiliar() {
        return arrFamiliar;
    }
    public List<SetProspectoResponse> getArrProspecto() {
        return arrProspecto;
    }
    public List<SetReferidoResponse> getArrReferido() {
        return arrReferido;
    }
    public String getResultMessage() {
        return resultMessage;
    }
    public List<ParametroResponse> getArrParametro() {
        return arrParametro;
    }

    public List<SetReunionInternaResponse> getArrReunionInterna() {
        return arrReunionInterna;
    }

    public List<SetProspectoMovimientoEtapaResponse> getArrProspectoMovimientoEtapa() {
        return arrProspectoMovimientoEtapa;
    }

    public List<SetCitaResponse> getArrCita() {
        return arrCita;
    }

    public List<SetCitaMovimientoEstadoResponse> getArrCitaMovimientoEstado() {
        return arrCitaMovimientoEstado;
    }

    public List<SetRecordatorioLlamadaResponse> getArrRecordatorioLlamada() {
        return arrRecordatorioLlamada;
    }

    public List<TablaIndiceResponse> getArrTablaIndice() {
        return arrTablaIndice;
    }

    public List<TablaTablasResponse> getArrTablaTablas() {
        return arrTablaTablas;
    }

    public List<CalendarioResponse> getArrCalendario() {
        return arrCalendario;
    }

    public List<MensajeSistemaResponse> getArrMensajeSistema() {
        return arrMensajeSistema;
    }

    public List<EntidadResponse> getArrEntidad() {
        return arrEntidad;
    }

    public List<IntermediarioResponse> getArrIntermediario() {
        return arrIntermediario;
    }

    public List<ProspectoResponse> getArrProspectoExterno() {
        return arrProspectoExterno;
    }

    public String getResultCode() {
        return resultCode;
    }
}