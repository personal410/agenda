package com.pacifico.agenda.Network.Request.ValidarUsuario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 27/07/16.
 */
public class ValidarUsuarioRequest{
    @SerializedName("token")
    @Expose
    private ValidarUsuarioTokenRequest token;

    public void setToken(ValidarUsuarioTokenRequest token) {
        this.token = token;
    }
}