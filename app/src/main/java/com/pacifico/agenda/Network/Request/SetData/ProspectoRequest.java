package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DSB on 01/07/2016.
 */
public class ProspectoRequest{
    @SerializedName("IdProspecto")
    @Expose
    private String IdProspecto;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;
    @SerializedName("Nombres")
    @Expose
    private String Nombres;
    @SerializedName("ApellidoPaterno")
    @Expose
    private String ApellidoPaterno;
    @SerializedName("ApellidoMaterno")
    @Expose
    private String ApellidoMaterno;
    @SerializedName("CodigoSexo")
    @Expose
    private String CodigoSexo;
    @SerializedName("CodigoEstadoCivil")
    @Expose
    private String CodigoEstadoCivil;
    @SerializedName("FechaNacimiento")
    @Expose
    private String FechaNacimiento;
    @SerializedName("CodigoRangoEdad")
    @Expose
    private String CodigoRangoEdad;
    @SerializedName("CodigoRangoIngreso")
    @Expose
    private String CodigoRangoIngreso;
    @SerializedName("FlagHijo")
    @Expose
    private String FlagHijo;
    @SerializedName("FlagConyuge")
    @Expose
    private String FlagConyuge;
    @SerializedName("CodigoTipoDocumento")
    @Expose
    private String CodigoTipoDocumento;
    @SerializedName("NumeroDocumento")
    @Expose
    private String NumeroDocumento;
    @SerializedName("TelefonoCelular")
    @Expose
    private String TelefonoCelular;
    @SerializedName("TelefonoFijo")
    @Expose
    private String TelefonoFijo;
    @SerializedName("TelefonoAdicional")
    @Expose
    private String TelefonoAdicional;
    @SerializedName("CorreoElectronico1")
    @Expose
    private String CorreoElectronico1;
    @SerializedName("CorreoElectronico2")
    @Expose
    private String CorreoElectronico2;
    @SerializedName("CodigoNacionalidad")
    @Expose
    private String CodigoNacionalidad;
    @SerializedName("CondicionFumador")
    @Expose
    private String CondicionFumador;
    @SerializedName("Empresa")
    @Expose
    private String Empresa;
    @SerializedName("Cargo")
    @Expose
    private String Cargo;
    @SerializedName("Nota")
    @Expose
    private String Nota;
    @SerializedName("FlagEnviarADNDigital")
    @Expose
    private String FlagEnviarADNDigital;
    @SerializedName("CodigoEtapa")
    @Expose
    private String CodigoEtapa;
    @SerializedName("CodigoEstado")
    @Expose
    private String CodigoEstado;
    @SerializedName("CodigoFuente")
    @Expose
    private String CodigoFuente;
    @SerializedName("CodigoEntorno")
    @Expose
    private String CodigoEntorno;
    @SerializedName("IdReferenciador")
    @Expose
    private String IdReferenciador;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String FechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String FechaModificacionDispositivo;
    @SerializedName("AdicionalTexto1")
    @Expose
    private String AdicionalTexto1;
    @SerializedName("AdicionalNumerico1")
    @Expose
    private String AdicionalNumerico1;
    @SerializedName("AdicionalTexto2")
    @Expose
    private String AdicionalTexto2;
    @SerializedName("AdicionalNumerico2")
    @Expose
    private String AdicionalNumerico2;

    public void setIdProspecto(String idProspecto) {
        IdProspecto = idProspecto;
    }

    public void setIdProspectoDispositivo(String idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public void setCodigoSexo(String codigoSexo) {
        CodigoSexo = codigoSexo;
    }

    public void setCodigoEstadoCivil(String codigoEstadoCivil) {
        CodigoEstadoCivil = codigoEstadoCivil;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        FechaNacimiento = fechaNacimiento;
    }

    public void setCodigoRangoEdad(String codigoRangoEdad) {
        CodigoRangoEdad = codigoRangoEdad;
    }

    public void setCodigoRangoIngreso(String codigoRangoIngreso) {
        CodigoRangoIngreso = codigoRangoIngreso;
    }

    public void setFlagHijo(String flagHijo) {
        FlagHijo = flagHijo;
    }

    public void setFlagConyuge(String flagConyuge) {
        FlagConyuge = flagConyuge;
    }

    public void setCodigoTipoDocumento(String codigoTipoDocumento) {
        CodigoTipoDocumento = codigoTipoDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        NumeroDocumento = numeroDocumento;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        TelefonoCelular = telefonoCelular;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        TelefonoFijo = telefonoFijo;
    }

    public void setTelefonoAdicional(String telefonoAdicional) {
        TelefonoAdicional = telefonoAdicional;
    }

    public void setCorreoElectronico1(String correoElectronico1) {
        CorreoElectronico1 = correoElectronico1;
    }

    public void setCorreoElectronico2(String correoElectronico2) {
        CorreoElectronico2 = correoElectronico2;
    }

    public void setCodigoNacionalidad(String codigoNacionalidad) {
        CodigoNacionalidad = codigoNacionalidad;
    }

    public void setCondicionFumador(String condicionFumador) {
        CondicionFumador = condicionFumador;
    }

    public void setEmpresa(String empresa) {
        Empresa = empresa;
    }

    public void setCargo(String cargo) {
        Cargo = cargo;
    }

    public void setNota(String nota) {
        Nota = nota;
    }

    public void setFlagEnviarADNDigital(String flagEnviarADNDigital) {
        FlagEnviarADNDigital = flagEnviarADNDigital;
    }

    public void setCodigoEtapa(String codigoEtapa) {
        CodigoEtapa = codigoEtapa;
    }

    public void setCodigoEstado(String codigoEstado) {
        CodigoEstado = codigoEstado;
    }

    public void setCodigoFuente(String codigoFuente) {
        CodigoFuente = codigoFuente;
    }

    public void setCodigoEntorno(String codigoEntorno) {
        CodigoEntorno = codigoEntorno;
    }

    public void setIdReferenciador(String idReferenciador) {
        IdReferenciador = idReferenciador;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public void setAdicionalTexto1(String adicionalTexto1) {
        AdicionalTexto1 = adicionalTexto1;
    }

    public void setAdicionalNumerico1(String adicionalNumerico1) {
        AdicionalNumerico1 = adicionalNumerico1;
    }

    public void setAdicionalTexto2(String adicionalTexto2) {
        AdicionalTexto2 = adicionalTexto2;
    }

    public void setAdicionalNumerico2(String adicionalNumerico2) {
        AdicionalNumerico2 = adicionalNumerico2;
    }
}