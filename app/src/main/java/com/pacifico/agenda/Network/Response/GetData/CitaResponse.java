package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joel on 08/06/2016.
 */
public class CitaResponse{
    @SerializedName("IdCita")
    @Expose
    private String idCita;
    @SerializedName("IdProspecto")
    @Expose
    private String idProspecto;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String idProspectoDispositivo;
    @SerializedName("IdCitaDispositivo")
    @Expose
    private String idCitaDispositivo;
    @SerializedName("NumeroEntrevista")
    @Expose
    private String numeroEntrevista;
    @SerializedName("CodigoEstado")
    @Expose
    private String codigoEstado;
    @SerializedName("CodigoResultado")
    @Expose
    private String codigoResultado;
    @SerializedName("FechaCita")
    @Expose
    private String fechaCita;
    @SerializedName("HoraInicio")
    @Expose
    private String horaInicio;
    @SerializedName("HoraFin")
    @Expose
    private String horaFin;
    @SerializedName("Ubicacion")
    @Expose
    private String ubicacion;
    @SerializedName("ReferenciaUbicacion")
    @Expose
    private String referenciaUbicacion;
    @SerializedName("FlagInvitadoGU")
    @Expose
    private String flagInvitadoGU;
    @SerializedName("FlagInvitadoGA")
    @Expose
    private String flagInvitadoGA;
    @SerializedName("AlertaMinutosAntes")
    @Expose
    private String alertaMinutosAntes;
    @SerializedName("CantidadVI")
    @Expose
    private String cantidadVI;
    @SerializedName("PrimaTargetVI")
    @Expose
    private String primaTargetVI;
    @SerializedName("CantidadAP")
    @Expose
    private String cantidadAP;
    @SerializedName("PrimaTargetAP")
    @Expose
    private String primaTargetAP;
    @SerializedName("CodigoIntermediarioCreacion")
    @Expose
    private String codigoIntermediarioCreacion;
    @SerializedName("CodigoIntermediarioModificacion")
    @Expose
    private String codigoIntermediarioModificacion;
    @SerializedName("CodigoEtapaProspecto")
    @Expose
    private String CodigoEtapaProspecto;
    @SerializedName("CodigoMotivoReagendado")
    @Expose
    private String CodigoMotivoReagendado;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String fechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String fechaModificacionDispositivo;

    public String getIdCita() {
        return idCita;
    }

    public String getIdProspecto() {
        return idProspecto;
    }

    public String getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public String getIdCitaDispositivo() {
        return idCitaDispositivo;
    }

    public String getNumeroEntrevista() {
        return numeroEntrevista;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public String getCodigoResultado() {
        return codigoResultado;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getReferenciaUbicacion() {
        return referenciaUbicacion;
    }

    public String getFlagInvitadoGU() {
        return flagInvitadoGU;
    }

    public String getFlagInvitadoGA() {
        return flagInvitadoGA;
    }

    public String getAlertaMinutosAntes() {
        return alertaMinutosAntes;
    }

    public String getCantidadVI() {
        return cantidadVI;
    }

    public String getPrimaTargetVI() {
        return primaTargetVI;
    }

    public String getCantidadAP() {
        return cantidadAP;
    }

    public String getPrimaTargetAP() {
        return primaTargetAP;
    }

    public String getCodigoIntermediarioCreacion() {
        return codigoIntermediarioCreacion;
    }

    public String getCodigoIntermediarioModificacion() {
        return codigoIntermediarioModificacion;
    }

    public String getCodigoEtapaProspecto() {
        return CodigoEtapaProspecto;
    }

    public String getCodigoMotivoReagendado() { return CodigoMotivoReagendado; }

    public String getFechaCreacionDispositivo() {
        return fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return fechaModificacionDispositivo;
    }
}