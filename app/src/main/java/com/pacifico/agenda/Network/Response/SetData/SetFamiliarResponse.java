package com.pacifico.agenda.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DSB on 01/07/2016.
 */
public class SetFamiliarResponse{
    @SerializedName("IdFamiliar")
    @Expose
    private String idFamiliar;
    @SerializedName("IdFamiliarDispositivo")
    @Expose
    private String idFamiliarDispositivo;
    public String getIdFamiliar() {
        return idFamiliar;
    }
    public String getIdFamiliarDispositivo() {
        return idFamiliarDispositivo;
    }
}