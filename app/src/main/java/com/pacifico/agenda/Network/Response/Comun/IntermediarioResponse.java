package com.pacifico.agenda.Network.Response.Comun;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 6/07/16.
 */
public class IntermediarioResponse{
    @SerializedName("CodigoIntermediario")
    @Expose
    private String codigoIntermediario;
    @SerializedName("NombreRazonSocial")
    @Expose
    private String nombreRazonSocial;
    @SerializedName("TipoDocumento")
    @Expose
    private String tipoDocumento;
    @SerializedName("DocumentoIdentidad")
    @Expose
    private String documentoIdentidad;
    @SerializedName("TelefonoCelular")
    @Expose
    private String telefonoCelular;
    @SerializedName("TelefonoFijo")
    @Expose
    private String telefonoFijo;
    @SerializedName("CorreoElectronico")
    @Expose
    private String correoElectronico;
    @SerializedName("FechaPromocion")
    @Expose
    private String fechaPromocion;
    @SerializedName("NombreGU")
    @Expose
    private String nombreGU;
    @SerializedName("CorreoElectronioGU")
    @Expose
    private String correoElectronioGU;
    @SerializedName("NombreGA")
    @Expose
    private String nombreGA;
    @SerializedName("CorreoElectronioGA")
    @Expose
    private String correoElectronioGA;
    @SerializedName("DescripcionAgencia")
    @Expose
    private String descripcionAgencia;
    @SerializedName("DescripcionOficina")
    @Expose
    private String descripcionOficina;
    @SerializedName("DescripcionCategoria")
    @Expose
    private String descripcionCategoria;
    @SerializedName("CodigoDepartamento")
    @Expose
    private String codigoDepartamento;

    public String getCodigoIntermediario() {
        return codigoIntermediario;
    }

    public String getNombreRazonSocial() {
        return nombreRazonSocial;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public String getDocumentoIdentidad() {
        return documentoIdentidad;
    }

    public String getTelefonoCelular() {
        return telefonoCelular;
    }

    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public String getFechaPromocion() {
        return fechaPromocion;
    }

    public String getNombreGU() {
        return nombreGU;
    }

    public String getCorreoElectronioGU() {
        return correoElectronioGU;
    }

    public String getNombreGA() {
        return nombreGA;
    }

    public String getCorreoElectronioGA() {
        return correoElectronioGA;
    }

    public String getDescripcionAgencia() {
        return descripcionAgencia;
    }

    public String getDescripcionOficina() {
        return descripcionOficina;
    }

    public String getDescripcionCategoria() {
        return descripcionCategoria;
    }

    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }
}