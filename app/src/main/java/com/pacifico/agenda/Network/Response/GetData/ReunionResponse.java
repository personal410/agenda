package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joel on 08/06/2016.
 */
public class ReunionResponse{

    @SerializedName("CodigoIntermediario")
    @Expose
    private String codigoIntermediario;
    @SerializedName("IdReunion")
    @Expose
    private String idReunion;
    @SerializedName("IdReunionDispositivo")
    @Expose
    private String idReunionDispositivo;
    @SerializedName("FechaReunion")
    @Expose
    private String fechaReunion;
    @SerializedName("HoraInicio")
    @Expose
    private String horaInicio;
    @SerializedName("HoraFin")
    @Expose
    private String horaFin;
    @SerializedName("Ubicacion")
    @Expose
    private String ubicacion;
    @SerializedName("FlagInvitadoGU")
    @Expose
    private String flagInvitadoGU;
    @SerializedName("FlagInvitadoGA")
    @Expose
    private String flagInvitadoGA;
    @SerializedName("Recordatorio")
    @Expose
    private String recordatorio;
    @SerializedName("CodigoTipoReunion")
    @Expose
    private int codigoTipoReunion;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String fechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String fechaModificacionDispositivo;

    public String getCodigoIntermediario() {
        return codigoIntermediario;
    }

    public String getIdReunion() {
        return idReunion;
    }

    public String getIdReunionDispositivo() {
        return idReunionDispositivo;
    }

    public String getFechaReunion() {
        return fechaReunion;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getFlagInvitadoGU() {
        return flagInvitadoGU;
    }

    public String getFlagInvitadoGA() {
        return flagInvitadoGA;
    }

    public String getRecordatorio() {
        return recordatorio;
    }

    public int getCodigoTipoReunion() {
        return codigoTipoReunion;
    }

    public String getFechaCreacionDispositivo() {
        return fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return fechaModificacionDispositivo;
    }
}