package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Network.Response.Comun.CalendarioResponse;
import com.pacifico.agenda.Network.Response.Comun.IntermediarioResponse;
import com.pacifico.agenda.Network.Response.Comun.MensajeSistemaResponse;
import com.pacifico.agenda.Network.Response.Comun.ParametroResponse;
import com.pacifico.agenda.Network.Response.Comun.ProcesoSincronizacionResponse;
import com.pacifico.agenda.Network.Response.Comun.TablaIndiceResponse;
import com.pacifico.agenda.Network.Response.Comun.TablaTablasResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vctrls3477 on 6/06/16.
 */
public class GetDataResponse {
    @SerializedName("ProcesoSincronizacion")
    @Expose
    private ProcesoSincronizacionResponse procesoSicronizacion;
    @SerializedName("Parametro")
    @Expose
    private List<ParametroResponse> parametro = new ArrayList<>();
    @SerializedName("TablaIndice")
    @Expose
    private List<TablaIndiceResponse> tablaIndice = new ArrayList<>();
    @SerializedName("TablaTablas")
    @Expose
    private List<TablaTablasResponse> tablaTablas = new ArrayList<>();
    @SerializedName("Calendario")
    @Expose
    private List<CalendarioResponse> calendario = new ArrayList<>();
    @SerializedName("MensajeSistema")
    @Expose
    private List<MensajeSistemaResponse> mensajeSistema = new ArrayList<>();
    @SerializedName("Entidad")
    @Expose
    private List<EntidadResponse> entidad = new ArrayList<>();
    @SerializedName("ConsolidadoIntermediario")
    @Expose
    private List<IntermediarioResponse> consolidadoIntermediario;
    @SerializedName("Reunion")
    @Expose
    private List<ReunionResponse> reunion = new ArrayList<>();
    @SerializedName("Prospecto")
    @Expose
    private List<ProspectoResponse> prospecto = new ArrayList<>();
    @SerializedName("ProspectoMovimientoEtapa")
    @Expose
    private List<ProspectoMovimientoEtapaResponse> prospectoMovimientoEtapa = new ArrayList<>();
    @SerializedName("ADN")
    @Expose
    private List<ADNResponse> adns = new ArrayList<>();
    @SerializedName("Familiar")
    @Expose
    private List<FamiliarResponse> familiar = new ArrayList<>();
    @SerializedName("Cita")
    @Expose
    private List<CitaResponse> cita = new ArrayList<>();
    @SerializedName("CitaMovimientoEstado")
    @Expose
    private List<CitaMovimientoEstadoResponse> citaMovimientoEstado = new ArrayList<>();
    @SerializedName("Referido")
    @Expose
    private List<ReferidoResponse> referido = new ArrayList<>();
    @SerializedName("RecordatorioLlamada")
    @Expose
    private List<RecordatorioLlamadaResponse> recordatorioLlamada = new ArrayList<>();
    @SerializedName("ResultCode")
    @Expose
    private String resultCode;
    @SerializedName("ResultMessage")
    @Expose
    private String resultMessage;
    public ProcesoSincronizacionResponse getProcesoSicronizacion() {
        return procesoSicronizacion;
    }
    public List<ParametroResponse> getParametro() {
        return parametro;
    }
    public List<TablaIndiceResponse> getTablaIndice() {
        return tablaIndice;
    }
    public List<TablaTablasResponse> getTablaTablas() {
        return tablaTablas;
    }
    public List<CalendarioResponse> getCalendario() {
        return calendario;
    }
    public List<MensajeSistemaResponse> getMensajeSistema() {
        return mensajeSistema;
    }
    public List<IntermediarioResponse> getConsolidadoIntermediario() {
        return consolidadoIntermediario;
    }
    public List<ReunionResponse> getReunion() {
        return reunion;
    }
    public List<ProspectoResponse> getProspecto() {
        return prospecto;
    }
    public List<ProspectoMovimientoEtapaResponse> getProspectoMovimientoEtapa() {
        return prospectoMovimientoEtapa;
    }
    public List<ADNResponse> getADN() {
        return adns;
    }
    public List<FamiliarResponse> getFamiliar() {
        return familiar;
    }
    public List<CitaResponse> getCita() {
        return cita;
    }
    public List<CitaMovimientoEstadoResponse> getCitaMovimientoEstado() {
        return citaMovimientoEstado;
    }
    public List<ReferidoResponse> getReferido() {
        return referido;
    }
    public List<RecordatorioLlamadaResponse> getRecordatorioLlamada() {
        return recordatorioLlamada;
    }
    public String getResultCode() {
        return resultCode;
    }
    public String getResultMessage() {
        return resultMessage;
    }
    public List<EntidadResponse> getEntidad() {
        return entidad;
    }

}