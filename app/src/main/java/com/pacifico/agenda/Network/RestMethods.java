package com.pacifico.agenda.Network;

import com.pacifico.agenda.Network.Request.SetData.SetDataRequest;
import com.pacifico.agenda.Network.Request.ValidarToken.ValidarTokenRequest;
import com.pacifico.agenda.Network.Request.ValidarUsuario.ValidarUsuarioRequest;
import com.pacifico.agenda.Network.Response.GetData.GetDataResponse;
import com.pacifico.agenda.Network.Response.SetData.SetDataResponse;
import com.pacifico.agenda.Network.Response.ValidarToken.ValidarTokenResponse;
import com.pacifico.agenda.Network.Response.ValidarUsuario.ValidarUsuarioResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by vctrls3477 on 6/06/16.
 */
public interface RestMethods{
    @POST("/Organizate.Security/Autenticacion.svc/ValidarUsuario")
    Call<ValidarUsuarioResponse> ValidarUsuario(@Body ValidarUsuarioRequest validarUsuarioRequest);
    @POST("/Organizate.Security/Autenticacion.svc/ValidarToken")
    Call<ValidarTokenResponse> ValidarToken(@Body ValidarTokenRequest validarTokenRequest);
    @GET("/OrganizateGet/Get.svc/GetData/{Login}/{CodigoIntermediario}/{TicketSegCen}/{Marca}/{Modelo}/{SistemaOperativo}/{MAC}/{NumeroSerie}")
    Call<GetDataResponse> GetData(@Path("Login") String Login
            , @Path("CodigoIntermediario") String CodigoIntermediario
            , @Path("TicketSegCen") String TicketSegCen
            , @Path("Marca") String Marca
            , @Path("Modelo") String Modelo
            , @Path("SistemaOperativo") String SistemaOperativo
            , @Path("MAC") String MAC
            , @Path("NumeroSerie") String NumeroSerie);
    @POST("/OrganizateSet/Set.svc/SetData")
    Call<SetDataResponse> SetData(@Body SetDataRequest setDataRequest);
}