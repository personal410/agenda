package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by joel on 7/18/16.
 */
public class CitaMovimientoEstadoRequest {
    @SerializedName("IdMovimiento")
    @Expose
    private String IdMovimiento;

    @SerializedName("IdMovimientoDispositivo")
    @Expose
    private String IdMovimientoDispositivo;

    @SerializedName("IdCita")
    @Expose
    private String IdCita;

    @SerializedName("IdCitaDispositivo")
    @Expose
    private String IdCitaDispositivo;

    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;

    @SerializedName("CodigoEstado")
    @Expose
    private String CodigoEstado;
    @SerializedName("CodigoResultado")
    @Expose
    private String CodigoResultado;
    @SerializedName("FechaMovimientoEstadoDispositivo")
    @Expose
    private String FechaMovimientoEstadoDispositivo;

    public void setIdMovimiento(String idMovimiento) {
        IdMovimiento = idMovimiento;
    }

    public void setIdCita(String idCita) {
        IdCita = idCita;
    }

    public void setIdProspectoDispositivo(String idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public void setIdCitaDispositivo(String idCitaDispositivo) {
        IdCitaDispositivo = idCitaDispositivo;
    }

    public void setIdMovimientoDispositivo(String idMovimientoDispositivo) {
        IdMovimientoDispositivo = idMovimientoDispositivo;
    }

    public void setCodigoEstado(String codigoEstado) {
        CodigoEstado = codigoEstado;
    }

    public void setCodigoResultado(String codigoResultado) {
        CodigoResultado = codigoResultado;
    }

    public void setFechaMovimientoEstadoDispositivo(String fechaMovimientoEstadoDispositivo) {
        FechaMovimientoEstadoDispositivo = fechaMovimientoEstadoDispositivo;
    }
}