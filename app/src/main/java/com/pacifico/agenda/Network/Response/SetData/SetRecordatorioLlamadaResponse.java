package com.pacifico.agenda.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 30/07/16.
 */
public class SetRecordatorioLlamadaResponse{
    @SerializedName("IdRecordatorioLlamada")
    @Expose
    private String IdRecordatorioLlamada;
    @SerializedName("IdRecordatorioLlamadaDispositivo")
    @Expose
    private String IdRecordatorioLlamadaDispositivo;

    public String getIdRecordatorioLlamada() {
        return IdRecordatorioLlamada;
    }

    public String getIdRecordatorioLlamadaDispositivo() {
        return IdRecordatorioLlamadaDispositivo;
    }
}