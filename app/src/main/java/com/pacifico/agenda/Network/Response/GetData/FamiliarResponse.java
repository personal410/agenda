package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joel on 08/06/2016.
 */
public class FamiliarResponse {
    @SerializedName("IdFamiliar")
    @Expose
    private String idFamiliar;
    @SerializedName("IdProspecto")
    @Expose
    private String idProspecto;
    @SerializedName("IdFamiliarDispositivo")
    @Expose
    private String idFamiliarDispositivo;
    @SerializedName("Nombres")
    @Expose
    private String nombres;
    @SerializedName("ApellidoPaterno")
    @Expose
    private String apellidoPaterno;
    @SerializedName("ApellidoMaterno")
    @Expose
    private String apellidoMaterno;
    @SerializedName("FechaNacimiento")
    @Expose
    private String fechaNacimiento;
    @SerializedName("Edad")
    @Expose
    private String edad;
    @SerializedName("Ocupacion")
    @Expose
    private String ocupacion;
    @SerializedName("CodigoTipoFamiliar")
    @Expose
    private String codigoTipoFamiliar;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String fechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String fechaModificacionDispositivo;
    @SerializedName("FlagActivo")
    @Expose
    private String flagActivo;

    public String getIdFamiliar() {
        return idFamiliar;
    }

    public String getIdProspecto() {
        return idProspecto;
    }

    public String getIdFamiliarDispositivo() {
        return idFamiliarDispositivo;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getEdad() {
        return edad;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public String getCodigoTipoFamiliar() {
        return codigoTipoFamiliar;
    }

    public String getFechaCreacionDispositivo() {
        return fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return fechaModificacionDispositivo;
    }

    public String getFlagActivo() {
        return flagActivo;
    }
}