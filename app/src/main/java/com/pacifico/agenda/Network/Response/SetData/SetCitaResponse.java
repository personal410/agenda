package com.pacifico.agenda.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by joel on 7/4/16.
 */
public class SetCitaResponse{
    @SerializedName("IdCita")
    @Expose
    private String IdCita;
    @SerializedName("IdCitaDispositivo")
    @Expose
    private String IdCitaDispositivo;

    public String getIdCita() {
        return IdCita;
    }

    public String getIdCitaDispositivo() {
        return IdCitaDispositivo;
    }
}