package com.pacifico.agenda.Network;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.pacifico.agenda.Model.Bean.AdnBean;
import com.pacifico.agenda.Model.Bean.CalendarioBean;
import com.pacifico.agenda.Model.Bean.CitaBean;
import com.pacifico.agenda.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.agenda.Model.Bean.DispositivoBean;
import com.pacifico.agenda.Model.Bean.EntidadBean;
import com.pacifico.agenda.Model.Bean.FamiliarBean;
import com.pacifico.agenda.Model.Bean.IntermediarioBean;
import com.pacifico.agenda.Model.Bean.MensajeSistemaBean;
import com.pacifico.agenda.Model.Bean.ParametroBean;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.agenda.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.agenda.Model.Bean.ReferidoBean;
import com.pacifico.agenda.Model.Bean.ReunionInternaBean;
import com.pacifico.agenda.Model.Bean.TablaIdentificadorBean;
import com.pacifico.agenda.Model.Bean.TablaIndiceBean;
import com.pacifico.agenda.Model.Bean.TablaTablasBean;
import com.pacifico.agenda.Model.Controller.ADNController;
import com.pacifico.agenda.Model.Controller.CitaReunionController;
import com.pacifico.agenda.Model.Controller.DispositivoController;
import com.pacifico.agenda.Model.Controller.EntidadController;
import com.pacifico.agenda.Model.Controller.FamiliarController;
import com.pacifico.agenda.Model.Controller.IntermediarioController;
import com.pacifico.agenda.Model.Controller.ParametroController;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.agenda.Model.Controller.ReferidoController;
import com.pacifico.agenda.Model.Controller.TablaIdentificadorController;
import com.pacifico.agenda.Model.Controller.TablasGeneralesController;
import com.pacifico.agenda.Network.Request.SetData.ADNRequest;
import com.pacifico.agenda.Network.Request.SetData.CitaMovimientoEstadoRequest;
import com.pacifico.agenda.Network.Request.SetData.CitaRequest;
import com.pacifico.agenda.Network.Request.SetData.FamiliarRequest;
import com.pacifico.agenda.Network.Request.SetData.ProspectoMovimientoEtapaRequest;
import com.pacifico.agenda.Network.Request.SetData.ProspectoRequest;
import com.pacifico.agenda.Network.Request.SetData.RecordatorioLLamadaRequest;
import com.pacifico.agenda.Network.Request.SetData.ReferidoRequest;
import com.pacifico.agenda.Network.Request.SetData.RequestDataRequest;
import com.pacifico.agenda.Network.Request.SetData.ReunionInternaRequest;
import com.pacifico.agenda.Network.Request.SetData.SetDataRequest;
import com.pacifico.agenda.Network.Response.Comun.CalendarioResponse;
import com.pacifico.agenda.Network.Response.Comun.IntermediarioResponse;
import com.pacifico.agenda.Network.Response.Comun.MensajeSistemaResponse;
import com.pacifico.agenda.Network.Response.Comun.ParametroResponse;
import com.pacifico.agenda.Network.Response.Comun.ProcesoSincronizacionResponse;
import com.pacifico.agenda.Network.Response.Comun.TablaIndiceResponse;
import com.pacifico.agenda.Network.Response.Comun.TablaTablasResponse;
import com.pacifico.agenda.Network.Response.GetData.EntidadResponse;
import com.pacifico.agenda.Network.Response.GetData.ProspectoResponse;
import com.pacifico.agenda.Network.Response.SetData.SetCitaMovimientoEstadoResponse;
import com.pacifico.agenda.Network.Response.SetData.SetCitaResponse;
import com.pacifico.agenda.Network.Response.SetData.SetDataResponse;
import com.pacifico.agenda.Network.Response.SetData.SetFamiliarResponse;
import com.pacifico.agenda.Network.Response.SetData.SetProspectoMovimientoEtapaResponse;
import com.pacifico.agenda.Network.Response.SetData.SetProspectoResponse;
import com.pacifico.agenda.Network.Response.SetData.SetRecordatorioLlamadaResponse;
import com.pacifico.agenda.Network.Response.SetData.SetReferidoResponse;
import com.pacifico.agenda.Network.Response.SetData.SetReunionInternaResponse;
import com.pacifico.agenda.Persistence.DatabaseConstants;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.DataMapperRequest;
import com.pacifico.agenda.Util.DataMapperResponse;
import com.pacifico.agenda.Util.ObjetoHash;
import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by victorsalazar on 9/08/16.
 */
public class SincronizacionController{
    private RespuestaSincronizacionListener respuestaSincronizacionListener;
    public int sincronizar(Context context){
        int cantObjectosSinEnviar = 0;
        ArrayList<ReunionInternaBean> arrReunionInternas = CitaReunionController.obtenerReunionInternasSinEnviar();
        cantObjectosSinEnviar += arrReunionInternas.size();
        ArrayList<ProspectoBean> arrProspectos = ProspectoController.obtenerProspectosSinEviar();
        cantObjectosSinEnviar += arrProspectos.size();
        ArrayList<ProspectoMovimientoEtapaBean> arrProspectoMovimientoEtapas = ProspectoMovimientoEtapaController.obtenerProspectoMovimientoEtapaSinEnviar();
        cantObjectosSinEnviar += arrProspectoMovimientoEtapas.size();
        final ArrayList<AdnBean> arrAdns = ADNController.obtenerADNSinEviar();
        cantObjectosSinEnviar += arrAdns.size();
        ArrayList<FamiliarBean> arrFamiliares = FamiliarController.obtenerFamiliaresSinEviar();
        cantObjectosSinEnviar += arrFamiliares.size();
        ArrayList<CitaBean> arrCitas = CitaReunionController.obtenerCitasSinEnviar();
        cantObjectosSinEnviar += arrCitas.size();
        ArrayList<CitaMovimientoEstadoBean> arrCitaMovimientoEstados = CitaReunionController.obtenerCitasMovimientoEstadoSinEnviar();
        cantObjectosSinEnviar += arrCitaMovimientoEstados.size();
        ArrayList<ReferidoBean> arrReferidos = ReferidoController.obtenerReferidosSinEnviar();
        cantObjectosSinEnviar += arrReferidos.size();
        ArrayList<RecordatorioLlamadaBean> arrRecordatorioLlamadas = CitaReunionController.obtenerRecordatorioLlamadasSinEnviar();
        cantObjectosSinEnviar += arrRecordatorioLlamadas.size();
        if(cantObjectosSinEnviar == 0){
            return 0;
        }
        ArrayList<ReunionInternaRequest> arrReunionInternasRequest = DataMapperRequest.transformarListaReunion(arrReunionInternas);
        ArrayList<ProspectoRequest> arrProspectoRequest = DataMapperRequest.transformarListaProspecto(arrProspectos);
        ArrayList<ProspectoMovimientoEtapaRequest> arrProspectoMovimientoEtapasRequest = DataMapperRequest.transformarListaProspectoMovimientoEtapa(arrProspectoMovimientoEtapas);
        final ArrayList<ADNRequest> arrAdnRequest = DataMapperRequest.transformarListaAdn(arrAdns);
        ArrayList<FamiliarRequest> arrFamiliarRequest = DataMapperRequest.transformarListaFamiliar(arrFamiliares);
        ArrayList<CitaRequest> arrCitasRequest = DataMapperRequest.transformarListaCita(arrCitas);
        ArrayList<CitaMovimientoEstadoRequest> arrCitaMovimientoEstadosRequest = DataMapperRequest.transformarListaCitaMovimientoEstado(arrCitaMovimientoEstados);
        ArrayList<ReferidoRequest> arrReferidoRequest = DataMapperRequest.transformListaReferidos(arrReferidos);
        ArrayList<RecordatorioLLamadaRequest> arrRecordatorioLlamadasRequest = DataMapperRequest.transformarListaRecordatorioLlamada(arrRecordatorioLlamadas);

        ObjetoHash objetoHash = new ObjetoHash();
        objetoHash.setReunion(arrReunionInternasRequest);
        objetoHash.setProspecto(arrProspectoRequest);
        objetoHash.setProspectoMovimientoEtapa(arrProspectoMovimientoEtapasRequest);
        objetoHash.setADN(arrAdnRequest);
        objetoHash.setFamiliar(arrFamiliarRequest);
        objetoHash.setCita(arrCitasRequest);
        objetoHash.setCitaMovimientoEstado(arrCitaMovimientoEstadosRequest);
        objetoHash.setReferido(arrReferidoRequest);
        objetoHash.setRecordatorioLlamada(arrRecordatorioLlamadasRequest);

        Gson gson = new Gson();
        String string = gson.toJson(objetoHash);
        Log.i("TAG", "json: " + string);
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(string.getBytes(StandardCharsets.UTF_8));
            StringBuffer hexString = new StringBuffer();
            for(int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            Log.i("TAG", "hash: " + hexString);
            OkHttpClient okHttpClient = new OkHttpClient();
            try{
                okHttpClient.setSslSocketFactory(getSSLSocketFactory(context));
            }catch(Exception e){}
            Retrofit retrofit = new Retrofit.Builder().baseUrl(Constantes.URLBase)
                    .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
            RestMethods restMethods = retrofit.create(RestMethods.class);
            RequestDataRequest requestDataRequest = new RequestDataRequest();
            requestDataRequest.setHashDeEnvio(hexString.toString());
            requestDataRequest.setListaReunion(arrReunionInternasRequest);
            requestDataRequest.setListaProspectos(arrProspectoRequest);
            requestDataRequest.setListaProspectosMovimientoEtapa(arrProspectoMovimientoEtapasRequest);
            requestDataRequest.setListaADNs(arrAdnRequest);
            requestDataRequest.setListaFamiliares(arrFamiliarRequest);
            requestDataRequest.setListaCitas(arrCitasRequest);
            requestDataRequest.setListaCitaMovimientoEstados(arrCitaMovimientoEstadosRequest);
            requestDataRequest.setListaReferidos(arrReferidoRequest);
            requestDataRequest.setListaRecordatorioLlamadas(arrRecordatorioLlamadasRequest);
            final SetDataRequest setDataRequest = new SetDataRequest();
            setDataRequest.setRequestData(requestDataRequest);
            String json2 = gson.toJson(setDataRequest);
            Log.i("TAG", "json: " + json2);
            Call<SetDataResponse> respuesta = restMethods.SetData(setDataRequest);
            respuesta.enqueue(new Callback<SetDataResponse>() {
                @Override
                public void onResponse(Response<SetDataResponse> response, Retrofit retrofit){
                    SetDataResponse dataResponse = response.body();
                    if(dataResponse == null){
                        if(respuestaSincronizacionListener != null){
                            respuestaSincronizacionListener.terminoSincronizacion(-2, "Hubo un error en el servidor");
                        }
                    }else{
                        ProcesoSincronizacionResponse procesoSincronizacionResponse = dataResponse.getProcesoSincronizacion();
                        Log.i("TAG", "flagExito: " + procesoSincronizacionResponse.getFlagExito());
                        if(procesoSincronizacionResponse.getFlagExito().equals("true")){
                            DispositivoBean dispositivoBean = DispositivoController.obtenerDispositivoPorIdDispositivo(1);
                            dispositivoBean.setFechaUltimaSincronizacion(procesoSincronizacionResponse.getFechaSincronizacion());
                            DispositivoController.guardarDispositivo(dispositivoBean);
                            List<SetReunionInternaResponse> arrReunion = dataResponse.getArrReunionInterna();
                            for(SetReunionInternaResponse reunionInternaResponse : arrReunion){
                                ReunionInternaBean reunionInternaBean = CitaReunionController.obtenerReunionInterPorIdReunionInternaDispositivo(Integer.parseInt(reunionInternaResponse.getIdReunionDispositivo()));
                                reunionInternaBean.setIdReunionInterna(Integer.parseInt(reunionInternaResponse.getIdReunion()));
                                reunionInternaBean.setFlagEnviado(2);
                                CitaReunionController.actualizarReunionIntera(reunionInternaBean);
                            }
                            List<SetProspectoResponse> arrProspectos = dataResponse.getArrProspecto();
                            for(SetProspectoResponse prospectoResponse : arrProspectos){
                                ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(Integer.parseInt(prospectoResponse.getIdProspectoDispositivo()));
                                prospectoBean.setIdProspecto(Integer.parseInt(prospectoResponse.getIdProspecto()));
                                prospectoBean.setFlagEnviado(2);
                                prospectoBean.setFlagEnviarADNDigital(0);
                                ProspectoController.actualizarProspecto(prospectoBean);
                            }
                            List<SetProspectoMovimientoEtapaResponse> arrProspectoMovimientoEtapas = dataResponse.getArrProspectoMovimientoEtapa();
                            if(arrProspectoMovimientoEtapas != null){
                                for(SetProspectoMovimientoEtapaResponse prospectoMovimientoEtapaResponse : arrProspectoMovimientoEtapas){
                                    ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean = ProspectoMovimientoEtapaController.obtenerProspectoMovimientoEtapaPorIdMovimientoDispositivo(Integer.parseInt(prospectoMovimientoEtapaResponse.getIdMovimientoDispositivo()));
                                    prospectoMovimientoEtapaBean.setIdMovimiento(Integer.parseInt(prospectoMovimientoEtapaResponse.getIdMovimiento()));
                                    prospectoMovimientoEtapaBean.setFlagEnviado(2);
                                    if(prospectoMovimientoEtapaBean.getIdProspecto() == -1){
                                        ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(prospectoMovimientoEtapaBean.getIdProspectoDispositivo());
                                        prospectoMovimientoEtapaBean.setIdProspecto(prospectoBean.getIdProspecto());
                                    }
                                    ProspectoMovimientoEtapaController.actualizarProspectoMovimientoEtapa(prospectoMovimientoEtapaBean);
                                }
                            }
                            for(AdnBean adnBean : arrAdns){
                                adnBean.setFlagEnviado(2);
                                if(adnBean.getIdProspecto() == -1){
                                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(adnBean.getIdProspectoDispositivo());
                                    adnBean.setIdProspecto(prospectoBean.getIdProspecto());
                                }
                                ADNController.actualizarADN(adnBean);
                            }
                            List<SetFamiliarResponse> arrFamiliares = dataResponse.getArrFamiliar();
                            for(SetFamiliarResponse familiarResponse : arrFamiliares){
                                FamiliarBean familiarBean = FamiliarController.obtenerFamiliarPorIdFamiliar(Integer.parseInt(familiarResponse.getIdFamiliarDispositivo()));
                                familiarBean.setIdFamiliar(Integer.parseInt(familiarResponse.getIdFamiliar()));
                                if(familiarBean.getIdProspecto() == -1){
                                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(familiarBean.getIdProspectoDispositivo());
                                    familiarBean.setIdProspecto(prospectoBean.getIdProspecto());
                                }
                                familiarBean.setFlagEnviado(2);
                                FamiliarController.actualizarFamiliar(familiarBean);
                            }
                            List<SetCitaResponse> arrCitas = dataResponse.getArrCita();
                            for(SetCitaResponse citaResponse : arrCitas){
                                CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCitaDispositivo(Integer.parseInt(citaResponse.getIdCitaDispositivo()));
                                citaBean.setIdCita(Integer.parseInt(citaResponse.getIdCita()));
                                citaBean.setFlagEnviado(2);
                                if(citaBean.getIdProspecto() == -1){
                                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(citaBean.getIdProspectoDispositivo());
                                    citaBean.setIdProspecto(prospectoBean.getIdProspecto());
                                }
                                CitaReunionController.actualizarCita(citaBean);
                            }
                            List<SetCitaMovimientoEstadoResponse> arrCitaMovimientoEstados = dataResponse.getArrCitaMovimientoEstado();
                            for(SetCitaMovimientoEstadoResponse citaMovimientoEstadoResponse : arrCitaMovimientoEstados){
                                CitaMovimientoEstadoBean citaMovimientoEstadoBean = CitaReunionController.obtenerCitaMovimientoEstadoPorIdMovimientoDispositivo(Integer.parseInt(citaMovimientoEstadoResponse.getIdMovimientoDispositivo()));
                                citaMovimientoEstadoBean.setIdMovimiento(Integer.parseInt(citaMovimientoEstadoResponse.getIdMovimiento()));
                                citaMovimientoEstadoBean.setFlagEnviado(2);
                                if(citaMovimientoEstadoBean.getIdCita() == -1){
                                    CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCitaDispositivo(citaMovimientoEstadoBean.getIdCitaDispositivo());
                                    citaMovimientoEstadoBean.setIdCita(citaBean.getIdCita());
                                }
                                CitaReunionController.actualizarCitaMovimientoEstado(citaMovimientoEstadoBean);
                            }
                            List<SetReferidoResponse> arrReferido = dataResponse.getArrReferido();
                            for(SetReferidoResponse referidoResponse : arrReferido){
                                ReferidoBean referidoBean = ReferidoController.obtenerReferidoPorIdReferidoDispositivo(Integer.parseInt(referidoResponse.getIdReferidoDispositivo()));
                                referidoBean.setIdReferido(Integer.parseInt(referidoResponse.getIdReferido()));
                                if(referidoBean.getIdProspecto() == -1){
                                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(referidoBean.getIdProspectoDispositivo());
                                    referidoBean.setIdProspecto(prospectoBean.getIdProspecto());
                                }
                                referidoBean.setFlagEnviado(2);
                                ReferidoController.actualizarReferido(referidoBean);
                            }
                            List<SetRecordatorioLlamadaResponse> arrRecordatorioLlamadas = dataResponse.getArrRecordatorioLlamada();
                            for(SetRecordatorioLlamadaResponse recordatorioLlamadaResponse : arrRecordatorioLlamadas){
                                RecordatorioLlamadaBean recordatorioLlamadaBean = CitaReunionController.obtenerRecordatorioLlamadaPorIdRecordatorioLlamadaDispositivo(Integer.parseInt(recordatorioLlamadaResponse.getIdRecordatorioLlamadaDispositivo()));
                                recordatorioLlamadaBean.setIdRecordatorioLlamada(Integer.parseInt(recordatorioLlamadaResponse.getIdRecordatorioLlamada()));
                                recordatorioLlamadaBean.setFlagEnviado(2);
                                if(recordatorioLlamadaBean.getIdCita() == -1 && recordatorioLlamadaBean.getIdCitaDispositivo() != -1){ // No siempre un recordatorio pertenece a una cita
                                    CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCitaDispositivo(recordatorioLlamadaBean.getIdCitaDispositivo());
                                        recordatorioLlamadaBean.setIdCita(citaBean.getIdCita());
                                }
                                if(recordatorioLlamadaBean.getIdProspecto() == -1){
                                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(recordatorioLlamadaBean.getIdProspectoDispositivo());
                                    recordatorioLlamadaBean.setIdProspecto(prospectoBean.getIdProspecto());
                                }
                                CitaReunionController.actualizarRecordatorioLlamada(recordatorioLlamadaBean);
                            }
                            List<ParametroResponse> arrParametros = dataResponse.getArrParametro();
                            for(ParametroResponse parametroResponse : arrParametros){
                                int idParametro = Integer.parseInt(parametroResponse.getIdParametro());
                                ParametroBean parametroBean = ParametroController.obtenerParametroBeanPorIdParametro(idParametro);
                                if(parametroBean == null){
                                    parametroBean = new ParametroBean();
                                    parametroBean.setIdParametro(idParametro);
                                    parametroBean.setDescripcion(parametroResponse.getDescripcion());
                                    parametroBean.setValorCadena(parametroResponse.getValorCadena());
                                    parametroBean.setValorNumerico(Double.parseDouble(parametroResponse.getValorNumerico()));
                                    ParametroController.guardarParametro(parametroBean);
                                }else{
                                    parametroBean.setDescripcion(parametroResponse.getDescripcion());
                                    parametroBean.setValorCadena(parametroResponse.getValorCadena());
                                    parametroBean.setValorNumerico(Double.parseDouble(parametroResponse.getValorNumerico()));
                                    ParametroController.actualizarParametro(parametroBean);
                                }
                            }
                            List<TablaIndiceResponse> arrTablaIndices = dataResponse.getArrTablaIndice();
                            for(TablaIndiceResponse tablaIndiceResponse : arrTablaIndices){
                                int idTabla = Integer.parseInt(tablaIndiceResponse.getIdTabla());
                                TablaIndiceBean tablaIndiceBean = TablasGeneralesController.obtenerTablaIndicePorIdTabla(idTabla);
                                if(tablaIndiceBean == null){
                                    tablaIndiceBean = new TablaIndiceBean();
                                    tablaIndiceBean.setIdTabla(idTabla);
                                    tablaIndiceBean.setNombreTabla(tablaIndiceResponse.getNombreTabla());
                                    TablasGeneralesController.guardarTablaIndice(tablaIndiceBean);
                                }else{
                                    tablaIndiceBean.setNombreTabla(tablaIndiceResponse.getNombreTabla());
                                    TablasGeneralesController.actualizarTablaIndice(tablaIndiceBean);
                                }
                            }
                            List<TablaTablasResponse> arrTablaTablas = dataResponse.getArrTablaTablas();
                            for(TablaTablasResponse tablaTablasResponse : arrTablaTablas){
                                int idTabla = Integer.parseInt(tablaTablasResponse.getIdTabla());
                                int codigoCampo = Integer.parseInt(tablaTablasResponse.getCodigoCampo());
                                TablaTablasBean tablaTablasBean = TablasGeneralesController.obtenerTablaTablasPorIdTablaCodigoCampo(idTabla, codigoCampo);
                                if(tablaTablasBean == null){
                                    tablaTablasBean = new TablaTablasBean();
                                    tablaTablasBean.setIdTabla(idTabla);
                                    tablaTablasBean.setCodigoCampo(codigoCampo);
                                    tablaTablasBean.setValorCadena(tablaTablasResponse.getValorCadena());
                                    tablaTablasBean.setValorNumerico(Double.parseDouble(tablaTablasResponse.getValorNumerico()));
                                    tablaTablasBean.setFlagActivo(Integer.parseInt(tablaTablasResponse.getFlagActivo()));
                                    TablasGeneralesController.guardarTablaTablas(tablaTablasBean);
                                }else{
                                    tablaTablasBean.setValorCadena(tablaTablasResponse.getValorCadena());
                                    tablaTablasBean.setValorNumerico(Double.parseDouble(tablaTablasResponse.getValorNumerico()));
                                    tablaTablasBean.setFlagActivo(Integer.parseInt(tablaTablasResponse.getFlagActivo()));
                                    TablasGeneralesController.actualizarTablaTablas(tablaTablasBean);
                                }
                            }
                            List<CalendarioResponse> arrCalendarios = dataResponse.getArrCalendario();
                            for(CalendarioResponse calendarioResponse : arrCalendarios){
                                int idCalendario = Integer.parseInt(calendarioResponse.getIdCalendario());
                                CalendarioBean calendarioBean = TablasGeneralesController.obtenerCalendarioPorIdCalendario(idCalendario);
                                if(calendarioBean == null){
                                    calendarioBean = new CalendarioBean();
                                    calendarioBean.setIdCalendario(idCalendario);
                                    calendarioBean.setAnio(Integer.parseInt(calendarioResponse.getAnio()));
                                    calendarioBean.setSemanaAno(Integer.parseInt(calendarioResponse.getSemanaAno()));
                                    calendarioBean.setMesAno(Integer.parseInt(calendarioResponse.getMesAnio()));
                                    calendarioBean.setSemanaMes(Integer.parseInt(calendarioResponse.getSemanaMes()));
                                    calendarioBean.setFechaInicioSemana(calendarioResponse.getFechaInicioSemana());
                                    calendarioBean.setFechaFinSemana(calendarioResponse.getFechaFinSemana());
                                    TablasGeneralesController.guardarCalendario(calendarioBean);
                                }else{
                                    calendarioBean.setAnio(Integer.parseInt(calendarioResponse.getAnio()));
                                    calendarioBean.setSemanaAno(Integer.parseInt(calendarioResponse.getSemanaAno()));
                                    calendarioBean.setMesAno(Integer.parseInt(calendarioResponse.getMesAnio()));
                                    calendarioBean.setSemanaMes(Integer.parseInt(calendarioResponse.getSemanaMes()));
                                    calendarioBean.setFechaInicioSemana(calendarioResponse.getFechaInicioSemana());
                                    calendarioBean.setFechaFinSemana(calendarioResponse.getFechaFinSemana());
                                    TablasGeneralesController.actualizarCalendario(calendarioBean);
                                }
                            }
                            List<MensajeSistemaResponse> arrMensajeSistemas = dataResponse.getArrMensajeSistema();
                            for(MensajeSistemaResponse mensajeSistemaResponse : arrMensajeSistemas){
                                int idMensajeSistema = Integer.parseInt(mensajeSistemaResponse.getIdMensajeSistema());
                                MensajeSistemaBean mensajeSistemaBean = TablasGeneralesController.obtenerMensajeSistemaPorIdMensajeSistema(idMensajeSistema);
                                if(mensajeSistemaBean == null){
                                    mensajeSistemaBean = new MensajeSistemaBean();
                                    mensajeSistemaBean.setIdMensajeSistema(idMensajeSistema);
                                    mensajeSistemaBean.setDescripcionMensaje(mensajeSistemaResponse.getDescripcionMensaje());
                                    mensajeSistemaBean.setFechaInicioVigencia(mensajeSistemaResponse.getFechaInicioVigencia());
                                    mensajeSistemaBean.setFechaFinVigencia(mensajeSistemaResponse.getFechaFinVigencia());
                                    TablasGeneralesController.guardarMensajeSistema(mensajeSistemaBean);
                                }else{
                                    mensajeSistemaBean.setDescripcionMensaje(mensajeSistemaResponse.getDescripcionMensaje());
                                    mensajeSistemaBean.setFechaInicioVigencia(mensajeSistemaResponse.getFechaInicioVigencia());
                                    mensajeSistemaBean.setFechaFinVigencia(mensajeSistemaResponse.getFechaFinVigencia());
                                    TablasGeneralesController.actualizarMensajeSistema(mensajeSistemaBean);
                                }
                            }
                            List<EntidadResponse> arrEntidades = dataResponse.getArrEntidad();
                            for(EntidadResponse entidadResponse : arrEntidades){
                                int idEntidad = Integer.parseInt(entidadResponse.getIdEntidad());
                                EntidadBean entidadBean = EntidadController.obtenerEntidadPorIdEntidad(idEntidad);
                                if(entidadBean == null){
                                    entidadBean = new EntidadBean();
                                    entidadBean.setIdEntidad(idEntidad);
                                    entidadBean.setDescripcion(entidadResponse.getDescripcion());
                                    entidadBean.setImagen(entidadResponse.getImagen());
                                    entidadBean.setCodigoTipoCobranza(Integer.parseInt(entidadResponse.getCodigoTipoCobranza()));
                                    EntidadController.guardarEntidad(entidadBean);
                                }else{
                                    entidadBean.setDescripcion(entidadResponse.getDescripcion());
                                    entidadBean.setImagen(entidadResponse.getImagen());
                                    entidadBean.setCodigoTipoCobranza(Integer.parseInt(entidadResponse.getCodigoTipoCobranza()));
                                    EntidadController.actualizarEntidad(entidadBean);
                                }
                            }
                            List<IntermediarioResponse> arrIntermediarios = dataResponse.getArrIntermediario();
                            if(arrIntermediarios.size() > 0){
                                IntermediarioResponse intermediarioResponse = arrIntermediarios.get(0);
                                IntermediarioBean intermediarioBean = IntermediarioController.obtenerIntermediario();
                                intermediarioBean.setCodigoIntermediario(Integer.parseInt(intermediarioResponse.getCodigoIntermediario()));
                                intermediarioBean.setNombreRazonSocial(intermediarioResponse.getNombreRazonSocial());
                                intermediarioBean.setTipoDocumento(intermediarioResponse.getTipoDocumento());
                                intermediarioBean.setDocumentoIdentidad(intermediarioResponse.getDocumentoIdentidad());
                                intermediarioBean.setTelefonoCelular(intermediarioResponse.getTelefonoCelular());
                                intermediarioBean.setTelefonoFijo(intermediarioResponse.getTelefonoFijo());
                                intermediarioBean.setCorreoElectronico(intermediarioResponse.getCorreoElectronico());
                                intermediarioBean.setFechaPromocion(intermediarioResponse.getFechaPromocion());
                                intermediarioBean.setNombreGU(intermediarioResponse.getNombreGU());
                                intermediarioBean.setCorreoElectronicoGU(intermediarioResponse.getCorreoElectronioGU());
                                intermediarioBean.setNombreGA(intermediarioResponse.getNombreGA());
                                intermediarioBean.setCorreoElectronicoGA(intermediarioResponse.getCorreoElectronioGA());
                                intermediarioBean.setDescripcionAgencia(intermediarioResponse.getDescripcionAgencia());
                                intermediarioBean.setDescripcionOficina(intermediarioResponse.getDescripcionOficina());
                                intermediarioBean.setDescripcionCategoria(intermediarioResponse.getDescripcionCategoria());
                                intermediarioBean.setCodigoDepartamento(Integer.parseInt(intermediarioResponse.getCodigoDepartamento()));
                                IntermediarioController.guardarIntermediario(intermediarioBean);
                            }
                            List<ProspectoResponse> arrProspectoExternos = dataResponse.getArrProspectoExterno();
                            ArrayList<ProspectoBean> arrProspectoExternosBean = DataMapperResponse.transformListProspecto(arrProspectoExternos);
                            TablaIdentificadorBean prospectoIdentificadorBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_PROSPECTO);
                            int idProspectoDispositivo = prospectoIdentificadorBean.getIdentity();
                            for(ProspectoBean prospectoBean : arrProspectoExternosBean){
                                if(prospectoBean.getIdProspectoDispositivo() == -1){
                                    idProspectoDispositivo++;
                                    prospectoBean.setIdProspectoDispositivo(idProspectoDispositivo);
                                    prospectoBean.setFlagEnviado(0);
                                }
                                ProspectoController.guardarProspecto(prospectoBean);
                            }
                            if(respuestaSincronizacionListener != null){
                                respuestaSincronizacionListener.terminoSincronizacion(Integer.parseInt(dataResponse.getResultCode()), null);
                            }
                        }else{
                            if(respuestaSincronizacionListener != null){
                                respuestaSincronizacionListener.terminoSincronizacion(Integer.parseInt(dataResponse.getResultCode()), dataResponse.getResultMessage());
                            }
                        }
                    }
                }
                @Override
                public void onFailure(Throwable t){
                    if(respuestaSincronizacionListener != null){
                        respuestaSincronizacionListener.terminoSincronizacion(-1, "Se guardaron los datos sin sincronizar");
                    }
                }
            });
        }catch (Exception e){
            return 2;
        }
        return 1;
    }
    public void setRespuestaSincronizacionListener(RespuestaSincronizacionListener respuestaSincronizacionListener){
        this.respuestaSincronizacionListener = respuestaSincronizacionListener;
    }
    private TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return originalTrustManager.getAcceptedIssuers();
                    }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        try {
                            originalTrustManager.checkClientTrusted(certs, authType);
                        } catch (CertificateException ignored) {
                        }
                    }
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        try {
                            originalTrustManager.checkServerTrusted(certs, authType);
                        } catch (CertificateException ignored) {
                        }
                    }
                }
        };
    }
    public SSLSocketFactory getSSLSocketFactory(Context context)
            throws CertificateException, KeyStoreException, IOException,
            NoSuchAlgorithmException, KeyManagementException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = context.getResources().openRawResource(R.raw.pacifico_x509); // your certificate file
        Certificate ca = cf.generateCertificate(caInput);
        caInput.close();
        KeyStore keyStore = KeyStore.getInstance("BKS");
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);
        TrustManager[] wrappedTrustManagers = getWrappedTrustManagers(tmf.getTrustManagers());
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, wrappedTrustManagers, null);
        return sslContext.getSocketFactory();
    }
}