package com.pacifico.agenda.Network.Response.Comun;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 6/06/16.
 */
public class TablaTablasResponse {
    @SerializedName("IdTabla")
    @Expose
    private String idTabla;
    @SerializedName("CodigoCampo")
    @Expose
    private String codigoCampo;
    @SerializedName("ValorCadena")
    @Expose
    private String valorCadena;
    @SerializedName("ValorNumerico")
    @Expose
    private String valorNumerico;
    @SerializedName("FlagActivo")
    @Expose
    private String flagActivo;
    public String getIdTabla() {
        return idTabla;
    }

    public String getCodigoCampo() {
        return codigoCampo;
    }

    public String getValorCadena() {
        return valorCadena;
    }

    public String getValorNumerico() {
        return valorNumerico;
    }

    public String getFlagActivo() {
        return flagActivo;
    }

}