package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 6/06/16.
 */
public class ProspectoResponse{
    @SerializedName("IdProspecto")
    @Expose
    private String idProspecto;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String idProspectoDispositivo;
    @SerializedName("IdProspectoExterno")
    @Expose
    private String idProspectoExterno;
    @SerializedName("Nombres")
    @Expose
    private String nombres;
    @SerializedName("ApellidoPaterno")
    @Expose
    private String apellidoPaterno;
    @SerializedName("ApellidoMaterno")
    @Expose
    private String apellidoMaterno;
    @SerializedName("CodigoSexo")
    @Expose
    private String codigoSexo;
    @SerializedName("CodigoEstadoCivil")
    @Expose
    private String codigoEstadoCivil;
    @SerializedName("FechaNacimiento")
    @Expose
    private String fechaNacimiento;
    @SerializedName("CodigoRangoEdad")
    @Expose
    private String codigoRangoEdad;
    @SerializedName("CodigoRangoIngreso")
    @Expose
    private String codigoRangoIngreso;
    @SerializedName("FlagHijo")
    @Expose
    private String flagHijo;
    @SerializedName("FlagConyuge")
    @Expose
    private String flagConyuge;
    @SerializedName("CodigoTipoDocumento")
    @Expose
    private String codigoTipoDocumento;
    @SerializedName("NumeroDocumento")
    @Expose
    private String numeroDocumento;
    @SerializedName("TelefonoCelular")
    @Expose
    private String telefonoCelular;
    @SerializedName("TelefonoFijo")
    @Expose
    private String telefonoFijo;
    @SerializedName("TelefonoAdicional")
    @Expose
    private String telefonoAdicional;
    @SerializedName("CorreoElectronico1")
    @Expose
    private String correoElectronico1;
    @SerializedName("CorreoElectronico2")
    @Expose
    private String correoElectronico2;
    @SerializedName("CodigoNacionalidad")
    @Expose
    private String codigoNacionalidad;
    @SerializedName("CondicionFumador")
    @Expose
    private String condicionFumador;
    @SerializedName("Empresa")
    @Expose
    private String empresa;
    @SerializedName("Cargo")
    @Expose
    private String cargo;
    @SerializedName("Nota")
    @Expose
    private String nota;
    @SerializedName("FlagEnviarADNDigital")
    @Expose
    private String flagEnviarADNDigital;
    @SerializedName("CodigoEtapa")
    @Expose
    private String codigoEtapa;
    @SerializedName("CodigoEstado")
    @Expose
    private String codigoEstado;
    @SerializedName("CodigoFuente")
    @Expose
    private String codigoFuente;
    @SerializedName("CodigoEntorno")
    @Expose
    private String codigoEntorno;
    @SerializedName("IdReferenciador")
    @Expose
    private String idReferenciador;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String fechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String fechaModificacionDispositivo;
    @SerializedName("AdicionalTexto1")
    @Expose
    private String adicionalTexto1;
    @SerializedName("AdicionalNumerico1")
    @Expose
    private String adicionalNumerico1;
    @SerializedName("AdicionalTexto2")
    @Expose
    private String adicionalTexto2;
    @SerializedName("AdicionalNumerico2")
    @Expose
    private String adicionalNumerico2;

    public String getIdProspecto() {
        return idProspecto;
    }

    public String getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public String getIdProspectoExterno() {
        return idProspectoExterno;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public String getCodigoSexo() {
        return codigoSexo;
    }

    public String getCodigoEstadoCivil() {
        return codigoEstadoCivil;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getCodigoRangoEdad() {
        return codigoRangoEdad;
    }

    public String getCodigoRangoIngreso() {
        return codigoRangoIngreso;
    }

    public String getFlagHijo() {
        return flagHijo;
    }

    public String getFlagConyuge() {
        return flagConyuge;
    }

    public String getCodigoTipoDocumento() {
        return codigoTipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public String getTelefonoCelular() {
        return telefonoCelular;
    }

    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public String getTelefonoAdicional() {
        return telefonoAdicional;
    }

    public String getCorreoElectronico1() {
        return correoElectronico1;
    }

    public String getCorreoElectronico2() {
        return correoElectronico2;
    }

    public String getCodigoNacionalidad() {
        return codigoNacionalidad;
    }

    public String getCondicionFumador() {
        return condicionFumador;
    }

    public String getEmpresa() {
        return empresa;
    }

    public String getCargo() {
        return cargo;
    }

    public String getNota() {
        return nota;
    }

    public String getFlagEnviarADNDigital() {
        return flagEnviarADNDigital;
    }

    public String getCodigoEtapa() {
        return codigoEtapa;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public String getCodigoFuente() {
        return codigoFuente;
    }

    public String getCodigoEntorno() {
        return codigoEntorno;
    }

    public String getIdReferenciador() {
        return idReferenciador;
    }

    public String getFechaCreacionDispositivo() {
        return fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return fechaModificacionDispositivo;
    }

    public String getAdicionalTexto1() {
        return adicionalTexto1;
    }

    public String getAdicionalNumerico1() {
        return adicionalNumerico1;
    }

    public String getAdicionalTexto2() {
        return adicionalTexto2;
    }

    public String getAdicionalNumerico2() {
        return adicionalNumerico2;
    }
}