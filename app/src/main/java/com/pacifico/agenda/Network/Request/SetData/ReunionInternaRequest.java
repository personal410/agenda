package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReunionInternaRequest {
    @SerializedName("IdReunion")
    @Expose
    private
    String IdReunion;
    @SerializedName("IdReunionDispositivo")
    @Expose
    private
    String IdReunionDispositivo;
    @SerializedName("FechaReunion")
    @Expose
    private
    String FechaReunion;
    @SerializedName("HoraInicio")
    @Expose
    private
    String HoraInicio;
    @SerializedName("HoraFin")
    @Expose
    private
    String HoraFin;
    @SerializedName("Ubicacion")
    @Expose
    private
    String Ubicacion;
    @SerializedName("FlagInvitadoGU")
    @Expose
    private
    String FlagInvitadoGU;
    @SerializedName("FlagInvitadoGA")
    @Expose
    private
    String FlagInvitadoGA;
    @SerializedName("Recordatorio")
    @Expose
    private
    String Recordatorio;
    @SerializedName("CodigoTipoReunion")
    @Expose
    private
    String CodigoTipoReunion;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private
    String FechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private
    String FechaModificacionDispositivo;

    public void setIdReunion(String idReunion) {
        IdReunion = idReunion;
    }

    public void setIdReunionDispositivo(String idReunionDispositivo) {
        IdReunionDispositivo = idReunionDispositivo;
    }

    public void setFechaReunion(String fechaReunion) {
        FechaReunion = fechaReunion;
    }

    public void setHoraInicio(String horaInicio) {
        HoraInicio = horaInicio;
    }

    public void setHoraFin(String horaFin) {
        HoraFin = horaFin;
    }

    public void setUbicacion(String ubicacion) {
        Ubicacion = ubicacion;
    }

    public void setFlagInvitadoGU(String flagInvitadoGU) {
        FlagInvitadoGU = flagInvitadoGU;
    }

    public void setFlagInvitadoGA(String flagInvitadoGA) {
        FlagInvitadoGA = flagInvitadoGA;
    }

    public void setRecordatorio(String recordatorio) {
        Recordatorio = recordatorio;
    }

    public void setCodigoTipoReunion(String codigoTipoReunion) {
        CodigoTipoReunion = codigoTipoReunion;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }
}