package com.pacifico.agenda.Network.Response.Comun;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DSB on 01/07/2016.
 */
public class ProcesoSincronizacionResponse {
    @SerializedName("FechaSincronizacion")
    @Expose
    private String FechaSincronizacion;
    @SerializedName("MensajeSincronizacion")
    @Expose
    private String MensajeSincronizacion;
    @SerializedName("FlagExito")
    @Expose
    private String FlagExito;
    public String getFechaSincronizacion() {
        return FechaSincronizacion;
    }
    public String getMensajeSincronizacion() {
        return MensajeSincronizacion;
    }
    public String getFlagExito() {
        return FlagExito;
    }
}