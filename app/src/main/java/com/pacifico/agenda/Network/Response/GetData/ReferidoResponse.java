package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 6/07/16.
 */
public class ReferidoResponse{
    @SerializedName("IdReferido")
    @Expose
    private String idReferido;
    @SerializedName("IdProspecto")
    @Expose
    private String idProspecto;
    @SerializedName("IdReferidoDispositivo")
    @Expose
    private String idReferidoDispositivo;
    @SerializedName("CodigoTipoReferido")
    @Expose
    private String codigoTipoReferido;
    @SerializedName("Nombres")
    @Expose
    private String nombres;
    @SerializedName("ApellidoPaterno")
    @Expose
    private String apellidoPaterno;
    @SerializedName("ApellidoMaterno")
    @Expose
    private String apellidoMaterno;
    @SerializedName("CodigoRangoEdad")
    @Expose
    private String codigoRangoEdad;
    @SerializedName("FlagHijo")
    @Expose
    private String flagHijo;
    @SerializedName("CodigoRangoIngreso")
    @Expose
    private String codigoRangoIngreso;
    @SerializedName("Telefono")
    @Expose
    private String telefono;
    @SerializedName("FechaCreaContacto")
    @Expose
    private String fechaCreaContacto;
    @SerializedName("CodigoTipoEmpleo")
    @Expose
    private String codigoTipoEmpleo;
    @SerializedName("FlagActivo")
    @Expose
    private String flagActivo;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String fechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String fechaModificacionDispositivo;

    public String getIdReferido() {
        return idReferido;
    }

    public String getIdProspecto() {
        return idProspecto;
    }

    public String getIdReferidoDispositivo() {
        return idReferidoDispositivo;
    }

    public String getCodigoTipoReferido() {
        return codigoTipoReferido;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public String getCodigoRangoEdad() {
        return codigoRangoEdad;
    }

    public String getFlagHijo() {
        return flagHijo;
    }

    public String getCodigoRangoIngreso() {
        return codigoRangoIngreso;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getFechaCreaContacto() {
        return fechaCreaContacto;
    }

    public String getCodigoTipoEmpleo() {
        return codigoTipoEmpleo;
    }

    public String getFlagActivo() {
        return flagActivo;
    }

    public String getFechaCreacionDispositivo() {
        return fechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return fechaModificacionDispositivo;
    }
}