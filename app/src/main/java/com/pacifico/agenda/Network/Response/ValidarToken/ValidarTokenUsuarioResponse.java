package com.pacifico.agenda.Network.Response.ValidarToken;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 27/07/16.
 */
public class ValidarTokenUsuarioResponse{
    @SerializedName("CodigoIntermediario")
    @Expose
    private String codigoIntermediario;
    @SerializedName("Nombres")
    @Expose
    private String nombres;

    public String getCodigoIntermediario() {
        return codigoIntermediario;
    }

    public String getNombres() {
        return nombres;
    }
}