package com.pacifico.agenda.Network.Request.ValidarUsuario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 27/07/16.
 */
public class ValidarUsuarioTokenRequest{
    @SerializedName("Login")
    @Expose
    private String login;
    @SerializedName("IdDispositivo")
    @Expose
    private String idDispositivo;

    public void setLogin(String login) {
        this.login = login;
    }

    public void setIdDispositivo(String idDispositivo) {
        this.idDispositivo = idDispositivo;
    }
}