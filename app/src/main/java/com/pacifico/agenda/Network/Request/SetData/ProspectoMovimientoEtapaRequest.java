package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by joel on 7/18/16.
 */
public class ProspectoMovimientoEtapaRequest {
    @SerializedName("IdMovimiento")
    @Expose
    private String IdMovimiento;
    @SerializedName("IdProspecto")
    @Expose
    private String IdProspecto;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;
    @SerializedName("IdMovimientoDispositivo")
    @Expose
    private String IdMovimientoDispositivo;
    @SerializedName("CodigoEtapa")
    @Expose
    private String CodigoEtapa;
    @SerializedName("CodigoEstado")
    @Expose
    private String CodigoEstado;
    @SerializedName("FechaMovimientoEtapaDispositivo")
    @Expose
    private String FechaMovimientoEtapaDispositivo;

    public void setIdMovimiento(String idMovimiento) {
        IdMovimiento = idMovimiento;
    }

    public void setIdProspecto(String idProspecto) {
        IdProspecto = idProspecto;
    }

    public void setIdProspectoDispositivo(String idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public void setIdMovimientoDispositivo(String idMovimientoDispositivo) {
        IdMovimientoDispositivo = idMovimientoDispositivo;
    }

    public void setCodigoEtapa(String codigoEtapa) {
        CodigoEtapa = codigoEtapa;
    }

    public void setCodigoEstado(String codigoEstado) {
        CodigoEstado = codigoEstado;
    }

    public void setFechaMovimientoEtapaDispositivo(String fechaMovimientoEtapaDispositivo) {
        FechaMovimientoEtapaDispositivo = fechaMovimientoEtapaDispositivo;
    }
}