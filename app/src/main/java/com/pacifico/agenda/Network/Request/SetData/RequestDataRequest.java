package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.pacifico.agenda.Model.Bean.DispositivoBean;
import com.pacifico.agenda.Model.Bean.IntermediarioBean;
import com.pacifico.agenda.Model.Controller.DispositivoController;
import com.pacifico.agenda.Model.Controller.IntermediarioController;
import com.pacifico.agenda.Util.Constantes;
import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vctrls3477 on 8/06/16.
 */
public class RequestDataRequest{
    @SerializedName("Login")
    @Expose
    private String login;
    @SerializedName("CodigoIntermediario")
    @Expose
    private String codigoIntermediario;
    @SerializedName("CodigoToken")
    @Expose
    private String codigoToken;
    @SerializedName("HashEnvio")
    @Expose
    private String hashEnvio;
    @SerializedName("FechaUltimaSincronizacion")
    @Expose
    private String fechaUltimaSincronizacion;
    @SerializedName("MAC")
    @Expose
    private String MAC;
    @SerializedName("NumeroSerie")
    @Expose
    private String numeroSerie;
    @SerializedName("Reunion")
    @Expose
    private List<ReunionInternaRequest> listaReunion;
    @SerializedName("Prospecto")
    @Expose
    private List<ProspectoRequest> listaProspectos;
    @SerializedName("ProspectoMovimientoEtapa")
    @Expose
    private List<ProspectoMovimientoEtapaRequest> listaProspectosMovimientoEtapa;
    @SerializedName("ADN")
    @Expose
    private List<ADNRequest> listaADNs;
    @SerializedName("Familiar")
    @Expose
    private List<FamiliarRequest> listaFamiliares;
    @SerializedName("Cita")
    @Expose
    private List<CitaRequest> listaCitas;
    @SerializedName("CitaMovimientoEstado")
    @Expose
    private List<CitaMovimientoEstadoRequest> listaCitaMovimientoEstados;
    @SerializedName("Referido")
    @Expose
    private List<ReferidoRequest> listaReferidos;
    @SerializedName("RecordatorioLlamada")
    @Expose
    private List<RecordatorioLLamadaRequest> listaRecordatorioLlamadas;

    public RequestDataRequest(){
        IntermediarioBean intermediarioBean = IntermediarioController.obtenerIntermediario();
        DispositivoBean dispositivoBean = DispositivoController.obtenerDispositivoPorIdDispositivo(1);
        try {
            login = AESCrypt.decrypt(Constantes.SemillaEncriptacion, intermediarioBean.getLogin());
            codigoToken = AESCrypt.decrypt(Constantes.SemillaEncriptacion, intermediarioBean.getToken());
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        codigoIntermediario = Integer.toString(intermediarioBean.getCodigoIntermediario());
        fechaUltimaSincronizacion = dispositivoBean.getFechaUltimaSincronizacion();
        MAC = dispositivoBean.getMAC();
        numeroSerie = dispositivoBean.getNumeroSerie();
        listaReunion = new ArrayList<>();
        listaProspectos = new ArrayList<>();
        listaProspectosMovimientoEtapa = new ArrayList<>();
        listaADNs = new ArrayList<>();
        listaFamiliares = new ArrayList<>();
        listaCitas = new ArrayList<>();
        listaCitaMovimientoEstados = new ArrayList<>();
        listaReferidos = new ArrayList<>();
        listaRecordatorioLlamadas = new ArrayList<>();
    }
    public void setHashDeEnvio(String hashDeEnvio) {
        this.hashEnvio = hashDeEnvio;
    }
    public void setListaADNs(List<ADNRequest> listaADNs) {
        this.listaADNs = listaADNs;
    }
    public void setListaProspectos(List<ProspectoRequest> listaProspectos) {
        this.listaProspectos = listaProspectos;
    }
    public void setListaFamiliares(List<FamiliarRequest> listaFamiliares) {
        this.listaFamiliares = listaFamiliares;
    }
    public void setListaReferidos(List<ReferidoRequest> listaReferidos) {
        this.listaReferidos = listaReferidos;
    }

    public void setListaReunion(List<ReunionInternaRequest> listaReunion) {
        this.listaReunion = listaReunion;
    }

    public void setListaProspectosMovimientoEtapa(List<ProspectoMovimientoEtapaRequest> listaProspectosMovimientoEtapa) {
        this.listaProspectosMovimientoEtapa = listaProspectosMovimientoEtapa;
    }

    public void setListaCitas(List<CitaRequest> listaCitas) {
        this.listaCitas = listaCitas;
    }

    public void setListaCitaMovimientoEstados(List<CitaMovimientoEstadoRequest> listaCitaMovimientoEstados) {
        this.listaCitaMovimientoEstados = listaCitaMovimientoEstados;
    }

    public void setListaRecordatorioLlamadas(List<RecordatorioLLamadaRequest> listaRecordatorioLlamadas) {
        this.listaRecordatorioLlamadas = listaRecordatorioLlamadas;
    }
}