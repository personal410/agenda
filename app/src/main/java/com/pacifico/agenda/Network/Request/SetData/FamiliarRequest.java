package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DSB on 01/07/2016.
 */
public class FamiliarRequest{
    @SerializedName("IdFamiliar")
    @Expose
    private String IdFamiliar;
    @SerializedName("IdFamiliarDispositivo")
    @Expose
    private String IdFamiliarDispositivo;
    @SerializedName("IdProspecto")
    @Expose
    private String IdProspecto;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;
    @SerializedName("Nombres")
    @Expose
    private String Nombres;
    @SerializedName("ApellidoPaterno")
    @Expose
    private String ApellidoPaterno;
    @SerializedName("ApellidoMaterno")
    @Expose
    private String ApellidoMaterno;
    @SerializedName("FechaNacimiento")
    @Expose
    private String FechaNacimiento;
    @SerializedName("Edad")
    @Expose
    private String Edad;
    @SerializedName("Ocupacion")
    @Expose
    private String Ocupacion;
    @SerializedName("CodigoTipoFamiliar")
    @Expose
    private String CodigoTipoFamiliar;
    @SerializedName("FlagActivo")
    @Expose
    private String FlagActivo;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String FechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String FechaModificacionDispositivo;

    public void setIdFamiliar(String idFamiliar) {
        IdFamiliar = idFamiliar;
    }

    public void setIdFamiliarDispositivo(String idFamiliarDispositivo) {
        IdFamiliarDispositivo = idFamiliarDispositivo;
    }

    public void setIdProspecto(String idProspecto) {
        IdProspecto = idProspecto;
    }

    public void setIdProspectoDispositivo(String idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        FechaNacimiento = fechaNacimiento;
    }

    public void setEdad(String edad) {
        Edad = edad;
    }

    public void setOcupacion(String ocupacion) {
        Ocupacion = ocupacion;
    }

    public void setCodigoTipoFamiliar(String codigoTipoFamiliar) {
        CodigoTipoFamiliar = codigoTipoFamiliar;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public void setFlagActivo(String flagActivo) {
        FlagActivo = flagActivo;
    }
}
