package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by victorsalazar on 14/06/16.
 */
public class ADNResponse{
    @SerializedName("IdProspecto")
    @Expose
    private String IdProspecto;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;
    @SerializedName("TipoCambio")
    @Expose
    private String TipoCambio;
    @SerializedName("MonedaEfectivoAhorros")
    @Expose
    private String MonedaEfectivoAhorros;
    @SerializedName("EfectivoAhorros")
    @Expose
    private String EfectivoAhorros;
    @SerializedName("MonedaPropiedades")
    @Expose
    private String MonedaPropiedades;
    @SerializedName("Propiedades")
    @Expose
    private String Propiedades;
    @SerializedName("MonedaVehiculos")
    @Expose
    private String MonedaVehiculos;
    @SerializedName("Vehiculos")
    @Expose
    private String Vehiculos;
    @SerializedName("MonedaTotalActivoRealizable")
    @Expose
    private String MonedaTotalActivoRealizable;
    @SerializedName("TotalActivoRealizable")
    @Expose
    private String TotalActivoRealizable;
    @SerializedName("MonedaSeguroIndividual")
    @Expose
    private String MonedaSeguroIndividual;
    @SerializedName("SeguroIndividual")
    @Expose
    private String SeguroIndividual;
    @SerializedName("MonedaIngresoBrutoMensualVidaLey")
    @Expose
    private String MonedaIngresoBrutoMensualVidaLey;
    @SerializedName("IngresoBrutoMensualVidaLey")
    @Expose
    private String IngresoBrutoMensualVidaLey;
    @SerializedName("FactorVidaLey")
    @Expose
    private String FactorVidaLey;
    @SerializedName("MonedaTopeVidaLey")
    @Expose
    private String MonedaTopeVidaLey;
    @SerializedName("TopeVidaLey")
    @Expose
    private String TopeVidaLey;
    @SerializedName("MonedaSeguroVidaLey")
    @Expose
    private String MonedaSeguroVidaLey;
    @SerializedName("SeguroVidaLey")
    @Expose
    private String SeguroVidaLey;
    @SerializedName("FlagCuentaConVidaLey")
    @Expose
    private String FlagCuentaConVidaLey;
    @SerializedName("MonedaTotalSegurosVida")
    @Expose
    private String MonedaTotalSegurosVida;
    @SerializedName("TotalSegurosVida")
    @Expose
    private String TotalSegurosVida;
    @SerializedName("PorcentajeAFPConyuge")
    @Expose
    private String PorcentajeAFPConyuge;
    @SerializedName("PorcentajeAFPHijos")
    @Expose
    private String PorcentajeAFPHijos;
    @SerializedName("NumeroHijos")
    @Expose
    private String NumeroHijos;
    @SerializedName("MonedaPensionConyuge")
    @Expose
    private String MonedaPensionConyuge;
    @SerializedName("PensionConyuge")
    @Expose
    private String PensionConyuge;
    @SerializedName("MonedaPensionHijos")
    @Expose
    private String MonedaPensionHijos;
    @SerializedName("PensionHijos")
    @Expose
    private String PensionHijos;
    @SerializedName("FlagPensionConyuge")
    @Expose
    private String FlagPensionConyuge;
    @SerializedName("FlagPensionHijo")
    @Expose
    private String FlagPensionHijos;
    @SerializedName("FlagPensionNoAFP")
    @Expose
    private String FlagPensionNoAFP;
    @SerializedName("MonedaTotalPensionMensualAFP")
    @Expose
    private String MonedaTotalPensionMensualAFP;
    @SerializedName("TotalPensionMensualAFP")
    @Expose
    private String TotalPensionMensualAFP;
    @SerializedName("MonedaIngresoMensualTitular")
    @Expose
    private String MonedaIngresoMensualTitular;
    @SerializedName("IngresoMensualTitular")
    @Expose
    private String IngresoMensualTitular;
    @SerializedName("MonedaIngresoMensualConyuge")
    @Expose
    private String MonedaIngresoMensualConyuge;
    @SerializedName("IngresoMensualConyuge")
    @Expose
    private String IngresoMensualConyuge;
    @SerializedName("MonedaIngresoFamiliarOtros")
    @Expose
    private String MonedaIngresoFamiliarOtros;
    @SerializedName("IngresoFamiliarOtros")
    @Expose
    private String IngresoFamiliarOtros;
    @SerializedName("MonedaTotalIngresoFamiliarMensual")
    @Expose
    private String MonedaTotalIngresoFamiliarMensual;
    @SerializedName("TotalIngresoFamiliarMensual")
    @Expose
    private String TotalIngresoFamiliarMensual;
    @SerializedName("MonedaGastoVivienda")
    @Expose
    private String MonedaGastoVivienda;
    @SerializedName("GastoVivienda")
    @Expose
    private String GastoVivienda;
    @SerializedName("MonedaGastoServicios")
    @Expose
    private String MonedaGastoServicios;
    @SerializedName("GastoServicios")
    @Expose
    private String GastoServicios;
    @SerializedName("MonedaGastoHogarAlimentacion")
    @Expose
    private String MonedaGastoHogarAlimentacion;
    @SerializedName("GastoHogarAlimentacion")
    @Expose
    private String GastoHogarAlimentacion;
    @SerializedName("MonedaGastoSalud")
    @Expose
    private String MonedaGastoSalud;
    @SerializedName("GastoSalud")
    @Expose
    private String GastoSalud;
    @SerializedName("MonedaGastoEducacion")
    @Expose
    private String MonedaGastoEducacion;
    @SerializedName("GastoEducacion")
    @Expose
    private String GastoEducacion;
    @SerializedName("MonedaGastoVehiculoTransporte")
    @Expose
    private String MonedaGastoVehiculoTransporte;
    @SerializedName("GastoVehiculoTransporte")
    @Expose
    private String GastoVehiculoTransporte;
    @SerializedName("MonedaGastoEsparcimiento")
    @Expose
    private String MonedaGastoEsparcimiento;
    @SerializedName("GastoEsparcimiento")
    @Expose
    private String GastoEsparcimiento;
    @SerializedName("MonedaGastoOtros")
    @Expose
    private String MonedaGastoOtros;
    @SerializedName("GastoOtros")
    @Expose
    private String GastoOtros;
    @SerializedName("MonedaTotalGastoFamiliarMensual")
    @Expose
    private String MonedaTotalGastoFamiliarMensual;
    @SerializedName("TotalGastoFamiliarMensual")
    @Expose
    private String TotalGastoFamiliarMensual;
    @SerializedName("MonedaDeficitMensual")
    @Expose
    private String MonedaDeficitMensual;
    @SerializedName("DeficitMensual")
    @Expose
    private String DeficitMensual;
    @SerializedName("AniosProteger")
    @Expose
    private String AniosProteger;
    @SerializedName("MonedaCapitalNecesarioFallecimiento")
    @Expose
    private String MonedaCapitalNecesarioFallecimiento;
    @SerializedName("CapitalNecesarioFallecimiento")
    @Expose
    private String CapitalNecesarioFallecimiento;
    @SerializedName("PorcentajeInversion")
    @Expose
    private String PorcentajeInversion;
    @SerializedName("MonedaMontoMensualInvertir")
    @Expose
    private String MonedaMontoMensualInvertir;
    @SerializedName("MontoMensualInvertir")
    @Expose
    private String MontoMensualInvertir;
    @SerializedName("IdEntidad")
    @Expose
    private String IdEntidad;
    @SerializedName("CodigoFrecuenciaPago")
    @Expose
    private String CodigoFrecuenciaPago;
    @SerializedName("IndicadorVentana")
    @Expose
    private String IndicadorVentana;
    @SerializedName("FlagTerminado")
    @Expose
    private String FlagTerminado;
    @SerializedName("FlagADNDigital")
    @Expose
    private String FlagADNDigital;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String FechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String FechaModificacionDispositivo;
    @SerializedName("AdicionalTexto1")
    @Expose
    private String AdicionalTexto1;
    @SerializedName("AdicionalNumerico1")
    @Expose
    private String AdicionalNumerico1;
    @SerializedName("AdicionalTexto2")
    @Expose
    private String AdicionalTexto2;
    @SerializedName("AdicionalNumerico2")
    @Expose
    private String AdicionalNumerico2;

    public String getIdProspecto() {
        return IdProspecto;
    }

    public String getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public String getTipoCambio() {
        return TipoCambio;
    }

    public String getMonedaEfectivoAhorros() {
        return MonedaEfectivoAhorros;
    }

    public String getEfectivoAhorros() {
        return EfectivoAhorros;
    }

    public String getMonedaPropiedades() {
        return MonedaPropiedades;
    }

    public String getPropiedades() {
        return Propiedades;
    }

    public String getMonedaVehiculos() {
        return MonedaVehiculos;
    }

    public String getVehiculos() {
        return Vehiculos;
    }

    public String getMonedaTotalActivoRealizable() {
        return MonedaTotalActivoRealizable;
    }

    public String getTotalActivoRealizable() {
        return TotalActivoRealizable;
    }

    public String getMonedaSeguroIndividual() {
        return MonedaSeguroIndividual;
    }

    public String getSeguroIndividual() {
        return SeguroIndividual;
    }

    public String getMonedaIngresoBrutoMensualVidaLey() {
        return MonedaIngresoBrutoMensualVidaLey;
    }

    public String getIngresoBrutoMensualVidaLey() {
        return IngresoBrutoMensualVidaLey;
    }

    public String getFactorVidaLey() {
        return FactorVidaLey;
    }

    public String getMonedaTopeVidaLey() {
        return MonedaTopeVidaLey;
    }

    public String getTopeVidaLey() {
        return TopeVidaLey;
    }

    public String getMonedaSeguroVidaLey() {
        return MonedaSeguroVidaLey;
    }

    public String getSeguroVidaLey() {
        return SeguroVidaLey;
    }

    public String getFlagCuentaConVidaLey() {
        return FlagCuentaConVidaLey;
    }

    public String getMonedaTotalSegurosVida() {
        return MonedaTotalSegurosVida;
    }

    public String getTotalSegurosVida() {
        return TotalSegurosVida;
    }

    public String getPorcentajeAFPConyuge() {
        return PorcentajeAFPConyuge;
    }

    public String getPorcentajeAFPHijos() {
        return PorcentajeAFPHijos;
    }

    public String getNumeroHijos() {
        return NumeroHijos;
    }

    public String getMonedaPensionConyuge() {
        return MonedaPensionConyuge;
    }

    public String getPensionConyuge() {
        return PensionConyuge;
    }

    public String getMonedaPensionHijos() {
        return MonedaPensionHijos;
    }

    public String getPensionHijos() {
        return PensionHijos;
    }

    public String getFlagPensionConyuge() {
        return FlagPensionConyuge;
    }

    public String getFlagPensionHijos() {
        return FlagPensionHijos;
    }

    public String getFlagPensionNoAFP() {
        return FlagPensionNoAFP;
    }

    public String getMonedaTotalPensionMensualAFP() {
        return MonedaTotalPensionMensualAFP;
    }

    public String getTotalPensionMensualAFP() {
        return TotalPensionMensualAFP;
    }

    public String getMonedaIngresoMensualTitular() {
        return MonedaIngresoMensualTitular;
    }

    public String getIngresoMensualTitular() {
        return IngresoMensualTitular;
    }

    public String getMonedaIngresoMensualConyuge() {
        return MonedaIngresoMensualConyuge;
    }

    public String getIngresoMensualConyuge() {
        return IngresoMensualConyuge;
    }

    public String getMonedaIngresoFamiliarOtros() {
        return MonedaIngresoFamiliarOtros;
    }

    public String getIngresoFamiliarOtros() {
        return IngresoFamiliarOtros;
    }

    public String getMonedaTotalIngresoFamiliarMensual() {
        return MonedaTotalIngresoFamiliarMensual;
    }

    public String getTotalIngresoFamiliarMensual() {
        return TotalIngresoFamiliarMensual;
    }

    public String getMonedaGastoVivienda() {
        return MonedaGastoVivienda;
    }

    public String getGastoVivienda() {
        return GastoVivienda;
    }

    public String getMonedaGastoServicios() {
        return MonedaGastoServicios;
    }

    public String getGastoServicios() {
        return GastoServicios;
    }

    public String getMonedaGastoHogarAlimentacion() {
        return MonedaGastoHogarAlimentacion;
    }

    public String getGastoHogarAlimentacion() {
        return GastoHogarAlimentacion;
    }

    public String getMonedaGastoSalud() {
        return MonedaGastoSalud;
    }

    public String getGastoSalud() {
        return GastoSalud;
    }

    public String getMonedaGastoEducacion() {
        return MonedaGastoEducacion;
    }

    public String getGastoEducacion() {
        return GastoEducacion;
    }

    public String getMonedaGastoVehiculoTransporte() {
        return MonedaGastoVehiculoTransporte;
    }

    public String getGastoVehiculoTransporte() {
        return GastoVehiculoTransporte;
    }

    public String getMonedaGastoEsparcimiento() {
        return MonedaGastoEsparcimiento;
    }

    public String getGastoEsparcimiento() {
        return GastoEsparcimiento;
    }

    public String getMonedaGastoOtros() {
        return MonedaGastoOtros;
    }

    public String getGastoOtros() {
        return GastoOtros;
    }

    public String getMonedaTotalGastoFamiliarMensual() {
        return MonedaTotalGastoFamiliarMensual;
    }

    public String getTotalGastoFamiliarMensual() {
        return TotalGastoFamiliarMensual;
    }

    public String getMonedaDeficitMensual() {
        return MonedaDeficitMensual;
    }

    public String getDeficitMensual() {
        return DeficitMensual;
    }

    public String getAniosProteger() {
        return AniosProteger;
    }

    public String getMonedaCapitalNecesarioFallecimiento() {
        return MonedaCapitalNecesarioFallecimiento;
    }

    public String getCapitalNecesarioFallecimiento() {
        return CapitalNecesarioFallecimiento;
    }

    public String getPorcentajeInversion() {
        return PorcentajeInversion;
    }

    public String getMonedaMontoMensualInvertir() {
        return MonedaMontoMensualInvertir;
    }

    public String getMontoMensualInvertir() {
        return MontoMensualInvertir;
    }

    public String getIdEntidad() {
        return IdEntidad;
    }

    public String getCodigoFrecuenciaPago() {
        return CodigoFrecuenciaPago;
    }

    public String getIndicadorVentana() {
        return IndicadorVentana;
    }

    public String getFlagTerminado() {
        return FlagTerminado;
    }

    public String getFlagADNDigital() {
        return FlagADNDigital;
    }

    public String getFechaCreacionDispositivo() {
        return FechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return FechaModificacionDispositivo;
    }

    public String getAdicionalTexto1() {
        return AdicionalTexto1;
    }

    public String getAdicionalNumerico1() {
        return AdicionalNumerico1;
    }

    public String getAdicionalTexto2() {
        return AdicionalTexto2;
    }

    public String getAdicionalNumerico2() {
        return AdicionalNumerico2;
    }
}