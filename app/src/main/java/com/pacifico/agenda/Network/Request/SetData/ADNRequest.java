package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 8/06/16.
 */
public class ADNRequest{
    @SerializedName("IdProspecto")
    @Expose
    private String IdProspecto;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;
    @SerializedName("TipoCambio")
    @Expose
    private String TipoCambio;
    @SerializedName("MonedaEfectivoAhorros")
    @Expose
    private String MonedaEfectivoAhorros;
    @SerializedName("EfectivoAhorros")
    @Expose
    private String EfectivoAhorros;
    @SerializedName("MonedaPropiedades")
    @Expose
    private String MonedaPropiedades;
    @SerializedName("Propiedades")
    @Expose
    private String Propiedades;
    @SerializedName("MonedaVehiculos")
    @Expose
    private String MonedaVehiculos;
    @SerializedName("Vehiculos")
    @Expose
    private String Vehiculos;
    @SerializedName("MonedaTotalActivoRealizable")
    @Expose
    private String MonedaTotalActivoRealizable;
    @SerializedName("TotalActivoRealizable")
    @Expose
    private String TotalActivoRealizable;
    @SerializedName("MonedaSeguroIndividual")
    @Expose
    private String MonedaSeguroIndividual;
    @SerializedName("SeguroIndividual")
    @Expose
    private String SeguroIndividual;
    @SerializedName("MonedaIngresoBrutoMensualVidaLey")
    @Expose
    private String MonedaIngresoBrutoMensualVidaLey;
    @SerializedName("IngresoBrutoMensualVidaLey")
    @Expose
    private String IngresoBrutoMensualVidaLey;
    @SerializedName("FactorVidaLey")
    @Expose
    private String FactorVidaLey;
    @SerializedName("MonedaTopeVidaLey")
    @Expose
    private String MonedaTopeVidaLey;
    @SerializedName("TopeVidaLey")
    @Expose
    private String TopeVidaLey;
    @SerializedName("MonedaSeguroVidaLey")
    @Expose
    private String MonedaSeguroVidaLey;
    @SerializedName("SeguroVidaLey")
    @Expose
    private String SeguroVidaLey;
    @SerializedName("FlagCuentaConVidaLey")
    @Expose
    private String FlagCuentaConVidaLey;
    @SerializedName("MonedaTotalSegurosVida")
    @Expose
    private String MonedaTotalSegurosVida;
    @SerializedName("TotalSegurosVida")
    @Expose
    private String TotalSegurosVida;
    @SerializedName("PorcentajeAFPConyuge")
    @Expose
    private String PorcentajeAFPConyuge;
    @SerializedName("PorcentajeAFPHijos")
    @Expose
    private String PorcentajeAFPHijos;
    @SerializedName("NumeroHijos")
    @Expose
    private String NumeroHijos;
    @SerializedName("MonedaPensionConyuge")
    @Expose
    private String MonedaPensionConyuge;
    @SerializedName("PensionConyuge")
    @Expose
    private String PensionConyuge;
    @SerializedName("MonedaPensionHijos")
    @Expose
    private String MonedaPensionHijos;
    @SerializedName("PensionHijos")
    @Expose
    private String PensionHijos;
    @SerializedName("FlagPensionConyuge")
    @Expose
    private String FlagPensionConyuge;
    @SerializedName("FlagPensionHijos")
    @Expose
    private String FlagPensionHijos;
    @SerializedName("FlagPensionNoAFP")
    @Expose
    private String FlagPensionNoAFP;
    @SerializedName("MonedaTotalPensionMensualAFP")
    @Expose
    private String MonedaTotalPensionMensualAFP;
    @SerializedName("TotalPensionMensualAFP")
    @Expose
    private String TotalPensionMensualAFP;
    @SerializedName("MonedaIngresoMensualTitular")
    @Expose
    private String MonedaIngresoMensualTitular;
    @SerializedName("IngresoMensualTitular")
    @Expose
    private String IngresoMensualTitular;
    @SerializedName("MonedaIngresoMensualConyuge")
    @Expose
    private String MonedaIngresoMensualConyuge;
    @SerializedName("IngresoMensualConyuge")
    @Expose
    private String IngresoMensualConyuge;
    @SerializedName("MonedaIngresoFamiliarOtros")
    @Expose
    private String MonedaIngresoFamiliarOtros;
    @SerializedName("IngresoFamiliarOtros")
    @Expose
    private String IngresoFamiliarOtros;
    @SerializedName("MonedaTotalIngresoFamiliarMensual")
    @Expose
    private String MonedaTotalIngresoFamiliarMensual;
    @SerializedName("TotalIngresoFamiliarMensual")
    @Expose
    private String TotalIngresoFamiliarMensual;
    @SerializedName("MonedaGastoVivienda")
    @Expose
    private String MonedaGastoVivienda;
    @SerializedName("GastoVivienda")
    @Expose
    private String GastoVivienda;
    @SerializedName("MonedaGastoServicios")
    @Expose
    private String MonedaGastoServicios;
    @SerializedName("GastoServicios")
    @Expose
    private String GastoServicios;
    @SerializedName("MonedaGastoHogarAlimentacion")
    @Expose
    private String MonedaGastoHogarAlimentacion;
    @SerializedName("GastoHogarAlimentacion")
    @Expose
    private String GastoHogarAlimentacion;
    @SerializedName("MonedaGastoSalud")
    @Expose
    private String MonedaGastoSalud;
    @SerializedName("GastoSalud")
    @Expose
    private String GastoSalud;
    @SerializedName("MonedaGastoEducacion")
    @Expose
    private String MonedaGastoEducacion;
    @SerializedName("GastoEducacion")
    @Expose
    private String GastoEducacion;
    @SerializedName("MonedaGastoVehiculoTransporte")
    @Expose
    private String MonedaGastoVehiculoTransporte;
    @SerializedName("GastoVehiculoTransporte")
    @Expose
    private String GastoVehiculoTransporte;
    @SerializedName("MonedaGastoEsparcimiento")
    @Expose
    private String MonedaGastoEsparcimiento;
    @SerializedName("GastoEsparcimiento")
    @Expose
    private String GastoEsparcimiento;
    @SerializedName("MonedaGastoOtros")
    @Expose
    private String MonedaGastoOtros;
    @SerializedName("GastoOtros")
    @Expose
    private String GastoOtros;
    @SerializedName("MonedaTotalGastoFamiliarMensual")
    @Expose
    private String MonedaTotalGastoFamiliarMensual;
    @SerializedName("TotalGastoFamiliarMensual")
    @Expose
    private String TotalGastoFamiliarMensual;
    @SerializedName("MonedaDeficitMensual")
    @Expose
    private String MonedaDeficitMensual;
    @SerializedName("DeficitMensual")
    @Expose
    private String DeficitMensual;
    @SerializedName("AniosProteger")
    @Expose
    private String AniosProteger;
    @SerializedName("MonedaCapitalNecesarioFallecimiento")
    @Expose
    private String MonedaCapitalNecesarioFallecimiento;
    @SerializedName("CapitalNecesarioFallecimiento")
    @Expose
    private String CapitalNecesarioFallecimiento;
    @SerializedName("PorcentajeInversion")
    @Expose
    private String PorcentajeInversion;
    @SerializedName("MonedaMontoMensualInvertir")
    @Expose
    private String MonedaMontoMensualInvertir;
    @SerializedName("MontoMensualInvertir")
    @Expose
    private String MontoMensualInvertir;
    @SerializedName("IdEntidad")
    @Expose
    private String IdEntidad;
    @SerializedName("CodigoFrecuenciaPago")
    @Expose
    private String CodigoFrecuenciaPago;
    @SerializedName("IndicadorVentana")
    @Expose
    private String IndicadorVentana;
    @SerializedName("FlagTerminado")
    @Expose
    private String FlagTerminado;
    @SerializedName("FlagADNDigital")
    @Expose
    private String FlagADNDigital;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String FechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String FechaModificacionDispositivo;
    @SerializedName("AdicionalTexto1")
    @Expose
    private String AdicionalTexto1;
    @SerializedName("AdicionalNumerico1")
    @Expose
    private String AdicionalNumerico1;
    @SerializedName("AdicionalTexto2")
    @Expose
    private String AdicionalTexto2;
    @SerializedName("AdicionalNumerico2")
    @Expose
    private String AdicionalNumerico2;

    public void setIdProspecto(String idProspecto) {
        IdProspecto = idProspecto;
    }

    public void setTipoCambio(String tipoCambio) {
        TipoCambio = tipoCambio;
    }

    public void setMonedaEfectivoAhorros(String monedaEfectivoAhorros) {
        MonedaEfectivoAhorros = monedaEfectivoAhorros;
    }

    public void setEfectivoAhorros(String efectivoAhorros) {
        EfectivoAhorros = efectivoAhorros;
    }

    public void setMonedaPropiedades(String monedaPropiedades) {
        MonedaPropiedades = monedaPropiedades;
    }

    public void setPropiedades(String propiedades) {
        Propiedades = propiedades;
    }

    public void setMonedaVehiculos(String monedaVehiculos) {
        MonedaVehiculos = monedaVehiculos;
    }

    public void setVehiculos(String vehiculos) {
        Vehiculos = vehiculos;
    }

    public void setMonedaTotalActivoRealizable(String monedaTotalActivoRealizable) {
        MonedaTotalActivoRealizable = monedaTotalActivoRealizable;
    }

    public void setTotalActivoRealizable(String totalActivoRealizable) {
        TotalActivoRealizable = totalActivoRealizable;
    }

    public void setMonedaSeguroIndividual(String monedaSeguroIndividual) {
        MonedaSeguroIndividual = monedaSeguroIndividual;
    }

    public void setSeguroIndividual(String seguroIndividual) {
        SeguroIndividual = seguroIndividual;
    }

    public void setMonedaIngresoBrutoMensualVidaLey(String monedaIngresoBrutoMensualVidaLey) {
        MonedaIngresoBrutoMensualVidaLey = monedaIngresoBrutoMensualVidaLey;
    }

    public void setIngresoBrutoMensualVidaLey(String ingresoBrutoMensualVidaLey) {
        IngresoBrutoMensualVidaLey = ingresoBrutoMensualVidaLey;
    }

    public void setFactorVidaLey(String factorVidaLey) {
        FactorVidaLey = factorVidaLey;
    }

    public void setMonedaTopeVidaLey(String monedaTopeVidaLey) {
        MonedaTopeVidaLey = monedaTopeVidaLey;
    }

    public void setTopeVidaLey(String topeVidaLey) {
        TopeVidaLey = topeVidaLey;
    }

    public void setMonedaSeguroVidaLey(String monedaSeguroVidaLey) {
        MonedaSeguroVidaLey = monedaSeguroVidaLey;
    }

    public void setSeguroVidaLey(String seguroVidaLey) {
        SeguroVidaLey = seguroVidaLey;
    }

    public void setFlagCuentaConVidaLey(String flagCuentaConVidaLey) {
        FlagCuentaConVidaLey = flagCuentaConVidaLey;
    }

    public void setMonedaTotalSegurosVida(String monedaTotalSegurosVida) {
        MonedaTotalSegurosVida = monedaTotalSegurosVida;
    }

    public void setTotalSegurosVida(String totalSegurosVida) {
        TotalSegurosVida = totalSegurosVida;
    }

    public void setPorcentajeAFPConyuge(String porcentajeAFPConyuge) {
        PorcentajeAFPConyuge = porcentajeAFPConyuge;
    }

    public void setPorcentajeAFPHijos(String porcentajeAFPHijos) {
        PorcentajeAFPHijos = porcentajeAFPHijos;
    }

    public void setNumeroHijos(String numeroHijos) {
        NumeroHijos = numeroHijos;
    }

    public void setMonedaPensionConyuge(String monedaPensionConyuge) {
        MonedaPensionConyuge = monedaPensionConyuge;
    }

    public void setPensionConyuge(String pensionConyuge) {
        PensionConyuge = pensionConyuge;
    }

    public void setMonedaPensionHijos(String monedaPensionHijos) {
        MonedaPensionHijos = monedaPensionHijos;
    }

    public void setPensionHijos(String pensionHijos) {
        PensionHijos = pensionHijos;
    }

    public void setMonedaTotalPensionMensualAFP(String monedaTotalPensionMensualAFP) {
        MonedaTotalPensionMensualAFP = monedaTotalPensionMensualAFP;
    }

    public void setTotalPensionMensualAFP(String totalPensionMensualAFP) {
        TotalPensionMensualAFP = totalPensionMensualAFP;
    }

    public void setMonedaIngresoMensualTitular(String monedaIngresoMensualTitular) {
        MonedaIngresoMensualTitular = monedaIngresoMensualTitular;
    }

    public void setIngresoMensualTitular(String ingresoMensualTitular) {
        IngresoMensualTitular = ingresoMensualTitular;
    }

    public void setMonedaIngresoMensualConyuge(String monedaIngresoMensualConyuge) {
        MonedaIngresoMensualConyuge = monedaIngresoMensualConyuge;
    }

    public void setIngresoMensualConyuge(String ingresoMensualConyuge) {
        IngresoMensualConyuge = ingresoMensualConyuge;
    }

    public void setMonedaIngresoFamiliarOtros(String monedaIngresoFamiliarOtros) {
        MonedaIngresoFamiliarOtros = monedaIngresoFamiliarOtros;
    }

    public void setIngresoFamiliarOtros(String ingresoFamiliarOtros) {
        IngresoFamiliarOtros = ingresoFamiliarOtros;
    }

    public void setMonedaTotalIngresoFamiliarMensual(String monedaTotalIngresoFamiliarMensual) {
        MonedaTotalIngresoFamiliarMensual = monedaTotalIngresoFamiliarMensual;
    }

    public void setTotalIngresoFamiliarMensual(String totalIngresoFamiliarMensual) {
        TotalIngresoFamiliarMensual = totalIngresoFamiliarMensual;
    }

    public void setMonedaGastoVivienda(String monedaGastoVivienda) {
        MonedaGastoVivienda = monedaGastoVivienda;
    }

    public void setGastoVivienda(String gastoVivienda) {
        GastoVivienda = gastoVivienda;
    }

    public void setMonedaGastoServicios(String monedaGastoServicios) {
        MonedaGastoServicios = monedaGastoServicios;
    }

    public void setGastoServicios(String gastoServicios) {
        GastoServicios = gastoServicios;
    }

    public void setMonedaGastoHogarAlimentacion(String monedaGastoHogarAlimentacion) {
        MonedaGastoHogarAlimentacion = monedaGastoHogarAlimentacion;
    }

    public void setGastoHogarAlimentacion(String gastoHogarAlimentacion) {
        GastoHogarAlimentacion = gastoHogarAlimentacion;
    }

    public void setMonedaGastoSalud(String monedaGastoSalud) {
        MonedaGastoSalud = monedaGastoSalud;
    }

    public void setGastoSalud(String gastoSalud) {
        GastoSalud = gastoSalud;
    }

    public void setMonedaGastoEducacion(String monedaGastoEducacion) {
        MonedaGastoEducacion = monedaGastoEducacion;
    }

    public void setGastoEducacion(String gastoEducacion) {
        GastoEducacion = gastoEducacion;
    }

    public void setMonedaGastoVehiculoTransporte(String monedaGastoVehiculoTransporte) {
        MonedaGastoVehiculoTransporte = monedaGastoVehiculoTransporte;
    }

    public void setGastoVehiculoTransporte(String gastoVehiculoTransporte) {
        GastoVehiculoTransporte = gastoVehiculoTransporte;
    }

    public void setMonedaGastoEsparcimiento(String monedaGastoEsparcimiento) {
        MonedaGastoEsparcimiento = monedaGastoEsparcimiento;
    }

    public void setGastoEsparcimiento(String gastoEsparcimiento) {
        GastoEsparcimiento = gastoEsparcimiento;
    }

    public void setMonedaGastoOtros(String monedaGastoOtros) {
        MonedaGastoOtros = monedaGastoOtros;
    }

    public void setGastoOtros(String gastoOtros) {
        GastoOtros = gastoOtros;
    }

    public void setMonedaTotalGastoFamiliarMensual(String monedaTotalGastoFamiliarMensual) {
        MonedaTotalGastoFamiliarMensual = monedaTotalGastoFamiliarMensual;
    }

    public void setTotalGastoFamiliarMensual(String totalGastoFamiliarMensual) {
        TotalGastoFamiliarMensual = totalGastoFamiliarMensual;
    }

    public void setMonedaDeficitMensual(String monedaDeficitMensual) {
        MonedaDeficitMensual = monedaDeficitMensual;
    }

    public void setDeficitMensual(String deficitMensual) {
        DeficitMensual = deficitMensual;
    }

    public void setAniosProteger(String aniosProteger) {
        AniosProteger = aniosProteger;
    }

    public void setMonedaCapitalNecesarioFallecimiento(String monedaCapitalNecesarioFallecimiento) {
        MonedaCapitalNecesarioFallecimiento = monedaCapitalNecesarioFallecimiento;
    }

    public void setCapitalNecesarioFallecimiento(String capitalNecesarioFallecimiento) {
        CapitalNecesarioFallecimiento = capitalNecesarioFallecimiento;
    }

    public void setPorcentajeInversion(String porcentajeInversion) {
        PorcentajeInversion = porcentajeInversion;
    }

    public void setMonedaMontoMensualInvertir(String monedaMontoMensualInvertir) {
        MonedaMontoMensualInvertir = monedaMontoMensualInvertir;
    }

    public void setMontoMensualInvertir(String montoMensualInvertir) {
        MontoMensualInvertir = montoMensualInvertir;
    }

    public void setIdEntidad(String idEntidad) {
        IdEntidad = idEntidad;
    }

    public void setCodigoFrecuenciaPago(String codigoFrecuenciaPago) {
        CodigoFrecuenciaPago = codigoFrecuenciaPago;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public void setIdProspectoDispositivo(String idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public void setFlagPensionConyuge(String flagPensionConyuge) {
        FlagPensionConyuge = flagPensionConyuge;
    }

    public void setFlagPensionHijos(String flagPensionHijos) {
        FlagPensionHijos = flagPensionHijos;
    }

    public void setFlagPensionNoAFP(String flagPensionNoAFP) {
        FlagPensionNoAFP = flagPensionNoAFP;
    }

    public void setIndicadorVentana(String indicadorVentana) {
        IndicadorVentana = indicadorVentana;
    }

    public void setFlagTerminado(String flagTerminado) {
        FlagTerminado = flagTerminado;
    }

    public void setAdicionalTexto1(String adicionalTexto1) {
        AdicionalTexto1 = adicionalTexto1;
    }

    public void setAdicionalNumerico1(String adicionalNumerico1) {
        AdicionalNumerico1 = adicionalNumerico1;
    }

    public void setAdicionalTexto2(String adicionalTexto2) {
        AdicionalTexto2 = adicionalTexto2;
    }

    public void setAdicionalNumerico2(String adicionalNumerico2) {
        AdicionalNumerico2 = adicionalNumerico2;
    }

    public void setFlagADNDigital(String flagADNDigital) {
        FlagADNDigital = flagADNDigital;
    }
}