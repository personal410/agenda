package com.pacifico.agenda.Network.Response.GetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by joel on 9/1/16.
 */
public class RecordatorioLlamadaResponse {
    @SerializedName("IdRecordatorioLlamada")
    @Expose
    private String IdRecordatorioLlamada;
    @SerializedName("IdProspecto")
    @Expose
    private String IdProspecto;
    @SerializedName("IdCita")
    @Expose
    private String IdCita;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;
    @SerializedName("IdCitaDispositivo")
    @Expose
    private String IdCitaDispositivo;
    @SerializedName("IdRecordatorioLlamadaDispositivo")
    @Expose
    private String IdRecordatorioLlamadaDispositivo;

    @SerializedName("FechaRecordatorio")
    @Expose
    private String FechaRecordatorio;
    @SerializedName("FlagActivo")
    @Expose
    private String FlagActivo;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String FechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String FechaModificacionDispositivo;

    public String getIdRecordatorioLlamada() {
        return IdRecordatorioLlamada;
    }

    public String getIdProspecto() {
        return IdProspecto;
    }

    public String getIdCita() {
        return IdCita;
    }

    public String getIdProspectoDispositivo() {
        return IdProspectoDispositivo;
    }

    public String getIdCitaDispositivo() {
        return IdCitaDispositivo;
    }

    public String getIdRecordatorioLlamadaDispositivo() {
        return IdRecordatorioLlamadaDispositivo;
    }

    public String getFechaRecordatorio() {
        return FechaRecordatorio;
    }

    public String getFlagActivo() {
        return FlagActivo;
    }

    public String getFechaCreacionDispositivo() {
        return FechaCreacionDispositivo;
    }

    public String getFechaModificacionDispositivo() {
        return FechaModificacionDispositivo;
    }
}
