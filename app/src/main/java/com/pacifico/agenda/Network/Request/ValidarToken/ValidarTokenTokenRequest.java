package com.pacifico.agenda.Network.Request.ValidarToken;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 27/07/16.
 */
public class ValidarTokenTokenRequest{
    @SerializedName("Login")
    @Expose
    private String login;
    @SerializedName("IdDispositivo")
    @Expose
    private String idDispositivo;
    @SerializedName("CodigoValidacion")
    @Expose
    private String codigoValidacion;

    public void setLogin(String login) {
        this.login = login;
    }

    public void setIdDispositivo(String idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public void setCodigoValidacion(String codigoValidacion) {
        this.codigoValidacion = codigoValidacion;
    }
}
