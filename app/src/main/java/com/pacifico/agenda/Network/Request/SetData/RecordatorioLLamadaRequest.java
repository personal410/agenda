package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by joel on 7/18/16.
 */
public class RecordatorioLLamadaRequest {
    @SerializedName("IdRecordatorioLlamada")
    @Expose
    private String IdRecordatorioLlamada;
    @SerializedName("IdProspecto")
    @Expose
    private String IdProspecto;
    @SerializedName("IdCita")
    @Expose
    private String IdCita;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;
    @SerializedName("IdCitaDispositivo")
    @Expose
    private String IdCitaDispositivo;
    @SerializedName("IdRecordatorioLlamadaDispositivo")
    @Expose
    private String IdRecordatorioLlamadaDispositivo;
    @SerializedName("FechaRecordatorio")
    @Expose
    private String FechaRecordatorio;

    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String FechaCreacionDispositivo;

    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String FechaModificacionDispositivo;

    @SerializedName("FlagActivo")
    @Expose
    private String FlagActivo;

    public void setIdRecordatorioLlamada(String idRecordatorioLlamada) {
        IdRecordatorioLlamada = idRecordatorioLlamada;
    }

    public void setIdProspecto(String idProspecto) {
        IdProspecto = idProspecto;
    }

    public void setIdCita(String idCita) {
        IdCita = idCita;
    }

    public void setIdProspectoDispositivo(String idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }

    public void setIdCitaDispositivo(String idCitaDispositivo) {
        IdCitaDispositivo = idCitaDispositivo;
    }

    public void setIdRecordatorioLlamadaDispositivo(String idRecordatorioLlamadaDispositivo) {
        IdRecordatorioLlamadaDispositivo = idRecordatorioLlamadaDispositivo;
    }

    public void setFechaRecordatorio(String fechaRecordatorio) {
        FechaRecordatorio = fechaRecordatorio;
    }

    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }

    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }

    public void setFlagActivo(String flagActivo) {
        FlagActivo = flagActivo;
    }
}