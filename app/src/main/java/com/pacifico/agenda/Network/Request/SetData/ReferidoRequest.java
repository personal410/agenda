package com.pacifico.agenda.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 11/07/16.
 */
public class ReferidoRequest{
    @SerializedName("IdReferido")
    @Expose
    private String IdReferido;
    @SerializedName("IdProspecto")
    @Expose
    private String IdProspecto;
    @SerializedName("IdReferidoDispositivo")
    @Expose
    private String IdReferidoDispositivo;
    @SerializedName("IdProspectoDispositivo")
    @Expose
    private String IdProspectoDispositivo;
    @SerializedName("CodigoTipoReferido")
    @Expose
    private String CodigoTipoReferido;
    @SerializedName("Nombres")
    @Expose
    private String Nombres;
    @SerializedName("ApellidoPaterno")
    @Expose
    private String ApellidoPaterno;
    @SerializedName("ApellidoMaterno")
    @Expose
    private String ApellidoMaterno;
    @SerializedName("CodigoRangoEdad")
    @Expose
    private String CodigoRangoEdad;
    @SerializedName("FlagHijo")
    @Expose
    private String FlagHijo;
    @SerializedName("CodigoRangoIngreso")
    @Expose
    private String CodigoRangoIngreso;
    @SerializedName("Telefono")
    @Expose
    private String Telefono;
    @SerializedName("FechaCreaContacto")
    @Expose
    private String FechaCreaContacto;
    @SerializedName("CodigoTipoEmpleo")
    @Expose
    private String CodigoTipoEmpleo;
    @SerializedName("FlagActivo")
    @Expose
    private String FlagActivo;
    @SerializedName("FechaCreacionDispositivo")
    @Expose
    private String FechaCreacionDispositivo;
    @SerializedName("FechaModificacionDispositivo")
    @Expose
    private String FechaModificacionDispositivo;

    public void setIdReferido(String idReferido) {
        IdReferido = idReferido;
    }
    public void setIdProspecto(String idProspecto) {
        IdProspecto = idProspecto;
    }
    public void setIdReferidoDispositivo(String idReferidoDispositivo) {
        IdReferidoDispositivo = idReferidoDispositivo;
    }
    public void setIdProspectoDispositivo(String idProspectoDispositivo) {
        IdProspectoDispositivo = idProspectoDispositivo;
    }
    public void setCodigoTipoReferido(String codigoTipoReferido) {
        CodigoTipoReferido = codigoTipoReferido;
    }
    public void setNombres(String nombres) {
        Nombres = nombres;
    }
    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }
    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }
    public void setCodigoRangoEdad(String codigoRangoEdad) {
        CodigoRangoEdad = codigoRangoEdad;
    }
    public void setFlagHijo(String flagHijo) {
        FlagHijo = flagHijo;
    }
    public void setCodigoRangoIngreso(String codigoRangoIngreso) {
        CodigoRangoIngreso = codigoRangoIngreso;
    }
    public void setTelefono(String telefono) {
        Telefono = telefono;
    }
    public void setFechaCreaContacto(String fechaCreaContacto) {
        FechaCreaContacto = fechaCreaContacto;
    }
    public void setCodigoTipoEmpleo(String codigoTipoEmpleo) {
        CodigoTipoEmpleo = codigoTipoEmpleo;
    }
    public void setFlagActivo(String flagActivo) {
        FlagActivo = flagActivo;
    }
    public void setFechaCreacionDispositivo(String fechaCreacionDispositivo) {
        FechaCreacionDispositivo = fechaCreacionDispositivo;
    }
    public void setFechaModificacionDispositivo(String fechaModificacionDispositivo) {
        FechaModificacionDispositivo = fechaModificacionDispositivo;
    }
}