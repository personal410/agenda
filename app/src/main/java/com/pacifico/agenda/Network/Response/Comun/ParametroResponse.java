package com.pacifico.agenda.Network.Response.Comun;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 6/06/16.
 */
public class ParametroResponse{
    @SerializedName("IdParametro")
    @Expose
    private String idParametro;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    @SerializedName("ValorCadena")
    @Expose
    private String valorCadena;
    @SerializedName("ValorNumerico")
    @Expose
    private String valorNumerico;
    public String getIdParametro() {
        return idParametro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getValorCadena() {
        return valorCadena;
    }

    public String getValorNumerico() {
        return valorNumerico;
    }
}