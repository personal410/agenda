package com.pacifico.agenda.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by victorsalazar on 15/07/16.
 */
public class SetReferidoResponse{
    @SerializedName("IdReferido")
    @Expose
    private String idReferido;
    @SerializedName("IdReferidoDispositivo")
    @Expose
    private String idReferidoDispositivo;
    public String getIdReferido() {
        return idReferido;
    }
    public String getIdReferidoDispositivo() {
        return idReferidoDispositivo;
    }
}