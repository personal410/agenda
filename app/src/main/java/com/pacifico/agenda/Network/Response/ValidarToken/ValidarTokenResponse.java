package com.pacifico.agenda.Network.Response.ValidarToken;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 27/07/16.
 */
public class ValidarTokenResponse{
    @SerializedName("IsValid")
    @Expose
    private String isValid;
    @SerializedName("ResultCode")
    @Expose
    private String resultCode;
    @SerializedName("ResultMessage")
    @Expose
    private String resultMessage;
    @SerializedName("Acceso")
    @Expose
    private Object acceso;
    @SerializedName("Aplicaciones")
    @Expose
    private Object aplicaciones;
    @SerializedName("Usuario")
    @Expose
    private ValidarTokenUsuarioResponse usuario;

    public String getResultCode() {
        return resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public Object getAcceso() {
        return acceso;
    }

    public Object getAplicaciones() {
        return aplicaciones;
    }

    public ValidarTokenUsuarioResponse getUsuario() {
        return usuario;
    }
}