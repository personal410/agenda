package com.pacifico.agenda.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetCitaMovimientoEstadoResponse{
    @SerializedName("IdMovimiento")
    @Expose
    private String IdMovimiento;
    @SerializedName("IdMovimientoDispositivo")
    @Expose
    private String IdMovimientoDispositivo;

    public String getIdMovimiento() {
        return IdMovimiento;
    }

    public String getIdMovimientoDispositivo() {
        return IdMovimientoDispositivo;
    }
}