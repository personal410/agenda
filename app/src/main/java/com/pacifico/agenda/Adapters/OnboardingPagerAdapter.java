package com.pacifico.agenda.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.pacifico.agenda.Fragments.Onboarding.Onboarding1BienvenidaFragment;
import com.pacifico.agenda.Fragments.Onboarding.Onboarding2ShowbookFragment;
import com.pacifico.agenda.Fragments.Onboarding.Onboarding3ADNFragment;
import com.pacifico.agenda.Fragments.Onboarding.Onboarding4AgendaFragment;
import com.pacifico.agenda.Fragments.Onboarding.Onboarding5IdentificaFragment;
import com.pacifico.agenda.Fragments.Onboarding.Onboarding6CarteraFragment;
import com.pacifico.agenda.Fragments.Onboarding.Onboarding7InformacionFragment;

import java.util.ArrayList;
import java.util.List;

public class OnboardingPagerAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 7;
    private static final List<Fragment> mFragmentList = new ArrayList<>();
    public OnboardingPagerAdapter(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        if(position < mFragmentList.size()){
            return mFragmentList.get(position);
        }else{
            Fragment newFrag = null;
            if(position == 0){
                newFrag = new Onboarding1BienvenidaFragment();
            }else if(position == 1){
                newFrag = new Onboarding2ShowbookFragment();
            }else if(position == 2){
                newFrag = new Onboarding3ADNFragment();
            }else if(position == 3){
                newFrag = new Onboarding4AgendaFragment();
            }else if(position == 4){
                newFrag = new Onboarding5IdentificaFragment();
            }else if(position == 5){
                newFrag = new Onboarding6CarteraFragment();
            }else if(position == 6){
                newFrag = new Onboarding7InformacionFragment();
            }
            mFragmentList.add(newFrag);
            return newFrag;
        }
    }
    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}