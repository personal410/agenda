package com.pacifico.agenda.Adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.agenda.Activity.CalendarioActivity;
import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.Model.Bean.ProspectoDatosReferidoBean;
import com.pacifico.agenda.Model.Controller.ProspectoController;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Model.Bean.CitaProspectoBean;
import com.pacifico.agenda.Util.Constantes;
import com.pacifico.agenda.Util.Util;

import java.util.List;

public class ProspectosAdapter extends ArrayAdapter<ProspectoDatosReferidoBean> {

    private Activity context;
    private int resource;
    private List<ProspectoDatosReferidoBean> items;

    public ProspectosAdapter(Activity context, int resource, List<ProspectoDatosReferidoBean> items) {
        super(context, resource, items);
        this.context = context;
        this.resource = resource;
        this.items = items;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ProspectoDatosReferidoBean item = items.get(position);
        final LinearLayout rowView;

        rowView = new LinearLayout(getContext());
        inflater.inflate(resource, rowView, true);

        ImageView ivProspecto = (ImageView)rowView.findViewById(R.id.ivProspecto);
        TextView tvProspecto =  (TextView)rowView.findViewById(R.id.tvProspecto);
        TextView tvReferencia =  (TextView)rowView.findViewById(R.id.tvReferencia);

        if (resource == R.layout.row_prospectos_agendados)
        {
            if (item.getEtapaProspecto() == Constantes.EtapaProspecto_PrimeraEntrevista){
                ivProspecto.setImageResource(R.drawable.primera_entrevista);
            }else if (item.getEtapaProspecto() == Constantes.EtapaProspecto_SegundaEntrevista){
                ivProspecto.setImageResource(R.drawable.segunda_entrevista);
            }else if(item.getEtapaProspecto() == Constantes.EtapaProspecto_EntrevistaAdicional){
                ivProspecto.setImageResource(R.drawable.entrevista_adicional);
            }else{
                ivProspecto.setImageResource(R.drawable.calendario_gris_oscuro);
            }
        }

        String nombreCompleto = String.format("%s %s %s",
                                            Util.capitalizedString(item.getNombres()),
                                            Util.capitalizedString(item.getApellidoPaterno()),
                                            Util.capitalizedString(item.getApellidoMaterno()).trim()
        );

        tvProspecto.setText(nombreCompleto);

        if (item.getIdReferenciadorDispositivo() != -1)
        {

            String nombreCompletoReferenciador = String.format("%s %s %s",
                    Util.capitalizedString(item.getNombresReferenciador()),
                    Util.capitalizedString(item.getApellidoPaternoReferenciador()),
                    Util.capitalizedString(item.getApellidoMaternoReferenciador()).trim()
            );

            if (item.getNombreFuente() == null) item.setNombreFuente("");

            tvReferencia.setText("Ref. " + nombreCompletoReferenciador);
        }else{
            tvReferencia.setText(item.getNombreFuente());
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(item.getIdProspectoDispositivo());
                ((CalendarioActivity)context).GestionFlujoProspecto(prospectoBean);
            }
        });

        return rowView;
    }

}
