package com.pacifico.agenda.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pacifico.agenda.Model.Bean.ProspectoBean;
import com.pacifico.agenda.R;
import com.pacifico.agenda.Util.Util;

import java.util.Collection;
import java.util.List;

/**
 * Created by Joel on 08/05/2016.
 */
public class ListaProspectosAutocompleteAdapter extends RecyclerView.Adapter<ListaProspectosAutocompleteAdapter.ListaProspectosAutocompleteViewHolder> {

    public interface OnItemClickListener {
        void onProspectoAutocompleteClicked(ProspectoBean prospectoBean); // TODO: En vez de un string mandar la clase
    }

    private List<ProspectoBean> prospectoCollection;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;

    public ListaProspectosAutocompleteAdapter(Context context, List<ProspectoBean> prospectoCollection) {
        this.validateProspectosAutocompleteCollection(prospectoCollection);
        //msingleSelector = new SingleSelector();
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.prospectoCollection = (List<ProspectoBean>) prospectoCollection;
    }

    private void validateProspectosAutocompleteCollection(Collection<ProspectoBean> prospectoCollection) {
        if (prospectoCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    @Override
    public ListaProspectosAutocompleteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.fila_prospecto_autocomplete, parent, false);
        ListaProspectosAutocompleteViewHolder listProspectosAutocompleteViewHolder = new ListaProspectosAutocompleteViewHolder(view);

        return listProspectosAutocompleteViewHolder;
    }

    @Override
    public void onBindViewHolder(ListaProspectosAutocompleteViewHolder holder, int position) {

        String nombreCompleto= "";

        final ProspectoBean prospectoBean = this.prospectoCollection.get(position);
        if (prospectoBean != null){
            nombreCompleto = (Util.capitalizedString(prospectoBean.getNombres()) + " " + Util.capitalizedString(prospectoBean.getApellidoPaterno()) + " " + Util.capitalizedString(prospectoBean.getApellidoMaterno())).trim();
        }

        final String prospecto = nombreCompleto;
        holder.txtProspectoAutomplete.setText(prospecto);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (ListaProspectosAutocompleteAdapter.this.onItemClickListener != null) {
                    //if(!msingleSelector.tapSelector(v)) {
                    ListaProspectosAutocompleteAdapter.this.onItemClickListener.onProspectoAutocompleteClicked(prospectoBean);
                    //}
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (this.prospectoCollection != null) ? this.prospectoCollection.size() : 0;
    }

    public void setProspectosCollection(Collection<ProspectoBean> prospectosCollection) {
        this.validateProspectosAutocompleteCollection(prospectosCollection);
        this.prospectoCollection = (List<ProspectoBean>) prospectosCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    static class ListaProspectosAutocompleteViewHolder extends RecyclerView.ViewHolder {
        TextView txtProspectoAutomplete;

        public ListaProspectosAutocompleteViewHolder(View itemView) {
            super(itemView);
            txtProspectoAutomplete = (TextView) itemView.findViewById(R.id.txtProspectoAutomplete);;
        }
    }
}
